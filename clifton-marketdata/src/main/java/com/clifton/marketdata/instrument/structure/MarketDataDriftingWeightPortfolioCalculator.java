package com.clifton.marketdata.instrument.structure;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityUtilHandler;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureWeight;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.calculators.InvestmentStructureCurrentWeightCalculator;
import com.clifton.investment.instrument.structure.search.InvestmentSecurityStructureAllocationSearchForm;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.datasource.MarketDataSourceService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataDriftingWeightPortfolioCalculator</code> is used by Model Portfolio type structured indices which are made up of an allocation of instruments specifically
 * structured as a model portfolio.  The structure value and allocation weights drift based on the performance of the defined portfolio since the starting values were specified.
 * <p>
 * Note: This saves the daily allocated values as it is not easy to just preview it based on the nature of the calculation.
 *
 * @author manderson
 */
public class MarketDataDriftingWeightPortfolioCalculator implements InvestmentStructureCurrentWeightCalculator {

	private static final int DEFAULT_WEIGHT_PRECISION = 2;

	public static final String SECURITY_CUSTOM_COLUMN_INITIAL_STRUCTURE_VALUE = "Initial Structure Value";
	public static final String SECURITY_CUSTOM_COLUMN_FX_SOURCE = "FX Source";

	/**
	 * Allows users to override if they want more precision in the weight calculations
	 */
	private Integer weightPrecision;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentInstrumentStructureService investmentInstrumentStructureService;
	private InvestmentSecurityUtilHandler investmentSecurityUtilHandler;

	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataRetriever marketDataRetriever;
	private MarketDataSourceService marketDataSourceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentInstrumentStructureWeight(InvestmentInstrumentStructureWeight instrumentWeight) {
		// NO ADDITIONAL VALIDATION NEEDED
	}


	@Override
	public void calculateCurrentWeights(InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date) {
		try {
			// Additional Multiplier Not Used for this:
			BigDecimal additionalMultiplier = structure.getAdditionalMultiplier();
			if (additionalMultiplier != null && !MathUtils.isEqual(additionalMultiplier, BigDecimal.ONE)) {
				throw new ValidationException("Additional Multiplier is not used for this calculator.  Should be set to 1 to imply no effect on the calculation.");
			}

			// Security Level Information
			InvestmentSecurity security = securityAllocationList.get(0).getSecurityStructureWeight().getSecurity();
			BigDecimal initialStructureValue = (BigDecimal) getInvestmentSecurityUtilHandler().getCustomColumnValue(security, SECURITY_CUSTOM_COLUMN_INITIAL_STRUCTURE_VALUE);
			Integer fxSource = (Integer) getInvestmentSecurityUtilHandler().getCustomColumnValue(security, SECURITY_CUSTOM_COLUMN_FX_SOURCE);
			String dataSourceName = getMarketDataSourceService().getMarketDataSource(fxSource.shortValue()).getName();
			Date startDate = securityAllocationList.get(0).getSecurityStructureWeight().getInstrumentWeight().getInstrumentStructure().getStartDate();
			ValidationUtils.assertNotNull(startDate, "Start Date is required, but missing from the Instrument Structure.");

			Date priorDate;
			if (security.getInstrument().getExchange() != null && security.getInstrument().getExchange().getCalendar() != null) {
				priorDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, security.getInstrument().getExchange().getCalendar().getId()), -1);
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(startDate, security.getInstrument().getExchange().getCalendar().getId()))) {
					throw new ValidationException("Instrument Structure start date [" + DateUtils.fromDateShort(startDate) + "] is not a valid business day.");
				}
			}
			else {
				priorDate = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), -1);
				if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(startDate))) {
					throw new ValidationException("Instrument Structure start date [" + DateUtils.fromDateShort(startDate) + "] is not a valid business day.");
				}
			}

			BigDecimal currentStructureValue = initialStructureValue;
			for (InvestmentSecurityStructureAllocation allocation : CollectionUtils.getIterable(securityAllocationList)) {
				BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName, allocation.getCurrentSecurity().getInstrument().getTradingCurrency().getSymbol(), security.getInstrument().getTradingCurrency().getSymbol(), date));
				allocation.setFxRateToBase(fxRate);

				// If start date = current date
				if (DateUtils.isEqualWithoutTime(startDate, date)) {
					// Current Value Local = (Initial Structure Value * Weight) / FX Rate
					allocation.setAllocationValueLocal(MathUtils.divide(MathUtils.getPercentageOf(allocation.getSecurityStructureWeight().getCoalesceWeight(), initialStructureValue, true), fxRate));
					// No Gain/Loss on Day 0
					allocation.setAdditionalAmount1(BigDecimal.ZERO);
					allocation.setAdditionalAmount2(BigDecimal.ZERO);
				}
				else {
					List<InvestmentSecurityStructureAllocation> historicalAllocationList = getHistoricalAllocationList(allocation);
					InvestmentSecurityStructureAllocation priorAllocation = getHistoricalAllocation(historicalAllocationList, priorDate);
					// Note: This method also sets the Realized Gain/Loss Base (Additional Amount 2) based on historical rebalances/roll days
					InvestmentSecurityStructureAllocation initialAllocation = getLastRebalanceAllocation(allocation, historicalAllocationList);
					// No rebalances - go back to start date
					if (initialAllocation == null) {
						initialAllocation = getHistoricalAllocation(historicalAllocationList, startDate);
					}

					BigDecimal dailyReturn = getMarketDataRetriever().getInvestmentSecurityReturn(null, allocation.getCurrentSecurity(), priorDate, date, null, false);
					// Daily Return * Prior Notional Local + (Prior Notional Local – Initial Notional Local)
					BigDecimal totalGainLossLocal = MathUtils.add(MathUtils.multiply(dailyReturn, priorAllocation.getAllocationValueLocal()), MathUtils.subtract(priorAllocation.getAllocationValueLocal(), initialAllocation.getAllocationValueLocal()));
					allocation.setAllocationValueLocal(MathUtils.add(initialAllocation.getAllocationValueLocal(), totalGainLossLocal));

					allocation.setAdditionalAmount1(MathUtils.multiply(totalGainLossLocal, fxRate));
				}
				currentStructureValue = MathUtils.add(MathUtils.add(currentStructureValue, allocation.getAdditionalAmount1()), allocation.getAdditionalAmount2());
			}

			// Set allocation weights as a percentage of the Current notional base of the Current Structure Value
			applyAllocationWeights(securityAllocationList, currentStructureValue);
		}
		catch (Throwable e) {
			String errorMessage = "Error calculating model portfolio structured index " + structure.getLabel() + " on " + DateUtils.fromDateShort(date) + ": " + ExceptionUtils.getOriginalMessage(e);
			LogUtils.errorOrInfo(LogCommand.ofThrowable(getClass(), e));
			throw new ValidationException(errorMessage, e);
		}
	}


	private void applyAllocationWeights(List<InvestmentSecurityStructureAllocation> securityAllocationList, BigDecimal currentStructureValue) {
		for (InvestmentSecurityStructureAllocation allocation : CollectionUtils.getIterable(securityAllocationList)) {
			allocation.setAllocationWeight(MathUtils.round(CoreMathUtils.getPercentValue(MathUtils.multiply(allocation.getAllocationValueLocal(), allocation.getFxRateToBase()), currentStructureValue, true), getCoalesceWeightPrecision()));
		}
	}


	private InvestmentSecurityStructureAllocation getHistoricalAllocation(List<InvestmentSecurityStructureAllocation> historicalAllocationList, Date date) {
		InvestmentSecurityStructureAllocation allocation = CollectionUtils.getFirstElement(BeanUtils.filter(historicalAllocationList, InvestmentSecurityStructureAllocation::getMeasureDate, date));
		ValidationUtils.assertNotNull(allocation, "Cannot calculate allocations. Missing historical allocation on " + DateUtils.fromDateShort(date));
		return allocation;
	}


	private List<InvestmentSecurityStructureAllocation> getHistoricalAllocationList(InvestmentSecurityStructureAllocation allocation) {
		InvestmentSecurityStructureAllocationSearchForm searchForm = new InvestmentSecurityStructureAllocationSearchForm();
		searchForm.setSecurityStructureWeightId(allocation.getSecurityStructureWeight().getId());
		List<InvestmentSecurityStructureAllocation> historicalAllocationList = getInvestmentInstrumentStructureService().getInvestmentSecurityStructureAllocationList(searchForm);
		return BeanUtils.filter(historicalAllocationList, historicalAllocation -> DateUtils.isDateBefore(historicalAllocation.getMeasureDate(), allocation.getMeasureDate(), false));
	}


	/**
	 * Returns the last allocation just before the current security changed (i.e. roll day)
	 * Goes through full history as well and for each roll day historically - adds that day's unrealized gain/loss to current day's realized gain/loss
	 */
	private InvestmentSecurityStructureAllocation getLastRebalanceAllocation(InvestmentSecurityStructureAllocation allocation, List<InvestmentSecurityStructureAllocation> historicalAllocationList) {
		historicalAllocationList = BeanUtils.sortWithFunction(historicalAllocationList, InvestmentSecurityStructureAllocation::getMeasureDate, false);
		InvestmentSecurityStructureAllocation lastRebalanceAllocation = null;
		InvestmentSecurity currentSecurity = allocation.getCurrentSecurity();
		BigDecimal realizedGainLossBase = BigDecimal.ZERO;
		for (InvestmentSecurityStructureAllocation historicalAllocation : historicalAllocationList) {
			if (!historicalAllocation.getCurrentSecurity().equals(currentSecurity)) {
				if (lastRebalanceAllocation == null) {
					lastRebalanceAllocation = historicalAllocation;
				}
				currentSecurity = historicalAllocation.getCurrentSecurity();
				realizedGainLossBase = MathUtils.add(realizedGainLossBase, historicalAllocation.getAdditionalAmount1());
			}
		}
		allocation.setAdditionalAmount2(realizedGainLossBase);
		return lastRebalanceAllocation;
	}


	private Integer getCoalesceWeightPrecision() {
		if (getWeightPrecision() != null) {
			return getWeightPrecision();
		}
		return DEFAULT_WEIGHT_PRECISION;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getWeightPrecision() {
		return this.weightPrecision;
	}


	public void setWeightPrecision(Integer weightPrecision) {
		this.weightPrecision = weightPrecision;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentStructureService getInvestmentInstrumentStructureService() {
		return this.investmentInstrumentStructureService;
	}


	public void setInvestmentInstrumentStructureService(InvestmentInstrumentStructureService investmentInstrumentStructureService) {
		this.investmentInstrumentStructureService = investmentInstrumentStructureService;
	}


	public InvestmentSecurityUtilHandler getInvestmentSecurityUtilHandler() {
		return this.investmentSecurityUtilHandler;
	}


	public void setInvestmentSecurityUtilHandler(InvestmentSecurityUtilHandler investmentSecurityUtilHandler) {
		this.investmentSecurityUtilHandler = investmentSecurityUtilHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
