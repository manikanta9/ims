package com.clifton.marketdata.instrument.allocation;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.marketdata.field.MarketDataValue;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataInvestmentSecurityAllocationService</code> ...
 *
 * @author manderson
 */
public interface MarketDataInvestmentSecurityAllocationService {

	/////////////////////////////////////////////////////////////////////
	////////   MarketDataInvestmentSecurityAllocation Methods  //////////
	/////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationList(SecurityAllocationSearchForm searchForm);


	/**
	 * Returns a DataTable representation (One row per Allocation, Column for each Date in the date range) of current weights on all weekdays for the date range
	 * If start date is null or before parent security start, will push start date forward to parent security start
	 * If end date is null or after today, will push end date back to today
	 */
	@SecureMethod(dtoClass = InvestmentSecurityAllocation.class)
	public DataTable getMarketDataInvestmentSecurityAllocationWeightListForDateRange(int parentInvestmentSecurityId, Date startDate, Date endDate);


	///////////////////////////////////////////////////////////////////////////////
	////////      Market Data Allocated Security Price Calculations      //////////
	///////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns calculated market data value and details of the calculation for preview
	 */
	@ModelAttribute("data")
	@SecureMethod(dtoClass = MarketDataValue.class)
	public String previewMarketDataAllocatedValueForSecurity(int securityId, Date previewDate, short dataSourceId);


	@DoNotAddRequestMapping
	public Status calculateInvestmentSecurityAllocationMarketDataValues(List<InvestmentSecurity> securityList, short dataSourceId, Date startDate, Date endDate, Status status);
}
