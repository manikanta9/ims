package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationRebalance;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationRebalanceCalculator;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataInvestmentSecurityAllocationSharePriceCalculator</code> ...
 *
 * @author manderson
 */
public class MarketDataInvestmentSecurityAllocationSharePriceCalculator extends BaseMarketDataInvestmentSecurityAllocationCalculator implements InvestmentSecurityAllocationRebalanceCalculator {

	@Override
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		ValidationUtils.assertFalse(bean.getAllocationWeight() == null && bean.getAllocationShares() == null, "At least one of [" + getInvestmentSecurityAllocationType().getWeightLabel() + " or "
				+ getInvestmentSecurityAllocationType().getSharesLabel() + "] is required for each allocation.", "allocationWeight");
	}


	@Override
	public List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationList(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		List<MarketDataInvestmentSecurityAllocation> mdaList = super.getMarketDataInvestmentSecurityAllocationList(security, date, config);
		// Calculate Current Weights
		calculateWeightsFromShares(mdaList, true);
		return mdaList;
	}


	@Override
	protected void populateMarketDataInvestmentSecurityAllocation(Date date, MarketDataInvestmentSecurityAllocation allocation, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		super.populateMarketDataInvestmentSecurityAllocation(date, allocation, config);
		populateCurrentAllocationValue(allocation, date, config);
	}


	@Override
	public BigDecimal calculateAllocatedSecurityValueImpl(InvestmentSecurity security, List<MarketDataInvestmentSecurityAllocation> allocationList, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config, StringBuilder previewResult, int count) {
		BigDecimal value = BigDecimal.ZERO;
		for (MarketDataInvestmentSecurityAllocation alloc : allocationList) {
			BigDecimal allocValue = alloc.getCurrentAllocationValue();
			// Missing Value - Skip
			if (allocValue == null) {
				return null;
			}
			value = MathUtils.add(value, allocValue);

			if (previewResult != null) {
				previewResult.append(StringUtils.NEW_LINE)
						.append(alloc.getCurrentSecurity().getSymbol())
						.append(" Price: ")
						.append(CoreMathUtils.formatNumberDecimal(alloc.getCurrentPrice()))
						.append(", Shares: ")
						.append(CoreMathUtils.formatNumberDecimal(alloc.getAllocationShares()))
						.append(", Allocation Value: ")
						.append(CoreMathUtils.formatNumberDecimal(allocValue));
			}
		}
		// Price Multiplier = Index Divisor
		value = MathUtils.divide(value, security.getPriceMultiplier());
		return value;
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////                     Rebalancing Methods                    //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void rebalanceInvestmentSecurity(InvestmentSecurity security, short dataSourceId, Date baseDate, boolean useBaseWeights, Date startDate, BigDecimal startingPrice) {
		List<InvestmentSecurityAllocationRebalance> rebalances = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationRebalanceList(security, false);
		// If baseDate is blank, default to use the last base date rebalancing was done
		Date lastBaseDate = null;
		for (InvestmentSecurityAllocationRebalance rebalance : CollectionUtils.getIterable(rebalances)) {
			if (DateUtils.compare(startDate, rebalance.getRebalanceDate(), false) <= 0) {
				throw new ValidationException("Cannot create a new Rebalance on [" + DateUtils.fromDateShort(startDate) + "] because this security has been rebalanced on or after this date ("
						+ DateUtils.fromDateShort(rebalance.getRebalanceDate()) + ")");
			}
			if (lastBaseDate == null || DateUtils.compare(lastBaseDate, rebalance.getRebalanceDate(), false) < 0) {
				lastBaseDate = rebalance.getRebalanceDate();
			}
		}
		MarketDataInvestmentSecurityAllocationCalculatorConfig config = new MarketDataInvestmentSecurityAllocationCalculatorConfig(getCalendarSetupService().getDefaultCalendar(), getMarketDataSourceService().getMarketDataSource(dataSourceId));
		// Get the weights from that date
		// If baseDate is null, then OK with current base weights - keep them
		if (baseDate == null) {
			baseDate = lastBaseDate;
		}
		List<MarketDataInvestmentSecurityAllocation> mdaList = getMarketDataInvestmentSecurityAllocationList(security, baseDate, config);

		// Get Active Allocations on Start Date
		List<InvestmentSecurityAllocation> allocationList = config.getInvestmentSecurityAllocationListActiveOnDate(security, startDate, getInvestmentSecurityAllocationHandler());
		List<InvestmentSecurityAllocation> newAllocationList = new ArrayList<>();
		// For each one - end it on start date - 1, and create a new list
		for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			allocation.setEndDate(DateUtils.addDays(startDate, -1));
		}

		// Go through Base Date Allocations - and Create new ones starting on start date
		for (MarketDataInvestmentSecurityAllocation baseAllocation : CollectionUtils.getIterable(mdaList)) {
			InvestmentSecurityAllocation newAllocation = new InvestmentSecurityAllocation();
			BeanUtils.copyPropertiesExceptAudit(baseAllocation, newAllocation);
			newAllocation.setId(null);
			newAllocation.setStartDate(startDate);
			newAllocation.setEndDate(null);
			if (useBaseWeights) {
				newAllocation.setAllocationWeight(baseAllocation.getCurrentAllocationWeight());
				newAllocation.setAllocationShares(null);
			}
			else {
				newAllocation.setAllocationWeight(null);
				newAllocation.setAllocationShares(baseAllocation.getAllocationShares());
			}
			newAllocationList.add(newAllocation);
		}

		getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(allocationList, null);
		getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(newAllocationList, null);

		// After saving the new allocations - recalculate shares from weights, or weights from shares
		recalculateRebalance(security, dataSourceId, startDate, startingPrice, !useBaseWeights);
	}


	@Override
	public void recalculateRebalance(InvestmentSecurity security, short dataSourceId, Date date, BigDecimal startingPrice, boolean updateWeights) {
		List<InvestmentSecurityAllocationRebalance> rebalances = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationRebalanceList(security, false);
		if (date != null) {
			rebalances = BeanUtils.filter(rebalances, InvestmentSecurityAllocationRebalance::getRebalanceDate, date);
			ValidationUtils.assertNotEmpty(rebalances, "[" + DateUtils.fromDateShort(date) + "] is not a valid date to recalculate rebalance on since it is not an existing rebalance date.");
		}

		MarketDataInvestmentSecurityAllocationCalculatorConfig config = new MarketDataInvestmentSecurityAllocationCalculatorConfig(getCalendarSetupService().getDefaultCalendar(), getMarketDataSourceService().getMarketDataSource(dataSourceId));

		for (InvestmentSecurityAllocationRebalance rebalance : CollectionUtils.getIterable(rebalances)) {
			// Because Rebalances causes overall changes in shares or base weights on every allocation, if the allocation start date is not the same as the rebalance
			// date - create a new allocation and end the old one.
			// What if date is today or in the future - use last known price since users can recalc again later?
			List<InvestmentSecurityAllocation> allocationList = config.getInvestmentSecurityAllocationListActiveOnDate(security, rebalance.getRebalanceDate(), getInvestmentSecurityAllocationHandler());
			if (CollectionUtils.isEmpty(allocationList)) {
				throw new ValidationException("Unable to recalculate rebalance on [" + DateUtils.fromDateShort(date) + "] because there are no active allocations on that date.");
			}

			List<InvestmentSecurityAllocation> updateList = new ArrayList<>();
			List<InvestmentSecurityAllocation> rebalanceList = new ArrayList<>();

			for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(allocationList)) {
				if (DateUtils.compare(rebalance.getRebalanceDate(), allocation.getStartDate(), false) == 0 || (rebalance.isSecurityStart() && allocation.getStartDate() == null)) {
					rebalanceList.add(allocation);
				}
				else {
					// Create a new one
					InvestmentSecurityAllocation newAllocation = BeanUtils.cloneBean(allocation, false, false);
					newAllocation.setId(null);
					newAllocation.setStartDate(rebalance.getRebalanceDate());
					rebalanceList.add(newAllocation);

					// End the old one and add to updateList for saving
					allocation.setEndDate(DateUtils.addDays(rebalance.getRebalanceDate(), -1));
					updateList.add(allocation);
				}
			}

			// Recalculate rebalance list
			// Calculate Weights From Shares
			List<MarketDataInvestmentSecurityAllocation> mdaList = getMarketDataInvestmentSecurityAllocationListFromList(rebalanceList, rebalance.getRebalanceDate(), config);
			if (!isMarketDataInvestmentSecurityAllocationListValid(mdaList)) {
				throw new ValidationException("Unable to recalculate rebalance on [" + DateUtils.fromDateShort(date) + "] because there is at least one allocation missing a price on that date.");
			}
			if (updateWeights) {
				calculateWeightsFromShares(mdaList, false);
			}
			else {
				// If starting price is null, keep pre-calculated price the same - otherwise, if not there, will default to 100
				if (startingPrice == null) {
					startingPrice = getMarketDataRetriever().getPrice(security, date, false, null);
				}
				BigDecimal total = MathUtils.multiply((startingPrice == null ? MathUtils.BIG_DECIMAL_ONE_HUNDRED : startingPrice), security.getPriceMultiplier());
				calculateSharesFromWeights(total, mdaList);
			}

			// If we had to discontinue anything:
			if (!CollectionUtils.isEmpty(updateList)) {
				getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(updateList, null);
			}

			// Save the updated properties
			List<InvestmentSecurityAllocation> saveList = new ArrayList<>();
			for (MarketDataInvestmentSecurityAllocation mda : CollectionUtils.getIterable(mdaList)) {
				InvestmentSecurityAllocation alloc = new InvestmentSecurityAllocation();
				BeanUtils.copyProperties(mda, alloc);
				saveList.add(alloc);
			}
			getInvestmentSecurityAllocationHandler().saveInvestmentSecurityAllocationList(saveList, null);
		}

		// Re-calc prices automatically after a rebalance is re-calculated
		calculateAllocatedMarketDataValues(security, date, new Date(), config);
	}

	////////////////////////////////////////////////////////////////////////////////
	//////////////                     Helper Methods                 //////////////
	////////////////////////////////////////////////////////////////////////////////


	private void populateCurrentAllocationValue(MarketDataInvestmentSecurityAllocation alloc, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		// Note: Not cached because only performs any actual db look ups if the security hierarchy IsIndexRatioAdjusted = 1
		BigDecimal price = alloc.getCurrentPrice();
		BigDecimal value = null;
		// Only if Price is there
		if (price != null) {
			// To avoid rounding issues (notional is usually rounded to 2) - set quantity to 1,000,000 when calculating notional, then back it out
			BigDecimal quantity = BigDecimal.valueOf(1_000_000);
			value = getInvestmentCalculator().calculateNotional(alloc.getCurrentSecurity(), price, quantity, date);
			if (!alloc.getParentInvestmentSecurity().getInstrument().getTradingCurrency().equals(alloc.getCurrentSecurity().getInstrument().getTradingCurrency())) {
				BigDecimal fxRate = getMarketDataExchangeRate(alloc.getInvestmentSecurity().getInstrument().getTradingCurrency(), alloc.getParentInvestmentSecurity()
						.getInstrument()
						.getTradingCurrency(), date, date, config, true);
				value = MathUtils.multiply(value, fxRate);
				alloc.setCurrencyExchangeRate(fxRate);
			}
			value = MathUtils.divide(value, quantity);
		}
		alloc.setCurrentValueOfOneShare(value);
	}


	private boolean calculateWeightsFromShares(List<MarketDataInvestmentSecurityAllocation> allocationList, boolean useCurrentWeight) {
		BigDecimal total = CoreMathUtils.sumProperty(allocationList, MarketDataInvestmentSecurityAllocation::getCurrentAllocationValue);

		// Verify None Missing Prices - If so - current weight can't be calculated (doesn't make sense)
		if (isMarketDataInvestmentSecurityAllocationListValid(allocationList)) {
			for (MarketDataInvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
				BigDecimal value = MathUtils.round(CoreMathUtils.getPercentValue(alloc.getCurrentAllocationValue(), total, true), 5);
				if (useCurrentWeight) {
					alloc.setCurrentAllocationWeight(value);
				}
				else {
					alloc.setAllocationWeight(value);
				}
			}
			CoreMathUtils.applySumPropertyDifference(allocationList, useCurrentWeight ? MarketDataInvestmentSecurityAllocation::getCurrentAllocationWeight : MarketDataInvestmentSecurityAllocation::getAllocationWeight, useCurrentWeight ? MarketDataInvestmentSecurityAllocation::setCurrentAllocationWeight : MarketDataInvestmentSecurityAllocation::setAllocationWeight, MathUtils.BIG_DECIMAL_ONE_HUNDRED, true);
			return true;
		}
		return false;
	}


	private boolean calculateSharesFromWeights(BigDecimal total, List<MarketDataInvestmentSecurityAllocation> allocationList) {
		// Verify None Missing Prices - If so - current weight can't be calculated (doesn't make sense)
		if (isMarketDataInvestmentSecurityAllocationListValid(allocationList)) {
			for (MarketDataInvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
				BigDecimal allocTotal = MathUtils.getPercentageOf(alloc.getAllocationWeight(), total, true);
				alloc.setAllocationShares(MathUtils.divide(allocTotal, alloc.getCurrentValueOfOneShare()));
			}
			return true;
		}
		return false;
	}


	private boolean isMarketDataInvestmentSecurityAllocationListValid(List<MarketDataInvestmentSecurityAllocation> allocationList) {
		// Nothing in the list - nothing to calculate
		if (CollectionUtils.isEmpty(allocationList)) {
			return false;
		}
		// Verify None Missing Prices - If so - current weight can't be calculated (doesn't make sense)
		List<MarketDataInvestmentSecurityAllocation> noPriceList = BeanUtils.filter(allocationList, allocation -> allocation.getCurrentPrice() == null);
		return CollectionUtils.isEmpty(noPriceList);
	}
}
