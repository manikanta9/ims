package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataInvestmentSecurityAllocationWeightedReturnCalculator</code> ...
 *
 * @author manderson
 */
public class MarketDataInvestmentSecurityAllocationWeightedReturnCalculator extends BaseMarketDataInvestmentSecurityAllocationCalculator {

	public enum WeightedReturnAdjustmentTypes {
		FROM_START,
		DAILY,
		MONTHLY
	}


	private WeightedReturnAdjustmentTypes adjustmentType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		ValidationUtils.assertNotNull(bean.getAllocationWeight(), getInvestmentSecurityAllocationType().getWeightLabel() + " is required for each allocation.", "allocationWeight");
	}


	@Override
	public BigDecimal calculateAllocatedSecurityValueImpl(InvestmentSecurity security, List<MarketDataInvestmentSecurityAllocation> allocationList, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config, StringBuilder previewResult, int count) {
		Date previousDate = null;
		BigDecimal previousValue;

		BigDecimal value = BigDecimal.ZERO;
		BigDecimal totalPercentChange = BigDecimal.ZERO;

		if (DateUtils.compare(date, security.getStartDate(), false) == 0) {
			value = MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		}
		else {
			BigDecimal totalAllocation = CoreMathUtils.sumProperty(allocationList, MarketDataInvestmentSecurityAllocation::getAbsAllocationWeight);

			StringBuilder priceInfo = null;
			if (previewResult != null) {
				priceInfo = new StringBuilder(16);
			}

			for (MarketDataInvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
				InvestmentSecurity allocationCurrentSecurity = alloc.getCurrentSecurity();
				BigDecimal allocationWeight = CoreMathUtils.getPercentValue(alloc.getAbsAllocationWeight(), totalAllocation, true);
				BigDecimal returnWeight = alloc.getAllocationWeight();

				BigDecimal allocationPreviousValue;

				// Daily
				if (WeightedReturnAdjustmentTypes.DAILY == getAdjustmentType()) {
					previousDate = getPreviousDay(security, date, config);
					previousValue = getMarketDataPriceForSecurity(security, null, security.getStartDate(), previousDate, config, false, null);
					if (previousValue == null) {
						previousValue = calculateAllocatedSecurityValue(security, previousDate, config, null, count);
					}
					// Get Previous Day's Value
					// Do Not Lookup Previous Business Day for the specific security - always use the parent's previous day first - price lookup will go back if it's not a business day
					InvestmentSecurity previousCurrentSecurity = config.getCurrentSecurityForSecurityAllocation(alloc, previousDate, getSystemBeanService());
					allocationPreviousValue = getMarketDataPriceForSecurity(previousCurrentSecurity, alloc, security.getStartDate(), previousDate, config, false, priceInfo);
				}
				// Monthly
				else if (WeightedReturnAdjustmentTypes.MONTHLY == getAdjustmentType()) {
					previousDate = getMonthlyRebalancePreviousDay(config, security, date);
					previousValue = getMarketDataPriceForSecurity(security, null, security.getStartDate(), previousDate, config, false, null);
					if (previousValue == null) {
						previousValue = calculateAllocatedSecurityValue(security, previousDate, config, null, count);
					}
					// Get Previous Month End Value
					InvestmentSecurity previousCurrentSecurity = config.getCurrentSecurityForSecurityAllocation(alloc, previousDate, getSystemBeanService());
					allocationPreviousValue = getMarketDataPriceForSecurity(previousCurrentSecurity, alloc, security.getStartDate(), previousDate, config, false, priceInfo);
				}
				else {
					// Get Start Date's Value
					previousDate = security.getStartDate();
					previousValue = MathUtils.BIG_DECIMAL_ONE_HUNDRED;
					allocationPreviousValue = getMarketDataPriceForSecurity(config.getCurrentSecurityForSecurityAllocation(alloc, security.getStartDate(), getSystemBeanService()), alloc, security.getStartDate(), security.getStartDate(),
							config, true, priceInfo);
				}
				// Get Current Value
				//BigDecimal allocationCurrentValue = alloc.getCurrentPrice();
				BigDecimal allocationCurrentValue = getMarketDataPriceForSecurity(alloc.getCurrentSecurity(), alloc, security.getStartDate(), date, config, true, priceInfo);

				// Missing Previous Value or Allocation Prices - Skip
				if ((previousValue == null) || allocationCurrentValue == null || allocationPreviousValue == null) {
					if (previewResult != null) {
						previewResult.append(" Missing Previous Value or Allocation Prices ");
					}
					return null;
				}

				BigDecimal percentChange = MathUtils.getPercentChange(allocationPreviousValue, allocationCurrentValue, true);
				if (previewResult != null) {
					previewResult.append(allocationCurrentSecurity.getSymbol()).append(": % Change: ")
							.append(CoreMathUtils.formatNumberDecimal(percentChange))
							.append(", Return Weight: ")
							.append(CoreMathUtils.formatNumberDecimal(returnWeight))
							.append(", Weighted % Change: ")
							.append(MathUtils.getPercentageOf(returnWeight, percentChange, true)).append(priceInfo.toString()).append(StringUtils.NEW_LINE);
					priceInfo = new StringBuilder();
				}
				percentChange = MathUtils.getPercentageOf(returnWeight, percentChange, true);
				totalPercentChange = MathUtils.add(totalPercentChange, percentChange);

				BigDecimal allocValue = MathUtils.getPercentageOf(allocationWeight, previousValue, true);
				value = MathUtils.add(value, allocValue);
			}
		}


		if (previewResult != null) {
			previewResult.append(StringUtils.NEW_LINE + "Previous Value [").append(CoreMathUtils.formatNumberDecimal(value)).append("] on [").append(DateUtils.fromDateShort(previousDate)).append("], Total % Change: ").append(CoreMathUtils.formatNumberDecimal(totalPercentChange));
		}

		value = MathUtils.add(value, MathUtils.getPercentageOf(totalPercentChange, value, true));
		return value;
	}


	private Date getMonthlyRebalancePreviousDay(MarketDataInvestmentSecurityAllocationCalculatorConfig config, InvestmentSecurity security, Date date) {
		Integer monthlyRebalanceBusinessDay = config.getMonthlyRebalanceBusinessDayForSecurity(security, getSystemColumnValueHandler());
		Date previousDate;
		// First business day of month, means always use previous month
		if (MathUtils.isEqual(1, monthlyRebalanceBusinessDay)) {
			previousDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(date), -1);
		}
		else {
			// Anything after that, we need to check what day we are on
			Integer dayOfMonth = getCalendarBusinessDayService().getBusinessDayOfMonth(CalendarBusinessDayCommand.forDate(date, config.getCalendarForSecurity(security, getInvestmentCalculator())));
			if (dayOfMonth >= monthlyRebalanceBusinessDay) {
				previousDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(date), -1);
			}
			else {
				previousDate = DateUtils.addDays(DateUtils.addMonths(DateUtils.getFirstDayOfMonth(date), -1), -1);
			}
		}
		// Don't get previous business day again.  Let's look up price on given date no matter what
		// Then if it's a holiday and no value, price look up itself will check previous business day
		// previousDate = getCalendarBusinessDayService().getPreviousBusinessDay(DateUtils.addDays(previousDate, 1), config.getCalendarForSecurity(security));
		// If previous date is before security start, always use security start
		if (DateUtils.compare(previousDate, security.getStartDate(), false) < 0) {
			return security.getStartDate();
		}
		return previousDate;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public WeightedReturnAdjustmentTypes getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(WeightedReturnAdjustmentTypes adjustmentType) {
		this.adjustmentType = adjustmentType;
	}
}
