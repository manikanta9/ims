package com.clifton.marketdata.instrument.structure;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructure;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.calculators.BaseInvestmentStructureCurrentWeightCalculator;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.datasource.MarketDataSourcePriceMultiplier;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.search.MarketDataSourcePriceMultiplierSearchForm;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataPriceWeightCalculator</code> calculates weights for each {@link InvestmentSecurityStructureAllocation}
 * as
 * Structure Additional Multiplier
 * * Adjusted Security Price (Price / Goldman's Price Multiplier) NOTE: Uses Goldman's Adjustment Factor to divide by since Bloomberg prices are actually the ones that are wrong - will be addressed separately
 * * Coalesce(SecurityWeightOverride, InstrumentWeight)
 *
 * @author manderson
 */
public class MarketDataPriceWeightCalculator extends BaseInvestmentStructureCurrentWeightCalculator {

	private MarketDataRetriever marketDataRetriever;

	private MarketDataSourceService marketDataSourceService;

	/**
	 * Special temporary group used for instrument structures - will be removed once
	 * Bloomberg/exchange prices are cleaned up to be automatically converted
	 */
	public static final String MARKET_DATASOURCE_PRICE_MULTIPLIERS = "Clifton Investment Structure Price Multipliers";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void calculateCurrentWeightsImpl(@SuppressWarnings("unused") InvestmentInstrumentStructure structure, List<InvestmentSecurityStructureAllocation> securityAllocationList, Date date,
	                                        BigDecimal additionalMultiplier) {
		// Look up all the "adjustment factors" at once - there aren't many and then can just filter the list for specific instruments
		MarketDataSourcePriceMultiplierSearchForm searchForm = new MarketDataSourcePriceMultiplierSearchForm();
		searchForm.setDataSourceName(MARKET_DATASOURCE_PRICE_MULTIPLIERS);
		List<MarketDataSourcePriceMultiplier> priceAdjustmentFactorList = getMarketDataSourceService().getMarketDataSourcePriceMultiplierList(searchForm);

		for (InvestmentSecurityStructureAllocation allocation : CollectionUtils.getIterable(securityAllocationList)) {
			calculateWeightForAllocation(allocation, date, priceAdjustmentFactorList, additionalMultiplier);
		}
	}


	private void calculateWeightForAllocation(InvestmentSecurityStructureAllocation allocation, Date date, List<MarketDataSourcePriceMultiplier> priceAdjustmentFactorList,
	                                          BigDecimal additionalMultiplier) {
		BigDecimal price = getMarketDataRetriever().getPriceFlexible(allocation.getCurrentSecurity(), date, true);
		allocation.appendComment("Current Security Price: " + CoreMathUtils.formatNumberDecimal(price));
		if (!CollectionUtils.isEmpty(priceAdjustmentFactorList)) {
			MarketDataSourcePriceMultiplier adjFactor = CollectionUtils.getFirstElement(
					BeanUtils.filter(priceAdjustmentFactorList, MarketDataSourcePriceMultiplier::getInvestmentInstrument, allocation.getCurrentSecurity().getInstrument()));
			if (adjFactor != null && !MathUtils.isNullOrZero(adjFactor.getAdditionalPriceMultiplier())) {
				price = MathUtils.divide(price, adjFactor.getAdditionalPriceMultiplier());
				allocation.appendComment("Price Adjustment Factor: " + adjFactor.getAdditionalPriceMultiplier());
			}
		}
		BigDecimal weight = MathUtils.multiply(additionalMultiplier, MathUtils.multiply(price, allocation.getSecurityStructureWeight().getCoalesceWeight()));
		// Will be re-allocated as a percentage after all allocations are calculated
		allocation.setAllocationWeight(weight);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}
}
