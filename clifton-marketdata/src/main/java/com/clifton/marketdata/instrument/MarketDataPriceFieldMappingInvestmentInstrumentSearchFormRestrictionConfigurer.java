package com.clifton.marketdata.instrument;

import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataPriceFieldMappingInvestmentInstrumentSearchFormRestrictionConfigurer</code> class allows to define cross-project form restriction
 * for finding instruments with the price field mapping id
 *
 * @author manderson
 */
@Component
public class MarketDataPriceFieldMappingInvestmentInstrumentSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {

	@Override
	public Class<?> getSearchFormClass() {
		return InstrumentSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "priceFieldMappingId";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			// JOIN ON THE MarketDataPriceFieldMapping table to find the instruments that use that specific mapping
			String sql = "EXISTS (SELECT i.InvestmentInstrumentID FROM InvestmentInstrument i " + //
					"INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID " + //
					"LEFT JOIN MarketDataPriceFieldMapping im ON i.InvestmentInstrumentID = im.InvestmentInstrumentID " + //
					"LEFT JOIN MarketDataPriceFieldMapping hm ON i.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID " + //
					"LEFT JOIN MarketDataPriceFieldMapping st12m ON h.InvestmentTypeSubTypeID = st12m.InvestmentTypeSubTypeID AND h.InvestmentTypeSubType2ID = st12m.InvestmentTypeSubType2ID " + //
					"LEFT JOIN MarketDataPriceFieldMapping st2m ON h.InvestmentTypeSubType2ID = st2m.InvestmentTypeSubType2ID AND st2m.InvestmentTypeSubTypeID IS NULL " + //
					"LEFT JOIN MarketDataPriceFieldMapping st1m ON h.InvestmentTypeSubTypeID = st1m.InvestmentTypeSubTypeID AND st1m.InvestmentTypeSubType2ID IS NULL " + //
					"LEFT JOIN MarketDataPriceFieldMapping tm ON h.InvestmentTypeID = tm.InvestmentTypeID AND tm.InvestmentTypeSubTypeID IS NULL AND tm.InvestmentTypeSubType2ID IS NULL " + //
					"INNER JOIN MarketDataPriceFieldMapping m ON m.MarketDataPriceFieldMappingID = COALESCE(im.MarketDataPriceFieldMappingID, hm.MarketDataPriceFieldMappingID, st12m.MarketDataPriceFieldMappingID, st2m.MarketDataPriceFieldMappingID, st1m.MarketDataPriceFieldMappingID, tm.MarketDataPriceFieldMappingID) " + //
					"WHERE {alias}.InvestmentInstrumentID = i.InvestmentInstrumentID AND " + //
					"m.MarketDataPriceFieldMappingID = ?)";

			criteria.add(Restrictions.sqlRestriction(sql, restriction.getValue(), IntegerType.INSTANCE));
		}
	}
}
