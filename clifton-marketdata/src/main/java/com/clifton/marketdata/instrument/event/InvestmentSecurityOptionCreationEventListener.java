package com.clifton.marketdata.instrument.event;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.live.theoretical.MarketDataOptionTheoreticalValueCommand;
import com.clifton.marketdata.live.theoretical.MarketDataTheoreticalValueService;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityResult;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityRetrieverCommand;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityService;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * A listener that responds to the creation of new options so that they can be provided with market data.
 *
 * @author davidi
 */
@Component
public class InvestmentSecurityOptionCreationEventListener extends BaseEventListener<Event<InvestmentSecurity, Object>> {

	private static final String BLOOMBERG = "Bloomberg";

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingService marketDataFieldMappingService;
	private MarketDataInvestmentSecurityService marketDataInvestmentSecurityService;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataTheoreticalValueService marketDataTheoreticalValueService;
	private MarketDataLiveService marketDataLiveService;
	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(Event<InvestmentSecurity, Object> event) {
		if (!event.getTarget().isNewBean()) {
			// Look up the security from the database to make sure it exists and was successfully persisted to the database.
			// It is possible the security save was rolled back for some reason and this event should be ignored.
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(event.getTarget().getId());
			if (security != null && security.getSymbol().equals(event.getTarget().getSymbol()) && InvestmentUtils.isSecurityCreatedToday(security)) {
				if (InvestmentSecurity.INVESTMENT_SECURITY_CREATED_TODAY_EVENT.equals(event.getEventName())
						|| InvestmentSecurity.NEW_INVESTMENT_SECURITY_UPDATED_TODAY_EVENT.equals(event.getEventName())) {
					if (InvestmentUtils.isListedOption(security)) {
						applyLivePriceToListedOption(security);
					}
					else if (security.getReferenceSecurity() != null && (InvestmentUtils.isFlexOption(security) || InvestmentUtils.isLookAlikeOption(security) || InvestmentUtils.isCustomOption(security))) {
						asynchronouslyImportTheoreticalPrice(security);
					}
				}
				else if (InvestmentSecurity.INVESTMENT_SECURITY_APPLY_MARKET_DATA_EVENT.equals(event.getEventName())) {
					applyMarketDataFieldMappingValuesToNewSecurity(security);
				}
			}
		}
	}


	@Override
	public List<String> getEventNameList() {
		return CollectionUtils.createList(InvestmentSecurity.INVESTMENT_SECURITY_CREATED_TODAY_EVENT,
				InvestmentSecurity.NEW_INVESTMENT_SECURITY_UPDATED_TODAY_EVENT,
				InvestmentSecurity.INVESTMENT_SECURITY_APPLY_MARKET_DATA_EVENT);
	}


	@Transactional
	protected void applyLivePriceToListedOption(InvestmentSecurity security) {
		if (security == null || !InvestmentUtils.isListedOption(security)) {
			return;
		}

		List<MarketDataFieldMapping> fieldMappingList = getMarketDataFieldMappingService().getMarketDataFieldMappingListByInstrument(security.getInstrument().getId());
		for (MarketDataFieldMapping fieldMapping : CollectionUtils.getIterable(fieldMappingList)) {
			if (BLOOMBERG.equalsIgnoreCase(fieldMapping.getMarketDataSource().getName())) {
				// import market data from Blooomberg for this option asynchronously to avoid blocking
				asynchronouslyImportBloombergLivePrice(security, fieldMapping.getMarketDataSource());
				// break to avoid double import
				break;
			}
		}
	}


	private void asynchronouslyImportBloombergLivePrice(InvestmentSecurity security, MarketDataSource marketDataSource) {
		Status status = new Status();
		Date now = new Date();
		String runId = security.getId() + "_" + DateUtils.fromDate(now, DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);

		Runner runner = new AbstractStatusAwareRunner("APPLY_LIVE_OPTION_PRICE", runId, now, new StatusHolder(status)) {
			@Override
			public void run() {
				try {

					final MarketDataLive marketDataLive = getMarketDataLiveService().getMarketDataLivePrice(security.getId(), null);
					if (marketDataLive != null && marketDataLive.getValue() != null) {
						final MarketDataField marketDataField = getMarketDataFieldService().getMarketDataFieldByName(marketDataLive.getFieldName());
						final MarketDataValue marketDataValue = new MarketDataValue();
						marketDataValue.setMeasureDate(marketDataLive.getMeasureDate());
						if (marketDataField.isTimeSensitive()) {
							marketDataValue.setMeasureTime(new Time(now));
						}
						marketDataValue.setInvestmentSecurity(security);
						marketDataValue.setDataField(marketDataField);

						marketDataValue.setDataSource(marketDataSource);
						marketDataValue.setMeasureValue(MathUtils.round(marketDataLive.asNumericValue(), marketDataField.getDecimalPrecision()));
						getMarketDataFieldService().saveMarketDataValue(marketDataValue);
					}
				}
				catch (Throwable e) {
					getStatus().setMessage("Error getting live price data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error getting live price data for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
	}


	private void asynchronouslyImportTheoreticalPrice(InvestmentSecurity security) {
		Status status = new Status();
		Date now = new Date();
		String runId = security.getId() + "_" + DateUtils.fromDate(now, DateUtils.DATE_FORMAT_FULL_NO_SEPARATORS);

		Runner runner = new AbstractStatusAwareRunner("APPLY_THEORETICAL_OPTION_PRICE", runId, now, new StatusHolder(status)) {
			@Override
			public void run() {
				try {
					MarketDataOptionTheoreticalValueCommand command = new MarketDataOptionTheoreticalValueCommand();
					command.setSecurity(security);
					MarketDataLive marketDataLive = getMarketDataTheoreticalValueService().getMarketDataOptionTheoreticalPrice(command);
					final MarketDataField marketDataField = getMarketDataFieldService().getMarketDataFieldByName(MarketDataField.FIELD_BLACK_SCHOLES_PRICE);

					MarketDataValue marketDataValue = new MarketDataValue();
					marketDataValue.setMeasureDate(DateUtils.addDays(now, -1));
					marketDataValue.setInvestmentSecurity(security);
					marketDataValue.setDataField(marketDataField);
					marketDataValue.setDataSource(getMarketDataSourceService().getMarketDataSourceByName(MarketDataSource.MARKET_DATA_SOURCE_NAME_PARAMETRIC_CLIFTON));
					marketDataValue.setMeasureValue(MathUtils.round(marketDataLive.asNumericValue(), marketDataField.getDecimalPrecision()));
					getMarketDataFieldService().saveMarketDataValue(marketDataValue);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error importing theoretical price data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error importing theoretical price data for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
	}


	private void applyMarketDataFieldMappingValuesToNewSecurity(InvestmentSecurity security) {
		MarketDataInvestmentSecurityRetrieverCommand securityRetrieverCommand = new MarketDataInvestmentSecurityRetrieverCommand();
		securityRetrieverCommand.setSymbol(security.getSymbol());
		securityRetrieverCommand.setSecurity(security);
		securityRetrieverCommand.setInstrument(security.getInstrument());
		securityRetrieverCommand.setHierarchy(security.getInstrument().getHierarchy());
		MarketDataInvestmentSecurityResult result = getMarketDataInvestmentSecurityService().getMarketDataInvestmentSecurity(securityRetrieverCommand);
		if (result.getNewSecurity() != null) {
			getInvestmentInstrumentService().saveInvestmentSecurity(result.getNewSecurity());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public MarketDataInvestmentSecurityService getMarketDataInvestmentSecurityService() {
		return this.marketDataInvestmentSecurityService;
	}


	public void setMarketDataInvestmentSecurityService(MarketDataInvestmentSecurityService marketDataInvestmentSecurityService) {
		this.marketDataInvestmentSecurityService = marketDataInvestmentSecurityService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataTheoreticalValueService getMarketDataTheoreticalValueService() {
		return this.marketDataTheoreticalValueService;
	}


	public void setMarketDataTheoreticalValueService(MarketDataTheoreticalValueService marketDataTheoreticalValueService) {
		this.marketDataTheoreticalValueService = marketDataTheoreticalValueService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
