package com.clifton.marketdata.instrument.allocation;


import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.db.ClassToSqlTypeConverter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.marketdata.MarketDataRetrieverImpl;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.instrument.allocation.calculators.MarketDataInvestmentSecurityAllocationCalculator;
import com.clifton.marketdata.instrument.allocation.calculators.MarketDataInvestmentSecurityAllocationCalculatorConfig;
import com.clifton.marketdata.instrument.allocation.calculators.MarketDataInvestmentSecurityAllocationCalculatorLocator;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MarketDataInvestmentSecurityAllocationServiceImpl implements MarketDataInvestmentSecurityAllocationService {

	private CalendarSetupService calendarSetupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;


	private MarketDataInvestmentSecurityAllocationCalculatorLocator marketDataInvestmentSecurityAllocationCalculatorLocator;

	private static final String DEFAULT_DATA_SOURCE_NAME = MarketDataRetrieverImpl.MARKET_DATASOURCE_BLOOMBERG;

	/////////////////////////////////////////////////////////////////////
	////////   MarketDataInvestmentSecurityAllocation Methods  //////////
	/////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationList(SecurityAllocationSearchForm searchForm) {
		List<MarketDataInvestmentSecurityAllocation> marketDataAllocationList;
		if (searchForm.getParentInvestmentSecurityId() != null && searchForm.getActiveOnDate() != null) {
			MarketDataInvestmentSecurityAllocationCalculatorConfig config = getMarketDataInvestmentSecurityAllocationCalculatorConfig(null, searchForm.getFxRateMarketDataSourceId(), searchForm.isDisableNoActiveAllocationsValidationError());
			InvestmentSecurity parentSecurity = getInvestmentInstrumentService().getInvestmentSecurity(searchForm.getParentInvestmentSecurityId());
			MarketDataInvestmentSecurityAllocationCalculator calculator = getMarketDataInvestmentSecurityAllocationCalculator(parentSecurity.getInstrument().getHierarchy().getSecurityAllocationType());
			marketDataAllocationList = calculator.getMarketDataInvestmentSecurityAllocationList(parentSecurity, searchForm.getActiveOnDate(), config);
		}
		else {
			// If not for a specific date, just return the wrapped list
			List<InvestmentSecurityAllocation> allocationList = getInvestmentSecurityAllocationHandler().getInvestmentSecurityAllocationList(searchForm);

			marketDataAllocationList = new ArrayList<>();
			for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(allocationList)) {
				marketDataAllocationList.add(new MarketDataInvestmentSecurityAllocation(allocation));
			}
		}

		// NOTE: if we need to support multiple data source conventions, refactor to be similar to InvestmentSecurityLocator
		if (searchForm.getSecurityConventionDataSource() != null && !CollectionUtils.isEmpty(marketDataAllocationList)) {
			MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSourceByName(searchForm.getSecurityConventionDataSource());
			for (MarketDataInvestmentSecurityAllocation allocation : marketDataAllocationList) {
				InvestmentSecurity security = allocation.getInvestmentSecurity();
				if (security != null) {
					// For example: "AAPL UW Equity"
					InvestmentInstrument instrument = security.getInstrument();
					StringBuilder symbol = new StringBuilder(32);
					symbol.append(security.getSymbol());
					if (instrument.getExchange() != null) {
						symbol.append(' ');
						symbol.append(instrument.getExchange().getExchangeCode());
					}
					MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(instrument, dataSource);
					if (mapping != null && mapping.getMarketSector() != null) {
						symbol.append(' ');
						symbol.append(mapping.getMarketSector().getName());
					}
					allocation.setDataSourceSecuritySymbol(symbol.toString());
				}
			}
		}

		return CollectionUtils.toPagingArrayList(marketDataAllocationList, searchForm.getStart(), searchForm.getLimit());
	}


	@Override
	public DataTable getMarketDataInvestmentSecurityAllocationWeightListForDateRange(int parentInvestmentSecurityId, Date startDate, Date endDate) {
		InvestmentSecurity parentSecurity = getInvestmentInstrumentService().getInvestmentSecurity(parentInvestmentSecurityId);
		ValidationUtils.assertNotNull(parentSecurity.getStartDate(), "Security [" + parentSecurity.getLabel()
				+ "] does not have a start date defined.  Please define the start date on the security before attempting to export allocation weights across a date range.");
		if (startDate == null || DateUtils.compare(parentSecurity.getStartDate(), startDate, false) > 0) {
			startDate = parentSecurity.getStartDate();
		}
		if (endDate == null || DateUtils.compare(endDate, new Date(), false) > 0) {
			endDate = DateUtils.clearTime(new Date());
		}
		// TODO - DO WE WANT TO RESTRICT HOW MANY DAYS CAN EXPORT AT A TIME?

		// Start on the First Weekday and iterate through weekdays
		Date date = DateUtils.getNextWeekday(DateUtils.addDays(startDate, -1));

		MarketDataInvestmentSecurityAllocationCalculatorConfig config = getMarketDataInvestmentSecurityAllocationCalculatorConfig(null, null);
		MarketDataInvestmentSecurityAllocationCalculator calculator = getMarketDataInvestmentSecurityAllocationCalculator(parentSecurity.getInstrument().getHierarchy().getSecurityAllocationType());

		List<DataColumn> dataColumnList = new ArrayList<>();
		Map<String, Map<String, BigDecimal>> allocationDateWeightMap = new HashMap<>();

		ClassToSqlTypeConverter typeConverter = new ClassToSqlTypeConverter();

		// Add First Column as Allocation Label
		dataColumnList.add(new DataColumnImpl("Allocation", typeConverter.convert(String.class)));
		while (DateUtils.isDateBetween(date, startDate, endDate, false)) {
			// Add a New Column For Each Date
			String dateString = DateUtils.fromDateShort(date);
			dataColumnList.add(new DataColumnImpl(dateString, typeConverter.convert(BigDecimal.class)));
			List<MarketDataInvestmentSecurityAllocation> marketDataAllocationList = calculator.getMarketDataInvestmentSecurityAllocationList(parentSecurity, date, config);
			for (MarketDataInvestmentSecurityAllocation allocation : CollectionUtils.getIterable(marketDataAllocationList)) {
				String allocationLabel = allocation.getAllocationLabel();
				Map<String, BigDecimal> allocationMap = allocationDateWeightMap.get(allocationLabel);
				if (allocationMap == null) {
					allocationMap = new HashMap<>();
				}
				allocationMap.put(dateString, allocation.getCurrentAllocationWeight());
				allocationDateWeightMap.put(allocationLabel, allocationMap);
			}

			// Move to next weekday
			date = DateUtils.getNextWeekday(date);
		}

		DataColumn[] dataColumnArray = dataColumnList.toArray(new DataColumn[dataColumnList.size()]);
		DataTable dt = new PagingDataTableImpl(dataColumnArray);

		// Now go through map and create rows for each allocation
		for (Map.Entry<String, Map<String, BigDecimal>> allocationLabelEntry : allocationDateWeightMap.entrySet()) {
			Map<String, BigDecimal> allocationMap = allocationLabelEntry.getValue();

			Object[] data = new Object[dataColumnList.size()];
			for (int i = 0; i < dataColumnArray.length; i++) {
				DataColumn dc = dataColumnArray[i];
				if ("Allocation".equals(dc.getColumnName())) {
					data[i] = allocationLabelEntry.getKey();
				}
				else {
					data[i] = allocationMap.get(dc.getColumnName());
				}
			}
			dt.addRow(new DataRowImpl(dt, data));
		}

		return dt;
	}

	///////////////////////////////////////////////////////////////////////////////
	////////      Market Data Allocated Security Price Calculations      //////////
	///////////////////////////////////////////////////////////////////////////////


	@Override
	public String previewMarketDataAllocatedValueForSecurity(int securityId, Date previewDate, short dataSourceId) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		InvestmentSecurityAllocationTypes type = security.getInstrument().getHierarchy().getSecurityAllocationType();
		ValidationUtils.assertNotNull(type, "Security [" + security.getLabel() + " does not have an allocation type set (on the hierarchy).");
		MarketDataInvestmentSecurityAllocationCalculator calculator = getMarketDataInvestmentSecurityAllocationCalculator(type);
		ValidationUtils.assertNotNull(calculator, "Security Allocation Type [" + type.name() + "] is missing a calculator.");
		MarketDataInvestmentSecurityAllocationCalculatorConfig config = getMarketDataInvestmentSecurityAllocationCalculatorConfig(dataSourceId, null);
		return calculator.previewAllocatedMarketDataValue(security, previewDate, config);
	}


	@Override
	public Status calculateInvestmentSecurityAllocationMarketDataValues(List<InvestmentSecurity> securityList, short dataSourceId, Date startDate, Date endDate, Status status) {
		Status result = (status != null) ? status : Status.ofMessage("Start processing");

		MarketDataInvestmentSecurityAllocationCalculatorConfig config = getMarketDataInvestmentSecurityAllocationCalculatorConfig(dataSourceId, null);
		EnumMap<InvestmentSecurityAllocationTypes, MarketDataInvestmentSecurityAllocationCalculator> calculatorMap = new EnumMap<>(InvestmentSecurityAllocationTypes.class);
		int count = 0, securityCount = 0, errorCount = 0;

		// Sort list in ascending order so the first call gets the most prices we'll need
		securityList = BeanUtils.sortWithFunction(securityList, InvestmentSecurity::getStartDate, true);

		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			// Only process if the security is active during start/end date range
			if (DateUtils.isOverlapInDates(startDate, endDate, security.getStartDate(), security.getEndDate())) {
				securityCount++;

				try {
					InvestmentSecurityAllocationTypes type = security.getInstrument().getHierarchy().getSecurityAllocationType();
					if (type != null && type.getCalculatedFieldType().isSystemCalculated()) {
						MarketDataInvestmentSecurityAllocationCalculator calculator = calculatorMap.computeIfAbsent(type, this::getMarketDataInvestmentSecurityAllocationCalculator);
						if (calculator != null) {
							count += calculator.calculateAllocatedMarketDataValues(security, startDate, endDate, config);
						}
					}
				}
				catch (Exception e) {
					errorCount++;
					String errorMessage = "Error calculating allocated security price for security [" + security.getLabel() + "]";
					result.addError(errorMessage + ": " + e.getMessage());
					LogUtils.errorOrInfo(getClass(), errorMessage, e);
				}
			}
		}
		result.setMessage("Updated [" + securityCount + "] securities, [" + count + "] market data value records." + ((errorCount > 0) ? " Errors [" + errorCount + "]" : ""));
		if (status == null && errorCount > 0) {
			throw new ValidationException(result.getErrorList().toString());
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                    Helper Methods                       //////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataInvestmentSecurityAllocationCalculator getMarketDataInvestmentSecurityAllocationCalculator(InvestmentSecurityAllocationTypes allocationType) {
		return getMarketDataInvestmentSecurityAllocationCalculatorLocator().locate(allocationType);
	}


	private MarketDataInvestmentSecurityAllocationCalculatorConfig getMarketDataInvestmentSecurityAllocationCalculatorConfig(Short dataSourceId, Short fxDataSourceId) {
		return getMarketDataInvestmentSecurityAllocationCalculatorConfig(dataSourceId, fxDataSourceId, false);
	}


	private MarketDataInvestmentSecurityAllocationCalculatorConfig getMarketDataInvestmentSecurityAllocationCalculatorConfig(Short dataSourceId, Short fxRateMarketDataSourceId, boolean disableNotActiveAllocationsValidationError) {
		Calendar defaultCalendar = getCalendarSetupService().getDefaultCalendar();

		MarketDataInvestmentSecurityAllocationCalculatorConfig config = new MarketDataInvestmentSecurityAllocationCalculatorConfig(defaultCalendar, (dataSourceId != null ? getMarketDataSourceService().getMarketDataSource(dataSourceId) : getMarketDataSourceService().getMarketDataSourceByName(DEFAULT_DATA_SOURCE_NAME)));
		config.setDisableNoActiveAllocationsValidationError(disableNotActiveAllocationsValidationError);

		if (fxRateMarketDataSourceId != null) {
			config.setFxRateMarketDataSource(getMarketDataSourceService().getMarketDataSource(fxRateMarketDataSourceId));
		}
		return config;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInvestmentSecurityAllocationCalculatorLocator getMarketDataInvestmentSecurityAllocationCalculatorLocator() {
		return this.marketDataInvestmentSecurityAllocationCalculatorLocator;
	}


	public void setMarketDataInvestmentSecurityAllocationCalculatorLocator(MarketDataInvestmentSecurityAllocationCalculatorLocator marketDataInvestmentSecurityAllocationCalculatorLocator) {
		this.marketDataInvestmentSecurityAllocationCalculatorLocator = marketDataInvestmentSecurityAllocationCalculatorLocator;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
