package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.allocation.dynamic.calculator.InvestmentSecurityAllocationDynamicCalculator;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.current.InvestmentCurrentSecurityCalculator;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>MarketDataFieldAllocatedValueCalculatorConfig</code> is a helper object for the
 * {@link com.clifton.marketdata.instrument.allocation.calculators.MarketDataInvestmentSecurityAllocationCalculator} that caches information for less db look ups
 * during pricing rebuilds.  Useful when rebuilding across securities which is most frequent case with batch job
 *
 * @author manderson
 */
public class MarketDataInvestmentSecurityAllocationCalculatorConfig {

	/**
	 * For monthly rebalancing, default business day of the month to rebalance
	 */
	private static final Integer DEFAULT_REBALANCE_BUSINESS_DAY = 1;

	////////////////////////////////////////////////////////////////////////////////

	private final Calendar defaultCalendar;
	private final MarketDataSource marketDataSource;
	private MarketDataSource fxRateMarketDataSource;  // the MarketDataSource for FxRate lookups.  This source may be null, in which case the default data source for rate lookups is used.

	final Map<Integer, Map<Date, BigDecimal>> securityDatePriceMap;
	final Map<String, Map<Date, BigDecimal>> fxRateMap;
	final Map<Integer, Short> instrumentCalendarMap; // Store at instrument level for securities that move from one security to another for an instrument the calendar is the same - comes from the instrument exchange
	final Map<Integer, Integer> securityMonthlyRebalanceBusinessDayMap; // Security custom field value used for Weighted Return Rebalanced Monthly

	/**
	 * The map key is InstrumentID_CurrentSecurityCalculatorBeanID, we don't use the allocation id, because if the allocations are dynamic it will be null
	 */
	final Map<String, Map<Date, InvestmentSecurity>> securityAllocationCurrentSecurityMap; // For those that use instrument and current security calculator - track current security on dates so when adjust daily we always know which security to compare to
	final Map<Integer, InvestmentCurrentSecurityCalculator> currentSecurityCalculatorBeanMap; // Store references to populated bean so don't have to keep looking it up and instantiating it
	final Map<Integer, List<InvestmentSecurityAllocation>> securityAllocationListMap; // Stores the list of ALL allocations for the security - which can easily be filtered for active on date when necessary

	private boolean disableNoActiveAllocationsValidationError;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInvestmentSecurityAllocationCalculatorConfig(Calendar defaultCalendar, MarketDataSource marketDataSource) {
		this.defaultCalendar = defaultCalendar;
		this.marketDataSource = marketDataSource;
		this.securityDatePriceMap = new HashMap<>();
		this.fxRateMap = new HashMap<>();
		this.instrumentCalendarMap = new HashMap<>();
		this.securityAllocationCurrentSecurityMap = new HashMap<>();
		this.currentSecurityCalculatorBeanMap = new HashMap<>();
		this.securityMonthlyRebalanceBusinessDayMap = new HashMap<>();
		this.securityAllocationListMap = new HashMap<>();
	}

	////////////////////////////////////////////////////////////////////////////


	public Short getCalendarForSecurity(InvestmentSecurity security, InvestmentCalculator investmentCalculator) {
		Short calendarId = this.instrumentCalendarMap.get(security.getInstrument().getId());
		if (calendarId == null) {
			calendarId = investmentCalculator.getInvestmentSecurityCalendar(security).getId();
			if (calendarId == null) {
				calendarId = this.defaultCalendar.getId();
			}
			this.instrumentCalendarMap.put(security.getInstrument().getId(), calendarId);
		}
		return calendarId;
	}

	////////////////////////////////////////////////////////////////////////////


	public Map<Date, BigDecimal> getPriceMapForSecurity(InvestmentSecurity security, Date startDate, InvestmentCalculator investmentCalculator, CalendarBusinessDayService calendarBusinessDayService, MarketDataFieldMappingRetriever marketDataFieldMappingRetriever, MarketDataFieldService marketDataFieldService) {
		if (this.securityDatePriceMap.containsKey(security.getId())) {
			return this.securityDatePriceMap.get(security.getId());
		}

		Date securityStartDate = security.getInstrument().getHierarchy().getPricingFrequency().getMinimumPriceDateForDate(startDate, getCalendarForSecurity(security, investmentCalculator), calendarBusinessDayService);

		Map<Date, BigDecimal> securityPriceMap = new HashMap<>();
		MarketDataPriceFieldMapping priceFieldMapping = marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(security.getInstrument());
		// One call to put all market data from start date to today in for the security
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm(true);
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setMinMeasureDate(securityStartDate);
		searchForm.setMaxMeasureDate(DateUtils.addDays(new Date(), 1));
		// Restrict to Closing price field data
		searchForm.setDataFieldId(MarketDataPriceFieldTypes.CLOSING.getPriceField(priceFieldMapping, marketDataFieldService).getId());
		// Optionally Closing Price can be required to come from a specific datasource
		searchForm.setDataSourceId(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(priceFieldMapping));
		List<MarketDataValue> valueList = marketDataFieldService.getMarketDataValueList(searchForm);
		for (MarketDataValue v : CollectionUtils.getIterable(valueList)) {
			securityPriceMap.put(v.getMeasureDate(), v.getMeasureValue());
		}
		this.securityDatePriceMap.put(security.getId(), securityPriceMap);
		return securityPriceMap;
	}


	public void addSecurityPriceToMap(InvestmentSecurity security, Date date, BigDecimal price) {
		Map<Date, BigDecimal> securityPriceMap = this.securityDatePriceMap.get(security.getId());
		if (securityPriceMap == null) {
			securityPriceMap = new HashMap<>();
		}
		securityPriceMap.put(date, price);
		this.securityDatePriceMap.put(security.getId(), securityPriceMap);
	}

	////////////////////////////////////////////////////////////////////////////


	public Map<Date, BigDecimal> getFxRateMap(InvestmentSecurity from, InvestmentSecurity to, Date startDate, MarketDataSourceService marketDataSourceService, MarketDataRatesService marketDataRatesService) {
		String key = from.getId() + "_" + to.getId();
		if (this.fxRateMap.containsKey(key)) {
			return this.fxRateMap.get(key);
		}

		Map<Date, BigDecimal> fromToFxRateMap = new HashMap<>();
		// Use configured fxRate MarketDataSource or the default if not configured.
		MarketDataSource dataSource = this.fxRateMarketDataSource != null ? this.fxRateMarketDataSource : marketDataSourceService.getMarketDataSourceByName(MarketDataRatesRetriever.MARKET_DATASOURCE_EXCHANGE_RATES);

		// One call to put all exchange rates from date to today
		ExchangeRateSearchForm searchForm = new ExchangeRateSearchForm();
		searchForm.setFromCurrencyId(from.getId());
		searchForm.setToCurrencyId(to.getId());
		searchForm.setMinRateDate(DateUtils.addDays(startDate, -1)); // Does a greater than search, so go back to start date - 1
		searchForm.setMaxRateDate(DateUtils.addDays(new Date(), 1));
		// Restrict to Default DataSource
		searchForm.setDataSourceId(dataSource.getId());

		List<MarketDataExchangeRate> rateList = marketDataRatesService.getMarketDataExchangeRateList(searchForm);
		for (MarketDataExchangeRate v : CollectionUtils.getIterable(rateList)) {
			fromToFxRateMap.put(v.getRateDate(), v.getExchangeRate());
		}
		this.fxRateMap.put(key, fromToFxRateMap);
		return fromToFxRateMap;
	}


	public Integer getMonthlyRebalanceBusinessDayForSecurity(InvestmentSecurity security, SystemColumnValueHandler systemColumnValueHandler) {
		if (this.securityMonthlyRebalanceBusinessDayMap.containsKey(security.getId())) {
			return this.securityMonthlyRebalanceBusinessDayMap.get(security.getId());
		}
		Integer monthlyRebalanceBusinessDay = (Integer) systemColumnValueHandler.getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_REBALANCE_BUSINESS_DAY, false);
		if (monthlyRebalanceBusinessDay == null) {
			monthlyRebalanceBusinessDay = DEFAULT_REBALANCE_BUSINESS_DAY;
		}
		this.securityMonthlyRebalanceBusinessDayMap.put(security.getId(), monthlyRebalanceBusinessDay);
		return monthlyRebalanceBusinessDay;
	}

	////////////////////////////////////////////////////////////////////////////


	public List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationListActiveOnDate(InvestmentSecurity security, Date activeOnDate, InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		List<InvestmentSecurityAllocation> allocationList = getInvestmentSecurityAllocationList(security, investmentSecurityAllocationHandler);

		List<InvestmentSecurityAllocation> filteredList = new ArrayList<>();
		for (InvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
			if (alloc.isActiveOnDate(activeOnDate)) {
				filteredList.add(alloc);
			}
		}
		// If it's dynamic - calculate allocations and weights
		if (security.getInstrument().getHierarchy().isAllocationOfSecuritiesDynamic()) {
			filteredList = investmentSecurityAllocationHandler.getInvestmentSecurityAllocationDynamicList(security, filteredList, activeOnDate);
			// Verify no dynamic calculations found an error:
			List<InvestmentSecurityAllocation> errorList = BeanUtils.filter(filteredList, securityAllocation -> securityAllocation.getNote() != null && securityAllocation.getNote().startsWith(InvestmentSecurityAllocationDynamicCalculator.DYNAMIC_NOTE_ERROR_PREFIX), true);
			if (!CollectionUtils.isEmpty(errorList)) {
				throw new ValidationException("Security [" + security.getLabel() + "] found errors dynamically generating allocation weights: " + StringUtils.collectionToCommaDelimitedString(errorList,
						(InvestmentSecurityAllocation from) -> from.getInvestmentSecurity().getSymbol() + " - " + from.getNote())
						+ "].  See Preview Weights tab for additional information. Unable to price this security.");
			}
		}

		if (CollectionUtils.isEmpty(filteredList) && !isDisableNoActiveAllocationsValidationError()) {
			throw new ValidationException("Security [" + security.getLabel() + "] does not have any active allocations on [" + DateUtils.fromDateShort(activeOnDate)
					+ "].  Unable to price this security.");
		}
		return filteredList;
	}


	private List<InvestmentSecurityAllocation> getInvestmentSecurityAllocationList(InvestmentSecurity security, InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		if (this.securityAllocationListMap.containsKey(security.getId())) {
			return this.securityAllocationListMap.get(security.getId());
		}

		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(security.getId());
		List<InvestmentSecurityAllocation> allocationList = investmentSecurityAllocationHandler.getInvestmentSecurityAllocationList(searchForm);
		this.securityAllocationListMap.put(security.getId(), allocationList);
		return allocationList;
	}

	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getCurrentSecurityForSecurityAllocation(InvestmentSecurityAllocation securityAllocation, Date date, SystemBeanService systemBeanService) {
		if (securityAllocation.getInvestmentSecurity() != null) {
			return securityAllocation.getInvestmentSecurity();
		}

		Map<Date, InvestmentSecurity> currentMap = this.securityAllocationCurrentSecurityMap.get(getCurrentSecurityKeyForAllocation(securityAllocation));
		if (currentMap != null) {
			if (currentMap.containsKey(date)) {
				return currentMap.get(date);
			}
		}
		else {
			currentMap = new HashMap<>();
		}
		InvestmentCurrentSecurityCalculator currentCalc = getCurrentSecurityCalculator(securityAllocation.getCurrentSecurityCalculatorBean(), systemBeanService);
		InvestmentSecurity currentSecurity = currentCalc.calculateCurrentSecurity(securityAllocation, date);
		if (currentSecurity == null) {
			throw new ValidationException("Unable to calculate current security for instrument [" + securityAllocation.getInvestmentInstrument().getLabel() + "] on ["
					+ DateUtils.fromDateShort(date) + "] using calculator [" + securityAllocation.getCurrentSecurityCalculatorBean().getName());
		}
		currentMap.put(date, currentSecurity);
		this.securityAllocationCurrentSecurityMap.put(getCurrentSecurityKeyForAllocation(securityAllocation), currentMap);
		return currentSecurity;
	}


	private String getCurrentSecurityKeyForAllocation(InvestmentSecurityAllocation securityAllocation) {
		return securityAllocation.getInvestmentInstrument().getId() + "_" + securityAllocation.getCurrentSecurityCalculatorBean().getId();
	}


	private InvestmentCurrentSecurityCalculator getCurrentSecurityCalculator(SystemBean calculatorBean, SystemBeanService systemBeanService) {
		return this.currentSecurityCalculatorBeanMap.computeIfAbsent(calculatorBean.getId(), k -> (InvestmentCurrentSecurityCalculator) systemBeanService.getBeanInstance(calculatorBean));
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public MarketDataSource getFxRateMarketDataSource() {
		return this.fxRateMarketDataSource;
	}


	public void setFxRateMarketDataSource(MarketDataSource fxRateMarketDataSource) {
		this.fxRateMarketDataSource = fxRateMarketDataSource;
	}


	public boolean isDisableNoActiveAllocationsValidationError() {
		return this.disableNoActiveAllocationsValidationError;
	}


	public void setDisableNoActiveAllocationsValidationError(boolean disableNoActiveAllocationsValidationError) {
		this.disableNoActiveAllocationsValidationError = disableNoActiveAllocationsValidationError;
	}
}
