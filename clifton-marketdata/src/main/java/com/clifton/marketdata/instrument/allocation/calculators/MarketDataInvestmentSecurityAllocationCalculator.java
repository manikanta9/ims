package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.calculator.InvestmentSecurityAllocationCalculator;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataInvestmentSecurityAllocationCalculator</code> interface extends the
 * {@link InvestmentSecurityAllocationCalculator} and includes a method for calculating
 * the price of an allocated security
 *
 * @author manderson
 */
public interface MarketDataInvestmentSecurityAllocationCalculator extends InvestmentSecurityAllocationCalculator {

	public int calculateAllocatedMarketDataValues(InvestmentSecurity security, Date startDate, Date endDate, MarketDataInvestmentSecurityAllocationCalculatorConfig config);


	/**
	 * Returns string representation of the calculated value for the given date and the details of the calculation including price dates, etc.
	 */
	public String previewAllocatedMarketDataValue(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config);


	/**
	 * Returns populated MarketDataInvestmentSecurityAllocation list for active InvestmentSecurityAllocationList
	 * with relevant market data fields populated based on the given date
	 */
	public List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationList(InvestmentSecurity security, Date date,
	                                                                                                  MarketDataInvestmentSecurityAllocationCalculatorConfig config);
}
