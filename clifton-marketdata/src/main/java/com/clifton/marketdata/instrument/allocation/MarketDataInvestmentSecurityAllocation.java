package com.clifton.marketdata.instrument.allocation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>MarketDataInvestmentSecurityAllocation</code> is a Virtual object wrapper for the
 * {@link InvestmentSecurityAllocation} that populates additional fields such as market data
 * based on a specified date
 * <p>
 * This is used for Share Price allocations that display "weight" as the base weight
 * and current weight is calculated based on the shares and price
 *
 * @author manderson
 */
@NonPersistentObject
public class MarketDataInvestmentSecurityAllocation extends InvestmentSecurityAllocation {

	private InvestmentSecurity currentSecurity;
	private BigDecimal currentPrice;

	private BigDecimal currentAllocationWeight;

	private BigDecimal currentValueOfOneShare;

	/**
	 * Optionally converts security symbol using specified Data Source convention.
	 * For example: "AAPL UW Equity".
	 */
	private String dataSourceSecuritySymbol;

	/**
	 * The FX Rate between the currentSecurity's trading currency and that of the parent investment security to which this allocation is assigned.
	 */
	private BigDecimal currencyExchangeRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInvestmentSecurityAllocation() {
		// NOTHING
	}


	public MarketDataInvestmentSecurityAllocation(InvestmentSecurityAllocation allocation) {
		BeanUtils.copyProperties(allocation, this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCurrentSecurityLabel() {
		if (getDataSourceSecuritySymbol() != null) {
			return getDataSourceSecuritySymbol();
		}
		if (getCurrentSecurity() != null) {
			return this.getCurrentSecurity().getLabel();
		}
		return null;
	}


	public BigDecimal getCurrentAllocationValue() {
		return MathUtils.multiply(getCurrentValueOfOneShare(), getAllocationShares());
	}


	public BigDecimal getWeightDifference() {
		if (getCurrentAllocationWeight() != null && getAllocationWeight() != null) {
			return MathUtils.subtract(getCurrentAllocationWeight(), getAllocationWeight());
		}
		return null;
	}


	@Override
	public String toString() {
		return getAllocationLabel() + ": " //
				+ "Shares [" + CoreMathUtils.formatNumberDecimal(getAllocationShares()) + "], " //
				+ " Base Weight [" + CoreMathUtils.formatNumberMoney(getAllocationWeight()) + "], " //
				+ " Current Weight [" + CoreMathUtils.formatNumberMoney(getCurrentAllocationWeight()) + "], " //
				+ " Current Price [" + CoreMathUtils.formatNumberDecimal(getCurrentPrice()) + "]";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getCurrentPrice() {
		return this.currentPrice;
	}


	public void setCurrentPrice(BigDecimal currentPrice) {
		this.currentPrice = currentPrice;
	}


	public BigDecimal getCurrentAllocationWeight() {
		return this.currentAllocationWeight;
	}


	public void setCurrentAllocationWeight(BigDecimal currentAllocationWeight) {
		this.currentAllocationWeight = currentAllocationWeight;
	}


	public InvestmentSecurity getCurrentSecurity() {
		return this.currentSecurity;
	}


	public void setCurrentSecurity(InvestmentSecurity currentSecurity) {
		this.currentSecurity = currentSecurity;
	}


	public BigDecimal getCurrentValueOfOneShare() {
		return this.currentValueOfOneShare;
	}


	public void setCurrentValueOfOneShare(BigDecimal currentValueOfOneShare) {
		this.currentValueOfOneShare = currentValueOfOneShare;
	}


	public String getDataSourceSecuritySymbol() {
		return this.dataSourceSecuritySymbol;
	}


	public void setDataSourceSecuritySymbol(String dataSourceSecuritySymbol) {
		this.dataSourceSecuritySymbol = dataSourceSecuritySymbol;
	}


	public BigDecimal getCurrencyExchangeRate() {
		return this.currencyExchangeRate;
	}


	public void setCurrencyExchangeRate(BigDecimal currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}
}
