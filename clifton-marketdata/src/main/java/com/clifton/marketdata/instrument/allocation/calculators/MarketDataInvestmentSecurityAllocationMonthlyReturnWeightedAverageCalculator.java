package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataInvestmentSecurityAllocationMonthlyReturnWeightedAverageCalculator</code> ...
 *
 * @author manderson
 */
public class MarketDataInvestmentSecurityAllocationMonthlyReturnWeightedAverageCalculator extends BaseMarketDataInvestmentSecurityAllocationCalculator {

	private SystemColumnValueService systemColumnValueService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateInvestmentSecurityAllocation(InvestmentSecurityAllocation bean) {
		ValidationUtils.assertNotNull(bean.getAllocationWeight(), getInvestmentSecurityAllocationType().getWeightLabel() + " is required for each allocation.", "allocationWeight");
	}


	@Override
	public BigDecimal calculateAllocatedSecurityValueImpl(InvestmentSecurity security, List<MarketDataInvestmentSecurityAllocation> allocationList, Date date,
	                                                      MarketDataInvestmentSecurityAllocationCalculatorConfig config, StringBuilder previewResult, int count) {
		BigDecimal value = BigDecimal.ZERO;
		Date startDate = DateUtils.addDays(DateUtils.getFirstDayOfMonth(date), -1);
		for (MarketDataInvestmentSecurityAllocation alloc : CollectionUtils.getIterable(allocationList)) {
			InvestmentSecurity endAllocationCurrentSecurity = alloc.getCurrentSecurity();
			InvestmentSecurity startAllocationCurrentSecurity = config.getCurrentSecurityForSecurityAllocation(alloc, startDate, getSystemBeanService());
			BigDecimal monthlyReturn;

			if (previewResult != null) {
				previewResult.append(StringUtils.NEW_LINE)
						.append(endAllocationCurrentSecurity.getSymbol());
			}

			if (startAllocationCurrentSecurity.equals(endAllocationCurrentSecurity)) {
				monthlyReturn = getMonthlyReturnForSecurity(endAllocationCurrentSecurity, date);
				appendStringAndValueForPreview(previewResult, " Monthly Return: ", monthlyReturn);
			}
			else {
				// Custom Return Calculation
				BigDecimal startValue = getMarketDataPriceForSecurity(startAllocationCurrentSecurity, alloc, startDate, startDate, config, true, previewResult);
				BigDecimal endValue = getMarketDataPriceForSecurity(endAllocationCurrentSecurity, alloc, date, date, config, true, previewResult);

				appendStringAndValueForPreview(previewResult, " Start Value: ", startValue);

				if (startAllocationCurrentSecurity.getInstrument().equals(endAllocationCurrentSecurity.getInstrument())) {
					BigDecimal priceAdjustment = getVolumeWeightedAveragePriceRollAdjustmentForInstrument(endAllocationCurrentSecurity.getInstrument(), startDate, date);
					endValue = MathUtils.subtract(endValue, priceAdjustment);

					appendStringAndValueForPreview(previewResult, " Price Adjustment Value: ", priceAdjustment);
				}
				monthlyReturn = MathUtils.getPercentChange(startValue, endValue, true);

				appendStringAndValueForPreview(previewResult, " End Value: ", endValue);
				appendStringAndValueForPreview(previewResult, " Monthly Return Value: ", monthlyReturn);
			}
			BigDecimal returnWeight = alloc.getAllocationWeight();
			value = MathUtils.add(value, MathUtils.getPercentageOf(returnWeight, monthlyReturn, true));

			appendStringAndValueForPreview(previewResult, " Return Weight Value: ", returnWeight);
			appendStringAndValueForPreview(previewResult, " Allocation Value: ", MathUtils.getPercentageOf(returnWeight, monthlyReturn, true));
		}
		return value;
	}


	private void appendStringAndValueForPreview(StringBuilder previewResult, String description, BigDecimal value) {
		if (previewResult != null) {
			previewResult.append(description)
					// For Readability - Use 4 decimal places since most of these are % returns
					.append(CoreMathUtils.formatNumber(value, "#,###.#####"));
		}
	}


	private BigDecimal getMonthlyReturnForSecurity(InvestmentSecurity security, Date date) {
		// Look up MTD - Since in order for it to apply to that month, need to set start/end dates for search
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setMinMeasureDate(DateUtils.getFirstDayOfMonth(date));
		searchForm.setMaxMeasureDate(DateUtils.addDays(date, 1)); // Does a less than search so move to next day
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setDataFieldId(getMarketDataFieldService().getMarketDataFieldByName(DEFAULT_MONTHLY_RETURN_DATA_FIELD_NAME).getId());
		searchForm.setOrderBy("measureDate:desc");
		// Get the most recent one to the summary measure date
		MarketDataValue mr = CollectionUtils.getFirstElement(getMarketDataFieldService().getMarketDataValueList(searchForm));
		// If it exists, use it
		if (mr != null) {
			return mr.getMeasureValue();
		}
		// otherwise try to calculate it based on prices - using benchmark start/end dates if different
		try {
			return getMarketDataRetriever().getInvestmentSecurityReturnFlexible(null, security, DateUtils.addDays(searchForm.getMinMeasureDate(), -1), date, "", true);
		}
		catch (ValidationException e) {
			throw new ValidationException("Security Allocation [" + security.getLabel() + "] missing monthly return value during date range [" + DateUtils.fromDateRange(searchForm.getMinMeasureDate(), date, true) + "] and unable to calculate monthly return because: " + e.getMessage(),
					e);
		}
	}


	private BigDecimal getVolumeWeightedAveragePriceRollAdjustmentForInstrument(InvestmentInstrument instrument, Date startDate, Date endDate) {
		SystemColumnDatedValueSearchForm searchForm = new SystemColumnDatedValueSearchForm();
		searchForm.setColumnGroupName(InvestmentInstrument.INSTRUMENT_ROLL_DATED_CUSTOM_FIELDS_GROUP_NAME);
		searchForm.setColumnName(InvestmentInstrument.INSTRUMENT_ROLL_VOLUME_WEIGHTED_AVERAGE_PRICE_CUSTOM_FIELD);
		searchForm.setFkFieldId(instrument.getId());
		// Will use the last one entered relative to the end date but on or after start date
		searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
		searchForm.addSearchRestriction(new SearchRestriction("endDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
		searchForm.setOrderBy("endDate:desc");
		searchForm.setLimit(1);

		SystemColumnDatedValue value = CollectionUtils.getFirstElement(getSystemColumnValueService().getSystemColumnDatedValueList(searchForm));
		if (value != null) {
			return (BigDecimal) getSystemColumnValueService().getSystemColumnDatedValueAsObject(value);
		}
		return BigDecimal.ZERO;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}
}
