package com.clifton.marketdata.instrument.event;

import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventAction;
import com.clifton.investment.instrument.event.action.InvestmentSecurityEventActionType;
import com.clifton.investment.instrument.event.action.processor.BaseInvestmentSecurityEventActionProcessor;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.system.audit.auditor.observer.SystemDaoAuditUpdateObserver;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The MarketDataValueAdjustorSecurityEventActionProcessor class updates market data values for the fields that support
 * adjustments prior to event's Event Date.  For example, for a 2 for 1 stock split (get 2 shares for 1 owned),
 * adjustment factor for prices before Event Date needs to be divided by 2 and for shares outstanding multiplied by 2.
 *
 * @author vgomelsky
 */
public class MarketDataValueAdjustorSecurityEventActionProcessor extends BaseInvestmentSecurityEventActionProcessor {

	private MarketDataFieldService marketDataFieldService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Specifies whether Ex Date as opposed to Event Date should be used to lookup affected values
	 */
	private boolean exDateUsed;

	/**
	 * Defines the formula to use to calculate adjustment value. Allowed values:
	 * "AFTER_DIVIDE_BY_BEFORE" for stock splits,  "ONE_PLUS_AFTER" for stock dividends, "ONE_DIVIDE_BY_ADDITIONAL" for stock spinoff
	 */
	private String adjustmentAction;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean processActionImpl(InvestmentSecurityEvent securityEvent, InvestmentSecurityEventActionType actionType) {
		// get market data values for security that support adjustments
		List<MarketDataValue> valueList = getAffectedValueList(securityEvent);
		if (!CollectionUtils.isEmpty(valueList)) {
			// update adjustment factor based on adjustment type
			BigDecimal adjustmentValue = getAdjustmentValue(securityEvent);
			for (MarketDataValue value : valueList) {
				value.setMeasureValueAdjustmentFactor(value.getDataField().getAdjustmentType().operate(value.getMeasureValueAdjustmentFactor(), adjustmentValue));
				// save with audit trail disabled
				DaoUtils.executeWithSpecificObserversDisabled(() -> getMarketDataFieldService().updateMarketDataValueWithoutValidation(value), SystemDaoAuditUpdateObserver.class);
			}
			return true;
		}

		return false;
	}


	@Override
	protected boolean rollbackActionImpl(InvestmentSecurityEventAction eventAction) {
		List<MarketDataValue> valueList = getAffectedValueList(eventAction.getSecurityEvent());

		// reverse adjustment factor based on adjustment type
		InvestmentSecurityEvent securityEvent = eventAction.getSecurityEvent();
		BigDecimal undoAdjustmentValue = MathUtils.divide(BigDecimal.ONE, getAdjustmentValue(securityEvent));
		for (MarketDataValue value : valueList) {
			value.setMeasureValueAdjustmentFactor(value.getDataField().getAdjustmentType().operate(value.getMeasureValueAdjustmentFactor(), undoAdjustmentValue));
			// save with audit trail disabled
			DaoUtils.executeWithSpecificObserversDisabled(() -> getMarketDataFieldService().updateMarketDataValueWithoutValidation(value), SystemDaoAuditUpdateObserver.class);
		}

		return true;
	}


	@Override
	public boolean isDataAvailableForProcessing(InvestmentSecurityEvent securityEvent) {
		return !CollectionUtils.isEmpty(getAffectedValueList(securityEvent));
	}


	/**
	 * Returns the adjustment value based on the configured adjustment action that defines the corresponding formula.
	 */
	protected BigDecimal getAdjustmentValue(InvestmentSecurityEvent securityEvent) {
		switch (getAdjustmentAction()) {
			case "AFTER_DIVIDE_BY_BEFORE":
				// stock split
				return MathUtils.divide(securityEvent.getAfterEventValue(), securityEvent.getBeforeEventValue());
			case "ONE_PLUS_AFTER":
				// stock dividend
				return MathUtils.add(BigDecimal.ONE, securityEvent.getAfterEventValue());
			case "ONE_DIVIDE_BY_ADDITIONAL":
				// stock spinoff
				return MathUtils.divide(BigDecimal.ONE, securityEvent.getAdditionalEventValue());
		}
		throw new IllegalArgumentException("Unsupported adjustment action: " + this.adjustmentAction);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Return the List of {@link MarketDataValue} objects for the specified security prior to Event Date
	 * that need to be adjusted.
	 **/
	private List<MarketDataValue> getAffectedValueList(InvestmentSecurityEvent securityEvent) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(securityEvent.getSecurity().getId());
		searchForm.setMaxMeasureDate(isExDateUsed() ? securityEvent.getExDate() : securityEvent.getEventDate());
		searchForm.setAdjustmentTypeDefined(true);
		return getMarketDataFieldService().getMarketDataValueList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public boolean isExDateUsed() {
		return this.exDateUsed;
	}


	public void setExDateUsed(boolean exDateUsed) {
		this.exDateUsed = exDateUsed;
	}


	public String getAdjustmentAction() {
		return this.adjustmentAction;
	}


	public void setAdjustmentAction(String adjustmentAction) {
		this.adjustmentAction = adjustmentAction;
	}
}
