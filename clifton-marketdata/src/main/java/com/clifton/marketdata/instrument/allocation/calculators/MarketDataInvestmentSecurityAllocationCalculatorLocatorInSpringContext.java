package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.core.util.AssertUtils;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class MarketDataInvestmentSecurityAllocationCalculatorLocatorInSpringContext implements MarketDataInvestmentSecurityAllocationCalculatorLocator, InitializingBean, ApplicationContextAware {

	private final Map<InvestmentSecurityAllocationTypes, MarketDataInvestmentSecurityAllocationCalculator> calculatorMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		Map<String, MarketDataInvestmentSecurityAllocationCalculator> beanMap = getApplicationContext().getBeansOfType(MarketDataInvestmentSecurityAllocationCalculator.class);

		// need a map with InvestmentSecurityAllocationTypes as keys instead of bean names
		for (Map.Entry<String, MarketDataInvestmentSecurityAllocationCalculator> stringMarketDataInvestmentSecurityAllocationCalculatorEntry : beanMap.entrySet()) {
			MarketDataInvestmentSecurityAllocationCalculator calculator = stringMarketDataInvestmentSecurityAllocationCalculatorEntry.getValue();
			if (getCalculatorMap().containsKey(calculator.getInvestmentSecurityAllocationType())) {
				throw new RuntimeException("Cannot register '" + stringMarketDataInvestmentSecurityAllocationCalculatorEntry.getKey() + "' as a calculator for investment security allocation type '" + calculator.getInvestmentSecurityAllocationType()
						+ "' because this allocation type already has a registered calculator.");
			}
			getCalculatorMap().put(calculator.getInvestmentSecurityAllocationType(), calculator);
		}
	}


	@Override
	public MarketDataInvestmentSecurityAllocationCalculator locate(InvestmentSecurityAllocationTypes allocationType) {
		AssertUtils.assertNotNull(allocationType, "Required allocation type cannot be null.");
		MarketDataInvestmentSecurityAllocationCalculator result = getCalculatorMap().get(allocationType);
		AssertUtils.assertNotNull(result, "Cannot locate MarketDataInvestmentSecurityAllocationCalculator for '%1s' allocation type.", allocationType);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Map<InvestmentSecurityAllocationTypes, MarketDataInvestmentSecurityAllocationCalculator> getCalculatorMap() {
		return this.calculatorMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
