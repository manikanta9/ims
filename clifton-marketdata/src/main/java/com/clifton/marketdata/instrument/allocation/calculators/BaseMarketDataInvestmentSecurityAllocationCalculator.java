package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationCalculatedFieldTypes;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocation;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseMarketDataInvestmentSecurityAllocationCalculator</code> ...
 *
 * @author manderson
 */
public abstract class BaseMarketDataInvestmentSecurityAllocationCalculator implements MarketDataInvestmentSecurityAllocationCalculator {

	public static final String DEFAULT_MONTHLY_RETURN_DATA_FIELD_NAME = MarketDataField.FIELD_MONTHLY_RETURN;


	////////////////////////////////////////////////////////////////////////////////

	private InvestmentSecurityAllocationTypes investmentSecurityAllocationType;

	////////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;


	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataRetriever marketDataRetriever;
	private MarketDataRatesService marketDataRatesService;

	private SystemBeanService systemBeanService;
	private SystemColumnValueHandler systemColumnValueHandler;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationList(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		return getMarketDataInvestmentSecurityAllocationListFromList(config.getInvestmentSecurityAllocationListActiveOnDate(security, date, getInvestmentSecurityAllocationHandler()), date, config);
	}


	protected List<MarketDataInvestmentSecurityAllocation> getMarketDataInvestmentSecurityAllocationListFromList(List<InvestmentSecurityAllocation> allocationList, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		List<MarketDataInvestmentSecurityAllocation> marketDataAllocationList = new ArrayList<>();
		for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(allocationList)) {
			MarketDataInvestmentSecurityAllocation mda = new MarketDataInvestmentSecurityAllocation(allocation);
			populateMarketDataInvestmentSecurityAllocation(date, mda, config);
			marketDataAllocationList.add(mda);
		}
		return marketDataAllocationList;
	}


	protected void populateMarketDataInvestmentSecurityAllocation(Date date, MarketDataInvestmentSecurityAllocation allocation, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		InvestmentSecurity security = config.getCurrentSecurityForSecurityAllocation(allocation, date, getSystemBeanService());
		allocation.setCurrentSecurity(security);
		// Since used for display only - don't require price on a specific date, just the most recent
		allocation.setCurrentPrice(getMarketDataPriceForSecurity(security, allocation, allocation.getParentInvestmentSecurity().getStartDate(), date, config, false, null));
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String previewAllocatedMarketDataValue(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		StringBuilder previewResult = new StringBuilder(50);
		BigDecimal result = calculateAllocatedSecurityValue(security, date, config, previewResult, 0);
		return "Calculation of [" + security.getSymbol() + "] on [" + DateUtils.fromDateShort(date) + "]: " + StringUtils.NEW_LINE + previewResult.toString() + StringUtils.NEW_LINE + "Result: " + CoreMathUtils.formatNumberDecimal(result);
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final int calculateAllocatedMarketDataValues(InvestmentSecurity security, Date startDate, Date endDate, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		MarketDataField field = null;
		if (InvestmentSecurityAllocationCalculatedFieldTypes.PRICE == getInvestmentSecurityAllocationType().getCalculatedFieldType()) {
			field = MarketDataPriceFieldTypes.LATEST.getPriceField(getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(security.getInstrument()), getMarketDataFieldService());
			// Latest Price Field will never be null - always returns the default if not mapped
		}
		else if (InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN == getInvestmentSecurityAllocationType().getCalculatedFieldType()) {
			field = getMarketDataFieldService().getMarketDataFieldByName(DEFAULT_MONTHLY_RETURN_DATA_FIELD_NAME);
		}
		ValidationUtils.assertNotNull(field, "Unknown Market Data Field to use for allocated security calculated field type " + getInvestmentSecurityAllocationType().getCalculatedFieldType().name());

		Date date = startDate;
		if (security.getStartDate() != null && security.getStartDate().after(date)) {
			date = security.getStartDate();
		}

		// Check if the security has custom field for Allocation Calculation Start Date entered - will override start date for when the system should perform any calculations
		Date overrideStartDate = (Date) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_ALLOCATED_SECURITY_CALCULATION_START_DATE, false);
		if (overrideStartDate != null) {
			ValidationUtils.assertTrue(DateUtils.compare(overrideStartDate, security.getStartDate(), false) > 0, "Security Allocation Calculation Start Date, when used, must come after Security start date.  If the same as the start date, then leave blank.");
			if (overrideStartDate.after(date)) {
				date = overrideStartDate;
			}
		}

		if (security.getEndDate() != null && security.getEndDate().before(endDate)) {
			endDate = security.getEndDate();
		}

		if (DateUtils.isDateAfter(startDate, endDate)) {
			throw new ValidationException("Nothing to calculate as start date [" + DateUtils.fromDateShort(startDate) + "] is after end date [" + DateUtils.fromDateShort(endDate));
		}
		// Get next day from previous to ensure we aren't starting on a non-business day
		date = getNextDay(security, getPreviousDay(security, date, config), config);
		// If start date was security start date, but that date is not a business day - throw an exception so users can fix start date to a business day
		// Only if required in order to properly calculate
		if (getInvestmentSecurityAllocationType().isStartDateBusinessDayRequired() && DateUtils.compare(date, startDate, false) != 0 && DateUtils.compare(startDate, security.getStartDate(), false) == 0) {
			throw new ValidationException("Unable to Calculate Security [" + security.getLabel() + "]: Start Date of [" + DateUtils.fromDateShort(security.getStartDate()) + "] is not a business day.  Please move start date to a business day.");
		}
		int count = 0;
		while (DateUtils.compare(date, endDate, false) <= 0) {
			try {
				BigDecimal value = calculateAllocatedSecurityValue(security, date, config, null, 0);
				if (value != null) {
					// Save Value
					saveMarketDataValue(security, value, date, field, config.getMarketDataSource());
					count++;
					date = getNextDay(security, date, config);
				}
				else {
					// If a value can't be calculated stop there and don't calculate any future data
					break;
				}
			}

			catch (Exception e) {
				if (!CoreExceptionUtils.isValidationException(e)) {
					LogUtils.error(getClass(), "Unable to Calculate Security [" + security.getLabel() + "] on " + DateUtils.fromDateShort(date) + ": " + e.getMessage(), e);
				}
				throw new ValidationException("Unable to Calculate Security [" + security.getLabel() + "] on " + DateUtils.fromDateShort(date) + ": " + e.getMessage(), e);
			}
		}
		return count;
	}


	protected BigDecimal calculateAllocatedSecurityValue(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config, StringBuilder previewResult, int count) {
		count++;
		if (count > 100) {
			throw new ValidationException("Potential Error – too many missing values prior to current calculation dates. Please manually rebuild the security with start date of inception or last value date");
		}
		List<MarketDataInvestmentSecurityAllocation> activeMarketDataAllocationList = getMarketDataInvestmentSecurityAllocationList(security, date, config);
		BigDecimal value = calculateAllocatedSecurityValueImpl(security, activeMarketDataAllocationList, date, config, previewResult, count);
		if (value != null) {
			// Put it in the map for later use without recalculating
			config.addSecurityPriceToMap(security, date, value);
		}
		return value;
	}


	public abstract BigDecimal calculateAllocatedSecurityValueImpl(InvestmentSecurity security, List<MarketDataInvestmentSecurityAllocation> allocationList, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config, StringBuilder previewResult, int count);


	////////////////////////////////////////////////////////////////////////////
	/////////                     Helper Methods                      //////////
	////////////////////////////////////////////////////////////////////////////


	private void saveMarketDataValue(InvestmentSecurity security, BigDecimal value, Date date, MarketDataField marketDataField, MarketDataSource marketDataSource) {
		MarketDataValue mdv = new MarketDataValue();
		mdv.setInvestmentSecurity(security);
		mdv.setMeasureDate(date);
		mdv.setDataField(marketDataField);
		mdv.setMeasureTime(mdv.getDataField().isTimeSensitive() ? new Time(0) : null);
		mdv.setDataSource(marketDataSource);

		// Max Decimal Places
		mdv.setMeasureValue(MathUtils.round(value, mdv.getDataField().getDecimalPrecision()));
		mdv.setMeasureValueAdjustmentFactor(BigDecimal.ONE);
		getMarketDataFieldService().saveMarketDataValueWithOptions(mdv, true);
	}


	protected BigDecimal getMarketDataPriceForSecurity(InvestmentSecurity security, InvestmentSecurityAllocation allocation, Date startDate, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config,
	                                                   boolean throwExceptionIfMissing, StringBuilder previewResult) {
		// Currency - Use change in Exchange Rate instead of prices
		// Since we aren't tied to any specific client, will use default Exchange Rate Data Source ("Goldman Sachs") and Rates to USD
		if (security.isCurrency()) {
			BigDecimal price = getMarketDataExchangeRate(security, null, startDate, date, config, throwExceptionIfMissing);
			appendPriceForDayToPreview(previewResult, security, price, date);
			return price;
		}

		Map<Date, BigDecimal> securityPriceMap = config.getPriceMapForSecurity(security, getPreviousDay(security, startDate, config), getInvestmentCalculator(), getCalendarBusinessDayService(), getMarketDataFieldMappingRetriever(), getMarketDataFieldService());
		if (securityPriceMap.get(date) != null) {
			BigDecimal price = securityPriceMap.get(date);
			appendPriceForDayToPreview(previewResult, security, price, date);
			return price;
		}

		// If price does not exist on that date, get Minimum Allowed Date
		Date minimumRequiredPriceDate = security.getInstrument().getHierarchy().getPricingFrequency().getMinimumPriceDateForDate(date, config.getCalendarForSecurity((allocation != null ? allocation.getParentInvestmentSecurity() : security), getInvestmentCalculator()), getCalendarBusinessDayService());
		Date maximumRequiredPriceDate = date;
		// If minimum required is after the date we are looking for (i.e. returns month end if last business day of the month) - set maximum to the minimum date
		if (DateUtils.compare(maximumRequiredPriceDate, minimumRequiredPriceDate, false) < 0) {
			maximumRequiredPriceDate = minimumRequiredPriceDate;
		}
		// Move minimum back to previous business day (For Monthly will return last day of month, quarterly - last day of quarter, etc.  So find latest price)
		minimumRequiredPriceDate = getPreviousBusinessDay(security, DateUtils.addDays(minimumRequiredPriceDate, 1), config);
		date = maximumRequiredPriceDate;
		while (DateUtils.compare(date, minimumRequiredPriceDate, false) >= 0) {
			if (securityPriceMap.get(date) != null) {
				BigDecimal price = securityPriceMap.get(date);
				appendPriceForDayToPreview(previewResult, security, price, date);
				return price;
			}
			// Need previous day, not previous week day because if month end pricing and month end lands on a weekend we need to catch it
			date = DateUtils.addDays(date, -1);
		}
		if (throwExceptionIfMissing) {
			if (DateUtils.compare(minimumRequiredPriceDate, maximumRequiredPriceDate, false) == 0) {
				throw new ValidationException("Missing price for security [" + security.getLabel() + "] on " + DateUtils.fromDateShort(minimumRequiredPriceDate));
			}
			else {
				throw new ValidationException("Missing price for security [" + security.getLabel() + "] between " + DateUtils.fromDateRange(minimumRequiredPriceDate, maximumRequiredPriceDate, true));
			}
		}
		return null;
	}


	protected BigDecimal getMarketDataExchangeRate(InvestmentSecurity fromCcy, InvestmentSecurity toCcy, Date startDate, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config, boolean throwExceptionIfMissing) {
		// Default for "prices", price allocation of a currency is considered it's fx rate changes to USD
		if (toCcy == null) {
			toCcy = getInvestmentInstrumentService().getInvestmentSecurityBySymbol("USD", true);
		}

		if (fromCcy.equals(toCcy)) {
			return BigDecimal.ONE;
		}

		Map<Date, BigDecimal> fxRateMap = config.getFxRateMap(fromCcy, toCcy, startDate, getMarketDataSourceService(), getMarketDataRatesService());
		if (fxRateMap.get(date) != null) {
			return fxRateMap.get(date);
		}

		if (throwExceptionIfMissing) {
			String marketDataSourceName = config.getFxRateMarketDataSource() != null ? config.getFxRateMarketDataSource().getName() : MarketDataRatesRetriever.MARKET_DATASOURCE_EXCHANGE_RATES;
			throw new ValidationException("Missing exchange rate from [" + fromCcy.getLabel() + "] to [" + toCcy.getLabel() + "] on " + DateUtils.fromDateShort(date) + " for data source [" + marketDataSourceName + "]");
		}
		return null;
	}


	private void appendPriceForDayToPreview(StringBuilder previewResult, InvestmentSecurity security, BigDecimal price, Date date) {
		if (previewResult != null) {
			previewResult.append(StringUtils.NEW_LINE)
					.append(StringUtils.TAB)
					.append(security.getSymbol()).append(" Price Lookup Value [")
					.append(CoreMathUtils.formatNumberDecimal(price))
					.append("] on [").append(DateUtils.fromDateShort(date)).append("]");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private boolean isMonthEndOnly() {
		return getInvestmentSecurityAllocationType().getCalculatedFieldType().isMonthEndOnly();
	}


	/**
	 * For Daily values will return previous business day
	 * For Month End values will return last day of the previous month - regardless if it is a business day or not
	 */
	protected Date getPreviousDay(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		if (isMonthEndOnly()) {
			return DateUtils.addDays(DateUtils.getFirstDayOfMonth(date), -1);
		}
		return getPreviousBusinessDay(security, date, config);
	}


	private Date getPreviousBusinessDay(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		return getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, config.getCalendarForSecurity(security, getInvestmentCalculator())));
	}


	/**
	 * For Daily values will return next business day
	 * For Month End values will return last day of the following month - regardless if it is a business day or not
	 */
	private Date getNextDay(InvestmentSecurity security, Date date, MarketDataInvestmentSecurityAllocationCalculatorConfig config) {
		if (isMonthEndOnly()) {
			return DateUtils.addDays(DateUtils.addMonths(DateUtils.getFirstDayOfMonth(date), 2), -1);
		}
		return getCalendarBusinessDayService().getNextBusinessDay(CalendarBusinessDayCommand.forDate(date, config.getCalendarForSecurity(security, getInvestmentCalculator())));
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public InvestmentSecurityAllocationTypes getInvestmentSecurityAllocationType() {
		return this.investmentSecurityAllocationType;
	}


	public void setInvestmentSecurityAllocationType(InvestmentSecurityAllocationTypes investmentSecurityAllocationType) {
		this.investmentSecurityAllocationType = investmentSecurityAllocationType;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentSecurityAllocationHandler getInvestmentSecurityAllocationHandler() {
		return this.investmentSecurityAllocationHandler;
	}


	public void setInvestmentSecurityAllocationHandler(InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler) {
		this.investmentSecurityAllocationHandler = investmentSecurityAllocationHandler;
	}


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
