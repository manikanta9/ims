package com.clifton.marketdata.instrument.allocation.calculators;


import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationTypes;


public interface MarketDataInvestmentSecurityAllocationCalculatorLocator {

	/**
	 * Returns MarketDataInvestmentSecurityAllocationCalculator for the specified allocationType
	 */
	public MarketDataInvestmentSecurityAllocationCalculator locate(InvestmentSecurityAllocationTypes allocationType);
}
