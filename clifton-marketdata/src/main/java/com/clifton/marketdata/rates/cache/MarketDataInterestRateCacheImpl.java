package com.clifton.marketdata.rates.cache;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.date.DateUtils;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The MarketDataInterestRateCacheImpl caches the BigDecimal rate value for a given index and date (using key IndexID_Date)
 *
 * @author manderson
 */
@Component
public class MarketDataInterestRateCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataInterestRate, String, ObjectWrapper<BigDecimal>> implements MarketDataInterestRateCache {


	@Override
	public ObjectWrapper<BigDecimal> getMarketDataInterestRateValue(int indexId, Date rateDate) {
		return getCacheHandler().get(getCacheName(), getBeanKey(indexId, rateDate));
	}


	@Override
	public void setMarketDataInterestRateValue(int indexId, Date rateDate, BigDecimal rate) {
		getCacheHandler().put(getCacheName(), getBeanKey(indexId, rateDate), new ObjectWrapper<>(rate));
	}


	private String getBeanKey(MarketDataInterestRate bean) {
		return getBeanKey(bean.getInterestRateIndex().getId(), bean.getInterestRateDate());
	}


	private String getBeanKey(int indexId, Date rateDate) {
		StringBuilder key = new StringBuilder(24);
		key.append(indexId);
		key.append('-');
		key.append(DateUtils.fromDateShort(rateDate));
		return key.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<MarketDataInterestRate> dao, DaoEventTypes event, MarketDataInterestRate bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<MarketDataInterestRate> dao, DaoEventTypes event, MarketDataInterestRate bean, Throwable e) {
		if (e == null) {
			String key = getBeanKey(bean);
			if (event.isUpdate()) {
				MarketDataInterestRate originalBean = getOriginalBean(dao, bean);
				String originalKey = getBeanKey(originalBean);
				if (!key.equals(originalKey)) {
					getCacheHandler().remove(getCacheName(), originalKey);
				}
			}
			getCacheHandler().remove(getCacheName(), getBeanKey(bean));
		}
	}
}
