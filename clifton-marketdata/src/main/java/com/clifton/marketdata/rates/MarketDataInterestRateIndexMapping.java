package com.clifton.marketdata.rates;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>MarketDataInterestRateIndexMapping</code> maps {@link InvestmentInterestRateIndex} to
 * {@link MarketDataSourceSector}
 *
 * @author manderson
 */
public class MarketDataInterestRateIndexMapping extends ManyToManyEntity<InvestmentInterestRateIndex, MarketDataSourceSector, Integer> {

	private String dataSourceSymbol;

	private SystemBean valueTransformationBean;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the InterestRateIndex symbol if one not specified for this mapping
	 */
	public String getSymbol() {
		if (StringUtils.isEmpty(getDataSourceSymbol())) {
			if (getReferenceOne() != null) {
				return getReferenceOne().getSymbol();
			}
		}
		return getDataSourceSymbol();
	}

	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////


	public String getDataSourceSymbol() {
		return this.dataSourceSymbol;
	}


	public void setDataSourceSymbol(String dataSourceSymbol) {
		this.dataSourceSymbol = dataSourceSymbol;
	}


	public SystemBean getValueTransformationBean() {
		return this.valueTransformationBean;
	}


	public void setValueTransformationBean(SystemBean valueTransformationBean) {
		this.valueTransformationBean = valueTransformationBean;
	}
}
