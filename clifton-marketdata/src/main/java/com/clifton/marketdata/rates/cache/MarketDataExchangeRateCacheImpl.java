package com.clifton.marketdata.rates.cache;


import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.date.DateUtils;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataExchangeRateCacheImpl</code> class provides caching and retrieval from cache of MarketDataExchangeRate objects
 * by a combination of parameters.
 *
 * @author vgomelsky
 */
@Component
public class MarketDataExchangeRateCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataExchangeRate, String, ObjectWrapper<MarketDataExchangeRate>> implements MarketDataExchangeRateCache {


	@Override
	public ObjectWrapper<MarketDataExchangeRate> getMarketDataExchangeRate(ExchangeRateSearchForm searchForm) {
		return getCacheHandler().get(getCacheName(), getBeanKey(searchForm));
	}


	@Override
	public void setMarketDataExchangeRate(MarketDataExchangeRate bean, ExchangeRateSearchForm searchForm) {
		getCacheHandler().put(getCacheName(), getBeanKey(searchForm), new ObjectWrapper<>(bean));
	}


	private String getBeanKey(ExchangeRateSearchForm searchForm) {
		if (searchForm.getLimit() != 1) {
			throw new IllegalArgumentException("searchForm must be limited to 1 argument");
		}
		StringBuilder key = new StringBuilder(64);
		key.append(searchForm.getDataSourceId());
		key.append('-');
		key.append(searchForm.getFromCurrencyId());
		key.append('-');
		key.append(searchForm.getToCurrencyId());
		key.append('-');
		// Use Specific Date formatting for including dates in the key.  Otherwise in some cases you could get 2017-10-25 and in others Wed Oct 25 00:00:00 EDT 2017
		key.append(searchForm.getMaxRateDate() == null ? null : DateUtils.fromDateShort(searchForm.getMaxRateDate()));
		key.append('-');
		key.append(searchForm.getRateDate() == null ? null : DateUtils.fromDateShort(searchForm.getRateDate()));
		key.append('-');
		return key.toString();
	}
}
