package com.clifton.marketdata.rates.cache;

import com.clifton.core.beans.ObjectWrapper;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The MarketDataInterestRateCache caches the BigDecimal rate value for a given index and date (using key IndexID_Date)
 *
 * @author manderson
 */
public interface MarketDataInterestRateCache {

	public ObjectWrapper<BigDecimal> getMarketDataInterestRateValue(int indexId, Date rateDate);


	public void setMarketDataInterestRateValue(int indexId, Date rateDate, BigDecimal rate);
}
