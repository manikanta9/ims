package com.clifton.marketdata.rates.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class InterestRateIndexMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceOne.name,referenceOne.symbol,referenceTwo.name,referenceTwo.dataSource.name")
	private String searchPattern;

	@SearchField(searchField = "referenceOne.id")
	private Integer interestRateIndexId;

	@SearchField(searchField = "referenceTwo.id")
	private Short dataSourceSectorId;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "name")
	private String dataSourceSectorName;

	@SearchField(searchFieldPath = "referenceTwo.dataSource", searchField = "name")
	private String dataSourceName;

	@SearchField(searchFieldPath = "referenceTwo", searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField
	private String dataSourceSymbol;

	@SearchField(searchFieldPath = "referenceOne", searchField = "groupList.referenceOne.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short interestRateIndexGroupId;

	@SearchField(searchFieldPath = "referenceOne", searchField = "name,symbol", sortField = "symbol")
	private String interestRateIndexLabel;

	// Custom Search Field
	private Boolean interestRateIndexActive;

	@SearchField(searchField = "valueTransformationBean.id")
	private Integer valueTransformationBeanId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getInterestRateIndexId() {
		return this.interestRateIndexId;
	}


	public void setInterestRateIndexId(Integer interestRateIndexId) {
		this.interestRateIndexId = interestRateIndexId;
	}


	public Short getDataSourceSectorId() {
		return this.dataSourceSectorId;
	}


	public void setDataSourceSectorId(Short dataSourceSectorId) {
		this.dataSourceSectorId = dataSourceSectorId;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getDataSourceSymbol() {
		return this.dataSourceSymbol;
	}


	public void setDataSourceSymbol(String dataSourceSymbol) {
		this.dataSourceSymbol = dataSourceSymbol;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataSourceSectorName() {
		return this.dataSourceSectorName;
	}


	public void setDataSourceSectorName(String dataSourceSectorName) {
		this.dataSourceSectorName = dataSourceSectorName;
	}


	public String getInterestRateIndexLabel() {
		return this.interestRateIndexLabel;
	}


	public void setInterestRateIndexLabel(String interestRateIndexLabel) {
		this.interestRateIndexLabel = interestRateIndexLabel;
	}


	public Short getInterestRateIndexGroupId() {
		return this.interestRateIndexGroupId;
	}


	public void setInterestRateIndexGroupId(Short interestRateIndexGroupId) {
		this.interestRateIndexGroupId = interestRateIndexGroupId;
	}


	public Boolean getInterestRateIndexActive() {
		return this.interestRateIndexActive;
	}


	public void setInterestRateIndexActive(Boolean interestRateIndexActive) {
		this.interestRateIndexActive = interestRateIndexActive;
	}


	public Integer getValueTransformationBeanId() {
		return this.valueTransformationBeanId;
	}


	public void setValueTransformationBeanId(Integer valueTransformationBeanId) {
		this.valueTransformationBeanId = valueTransformationBeanId;
	}
}
