package com.clifton.marketdata.rates;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.marketdata.datasource.MarketDataSource;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataInterestRate</code> class represents a single observation of an interest rate index
 * at a specific date.
 *
 * @author vgomelsky
 */
public class MarketDataInterestRate extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentInterestRateIndex interestRateIndex;
	private MarketDataSource dataSource;
	private Date interestRateDate;
	private BigDecimal interestRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append(this.interestRateIndex.getName());
		result.append(" from ");
		result.append(this.dataSource.getName());
		result.append(" on ");
		result.append(DateUtils.fromDateShort(this.interestRateDate));
		result.append(" was ");
		result.append(this.interestRate);
		return result.toString();
	}


	public InvestmentInterestRateIndex getInterestRateIndex() {
		return this.interestRateIndex;
	}


	public void setInterestRateIndex(InvestmentInterestRateIndex interestRateIndex) {
		this.interestRateIndex = interestRateIndex;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}
}
