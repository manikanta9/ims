package com.clifton.marketdata.rates.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ExchangeRateSearchForm</code> class provides search definitions for the MarketDataExchangeRate objects.
 *
 * @author vgomelsky
 */
public class ExchangeRateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "fromCurrency.id")
	private Integer fromCurrencyId;

	@SearchField(searchField = "toCurrency.id")
	private Integer toCurrencyId;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchField = "dataSource.id")
	private Short[] dataSourceIds;

	@SearchField(searchFieldPath = "dataSource", searchField = "name")
	private String dataSourceName;

	@SearchField
	private Date rateDate;

	@SearchField(searchField = "rateDate", comparisonConditions = ComparisonConditions.GREATER_THAN)
	private Date minRateDate;

	@SearchField(searchField = "rateDate", comparisonConditions = ComparisonConditions.LESS_THAN)
	private Date maxRateDate;

	@SearchField
	private BigDecimal exchangeRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getFromCurrencyId() {
		return this.fromCurrencyId;
	}


	public void setFromCurrencyId(Integer fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}


	public Integer getToCurrencyId() {
		return this.toCurrencyId;
	}


	public void setToCurrencyId(Integer toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Short[] getDataSourceIds() {
		return this.dataSourceIds;
	}


	public void setDataSourceIds(Short[] dataSourceIds) {
		this.dataSourceIds = dataSourceIds;
	}


	public Date getRateDate() {
		return this.rateDate;
	}


	public void setRateDate(Date date) {
		this.rateDate = date;
	}


	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Date getMaxRateDate() {
		return this.maxRateDate;
	}


	public void setMaxRateDate(Date maxRateDate) {
		this.maxRateDate = maxRateDate;
	}


	public Date getMinRateDate() {
		return this.minRateDate;
	}


	public void setMinRateDate(Date minRateDate) {
		this.minRateDate = minRateDate;
	}
}
