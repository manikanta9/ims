package com.clifton.marketdata.rates.transformation;

/**
 * Base interface for creating beans that transform one value/object to another value/object of the same type
 *
 * @author mitchellf
 */
public interface MarketDataValueTransformer<T> {

	public T transform(T value);
}
