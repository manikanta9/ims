package com.clifton.marketdata.rates.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>InterestRateSearchForm</code> class defines allowed search filters for the InterestRateSearch objects.
 *
 * @author vgomelsky
 */
public class InterestRateSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "interestRateIndex.id")
	private Integer interestRateIndexId;

	@SearchField(searchField = "interestRateIndex.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] interestRateIndexIdList;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchFieldPath = "dataSource", searchField = "name")
	private String dataSourceName;

	@SearchField(searchFieldPath = "interestRateIndex", searchField = "name,symbol", sortField = "symbol")
	private String interestRateIndexLabel;

	@SearchField
	private Date interestRateDate;

	@SearchField
	private BigDecimal interestRate;

	@SearchField(searchFieldPath = "interestRateIndex", searchField = "order")
	private Integer interestRateIndexOrder;

	@SearchField(searchFieldPath = "interestRateIndex.currencyDenomination", searchField = "symbol,name", sortField = "symbol")
	private String currencyLabel;

	@SearchField(searchFieldPath = "interestRateIndex.calendar", searchField = "name")
	private String calendarName;

	@SearchField(searchFieldPath = "interestRateIndex", searchField = "numberOfDays")
	private Integer interestRateIndexDays;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	public Integer getInterestRateIndexId() {
		return this.interestRateIndexId;
	}


	public Integer[] getInterestRateIndexIdList() {
		return this.interestRateIndexIdList;
	}


	public void setInterestRateIndexIdList(Integer[] interestRateIndexIdList) {
		this.interestRateIndexIdList = interestRateIndexIdList;
	}


	public void setInterestRateIndexId(Integer interestRateIndexId) {
		this.interestRateIndexId = interestRateIndexId;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Date getInterestRateDate() {
		return this.interestRateDate;
	}


	public void setInterestRateDate(Date interestRateDate) {
		this.interestRateDate = interestRateDate;
	}


	public BigDecimal getInterestRate() {
		return this.interestRate;
	}


	public void setInterestRate(BigDecimal interestRate) {
		this.interestRate = interestRate;
	}


	public String getInterestRateIndexLabel() {
		return this.interestRateIndexLabel;
	}


	public void setInterestRateIndexLabel(String interestRateIndexLabel) {
		this.interestRateIndexLabel = interestRateIndexLabel;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Integer getInterestRateIndexOrder() {
		return this.interestRateIndexOrder;
	}


	public void setInterestRateIndexOrder(Integer interestRateIndexOrder) {
		this.interestRateIndexOrder = interestRateIndexOrder;
	}


	public String getCurrencyLabel() {
		return this.currencyLabel;
	}


	public void setCurrencyLabel(String currencyLabel) {
		this.currencyLabel = currencyLabel;
	}


	public String getCalendarName() {
		return this.calendarName;
	}


	public void setCalendarName(String calendarName) {
		this.calendarName = calendarName;
	}


	public Integer getInterestRateIndexDays() {
		return this.interestRateIndexDays;
	}


	public void setInterestRateIndexDays(Integer interestRateIndexDays) {
		this.interestRateIndexDays = interestRateIndexDays;
	}
}
