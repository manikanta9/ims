package com.clifton.marketdata.rates;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataExchangeRate</code> class defines an exchange rate from one currency to another at a given time
 * from a given market data source.
 *
 * @author vgomelsky
 */
public class MarketDataExchangeRate extends BaseEntity<Integer> implements LabeledObject {

	private InvestmentSecurity fromCurrency;
	private InvestmentSecurity toCurrency;
	private MarketDataSource dataSource;
	private Date rateDate;
	private BigDecimal exchangeRate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append(this.dataSource.getName());
		result.append(": 1 ");
		result.append(this.fromCurrency.getSymbol());
		result.append(" = ");
		result.append(this.exchangeRate.toPlainString());
		result.append(' ');
		result.append(this.toCurrency.getSymbol());
		result.append(" on ");
		result.append(DateUtils.fromDateShort(this.rateDate));
		return result.toString();
	}


	public InvestmentSecurity getFromCurrency() {
		return this.fromCurrency;
	}


	public void setFromCurrency(InvestmentSecurity fromCurrency) {
		this.fromCurrency = fromCurrency;
	}


	public InvestmentSecurity getToCurrency() {
		return this.toCurrency;
	}


	public void setToCurrency(InvestmentSecurity toCurrency) {
		this.toCurrency = toCurrency;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public Date getRateDate() {
		return this.rateDate;
	}


	public void setRateDate(Date rateDate) {
		this.rateDate = rateDate;
	}


	public BigDecimal getExchangeRate() {
		return this.exchangeRate;
	}


	public void setExchangeRate(BigDecimal exchangeRate) {
		this.exchangeRate = exchangeRate;
	}
}
