package com.clifton.marketdata.rates;


import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.marketdata.live.MarketDataLiveExchangeRate;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataRatesRetriever</code> interface provides methods for retrieving Exchange Rates and Interest Rates for various use cases and types of arguments.
 *
 * @author manderson
 */
public interface MarketDataRatesRetriever {

	public static final String MARKET_DATASOURCE_EXCHANGE_RATES = "Goldman Sachs";


	////////////////////////////////////////////////////////////////////////////
	///////////////////     Exchange Rate Lookup Methods        ////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataLiveExchangeRate getLiveExchangeRate(InvestmentSecurity security, InvestmentSecurity baseCurrency);


	/**
	 * Returns BigDecimal ExchangeRate value from/to currencies for a given date.
	 * Will check datasource and if missing will use the default data source
	 * <p>
	 * If still missing, will look for the reverse rate and return 1/rate.
	 * If missing, and exceptionIfRateIsMissing is true, will throw an exception, otherwise will return null
	 */
	public BigDecimal getExchangeRateForDataSource(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing);


	/**
	 * Returns BigDecimal ExchangeRate value from/to currencies for a given date and data source.
	 * If the rate is missing, returns the closest rate available prior to the specified date.
	 * <p>
	 * If fallbackToDefaultDataSource is true, then also looks up default data source exchange rate and returns it only if it's closer to the
	 * specified date than the closest rate available from the specified data source.
	 * <p>
	 * If still missing, will look for the reverse rate and return 1/rate.
	 */
	public BigDecimal getExchangeRateForDataSourceFlexible(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing);


	public BigDecimal getExchangeRateReturn(InvestmentAccount account, InvestmentSecurity fromCurrency, Date startDate, Date endDate, String securityTypeString, boolean returnAsHundredsValue, boolean useFlexibleLookup);


	/**
	 * Returns BigDecimal ExchangeRate for the dominant currency of the two provided.
	 * <p>
	 * Functions the same way as {@link MarketDataRatesRetriever#getExchangeRateForDataSourceFlexible(String, String, String, Date, boolean, boolean)}
	 * using the dominant currency as the fromCurrency and the other currency as the toCurrency.
	 */
	public BigDecimal getDominantCurrencyExchangeRateForDataSourceFlexible(String currencyOne, String currencyTwo, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing);


	////////////////////////////////////////////////////////////////////////////
	/////////             Interest Rate Lookup Methods                  ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the numeric rate for given index and date.
	 * Note: Uses Cache lookup
	 * <p>
	 * If the rate is missing, checks if the date is a holiday for the index's calendar (if not defined uses system calendar)
	 * if it is will look up and return rate for previous business day
	 * Throws an exception if the index is not active on the date or if the rate is missing (if opted)
	 */
	public BigDecimal getMarketDataInterestRateForDate(InvestmentInterestRateIndex index, Date date, boolean exceptionIfRateIsMissing);


	public BigDecimal getInvestmentInterestRateIndexReturn(InvestmentInterestRateIndex index, Date startDate, Date endDate, boolean returnAsHundredsValue);
}
