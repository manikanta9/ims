package com.clifton.marketdata.rates.cache;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;


/**
 * The <code>MarketDataExchangeRateCache</code> class provides caching and retrieval from cache of MarketDataExchangeRate objects
 * by a combination of parameters.
 *
 * @author vgomelsky
 */
public interface MarketDataExchangeRateCache {


	public ObjectWrapper<MarketDataExchangeRate> getMarketDataExchangeRate(ExchangeRateSearchForm searchForm);


	public void setMarketDataExchangeRate(MarketDataExchangeRate bean, ExchangeRateSearchForm searchForm);
}
