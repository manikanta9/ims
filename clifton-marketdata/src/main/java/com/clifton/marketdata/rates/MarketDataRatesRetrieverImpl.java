package com.clifton.marketdata.rates;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.api.security.InvestmentSecurityApiService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.currency.InvestmentCurrencyService;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.marketdata.MarketDataRetrieverImpl;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.live.MarketDataLiveExchangeRate;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.rates.cache.MarketDataInterestRateCache;
import com.clifton.marketdata.rates.search.InterestRateSearchForm;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class MarketDataRatesRetrieverImpl implements MarketDataRatesRetriever {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarSetupService calendarSetupService;

	private InvestmentCurrencyService investmentCurrencyService;
	private InvestmentSecurityApiService investmentSecurityApiService;

	private MarketDataLiveService marketDataLiveService;
	private MarketDataRatesService marketDataRatesService;
	private MarketDataSourceService marketDataSourceService;

	private MarketDataInterestRateCache marketDataInterestRateCache;


	//////////////////////////////////////////////////////////
	/////////     Exchange Rate Lookup Methods        ////////
	//////////////////////////////////////////////////////////


	@Override
	public BigDecimal getExchangeRateForDataSourceFlexible(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing) {
		return getExchangeRateImpl(fromCurrency, toCurrency, dataSourceName, date, true, fallbackToDefaultDataSource, exceptionIfRateIsMissing);
	}


	@Override
	public BigDecimal getExchangeRateForDataSource(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing) {
		return getExchangeRateImpl(fromCurrency, toCurrency, dataSourceName, date, false, fallbackToDefaultDataSource, exceptionIfRateIsMissing);
	}


	@Override
	public BigDecimal getDominantCurrencyExchangeRateForDataSourceFlexible(String currencyOne, String currencyTwo, String dataSourceName, Date date, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing) {
		String dominantCurrencySymbol = getInvestmentCurrencyService().getInvestmentCurrencyConventionDominantCurrency(currencyOne, currencyTwo);
		if (currencyOne.equals(dominantCurrencySymbol)) {
			return getExchangeRateForDataSourceFlexible(currencyOne, currencyTwo, dataSourceName, date, fallbackToDefaultDataSource, exceptionIfRateIsMissing);
		}
		else {
			return getExchangeRateForDataSourceFlexible(currencyTwo, currencyOne, dataSourceName, date, fallbackToDefaultDataSource, exceptionIfRateIsMissing);
		}
	}


	private BigDecimal getExchangeRateImpl(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean previousIfMissing, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing) {
		return getExchangeRateImpl(fromCurrency, toCurrency, dataSourceName, date, previousIfMissing, fallbackToDefaultDataSource, exceptionIfRateIsMissing, true);
	}


	private BigDecimal getExchangeRateImpl(String fromCurrency, String toCurrency, String dataSourceName, Date date, boolean previousIfMissing, boolean fallbackToDefaultDataSource, boolean exceptionIfRateIsMissing, boolean checkReverseRate) {
		if (fromCurrency.equals(toCurrency)) {
			return BigDecimal.ONE;
		}

		int fromCurrencyId = getCurrencyId(fromCurrency);
		int toCurrencyId = getCurrencyId(toCurrency);
		MarketDataExchangeRate exchangeRate;
		if (previousIfMissing) {
			exchangeRate = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDateFlexible(dataSourceName, fromCurrencyId, toCurrencyId, date);
			if (fallbackToDefaultDataSource && !MARKET_DATASOURCE_EXCHANGE_RATES.equals(dataSourceName)) {
				// it's better to return more current rate from default data source than less current from specific
				if (exchangeRate != null && DateUtils.compare(exchangeRate.getRateDate(), date, false) != 0) {
					MarketDataExchangeRate exchangeRateFromDefault = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDateFlexible(MARKET_DATASOURCE_EXCHANGE_RATES, fromCurrencyId, toCurrencyId, date);
					if (exchangeRateFromDefault != null && DateUtils.compare(exchangeRate.getRateDate(), exchangeRateFromDefault.getRateDate(), false) < 0) {
						exchangeRate = exchangeRateFromDefault;
					}
				}
			}
		}
		else {
			exchangeRate = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDate(dataSourceName, fromCurrencyId, toCurrencyId, date);
		}

		if (exchangeRate == null) {
			// may not have exchange rates for all data sources: try default if no rate for this data source defined
			if (fallbackToDefaultDataSource && !MARKET_DATASOURCE_EXCHANGE_RATES.equals(dataSourceName)) {
				if (previousIfMissing) {
					exchangeRate = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDateFlexible(MARKET_DATASOURCE_EXCHANGE_RATES, fromCurrencyId, toCurrencyId, date);
				}
				else {
					exchangeRate = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDate(MARKET_DATASOURCE_EXCHANGE_RATES, fromCurrencyId, toCurrencyId, date);
				}
			}

			if (exchangeRate == null) {
				if (checkReverseRate) {
					// If still null, try the exchange rate in the reverse, set checkReverseRate to false to avoid infinite loop
					BigDecimal reverseRate = getExchangeRateImpl(toCurrency, fromCurrency, dataSourceName, date, previousIfMissing, fallbackToDefaultDataSource, exceptionIfRateIsMissing, false);
					if (!MathUtils.isNullOrZero(reverseRate)) {
						return MathUtils.divide(BigDecimal.ONE, reverseRate, DataTypes.EXCHANGE_RATE.getPrecision());
					}
				}

				if (exceptionIfRateIsMissing) {
					throw new ValidationException("Missing Exchange Rate from [" + fromCurrency + " to " + toCurrency + "] on " + (previousIfMissing ? " or before " : "")
							+ "[" + DateUtils.fromDateShort(date) + "]" + (fallbackToDefaultDataSource ? "." : " for [" + dataSourceName + "]."));
				}
				return null;
			}
		}
		return exchangeRate.getExchangeRate();
	}


	/**
	 * Currency symbols should never change so it should be safe to do this here.  Using a map in order to maximize performance while keeping API simple.
	 * May need to revisit this implementation later.
	 */
	private final Map<String, Integer> currencySymbolToId = new ConcurrentHashMap<>();


	private int getCurrencyId(String currencySymbol) {
		Integer currencyId = this.currencySymbolToId.get(currencySymbol);
		if (currencyId == null) {
			currencyId = getInvestmentSecurityApiService().getCurrencyBySymbol(currencySymbol).getId();
			this.currencySymbolToId.put(currencySymbol, currencyId);
		}
		return currencyId;
	}


	@Override
	public MarketDataLiveExchangeRate getLiveExchangeRate(InvestmentSecurity security, InvestmentSecurity baseCurrency) {
		InvestmentSecurity fromCurrency = security.isCurrency() ? security : security.getInstrument().getTradingCurrency();
		// Not necessary to set this, always one.
		if (baseCurrency.equals(fromCurrency)) {
			return null;
		}
		try {
			return getMarketDataLiveService().getMarketDataLiveExchangeRate(fromCurrency.getId(), baseCurrency.getId(),
					MarketDataRetrieverImpl.MARKET_DATA_MAX_AGE_IN_SECONDS);
		}
		catch (Exception e) {
			throw new ValidationException("Unable to lookup live exchange rate for security [" + security.getLabel() + "], exchange rate from [" + fromCurrency.getSymbol() + " to "
					+ baseCurrency.getSymbol() + ": " + e.getMessage(), e);
		}
	}


	@Override
	public BigDecimal getExchangeRateReturn(InvestmentAccount account, InvestmentSecurity fromCurrency, Date startDate, Date endDate, String securityTypeString, boolean returnAsHundredsValue, boolean useFlexibleLookup) {
		String dataSourceName = null;
		String baseCurrency = null;
		if (account != null) {
			dataSourceName = getExchangeRateDataSourceForClientAccount(account);
			baseCurrency = account.getBaseCurrency().getSymbol();
		}

		if (baseCurrency == null) {
			// Just In Case account happens to be null, just use exchange rates to USD
			// The only place this could happen is if it's used for proxy manager adjustments however the UI dropdown for that screen
			// will not be changed to allow currencies yet anyway, because it really only makes sense for asset classes
			baseCurrency = "USD";
		}
		if (StringUtils.isEmpty(dataSourceName)) {
			dataSourceName = MARKET_DATASOURCE_EXCHANGE_RATES;
		}
		try {

			BigDecimal startValue = (useFlexibleLookup ? getExchangeRateForDataSourceFlexible(fromCurrency.getSymbol(), baseCurrency, dataSourceName, startDate, true, true) :
					getExchangeRateForDataSource(fromCurrency.getSymbol(), baseCurrency, dataSourceName, startDate, true, true));
			BigDecimal endValue = (useFlexibleLookup ? getExchangeRateForDataSourceFlexible(fromCurrency.getSymbol(), baseCurrency, dataSourceName, endDate, true, true) :
					getExchangeRateForDataSource(fromCurrency.getSymbol(), baseCurrency, dataSourceName, endDate, true, true));
			return MathUtils.getPercentChange(startValue, endValue, returnAsHundredsValue);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", fromCurrency.getId(), securityTypeString + e.getMessage() + " for data source [" + dataSourceName + "]"
					+ (!CompareUtils.isEqual(dataSourceName, MARKET_DATASOURCE_EXCHANGE_RATES) ? ", and there is no exchange rate for the default data source [" + MARKET_DATASOURCE_EXCHANGE_RATES + "]." : ""));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	/////          Exchange Rate Datasource Name Lookup Methods            /////
	////////////////////////////////////////////////////////////////////////////


	private String getExchangeRateDataSourceForClientAccount(InvestmentAccount clientAccount) {
		if (clientAccount != null && clientAccount.getDefaultExchangeRateSourceCompany() != null) {
			return getMarketDataSourceService().getMarketDataSourceName(clientAccount.getDefaultExchangeRateSourceCompany());
		}
		return MARKET_DATASOURCE_EXCHANGE_RATES;
	}


	//////////////////////////////////////////////////////////
	/////////     Interest Rate Lookup Methods        ////////
	//////////////////////////////////////////////////////////


	@Override
	public BigDecimal getMarketDataInterestRateForDate(InvestmentInterestRateIndex index, Date date, boolean exceptionIfRateIsMissing) {
		// First Check Cache
		ObjectWrapper<BigDecimal> rateObj = getMarketDataInterestRateCache().getMarketDataInterestRateValue(index.getId(), date);
		BigDecimal rateValue;
		// If NOT NULL use the cached value
		if (rateObj != null) {
			rateValue = rateObj.getObject();
		}
		// If it is NULL - first call and do full lookup and store in cache
		else {
			if (!index.isActiveOnDate(date)) {
				throw new ValidationException("Interest Rate Index [" + index.getLabelWithActiveDates() + "] is not active on [" + DateUtils.fromDateShort(date) + "]");
			}
			InterestRateSearchForm searchForm = new InterestRateSearchForm();
			searchForm.setInterestRateIndexId(index.getId());
			searchForm.setInterestRateDate(date);
			MarketDataInterestRate rate = CollectionUtils.getFirstElement(getMarketDataRatesService().getMarketDataInterestRateList(searchForm));
			rateValue = (rate != null ? rate.getInterestRate() : null);
			getMarketDataInterestRateCache().setMarketDataInterestRateValue(index.getId(), date, rateValue);
		}

		// If rateValue is null, check if it's a holiday - if so - return previous day
		// Note: If cached value wraps a null value, then we know not to try to look it up again, but still verify if it's not a business day to return previous day's value
		if (rateValue == null) {
			Short calendarId = (index.getCalendar() != null ? index.getCalendar().getId() : getCalendarSetupService().getDefaultCalendar().getId());
			if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarId))) {
				return getMarketDataInterestRateForDate(index, getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, calendarId)), exceptionIfRateIsMissing);
			}
			else if (exceptionIfRateIsMissing) {
				throw new ValidationExceptionWithCause("InvestmentInterestRateIndex", index.getId(), "Unable to find interest rate for index [" + index.getLabel()
						+ "] and date [" + DateUtils.fromDateShort(date) + "]. " + DateUtils.fromDateShort(date) + " is not a holiday for calendar ["
						+ (index.getCalendar() != null ? index.getCalendar().getName() : " Default ") + ".");
			}
		}
		return rateValue;
	}


	@Override
	public BigDecimal getInvestmentInterestRateIndexReturn(InvestmentInterestRateIndex index, Date startDate, Date endDate, boolean returnAsHundredsValue) {
		BigDecimal rate = getMarketDataInterestRateForDate(index, startDate, true);
		// Interest Rate Performance Calculation: Multiply # of days by the deannualized rate
		BigDecimal dailyChange = MathUtils.getDeAnnualizedRate(rate);
		if (returnAsHundredsValue) {
			dailyChange = MathUtils.multiply(dailyChange, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
		return MathUtils.multiply(dailyChange, DateUtils.getDaysDifference(endDate, startDate));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public InvestmentCurrencyService getInvestmentCurrencyService() {
		return this.investmentCurrencyService;
	}


	public void setInvestmentCurrencyService(InvestmentCurrencyService investmentCurrencyService) {
		this.investmentCurrencyService = investmentCurrencyService;
	}


	public InvestmentSecurityApiService getInvestmentSecurityApiService() {
		return this.investmentSecurityApiService;
	}


	public void setInvestmentSecurityApiService(InvestmentSecurityApiService investmentSecurityApiService) {
		this.investmentSecurityApiService = investmentSecurityApiService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public MarketDataInterestRateCache getMarketDataInterestRateCache() {
		return this.marketDataInterestRateCache;
	}


	public void setMarketDataInterestRateCache(MarketDataInterestRateCache marketDataInterestRateCache) {
		this.marketDataInterestRateCache = marketDataInterestRateCache;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}
}
