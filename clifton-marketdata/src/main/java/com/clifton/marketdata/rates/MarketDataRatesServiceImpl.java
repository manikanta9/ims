package com.clifton.marketdata.rates;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.rates.cache.MarketDataExchangeRateCache;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;
import com.clifton.marketdata.rates.search.InterestRateIndexMappingSearchForm;
import com.clifton.marketdata.rates.search.InterestRateSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataRatesServiceImpl</code> class provides basic implementation of MarketDataRatesService interface.
 *
 * @author vgomelsky
 */
@Service
public class MarketDataRatesServiceImpl implements MarketDataRatesService {

	private AdvancedUpdatableDAO<MarketDataExchangeRate, Criteria> marketDataExchangeRateDAO;
	private AdvancedUpdatableDAO<MarketDataInterestRate, Criteria> marketDataInterestRateDAO;
	private AdvancedUpdatableDAO<MarketDataInterestRateIndexMapping, Criteria> marketDataInterestRateIndexMappingDAO;

	private BusinessCompanyService businessCompanyService;
	private MarketDataSourceService marketDataSourceService;

	private MarketDataExchangeRateCache marketDataExchangeRateCache;

	////////////////////////////////////////////////////////////////////////////
	//////   Market Data Interest Rate Index Mapping Business Methods     //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataInterestRateIndexMapping getMarketDataInterestRateIndexMapping(int id) {
		return getMarketDataInterestRateIndexMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataInterestRateIndexMapping> getMarketDataInterestRateIndexMappingListForDataSource(Short dataSourceId, boolean activeOnly) {
		InterestRateIndexMappingSearchForm searchForm = new InterestRateIndexMappingSearchForm();
		searchForm.setDataSourceId(dataSourceId);
		if (activeOnly) {
			searchForm.setInterestRateIndexActive(true);
		}
		return getMarketDataInterestRateIndexMappingList(searchForm);
	}


	@Override
	public List<MarketDataInterestRateIndexMapping> getMarketDataInterestRateIndexMappingList(final InterestRateIndexMappingSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getInterestRateIndexActive() != null) {
					criteria.add(ActiveExpressionForDates.forActiveWithAlias(searchForm.getInterestRateIndexActive(), getPathAlias("referenceOne", criteria)));
				}
			}
		};
		return getMarketDataInterestRateIndexMappingDAO().findBySearchCriteria(config);
	}


	@Override
	public MarketDataInterestRateIndexMapping saveMarketDataInterestRateIndexMapping(MarketDataInterestRateIndexMapping bean) {
		return getMarketDataInterestRateIndexMappingDAO().save(bean);
	}


	@Override
	public void deleteMarketDataInterestRateIndexMapping(int id) {
		getMarketDataInterestRateIndexMappingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////           Market Data Interest Rate Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataInterestRate getMarketDataInterestRate(int id) {
		return getMarketDataInterestRateDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataInterestRate getMarketDataInterestRateLatest(int indexId) {
		InterestRateSearchForm searchForm = new InterestRateSearchForm();
		searchForm.setInterestRateIndexId(indexId);
		searchForm.setLimit(1);
		// TODO: use sorting to control dates and data source priorities?
		return CollectionUtils.getFirstElement(getMarketDataInterestRateList(searchForm));
	}


	@Override
	public List<MarketDataInterestRate> getMarketDataInterestRateListForPeriod(int indexId, Date startDate, Date endDate) {
		if (startDate == null || endDate == null || indexId < 1) {
			throw new IllegalArgumentException("Must have a non null index, start Date, and end Date.");
		}
		InterestRateSearchForm searchForm = new InterestRateSearchForm();
		searchForm.setInterestRateIndexId(indexId);
		SearchRestriction startRestriction = new SearchRestriction();
		startRestriction.setComparison(ComparisonConditions.GREATER_THAN);
		startRestriction.setValue(DateUtils.clearTime(startDate));
		startRestriction.setField("interestRateDate");
		searchForm.addSearchRestriction(startRestriction);
		SearchRestriction endRestriction = new SearchRestriction();
		endRestriction.setComparison(ComparisonConditions.LESS_THAN);
		endRestriction.setValue(DateUtils.clearTime(endDate));
		endRestriction.setField("interestRateDate");
		searchForm.addSearchRestriction(endRestriction);
		return getMarketDataInterestRateList(searchForm);
	}


	@Override
	public List<MarketDataInterestRate> getMarketDataInterestRateListForIndicesPeriod(List<Integer> indexIds, Date startDate, Date endDate) {
		if (startDate == null || endDate == null || CollectionUtils.isEmpty(indexIds)) {
			throw new IllegalArgumentException("Must have a non null index list, start Date, and end Date.");
		}
		InterestRateSearchForm searchForm = new InterestRateSearchForm();
		searchForm.setInterestRateIndexIdList(CollectionUtils.toArray(indexIds, Integer.class));
		SearchRestriction startRestriction = new SearchRestriction();
		startRestriction.setComparison(ComparisonConditions.GREATER_THAN_OR_EQUALS);
		startRestriction.setValue(DateUtils.clearTime(startDate));
		startRestriction.setField("interestRateDate");
		searchForm.addSearchRestriction(startRestriction);
		SearchRestriction endRestriction = new SearchRestriction();
		endRestriction.setComparison(ComparisonConditions.LESS_THAN_OR_EQUALS);
		endRestriction.setValue(DateUtils.clearTime(endDate));
		endRestriction.setField("interestRateDate");
		searchForm.addSearchRestriction(endRestriction);
		return getMarketDataInterestRateList(searchForm);
	}


	@Override
	public List<MarketDataInterestRate> getMarketDataInterestRateList(InterestRateSearchForm searchForm) {
		return getMarketDataInterestRateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataInterestRate saveMarketDataInterestRate(MarketDataInterestRate bean) {
		ValidationUtils.assertNotNull(bean.getDataSource(), "Market Data Source is required to save Market Data Interest Rate");
		ValidationUtils.assertTrue(bean.getDataSource().isInterestRateSupported(), () -> "Market Data Source selected does not support Interest Rates: " + bean.getDataSource().getName());
		return getMarketDataInterestRateDAO().save(bean);
	}


	@Override
	public void deleteMarketDataInterestRate(int id) {
		getMarketDataInterestRateDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////           Market Data Exchange Rate Business Methods        /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRate(int id) {
		return getMarketDataExchangeRateDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateForDataSourceAndDate(String dataSourceName, int fromCurrencyId, int toCurrencyId, Date rateDate) {
		return getMarketDataExchangeRateImpl(dataSourceName, fromCurrencyId, toCurrencyId, rateDate, false);
	}


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateForDataSourceAndDateFlexible(String dataSourceName, int fromCurrencyId, int toCurrencyId, Date rateDate) {
		return getMarketDataExchangeRateImpl(dataSourceName, fromCurrencyId, toCurrencyId, rateDate, true);
	}


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateForDate(int companyId, int fromCurrencyId, int toCurrencyId, Date rateDate) {
		BusinessCompany company = getBusinessCompanyService().getBusinessCompany(companyId);
		String dataSourceName = getMarketDataSourceService().getMarketDataSourceName(company);
		return getMarketDataExchangeRateImpl(dataSourceName, fromCurrencyId, toCurrencyId, rateDate, false);
	}


	@Override
	public MarketDataExchangeRate getMarketDataExchangeRateForDateFlexible(int companyId, int fromCurrencyId, int toCurrencyId, Date rateDate) {
		BusinessCompany company = getBusinessCompanyService().getBusinessCompany(companyId);
		String dataSourceName = getMarketDataSourceService().getMarketDataSourceName(company);
		return getMarketDataExchangeRateImpl(dataSourceName, fromCurrencyId, toCurrencyId, rateDate, true);
	}


	private MarketDataExchangeRate getMarketDataExchangeRateImpl(String dataSourceName, int fromCurrencyId, int toCurrencyId, Date rateDate, boolean flexible) {
		// Always Clear time component from the rate date
		rateDate = DateUtils.clearTime(rateDate);

		if (fromCurrencyId == toCurrencyId) { // same currency: always 1
			MarketDataExchangeRate result = new MarketDataExchangeRate();
			result.setExchangeRate(BigDecimal.ONE);
			result.setRateDate(rateDate);
			return result;
		}

		ExchangeRateSearchForm searchForm = new ExchangeRateSearchForm();
		if (!StringUtils.isEmpty(dataSourceName)) {
			try {
				searchForm.setDataSourceId(getMarketDataSourceService().getMarketDataSourceByName(dataSourceName).getId()); // cached lookup to avoid join
			}
			catch (EntityNotFoundValidationException e) {
				return null; // no data source means no market data
			}
		}
		searchForm.setFromCurrencyId(fromCurrencyId);
		searchForm.setToCurrencyId(toCurrencyId);

		if (flexible) {
			rateDate = DateUtils.addDays(rateDate, 1);
			searchForm.setMaxRateDate(rateDate);
		}
		else {
			searchForm.setRateDate(rateDate);
		}

		searchForm.setOrderBy("rateDate:desc");
		searchForm.setLimit(1);

		// lookup in cache first
		MarketDataExchangeRate result; // support nulls
		ObjectWrapper<MarketDataExchangeRate> wrappedResult = getMarketDataExchangeRateCache().getMarketDataExchangeRate(searchForm);
		if (wrappedResult == null) {
			List<MarketDataExchangeRate> list = getMarketDataExchangeRateList(searchForm);
			result = CollectionUtils.getFirstElement(list);
			// update cache
			getMarketDataExchangeRateCache().setMarketDataExchangeRate(result, searchForm);
		}
		else {
			result = wrappedResult.getObject();
		}
		return result;
	}


	@Override
	public List<MarketDataExchangeRate> getMarketDataExchangeRateList(ExchangeRateSearchForm searchForm) {
		return getMarketDataExchangeRateDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataExchangeRate saveMarketDataExchangeRate(MarketDataExchangeRate bean) {
		ValidationUtils.assertNotNull(bean.getDataSource(), "Market Data Source is required to save Market Data Exchange Rate");
		ValidationUtils.assertTrue(bean.getDataSource().isExchangeRateSupported(), () -> "Market Data Source selected does not support Exchange Rates: " + bean.getDataSource().getName());
		return getMarketDataExchangeRateDAO().save(bean);
	}


	@Override
	public void deleteMarketDataExchangeRate(int id) {
		getMarketDataExchangeRateDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataInterestRate, Criteria> getMarketDataInterestRateDAO() {
		return this.marketDataInterestRateDAO;
	}


	public void setMarketDataInterestRateDAO(AdvancedUpdatableDAO<MarketDataInterestRate, Criteria> marketDataInterestRateDAO) {
		this.marketDataInterestRateDAO = marketDataInterestRateDAO;
	}


	public AdvancedUpdatableDAO<MarketDataExchangeRate, Criteria> getMarketDataExchangeRateDAO() {
		return this.marketDataExchangeRateDAO;
	}


	public void setMarketDataExchangeRateDAO(AdvancedUpdatableDAO<MarketDataExchangeRate, Criteria> marketDataExchangeRateDAO) {
		this.marketDataExchangeRateDAO = marketDataExchangeRateDAO;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public MarketDataExchangeRateCache getMarketDataExchangeRateCache() {
		return this.marketDataExchangeRateCache;
	}


	public void setMarketDataExchangeRateCache(MarketDataExchangeRateCache marketDataExchangeRateCache) {
		this.marketDataExchangeRateCache = marketDataExchangeRateCache;
	}


	public AdvancedUpdatableDAO<MarketDataInterestRateIndexMapping, Criteria> getMarketDataInterestRateIndexMappingDAO() {
		return this.marketDataInterestRateIndexMappingDAO;
	}


	public void setMarketDataInterestRateIndexMappingDAO(AdvancedUpdatableDAO<MarketDataInterestRateIndexMapping, Criteria> marketDataInterestRateIndexMappingDAO) {
		this.marketDataInterestRateIndexMappingDAO = marketDataInterestRateIndexMappingDAO;
	}
}
