package com.clifton.marketdata.rates.transformation;

import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * Implementation of {@link MarketDataValueTransformer} that applies a user-defined Freemarker template to transform the provided value
 *
 * @author mitchellf
 */
public class MarketDataValueFreemarkerTransformer implements MarketDataValueTransformer<BigDecimal> {

	private TemplateConverter templateConverter;

	private String freemarkerTemplate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal transform(BigDecimal value) {
		Map<String, Object> variables = new HashMap<>();

		variables.put("INDEX_VALUE", value);

		String convertedExpression = getTemplateConverter().convertExpression(getFreemarkerTemplate(), variables);

		return CoreMathUtils.evaluateExpression(convertedExpression);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getFreemarkerTemplate() {
		return this.freemarkerTemplate;
	}


	public void setFreemarkerTemplate(String freemarkerTemplate) {
		this.freemarkerTemplate = freemarkerTemplate;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
