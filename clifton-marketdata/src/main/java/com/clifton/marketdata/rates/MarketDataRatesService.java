package com.clifton.marketdata.rates;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;
import com.clifton.marketdata.rates.search.InterestRateIndexMappingSearchForm;
import com.clifton.marketdata.rates.search.InterestRateSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataRatesService</code> interface defines methods for working with
 * interest rates and exchange rates.
 *
 * @author vgomelsky
 */
public interface MarketDataRatesService {

	//////////////////////////////////////////////////////////////////////////// 
	//////   Market Data Interest Rate Index Mapping Business Methods     ////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRateIndexMapping getMarketDataInterestRateIndexMapping(int id);


	public List<MarketDataInterestRateIndexMapping> getMarketDataInterestRateIndexMappingListForDataSource(Short dataSourceId, boolean activeOnly);


	public List<MarketDataInterestRateIndexMapping> getMarketDataInterestRateIndexMappingList(InterestRateIndexMappingSearchForm searchForm);


	public MarketDataInterestRateIndexMapping saveMarketDataInterestRateIndexMapping(MarketDataInterestRateIndexMapping bean);


	public void deleteMarketDataInterestRateIndexMapping(int id);


	//////////////////////////////////////////////////////////////////////////// 
	//////           Market Data Interest Rate Business Methods        ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRate getMarketDataInterestRate(int id);


	public MarketDataInterestRate getMarketDataInterestRateLatest(int indexId);


	public List<MarketDataInterestRate> getMarketDataInterestRateListForPeriod(int indexId, Date startDate, Date endDate);


	public List<MarketDataInterestRate> getMarketDataInterestRateListForIndicesPeriod(List<Integer> indexIds, Date startDate, Date endDate);


	public List<MarketDataInterestRate> getMarketDataInterestRateList(InterestRateSearchForm searchForm);


	public MarketDataInterestRate saveMarketDataInterestRate(MarketDataInterestRate bean);


	public void deleteMarketDataInterestRate(int id);


	//////////////////////////////////////////////////////////////////////////// 
	//////            Market Data Exchange Rate Business Methods       ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataExchangeRate getMarketDataExchangeRate(int id);


	/**
	 * Provides the Exchange Rate using the given data source for Exchange Rates on the given date.
	 * <p>
	 * Date search is exact.
	 * <p>
	 * If datasource name is not supplied, then doesn't restrict search on it
	 */
	@DoNotAddRequestMapping
	public MarketDataExchangeRate getMarketDataExchangeRateForDataSourceAndDate(String dataSourceName, int fromCurrencyId, int toCurrencyId, Date rateDate);


	/**
	 * Provides the Exchange Rate using the given data source for Exchange Rates on the given date.
	 * <p>
	 * Date search is exact.
	 * <p>
	 * Looks up the datasource name for the company.  If no company is provided then doesn't restrict search on it
	 */
	@DoNotAddRequestMapping
	public MarketDataExchangeRate getMarketDataExchangeRateForDate(int companyId, int fromCurrencyId, int toCurrencyId, Date rateDate);


	/**
	 * Provides the Exchange Rate using the given data source for Exchange Rates on the given date.
	 * <p>
	 * Date search is flexible - meaning gets the closest exchange rate on or before given date.
	 * <p>
	 * If datasource name is not supplied, then doesn't restrict search on it
	 */
	@DoNotAddRequestMapping
	public MarketDataExchangeRate getMarketDataExchangeRateForDataSourceAndDateFlexible(String dataSourceName, int fromCurrencyId, int toCurrencyId, Date rateDate);


	/**
	 * Provides the Exchange Rate using the given data source for Exchange Rates on the given date.
	 * <p>
	 * Date search is flexible - meaning gets the closest exchange rate on or before given date.
	 * <p>
	 * Looks up the datasource name for the company.  If no company is provided then doesn't restrict search on it
	 */
	public MarketDataExchangeRate getMarketDataExchangeRateForDateFlexible(int companyId, int fromCurrencyId, int toCurrencyId, Date rateDate);


	public List<MarketDataExchangeRate> getMarketDataExchangeRateList(ExchangeRateSearchForm searchForm);


	public MarketDataExchangeRate saveMarketDataExchangeRate(MarketDataExchangeRate bean);


	public void deleteMarketDataExchangeRate(int id);
}
