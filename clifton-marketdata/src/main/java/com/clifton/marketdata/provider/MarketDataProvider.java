package com.clifton.marketdata.provider;


import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataProvider</code> interface defines methods for retrieving market data information from external providers.
 *
 * @author vgomelsky
 */
public interface MarketDataProvider<T extends MarketDataValueGeneric<?>> {

	/**
	 * Returns data source name that this market data provider serves. For example: "Bloomberg".
	 */
	public String getMarketDataSourceName();


	//////////////////////////////////////////////////////////////////////////// 
	//////               Interest Rates Retrieval Methods              ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the latest interest rate available for the specified index mapping
	 */
	public MarketDataInterestRate getMarketDataInterestRateLatest(MarketDataInterestRateIndexMapping mapping);


	/**
	 * Returns the end-of-day interest rate for the specified index mapping on the specified date.
	 */
	public MarketDataInterestRate getMarketDataInterestRateForDate(MarketDataInterestRateIndexMapping mapping, Date date);


	/**
	 * Returns historic end-of-day interest rates for the specified index mapping and date range.
	 */
	public List<MarketDataInterestRate> getMarketDataInterestRateListForPeriod(MarketDataInterestRateIndexMapping mapping, Date fromDate, Date toDate);


	//////////////////////////////////////////////////////////////////////////// 
	//////               Exchange Rates Retrieval Methods              ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the latest exchange rate available between the specified currencies.
	 */
	public MarketDataExchangeRate getMarketDataExchangeRateLatest(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency);


	/**
	 * Returns the end-of-day exchange rate between the specified currencies on the specified date.
	 */
	public MarketDataExchangeRate getMarketDataExchangeRateForDate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date date);


	/**
	 * Returns historic end-of-day exchange rates between the specified currencies for the specified date range.
	 */
	public List<MarketDataExchangeRate> getMarketDataExchangeRateListForPeriod(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date fromDate, Date toDate);


	//////////////////////////////////////////////////////////////////////////// 
	//////              Data Field Value Retrieval Methods             ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the latest MarketDataValue objects for the specified properties of the {@link DataFieldValueCommand}.
	 * Errors will be appended to the command's list of errors if provided, otherwise exceptions will be thrown.
	 */
	public DataFieldValueResponse<T> getMarketDataValueLatest(DataFieldValueCommand command);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a List of MarketDataValue field objects for the given {@link DataFieldValueCommand}.
	 * If a specific value cannot be obtained, adds a corresponding error message to the specified errors list.
	 */
	public List<T> getMarketDataValueListHistorical(DataFieldValueCommand command, List<String> errors);

	////////////////////////////////////////////////////////////////////////////
	//////                 Data Field Subscription Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Processes a subscription request to subscribe to data updates according to the provided {@link DataFieldValueCommand}.
	 * The command specifies the {@link InvestmentSecurity}(s) and {@link MarketDataField}(s) to subscribe to.
	 * <p>
	 * Returns a list of {@link MarketDataSubscriptionSecurityData} for each security subscription of the command with a status.
	 */
	public List<MarketDataSubscriptionSecurityData> subscribeToMarketData(DataFieldValueCommand command);


	/**
	 * Processes a subscription request to unsubscribe to data updates according to the provided {@link DataFieldValueCommand}.
	 * The command specifies the {@link InvestmentSecurity}(s) and {@link MarketDataField}(s) to unsubscribe to.
	 * <p>
	 * Returns a list of {@link MarketDataSubscriptionSecurityData} for each security subscription of the command with a status.
	 */
	public List<MarketDataSubscriptionSecurityData> unsubscribeToMarketData(DataFieldValueCommand command);


	/**
	 * Returns a list of active security subscriptions following to the provided operation.
	 * <p>
	 * The {@link MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses} and {@link MarketDataUserIdentity}s will be populated for each active subscription.
	 */
	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation);
}
