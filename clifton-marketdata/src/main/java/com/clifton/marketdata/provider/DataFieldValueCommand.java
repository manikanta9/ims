package com.clifton.marketdata.provider;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;


/**
 * The DataFieldValueCommand class defines parameters for requesting market data field values.
 *
 * @author vgomelsky
 */
public class DataFieldValueCommand implements Iterable<Map.Entry<InvestmentSecurity, List<MarketDataField>>> {

	private final Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListMap;
	private final String marketSector;
	private final String pricingSource;
	private final String exchangeCode;
	private final InvestmentExchangeTypes exchangeCodeType;
	private final Date startDate;
	private final Date endDate;
	/**
	 * If not null, will suppress exception and append errors to this list.
	 * Otherwise exception will be thrown in case of an error.
	 */
	private final List<String> errors;
	/**
	 * Optional {@link MarketDataProviderOverrides} to use when making a request to target a specific source or property.
	 * For Bloomberg MarketData, we can direct requests to a TERMINAL or BPIPE using this override.
	 */
	private final MarketDataProviderOverrides providerOverride;

	/**
	 * Optional {@link MarketDataUserIdentity} to use when making a request to a {@link MarketDataProvider}.
	 * For Bloomberg B-Pipe requests, a valid user and client IP address are needed when making the request. The command should contain this data.
	 */
	private final MarketDataUserIdentity marketDataUserIdentity;

	/**
	 * If set, will override the default provider timeout
	 */
	private final Integer marketDataProviderTimeoutOverride;

	/**
	 * The number of securities to be requested at one time
	 */
	private final Integer marketDataProviderBatchSizeOverride;

	/**
	 * Turns off shutting down the terminal after number of errors is received
	 */

	private final Boolean consecutiveErrorCountDisabled;

	/**
	 * Turns off retrying a request on error
	 */
	private final Integer retryCountOverride;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataFieldValueCommand(DataFieldValueCommandBuilder builder) {
		this.securityToFieldListMap = builder.securityToFieldListMap;
		this.marketSector = builder.marketSector;
		this.pricingSource = builder.pricingSource;
		this.exchangeCode = builder.exchangeCode;
		this.exchangeCodeType = builder.exchangeCodeType;
		this.errors = builder.errors;
		this.providerOverride = builder.providerOverride;
		this.marketDataUserIdentity = builder.marketDataUserIdentity;
		this.startDate = builder.startDate;
		this.endDate = builder.endDate;
		this.marketDataProviderTimeoutOverride = builder.marketDataProviderTimeoutOverride;
		this.marketDataProviderBatchSizeOverride = builder.marketDataProviderBatchSizeOverride;
		this.consecutiveErrorCountDisabled = builder.consecutiveErrorCountDisabled;
		this.retryCountOverride = builder.retryCountOverride;
	}

	///////////////////////////////////////////////////////////////////////////
	/////////                Convenience Constructors                //////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a DataFieldValueCommand for the provided {@link MarketDataField} and {@link InvestmentSecurity}.
	 */
	public static DataFieldValueCommand ofFieldAndSecurity(MarketDataField dataField, InvestmentSecurity... investmentSecurity) {
		return ofFieldAndSecuritySuppressingErrors(dataField, null, investmentSecurity);
	}


	/**
	 * Creates a DataFieldValueCommand for the provided {@link MarketDataField} and {@link InvestmentSecurity}.
	 * If a list for adding errors is not null, errors will be added to the list to suppress thrown exceptions.
	 */
	public static DataFieldValueCommand ofFieldAndSecuritySuppressingErrors(MarketDataField dataField, List<String> errorList, InvestmentSecurity... investmentSecurity) {
		DataFieldValueCommandBuilder builder = DataFieldValueCommand.newBuilder()
				.setErrors(errorList);
		ArrayUtils.getStream(investmentSecurity)
				.forEach(security -> builder.addSecurity(security, dataField));
		return builder.build();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Iterator<Map.Entry<InvestmentSecurity, List<MarketDataField>>> iterator() {
		return this.securityToFieldListMap.entrySet().iterator();
	}


	public void forEach(BiConsumer<InvestmentSecurity, List<MarketDataField>> consumer) {
		this.securityToFieldListMap.forEach(consumer);
	}


	public List<MarketDataField> getDataFieldList(InvestmentSecurity security) {
		return this.securityToFieldListMap.get(security);
	}


	public List<InvestmentSecurity> getInvestmentSecurityList() {
		return new ArrayList<>(this.securityToFieldListMap.keySet());
	}


	public String getMarketSector() {
		return this.marketSector;
	}


	public String getPricingSource() {
		return this.pricingSource;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public String getExchangeCodeFromType(InvestmentSecurity security) {
		return this.exchangeCodeType != null ? this.exchangeCodeType.getSecurityExchangeCode(security) : null;
	}


	public List<String> getErrors() {
		return this.errors;
	}


	public boolean isSuppressErrors() {
		return this.errors != null;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public Integer getMarketDataProviderTimeoutOverride() {
		return this.marketDataProviderTimeoutOverride;
	}


	public Integer getMarketDataProviderBatchSizeOverride() {
		return this.marketDataProviderBatchSizeOverride;
	}


	public Boolean getConsecutiveErrorCountDisabled() {
		return this.consecutiveErrorCountDisabled;
	}


	public Integer getRetryCountOverride() {
		return this.retryCountOverride;
	}


	public MarketDataProviderOverrides getProviderOverride() {
		return this.providerOverride;
	}


	public boolean hasProviderOverride() {
		return Objects.nonNull(getProviderOverride());
	}


	public MarketDataUserIdentity getMarketDataUserIdentity() {
		return this.marketDataUserIdentity;
	}


	public boolean hasMarketDataUserIdentity() {
		return Objects.nonNull(getMarketDataUserIdentity());
	}


	public boolean isLatest() {
		return this.startDate == null && this.endDate == null;
	}


	public boolean isBatchSizeEnabled() {
		return getMarketDataProviderBatchSizeOverride() != null && MathUtils.isGreaterThan(getMarketDataProviderBatchSizeOverride(), 0);
	}


	public static DataFieldValueCommandBuilder newBuilder() {
		return new DataFieldValueCommandBuilder();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	public static class DataFieldValueCommandBuilder {

		/**
		 * Use concurrent map so securities and associated fields can be populated in parallel threads without issue.
		 */
		private final Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListMap = new ConcurrentHashMap<>();
		private String marketSector;
		private String pricingSource;
		private String exchangeCode;
		private InvestmentExchangeTypes exchangeCodeType;

		private Date startDate;
		private Date endDate;

		private List<String> errors;

		private MarketDataProviderOverrides providerOverride;
		private MarketDataUserIdentity marketDataUserIdentity;

		private Integer marketDataProviderTimeoutOverride;

		private Integer marketDataProviderBatchSizeOverride = 1;

		private Boolean consecutiveErrorCountDisabled = Boolean.FALSE;

		private Integer retryCountOverride;

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		private DataFieldValueCommandBuilder() {
		}


		public DataFieldValueCommandBuilder addSecurity(InvestmentSecurity security, MarketDataField... dataField) {
			return addSecurity(security, CollectionUtils.createList(dataField));
		}


		public DataFieldValueCommandBuilder addSecurity(InvestmentSecurity security, List<MarketDataField> dataFieldList) {
			if (security != null && !CollectionUtils.isEmpty(dataFieldList)) {
				this.securityToFieldListMap.put(security, dataFieldList);
			}
			return this;
		}


		public DataFieldValueCommandBuilder setMarketSector(String marketSector) {
			this.marketSector = marketSector;
			return this;
		}


		public DataFieldValueCommandBuilder setPricingSource(String pricingSource) {
			this.pricingSource = pricingSource;
			return this;
		}


		public DataFieldValueCommandBuilder setExchangeCode(String exchangeCode) {
			this.exchangeCode = exchangeCode;
			return this;
		}


		public DataFieldValueCommandBuilder setExchangeCodeType(InvestmentExchangeTypes exchangeCodeType) {
			this.exchangeCodeType = exchangeCodeType;
			return this;
		}


		public DataFieldValueCommandBuilder setErrors(List<String> errors) {
			this.errors = errors;
			return this;
		}


		public DataFieldValueCommandBuilder withProviderOverride(MarketDataProviderOverrides providerOverride) {
			this.providerOverride = providerOverride;
			return this;
		}


		public DataFieldValueCommandBuilder withMarketDataUserIdentity(MarketDataUserIdentity marketDataUserIdentity) {
			this.marketDataUserIdentity = marketDataUserIdentity;
			return this;
		}


		public DataFieldValueCommandBuilder setStartDate(Date date) {
			this.startDate = date;
			return this;
		}


		public DataFieldValueCommandBuilder setEndDate(Date date) {
			this.endDate = date;
			return this;
		}


		public DataFieldValueCommandBuilder setProviderTimeoutOverride(Integer timeout) {
			this.marketDataProviderTimeoutOverride = timeout;
			return this;
		}


		public DataFieldValueCommandBuilder setProviderBatchSizeOverride(Integer batchSize) {
			if (batchSize != null) {
				this.marketDataProviderBatchSizeOverride = batchSize;
			}
			return this;
		}


		public DataFieldValueCommandBuilder setConsecutiveErrorCountDisabled(Boolean disabled) {
			if (disabled != null) {
				this.consecutiveErrorCountDisabled = disabled;
			}
			return this;
		}


		public DataFieldValueCommandBuilder setRetryCountOverride(Integer numberOfAllowedRetries) {
			if (numberOfAllowedRetries != null) {
				this.retryCountOverride = numberOfAllowedRetries;
			}
			return this;
		}


		public DataFieldValueCommand build() {
			return new DataFieldValueCommand(this);
		}
	}
}
