package com.clifton.marketdata.provider.subscription;

import com.clifton.marketdata.provider.MarketDataUserIdentity;

import java.util.Map;
import java.util.Set;


/**
 * <code>MarketDataSubscriptionSecurityData</code> holds market data values for a subscription including:
 * the status, data type qualifier, field data, and user identities with access to the data.
 *
 * @author NickK
 */
public interface MarketDataSubscriptionSecurityData {

	/**
	 * Status values defining the status of the subscription for a security.
	 */
	public enum MarketDataSubscriptionStatuses {
		ACTIVE, // subscription is active
		PENDING, // subscription request is received and is being validated
		INACTIVE, // subscription has been unsubscribed
		FAILED, // subscription request failed
		TERMINATED // subscription was terminated by the provider
	}


	/**
	 * Returns the {@link MarketDataSubscriptionStatuses} of the subscription for the security of this object.
	 */
	public MarketDataSubscriptionStatuses getStatus();


	/**
	 * Returns an optional type qualifier for this subscription data object. The qualifier is a generic way
	 * of relaying data information from the underlying information.
	 * <p>
	 * For example, Bloomberg subscriptions will relay the subscription event message types.
	 */
	public String getDataTypeQualifier();


	/**
	 * Returns the set of {@link MarketDataUserIdentity}s entitled to view the data of this object.
	 */
	public Set<MarketDataUserIdentity> getUserIdentitySet();


	/**
	 * Returns the security symbol the data of this object relates to. This symbol is the external market data
	 * security symbol and may not link directly to an {@link com.clifton.investment.instrument.InvestmentSecurity}.
	 */
	public String getSecuritySymbol();


	/**
	 * Returns the available subscription security field data. The map of data may not contain values for each
	 * field subscribed to.
	 * <p>
	 * The map may be empty if the status is not {@link MarketDataSubscriptionStatuses#ACTIVE};
	 */
	public Map<String, Object> getSecurityDataValueMap();


	/**
	 * Returns true if an error is included in this object. Use {@link #getError()} to get the error.
	 */
	public boolean isErrorPresent();


	/**
	 * Returns the error that was caught while generating this subscription security data object. The value will be
	 * null if {@link #isErrorPresent()} returns false.
	 */
	public Throwable getError();
}
