package com.clifton.marketdata.provider.subscription;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;

import java.util.List;


/**
 * <code>MarketDataSubscriptionCommand</code> is a {@link BeanListCommand} that contains
 * additional details for processing market data subscription requests for a list of {@link InvestmentSecurity}s.
 *
 * @author NickK
 */
public class MarketDataSubscriptionCommand extends BeanListCommand<InvestmentSecurity> {

	/**
	 * If true, a subscription request will be created and process to subscribe to the list of securities.
	 * If false, the list of securities will be unsubscribed from market data updates.
	 */
	private boolean subscribe;

	/**
	 * If true, this command is to update the local cache only and not make requests to a marketdata provider.
	 * This flag is only applicable if subscribe is false.
	 */
	private boolean cacheOnly;

	/**
	 * The {@link com.clifton.security.user.SecurityUser} this subscription command is to execute on behalf of.
	 * A use for setting this is for administration of active subscriptions to remove active subscriptions.
	 */
	private SecurityUser securityUser;
	/**
	 * The client IP address to use for a subscription request. If left blank, the value is retrieved from either
	 * a user subscription from cache or the context of the current request. For the event of managing active
	 * subscriptions, the client IP is necessary for the impersonation of the user.
	 * <p>
	 * Client IP address is required with user ID for all Bloomberg B-Pipe requests.
	 */
	private String clientIpAddress;

	/**
	 * If true, the command is treated as a forceful action, which disregards local cached reference counts.
	 * Subscription requests generally only get made to the market data provider when reference counts are 1.
	 * When administrators clean up active subscriptions, we want to force the unsubscribe.
	 */
	private boolean force;

	/**
	 * Rather than pass in an array of {@link InvestmentSecurity} in the bean list, an array of security IDs can be provided.
	 */
	private Integer[] securityIds;

	///////////////////////////////////////////////////////////////////////////


	public static MarketDataSubscriptionCommand of(SecurityUser securityUser) {
		MarketDataSubscriptionCommand command = new MarketDataSubscriptionCommand();
		command.setSecurityUser(securityUser);
		return command;
	}


	@Override
	public String toString() {
		return "{securityUser: {" + getSecurityUser() + "}, subscribe: " + isSubscribe() + ", force: " + isForce() + ", securityList: {" + StringUtils.collectionToCommaDelimitedString(getBeanList()) + "}}";
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isSubscribe() {
		return this.subscribe;
	}


	public void setSubscribe(boolean subscribe) {
		this.subscribe = subscribe;
	}


	public boolean isCacheOnly() {
		return this.cacheOnly;
	}


	public void setCacheOnly(boolean cacheOnly) {
		this.cacheOnly = cacheOnly;
	}


	public MarketDataSubscriptionCommand withSubscribe(boolean subscribe) {
		setSubscribe(subscribe);
		return this;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public MarketDataSubscriptionCommand withUser(SecurityUser securityUser) {
		setSecurityUser(securityUser);
		return this;
	}


	public String getClientIpAddress() {
		return this.clientIpAddress;
	}


	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}


	public MarketDataSubscriptionCommand withClientIpAddress(String clientIpAddress) {
		setClientIpAddress(clientIpAddress);
		return this;
	}


	public MarketDataSubscriptionCommand withSecurityList(List<InvestmentSecurity> securityList) {
		setBeanList(securityList);
		return this;
	}


	public boolean isForce() {
		return this.force;
	}


	public void setForce(boolean force) {
		this.force = force;
	}


	public MarketDataSubscriptionCommand withForce(boolean force) {
		setForce(force);
		return this;
	}


	public Integer[] getSecurityIds() {
		return this.securityIds;
	}


	public void setSecurityIds(Integer[] securityIds) {
		this.securityIds = securityIds;
	}
}
