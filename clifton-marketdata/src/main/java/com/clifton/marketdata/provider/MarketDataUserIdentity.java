package com.clifton.marketdata.provider;

/**
 * <code>MarketDataUserIdentity</code> is an interface used to pass user data through market data service.
 * Some market data providers access external systems and use different user identities. This object allows
 * the external system identities back to the IMS {@link com.clifton.security.user.SecurityUser}.
 * <p>
 * An example of this object's usage is in making Bloomberg subscriptions and handling data events. The
 * MarketDataUserIdentity is used to link the Bloomberg user back to the IMS user.
 *
 * @author NickK
 */
public interface MarketDataUserIdentity {

	/**
	 * Returns the identity of the {@link com.clifton.security.user.SecurityUser} in order to
	 * link this identity to a system user.
	 */
	public Short getSecurityUserId();


	/**
	 * Returns the IP address associated with the user identity.
	 */
	public String getClientIpAddress();
}
