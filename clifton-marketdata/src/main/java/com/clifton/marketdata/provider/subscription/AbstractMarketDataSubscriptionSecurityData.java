package com.clifton.marketdata.provider.subscription;

import com.clifton.marketdata.provider.MarketDataUserIdentity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * <code>AbstractMarketDataSubscriptionSecurityData</code> defines a basic implementations for an {@link AbstractMarketDataSubscriptionSecurityData}.
 *
 * @author NickK
 */
public abstract class AbstractMarketDataSubscriptionSecurityData implements MarketDataSubscriptionSecurityData {

	private final String securitySymbol;
	/**
	 * The field data in a security subscription event
	 */
	private final Map<String, Object> fieldData = new HashMap<>();
	/**
	 * The set of users entitled to the security data.
	 */
	private Set<MarketDataUserIdentity> identitySet = new HashSet<>();
	private MarketDataSubscriptionStatuses status;
	private String dataTypeQualifier;
	private Throwable error;

	///////////////////////////////////////////////////////////////////////////


	public AbstractMarketDataSubscriptionSecurityData(String securitySymbol) {
		this.securitySymbol = securitySymbol;
	}


	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder("{security=").append(getSecuritySymbol())
				.append(", status=").append(getStatus())
				.append(", dataTypeQualifier=").append(getDataTypeQualifier())
				.append(", identities=").append(getUserIdentitySet())
				.append(", fieldData=").append(getSecurityDataValueMap());
		if (isErrorPresent()) {
			stringBuilder.append(", error=").append(getError().getMessage());
		}
		return stringBuilder.append("}").toString();
	}


	@Override
	public String getSecuritySymbol() {
		return this.securitySymbol;
	}


	@Override
	public Map<String, Object> getSecurityDataValueMap() {
		return this.fieldData;
	}


	@Override
	public Set<MarketDataUserIdentity> getUserIdentitySet() {
		return this.identitySet;
	}


	@Override
	public MarketDataSubscriptionStatuses getStatus() {
		return this.status;
	}


	@Override
	public String getDataTypeQualifier() {
		return this.dataTypeQualifier;
	}


	@Override
	public boolean isErrorPresent() {
		return getError() != null;
	}


	@Override
	public Throwable getError() {
		return this.error;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void addFieldData(String field, Object value) {
		this.fieldData.put(field, value);
	}


	public void addIdentity(MarketDataUserIdentity identity) {
		this.identitySet.add(identity);
	}


	public void setStatus(MarketDataSubscriptionStatuses status) {
		this.status = status;
	}


	public void setDataTypeQualifier(String dataTypeQualifier) {
		this.dataTypeQualifier = dataTypeQualifier;
	}


	public void setError(Throwable error) {
		this.error = error;
	}
}
