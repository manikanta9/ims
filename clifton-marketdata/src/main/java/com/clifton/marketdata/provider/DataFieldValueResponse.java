package com.clifton.marketdata.provider;

import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueGeneric;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;


/**
 * <code>DataFieldValueResponse</code> encapsulates result data of a request for {@link MarketDataValueGeneric}s. The
 * values can be extracted by {@link InvestmentSecurity} and {@link MarketDataField} since the request may have been
 * for multiple securities and/or fields.
 *
 * @author NickK
 */
public class DataFieldValueResponse<T extends MarketDataValueGeneric<?>> implements Iterable<Map.Entry<InvestmentSecurity, Map<MarketDataField, T>>> {

	private final Map<InvestmentSecurity, Map<MarketDataField, T>> securityDataFieldValueMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataFieldValueResponse(Map<InvestmentSecurity, Map<MarketDataField, T>> securityDataFieldValueMap) {
		this.securityDataFieldValueMap = (securityDataFieldValueMap != null) ? securityDataFieldValueMap : Collections.emptyMap();
	}


	/**
	 * Returns a non-null, empty DataFieldValueResponse.
	 */
	public static <T extends MarketDataValueGeneric<?>> DataFieldValueResponse<T> emptyResponse() {
		return createResponse(null);
	}


	/**
	 * Returns a DataFieldValueResponse containing security data from the provided data map.
	 */
	public static <T extends MarketDataValueGeneric<?>> DataFieldValueResponse<T> createResponse(Map<InvestmentSecurity, Map<MarketDataField, T>> securityDataFieldValueMap) {
		return new DataFieldValueResponse<>(securityDataFieldValueMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a map of {@link MarketDataField}s and the corresponding retrieved value for each for the {@link InvestmentSecurity} provided.
	 * The value for a field may be null.
	 */
	public Map<MarketDataField, T> getMarketDataValueMapForSecurity(InvestmentSecurity security) {
		if (security == null) {
			return Collections.emptyMap();
		}
		return CollectionUtils.getValue(this.securityDataFieldValueMap, security, Collections::emptyMap);
	}


	/**
	 * Returns a map of {@link MarketDataField}s and the corresponding retrieved value for each for an
	 * expected single {@link InvestmentSecurity} from the request.
	 * <p>
	 * The value for a field may be null.
	 *
	 * @throws IllegalStateException  if there are more than one security with values
	 * @throws NoSuchElementException if there is no security with values
	 */
	public Map<MarketDataField, T> getMarketDataValueMapForSingleSecurityStrict() {
		return CollectionUtils.getOnlyElementStrict(this.securityDataFieldValueMap.entrySet()).getValue();
	}


	/**
	 * Returns a list of market data field values from {@link #getMarketDataValueMapForSecurity(InvestmentSecurity)} with null values removed.
	 */
	public List<T> getMarketDataValueListForSecurity(InvestmentSecurity security) {
		Map<MarketDataField, T> valueMap = getMarketDataValueMapForSecurity(security);
		return valueMap.entrySet().stream().map(Map.Entry::getValue).filter(Objects::nonNull).collect(Collectors.toList());
	}


	/**
	 * Returns a list of market data field values from {@link #getMarketDataValueMapForSingleSecurityStrict()} with null values removed.
	 *
	 * @throws IllegalStateException  if there are more than one security with values
	 * @throws NoSuchElementException if there is no security with values
	 */
	public List<T> getMarketDataValueListForSingleSecurityStrict() {
		return getMarketDataValueMapForSingleSecurityStrict().entrySet().stream()
				.map(Map.Entry::getValue).filter(Objects::nonNull).collect(Collectors.toList());
	}


	/**
	 * Returns the market data value for the provided {@link InvestmentSecurity} and {@link MarketDataField}.
	 * The value may be null.
	 */
	public T getMarketDataValue(InvestmentSecurity security, MarketDataField field) {
		return getMarketDataValueMapForSecurity(security).get(field);
	}


	/**
	 * Returns the market data value for the single {@link InvestmentSecurity} and single {@link MarketDataField} from a request.
	 * The value may be null.
	 *
	 * @throws IllegalStateException  if there are more than one security with values
	 * @throws NoSuchElementException if there is no security with values
	 */
	public T getSingleMarketDataValueStrict() {
		List<T> valueList = getMarketDataValueListForSingleSecurityStrict();
		return valueList.isEmpty() ? null : valueList.get(0);
	}


	@Override
	public Iterator<Map.Entry<InvestmentSecurity, Map<MarketDataField, T>>> iterator() {
		return this.securityDataFieldValueMap.entrySet().iterator();
	}


	public void forEach(BiConsumer<InvestmentSecurity, Map<MarketDataField, T>> action) {
		Objects.requireNonNull(action);
		for (Map.Entry<InvestmentSecurity, Map<MarketDataField, T>> entry : this) {
			action.accept(entry.getKey(), entry.getValue());
		}
	}
}
