package com.clifton.marketdata.provider;


import com.clifton.core.util.validation.ValidationException;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>MarketDataProviderLocator</code> class is responsible for locating MarketDataProvider
 * implementation beans for specific data providers. It finds all Spring managed MarketDataProvider
 * implementations and registers them under corresponding names.
 *
 * @author vgomelsky
 */
@Component
@SuppressWarnings("rawtypes")
public class MarketDataProviderLocator implements InitializingBean, ApplicationContextAware {

	/**
	 * When MarketDataSource argument is not passed, use the following name.
	 */
	private String defaultDataSourceName = "Bloomberg";

	/**
	 * Market Data Source name to corresponding MarketDataProvider mapping.
	 */
	private final Map<String, MarketDataProvider> marketDataProviderMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		// get all beans registered in the context
		Map<String, MarketDataProvider> beanMap = getApplicationContext().getBeansOfType(MarketDataProvider.class);
		for (MarketDataProvider dataProvider : beanMap.values()) {
			if (getMarketDataProviderMap().containsKey(dataProvider.getMarketDataSourceName())) {
				throw new ValidationException("Cannot have multiple MarketaDataProvider instances for the same data source: " + dataProvider.getMarketDataSourceName());
			}
			getMarketDataProviderMap().put(dataProvider.getMarketDataSourceName(), dataProvider);
		}
	}


	/**
	 * Returns MarketDataProvider implementation for the specified data source.
	 */
	@SuppressWarnings("unchecked")
	public <T extends MarketDataValueGeneric<?>> MarketDataProvider<T> locate(MarketDataSource dataSource) {
		String dataSourceName = getDefaultDataSourceName();
		if (dataSource != null && dataSource.getName() != null) {
			dataSourceName = dataSource.getName();
		}

		MarketDataProvider dataProvider = getMarketDataProviderMap().get(dataSourceName);
		if (dataProvider == null) {
			throw new ValidationException("Cannot locate MarketDataProvider for: " + dataSourceName);
		}

		return dataProvider;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, MarketDataProvider> getMarketDataProviderMap() {
		return this.marketDataProviderMap;
	}


	public String getDefaultDataSourceName() {
		return this.defaultDataSourceName;
	}


	public void setDefaultDataSourceName(String defaultDataSourceName) {
		this.defaultDataSourceName = defaultDataSourceName;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
