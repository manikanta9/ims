package com.clifton.marketdata.provider.subscription;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.security.user.SecurityUser;

import java.util.List;


/**
 * <code>MarketDataActiveUserSubscriptionCommand</code> is a {@link BeanListCommand} that contains additional details for management of active market data subscriptions.
 *
 * @author NickK
 */
@NonPersistentObject
public class MarketDataActiveUserSubscriptionCommand extends BeanListCommand<MarketDataActiveUserSubscription> {

	/**
	 * Flag to process all local {@link MarketDataActiveUserSubscription}s. The {@link #getBeanList()} provided is ignored if this is true.
	 */
	private boolean processAll;
	/**
	 * User to process active active subscriptions for.
	 */
	private SecurityUser securityUser;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static MarketDataActiveUserSubscriptionCommand of(List<MarketDataActiveUserSubscription> activeUserSubscriptionList) {
		MarketDataActiveUserSubscriptionCommand command = new MarketDataActiveUserSubscriptionCommand();
		command.setBeanList(activeUserSubscriptionList);
		return command;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isProcessAll() {
		return this.processAll;
	}


	public void setProcessAll(boolean processAll) {
		this.processAll = processAll;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}
}
