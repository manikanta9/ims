package com.clifton.marketdata.provider.subscription;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.compare.CompareUtils;

import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * <code>MarketDataSubscriptionDataValue</code> represents a realtime data value that can be concurrently updated and viewed.
 * This object contains the name associated with the data value, the value, its data type, and its updated time.
 *
 * @author NickK
 */
@NonPersistentObject
public class MarketDataSubscriptionDataValue {

	/**
	 * The name of the external subscription data field this value is associated with.
	 * The name is for external field names because {@link com.clifton.marketdata.field.MarketDataField#externalFieldName}
	 * may be composed of a comma separated list of values and a value expression for transformation, and the market data
	 * field name may be verbose.
	 * <p>
	 * Examples: Most security subscriptions are interested in BID, ASK, and LAST_TRADE values. The names for these
	 * external fields are: "Bid Price - Subscription," "Ask Price - Subscription," and "Last Price - Subscription" respectively.
	 *
	 * @see com.clifton.marketdata.field.MarketDataField#externalFieldName
	 */
	private final String fieldName;
	private final DataTypeNames dataTypeName;
	private Object value;
	/**
	 * Tracks whether the value has been updates since last retrieved
	 */
	private final AtomicBoolean updated = new AtomicBoolean();
	private LocalDateTime updateDateTime;

	///////////////////////////////////////////////////////////////////////////


	public MarketDataSubscriptionDataValue(String fieldName, DataTypeNames dataTypeName) {
		this.fieldName = fieldName;
		this.dataTypeName = dataTypeName;
	}


	public MarketDataSubscriptionDataValue(String name, DataTypeNames dataTypeName, Object value) {
		this(name, dataTypeName);
		setValue(value);
	}


	@Override
	public String toString() {
		return "{fieldName=" + getFieldName() + ",dataType=" + getDataTypeName() + "}";
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getFieldName() {
		return this.fieldName;
	}


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}


	public boolean isUpdated() {
		return this.updated.get();
	}


	@ValueChangingSetter
	public void setUpdated(boolean updated) {
		this.updated.set(updated);
	}


	public LocalDateTime getUpdateDateTime() {
		return this.updateDateTime;
	}


	public void setUpdateDateTime(LocalDateTime updateDateTime) {
		this.updateDateTime = updateDateTime;
	}


	public Object getValue() {
		setUpdated(false);
		return this.value;
	}


	@ValueChangingSetter
	public void setValue(Object value) {
		if (!CompareUtils.isEqual(this.value, value)) {
			setUpdateDateTime(LocalDateTime.now());
			this.value = value;
			setUpdated(true);
		}
	}
}
