package com.clifton.marketdata.provider.subscription.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.specificity.InvestmentSpecificityEntry;
import com.clifton.investment.specificity.InvestmentSpecificityUtilHandler;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.specificity.MarketDataFieldSpecificityMapping;
import com.clifton.marketdata.live.theoretical.MarketDataTheoreticalValueService;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.provider.MarketDataUserIdentity;
import com.clifton.marketdata.provider.subscription.MarketDataActiveUserSubscription;
import com.clifton.marketdata.provider.subscription.MarketDataActiveUserSubscriptionCommand;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionCommand;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValue;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValueSet;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionService;
import com.clifton.marketdata.provider.subscription.event.MarketDataSubscriptionDataEvent;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.websocket.WebSocketHandler;
import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.Match;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * <code>MarketDataSubscriptionUtilHandlerImpl</code> implements {@link MarketDataSubscriptionUtilHandler}
 * managing user subscriptions. Subscription metadata is maintained in local caches to maintain consistency and avoid database access.
 * This implementation is an {@link com.clifton.core.util.event.EventListener} for {@link MarketDataSubscriptionDataEvent}s, and uses
 * the local caches to transform the events into {@link MarketDataSubscriptionDataValue}s that can be sent to users.
 * <p>
 * Processing of subscription requests are done with multiple threads to speed up requests for multiple securities. Some securities, such as theoretically priced securities
 * (e.g. FLEX and OTC Options), use a reference security to pull in reference market data. Using multiple threads avoids the sequential delays of using a single thread.
 * Since multiple threads are used, annotating the methods of this component with {@link Transactional} is not necessary as the resulting transaction is tied to a single
 * thread and tasks submitted to a thread pool would have to start a new transaction anyway.
 *
 * @author NickK
 */
@Component
public class MarketDataSubscriptionUtilHandlerImpl extends BaseEventListener<MarketDataSubscriptionDataEvent> implements MarketDataSubscriptionUtilHandler {

	/**
	 * A map containing user subscription specific data. The key is a SecurityUser's ID and the value contains active subscription field and value data.
	 * <p>
	 * This map is not a traditional cache because it should unsubscribe all entries upon clearing. If we create a custom cache, then we would have a circular reference between it and this item.
	 * <p>
	 * This object reference could be static, but we are relying on the parent bean's singleton status.
	 *
	 * @see MarketDataUserSubscription
	 */
	private final Map<Short, MarketDataUserSubscription> userSubscriptionMap = new ConcurrentHashMap<>();
	/**
	 * Executor service for executing {@link CompletableFuture}s for this class to avoid overwhelming the default Java ForkJoinPool.
	 * The executor is used to allow subscription commands to be populated by parallel threads. The process of populating the command
	 * looks up reference market data in some cases (e.g. FlEX Options) and using parallel threads can speed up the request.
	 * <p>
	 * As a ForkJoinPool, this executor can grow as needed and should self-heal blocking when one task spawns other tasks in the same queue.
	 * ForkJoinPool has other benefits that can improve performance in some cases with the work stealing logic.
	 * <p>
	 * This object is a Spring managed prototype object that will be cleaned up on context shutdown.
	 */
	private ForkJoinPool forkJoinPool;

	private ContextHandler contextHandler;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler;

	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataTheoreticalValueService marketDataTheoreticalValueService;

	private SecurityImpersonationHandler securityImpersonationHandler;
	private SecurityUserService securityUserService;

	private TemplateConverter templateConverter;

	private WebSocketHandler webSocketHandler;

	///////////////////////////////////////////////////////////////////////////
	/////////////          Event Listener Methods               ///////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return MarketDataSubscriptionDataEvent.EVENT_NAME;
	}


	/**
	 * The subscription event contains a set of {@link MarketDataUserIdentity}s and a map of subscription data (external field name to value).
	 * Each event is processed for each user so the information can be sent to the user's client.
	 *
	 * @param event The subscription event that has been fired.
	 */
	@Override
	public void onEvent(MarketDataSubscriptionDataEvent event) {
		MarketDataSubscriptionSecurityData subscriptionSecurityData = event.getTarget();

		if (subscriptionSecurityData.isErrorPresent()) {
			Throwable error = subscriptionSecurityData.getError();
			LogUtils.warn(LogCommand.ofMessage(getClass(), "Received error in subscription response (", error.getMessage(), "):", subscriptionSecurityData));
		}

		if (subscriptionSecurityData.getStatus() == null) {
			LogUtils.warn(LogCommand.ofMessage(getClass(), "Received subscription response with null status:", subscriptionSecurityData));
			return;
		}

		// Flag used to determine if we should resubscribe to unknown events (e.g. cache not populated on restart). Any other status is treated as a termination.
		boolean activeSubscriptionStatus = subscriptionSecurityData.getStatus() == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE;

		// The following are used to sync the local cache with active subscriptions with the provider.
		// The cache can be out of sync by user(s) and/or security.
		Map<Short, String> syncUserIdToClientIpMap = new HashMap<>();
		Integer investmentSecurityId = null;

		// Populate the user subscription data value map for each user referenced in the event
		String marketDataSecuritySymbol = subscriptionSecurityData.getSecuritySymbol();
		for (MarketDataUserIdentity marketDataUserIdentity : subscriptionSecurityData.getUserIdentitySet()) {
			Short securityUserId = marketDataUserIdentity.getSecurityUserId();
			MarketDataUserSubscription userSubscription = getUserSubscriptionMap().get(securityUserId);
			if (userSubscription == null) {
				if (activeSubscriptionStatus) {
					// not found user subscription in cache for active provider subscription; resubscribe to sync cache
					syncUserIdToClientIpMap.put(securityUserId, marketDataUserIdentity.getClientIpAddress());
				}
			}
			else {
				MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference securityReference = userSubscription.getSubscriptionSecurityReference(marketDataSecuritySymbol);
				if (securityReference == null) {
					if (activeSubscriptionStatus) {
						// not found user subscription in cache for active provider subscription; resubscribe to sync cache
						syncUserIdToClientIpMap.put(securityUserId, marketDataUserIdentity.getClientIpAddress());
					}
				}
				else {
					securityReference.updateStatus(subscriptionSecurityData.getStatus());

					if (investmentSecurityId == null) {
						investmentSecurityId = securityReference.getInvestmentSecurityId();
					}

					if (!activeSubscriptionStatus && securityReference.getUsageCount() > 0) {
						// user subscription is found and subscription is terminated at the provider without on user request; unsubscribe to sync cache if not manually unsubscribe
						syncUserIdToClientIpMap.put(securityUserId, marketDataUserIdentity.getClientIpAddress());
					}

					MarketDataSubscriptionDataValueSet dataValueSet = userSubscription.processSubscriptionData(
							investmentSecurityId,
							marketDataSecuritySymbol,
							subscriptionSecurityData.getSecurityDataValueMap());
					if (!dataValueSet.isEmpty()) {
						JsonView<Map<Integer, Collection<MarketDataSubscriptionDataValue>>> dataValueSetView = JsonView.with(dataValueSet.getSecurityDataValueMap())
								.onClass(MarketDataSubscriptionDataValue.class, Match.match()
										.exclude("*")
										.include("fieldName", "value"));
						getWebSocketHandler().sendMessageToUser(userSubscription.getUser(), MarketDataSubscriptionService.CHANNEL_USER_TOPIC_MARKET_DATA, dataValueSetView);
					}
				}
			}
		}

		if (!syncUserIdToClientIpMap.isEmpty()) {
			// Use activeSubscriptionStatus for both conditions because it will be true for active (resubscribe) and false for terminated (unsubscribe)
			syncProviderMarketDataActiveUserSubscriptionListWithLocalCacheUponEvent(investmentSecurityId, subscriptionSecurityData.getSecuritySymbol(), syncUserIdToClientIpMap, activeSubscriptionStatus);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	////////////         Subscription Util Handler Methods        /////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void processMarketDataSubscriptionCommand(MarketDataSubscriptionCommand command) {
		if (CollectionUtils.isEmpty(command.getBeanList())) {
			return;
		}

		MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
		DataFieldValueCommand subscriptionCommand = getSubscriptionDataFieldCommandForUserSubscriptionCommand(command, bloombergDataSource);

		MarketDataProvider<?> marketDataProvider = getMarketDataProviderLocator().locate(bloombergDataSource);
		List<MarketDataSubscriptionSecurityData> subscriptionResultList;
		try {
			subscriptionResultList = command.isSubscribe()
					? marketDataProvider.subscribeToMarketData(subscriptionCommand)
					: marketDataProvider.unsubscribeToMarketData(subscriptionCommand);
		}
		catch (RuntimeException e) {
			// unlock security subscriptions for the request
			if (command.isSubscribe()) {
				// reverse the request command and process it to clean up the local caches
				command.setSubscribe(!command.isSubscribe());
			}
			updateSubscriptionDataFieldCacheForUserSubscriptionCommand(command);
			throw e;
		}

		// List of any errors for the subscription request that can be returned to the user. In most cases the immediate status' will be PENDING
		List<MarketDataSubscriptionSecurityData> subscriptionErrorList = new ArrayList<>();
		// Each request is populated with the user so we can get it from the command or the security data response
		Short userId = command.getSecurityUser() != null ? command.getSecurityUser().getId() : null;
		for (MarketDataSubscriptionSecurityData securityData : CollectionUtils.getIterable(subscriptionResultList)) {
			if (securityData.isErrorPresent()) {
				subscriptionErrorList.add(securityData);
			}

			if (userId == null) {
				// get the user from the securityData response, which should always be present
				MarketDataUserIdentity userIdentity = CollectionUtils.getFirstElement(securityData.getUserIdentitySet());
				if (userIdentity != null) {
					userId = userIdentity.getSecurityUserId();
				}
			}

			// If any subscriptions are processed by the provider, we need to update the usage reference to release the request lock
			// the request lock is handled here and/or by the asynchronous event handling
			Optional.ofNullable(userId)
					.map(id -> getUserSubscriptionMap().get(id))
					.map(userSubscription -> userSubscription.getSubscriptionSecurityReference(securityData.getSecuritySymbol()))
					.ifPresent(securityReference -> securityReference.updateStatus(securityData.getStatus()));
		}

		if (command.isSubscribe()) {
			// Throw an exception displaying any subscriptions that fail. Doing this should cause the UI to unsubscribe to all securities.
			if (!CollectionUtils.isEmpty(subscriptionErrorList)) {
				StringBuilder errorMessage = new StringBuilder("Subscription(s) failed: \n");
				subscriptionErrorList.forEach(securityData -> errorMessage.append(String.format("\tSecurity: %s: %s%n", securityData.getSecuritySymbol(), securityData.getError().getMessage())));
				throw new RuntimeException(errorMessage.toString());
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	////////////              Management APIs                 /////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation) {
		ValidationUtils.assertTrue(subscriptionOperation.isManagement(), "Only able to execute Market Date Subscription Management Operations");
		MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
		MarketDataProvider<?> marketDataProvider = getMarketDataProviderLocator().locate(bloombergDataSource);
		return marketDataProvider.processMarketDataSubscriptionOperation(subscriptionOperation);
	}


	@Override
	public List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionList() {
		return doGetMarketDataActiveUserSubscriptionList(false);
	}


	@Override
	public void unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand) {
		if (activeUserSubscriptionCommand == null) {
			return;
		}

		boolean cleanCache = false;
		List<MarketDataActiveUserSubscription> activeLocalSubscriptionList = activeUserSubscriptionCommand.getBeanList();
		if (activeUserSubscriptionCommand.isProcessAll()) {
			cleanCache = true;
			activeLocalSubscriptionList = getMarketDataActiveUserSubscriptionListFromLocalCache();
		}
		else if (activeUserSubscriptionCommand.getSecurityUser() != null) {
			cleanCache = true;
			activeLocalSubscriptionList = getMarketDataActiveUserSubscriptionListFromLocalCacheWithFiltering(activeUserSubscriptionCommand.getSecurityUser(), null);
		}

		if (cleanCache) {
			Iterator<Map.Entry<Short, MarketDataUserSubscription>> userSubscriptionEntryIterator = getUserSubscriptionMap().entrySet().iterator();
			while (userSubscriptionEntryIterator.hasNext()) {
				Map.Entry<Short, MarketDataUserSubscription> userSubscriptionEntry = userSubscriptionEntryIterator.next();
				if (!userSubscriptionEntry.getValue().isInactive()) {
					userSubscriptionEntry.getValue().setInactive();
				}
				userSubscriptionEntryIterator.remove();
			}
		}

		if (!CollectionUtils.isEmpty(activeLocalSubscriptionList)) {
			processMarketDataActiveUserSubscriptionList(activeLocalSubscriptionList, false, true);
		}
	}


	@Override
	public void unsubscribeMarketDataActiveUserSubscriptionListAll() {
		MarketDataActiveUserSubscriptionCommand command = new MarketDataActiveUserSubscriptionCommand();
		command.setProcessAll(true);
		unsubscribeMarketDataActiveUserSubscriptionList(command);
	}


	@Override
	public void unlockMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand) {
		if (activeUserSubscriptionCommand == null) {
			return;
		}

		List<MarketDataActiveUserSubscription> activeUserSubscriptionList = activeUserSubscriptionCommand.getBeanList();
		if (activeUserSubscriptionCommand.isProcessAll()) {
			activeUserSubscriptionList = getMarketDataActiveUserSubscriptionListFromLocalCache();
		}
		else if (activeUserSubscriptionCommand.getSecurityUser() != null) {
			activeUserSubscriptionList = getMarketDataActiveUserSubscriptionListFromLocalCacheWithFiltering(activeUserSubscriptionCommand.getSecurityUser(), null);
		}

		if (CollectionUtils.isEmpty(activeUserSubscriptionList)) {
			return;
		}

		MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
		// local cache for external market data security symbols by security ID to prevent numerous market data field mapping lookups
		Map<Integer, String> investmentSecurityIdToExternalSymbolMap = new HashMap<>();

		CollectionUtils.getStream(activeUserSubscriptionList)
				.filter(subscription -> Objects.nonNull(subscription.getSecurity()))
				.forEach(subscription -> {
					String marketDataSecuritySymbol = investmentSecurityIdToExternalSymbolMap.computeIfAbsent(subscription.getSecurity().getId(), key -> getMarketDataSecuritySymbol(subscription.getSecurity(), bloombergDataSource));
					Optional.ofNullable(getUserSubscriptionMap().get(subscription.getSecurityUser().getId()))
							.map(userSubscription -> userSubscription.getSubscriptionSecurityReference(marketDataSecuritySymbol))
							.ifPresent(MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference::unlock);
				});
	}


	/**
	 * Shutdown hook in Spring prior to bean destroy to unsubscribe active subscriptions in local cache and to clean up resources.
	 */
	@PreDestroy
	public void shutdownHook() {
		unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand.of(doGetMarketDataActiveUserSubscriptionList(true)));
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////                 Helper Methods                ///////////////
	///////////////////////////////////////////////////////////////////////////


	private SecurityUser getRequestingUser() {
		return (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
	}


	private SecurityUser getUserForSubscription(MarketDataSubscriptionCommand command) {
		SecurityUser subscriptionUser = command.getSecurityUser() == null ? getRequestingUser() : command.getSecurityUser();
		ValidationUtils.assertNotNull(subscriptionUser, "Unable to determine the user to process a subscription request as");
		return subscriptionUser;
	}


	/**
	 * Updates the local subscription cache for the provided command without making any requests to the provider for subscription syncing.
	 * Cache is only updated if the command is to unsubscribe, so the cache references are only decreased and not increased.
	 */
	private void updateSubscriptionDataFieldCacheForUserSubscriptionCommand(MarketDataSubscriptionCommand command) {
		if (!command.isSubscribe()) {
			MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
			command.setCacheOnly(true);
			getSubscriptionDataFieldCommandForUserSubscriptionCommand(command, bloombergDataSource);
		}
	}


	/**
	 * Returns a {@link DataFieldValueCommand} for a subscription request including the requested list of securities.
	 */
	private DataFieldValueCommand getSubscriptionDataFieldCommandForUserSubscriptionCommand(MarketDataSubscriptionCommand command, MarketDataSource marketDataSource) {
		SecurityUser subscriptionUser = getUserForSubscription(command);
		String clientIp = command.getClientIpAddress() != null ? command.getClientIpAddress() : (String) getContextHandler().getBean(Context.REQUEST_CLIENT_ADDRESS);

		MarketDataUserSubscription userSubscription = CollectionUtils.getValue(getUserSubscriptionMap(), subscriptionUser.getId(), () -> new MarketDataUserSubscription(subscriptionUser, clientIp, getTemplateConverter()));

		DataFieldValueCommand.DataFieldValueCommandBuilder commandBuilder = DataFieldValueCommand.newBuilder()
				.withMarketDataUserIdentity(new MarketDataUserIdentity() {
					@Override
					public Short getSecurityUserId() {
						return subscriptionUser.getId();
					}


					@Override
					public String getClientIpAddress() {
						// Use the cached subscription IP address if available rather than the one from the context.
						// The one from the context may be incorrect if the request was run on behalf of another user (e.g. management of subscriptions).
						if (userSubscription.getClientIpAddress() != null && !CompareUtils.isEqual(clientIp, userSubscription.getClientIpAddress())) {
							// use the cached subscription address if available and not the same
							return userSubscription.getClientIpAddress();
						}
						return clientIp;
					}
				});

		// items used for tracking security and field mappings
		MarketDataSubscriptionOperations operation;
		// predicate to determine if a security is able to be included in a subscription request depending on its reference count
		// e.g. we want to avoid unsubscribing from an Option's underlying security until all Options using it are unsubscribed
		IntPredicate securityReferenceCountOperationPredicate;
		// bi-consumer to add or remove theoretical pricing sources to/from a user subscription based on the requested subscription operation.
		BiConsumer<String, MarketDataOptionTheoreticalValueSource> userSubscriptionTheoreticalValueOperation;
		// bi-consumer to add or remove subscription security fields to/from a user subscription based on the requested subscription operation.
		BiConsumer<String, MarketDataField> userSubscriptionFieldOperation;
		if (command.isSubscribe()) {
			operation = MarketDataSubscriptionOperations.SUBSCRIBE;
			securityReferenceCountOperationPredicate = referenceCount -> referenceCount == 1; // only subscribe if reference count is 1
			userSubscriptionTheoreticalValueOperation = userSubscription::registerMarketDataTheoreticalValueSource;
			userSubscriptionFieldOperation = userSubscription::registerMarketDataField;
		}
		else {
			operation = MarketDataSubscriptionOperations.UNSUBSCRIBE;
			securityReferenceCountOperationPredicate = referenceCount -> referenceCount < 1; // only unsubscribe if reference count is < 1
			userSubscriptionTheoreticalValueOperation = userSubscription::deregisterMarketDataTheoreticalValueSource;
			userSubscriptionFieldOperation = userSubscription::deregisterMarketDataField;
		}

		boolean cacheOnlyOperation = command.isCacheOnly() && !command.isSubscribe();
		Date currentDay = DateUtils.clearTime(new Date());
		// unique identifier for the request used in usage reference handling
		UUID uuid = UUID.randomUUID();
		// local instrument field list map to avoid unnecessary db hits
		Map<InvestmentInstrument, List<MarketDataField>> instrumentFieldListMap = new HashMap<>();
		// local cache for external market data security symbols by security ID to prevent numerous market data field mapping lookups
		Map<Integer, String> investmentSecurityIdToExternalSymbolMap = new HashMap<>();
		Function<InvestmentSecurity, String> securityIdToMarketDataSymbolFunction = security ->
				investmentSecurityIdToExternalSymbolMap.computeIfAbsent(security.getId(), key -> getMarketDataSecuritySymbol(security, marketDataSource));

		Predicate<InvestmentSecurity> securityInclusionPredicate = security -> {
			MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference securityReference = userSubscription.getSubscriptionSecurityReference(securityIdToMarketDataSymbolFunction.apply(security));
			return securityReference != null && securityReference.isActive();
		};
		List<SubscriptionSecurity> subscriptionSecurityList = getSubscriptionSecurityList(command.getBeanList(), currentDay, (operation == MarketDataSubscriptionOperations.UNSUBSCRIBE), securityInclusionPredicate);
		// Collection of usage references that obtained a lock and are in the subscription command. Used to reset reference state upon errors.
		Set<MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference> usageReferenceSetInSubscriptionCommand = ConcurrentHashMap.newKeySet();
		// Collection of usage references that obtained a lock and never made it into the subscription command because it only affected the reference count; the reference state needs to be reset to active.
		Set<MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference> usageReferenceSetToReactivate = ConcurrentHashMap.newKeySet();
		// Collection of usage references that could not obtain a lock
		Set<MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference> usageReferenceSetWithNoLock = ConcurrentHashMap.newKeySet();

		/*
		Iterate all subscription securities to process each security in the request. Each subscription security can result in one or more investment securities
		to subscribe to (e.g. options -> the option and the underlying). The reference count of each unique investment security is calculated and added, as necessary,
		to the local security subscription cache and market data subscription command for the requesting user. Reference counts are tracked to avoid numerous subscription
		requests and premature subscription termination requests to the underlying market data subscription provider.

		Processing the request is done across multiple threads to speed up the total request. Theoretically priced securities may have to look up market data and spreading
		the processing of these securities across numerous threads will speed up the overall request. Use the local pool instead of the default common pool
		 */
		Function<SubscriptionSecurity, Collection<Callable<Object>>> subscriptionSecurityToCallableListFunction = subscriptionSecurity ->
				CollectionUtils.getConverted(subscriptionSecurity.getSecuritySetToSubscribeTo(), investmentSecurity -> Executors.callable(() -> {
					String marketDataSymbol;
					try {
						marketDataSymbol = securityIdToMarketDataSymbolFunction.apply(investmentSecurity);
					}
					catch (Exception e) {
						LogUtils.warn(getClass(), "Attempt to " + operation + " to security {" + investmentSecurity + "} failed:\n" + ExceptionUtils.getDetailedMessage(e));
						return;
					}

					MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference usageReference = userSubscription.computeSubscriptionSecurityReferenceIfAbsent(marketDataSymbol, investmentSecurity);

					if (!cacheOnlyOperation && (usageReferenceSetWithNoLock.contains(usageReference) || !usageReference.registerUsage(uuid, operation))) {
						usageReferenceSetWithNoLock.add(usageReference);
						return;
					}

					// Add theoretical value source if applicable whether first usage or subsequent (reference count > 1).
					// Theoretically priced securities may use the same reference security, or a security already subscribed to.
					if (subscriptionSecurity.isTheoreticallyPriced() && subscriptionSecurity.hasReferenceSecurity()) {
						Map<String, Object> contextOverrideMap = new HashMap<>();
						contextOverrideMap.put(Context.REQUEST_CLIENT_ADDRESS, ObjectUtils.coalesce(userSubscription.getClientIpAddress(), clientIp));
						MarketDataOptionTheoreticalValueSource theoreticalValueSource = getSecurityImpersonationHandler().runAsSecurityUserAndReturn(subscriptionUser, () -> createMarketDataTheoreticalValueSource(subscriptionSecurity, currentDay), contextOverrideMap);
						if (theoreticalValueSource != null) {
							userSubscriptionTheoreticalValueOperation.accept(marketDataSymbol, theoreticalValueSource);
						}
					}

					// Update the security usage cache for the provided security information and user.
					int securityReferenceCount = usageReference.getUsageCount();
					if (!(securityReferenceCountOperationPredicate.test(securityReferenceCount) || command.isForce() || cacheOnlyOperation)) {
						usageReferenceSetToReactivate.add(usageReference);
						return;
					}

					try {
						List<MarketDataField> marketDataFieldList = getSubscriptionMarketDataFieldListForSecurity(instrumentFieldListMap, investmentSecurity, marketDataSource);
						commandBuilder.addSecurity(investmentSecurity, marketDataFieldList);
						usageReferenceSetInSubscriptionCommand.add(usageReference);

						// Update field mappings for external market data names to MarketDataField on the user subscription
						for (MarketDataField marketDataField : CollectionUtils.getIterable(marketDataFieldList)) {
							userSubscriptionFieldOperation.accept(marketDataSymbol, marketDataField);
						}
					}
					catch (Exception e) {
						// update usage reference to unlock it
						usageReference.updateStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.FAILED);
						LogUtils.warn(getClass(), "Attempt to " + operation + " to security {" + investmentSecurity + "} failed:\n" + ExceptionUtils.getDetailedMessage(e));
					}
					finally {
						if (cacheOnlyOperation || securityReferenceCount < 1) {
							// If usage is reduced to 0, ensure the subscription is inactive for the user. This double check if to prevent lingering subscriptions on timeouts.
							userSubscription.setSubscriptionInactive(marketDataSymbol);
						}
					}
				}));
		boolean invalidateSubscriptionCommandUsageReferences = true;
		try {
			List<Callable<Object>> securitySubscriptionCallableList = CollectionUtils.getConvertedFlattened(subscriptionSecurityList, subscriptionSecurityToCallableListFunction);
			List<Future<Object>> processedTaskList = getForkJoinPool().invokeAll(securitySubscriptionCallableList, 10, TimeUnit.SECONDS);
			boolean taskCanceled = isFutureCanceled(processedTaskList);
			if (taskCanceled || !usageReferenceSetWithNoLock.isEmpty()) {
				String securitySymbolsWithNoLock = usageReferenceSetWithNoLock.isEmpty() ? "" : usageReferenceSetWithNoLock.stream()
						.map(MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference::getExternalSecurityName)
						.collect(Collectors.joining("\n", " Securities that failed to get locks:\n", ""));
				throw new RuntimeException(operation + " timed out while processing the list of securities in the request. Please try again. If the problem continues, active subscriptions may have to be managed." + securitySymbolsWithNoLock);
			}
			invalidateSubscriptionCommandUsageReferences = false;
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		finally {
			// Reactivate usage references that had reference count updates only to remove any obtained locks.
			usageReferenceSetToReactivate.forEach(usageReference -> usageReference.updateStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE));
			if (invalidateSubscriptionCommandUsageReferences) {
				// If an exception or lock timeout occurs on subscription request, the securities added to the command must be set to inactive to clear the usage reference and remove locks.
				usageReferenceSetInSubscriptionCommand.forEach(usageReference -> usageReference.updateStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.INACTIVE));
			}
		}

		return commandBuilder.build();
	}


	/**
	 * Returns a {@link MarketDataOptionTheoreticalValueSource} for the provided {@link SubscriptionSecurity} if it supports theoretical pricing, otherwise returns null.
	 */
	private MarketDataOptionTheoreticalValueSource createMarketDataTheoreticalValueSource(SubscriptionSecurity subscriptionSecurity, Date date) {
		if (subscriptionSecurity.isTheoreticallyPriced()) {
			InvestmentSecurity security = subscriptionSecurity.getSecurity();
			MarketDataOptionTheoreticalValueSource theoreticalValueSource = new MarketDataOptionTheoreticalValueSource(security, getMarketDataTheoreticalValueService());
			if (InvestmentUtils.isSecurityActiveOn(security, date == null ? new Date() : date)) {
				// getting the price will populate the theoretical value source with defaults.
				Optional.ofNullable(getMarketDataTheoreticalValueService().getMarketDataOptionTheoreticalPrice(theoreticalValueSource))
						.ifPresent(value -> theoreticalValueSource.setPrice(value.asNumericValue()));
			}
			return theoreticalValueSource;
		}
		return null;
	}


	/**
	 * Returns the external market data security symbol for the provided {@link InvestmentSecurity} and {@link MarketDataSource}.
	 */
	private String getMarketDataSecuritySymbol(InvestmentSecurity security, MarketDataSource marketDataSource) {
		MarketDataFieldMapping fieldMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), marketDataSource);
		ValidationUtils.assertNotNull(fieldMapping, "Unable to find Market Data Field Mapping for Security [" + security + "]" + (marketDataSource != null ? " and Market Data Source [" + marketDataSource + "]" : ""));
		StringBuilder marketDataSymbolName = new StringBuilder(security.getSymbol().toUpperCase());
		if (fieldMapping.getExchangeCodeType() != null) {
			marketDataSymbolName.append(' ').append(fieldMapping.getExchangeCodeType().getSecurityExchangeCode(security));
		}
		return marketDataSymbolName.append(' ').append(fieldMapping.getMarketSector().getName()).toString();
	}


	/**
	 * Returns the list of {@link MarketDataField}s for the provided {@link InvestmentSecurity} and {@link MarketDataSource}.
	 * The list will be returned from the provided cache if there is a mapping, otherwise, the list will be looked up, added to the cache, and returned.
	 */
	private List<MarketDataField> getSubscriptionMarketDataFieldListForSecurity(Map<InvestmentInstrument, List<MarketDataField>> instrumentToFieldListMap, InvestmentSecurity security, MarketDataSource marketDataSource) {
		return CollectionUtils.getValue(instrumentToFieldListMap, security.getInstrument(), () -> {
			List<InvestmentSpecificityEntry> specificityEntryList = getInvestmentSpecificityUtilHandler().getInvestmentSpecificityEntryListForSecurity(MarketDataFieldSpecificityMapping.MARKET_DATA_FIELD_MAPPING_SPECIFICITY_DEFINITION_NAME, security);
			return CollectionUtils.getStream(specificityEntryList)
					.filter(entry -> MarketDataFieldSpecificityMapping.SUBSCRIPTION_MARKET_DATA_FIELD_MAPPING_SPECIFICITY_FIELD_NAME.equals(entry.getField().getName()))
					.map(getInvestmentSpecificityUtilHandler()::generatePopulatedSpecificityBean)
					.map(MarketDataFieldSpecificityMapping.class::cast)
					.filter(marketDataFieldMapping -> marketDataFieldMapping.getMarketDataSource() == null || (marketDataSource != null && marketDataFieldMapping.getMarketDataSource().equals(marketDataSource.getId())))
					.map(MarketDataFieldSpecificityMapping::getMarketDataFields)
					.flatMap(ArrayUtils::getStream)
					.map(fieldId -> getMarketDataFieldService().getMarketDataField(fieldId))
					.collect(Collectors.toList());
		});
	}


	private List<SubscriptionSecurity> getSubscriptionSecurityList(List<InvestmentSecurity> securityList, Date date, boolean includeExpired, Predicate<InvestmentSecurity> securityInclusionPredicate) {
		return CollectionUtils.getStream(securityList)
				.filter(Objects::nonNull)
				.distinct()
				.filter(security -> {
					try {
						return InvestmentUtils.isSecurityActiveOn(security, date == null ? new Date() : date)
								// if includeExpired, make sure there is a subscription reference for the security to avoid making unnecessary unsubscribe requests
								// unnecessary unsubscribe requests can mess up the reference counts for underlying Option security subscriptions causing them to be prematurely ended
								|| (includeExpired && securityInclusionPredicate.test(security));
					}
					catch (ValidationException e) {
						// lookup of no longer active security market data symbol may throw validation exception if field mappings do not exist
						// return true and let it get filtered out later
						return true;
					}
				})
				.map(SubscriptionSecurity::new)
				.collect(Collectors.toList());
	}


	/**
	 * Gets a list of {@link MarketDataActiveUserSubscription}s. The returned list is the active list from the
	 * {@link MarketDataProvider} handling subscriptions. The subscriptions available in local cache and not available
	 * on the provider will be cleaned up.
	 * <p>
	 * If shutdown is true, the list returned is the intersection of the the local and provider active subscriptions, which
	 * can be unsubscribed.
	 */
	private List<MarketDataActiveUserSubscription> doGetMarketDataActiveUserSubscriptionList(boolean shutdown) {
		List<MarketDataActiveUserSubscription> providerSubscriptionList = new ArrayList<>();
		List<MarketDataActiveUserSubscription> localSubscriptionList = new ArrayList<>();

		CompletableFuture<List<MarketDataActiveUserSubscription>> activeSubscriptionFuture = CompletableFuture.allOf(
				CompletableFuture.runAsync(() -> providerSubscriptionList.addAll(getMarketDataActiveUserSubscriptionListFromProvider()), getForkJoinPool()),
				CompletableFuture.runAsync(() -> localSubscriptionList.addAll(getMarketDataActiveUserSubscriptionListFromLocalCache()), getForkJoinPool()))
				.thenApply(v -> {
					if (shutdown) {
						// Only unsubscribe subscriptions in local cache on shutdown
						return CollectionUtils.getIntersection(providerSubscriptionList, localSubscriptionList);
					}

					// the MarketData subscriptions from the provider are the true subscriptions; update the reference counts from local cache
					providerSubscriptionList.forEach(providerSubscription -> localSubscriptionList.stream()
							.filter(localSubscription -> localSubscription.equals(providerSubscription))
							.findFirst()
							.ifPresent(localSubscription -> {
								providerSubscription.setReferenceCount(localSubscription.getReferenceCount());
								providerSubscription.setLocked(localSubscription.isLocked());
							}));

					// anything in the local cache not active in provider list can be removed.
					localSubscriptionList.removeAll(providerSubscriptionList);
					if (!localSubscriptionList.isEmpty()) {
						// cancel remaining local active subscriptions
						LogUtils.debug(LogCommand.ofMessage(getClass(), "Unsubscribing local active subscriptions unavailable on provider: ", localSubscriptionList));
						processMarketDataActiveUserSubscriptionListAsynchronously(localSubscriptionList, false, true);
					}
					return providerSubscriptionList;
				});

		try {
			// wait ten seconds for a result or timeout; tests will block on this indefinitely otherwise
			return activeSubscriptionFuture.get(10, TimeUnit.SECONDS);
		}
		catch (InterruptedException e) {
			if (!activeSubscriptionFuture.isDone()) {
				activeSubscriptionFuture.cancel(true);
			}
			Thread.currentThread().interrupt();
			return Collections.emptyList();
		}
		catch (Exception e) { // timeout or task error
			if (!activeSubscriptionFuture.isDone()) {
				activeSubscriptionFuture.cancel(true);
			}
			throw new RuntimeException("Building active subscription list failed: " + ExceptionUtils.getDetailedMessage(e), e);
		}
	}


	/**
	 * Makes a request to {@link MarketDataProvider#processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations)} with {@link MarketDataSubscriptionOperations#LIST} to get active subscriptions.
	 */
	private List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionListFromProvider() {
		MarketDataSource bloombergDataSource = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
		MarketDataProvider<?> marketDataProvider = getMarketDataProviderLocator().locate(bloombergDataSource);
		List<MarketDataSubscriptionSecurityData> bloombergSubscriptionList = marketDataProvider.processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations.LIST);
		return CollectionUtils.getStream(bloombergSubscriptionList)
				.filter(subscriptionSecurityData -> subscriptionSecurityData.getStatus() == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE)
				.map(subscriptionSecurityData -> {
					InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurityBySymbolOnly(subscriptionSecurityData.getSecuritySymbol());
					return subscriptionSecurityData.getUserIdentitySet().stream()
							.map(marketDataUserIdentity -> {
								// If a subscription is active on Bloomberg and not in local cache, look up the user
								MarketDataUserSubscription localSubscription = getUserSubscriptionMap().get(marketDataUserIdentity.getSecurityUserId());
								SecurityUser user = localSubscription != null ? localSubscription.getUser() : getSecurityUserService().getSecurityUser(marketDataUserIdentity.getSecurityUserId());
								String clientIpAddress = localSubscription != null ? localSubscription.getClientIpAddress() : marketDataUserIdentity.getClientIpAddress();
								MarketDataActiveUserSubscription subscription = MarketDataActiveUserSubscription.of(user, clientIpAddress, security, subscriptionSecurityData.getSecuritySymbol());
								subscription.setReferenceCount(1);
								return subscription;
							})
							.collect(Collectors.toList());
				})
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}


	/**
	 * Evaluates the local caches of this object to determine active subscriptions.
	 */
	private List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionListFromLocalCache() {
		return getMarketDataActiveUserSubscriptionListFromLocalCacheWithFiltering(null, null);
	}


	/**
	 * Returns a list of {@link MarketDataActiveUserSubscription}s for the provided filters.
	 * <p>
	 * If the user filter is null, subscriptions for all users will be returned.
	 * If the security list filter is null, subscriptions for all securities will be returned.
	 */
	private List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionListFromLocalCacheWithFiltering(SecurityUser securityUserFilter, List<InvestmentSecurity> securityListFilter) {
		Predicate<Short> userPredicate = securityUserFilter == null ? userId -> true : userId -> userId.equals(securityUserFilter.getId());
		Set<Integer> securityIdSet = CollectionUtils.getStream(securityListFilter).map(InvestmentSecurity::getId).collect(Collectors.toSet());
		IntPredicate securityPredicate = securityIdSet.isEmpty() ? securityId -> true : securityIdSet::contains;

		Map<Short, List<MarketDataActiveUserSubscription>> userIdToSecuritySubscriptionMap = new HashMap<>();
		Iterator<Map.Entry<Short, MarketDataUserSubscription>> userSubscriptionIterator = getUserSubscriptionMap().entrySet().iterator();
		while (userSubscriptionIterator.hasNext()) {
			Map.Entry<Short, MarketDataUserSubscription> userSubscriptionEntry = userSubscriptionIterator.next();
			MarketDataUserSubscription userSubscription = userSubscriptionEntry.getValue();
			if (userPredicate.test(userSubscriptionEntry.getKey())) {
				Iterator<Map.Entry<String, MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference>> userSubscriptionReferenceIterator = userSubscription.getSubscriptionSecurityReferenceIterator();
				while (userSubscriptionReferenceIterator.hasNext()) {
					Map.Entry<String, MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference> userSecurityReferenceEntry = userSubscriptionReferenceIterator.next();
					MarketDataUserSubscription.MarketDataUserSubscriptionSecurityReference securityUsageReference = userSecurityReferenceEntry.getValue();
					if (!securityUsageReference.isActive()) {
						userSubscriptionReferenceIterator.remove();
					}
					else if (securityPredicate.test(securityUsageReference.getInvestmentSecurityId())) {
						InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityUsageReference.getInvestmentSecurityId());

						MarketDataActiveUserSubscription securitySubscription = MarketDataActiveUserSubscription.of(userSubscription.getUser(), userSubscription.getClientIpAddress(), security, securityUsageReference.getExternalSecurityName());
						securitySubscription.setReferenceCount(securityUsageReference.getUsageCount());
						securitySubscription.setLocked(securityUsageReference.isLocked());
						userIdToSecuritySubscriptionMap.computeIfAbsent(userSubscription.getUser().getId(), key -> new ArrayList<>()).add(securitySubscription);
					}
				}
			}
			if (userSubscription.isInactive()) {
				userSubscriptionIterator.remove();
			}
		}

		return userIdToSecuritySubscriptionMap.values().stream()
				.flatMap(CollectionUtils::getStream)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
	}


	/**
	 * Processes the list of {@link MarketDataActiveUserSubscription}s asynchronously using the pool obtained from {@link #getForkJoinPool()}.
	 *
	 * @see #processMarketDataActiveUserSubscriptionList(List, boolean, boolean)
	 */
	private void processMarketDataActiveUserSubscriptionListAsynchronously(List<MarketDataActiveUserSubscription> activeUserSubscriptionList, boolean resubscribe, boolean updateCacheOnly) {
		// execute asynchronously to make sure event processing is as fast as possible when processing message for subscription sync requests
		getForkJoinPool().execute(() -> {
			if (updateCacheOnly) {
				// only update cache for unsubscribe actions
				if (!resubscribe) {
					activeUserSubscriptionList.stream()
							.map(activeUserSubscription -> MarketDataSubscriptionCommand.of(activeUserSubscription.getSecurityUser())
									.withClientIpAddress(activeUserSubscription.getClientIpAddress())
									.withSubscribe(false)
									.withSecurityList(Collections.singletonList(activeUserSubscription.getSecurity())))
							.forEach(this::updateSubscriptionDataFieldCacheForUserSubscriptionCommand);
				}
			}
			else if (resubscribe) {
				resubscribeMarketDataActiveUserSubscriptionList(activeUserSubscriptionList);
			}
			else {
				unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand.of(activeUserSubscriptionList));
			}
		});
	}


	/**
	 * Processes each {@link MarketDataActiveUserSubscription} for resubscribing instead of unsubscribing.
	 */
	private void resubscribeMarketDataActiveUserSubscriptionList(List<MarketDataActiveUserSubscription> activeUserSubscriptionList) {
		processMarketDataActiveUserSubscriptionList(activeUserSubscriptionList, true, false);
	}


	/**
	 * Groups the provided {@link MarketDataActiveUserSubscription} list by user and creates a {@link MarketDataSubscriptionCommand} to be processed
	 * for each grouping. If resubscribe is true, the command is set to subscribe, otherwise, it will be unsubscribe. If the provided user is null, the
	 * processing will be completed as the systemuser.
	 */
	private void processMarketDataActiveUserSubscriptionList(List<MarketDataActiveUserSubscription> activeUserSubscriptionList, boolean resubscribe, boolean forcefully) {
		// Attempt to unlock any items that will be processed in the event previous requests did not get a response.
		unlockMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand.of(activeUserSubscriptionList));

		Date currentDay = DateUtils.clearTime(new Date());
		Map<SecurityUser, List<MarketDataActiveUserSubscription>> map = BeanUtils.getBeansMap(activeUserSubscriptionList, MarketDataActiveUserSubscription::getSecurityUser);
		for (Map.Entry<SecurityUser, List<MarketDataActiveUserSubscription>> userSubscriptionListEntry : map.entrySet()) {
			SecurityUser user = userSubscriptionListEntry.getKey();
			List<MarketDataActiveUserSubscription> userSubscriptionList = userSubscriptionListEntry.getValue();
			if (!CollectionUtils.isEmpty(userSubscriptionList)) {
				String userClientIpAddress = userSubscriptionList.stream().findFirst()
						.map(MarketDataActiveUserSubscription::getClientIpAddress)
						.orElse(null);

				// Build security lists for (re|un)subscribing. Unsubscribe if the request is to do so or a security is inactive.
				Map<Boolean, List<InvestmentSecurity>> activeSecurityMap = CollectionUtils.getStream(userSubscriptionList)
						.map(MarketDataActiveUserSubscription::getSecurity)
						.filter(Objects::nonNull)
						.collect(Collectors.groupingBy(security -> resubscribe && InvestmentUtils.isSecurityActiveOn(security, currentDay)));

				activeSecurityMap.entrySet().stream()
						.map(activeSecurityListEntry ->
								MarketDataSubscriptionCommand.of(user)
										.withClientIpAddress(userClientIpAddress)
										.withSubscribe(activeSecurityListEntry.getKey())
										.withSecurityList(activeSecurityListEntry.getValue())
										.withForce(forcefully))
						.forEach(command ->
								getSecurityImpersonationHandler().runAsSecurityUser(command.getSecurityUser(),
										() -> {
											LogUtils.debug(LogCommand.ofMessage(getClass(), "Processing active user subscription request: ", command));
											try {
												processMarketDataSubscriptionCommand(command);
											}
											catch (Exception e) {
												LogUtils.error(getClass(), "Failed to " + (command.isSubscribe() ? MarketDataSubscriptionOperations.SUBSCRIBE : MarketDataSubscriptionOperations.UNSUBSCRIBE) + " security subscription: {" + command + "}, error: " + e.getMessage(), e);
											}
										}));
			}
		}
	}


	/**
	 * This method is used to sync an active subscription with the {@link MarketDataProvider} by processing a subscription for an event that could not
	 * be handled due to out of sync local cache. A subscription is processed for a security by looking up the security by the provided ID, or symbol if null.
	 * The security is looked up by ID, if present, otherwise by symbol.
	 * <p>
	 * The activeSubscription argument is used to tell if the security should be subscribed or unsubscribed to.
	 * <p>
	 * <br/>- If the security is active and activeSubscription is true, a resubscribe request will be made for the security and each security user ID.
	 * <br/>- If the security is inactive or activeSubscription is false, the subscription will be unsubscribed to for each security user ID.
	 * <br/>- If the security is not found, a warning will be logged.
	 */
	private void syncProviderMarketDataActiveUserSubscriptionListWithLocalCacheUponEvent(Integer securityId, String marketDataSecuritySymbol, Map<Short, String> securityUserIdToClientIpMap, boolean activeSubscription) {
		InvestmentSecurity security = (securityId != null)
				? getInvestmentInstrumentService().getInvestmentSecurity(securityId)
				: getInvestmentInstrumentService().getInvestmentSecurityBySymbolOnly(marketDataSecuritySymbol);
		if (security != null) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Sync provider subscriptions with local cache: {security: ", security, ", subscribe: ", activeSubscription, ", userIds: ", StringUtils.collectionToCommaDelimitedString(securityUserIdToClientIpMap.keySet()), "}"));
			List<MarketDataActiveUserSubscription> activeUserSubscriptionList = CollectionUtils.getStream(securityUserIdToClientIpMap.entrySet())
					.map(userIdToIpEntry -> {
						SecurityUser user = getSecurityUserService().getSecurityUser(userIdToIpEntry.getKey());
						return MarketDataActiveUserSubscription.of(user, userIdToIpEntry.getValue(), security, marketDataSecuritySymbol);
					}).collect(Collectors.toList());
			processMarketDataActiveUserSubscriptionListAsynchronously(activeUserSubscriptionList, activeSubscription, true);
		}
		else {
			LogUtils.warn(getClass(), "MarketData subscription for security symbol [" + marketDataSecuritySymbol + "] is active and the system is unable to process its events because caches are out of sync.");
		}
	}


	private boolean isFutureCanceled(List<Future<Object>> futureList) {
		boolean taskFailed = false;
		// Iterate all tasks to throw an exception from a failed future/task.
		for (Future<Object> future : futureList) {
			try {
				future.get();
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				taskFailed = true; // interrupted, treat as canceled
			}
			catch (CancellationException ce) {
				taskFailed = true; // canceled
			}
			catch (ExecutionException e) {
				Throwable cause = ExceptionUtils.getOriginalException(e);
				if (cause == null) {
					cause = e;
				}
				throw new RuntimeException(cause.getMessage(), cause);
			}
		}
		return taskFailed;
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////                 Helper Classes                ///////////////
	///////////////////////////////////////////////////////////////////////////

	private static class SubscriptionSecurity {

		private final InvestmentSecurity security;


		public SubscriptionSecurity(InvestmentSecurity security) {
			this.security = security;
		}


		public InvestmentSecurity getSecurity() {
			return this.security;
		}


		/**
		 * Returns a Set of unique {@link InvestmentSecurity}s that should be subscribed to for the provided security.
		 * Some securities will return more than one (e.g. an Option will return itself and its underlying security).
		 */
		public Set<InvestmentSecurity> getSecuritySetToSubscribeTo() {
			if (InvestmentUtils.isSecurityOfType(this.security, InvestmentType.OPTIONS)) {
				if (isTheoreticallyPriced()) {
					return (hasReferenceSecurity())
							? CollectionUtils.createHashSet(this.security.getReferenceSecurity(), this.security.getUnderlyingSecurity())
							: CollectionUtils.createHashSet(this.security.getUnderlyingSecurity());
				}
				return CollectionUtils.createHashSet(this.security, this.security.getUnderlyingSecurity());
			}
			return CollectionUtils.createHashSet(this.security);
		}


		/**
		 * Returns true if the security this subscription security is for is a theoretically priced security.
		 */
		public boolean isTheoreticallyPriced() {
			return InvestmentUtils.isSecurityOfTypeSubType2(this.security, InvestmentTypeSubType2.OPTIONS_FLEX)
					&& this.security.getInstrument().getHierarchy().isReferenceSecurityAllowed();
		}


		/**
		 * Returns true if the security has a reference security defined
		 */
		public boolean hasReferenceSecurity() {
			return this.security.getReferenceSecurity() != null;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Map<Short, MarketDataUserSubscription> getUserSubscriptionMap() {
		return this.userSubscriptionMap;
	}


	public ForkJoinPool getForkJoinPool() {
		return this.forkJoinPool;
	}


	public void setForkJoinPool(ForkJoinPool forkJoinPool) {
		this.forkJoinPool = forkJoinPool;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSpecificityUtilHandler getInvestmentSpecificityUtilHandler() {
		return this.investmentSpecificityUtilHandler;
	}


	public void setInvestmentSpecificityUtilHandler(InvestmentSpecificityUtilHandler investmentSpecificityUtilHandler) {
		this.investmentSpecificityUtilHandler = investmentSpecificityUtilHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataTheoreticalValueService getMarketDataTheoreticalValueService() {
		return this.marketDataTheoreticalValueService;
	}


	public void setMarketDataTheoreticalValueService(MarketDataTheoreticalValueService marketDataTheoreticalValueService) {
		this.marketDataTheoreticalValueService = marketDataTheoreticalValueService;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
