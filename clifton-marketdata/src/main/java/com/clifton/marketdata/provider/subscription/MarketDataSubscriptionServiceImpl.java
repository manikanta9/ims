package com.clifton.marketdata.provider.subscription;

import com.clifton.core.security.SecurityUser;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.provider.subscription.util.MarketDataSubscriptionUtilHandler;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * @author NickK
 */
@Service
public class MarketDataSubscriptionServiceImpl implements MarketDataSubscriptionService {

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataSubscriptionUtilHandler marketDataSubscriptionUtilHandler;
	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public void processMarketDataSubscriptionCommand(MarketDataSubscriptionCommand subscriptionCommand) {
		consolidateMarketDataSubscriptionCommandSecurityList(subscriptionCommand);
		getMarketDataSubscriptionUtilHandler().processMarketDataSubscriptionCommand(subscriptionCommand);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionList() {
		return getMarketDataSubscriptionUtilHandler().getMarketDataActiveUserSubscriptionList();
	}


	@Override
	public void unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand) {
		validateMarketDataSubscriptionManagementAuthorization(activeUserSubscriptionCommand);
		getMarketDataSubscriptionUtilHandler().unsubscribeMarketDataActiveUserSubscriptionList(activeUserSubscriptionCommand);
	}


	@Override
	public void unlockMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand) {
		validateMarketDataSubscriptionManagementAuthorization(activeUserSubscriptionCommand);
		getMarketDataSubscriptionUtilHandler().unlockMarketDataActiveUserSubscriptionList(activeUserSubscriptionCommand);
	}


	@Override
	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation) {
		return getMarketDataSubscriptionUtilHandler().processMarketDataSubscriptionOperation(subscriptionOperation);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Consolidates the subscription command's security bean list. Securities can be provided in the bean list and/or by security IDs.
	 * This method will merge and consolidate securities to a list of unique securities.
	 */
	private void consolidateMarketDataSubscriptionCommandSecurityList(MarketDataSubscriptionCommand subscriptionCommand) {
		Set<InvestmentSecurity> securitySet = new HashSet<>();
		if (!CollectionUtils.isEmpty(subscriptionCommand.getBeanList())) {
			securitySet.addAll(subscriptionCommand.getBeanList());
		}
		if (!ArrayUtils.isEmpty(subscriptionCommand.getSecurityIds())) {
			ArrayUtils.getStream(subscriptionCommand.getSecurityIds())
					.map(id -> getInvestmentInstrumentService().getInvestmentSecurity(id))
					.filter(Objects::nonNull)
					.forEach(securitySet::add);
		}
		subscriptionCommand.setBeanList(new ArrayList<>(securitySet));
	}


	/**
	 * Validates that the user has access to manage market data subscriptions or the user is performing actions on behalf of (him|her)self.
	 */
	private void validateMarketDataSubscriptionManagementAuthorization(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand) {
		if (!getSecurityAuthorizationService().isSecurityAccessAllowed(SYSTEM_MANAGEMENT_SECURITY_RESOURCE, SecurityPermission.PERMISSION_READ)) {
			SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
			if (activeUserSubscriptionCommand.isProcessAll() || !CompareUtils.isEqual(currentUser, activeUserSubscriptionCommand.getSecurityUser())) {
				throw new AccessDeniedException(SYSTEM_MANAGEMENT_SECURITY_RESOURCE, SecurityPermission.PERMISSION_READ);
			}
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataSubscriptionUtilHandler getMarketDataSubscriptionUtilHandler() {
		return this.marketDataSubscriptionUtilHandler;
	}


	public void setMarketDataSubscriptionUtilHandler(MarketDataSubscriptionUtilHandler marketDataSubscriptionUtilHandler) {
		this.marketDataSubscriptionUtilHandler = marketDataSubscriptionUtilHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
