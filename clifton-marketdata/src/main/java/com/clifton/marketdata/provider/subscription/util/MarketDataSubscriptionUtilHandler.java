package com.clifton.marketdata.provider.subscription.util;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.provider.subscription.MarketDataActiveUserSubscription;
import com.clifton.marketdata.provider.subscription.MarketDataActiveUserSubscriptionCommand;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionCommand;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;

import java.util.List;


/**
 * <code>MarketDataSubscriptionUtilHandler</code> is an utility for the management of market data subscriptions.
 *
 * @author NickK
 */
public interface MarketDataSubscriptionUtilHandler {

	/**
	 * Processes the provide {@link MarketDataSubscriptionCommand} by either subscribing or unsubscribing to
	 * the list of {@link InvestmentSecurity}s of the command. If the user is not specified on the command, the
	 * requesting user will be obtained from the active {@link com.clifton.core.context.ContextHandler}.
	 * <p>
	 * The subscriptions for each security are based on defined investment specificity subscription market data field mappings.
	 */
	public void processMarketDataSubscriptionCommand(MarketDataSubscriptionCommand command);


	///////////////////////////////////////////////////////////////////////////
	////////////              Management APIs                 /////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation);


	/**
	 * Returns a list of {@link MarketDataActiveUserSubscription} base on the local subscription cache.
	 */
	public List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionList();


	/**
	 * Unsubscribes the provided list of {@link MarketDataActiveUserSubscription}s per the provided {@link MarketDataActiveUserSubscriptionCommand}
	 */
	public void unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand);


	/**
	 * Unsubscribes the all active {@link MarketDataActiveUserSubscription}s.
	 * <p>
	 * Similar to {@link #unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand)} for all active subscriptions.
	 */
	public void unsubscribeMarketDataActiveUserSubscriptionListAll();


	/**
	 * Unlocks any locked security usage reference locks for the provided {@link MarketDataActiveUserSubscriptionCommand}.
	 * <p>
	 * The cached user references use reference counts to avoid unnecessary subscription requests and premature subscription terminations.
	 * Locks are used in the references to avoid concurrent requests for the same user and security combination. If a request does not receive a
	 * response, the lock may remain locked preventing future requests from being made. This management service will unlock any locked reference
	 * as long as there are no requests waiting to obtain the lock. Waiting requests will timeout, so the lock will eventually be able to be released.
	 */
	public void unlockMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand);
}
