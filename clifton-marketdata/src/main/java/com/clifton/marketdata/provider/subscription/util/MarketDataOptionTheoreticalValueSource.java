package com.clifton.marketdata.provider.subscription.util;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.live.theoretical.MarketDataOptionTheoreticalValueCommand;
import com.clifton.marketdata.live.theoretical.MarketDataTheoreticalValueService;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * <code>MarketDataOptionTheoreticalValueSource</code> is a data holder for an {@link InvestmentSecurity} that is
 * theoretically priced via Black-Scholes or similar function.
 *
 * @author NickK
 */
@NonPersistentObject
public class MarketDataOptionTheoreticalValueSource extends MarketDataOptionTheoreticalValueCommand {

	private final AtomicBoolean recalculate = new AtomicBoolean();
	private final MarketDataTheoreticalValueService marketDataTheoreticalValueService;

	///////////////////////////////////////////////////////////////////////////


	public MarketDataOptionTheoreticalValueSource(InvestmentSecurity security, MarketDataTheoreticalValueService marketDataTheoreticalValueService) {
		setSecurity(security);
		setProviderOverride(MarketDataProviderOverrides.BPIPE);
		this.marketDataTheoreticalValueService = marketDataTheoreticalValueService;
	}


	@Override
	public boolean equals(Object toCompare) {
		if (toCompare instanceof MarketDataOptionTheoreticalValueSource) {
			InvestmentSecurity toCompareSecurity = ((MarketDataOptionTheoreticalValueSource) toCompare).getSecurity();
			return getSecurity() == null ? getSecurity() == toCompareSecurity : getSecurity().equals(toCompareSecurity);
		}
		return false;
	}


	@Override
	public int hashCode() {
		return getSecurity() == null ? Integer.MIN_VALUE : getSecurity().hashCode();
	}

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Applies the market data value and returns true if the value is applicable to this theoretical pricing source.
	 * If the value is not applicable, no updates are done and false is returned.
	 */
	public boolean applyDependentSecurityFieldValue(Integer securityId, MarketDataField marketDataField, Object value) {
		if (getSecurity().getReferenceSecurity() != null && securityId.equals(getSecurity().getReferenceSecurity().getId())) {
			if (MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_VOLATILITY.equals(marketDataField.getExternalFieldName())) {
				setVolatility(value instanceof Number ? BigDecimal.valueOf(((Number) value).doubleValue()) : new BigDecimal(value.toString()));
				this.recalculate.set(true);
				return true;
			}
		}
		else if (getSecurity().getUnderlyingSecurity() != null && securityId.equals(getSecurity().getUnderlyingSecurity().getId())) {
			if (MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_LAST_PRICE.equals(marketDataField.getExternalFieldName())) {
				setUnderlyingPrice(value instanceof Number ? BigDecimal.valueOf(((Number) value).doubleValue()) : new BigDecimal(value.toString()));
				this.recalculate.set(true);
				return true;
			}
		}
		return false;
	}


	/**
	 * Calculates the updated theoretical price based on current property values of this object. The price only needs to be recalculated
	 * upon a dependent security field value update.
	 * <p>
	 * Returns the current price, which is updated only if the calculation is necessary.
	 *
	 * @see #applyDependentSecurityFieldValue(Integer, MarketDataField, Object)
	 */
	public BigDecimal calculatePrice() {
		if (this.recalculate.getAndSet(false)) {
			Optional.ofNullable(this.marketDataTheoreticalValueService.getMarketDataOptionTheoreticalPrice(this)).ifPresent(live -> setPrice(live.asNumericValue()));
		}
		return getPrice();
	}


	public boolean isRecalculateNecessary() {
		return this.recalculate.get();
	}
}
