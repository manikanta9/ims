package com.clifton.marketdata.provider;

import com.clifton.core.messaging.MessagingMessage;


/**
 * <code>MarketDataProviderOverrides</code> represents a {@link MarketDataProvider} override that can change how a request is directed.
 * <p>
 * For example, the Bloomberg MarketDataProvider can retrieve values from a TERMINAL or BPIPE service. This override can be used in
 * a request to override the system's default configuration (e.g. IMS may use TERMINAL and a particular service of IMS wants to use BPIPE).
 *
 * @author NickK
 */
public enum MarketDataProviderOverrides {

	// Bloomberg Provider Source Overrides
	BPIPE(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME),
	TERMINAL(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME),
	BACKOFFICE(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME),
	MDS(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME);

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private final String overrideKey;


	MarketDataProviderOverrides(String overrideKey) {
		this.overrideKey = overrideKey;
	}


	/**
	 * MarketDataProvider specific key the override pertains to.
	 */
	public String getOverrideKey() {
		return this.overrideKey;
	}


	/**
	 * MarketDataProvider specific value the override represents.
	 */
	public String getOverrideValue() {
		return name();
	}
}
