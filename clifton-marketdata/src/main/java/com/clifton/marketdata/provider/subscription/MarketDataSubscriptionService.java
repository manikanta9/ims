package com.clifton.marketdata.provider.subscription;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.subscription.util.MarketDataSubscriptionUtilHandler;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * <code>MarketDataSubscriptionService</code> defines methods for subscription to realtime market data.
 *
 * @author NickK
 */
public interface MarketDataSubscriptionService {

	public static final String MARKET_DATA_SUBSCRIPTION_VALUES_SECURITY_RESOURCE = "Market Data Subscription Values";
	public static final String SYSTEM_MANAGEMENT_SECURITY_RESOURCE = "System Management";

	/**
	 * The STOMP channel through which market data is sent to users.
	 */
	public static final String CHANNEL_USER_TOPIC_MARKET_DATA = "/user/topic/market-data";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates and submits a market data subscription request for the provided {@link MarketDataSubscriptionCommand}'s
	 * list of {@link InvestmentSecurity}s. The command defines whether the request should be a subscribe of unsubscribe action.
	 */
	@SecureMethod(securityResource = MARKET_DATA_SUBSCRIPTION_VALUES_SECURITY_RESOURCE, permissions = SecurityPermission.PERMISSION_READ)
	public void processMarketDataSubscriptionCommand(MarketDataSubscriptionCommand subscriptionCommand);


	///////////////////////////////////////////////////////////////////////////
	////////////              Management APIs                 /////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of {@link MarketDataActiveUserSubscription}s currently in the system.
	 *
	 * @see MarketDataProvider#processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations) ()
	 * @see MarketDataSubscriptionUtilHandler#getMarketDataActiveUserSubscriptionList()
	 */
	@SecureMethod(securityResource = MARKET_DATA_SUBSCRIPTION_VALUES_SECURITY_RESOURCE)
	public List<MarketDataActiveUserSubscription> getMarketDataActiveUserSubscriptionList();


	/**
	 * Unsubscribes the list of {@link MarketDataActiveUserSubscription}s of the provided {@link MarketDataActiveUserSubscriptionCommand}.
	 * <p>
	 * Additional authorization is validated in the service for access to System Management, or the request is managing only the current user.
	 */
	@SecureMethod(securityResource = MARKET_DATA_SUBSCRIPTION_VALUES_SECURITY_RESOURCE, permissions = SecurityPermission.PERMISSION_READ)
	@RequestMapping("marketDataActiveUserSubscriptionListUnsubscribe")
	public void unsubscribeMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand);


	/**
	 * Unlock any locked security usage reference locks for the list of {@link MarketDataActiveUserSubscription}s of the provided {@link MarketDataActiveUserSubscriptionCommand}.
	 * <p>
	 * Additional authorization is validated in the service for access to System Management, or the request is managing only the current user.
	 */
	@SecureMethod(securityResource = MARKET_DATA_SUBSCRIPTION_VALUES_SECURITY_RESOURCE, permissions = SecurityPermission.PERMISSION_READ)
	public void unlockMarketDataActiveUserSubscriptionList(MarketDataActiveUserSubscriptionCommand activeUserSubscriptionCommand);


	/**
	 * Performs the provided {@link MarketDataSubscriptionOperations} against the {@link MarketDataProvider} handling subscription support, and returns
	 * the list of active security subscriptions represented as {@link MarketDataSubscriptionSecurityData}.
	 */
	@SecureMethod(securityResource = SYSTEM_MANAGEMENT_SECURITY_RESOURCE, permissions = SecurityPermission.PERMISSION_READ)
	public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation);
}
