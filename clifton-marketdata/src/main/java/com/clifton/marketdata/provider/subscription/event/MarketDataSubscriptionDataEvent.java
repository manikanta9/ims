package com.clifton.marketdata.provider.subscription.event;

import com.clifton.core.util.event.EventObject;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;


/**
 * <code>MarketDataSubscriptionDataEvent</code> represents an {@link com.clifton.core.util.event.Event} for
 * Bloomberg subscription data udpates.
 *
 * @author NickK
 * @see MarketDataSubscriptionSecurityData
 */
public class MarketDataSubscriptionDataEvent extends EventObject<MarketDataSubscriptionSecurityData, Object> {

	public static final String EVENT_NAME = "Market Data Subscription Data";

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private MarketDataSubscriptionDataEvent(MarketDataSubscriptionSecurityData subscriptionData) {
		super(EVENT_NAME);
		setTarget(subscriptionData);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static MarketDataSubscriptionDataEvent ofEventTarget(MarketDataSubscriptionSecurityData subscriptionData) {
		return new MarketDataSubscriptionDataEvent(subscriptionData);
	}
}

