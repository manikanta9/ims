package com.clifton.marketdata.provider.subscription;

import com.clifton.investment.instrument.InvestmentSecurity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * <code>MarketDataSubscriptionDataValueSet</code> holds {@link InvestmentSecurity} subscription data values to be sent to a user.
 * The data is held as a map containing the security ID mapped to a collection of {@link MarketDataSubscriptionDataValue}s.
 *
 * @author NickK
 */
public class MarketDataSubscriptionDataValueSet {

	/**
	 * A map of maps containing {@link MarketDataSubscriptionDataValue}s for {@link InvestmentSecurity}s.
	 * The first key is the security ID, and maps to a map of values keyed by the subscription data value's name.
	 * <p>
	 * The map of values is a map so the values can be easily updated so only the latest value is available.
	 */
	private final Map<Integer, Map<String, MarketDataSubscriptionDataValue>> securityToDataValueMap = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a map containing {@link MarketDataSubscriptionDataValue}s for one or more {@link InvestmentSecurity} IDs
	 * that are subscribed to by a user.
	 */
	public Map<Integer, Collection<MarketDataSubscriptionDataValue>> getSecurityDataValueMap() {
		// The collection is wrapped in a list type here so that the collection can be properly serialized (see https://github.com/monitorjbl/json-view/issues/57)
		return this.securityToDataValueMap.entrySet().stream()
				.collect(Collectors.toMap(Map.Entry::getKey, entry -> new ArrayList<>(entry.getValue().values())));
	}


	/**
	 * Adds or updates the {@link MarketDataSubscriptionDataValue} for the provided {@link InvestmentSecurity} ID.
	 */
	public void addSecurityDataValue(Integer investmentSecurityId, MarketDataSubscriptionDataValue value) {
		this.securityToDataValueMap.computeIfAbsent(investmentSecurityId, HashMap::new)
				.put(value.getFieldName(), value);
	}


	/**
	 * Returns {@code true} if the data value set is empty.
	 *
	 * @return {@code true} if the data value set is empty, or {@code false} otherwise
	 */
	public boolean isEmpty() {
		return this.securityToDataValueMap.isEmpty();
	}
}
