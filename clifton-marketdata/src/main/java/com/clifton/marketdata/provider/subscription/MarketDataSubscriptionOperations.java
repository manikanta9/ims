package com.clifton.marketdata.provider.subscription;

/**
 * <code>MarketDataSubscriptionOperations</code> defines operations that can be performed for market data subscriptions.
 *
 * @author NickK
 */
public enum MarketDataSubscriptionOperations {
	SUBSCRIBE(false, true),
	RESUBSCRIBE(false, true),
	UNSUBSCRIBE(false, false),
	LIST(true, false),
	CLEAR_SUBSCRIPTIONS(true, false);

	private final boolean subscribe;
	private final boolean management;


	MarketDataSubscriptionOperations(boolean management, boolean subscribe) {
		this.management = management;
		this.subscribe = subscribe;
	}


	/**
	 * Returns true if the operation supports adding or updating a subscription.
	 */
	public boolean isSubscribe() {
		return this.subscribe;
	}


	public boolean isManagement() {
		return this.management;
	}
}
