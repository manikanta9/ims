package com.clifton.marketdata.provider.subscription;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.security.user.SecurityUser;


/**
 * <code>MarketDataActiveUserSubscription</code> is an object holding data pertaining to an active user MarketData subscription
 * that can be used for viewing and managing data subscriptions.
 *
 * @author NickK
 */
@NonPersistentObject
public class MarketDataActiveUserSubscription {

	private SecurityUser securityUser;
	private String clientIpAddress;
	private InvestmentSecurity security;
	private String marketDataSecuritySymbol;
	/**
	 * The reference count is the number of times a security has been used in subscriptions for this user.
	 * <p>
	 * The reference count is used to avoid excessive subscription requests when a security is used in more than one security, such as with Option underlying securities.
	 * The reference count also avoid prematurely unsubscribing from a used security; all references must be removed before unsubscribing.
	 */
	private int referenceCount;
	/**
	 * Security references use locks for a user and security combination to prevent concurrent subscription requests.
	 * <p>
	 * If locked, the security reference for the a subscription had a request made and did not receive a response.
	 * In order to prevent subscriptions from continuously failing, the lock can be unlocked to clean up the reference state.
	 */
	private boolean locked;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static MarketDataActiveUserSubscription of(SecurityUser securityUser, String clientIpAddress, InvestmentSecurity security, String marketDataSecuritySymbol) {
		MarketDataActiveUserSubscription subscription = new MarketDataActiveUserSubscription();
		subscription.setSecurityUser(securityUser);
		subscription.setClientIpAddress(clientIpAddress);
		subscription.setSecurity(security);
		subscription.setMarketDataSecuritySymbol(marketDataSecuritySymbol);
		return subscription;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{user=" + getSecurityUser() + ", security=" + getSecurity() + ", referenceCount: " + getReferenceCount() + ", locked: " + isLocked() + "}";
	}


	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		else if (object == null || !object.getClass().equals(getClass())) {
			return false;
		}
		MarketDataActiveUserSubscription toCompare = (MarketDataActiveUserSubscription) object;
		return (getSecurityUser() == null ? getSecurityUser() == toCompare.getSecurityUser() : getSecurityUser().equals(toCompare.getSecurityUser()))
				&& (getSecurity() == null ? getSecurity() == toCompare.getSecurity() : getSecurity().equals(toCompare.getSecurity()));
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (getSecurityUser() == null ? 0 : getSecurityUser().hashCode());
		result = prime * result + (getSecurity() == null ? 0 : getSecurity().hashCode());
		return result;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public String getClientIpAddress() {
		return this.clientIpAddress;
	}


	public void setClientIpAddress(String clientIpAddress) {
		this.clientIpAddress = clientIpAddress;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public String getMarketDataSecuritySymbol() {
		return this.marketDataSecuritySymbol;
	}


	public void setMarketDataSecuritySymbol(String marketDataSecuritySymbol) {
		this.marketDataSecuritySymbol = marketDataSecuritySymbol;
	}


	public int getReferenceCount() {
		return this.referenceCount;
	}


	public void setReferenceCount(int referenceCount) {
		this.referenceCount = referenceCount;
	}


	public boolean isLocked() {
		return this.locked;
	}


	public void setLocked(boolean locked) {
		this.locked = locked;
	}
}
