package com.clifton.marketdata.provider.subscription.util;

import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValue;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionDataValueSet;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.SystemDataType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


/**
 * <code>MarketDataUserSubscription</code> is an object containing security subscription metadata
 * that can be used to extract subscription data from subscription events for a user.
 * <p>
 * A new instance should be created and cached for reuse for each unique user.
 *
 * @author NickK
 */
public class MarketDataUserSubscription {

	private final SecurityUser user;
	/**
	 * The client IP of the user, which is required for Bloomberg authentication with B-Pipe.
	 */
	private final String clientIpAddress;
	private final TemplateConverter templateConverter;

	/**
	 * A map of maps containing active subscription fields for subscription management and subscription event handling purposes.
	 * Each external security symbol maps to a map of external subscription field names to their {@link MarketDataField} for
	 * reverse lookup and value conversion upon subscription data events.
	 */
	private final Map<String, Map<String, MarketDataField>> securityDataFieldMap = new ConcurrentHashMap<>();

	private final Map<String, Set<MarketDataOptionTheoreticalValueSource>> securityTheoreticalValueSourceMap = new ConcurrentHashMap<>();

	private final Map<String, MarketDataUserSubscriptionSecurityReference> securityUsageReferenceMap = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a new instance for the provided user. The template converter is used to convert
	 * subscription data values based upon the associated {@link MarketDataField} conversion for the value.
	 */
	public MarketDataUserSubscription(SecurityUser user, String clientIpAddress, TemplateConverter templateConverter) {
		this.user = user;
		this.clientIpAddress = clientIpAddress;
		this.templateConverter = templateConverter;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUser getUser() {
		return this.user;
	}


	public String getClientIpAddress() {
		return this.clientIpAddress;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////                Security Usage Methods              ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the current {@link MarketDataUserSubscriptionSecurityReference} for the provided key. If there is no value for the key, null is returned.
	 */
	public MarketDataUserSubscriptionSecurityReference getSubscriptionSecurityReference(String externalSecuritySymbol) {
		return this.securityUsageReferenceMap.get(externalSecuritySymbol);
	}


	/**
	 * Returns the current {@link MarketDataUserSubscriptionSecurityReference} for the provided key. If there is no value for the key, a new reference is created and returned.
	 */
	public MarketDataUserSubscriptionSecurityReference computeSubscriptionSecurityReferenceIfAbsent(String externalSecuritySymbol, InvestmentSecurity investmentSecurity) {
		return this.securityUsageReferenceMap.computeIfAbsent(externalSecuritySymbol, key -> new MarketDataUserSubscriptionSecurityReference(getUser(), key, investmentSecurity));
	}


	public Iterator<Map.Entry<String, MarketDataUserSubscriptionSecurityReference>> getSubscriptionSecurityReferenceIterator() {
		return this.securityUsageReferenceMap.entrySet().iterator();
	}

	////////////////////////////////////////////////////////////////////////////
	//////////           Security Field Methods                /////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Registers the provided {@link MarketDataField} for the security symbol provided
	 */
	public void registerMarketDataField(String externalSecuritySymbol, MarketDataField marketDataField) {
		updateMarketDataFieldMap(externalSecuritySymbol, marketDataField, true);
	}


	/**
	 * Removes the registration for the provided {@link MarketDataField} and security symbol
	 */
	public void deregisterMarketDataField(String externalSecuritySymbol, MarketDataField marketDataField) {
		updateMarketDataFieldMap(externalSecuritySymbol, marketDataField, false);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Theoretical Value Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Registers the provided {@link MarketDataOptionTheoreticalValueSource} to the security symbol provided
	 */
	public void registerMarketDataTheoreticalValueSource(String externalSecuritySymbol, MarketDataOptionTheoreticalValueSource theoreticalValueSource) {
		updateTheoreticalValueSourceMap(externalSecuritySymbol, theoreticalValueSource, true);
	}


	/**
	 * Removes the registration for the provided {@link MarketDataOptionTheoreticalValueSource} and security symbol
	 */
	public void deregisterMarketDataTheoreticalValueSource(String externalSecuritySymbol, MarketDataOptionTheoreticalValueSource theoreticalValueSource) {
		updateTheoreticalValueSourceMap(externalSecuritySymbol, theoreticalValueSource, false);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                Subscription Value Methods         /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Extracts subscription data values from the provided data map for registered subscription security fields.
	 * Applicable values will also be applied to any registered {@link MarketDataOptionTheoreticalValueSource}s
	 */
	public MarketDataSubscriptionDataValueSet processSubscriptionData(Integer securityId, String externalSecuritySymbol, Map<String, Object> externalFieldNameToValueMap) {
		final MarketDataSubscriptionDataValueSet dataValueSet = new MarketDataSubscriptionDataValueSet();
		Map<String, MarketDataField> dataFieldMap = this.securityDataFieldMap.get(externalSecuritySymbol);
		// Guard-clause: Exit early if the data field does not have an active IMS-side subscription
		if (CollectionUtils.isEmpty(dataFieldMap)) {
			return dataValueSet;
		}

		// Process data value
		externalFieldNameToValueMap.forEach((externalFieldName, subscriptionDataValue) -> {
			MarketDataField marketDataField = dataFieldMap.get(externalFieldName);
			Object convertedValue = convertMarketDataFieldValue(marketDataField, subscriptionDataValue, externalFieldNameToValueMap);
			if (convertedValue != null) {
				// Could use the MarketDataField name, or its override value MarketDataField's name.
				// Currently using the MarketDataField's external name because the MarketDataField names do not seem very clean (e.g. "Ask Price - Subscription").
				dataValueSet.addSecurityDataValue(securityId, new MarketDataSubscriptionDataValue(externalFieldName, marketDataField.getDataType().asDataTypeNames(), convertedValue));
				applySubscriptionValueToTheoreticalValueSources(securityId, externalSecuritySymbol, marketDataField, convertedValue);
			}
		});
		applyTheoreticalValueSourceUpdatesToDataValueSet(dataValueSet);
		return dataValueSet;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////////              Management Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the subscription for the provided security symbol as inactive.
	 */
	public void setSubscriptionInactive(String externalSecuritySymbol) {
		this.securityUsageReferenceMap.compute(externalSecuritySymbol, (key, currentReference) -> {
			if (currentReference == null) {
				this.securityDataFieldMap.remove(externalSecuritySymbol);
				this.securityTheoreticalValueSourceMap.remove(externalSecuritySymbol);
				return currentReference;
			}
			if (currentReference.isActive()) {
				return currentReference;
			}

			this.securityDataFieldMap.remove(externalSecuritySymbol);
			this.securityTheoreticalValueSourceMap.remove(externalSecuritySymbol);
			currentReference.updateStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.TERMINATED);
			currentReference.unlock(true);
			return null;
		});
	}


	/**
	 * Returns true if there are no subscriptions present within this object.
	 */
	public boolean isInactive() {
		return this.securityUsageReferenceMap.isEmpty() && this.securityDataFieldMap.isEmpty() && this.securityTheoreticalValueSourceMap.isEmpty();
	}


	/**
	 * Sets any active security subscriptions as inactive clearing any subscription state on this object.
	 */
	public void setInactive() {
		Set<String> marketDataSecuritySymbolSet = new HashSet<>(this.securityDataFieldMap.keySet());
		marketDataSecuritySymbolSet.addAll(this.securityTheoreticalValueSourceMap.keySet());
		marketDataSecuritySymbolSet.addAll(this.securityUsageReferenceMap.keySet());
		marketDataSecuritySymbolSet.forEach(this::setSubscriptionInactive);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void updateMarketDataFieldMap(String externalSecuritySymbol, MarketDataField marketDataField, boolean add) {
		String[] externalFieldNames = marketDataField.getExternalFieldName().split(",");
		if (add) {
			Map<String, MarketDataField> dataFieldMap = this.securityDataFieldMap.computeIfAbsent(externalSecuritySymbol, key -> new ConcurrentHashMap<>());
			ArrayUtils.getStream(externalFieldNames)
					.forEach(externalFieldName -> dataFieldMap.put(externalFieldName, marketDataField));
		}
		else {
			this.securityDataFieldMap.computeIfPresent(externalSecuritySymbol, (key, currentValue) -> {
				ArrayUtils.getStream(externalFieldNames).forEach(currentValue::remove);
				return currentValue.isEmpty() ? null : currentValue;
			});
		}
	}


	private void updateTheoreticalValueSourceMap(String externalSecuritySymbol, MarketDataOptionTheoreticalValueSource theoreticalValueSource, boolean add) {
		if (add) {
			this.securityTheoreticalValueSourceMap.computeIfAbsent(externalSecuritySymbol, key -> ConcurrentHashMap.newKeySet())
					.add(theoreticalValueSource);
		}
		else {
			this.securityTheoreticalValueSourceMap.computeIfPresent(externalSecuritySymbol, (key, currentValue) -> {
				currentValue.remove(theoreticalValueSource);
				return currentValue.isEmpty() ? null : currentValue;
			});
		}
	}


	private void applySubscriptionValueToTheoreticalValueSources(Integer securityId, String externalSecuritySymbol, MarketDataField marketDataField, Object value) {
		CollectionUtils.getStream(this.securityTheoreticalValueSourceMap.get(externalSecuritySymbol))
				.forEach(theoreticalValueSource -> theoreticalValueSource.applyDependentSecurityFieldValue(securityId, marketDataField, value));
	}


	/**
	 * Converts a retrieved value according to the {@link MarketDataField} definition.
	 * The value will be transformed if there is a value expression and formatted to the proper precision.
	 */
	private Object convertMarketDataFieldValue(MarketDataField marketDataField, Object value, Map<String, Object> fieldDataMap) {
		if (marketDataField == null) {
			// marketDataField could be null if the field was removed from subscription and an event is being processed; return null to represent no value
			return null;
		}

		Object bloomValue = value;
		// if has a value expression, get expression value as string then convert to its specific data type.
		if (!StringUtils.isEmpty(marketDataField.getValueExpression())) {
			String expressionValue = getTemplateConverter().convertExpression(marketDataField.getValueExpression(), fieldDataMap);
			if (StringUtils.isEmpty(expressionValue)) {
				bloomValue = null; // empty string means no value: null
			}
			else if (marketDataField.getDataType().isNumeric()) {
				// evaluate the expression and return BigDecimal
				bloomValue = CoreMathUtils.evaluateExpression(expressionValue);
			}
			else {
				bloomValue = expressionValue;
			}
		}
		if (bloomValue != null) {
			if (marketDataField.getDataType().isNumeric()) {
				if (SystemDataType.INTEGER.equals(marketDataField.getDataType().getName())) {
					// value is either String or BigDecimal
					bloomValue = (bloomValue instanceof String) ? Integer.parseInt((String) bloomValue) : ((BigDecimal) bloomValue).intValue();
				}
				else {
					if (bloomValue instanceof Double) {
						bloomValue = BigDecimal.valueOf(((Double) bloomValue));
					}
					if (bloomValue instanceof BigDecimal) {
						bloomValue = ((BigDecimal) bloomValue).setScale(marketDataField.getDecimalPrecision(), RoundingMode.HALF_UP);
					}
				}
			}
		}
		return bloomValue;
	}


	/**
	 * Applies any theoretical pricing updates to the given {@link MarketDataSubscriptionDataValueSet}.
	 */
	private void applyTheoreticalValueSourceUpdatesToDataValueSet(MarketDataSubscriptionDataValueSet dataValueSet) {
		CollectionUtils.getStream(this.securityTheoreticalValueSourceMap.values())
				.flatMap(Collection::stream)
				.filter(MarketDataOptionTheoreticalValueSource::isRecalculateNecessary)
				.forEach(theoreticalValueSource -> dataValueSet.addSecurityDataValue(theoreticalValueSource.getSecurity().getId(),
						new MarketDataSubscriptionDataValue(MarketDataField.SUBSCRIPTION_EXTERNAL_FIELD_LAST_PRICE, DataTypeNames.DECIMAL, theoreticalValueSource.calculatePrice())));
	}


	///////////////////////////////////////////////////////////////////////////
	/////////////                  Helper Class                 ///////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * <code>MarketDataUserSubscriptionSecurityReference</code> is a class that tracks the reference count and usage details for a security.
	 * <p>
	 * Locking is in place to prevent multiple subscription requests to occur at the same time. Each subscription request must have its state updated before the next request can occur.
	 */
	public static final class MarketDataUserSubscriptionSecurityReference {

		/**
		 * Locking for the reference. The lock can be obtained and released by more than one thread, so
		 * a semaphore is used with the lock holder defined as a UUID that can be cleared.
		 */
		private final Semaphore lock = new Semaphore(1);
		// Use UUID for unique request. Threads can be pooled and not unique across requests
		private final AtomicReference<UUID> lockHolder = new AtomicReference<>();
		// Reference counter for security
		private final AtomicInteger counter = new AtomicInteger(0);
		// active subscription operation of the lock holder
		private final AtomicReference<MarketDataSubscriptionOperations> activeOperation = new AtomicReference<>();

		private final Short userId;
		private final String marketDataSecuritySymbol;
		private final Integer investmentSecurityId;

		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////


		MarketDataUserSubscriptionSecurityReference(SecurityUser user, String marketDataSecuritySymbol, InvestmentSecurity investmentSecurity) {
			this.userId = user.getId();
			this.marketDataSecuritySymbol = marketDataSecuritySymbol;
			this.investmentSecurityId = investmentSecurity.getId();
		}


		@Override
		public String toString() {
			return new StringBuilder("{userId: ").append(getUserId())
					.append(", marketDataSymbol: ").append(getExternalSecurityName())
					.append(", securityId: ").append(getInvestmentSecurityId()).append("}").toString();
		}


		@Override
		public boolean equals(Object object) {
			if (!(object instanceof MarketDataUserSubscriptionSecurityReference)) {
				return false;
			}
			MarketDataUserSubscriptionSecurityReference toCompare = (MarketDataUserSubscriptionSecurityReference) object;
			return CompareUtils.isEqual(getUserId(), toCompare.getUserId())
					&& CompareUtils.isEqual(getInvestmentSecurityId(), toCompare.getInvestmentSecurityId())
					&& CompareUtils.isEqual(getExternalSecurityName(), toCompare.getExternalSecurityName());
		}


		@Override
		public int hashCode() {
			return Objects.hash(
					getUserId(),
					getInvestmentSecurityId(),
					getExternalSecurityName()
			);
		}

		///////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////


		public Short getUserId() {
			return this.userId;
		}


		public String getExternalSecurityName() {
			return this.marketDataSecuritySymbol;
		}


		public Integer getInvestmentSecurityId() {
			return this.investmentSecurityId;
		}


		/**
		 * Attempts to obtain a lock for this security reference between other threads. If a lock is obtained, updates the reference's usage count and returns true.
		 * If the attempt to lock fails, no updates are made to this reference and false is returned.
		 * The usage count will be incremented for {@link MarketDataSubscriptionOperations#SUBSCRIBE} operations and decremented for {@link MarketDataSubscriptionOperations#UNSUBSCRIBE} operations.
		 * <p>
		 * The UUID is provided to differentiate between different requests. The same UUID should be used for each unique request.
		 */
		public boolean registerUsage(UUID uuid, MarketDataSubscriptionOperations operation) {
			if (acquireSubscriptionLock(uuid)) {
				// hold the lock, so set the request state and adjust reference counter
				this.activeOperation.set(operation);
				if (operation == MarketDataSubscriptionOperations.SUBSCRIBE) {
					this.counter.incrementAndGet();
				}
				else {
					// only decrement to 0 to avoid decrementing negative
					this.counter.updateAndGet(current -> current < 1 ? 0 : --current);
				}
				return true;
			}
			return false;
		}


		/**
		 * Updates the security reference's usage status for the provided user.
		 * <p>
		 * If an {@link MarketDataSubscriptionOperations} is completed for the user, the operation value is returned. Null is returned otherwise.
		 */
		public MarketDataSubscriptionOperations updateStatus(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses status) {
			return releaseSubscriptionLock(status);
		}


		public int getUsageCount() {
			return this.counter.get();
		}


		public boolean isActive() {
			return getUsageCount() > 0 || this.lock.hasQueuedThreads() || isLocked();
		}


		public boolean isLocked() {
			return this.lock.availablePermits() < 1;
		}


		public boolean unlock() {
			return unlock(false);
		}


		private boolean unlock(boolean force) {
			if (isLocked() && !this.lock.hasQueuedThreads()) {
				releaseSubscriptionLock(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.ACTIVE);
			}
			if (force && isLocked() && !this.lock.hasQueuedThreads()) {
				synchronized (this) {
					this.lockHolder.set(null);
					this.lock.release();
				}
			}
			return !isLocked();
		}


		/**
		 * Acquires an available lock for the provided UUID. Returns true if the lock is obtained or is already held by the UUID, otherwise false.
		 */
		private boolean acquireSubscriptionLock(UUID uuid) {
			synchronized (this) {
				// Do to possibility of parallel threads preparing a subscription command, multiple threads with the same UUID could get here at the same time.
				// Threads with the same uuid, should be treated as the same. Thus, since only one can obtain the lock, we  must check if the holder is the same.
				// The double checking of lock holder is necessary because we have two objects (lockHolder and lock) that can not be atomically changed without excessive blocking.
				if (this.lock.tryAcquire()) {
					this.lockHolder.set(uuid);
					return true;
				}
				else if (this.lockHolder.get() == uuid) {
					return true;
				}
			}
			try {
				// Lock could not be acquired immediately and is held by a different holder. Try to wait a few seconds for the lock to be released to prevent concurrent subscription requests with the same security.
				// Release of a lock occurs after a subscription command is processed by the remote marketdata provider service (e.g. BPipe).
				if (this.lock.tryAcquire(3, TimeUnit.SECONDS)) {
					this.lockHolder.set(uuid);
					return true;
				}
				LogUtils.warn(getClass(), "Timed out waiting to perform subscription request: (id: " + uuid + ", holder: " + this.lockHolder.get() + "); reference: " + toString());
				return false;
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				return false;
			}
		}


		private MarketDataSubscriptionOperations releaseSubscriptionLock(MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses status) {
			MarketDataSubscriptionOperations endedOperation = this.activeOperation.getAndUpdate(currentValue -> {
				if (status == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.PENDING) {
					// action has not been completed
					return currentValue;
				}
				if (status == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.INACTIVE
						|| status == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.FAILED
						|| status == MarketDataSubscriptionSecurityData.MarketDataSubscriptionStatuses.TERMINATED
						|| status == null) {
					// set the counter to 0 as all references became inactive
					this.counter.set(0);
				}
				synchronized (this) {
					this.lockHolder.set(null);
					this.lock.release();
				}
				return null;
			});
			return endedOperation;
		}
	}
}
