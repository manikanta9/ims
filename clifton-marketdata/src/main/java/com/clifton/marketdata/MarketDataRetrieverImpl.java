package com.clifton.marketdata;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentPricingFrequencies;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldTypes;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataRetrieverImpl</code> contain helpful methods for dealing with market data look ups
 * and calculations against market data.  This class is not a service, i.e. NOT exposed to UI but used
 * within code only
 * <p>
 * Examples: Get Security Return for Given Date Range, CCY Return for Date Range, etc.
 *
 * @author manderson
 */
@Component
public class MarketDataRetrieverImpl implements MarketDataRetriever {

	public static final String MARKET_DATASOURCE_BLOOMBERG = "Bloomberg";
	public static final int MARKET_DATA_MAX_AGE_IN_SECONDS = 180;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataLiveService marketDataLiveService;
	private MarketDataRatesRetriever marketDataRatesRetriever;

	////////////////////////////////////////////////////////////////////////////
	///////                       Price Lookup Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getPrice(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		return getPriceImpl(security, date, false, false, exceptionIfMissing, exceptionMessagePrefix);
	}


	@Override
	public MarketDataValueHolder getPriceMarketDataValueHolder(InvestmentSecurity security, Date date, boolean flexible, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		return getPriceObjectImpl(security, date, flexible, exceptionIfMissing, exceptionMessagePrefix);
	}


	@Override
	public MarketDataValueHolder getOfficialPriceFlexible(InvestmentSecurity security, Date date, Integer maxDaysBackAllowed) {
		MarketDataPriceFieldMapping priceFieldMapping = getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(security.getInstrument());

		MarketDataField officialPriceField = MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(priceFieldMapping, getMarketDataFieldService());
		MarketDataSource officialDataSource = MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSource(priceFieldMapping);

		if (officialPriceField != null) {
			MarketDataValueHolder result = getMarketDataPriceForField(security, officialPriceField, officialDataSource, date, true, false);
			if (result == null) {
				throw new ValidationException("Security [" + security.getLabelWithBigSecurity() + "] price data [" + officialPriceField.getName() + (officialDataSource == null ? "" : " (" + officialDataSource.getName() + ")")
						+ "] is missing on or before date [" + DateUtils.fromDateShort(date) + "]");
			}
			if (maxDaysBackAllowed != null) {
				// Note: Compare Date to Measure Date to get Days Difference as Positive Value
				int daysBack = DateUtils.getDaysDifference(date, result.getMeasureDate());
				if (daysBack > maxDaysBackAllowed) {
					throw new ValidationException("Security [" + security.getLabelWithBigSecurity() + "] latest price data [" + officialPriceField.getName() + (officialDataSource == null ? "" : " (" + officialDataSource.getName() + ")")
							+ "] on or before date [" + DateUtils.fromDateShort(date) + "] is entered on [" + DateUtils.fromDateShort(result.getMeasureDate()) + "] which is " + daysBack + " days behind and the maximum allowed days back is " + maxDaysBackAllowed);
				}
			}
			return result;
		}
		// If there is no official price field available, just return null
		return null;
	}


	@Override
	public Date getPriceDate(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		MarketDataValueHolder priceObject = getPriceObjectImpl(security, date, false, exceptionIfMissing, exceptionMessagePrefix);
		return (priceObject == null) ? null : priceObject.getMeasureDate();
	}


	@Override
	public BigDecimal getPriceFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getPriceImpl(security, date, false, true, exceptionIfMissing, null);
	}


	@Override
	public BigDecimal getPriceAdjusted(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		return getPriceImpl(security, date, true, false, exceptionIfMissing, exceptionMessagePrefix);
	}


	@Override
	public BigDecimal getPriceAdjustedFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getPriceImpl(security, date, true, true, exceptionIfMissing, null);
	}


	private BigDecimal getPriceImpl(InvestmentSecurity security, Date date, boolean adjustedPrice, boolean previousIfMissing, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		MarketDataValueHolder priceObject = getPriceObjectImpl(security, date, previousIfMissing, exceptionIfMissing, exceptionMessagePrefix);
		return (priceObject == null) ? null : ((adjustedPrice) ? priceObject.getMeasureValueAdjusted() : priceObject.getMeasureValue());
	}


	private MarketDataValueHolder getPriceObjectImpl(InvestmentSecurity security, Date date, boolean previousIfMissing, boolean exceptionIfMissing, String exceptionMessagePrefix) {
		// Currencies don't use Prices and Hierarchies flagged as not real securities don't use prices
		if (security.isCurrency() || security.getInstrument().getHierarchy().isNotASecurity()) {
			return null;
		}

		// If it's not a flexible look up, but the security is not priced daily - need to do a flexible look up with validation
		Date flexibleStartDate = null;
		if (!previousIfMissing) {
			InvestmentPricingFrequencies pricingFrequency = security.getInstrument().getHierarchy().getPricingFrequency();
			if (pricingFrequency != InvestmentPricingFrequencies.DAILY) {
				previousIfMissing = true;
				short calendarId = getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId();
				flexibleStartDate = pricingFrequency.getMinimumPriceDateForDate(date, calendarId, getCalendarBusinessDayService());
				// If flexible start date happens to be on or AFTER date (would be the case for last business day of month vs. last day of month) - move date forward so we make sure we capture month end prices
				if (DateUtils.compare(flexibleStartDate, date, false) >= 0) {
					date = flexibleStartDate;
				}
				// Move flexible start date back to the latest business day to it's date
				flexibleStartDate = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.addDays(flexibleStartDate, 1), calendarId));
			}
		}

		try {
			// start with "Official Price"
			MarketDataValueHolder result = null;
			MarketDataPriceFieldMapping priceFieldMapping = getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(security.getInstrument());

			MarketDataField officialPriceField = MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(priceFieldMapping, getMarketDataFieldService());
			MarketDataSource officialDataSource = MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSource(priceFieldMapping);
			MarketDataField closingPriceField = null;
			MarketDataSource closingDataSource = null;
			MarketDataField latestPriceField = null;
			MarketDataSource latestDataSource = null;

			if (officialPriceField != null) {
				// never throw an exception if Official Price is missing (it maybe not known until much later and it's ok to use Closing Price)
				result = getMarketDataPriceForField(security, officialPriceField, officialDataSource, date, previousIfMissing, false);
			}

			if (result == null || DateUtils.compare(date, result.getMeasureDate(), false) != 0) {
				// exact match was not found: continue looking for a better price
				// lookup "Closing Price" next
				closingPriceField = MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(priceFieldMapping, getMarketDataFieldService());
				// if both fields and both data sources are the same (or closing has a specific data source but official doesn't), no point of looking up
				if (closingPriceField != null) {
					closingDataSource = MarketDataPriceFieldTypes.CLOSING.getPriceDataSource(priceFieldMapping);
					// Only throw an exception here if exceptionIsMissing is true and we aren't going to check latest price field too.  Otherwise throwing the exception is handled at the end of this method if both closing and last price fields are missing
					MarketDataValueHolder closingPrice = getMarketDataPriceForField(security, closingPriceField, closingDataSource, date, previousIfMissing, (exceptionIfMissing && !previousIfMissing));
					if (closingPrice != null) {
						if (result == null || DateUtils.compare(closingPrice.getMeasureDate(), result.getMeasureDate(), false) > 0) {
							result = closingPrice;
						}
					}
				}

				// if flexible lookup and exact match on the date is not found, check if more recent "Latest Price" exists
				if (previousIfMissing) {
					if (result == null || DateUtils.compare(date, result.getMeasureDate(), false) != 0) {
						// lookup "Latest Price" as the last resort
						latestPriceField = MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(priceFieldMapping, getMarketDataFieldService());
						if (latestPriceField != null) {
							latestDataSource = MarketDataPriceFieldTypes.LATEST.getPriceDataSource(priceFieldMapping);
							// Don't throw an exception here if missing - checked and thrown before returning the final result
							MarketDataValueHolder latestPrice = getMarketDataPriceForField(security, latestPriceField, latestDataSource, date, previousIfMissing, false);
							if (latestPrice != null) {
								// if latest price is after closing price
								if (result == null || DateUtils.compare(latestPrice.getMeasureDate(), result.getMeasureDate(), false) > 0) {
									result = latestPrice;
								}
							}
						}
					}
				}
			}

			if (flexibleStartDate != null && result != null) {
				// If result is before flexible start date - then considered to be missing
				if (DateUtils.compare(result.getMeasureDate(), flexibleStartDate, false) < 0) {
					result = null;
				}
			}

			if (result == null && exceptionIfMissing) {
				throw new ValidationException("Security [" + security.getLabelWithBigSecurity() + "] price data [" + (officialPriceField == null ? "" : officialPriceField.getName() + (officialDataSource == null ? "" : " (" + officialDataSource.getName() + ")"))
						+ (closingPriceField == null ? "" : closingPriceField.getName() + (closingDataSource == null ? "" : " (" + closingDataSource.getName() + ")"))
						+ (latestPriceField != null && (!latestPriceField.equals(closingPriceField) || !CompareUtils.isEqual(latestDataSource, closingDataSource)) ? " or " + latestPriceField.getName() + (latestDataSource == null ? "" : "(" + latestDataSource.getName() + ")") : "") + "] is missing on or before date ["
						+ DateUtils.fromDateShort(date) + "]" + (flexibleStartDate != null ? " and on or after [" + DateUtils.fromDateShort(flexibleStartDate) + "]." : "."));
			}
			return result;
		}
		catch (ValidationException e) {
			String message = (exceptionMessagePrefix == null) ? e.getMessage() : exceptionMessagePrefix + " " + e.getMessage();
			throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), message);
		}
	}


	private MarketDataValueHolder getMarketDataPriceForField(InvestmentSecurity security, MarketDataField securityPriceField, MarketDataSource dataSource, Date date, boolean previousIfMissing, boolean exceptionIfMissing) {
		MarketDataValueHolder value = getMarketDataPriceObjectImpl(security, date, previousIfMissing, securityPriceField, dataSource);
		// If value is missing on date (and not a flexible search) see if it's a holiday and get previous bd price
		if (value == null) {
			// Flexible Searches with a null value means there are NO previous values either
			if (previousIfMissing) {
				if (exceptionIfMissing) {
					throw new ValidationException("Security [" + security.getLabelWithBigSecurity() + "] price data [" + securityPriceField.getName() + "] is missing on or before date ["
							+ DateUtils.fromDateShort(date) + "].");
				}
				return null;
			}

			// Exact Date searches and value is null, check holiday & previous business day
			// See if it's a holiday
			Calendar calendar = null;

			//check underlying
			InvestmentInstrument underlying = security.getInstrument().getUnderlyingInstrument();

			if (underlying != null && underlying.getExchange() != null) {
				calendar = underlying.getExchange().getCalendar();
				value = calculateValueForHolidayOnCalendar(date, calendar, security, securityPriceField, dataSource); //check if underlying exchange is on holiday
			}

			if (value == null) {
				Calendar tempCal = getInvestmentCalculator().getInvestmentSecurityCalendar(security);
				if (tempCal != calendar) {
					calendar = tempCal;
					value = calculateValueForHolidayOnCalendar(date, calendar, security, securityPriceField, dataSource); //check if initial exchange is on holiday
				}
			}

			if (value == null) {
				if (exceptionIfMissing) {
					throw new ValidationException("Security [" + security.getLabelWithBigSecurity() + "] price data [" + securityPriceField.getName() + "] is missing on date ["
							+ DateUtils.fromDateShort(date) + "]. " + DateUtils.fromDateShort(date) + " is not a holiday for the calendar [" + (calendar != null ? (calendar.getName()) : " calendar not set ") + "].");
				}
				return null;
			}
		}

		return value;
	}

	////////////////////////////////////////////////////////////////////////////
	///////              Other Data Fields Lookup Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getDV01Flexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getSecurityDataFieldValueFlexible(security, date, MarketDataField.FIELD_DV01, exceptionIfMissing);
	}


	@Override
	public BigDecimal getDV01SpotFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getSecurityDataFieldValueFlexible(security, date, MarketDataField.FIELD_DV01_SPOT, exceptionIfMissing);
	}


	private MarketDataValueHolder calculateValueForHolidayOnCalendar(Date date, Calendar calendar, InvestmentSecurity security, MarketDataField securityPriceField, MarketDataSource dataSource) {
		MarketDataValueHolder value = null;

		if (!getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, calendar.getId()))) {
			// If it is a holiday, get the previous business day
			date = getCalendarBusinessDayService().getBusinessDayFrom(CalendarBusinessDayCommand.forDate(date, calendar.getId()), -1);
			value = getMarketDataPriceObjectImpl(security, date, false, securityPriceField, dataSource);
		}
		return value;
	}


	/**
	 * Returns MarketDataValue object representing the given {@link InvestmentSecurity} price for a given date.
	 * 1.  Currencies don't have prices, so returns null if the given security is a currency
	 * 2.  Looks at Price field mapped to the security for the date
	 * 3.  Uses Normalized Pricing, so look ups BIG price if necessary
	 * 4.  If missing on the given date, and previousIfMissing is true, will do a flexible search for the two price fields and return the most recent one
	 */
	private MarketDataValueHolder getMarketDataPriceObjectImpl(InvestmentSecurity security, Date date, boolean previousIfMissing, MarketDataField priceField, MarketDataSource dataSource) {
		// Currencies don't use Prices
		if (security.isCurrency()) {
			return null;
		}
		return getMarketDataFieldService().getMarketDataValueForDate(security, date, priceField.getName(), (dataSource != null ? dataSource.getId() : null), previousIfMissing, true);
	}


	@Override
	public BigDecimal getDurationFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getDurationFlexible(security, date, null, exceptionIfMissing);
	}


	@Override
	public BigDecimal getDurationFlexible(InvestmentSecurity security, Date date, String fieldNameOverride, boolean exceptionIfMissing) {
		String fieldName = StringUtils.isEmpty(fieldNameOverride) ? MarketDataField.FIELD_DURATION : fieldNameOverride;
		return getSecurityDataFieldValueFlexible(security, date, fieldName, exceptionIfMissing);
	}


	@Override
	public BigDecimal getDeltaFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		return getSecurityDataFieldValueFlexible(security, date, MarketDataField.FIELD_DELTA, exceptionIfMissing);
	}


	@Override
	public BigDecimal getDeltaOnDateOrFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		// If the security is priced daily, require Delta for the date, otherwise do a flexible retrieval
		InvestmentPricingFrequencies pricingFrequency = security.getInstrument().getHierarchy().getPricingFrequency();
		if (pricingFrequency != InvestmentPricingFrequencies.DAILY) {
			return getDeltaFlexible(security, date, exceptionIfMissing);
		}

		BigDecimal fieldValue = getSecurityDataFieldValue(security, date, MarketDataField.FIELD_DELTA, false);
		if (fieldValue != null) {
			return fieldValue;
		}

		// Handle case where the InvestmentPricingFrequency is DAILY, but the date was for a weekend or holiday.  In such cases try to get price for the previous business day.
		Calendar securityCalendar = getInvestmentCalculator().getInvestmentSecurityCalendar(security);
		boolean businessDay = getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(date, securityCalendar.getId()));

		if (!businessDay) {
			Date previousBusinessDay = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(date, securityCalendar.getId()));
			return getSecurityDataFieldValue(security, previousBusinessDay, MarketDataField.FIELD_DELTA, exceptionIfMissing);
		}

		// if the original date was a business day and exceptionIfMissing is true, throw same exception that getSecurityDataFieldValue would have thrown.
		if (exceptionIfMissing) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), "Missing " + MarketDataField.FIELD_DELTA + " for security [" + security.getLabel() + "] on [" + DateUtils.fromDateShort(date) + "].");
		}

		return null;
	}


	@Override
	public BigDecimal getCreditDuration(InvestmentSecurity security, Date date, boolean exceptionIfMissing) {
		try {
			return getSecurityDataFieldValueFlexible(security, date, MarketDataField.FIELD_CREDIT_DURATION, exceptionIfMissing);
		}
		catch (ValidationException e) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), e.getMessage());
		}
	}


	@Override
	public BigDecimal getFutureFairValue(InvestmentSecurity security, Date date) {
		// Note: These only apply to Equity Index Futures - not sure if we can add a mapping check to only lookup if it applies?
		if (security.getInstrument().isFairValueAdjustmentUsed()) {
			return getSecurityDataFieldValueFlexible(security, date, MarketDataField.FIELD_FUTURE_FAIR_VALUE, false);
		}
		return null;
	}


	@Override
	public BigDecimal getSecurityDataFieldValueFlexible(InvestmentSecurity security, Date date, String fieldName, boolean exceptionIfMissing) {
		MarketDataValueHolder value = getMarketDataFieldService().getMarketDataValueForDateFlexible(security.getId(), date, fieldName);
		if (value != null) {
			return value.getMeasureValue();
		}
		if (exceptionIfMissing) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), "Missing " + fieldName + " for security [" + security.getLabel() + "] on or before [" + DateUtils.fromDateShort(date) + "].");
		}
		return null;
	}


	@Override
	public BigDecimal getSecurityDataFieldValue(InvestmentSecurity security, Date date, String fieldName, boolean exceptionIfMissing) {
		MarketDataValueHolder value = getMarketDataFieldService().getMarketDataValueForDate(security.getId(), date, fieldName);
		if (value != null) {
			return value.getMeasureValue();
		}
		if (exceptionIfMissing) {
			throw new ValidationExceptionWithCause("InvestmentSecurity", security.getId(), "Missing " + fieldName + " for security [" + security.getLabel() + "] on [" + DateUtils.fromDateShort(date) + "].");
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	/////              Market Data Return Calculation Methods             //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getInvestmentSecurityReturn(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix, boolean returnAsHundredsValue) {
		return getInvestmentSecurityReturnImpl(account, security, startDate, endDate, exceptionMessagePrefix, returnAsHundredsValue, false);
	}


	@Override
	public BigDecimal getInvestmentSecurityReturnAnnualized(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix,
	                                                        boolean returnAsHundredsValue, boolean dailyNotMonthlyCalculation) {
		BigDecimal returnValue = getInvestmentSecurityReturnImpl(account, security, startDate, endDate, exceptionMessagePrefix, returnAsHundredsValue, false);
		return CoreMathUtils.annualizeByDateRange(returnValue, startDate, endDate, dailyNotMonthlyCalculation, returnAsHundredsValue);
	}


	@Override
	public BigDecimal getInvestmentSecurityReturnFlexible(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix,
	                                                      boolean returnAsHundredsValue) {
		return getInvestmentSecurityReturnImpl(account, security, startDate, endDate, exceptionMessagePrefix, returnAsHundredsValue, true);
	}


	private BigDecimal getInvestmentSecurityReturnImpl(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix,
	                                                   boolean returnAsHundredsValue, boolean useFlexiblePriceLookup) {

		// Currencies don't have prices, so % change is based on Exchange Rate Changes for the given dates
		if (security != null && security.isCurrency()) {
			return getMarketDataRatesRetriever().getExchangeRateReturn(account, security, startDate, endDate, exceptionMessagePrefix, returnAsHundredsValue, useFlexiblePriceLookup);
		}
		// Use Adjusted Prices
		BigDecimal startValue = getPriceImpl(security, startDate, true, useFlexiblePriceLookup, true, exceptionMessagePrefix);
		BigDecimal endValue = getPriceImpl(security, endDate, true, useFlexiblePriceLookup, true, exceptionMessagePrefix);
		return MathUtils.getPercentChange(startValue, endValue, returnAsHundredsValue);
	}

	////////////////////////////////////////////////////////////////////////////
	///////        Investment Security Market Data Live Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive getLivePrice(InvestmentSecurity security) {
		if (security.isCurrency()) {
			return null;
		}
		if (security.isActive()) {
			try {
				MarketDataLiveCommand command = new MarketDataLiveCommand();
				command.setMaxAgeInSeconds(MARKET_DATA_MAX_AGE_IN_SECONDS);
				return getMarketDataLiveService().getMarketDataLivePrice(security.getId(), command);
			}
			catch (Exception e) {
				// TODO: Don't throw the error if we can't get live price for a security - if it's a real exception log it, but want to return
				// to user all cases where live price is available to get.  Will need to research securities/cases where it's not working...
				LogUtils.errorOrInfo(getClass(), "Unable to lookup live market data for security [" + security.getLabel() + "]: " + e.getMessage(), e);
				return null;
			}
		}
		// If Security is Expired, get the value as of the End Date for the Security
		else if (security.getEndDate() != null) {
			BigDecimal value = getPrice(security, security.getEndDate(), false, null);
			if (value != null) {
				return new MarketDataLive(security.getId(), value, security.getEndDate());
			}
		}
		return null;
	}


	@Override
	public MarketDataLive getLiveFutureFairValue(InvestmentSecurity security) {
		// Note: These only apply to Equity Index Futures - not sure if we can add a mapping check to only lookup if it applies?
		if (security.getInstrument().isFairValueAdjustmentUsed()) {
			MarketDataLiveCommand command = new MarketDataLiveCommand();
			command.setMaxAgeInSeconds(MARKET_DATA_MAX_AGE_IN_SECONDS);
			return getMarketDataLiveService().getMarketDataLiveFieldValue(security.getId(), MarketDataField.FIELD_FUTURE_FAIR_VALUE, command);
		}
		return null;
	}


	@Override
	public MarketDataLive getLiveDelta(InvestmentSecurity security) {
		if (InvestmentUtils.isSecurityOfType(security, InvestmentType.OPTIONS)) {
			if (security.isActive()) {
				try {
					MarketDataLiveCommand command = new MarketDataLiveCommand();
					command.setMaxAgeInSeconds(MARKET_DATA_MAX_AGE_IN_SECONDS);
					return getMarketDataLiveService().getMarketDataLiveFieldValue(security.getId(), MarketDataField.FIELD_DELTA, command);
				}
				catch (Exception e) {
					LogUtils.errorOrInfo(getClass(), "Unable to lookup live delta data for security [" + security.getLabel() + "]: " + e.getMessage(), e);
				}
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
