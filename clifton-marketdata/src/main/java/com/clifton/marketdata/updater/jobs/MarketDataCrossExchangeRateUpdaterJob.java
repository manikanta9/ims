package com.clifton.marketdata.updater.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.marketdata.rates.search.ExchangeRateSearchForm;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Batch job that is responsible for calculating a cross rate from two known exchange rates of three currencies.
 * A rate can be retrieved for one of many defined {@link MarketDataSource}s for calculating the cross rate. We do
 * not always have rates for every currency pair, but often have enough rates from similar sources to derive rates
 * <p>
 * An example of calculating a cross rate between three currencies: We want an exchange rate from ILS to AUD, but it
 * is missing. We have exchange rates from ILS to USD and USD to AUD both from Goldman Sachs. To calculate a cross
 * rate for ILS to AUD, we can multiply the exchange rate for ILS to USD with the exchange rate for USD to AUD.
 *
 * @author nickk
 */
public class MarketDataCrossExchangeRateUpdaterJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private InvestmentInstrumentService investmentInstrumentService;

	private MarketDataRatesService marketDataRatesService;
	private MarketDataSourceService marketDataSourceService;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * One or more Market Data Sources for retrieving exchange rates used in cross rate calculations
	 */
	private Short[] sourceMarketDataSourceIds;
	/**
	 * The Market Data Sources for saving calculated cross exchange rates
	 */
	private Short destinationMarketDataSourceId;

	/**
	 * Currencies used in the cross rate calculations. Calculated cross rates will be between from and to currencies.
	 * The cross rate is calculated using the rates between from->common pair and common->to pair.
	 */
	private Integer fromCurrencyId;
	private Integer commonCurrencyId;
	private Integer toCurrencyId;

	/**
	 * Number of calendar days back for which market data values should be copied.
	 */
	private int daysBackIncludingToday;

	/**
	 * If true, calculate cross exchange rate by multiplying the two exchange rates for common currency pairs.
	 * Otherwise, divide the first currency pair rate by the second for the cross rate.
	 */
	private boolean multiplyRates;
	/**
	 * If true, overwrite existing market data values on each day with the latest-applied market data value for that
	 * day. Otherwise, only non-existent market data values will be created.
	 */
	private boolean overwriteExistingRate;

	private StatusHolderObject<Status> statusHolder;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		validateMarketDataSourceProperties();
		validateCurrencyProperties();
		ValidationUtils.assertTrue(getDaysBackIncludingToday() > 0 && getDaysBackIncludingToday() < 365, "Business Days Back must be greater than 0 and less than 365");
	}


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();

		MarketDataSource destinationMarketDataSource = getMarketDataSourceService().getMarketDataSource(getDestinationMarketDataSourceId());
		InvestmentSecurity fromCurrency = getInvestmentInstrumentService().getInvestmentSecurity(getFromCurrencyId());
		InvestmentSecurity commonCurrency = getInvestmentInstrumentService().getInvestmentSecurity(getCommonCurrencyId());
		InvestmentSecurity toCurrency = getInvestmentInstrumentService().getInvestmentSecurity(getToCurrencyId());

		int inserts = 0;
		int updates = 0;
		Set<Date> skipDateSet = new LinkedHashSet<>();
		Date endDate = new Date();
		int daysBack = -getDaysBackIncludingToday();
		Date startDate = DateUtils.addDays(endDate, daysBack);

		Map<Date, MarketDataExchangeRate> currentDestinationExchangeRateByDateMap = getDataSourceExchangeRateForDateRange(destinationMarketDataSource, fromCurrency, toCurrency, startDate, endDate);
		Map<Date, BigDecimal> firstPairRateByDateMap = getSourceExchangeRateForDateRange(fromCurrency, commonCurrency, startDate, endDate);
		Map<Date, BigDecimal> secondPairRateByDateMap = getSourceExchangeRateForDateRange(commonCurrency, toCurrency, startDate, endDate);

		Date processingDate = DateUtils.clearTime(startDate);
		while (DateUtils.isDateBefore(processingDate, endDate, false)) {
			status.setMessage("Processing date: " + DateUtils.fromDateShort(processingDate) + " ( " + DateUtils.getDaysDifference(endDate, processingDate) + " of " + getDaysBackIncludingToday() + ")");

			MarketDataExchangeRate currentExchangeRate = currentDestinationExchangeRateByDateMap.get(processingDate);
			if (currentExchangeRate == null || isOverwriteExistingRate()) {
				BigDecimal firstPairRate = firstPairRateByDateMap.get(processingDate);
				BigDecimal secondPairRate = secondPairRateByDateMap.get(processingDate);

				if (firstPairRate == null || secondPairRate == null) {
					StringBuilder errorDetail = new StringBuilder("Cross Exchange Rate calculation failed for ")
							.append(DateUtils.fromDateShort(processingDate))
							.append(" because a currency pair rate is missing: ");
					if (firstPairRate == null) {
						errorDetail.append(fromCurrency.getSymbol()).append(" to ").append(commonCurrency.getSymbol());
					}
					if (secondPairRate == null) {
						if (firstPairRate == null) {
							errorDetail.append(" and ");
						}
						errorDetail.append(commonCurrency.getSymbol()).append(" to ").append(toCurrency.getSymbol());
					}
					status.addError(errorDetail.toString());
				}
				else {
					BigDecimal crossRate = MathUtils.round(isMultiplyRates() ? MathUtils.multiply(firstPairRate, secondPairRate) : MathUtils.divide(firstPairRate, secondPairRate), DataTypes.EXCHANGE_RATE.getPrecision());

					MarketDataExchangeRate exchangeRateToSave;
					if (currentExchangeRate != null) {
						exchangeRateToSave = currentExchangeRate;
						exchangeRateToSave.setExchangeRate(crossRate);

						updates++;
					}
					else {
						exchangeRateToSave = new MarketDataExchangeRate();
						exchangeRateToSave.setDataSource(destinationMarketDataSource);
						exchangeRateToSave.setExchangeRate(crossRate);
						exchangeRateToSave.setFromCurrency(fromCurrency);
						exchangeRateToSave.setToCurrency(toCurrency);
						exchangeRateToSave.setRateDate(processingDate);

						inserts++;
					}

					getMarketDataRatesService().saveMarketDataExchangeRate(exchangeRateToSave);
				}
			}
			else {
				skipDateSet.add(processingDate);
			}

			processingDate = DateUtils.clearTime(DateUtils.addDays(processingDate, 1));
		}

		StringBuilder message = new StringBuilder("Successfully inserted ")
				.append(inserts).append(" and updated ").append(updates).append(" Cross Exchange Rates for ")
				.append(getDaysBackIncludingToday()).append(" days.");
		if (!skipDateSet.isEmpty()) {
			message.append(" Skipped dates [")
					.append(StringUtils.join(skipDateSet, DateUtils::fromDateShort, ", "))
					.append("] because rates exist and were not to be overwritten.");
		}
		status.setMessage(message.toString());
		return status;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void validateMarketDataSourceProperties() {
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(getSourceMarketDataSourceIds()), "At least one Market Data Source is required to pull Exchange Rates from for Cross Rate Calculation");
		ValidationUtils.assertNotNull(getDestinationMarketDataSourceId(), "The Market Data Source to save calculated Cross Rates is required");
	}


	private void validateCurrencyProperties() {
		validateCurrency(getFromCurrencyId(), "From");
		validateCurrency(getCommonCurrencyId(), "Common Intermediate");
		validateCurrency(getToCurrencyId(), "To");
		ValidationUtils.assertTrue(CollectionUtils.getSize(CollectionUtils.createHashSet(getFromCurrencyId(), getCommonCurrencyId(), getToCurrencyId())) == 3, "The three currencies defined must be three unique currencies.");
	}


	private void validateCurrency(Integer currencyId, String currencyLabel) {
		ValidationUtils.assertNotNull(currencyId, "The " + currencyLabel + " Currency of the Cross Rate Calculation is required");
		InvestmentSecurity currency = getInvestmentInstrumentService().getInvestmentSecurity(currencyId);
		ValidationUtils.assertTrue(currency.isCurrency(), "The " + currencyLabel + " Currency is not a currency");
	}


	private Map<Date, BigDecimal> getSourceExchangeRateForDateRange(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date startDate, Date endDate) {
		List<MarketDataExchangeRate> exchangeRateList = getExchangeRatesForDateRange(fromCurrency, toCurrency, startDate, endDate, getSourceMarketDataSourceIds());

		if (CollectionUtils.isEmpty(exchangeRateList)) {
			return Collections.emptyMap();
		}

		Map<Date, List<MarketDataExchangeRate>> rateListByDateMap = BeanUtils.getBeansMap(exchangeRateList, MarketDataExchangeRate::getRateDate);
		Map<Date, BigDecimal> rateByDateMap = new HashMap<>();
		rateListByDateMap.forEach((date, rateList) -> {
			MarketDataExchangeRate exchangeRate = rateList.stream().filter(rate -> getDestinationMarketDataSourceId().equals(rate.getDataSource().getId()))
					.findFirst()
					.orElse(rateList.get(0));
			rateByDateMap.put(date, exchangeRate.getExchangeRate());
		});
		return rateByDateMap;
	}


	private Map<Date, MarketDataExchangeRate> getDataSourceExchangeRateForDateRange(MarketDataSource marketDataSource, InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date startDate, Date endDate) {
		List<MarketDataExchangeRate> exchangeRateList = getExchangeRatesForDateRange(fromCurrency, toCurrency, startDate, endDate, marketDataSource.getId());

		if (CollectionUtils.isEmpty(exchangeRateList)) {
			return Collections.emptyMap();
		}
		return BeanUtils.getBeanMap(exchangeRateList, MarketDataExchangeRate::getRateDate);
	}


	private List<MarketDataExchangeRate> getExchangeRatesForDateRange(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date startDate, Date endDate, Short... marketDataSourceIds) {
		ExchangeRateSearchForm exchangeRateSearchForm = new ExchangeRateSearchForm();
		if (marketDataSourceIds.length == 1) {
			exchangeRateSearchForm.setDataSourceId(marketDataSourceIds[0]);
		}
		else if (marketDataSourceIds.length > 1) {
			exchangeRateSearchForm.setDataSourceIds(marketDataSourceIds);
		}
		exchangeRateSearchForm.setFromCurrencyId(fromCurrency.getId());
		exchangeRateSearchForm.setToCurrencyId(toCurrency.getId());
		exchangeRateSearchForm.setMinRateDate(DateUtils.addDays(startDate, -1));
		exchangeRateSearchForm.setMaxRateDate(DateUtils.addDays(endDate, 1));
		return getMarketDataRatesService().getMarketDataExchangeRateList(exchangeRateSearchForm);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}

///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short[] getSourceMarketDataSourceIds() {
		return this.sourceMarketDataSourceIds;
	}


	public void setSourceMarketDataSourceIds(Short[] sourceMarketDataSourceIds) {
		this.sourceMarketDataSourceIds = sourceMarketDataSourceIds;
	}


	public Short getDestinationMarketDataSourceId() {
		return this.destinationMarketDataSourceId;
	}


	public void setDestinationMarketDataSourceId(Short destinationMarketDataSourceId) {
		this.destinationMarketDataSourceId = destinationMarketDataSourceId;
	}


	public Integer getFromCurrencyId() {
		return this.fromCurrencyId;
	}


	public void setFromCurrencyId(Integer fromCurrencyId) {
		this.fromCurrencyId = fromCurrencyId;
	}


	public Integer getCommonCurrencyId() {
		return this.commonCurrencyId;
	}


	public void setCommonCurrencyId(Integer commonCurrencyId) {
		this.commonCurrencyId = commonCurrencyId;
	}


	public Integer getToCurrencyId() {
		return this.toCurrencyId;
	}


	public void setToCurrencyId(Integer toCurrencyId) {
		this.toCurrencyId = toCurrencyId;
	}


	public boolean isMultiplyRates() {
		return this.multiplyRates;
	}


	public void setMultiplyRates(boolean multiplyRates) {
		this.multiplyRates = multiplyRates;
	}


	public int getDaysBackIncludingToday() {
		return this.daysBackIncludingToday;
	}


	public void setDaysBackIncludingToday(int daysBackIncludingToday) {
		this.daysBackIncludingToday = daysBackIncludingToday;
	}


	public boolean isOverwriteExistingRate() {
		return this.overwriteExistingRate;
	}


	public void setOverwriteExistingRate(boolean overwriteExistingRate) {
		this.overwriteExistingRate = overwriteExistingRate;
	}
}
