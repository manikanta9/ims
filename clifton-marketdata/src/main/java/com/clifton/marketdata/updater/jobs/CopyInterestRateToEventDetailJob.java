package com.clifton.marketdata.updater.jobs;

import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusWithCounts;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.calculator.accrual.AccrualMethods;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventDetailSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.marketdata.rates.search.InterestRateSearchForm;
import com.clifton.system.schema.column.SystemColumnCustomConfig;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * Batch job to look for interest rate changes in Security Events and generate event details, copying them into the leg of the Investment Security to which the event pertains.
 * Updated to be more generic, and allow both Floating Leg and Interest Leg updates.
 *
 * @author jonathanr
 */
public class CopyInterestRateToEventDetailJob implements Task, ValidationAware {

	private CalendarBusinessDayService calendarBusinessDayService;

	private InvestmentCalculator investmentCalculator;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;
	private SystemColumnValueHandler systemColumnValueHandler;
	private SystemColumnValueService systemColumnValueService;
	private SystemColumnService systemColumnService;
	private MarketDataRatesService marketDataRatesService;

	private Short investmentHierarchyId;
	private Short accrualEventTypeId;
	private AccrualMethods[] accrualMethods;
	private String referenceRateCustomColumnName;
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short investmentGroupId;
	private int daysBack = 1;
	private boolean activeSecuritiesOnly;
	private boolean updateEventEffectiveRate;

	private static final String REFERENCE_RATE_CUSTOM_COLUMN_TABLE = "InvestmentInterestRateIndex";

	///////////////////////////////////////////////////////////////////////////
	///////////////                Business Methods               /////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		if (!StringUtils.isEmpty(getReferenceRateCustomColumnName())) {
			SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
			searchForm.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
			searchForm.setValueTableName(REFERENCE_RATE_CUSTOM_COLUMN_TABLE);
			searchForm.setName(getReferenceRateCustomColumnName());
			searchForm.setCustomOnly(Boolean.TRUE);
			ValidationUtils.assertNotEmpty(getSystemColumnService().getSystemColumnList(searchForm), "There are no Security custom columns for Interest Rate Index with name [" + getReferenceRateCustomColumnName() + "]");
		}
		ValidationUtils.assertFalse(ArrayUtils.isEmpty(getAccrualMethods()), "At least one Accrual Method must be defined for copying period interest rates.");
		ValidationUtils.assertTrue(ArrayUtils.getStream(getAccrualMethods()).anyMatch(method -> !method.isConstantRate()), "Please specify an Accrual Method that is not a constant rate.");
	}


	@Override
	public Status run(Map<String, Object> context) {
		InvestmentSecurityEventType accrualEventType = getInvestmentSecurityEventService().getInvestmentSecurityEventType(getAccrualEventTypeId());

		Status status = Status.ofEmptyMessage();

		Date currentDate = DateUtils.clearTime(new Date());
		Date daysBackDate = DateUtils.addDays(currentDate, Math.negateExact(this.daysBack));

		StringBuilder aggregateMessage = new StringBuilder();
		for (AccrualMethods accrualMethod : getAccrualMethods()) {
			if (accrualMethod.isConstantRate()) {
				aggregateMessage.append('\n').append(accrualMethod).append(": is a constant rate accrual method processing was skipped.");
				continue;
			}

			Status accrualMethodStatus = processAccrualMethod(accrualEventType, accrualMethod, currentDate, daysBackDate);
			status.mergeStatus(accrualMethodStatus);
			aggregateMessage.append('\n').append(accrualMethodStatus.getMessage());
		}

		status.setMessage(aggregateMessage.substring(1));
		return status;
	}


	private Status processAccrualMethod(InvestmentSecurityEventType accrualEventType, AccrualMethods accrualMethod, Date currentDate, Date daysBackDate) {
		StatusWithCounts status = new StatusWithCounts();
		List<InvestmentSecurity> investmentSecurityList = getInvestmentSecuritiesList(daysBackDate, accrualMethod);
		if (CollectionUtils.isEmpty(investmentSecurityList)) {
			status.setMessage(accrualMethod + ": No securities were found matching the configured criteria.");
			return status;
		}

		List<InvestmentSecurityEvent> accrualEventList = getAllPaymentEventsForSecurities(investmentSecurityList, accrualEventType);
		if (CollectionUtils.isEmpty(accrualEventList)) {
			status.addError(accrualMethod + ": No '" + accrualEventType.getName() + "' active during configured query period ["
					+ DateUtils.fromDateShort(daysBackDate) + "-" + DateUtils.fromDateShort(currentDate) + "].");
			return status;
		}

		Map<InvestmentSecurity, List<InvestmentSecurityEvent>> securityToEventMap = BeanUtils.getBeansMap(accrualEventList, InvestmentSecurityEvent::getSecurity);
		for (Map.Entry<InvestmentSecurity, List<InvestmentSecurityEvent>> securityEventListEntry : CollectionUtils.getIterable(securityToEventMap.entrySet())) {
			status.incrementProcessCount();
			try {
				processSecurity(securityEventListEntry.getKey(), securityEventListEntry.getValue(), accrualEventType, status, daysBackDate, currentDate);
			}
			catch (Throwable e) {
				String errorMessage = ExceptionUtils.getOriginalMessage(e);
				status.addError(errorMessage);
			}
		}

		String message = new StringBuilder(accrualMethod.name())
				.append(": Processed ").append(status.getProcessCount()).append(" Securities;")
				.append(" Inserted ").append(status.getInsertCount()).append(" and Deleted ").append(status.getDeleteCount())
				.append(" Event Details; Errors: ").append(status.getErrorCount()).toString();
		status.setMessage(message);
		return status;
	}


	protected void processSecurity(InvestmentSecurity investmentSecurity, List<InvestmentSecurityEvent> eventList, InvestmentSecurityEventType accrualEventType, StatusWithCounts status, Date daysBackDate, Date currentDate) {
		Set<InvestmentSecurityEvent> updatedEvents = new HashSet<>();
		List<MarketDataInterestRate> marketDataInterestRateList = getMarketDataRatesList(investmentSecurity, DateUtils.addDays(daysBackDate, -1));
		BigDecimal spread = (BigDecimal) getSecurityFieldValue(investmentSecurity, InvestmentSecurity.CUSTOM_FIELD_SPREAD);

		Map<InvestmentSecurityEvent, List<InvestmentSecurityEventDetail>> newEventDetailListMap = new LinkedHashMap<>();
		Set<Date> missingActiveEventForRateChangeDates = new HashSet<>();
		String securityRateIndex = null;
		for (MarketDataInterestRate interestRateChange : CollectionUtils.getIterable(getChangeList(marketDataInterestRateList))) {
			List<InvestmentSecurityEvent> applicableEventList = findEventListBySecurityAndDate(eventList, investmentSecurity, interestRateChange.getInterestRateDate());
			if (CollectionUtils.isEmpty(applicableEventList)) {
				missingActiveEventForRateChangeDates.add(interestRateChange.getInterestRateDate());
				if (securityRateIndex == null) {
					securityRateIndex = interestRateChange.getInterestRateIndex().getName();
				}
				continue;
			}
			for (InvestmentSecurityEvent event : applicableEventList) {
				InvestmentSecurityEventDetail newSecurityEventDetail = new InvestmentSecurityEventDetail();
				newSecurityEventDetail.setEvent(event);
				newSecurityEventDetail.setEffectiveDate(interestRateChange.getInterestRateDate());
				newSecurityEventDetail.setReferenceRate(interestRateChange.getInterestRate());
				newSecurityEventDetail.setSpread(spread);
				newEventDetailListMap.computeIfAbsent(event, k -> new ArrayList<>()).add(newSecurityEventDetail);

				if (isUpdateEventEffectiveRate() && !updatedEvents.contains(event) && event.getAdditionalDate() != null && DateUtils.isDateBeforeOrEqual(event.getAdditionalDate(), currentDate, false)) {
					updateEventEffectiveRate(event, spread, status);
					updatedEvents.add(event);
				}
			}
		}
		int eventDetailSaveFailedCount = 0;
		if (!CollectionUtils.isEmpty(newEventDetailListMap)) {
			for (Map.Entry<InvestmentSecurityEvent, List<InvestmentSecurityEventDetail>> eventDetailListEntry : newEventDetailListMap.entrySet()) {
				try {
					processEventDetailUpdates(eventDetailListEntry.getKey(), eventDetailListEntry.getValue(), daysBackDate, status);
				}
				catch (Exception e) {
					eventDetailSaveFailedCount++;
					String errorMessage = ExceptionUtils.getOriginalMessage(e);
					status.addError(investmentSecurity.getSymbol() + ": " + errorMessage);
				}
			}
		}
		if (!missingActiveEventForRateChangeDates.isEmpty()) {
			status.addWarning(investmentSecurity.getSymbol() + ": No '" + accrualEventType.getName() + "' active for " + securityRateIndex + " interest rate change on ["
					+ missingActiveEventForRateChangeDates.stream().map(DateUtils::fromDateShort).collect(Collectors.joining(", ")) + "]");
		}
		if (eventDetailSaveFailedCount > 0) {
			status.addError(investmentSecurity.getSymbol() + ": Processing event details failed for [" + eventDetailSaveFailedCount + "] of [" + newEventDetailListMap.size() + "] events");
		}
	}


	@Transactional
	protected void processEventDetailUpdates(InvestmentSecurityEvent event, List<InvestmentSecurityEventDetail> newEventDetailList, Date daysBackDate, StatusWithCounts status) {
		deleteEventDetailsForEventsAfterDate(event, daysBackDate, status);

		if (!CollectionUtils.isEmpty(newEventDetailList)) {
			getInvestmentSecurityEventService().saveInvestmentSecurityEventDetailList(newEventDetailList);
			status.incrementInsertCount(newEventDetailList.size());
		}
	}


	private void deleteEventDetailsForEventsAfterDate(InvestmentSecurityEvent event, Date daysBack, StatusWithCounts status) {
		InvestmentSecurityEventDetailSearchForm searchForm = new InvestmentSecurityEventDetailSearchForm();
		searchForm.setEffectiveDateAfter(daysBack);
		searchForm.setEventId(event.getId());
		List<InvestmentSecurityEventDetail> eventDetailsToDeleteList = getInvestmentSecurityEventService().getInvestmentSecurityEventDetailList(searchForm);
		if (!CollectionUtils.isEmpty(eventDetailsToDeleteList)) {
			// without executeWithPostUpdateFlushEnabled duplicate key error occurs
			DaoUtils.executeWithPostUpdateFlushEnabled(() -> getInvestmentSecurityEventService().deleteInvestmentSecurityEventDetailList(eventDetailsToDeleteList));
			status.incrementDeleteCount(eventDetailsToDeleteList.size());
		}
	}


	/**
	 * Looks up the MarketDataInterestRate for the Security associated with the InvestmentSecurityEvent, and saves it to
	 * the BeforeEventValue property (also labeled "Effective Rate" in the UI).  Null values are skipped.  This is generally
	 * used with events that pertain to interest rate payments on the Interest Leg of a SWAP (e.g. TRS).
	 */
	private void updateEventEffectiveRate(InvestmentSecurityEvent event, BigDecimal spread, StatusWithCounts status) {
		Date fixingDate = event.getAdditionalDate();
		MarketDataInterestRate referenceRate = getMarketDataRate(event.getSecurity(), fixingDate);
		if (referenceRate != null && referenceRate.getInterestRate() != null) {
			BigDecimal effectiveRate = MathUtils.add(referenceRate.getInterestRate(), spread == null ? BigDecimal.ZERO : MathUtils.divide(spread, MathUtils.BIG_DECIMAL_ONE_HUNDRED));
			event.setBeforeEventValue(effectiveRate);
			if (event.getType().isBeforeSameAsAfter()) {
				event.setAfterEventValue(event.getBeforeEventValue());
			}
			getInvestmentSecurityEventService().saveInvestmentSecurityEvent(event);
		}
		else {
			status.addError(event.getSecurity().getSymbol() + ": Reference rate not found for fixing date: " + fixingDate);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////             Service Lookup Methods            /////////////
	///////////////////////////////////////////////////////////////////////////


	private List<MarketDataInterestRate> getMarketDataRatesList(InvestmentSecurity security, Date daysBackDate) {
		InterestRateSearchForm searchForm;
		String referenceRateColumnName = getSecurityReferenceRateColumnName();
		Integer referenceRateTableId = getSecurityReferenceRateTableId(security, referenceRateColumnName);

		if (referenceRateTableId != null) {
			searchForm = new InterestRateSearchForm();
			searchForm.setInterestRateIndexId(referenceRateTableId);
			List<SearchRestriction> restrictionList = new ArrayList<>();
			restrictionList.add(new SearchRestriction("interestRateDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, daysBackDate));
			searchForm.setRestrictionList(restrictionList);
			searchForm.setOrderBy("interestRateDate:asc");
		}
		else {
			throw new RuntimeException(security.getSymbol() + "Error retrieving interest rates; no interest rate index was found for Reference Rate column [" + referenceRateColumnName + "]");
		}
		List<MarketDataInterestRate> indexRateList = getMarketDataRatesService().getMarketDataInterestRateList(searchForm);
		// Need at lease 2 data points to compare so get the day before days back.
		if (CollectionUtils.getSize(indexRateList) < 2) {
			throw new RuntimeException(security.getSymbol() + "Error retrieving interest rates; at least two interest rates are required to detect changes but found "
					+ indexRateList.size() + ". Could try increasing the 'Days Back' parameter.");
		}
		return indexRateList;
	}


	private MarketDataInterestRate getMarketDataRate(InvestmentSecurity security, Date date) {
		InterestRateSearchForm searchForm;
		String referenceRateColumnName = getSecurityReferenceRateColumnName();
		Integer referenceRateTableId = getSecurityReferenceRateTableId(security, referenceRateColumnName);

		if (referenceRateTableId != null) {
			searchForm = new InterestRateSearchForm();
			searchForm.setInterestRateDate(date);
			searchForm.setInterestRateIndexId(referenceRateTableId);
		}
		else {
			throw new RuntimeException(security.getSymbol() + "Error retrieving interest rates; no interest rate index was found for Reference Rate column [" + referenceRateColumnName + "]");
		}

		List<MarketDataInterestRate> indexRateList = getMarketDataRatesService().getMarketDataInterestRateList(searchForm);
		return CollectionUtils.getFirstElement(indexRateList);
	}


	private Integer getSecurityReferenceRateTableId(InvestmentSecurity security, String referenceRateColumnName) {
		SystemColumnCustomConfig customConfig = getSystemColumnCustomConfigForSecurity(security);
		for (SystemColumnValue systemColumnValue : CollectionUtils.getIterable(customConfig.getColumnValueList())) {
			if (CompareUtils.isEqual(referenceRateColumnName, systemColumnValue.getColumn().getName())
					&& (systemColumnValue.getColumn().getValueTable() != null && REFERENCE_RATE_CUSTOM_COLUMN_TABLE.equalsIgnoreCase(systemColumnValue.getColumn().getValueTable().getName()))) {
				return systemColumnValue.getValue() != null ? Integer.valueOf(systemColumnValue.getValue()) : null;
			}
		}
		return null;
	}


	private String getSecurityReferenceRateColumnName() {
		return StringUtils.isEmpty(getReferenceRateCustomColumnName()) ? "Reference Rate" : getReferenceRateCustomColumnName();
	}


	private Object getSecurityFieldValue(InvestmentSecurity security, String fieldName) {
		return getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, fieldName, false);
	}


	private List<InvestmentSecurity> getInvestmentSecuritiesList(Date activeOnDate, AccrualMethods accrualMethod) {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setHierarchyId(getInvestmentHierarchyId());
		searchForm.setActiveOnDate(activeOnDate);
		searchForm.setInvestmentTypeSubTypeId(getInvestmentTypeSubTypeId());
		searchForm.setInvestmentTypeSubType2Id(getInvestmentTypeSubType2Id());
		searchForm.setAccrualEventTypeIdOneAndTwo(getAccrualEventTypeId());
		searchForm.setAccrualMethodOneAndTwo(accrualMethod);
		if (isActiveSecuritiesOnly()) {
			searchForm.setActive(isActiveSecuritiesOnly());
		}
		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	private List<InvestmentSecurityEvent> getAllPaymentEventsForSecurities(List<InvestmentSecurity> investmentSecurityList, InvestmentSecurityEventType accrualEventType) {
		InvestmentSecurityEventSearchForm investmentSecurityEventSearchForm = new InvestmentSecurityEventSearchForm();
		investmentSecurityEventSearchForm.setTypeName(accrualEventType.getName());
		investmentSecurityEventSearchForm.setSecurityIds(CollectionUtils.getStream(investmentSecurityList).map(InvestmentSecurity::getId).toArray(Integer[]::new));
		return getInvestmentSecurityEventService().getInvestmentSecurityEventList(investmentSecurityEventSearchForm);
	}


	private SystemColumnCustomConfig getSystemColumnCustomConfigForSecurity(InvestmentSecurity investmentSecurity) {
		SystemColumnCustomConfig systemColumnCustomConfig = new SystemColumnCustomConfig();
		systemColumnCustomConfig.setColumnGroupName("Security Custom Fields");
		systemColumnCustomConfig.setEntityId(investmentSecurity.getId());
		systemColumnCustomConfig.setLinkedValue(String.valueOf(investmentSecurity.getInstrument().getHierarchy()));
		return getSystemColumnValueService().getSystemColumnCustomConfig(systemColumnCustomConfig);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////                Helper Methods               /////////////
	///////////////////////////////////////////////////////////////////////////


	private List<MarketDataInterestRate> getChangeList(List<MarketDataInterestRate> marketDataInterestRateList) {
		List<MarketDataInterestRate> changeList = new ArrayList<>();
		BigDecimal currentRate = null;
		for (MarketDataInterestRate interestRate : CollectionUtils.getIterable(marketDataInterestRateList)) {
			if (currentRate == null) {
				currentRate = interestRate.getInterestRate();
			}
			if (!MathUtils.isEqual(interestRate.getInterestRate(), currentRate)) {
				currentRate = interestRate.getInterestRate();
				changeList.add(interestRate);
			}
		}
		return changeList;
	}


	private List<InvestmentSecurityEvent> findEventListBySecurityAndDate(List<InvestmentSecurityEvent> investmentSecurityEventList, InvestmentSecurity investmentSecurity, Date activeOnDate) {
		return CollectionUtils.getStream(investmentSecurityEventList)
				.filter(investmentSecurityEvent -> {
					if (!CompareUtils.isEqual(investmentSecurityEvent.getSecurity(), investmentSecurity)) {
						return false;
					}
					Date accrualStartDate = investmentSecurityEvent.getAccrualStartDate();
					if (investmentSecurityEvent.getAdditionalDate() != null && DateUtils.isDateBefore(investmentSecurityEvent.getAdditionalDate(), accrualStartDate, false)) {
						// Use Fixing Date (additional date) as the accrual start if it exists and is before the accrual start date
						accrualStartDate = investmentSecurityEvent.getAdditionalDate();
					}
					return DateUtils.isDateBetween(activeOnDate, accrualStartDate, investmentSecurityEvent.getAccrualEndDate(), false);
				})
				.collect(Collectors.toList());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////               Getters and Setters             /////////////
	///////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getAccrualEventTypeId() {
		return this.accrualEventTypeId;
	}


	public void setAccrualEventTypeId(Short accrualEventTypeId) {
		this.accrualEventTypeId = accrualEventTypeId;
	}


	public AccrualMethods[] getAccrualMethods() {
		return this.accrualMethods;
	}


	public void setAccrualMethods(AccrualMethods[] accrualMethods) {
		this.accrualMethods = accrualMethods;
	}


	public String getReferenceRateCustomColumnName() {
		return this.referenceRateCustomColumnName;
	}


	public void setReferenceRateCustomColumnName(String referenceRateCustomColumnName) {
		this.referenceRateCustomColumnName = referenceRateCustomColumnName;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public int getDaysBack() {
		return this.daysBack;
	}


	public void setDaysBack(int daysBack) {
		this.daysBack = daysBack;
	}


	public boolean isActiveSecuritiesOnly() {
		return this.activeSecuritiesOnly;
	}


	public void setActiveSecuritiesOnly(boolean activeSecuritiesOnly) {
		this.activeSecuritiesOnly = activeSecuritiesOnly;
	}


	public boolean isUpdateEventEffectiveRate() {
		return this.updateEventEffectiveRate;
	}


	public void setUpdateEventEffectiveRate(boolean updateEventEffectiveRate) {
		this.updateEventEffectiveRate = updateEventEffectiveRate;
	}
}
