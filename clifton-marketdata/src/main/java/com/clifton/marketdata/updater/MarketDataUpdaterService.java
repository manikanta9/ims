package com.clifton.marketdata.updater;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.Date;


/**
 * The <code>MarketDataUpdaterService</code> interface defines service methods that retrieve market data
 * information from external data providers and use it to update our tables.
 */
public interface MarketDataUpdaterService {

	////////////////////////////////////////////////////////////////////////////
	///////               Interest Rate Update Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves latest interest rates from the specified provider and saves them
	 * in our data structures.  Uses the default provider if dataSourceName = null.
	 */
	public void updateMarketDataInterestRateListForDataSource(String dataSourceName);


	/**
	 * Updates interest rates for the data source on the given date range.
	 * Uses the default provider if dataSourceName = null.
	 * No update will be performed to existing interest rate entities within the command's date range.
	 * Sequential dates within the date range having no existing values nor holidays will be aggregated
	 * into a single request, the number of provider requests will be kept to a minimum.
	 */
	@SecureMethod(dtoClass = MarketDataInterestRate.class)
	public Status updateMarketDataInterestRates(MarketDataInterestRateUpdaterCommand command);


	////////////////////////////////////////////////////////////////////////////
	///////                 Data Field Update Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Updates market data field values for based on the specified command.
	 * <p>
	 * NOTE: uses LATEST data if both startDate and endDate are null.
	 * Errors do not error out on provider errors for individual securities (likely mis-configured) but add a corresponding error message to this list
	 *
	 * @return returns the status string with numbers of market data values values updated/created/errors/etc.
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = MarketDataValue.class)
	public Status updateMarketDataFieldValues(MarketDataFieldValueUpdaterCommand command);


	////////////////////////////////////////////////////////////////////////////
	///////     Security Allocation Market Data Rebuild Methods          /////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Start/End Date are optional and default max range of Security Start to Today
	 */
	@ModelAttribute("result")
	@SecureMethod(dtoClass = MarketDataValue.class)
	public String rebuildMarketDataAllocatedValueForSecurity(int securityId, short dataSourceId, Date startDate, Date endDate, boolean asynchronous);
}
