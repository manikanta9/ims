package com.clifton.marketdata.updater.security;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;

import java.util.List;


public interface MarketDataInvestmentSecurityService {


	@SecureMethod(dtoClass = MarketDataValue.class)
	public void saveMarketDataInvestmentSecurityFieldUpdateResult(MarketDataInvestmentSecurityFieldUpdateResult fieldData);


	@SecureMethod(dtoClass = MarketDataFieldColumnMapping.class)
	public List<MarketDataInvestmentSecurityFieldUpdateResult> updateMarketDataInvestmentSecurityFieldData(MarketDataInvestmentSecurityRetrieverCommand retrieverCommand);


	@SecureMethod(dtoClass = MarketDataFieldColumnMapping.class)
	public List<MarketDataInvestmentSecurityFieldUpdateResult> getMarketDataInvestmentSecurityFieldDataPreview(MarketDataInvestmentSecurityRetrieverCommand retrieverCommand);


	/**
	 * Create or update an InvestmentSecurity from market data.
	 */
	@SecureMethod(dtoClass = MarketDataField.class)
	public MarketDataInvestmentSecurityResult getMarketDataInvestmentSecurity(MarketDataInvestmentSecurityRetrieverCommand command);


	@SecureMethod(dtoClass = MarketDataField.class)
	public List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingList(MarketDataInvestmentSecurityRetrieverCommand command);
}
