package com.clifton.marketdata.updater.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventDetail;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventDetailSearchForm;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.system.schema.SystemDataType;
import com.clifton.core.util.MathUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The MarketDataValueCopyToEventDetailJob class copies security market data values for the specified field into
 * corresponding security event details.
 * <p>
 * Exclusive of Accrual Start (Declare Date) and inclusive of Accrual End (Record Date).
 * <p>
 * For example, copy "Dividend Points" values for TRS Baskets into corresponding Dividend Leg Payment event details.
 *
 * @author vgomelsky
 */
public class MarketDataValueCopyToEventDetailJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityEventService investmentSecurityEventService;

	private MarketDataFieldService marketDataFieldService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StatusHolderObject<Status> statusHolder;


	/**
	 * Number of days back (regular days, not calendar days) to insert missing (optionally overwrite existing) prices for
	 */
	private Short daysBackIncludingToday;

	/**
	 * Optionally overwrite market data (if false just inserts where missing)
	 */
	private Boolean overwriteExisting;

	/**
	 * If true, will update event before/after value(s) to the latest detail Effective Rate.
	 */
	private Boolean updateEventValueToLatestDetail;

	/**
	 * Which data field to copy from
	 */
	private Short fromMarketDataFieldId;
	private Short fromDataSourceId;

	/**
	 * Copy to events of the following type
	 */
	private Short toEventTypeId;


	/**
	 * Limit the scope to the following securities:
	 */
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short investmentHierarchyId;
	private Short investmentGroupId;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		MarketDataField fromDataField = getMarketDataFieldService().getMarketDataField(getFromMarketDataFieldId());
		ValidationUtils.assertNotNull(fromDataField, "From data field is required");
		ValidationUtils.assertEquals(fromDataField.getDataType().getName(), SystemDataType.DECIMAL, "From Market Data Field must be a DECIMAL.");
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date endDate = new Date();
		Date startDate = DateUtils.addDays(endDate, getDaysBackIncludingToday() * -1);

		List<InvestmentSecurity> securityList = getSecurityList();
		int total = CollectionUtils.getSize(securityList);
		int processedCount = 0, failedCount = 0, updateCount = 0;

		Status status = this.statusHolder.getStatus();
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			try {
				updateCount += copyValuesForSecurity(security, startDate, endDate);
			}
			catch (Exception e) {
				failedCount++;
				String msg = "" + security.getLabel() + ": " + ExceptionUtils.getDetailedMessage(e);
				status.addError(msg);
				LogUtils.errorOrInfo(getClass(), msg, e);
			}
			finally {
				processedCount++;
				status.setMessage("Processed [" + processedCount + "] out of [" + total + "] securities. Updated [" + updateCount + "] event details." + ((failedCount > 0) ? " Failed [" + failedCount + "]" : ""));
			}
		}

		status.setMessage("Processing Completed. [" + total + "] securities. [" + updateCount + "] updated event details." + ((failedCount > 0) ? " Failed [" + failedCount + "]" : ""));
		status.setActionPerformed(updateCount != 0);
		return status;
	}


	private int copyValuesForSecurity(InvestmentSecurity security, Date startDate, Date endDate) {
		List<MarketDataValue> valueList = getMarketDataValueList(security, startDate, endDate);
		Map<Date, MarketDataValue> valueMap = BeanUtils.getBeanMap(valueList, v -> DateUtils.clearTime(v.getMeasureDate()));

		List<InvestmentSecurityEventDetail> detailList = getExistingEventDetailList(security, startDate, endDate);
		Map<Date, InvestmentSecurityEventDetail> detailMap = BeanUtils.getBeanMap(detailList, d -> DateUtils.clearTime(d.getEffectiveDate()));

		int updateCount = 0;
		boolean overwrite = BooleanUtils.isTrue(getOverwriteExisting());

		// exclude/update existing details first
		for (MarketDataValue value : CollectionUtils.getIterable(valueList)) {
			Date key = DateUtils.clearTime(value.getMeasureDate());
			InvestmentSecurityEventDetail detail = detailMap.remove(key);
			if (detail != null) {
				valueMap.remove(key);
				if (overwrite && MathUtils.isNotEqual(detail.getReferenceRate(), value.getMeasureValue())) {
					detail.setReferenceRate(value.getMeasureValue());
					getInvestmentSecurityEventService().saveInvestmentSecurityEventDetail(detail);
					updateCount++;
				}
			}
		}

		// delete no longer necessary details
		if (overwrite) {
			for (InvestmentSecurityEventDetail detail : CollectionUtils.getIterable(detailMap.values())) {
				getInvestmentSecurityEventService().deleteInvestmentSecurityEventDetail(detail.getId());
				updateCount++;
			}
		}

		// insert new details
		for (MarketDataValue value : CollectionUtils.getIterable(valueMap.values())) {
			// skip copy on start date of security because first accrual period is exclusive of Accrual Start Date (may want to make this configurable at job level)
			boolean skip = MathUtils.isNullOrZero(value.getMeasureValue()) && DateUtils.isEqualWithoutTime(value.getMeasureDate(), security.getStartDate());
			if (!skip) {
				InvestmentSecurityEventDetail detail = new InvestmentSecurityEventDetail();
				detail.setEvent(getInvestmentSecurityEvent(security, value.getMeasureDate()));
				detail.setEffectiveDate(value.getMeasureDate());
				detail.setReferenceRate(value.getMeasureValue());
				// skip copy of 0 values on Accrual Start Date (may want to make this configurable at job level)
				skip = MathUtils.isNullOrZero(detail.getReferenceRate()) && DateUtils.isEqualWithoutTime(detail.getEffectiveDate(), detail.getEvent().getAccrualStartDate());
				if (!skip) {
					getInvestmentSecurityEventService().saveInvestmentSecurityEventDetail(detail);
					updateCount++;
				}
			}
		}

		if (BooleanUtils.isTrue(getUpdateEventValueToLatestDetail())) {
			// there can be multiple events for the specified date range
			InvestmentSecurityEvent lastEvent = null;
			detailList = getExistingEventDetailList(security, startDate, endDate);
			for (InvestmentSecurityEventDetail detail : CollectionUtils.getIterable(detailList)) {
				if (lastEvent == null || !lastEvent.equals(detail.getEvent())) {
					lastEvent = detail.getEvent();
					if (MathUtils.isNotEqual(lastEvent.getBeforeEventValue(), detail.getReferenceRate())) {
						lastEvent.setBeforeEventValue(detail.getReferenceRate());
						if (lastEvent.getType().isBeforeSameAsAfter()) {
							lastEvent.setAfterEventValue(lastEvent.getBeforeEventValue());
						}
						getInvestmentSecurityEventService().saveInvestmentSecurityEvent(lastEvent);
						updateCount++;
					}
				}
			}
		}

		return updateCount;
	}


	private List<InvestmentSecurity> getSecurityList() {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInvestmentTypeId(getInvestmentTypeId());
		searchForm.setInvestmentTypeSubTypeId(getInvestmentTypeSubTypeId());
		searchForm.setInvestmentTypeSubType2Id(getInvestmentTypeSubType2Id());
		searchForm.setHierarchyId(getInvestmentHierarchyId());
		searchForm.setInvestmentGroupId(getInvestmentGroupId());
		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	private List<MarketDataValue> getMarketDataValueList(InvestmentSecurity security, Date startDate, Date endDate) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm(true);
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setDataFieldId(getFromMarketDataFieldId());
		searchForm.setDataSourceId(getFromDataSourceId());
		// make dates inclusive
		searchForm.setMinMeasureDate(DateUtils.addDays(startDate, -1));
		searchForm.setMaxMeasureDate(DateUtils.addDays(endDate, 1));
		return getMarketDataFieldService().getMarketDataValueList(searchForm);
	}


	private List<InvestmentSecurityEventDetail> getExistingEventDetailList(InvestmentSecurity security, Date startDate, Date endDate) {
		InvestmentSecurityEventDetailSearchForm searchForm = new InvestmentSecurityEventDetailSearchForm();
		searchForm.setInvestmentSecurityId(security.getId());
		// dates are inclusive
		searchForm.setEffectiveDateAfter(startDate);
		searchForm.setEffectiveDateBefore(endDate);
		searchForm.setOrderBy("effectiveDate:desc");
		return getInvestmentSecurityEventService().getInvestmentSecurityEventDetailList(searchForm);
	}


	private InvestmentSecurityEvent getInvestmentSecurityEvent(InvestmentSecurity security, Date effectiveDate) {
		InvestmentSecurityEventSearchForm searchForm = new InvestmentSecurityEventSearchForm();
		searchForm.setSecurityId(security.getId());
		searchForm.setTypeId(getToEventTypeId());
		searchForm.addSearchRestriction(new SearchRestriction("declareDate", ComparisonConditions.LESS_THAN, effectiveDate));
		searchForm.addSearchRestriction(new SearchRestriction("recordDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, effectiveDate));
		List<InvestmentSecurityEvent> eventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(searchForm);
		if (eventList == null || eventList.isEmpty()) {
			throw new IllegalStateException("Cannot find " + getInvestmentSecurityEventService().getInvestmentSecurityEventType(getToEventTypeId()) + " for " + security + " on " + effectiveDate);
		}
		if (eventList.size() > 1) {
			throw new IllegalStateException("Cannot have multiple events of " + getInvestmentSecurityEventService().getInvestmentSecurityEventType(getToEventTypeId()) + " for " + security + " on " + effectiveDate + ": " + eventList);
		}

		return eventList.get(0);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	public void setStatusHolder(StatusHolderObject<Status> statusHolder) {
		this.statusHolder = statusHolder;
	}


	public Short getDaysBackIncludingToday() {
		return this.daysBackIncludingToday;
	}


	public void setDaysBackIncludingToday(Short daysBackIncludingToday) {
		this.daysBackIncludingToday = daysBackIncludingToday;
	}


	public Boolean getOverwriteExisting() {
		return this.overwriteExisting;
	}


	public void setOverwriteExisting(Boolean overwriteExisting) {
		this.overwriteExisting = overwriteExisting;
	}


	public Boolean getUpdateEventValueToLatestDetail() {
		return this.updateEventValueToLatestDetail;
	}


	public void setUpdateEventValueToLatestDetail(Boolean updateEventValueToLatestDetail) {
		this.updateEventValueToLatestDetail = updateEventValueToLatestDetail;
	}


	public Short getFromMarketDataFieldId() {
		return this.fromMarketDataFieldId;
	}


	public void setFromMarketDataFieldId(Short fromMarketDataFieldId) {
		this.fromMarketDataFieldId = fromMarketDataFieldId;
	}


	public Short getFromDataSourceId() {
		return this.fromDataSourceId;
	}


	public void setFromDataSourceId(Short fromDataSourceId) {
		this.fromDataSourceId = fromDataSourceId;
	}


	public Short getToEventTypeId() {
		return this.toEventTypeId;
	}


	public void setToEventTypeId(Short toEventTypeId) {
		this.toEventTypeId = toEventTypeId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}
}
