package com.clifton.marketdata.updater.request;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>MarketDataFieldRequestCollection</code> builds {@link MarketDataFieldRequest}
 * objects by consolidating requests based on equivalent field lists by expanding
 * the date period.
 * <p>
 * Assumptions:
 * Each date specific request may have a different set of fields.
 * Date periods may include weekends and holidays; these days do not count towards
 * query limits.
 */
public class MarketDataFieldRequestCollection {

	private final InvestmentSecurity security;
	private final Set<MarketDataField> fieldSet;
	private final MarketDataFieldValueUpdaterCommand command;

	private LocalDate startDate;
	private LocalDate endDate;

	private final Set<LocalDate> nonBusinessDates = new HashSet<>();
	private final Map<LocalDate, Set<MarketDataField>> missingDateFieldValues = new HashMap<>();


	public MarketDataFieldRequestCollection(InvestmentSecurity security, List<MarketDataField> fieldList, MarketDataFieldValueUpdaterCommand command) {
		ValidationUtils.assertNotNull(security, "InvestmentSecurity parameter is required.");
		ValidationUtils.assertNotNull(fieldList, "The list of MarketDataFields is required.");
		ValidationUtils.assertNotNull(command, "The MarketDataFieldValueUpdaterCommand is required.");

		this.security = security;
		this.fieldSet = new HashSet<>(fieldList);
		this.command = command;

		initializeDates();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addMissingFieldValue(MarketDataField field, LocalDate date) {
		Set<MarketDataField> fields = this.missingDateFieldValues.computeIfAbsent(date, k -> new HashSet<>());
		fields.add(field);
	}


	public void addNonBusinessDate(LocalDate date) {
		this.nonBusinessDates.add(date);
	}


	public List<MarketDataFieldRequest> populateRequests() {
		final List<MarketDataFieldRequest> requests = new ArrayList<>();

		// return immediately if skipping is not turned on, one request for everything.
		if (!isSkipIfAlreadyExists()) {
			requests.add(new MarketDataFieldRequest(this.security, this.fieldSet, this.startDate, this.endDate));
			return requests;
		}

		if (!this.missingDateFieldValues.isEmpty()) {
			// get all missing field value dates.
			ArrayList<LocalDate> periodDates = new ArrayList<>(this.missingDateFieldValues.keySet());
			Collections.sort(periodDates);
			// first day with missing field values
			LocalDate firstDate = CollectionUtils.getFirstElementStrict(periodDates);
			// add weekends and holidays.
			periodDates.addAll(this.nonBusinessDates);
			Collections.sort(periodDates);
			// add first request
			requests.add(new MarketDataFieldRequest(getSecurity(), this.missingDateFieldValues.get(firstDate), firstDate, firstDate));
			for (LocalDate currentDate : periodDates) {
				// the first date was already processed, go to the next.
				if (currentDate.isAfter(firstDate)) {
					MarketDataFieldRequest previousRequest = CollectionUtils.getLastElementStrict(requests);
					// a date with missing data values, compare fields to previous request.
					if (this.missingDateFieldValues.containsKey(currentDate)) {
						Set<MarketDataField> fields = this.missingDateFieldValues.get(currentDate);
						if (previousRequest.canExtend(currentDate, fields)) {
							// fields didn't change and date is sequential, extend ending date.
							previousRequest.extendEndingDate(currentDate);
						}
						else {
							// date is not sequential or the request field list has changed, create new request
							requests.add(new MarketDataFieldRequest(getSecurity(), fields, currentDate, currentDate));
						}
					}
					else if (previousRequest.isSequential(currentDate)) {
						// this is a non-business day and is sequential with previous request, extend it.
						// else case is skipped, non-sequential non-business days can be skipped.
						previousRequest.extendEndingDate(currentDate);
					}
				}
			}
		}

		return requests;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSkipIfAlreadyExists() {
		return this.command.isSkipDataRequestIfAlreadyExists();
	}


	public Time getLatestValuesOverrideTimestamp() {
		return this.command.getLatestValuesOverrideTimestamp();
	}


	public boolean calculateSkipIfAlreadyExists(Date date, Time timestamp) {
		return this.command.calculateSkipIfAlreadyExists(date, timestamp);
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.security;
	}


	public Integer getSecurityId() {
		return this.security.getId();
	}


	public LocalDate getStartDate() {
		return this.startDate;
	}


	public LocalDate getEndDate() {
		return this.endDate;
	}


	public String getDataSourceName() {
		return this.command.getDataSourceName();
	}


	public boolean isUsePreviousWeekday() {
		return this.command.isLatestValueIsForPreviousWeekday();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void initializeDates() {
		LocalDate fromDate = DateUtils.asLocalDate(this.command.getStartDate());
		LocalDate toDate = DateUtils.asLocalDate(this.command.getEndDate());
		if (fromDate == null) {
			toDate = fromDate;
		}
		this.startDate = fromDate;
		this.endDate = toDate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public Set<MarketDataField> getFields() {
		return this.fieldSet;
	}
}
