package com.clifton.marketdata.updater.security;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingCommand;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingService;
import com.clifton.system.schema.column.CustomColumnTypes;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * @author mwacker
 */
@Component
public class MarketDataInvestmentSecurityUtilHandlerImpl implements MarketDataInvestmentSecurityUtilHandler {

	private SystemColumnService systemColumnService;

	private DaoLocator daoLocator;

	private MarketDataFieldColumnMappingService marketDataFieldColumnMappingService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> MarketDataFieldColumnMappingCommand<T> getDataFieldColumnMappingCommand(MarketDataInvestmentSecurityRetrieverCommand command, Class<T> clazz) {
		MarketDataFieldColumnMappingCommand<T> instrumentCommand = new MarketDataFieldColumnMappingCommand<>();
		instrumentCommand.setSymbol(command.getSymbol());
		instrumentCommand.setMarketSector(command.getMarketSector());
		instrumentCommand.setBeanClass(clazz);
		instrumentCommand.setSecurityId(command.getSecurity().getId());
		instrumentCommand.setDataSource(command.getDataSource());
		instrumentCommand.setInstrumentHierarchyId(command.getInstrumentHierarchyId());
		instrumentCommand.setInvestmentTypeId(command.getHierarchy().getInvestmentType().getId());
		if (command.getHierarchy().getInvestmentTypeSubType() != null) {
			instrumentCommand.setInvestmentTypeSubTypeId(command.getHierarchy().getInvestmentTypeSubType().getId());
		}
		if (command.getHierarchy().getInvestmentTypeSubType2() != null) {
			instrumentCommand.setInvestmentTypeSubType2Id(command.getHierarchy().getInvestmentTypeSubType2().getId());
		}
		if (command.getInstrumentId() != null) {
			instrumentCommand.setExchangeCode(command.getExchangeCode());
		}
		instrumentCommand.setErrors(command.getErrors());
		return instrumentCommand;
	}


	@Override
	public List<MarketDataFieldColumnMapping> getMappingObjectForUnmappedColumns(MarketDataInvestmentSecurityRetrieverCommand command, List<MarketDataFieldColumnMapping> mappings) {
		List<SystemColumn> mappedColumnList = getMappedColumnList(mappings, command.getSecurity());
		List<SystemColumn> unmappedColumnList = removeMappedColumnFromList(getTableColumnList(command), mappedColumnList);

		List<MarketDataFieldColumnMapping> result = new ArrayList<>();
		int tempId = -10;
		for (SystemColumn column : CollectionUtils.getIterable(unmappedColumnList)) {
			MarketDataFieldColumnMapping newMapping = new MarketDataFieldColumnMapping();
			newMapping.setId(tempId);
			newMapping.setColumn(column);
			result.add(newMapping);
			tempId -= 10;
		}
		return result;
	}


	@Override
	public MarketDataInvestmentSecurityResult getSecurityMarketDataValues(MarketDataInvestmentSecurityRetrieverCommand command, MarketDataFieldColumnMappingHolder columnMappingHolder) {
		InvestmentInstrument instrument = null;
		Map<MarketDataFieldColumnMapping, Object> instrumentValueMap = null;
		if (command.getHierarchy().isOneSecurityPerInstrument()) {
			instrumentValueMap = getInstrumentValueMap(command);
			instrument = applyInvestmentInstrumentMarketDataValue(instrumentValueMap, true, command);
		}

		columnMappingHolder.setSecurityValueMap(getSecurityValueMap(command));
		InvestmentSecurity newSecurity = applyInvestmentSecurityMarketDataValue(columnMappingHolder.getSecurityValueMap(), true, command);

		if (instrument != null && (instrumentValueMap != null)) {
			newSecurity.setInstrument(instrument);
			columnMappingHolder.getSecurityValueMap().putAll(instrumentValueMap);
		}

		MarketDataInvestmentSecurityResult result = new MarketDataInvestmentSecurityResult();
		result.setNewSecurity(newSecurity);
		result.setExistingSecurity(command.getSecurity());
		result.setError(buildErrorString(command.getErrors()));

		return result;
	}


	@Override
	public InvestmentInstrument applyInvestmentInstrumentMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, boolean applyIfNotNull, MarketDataInvestmentSecurityRetrieverCommand command) {
		InvestmentSecurity security = command.isApplyToSecurity() ? command.getSecurity() : null;
		InvestmentInstrument instrument;
		if ((security == null) || (security.getInstrument() == null)) {
			instrument = new InvestmentInstrument();
			instrument.setHierarchy(command.getHierarchy());
		}
		else {
			instrument = security.getInstrument();
		}

		instrument.setColumnGroupName(InvestmentInstrument.INSTRUMENT_CUSTOM_FIELDS_GROUP_NAME);
		return getMarketDataFieldColumnMappingService().applyMarketDataValue(valueMap, instrument, command.getErrors(), applyIfNotNull);
	}


	@Override
	public InvestmentSecurity applyInvestmentSecurityMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, boolean applyIfNotNull, MarketDataInvestmentSecurityRetrieverCommand command) {
		InvestmentSecurity security = command.isApplyToSecurity() ? command.getSecurity() : null;
		if (security == null) {
			security = new InvestmentSecurity();
			security.setSymbol(command.getSymbol());
			security.setInstrument(command.getInstrument());
		}

		security.setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		InvestmentSecurity result = getMarketDataFieldColumnMappingService().applyMarketDataValue(valueMap, security, command.getErrors(), applyIfNotNull);
		result.setSymbol(removeExchangeCodeFromSymbol(result));

		return result;
	}


	@Override
	public String removeExchangeCodeFromSymbol(InvestmentSecurity security) {
		String result = security.getSymbol();
		for (InvestmentExchangeTypes type : InvestmentExchangeTypes.values()) {
			String exchangeCode = type.getSecurityExchangeCode(security);
			result = removeExchangeCodeFromSymbol(result, exchangeCode);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String removeExchangeCodeFromSymbol(String symbol, String exchangeCode) {
		if (!StringUtils.isEmpty(symbol) && !StringUtils.isEmpty(exchangeCode)) {
			if (symbol.endsWith(" " + exchangeCode)) {
				return symbol.substring(0, symbol.length() - (exchangeCode.length() + 1));
			}
		}
		return symbol;
	}


	private Map<MarketDataFieldColumnMapping, Object> getSecurityValueMap(MarketDataInvestmentSecurityRetrieverCommand command) {

		MarketDataFieldColumnMappingCommand<InvestmentSecurity> mappingCommand = getDataFieldColumnMappingCommand(command, InvestmentSecurity.class);

		List<MarketDataFieldColumnMapping> columnMappings = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingListForBean(mappingCommand);
		mappingCommand.setColumnMappingList(filterColumnMappingList(command, command.getSecurity(), columnMappings));

		return getMarketDataFieldColumnMappingService().getMarketDataFieldValueMap(mappingCommand);
	}


	private Map<MarketDataFieldColumnMapping, Object> getInstrumentValueMap(MarketDataInvestmentSecurityRetrieverCommand command) {
		MarketDataFieldColumnMappingCommand<InvestmentInstrument> mappingCommand = getDataFieldColumnMappingCommand(command, InvestmentInstrument.class);

		List<MarketDataFieldColumnMapping> columnMappings = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingListForBean(mappingCommand);
		mappingCommand.setColumnMappingList(filterColumnMappingList(command, command.getSecurity(), columnMappings));

		return getMarketDataFieldColumnMappingService().getMarketDataFieldValueMap(mappingCommand);
	}


	private <T extends IdentityObject> List<SystemColumn> getMappedColumnList(List<MarketDataFieldColumnMapping> mappings, T object) {
		List<SystemColumn> result = new ArrayList<>();
		for (MarketDataFieldColumnMapping mapping : CollectionUtils.getIterable(mappings)) {
			result.add(mapping.getTemplate() != null ? getMarketDataFieldColumnMappingService().resolveSystemColumnFromTemplate(mapping, object) : mapping.getColumn());
		}
		return result;
	}


	private List<SystemColumn> removeMappedColumnFromList(List<SystemColumn> tableColumnList, List<SystemColumn> mappedColumnList) {
		for (SystemColumn mappedColumn : CollectionUtils.getIterable(mappedColumnList)) {
			tableColumnList.remove(mappedColumn);
		}
		return tableColumnList;
	}


	private List<SystemColumn> getTableColumnList(MarketDataInvestmentSecurityRetrieverCommand command) {
		DAOConfiguration<InvestmentSecurity> config = getDaoLocator().locate(InvestmentSecurity.class).getConfiguration();

		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setTableName(config.getTableName());
		searchForm.setLinkedValue(command.getHierarchy().getId().toString());
		searchForm.setIncludeNullLinkedValue(true);
		searchForm.setExcludeCustomColumnType(CustomColumnTypes.COLUMN_DATED_VALUE);
		return getSystemColumnService().getSystemColumnList(searchForm);
	}


	private List<MarketDataFieldColumnMapping> filterColumnMappingList(MarketDataInvestmentSecurityRetrieverCommand updateCommand, InvestmentSecurity security, List<MarketDataFieldColumnMapping> mappingList) {
		List<MarketDataFieldColumnMapping> result = new ArrayList<>();
		for (MarketDataFieldColumnMapping mapping : CollectionUtils.getIterable(mappingList)) {
			if ((updateCommand.getTemplateId() != null) || (updateCommand.getColumnId() != null)) {
				if (updateCommand.getColumnId() != null) {
					SystemColumn column;
					if (mapping.getTemplate() != null) {
						column = getMarketDataFieldColumnMappingService().resolveSystemColumnFromTemplate(mapping, security);
					}
					else {
						column = mapping.getColumn();
					}
					if ((column != null) && column.getId().equals(updateCommand.getColumnId())) {
						result.add(mapping);
					}
				}
				if ((updateCommand.getTemplateId() != null) && (mapping.getTemplate() != null) && mapping.getTemplate().getId().equals(updateCommand.getTemplateId())) {
					result.add(mapping);
				}
			}
			else {
				result.add(mapping);
			}
		}
		return result;
	}


	private String buildErrorString(List<String> errors) {
		StringBuilder error = new StringBuilder();
		if (!errors.isEmpty()) {
			error.append("Error count = ");
			error.append(errors.size());
			for (String err : errors) {
				error.append("\n\n");
				error.append(err);
			}
		}
		return error.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public MarketDataFieldColumnMappingService getMarketDataFieldColumnMappingService() {
		return this.marketDataFieldColumnMappingService;
	}


	public void setMarketDataFieldColumnMappingService(MarketDataFieldColumnMappingService marketDataFieldColumnMappingService) {
		this.marketDataFieldColumnMappingService = marketDataFieldColumnMappingService;
	}
}
