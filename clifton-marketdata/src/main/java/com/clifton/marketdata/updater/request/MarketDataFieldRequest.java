package com.clifton.marketdata.updater.request;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * The <code>MarketDataFieldRequest</code> represents a single request against the data provider
 * for a specific security, date range, and field list.
 */
public class MarketDataFieldRequest {

	private final InvestmentSecurity security;
	private final Set<MarketDataField> fieldSet;
	private final LocalDate startDate;
	private LocalDate endDate;


	public MarketDataFieldRequest(InvestmentSecurity security, Set<MarketDataField> fieldSet, LocalDate startDate, LocalDate endDate) {
		AssertUtils.assertNotNull(security, "Security cannot be null.");
		AssertUtils.assertNotNull(startDate, "StartDate cannot be null.");
		AssertUtils.assertNotNull(endDate, "EndDate cannot be null.");
		AssertUtils.assertTrue(startDate.compareTo(endDate) <= 0, "End Date must be after the start date.");

		this.security = security;
		this.fieldSet = (fieldSet == null) ? Collections.emptySet() : fieldSet;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean canExtend(LocalDate newEndingDate, Set<MarketDataField> fields) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		return (isSequential(newEndingDate) && Objects.equals(this.fieldSet, fields));
	}


	public boolean isSequential(LocalDate newEndingDate) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		LocalDate next = getEndDate().plus(1, ChronoUnit.DAYS);
		return next.equals(newEndingDate);
	}


	public void extendEndingDate(LocalDate newEndingDate) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		if (getEndDate().compareTo(newEndingDate) < 0) {
			this.endDate = newEndingDate;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		MarketDataFieldRequest that = (MarketDataFieldRequest) o;
		return Objects.equals(this.security, that.security) &&
				Objects.equals(this.fieldSet, that.fieldSet) &&
				Objects.equals(this.startDate, that.startDate) &&
				Objects.equals(this.endDate, that.endDate);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.security, this.fieldSet, this.startDate, this.endDate);
	}


	@Override
	public String toString() {
		return "MarketDataFieldRequest{" +
				"security=" + this.security +
				", fieldSet=" + this.fieldSet +
				", startDate=" + this.startDate +
				", endDate=" + this.endDate +
				'}';
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public List<MarketDataField> getFieldList() {
		return CollectionUtils.toArrayList(this.fieldSet);
	}


	public LocalDate getStartDate() {
		return this.startDate;
	}


	public LocalDate getEndDate() {
		return this.endDate;
	}
}
