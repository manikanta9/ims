package com.clifton.marketdata.updater.jobs;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.system.schema.SystemDataType;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * MarketDataValueCopyFromUnderlyingJob class is used for Swaps to copy prices from their underlying securities.
 *
 * @author manderson
 */
public class MarketDataValueCopyFromUnderlyingJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private InvestmentInstrumentService investmentInstrumentService;


	private MarketDataFieldService marketDataFieldService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private StatusHolderObject<Status> statusHolder;


	/**
	 * Number of days back (regular days, not calendar days) to insert missing (optionally overwrite existing) prices for
	 */
	private Short daysBackIncludingToday;

	/**
	 * Optionally overwrite market data (if false just inserts where missing)
	 */
	private Boolean overwriteExisting;

	/**
	 * Which data field to copy from
	 */
	private Short fromMarketDataFieldId;

	/**
	 * Which data field to copy to
	 */
	private Short toMarketDataFieldId;

	// Market Data Source ID to filter Market Data Values to copy
	private Short dataSourceId;

	/**
	 * If true will also do Measure Value conversion based on
	 * exchange rate if Trading Currency != Underlying Trading Currency
	 * (for the Business Company tied to the Swap if a Swap)
	 */
	private Boolean alterValueForFXRate;


	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;

	/**
	 * Note, the following Options uses System List for Yes, No, or Blank to include True, False, Null to allow not filtering on these options
	 */
	private String hierarchyIsOTC;
	private String securityStartDateIsPopulated;
	private String securityEndDateIsPopulated;


	//  Underlying Filters
	private Short underlyingInvestmentTypeId;
	private String underlyingIsOneSecurityPerInstrument;
	private String underlyingSecurityEndDateEqualsSecurityEndDate; // NOTE: This is checked in code during iteration through each security

	// InvestmentGroup to filter Securities for updating values
	private Short instrumentGroupId;

	//Issuing BusinessCompany ID to filter Securities for updating values
	private Integer issuingCompanyId;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() throws ValidationException {
		MarketDataField fromDataField = getMarketDataFieldService().getMarketDataField(getFromMarketDataFieldId());
		MarketDataField toDataField = getMarketDataFieldService().getMarketDataField(getToMarketDataFieldId());
		ValidationUtils.assertNotNull(fromDataField, "From data field is required");
		ValidationUtils.assertNotNull(toDataField, "To data field is required");
		ValidationUtils.assertEquals(fromDataField.getDataType().getName(), SystemDataType.DECIMAL, "From Market Data Field must be a DECIMAL.");
		ValidationUtils.assertEquals(toDataField.getDataType().getName(), SystemDataType.DECIMAL, "To Market Data Field must be a DECIMAL.");
	}


	@Override
	public Status run(Map<String, Object> context) {
		Optional<Boolean> underlyingEndDateEquals = getPropertyOptionAsBoolean(getUnderlyingSecurityEndDateEqualsSecurityEndDate());
		List<InvestmentSecurity> securityList = getSecurityList();

		Date endDate = new Date();
		Date startDate = DateUtils.addDays(endDate, getDaysBackIncludingToday() * -1);
		MarketDataField fromDataField = getMarketDataFieldService().getMarketDataField(getFromMarketDataFieldId());
		MarketDataField toDataField = getMarketDataFieldService().getMarketDataField(getToMarketDataFieldId());

		int total = CollectionUtils.getSize(securityList);
		int processedCount = 0, failedCount = 0, marketDataValueCount = 0;

		Status status = this.statusHolder.getStatus();
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			try {
				// Should never get here without underlying because of filtering
				if (security.getUnderlyingSecurity() != null) {
					if (underlyingEndDateEquals.isPresent()) {
						if ((DateUtils.compare(security.getEndDate(), security.getUnderlyingSecurity().getEndDate(), false) == 0) != underlyingEndDateEquals.get()) {
							continue;
						}
					}
					marketDataValueCount += copyValuesForSecurity(security, fromDataField, toDataField, startDate, endDate);
				}
			}
			catch (Exception e) {
				failedCount++;
				String msg = "Error copying values for [" + security.getLabel() + "] from underlying: " + e.getMessage();
				status.addError(msg);
				LogUtils.errorOrInfo(getClass(), msg, e);
			}
			finally {
				processedCount++;
				status.setMessage("Processed [" + processedCount + "] out of [" + total + "] securities. Updated [" + marketDataValueCount + "] market data values." + ((failedCount > 0) ? " Failed [" + failedCount + "]" : ""));
			}
		}

		status.setMessage("Processing Completed. [" + total + "] securities. [" + marketDataValueCount + "] market data values." + ((failedCount > 0) ? " Failed [" + failedCount + "]" : ""));
		status.setActionPerformed(processedCount != 0);
		return status;
	}


	/////////////////////////////////////////////////////////////////


	private int copyValuesForSecurity(InvestmentSecurity security, MarketDataField fromDataField, MarketDataField toDataField, Date startDate, Date endDate) {
		int count = 0;
		List<MarketDataValue> underlyingValueList = getMarketDataValueList(security.getUnderlyingSecurity(), fromDataField.getId(), startDate, endDate);
		List<MarketDataValue> securityValueList = getMarketDataValueList(security, toDataField.getId(), startDate, endDate);

		for (MarketDataValue underlyingValue : CollectionUtils.getIterable(underlyingValueList)) {
			// Only copy price if the security is active on the value date
			if (InvestmentUtils.isSecurityActiveOn(security, underlyingValue.getMeasureDate())) {
				MarketDataValue mdv = null;
				for (MarketDataValue value : CollectionUtils.getIterable(securityValueList)) {
					if (DateUtils.compare(underlyingValue.getMeasureDate(), value.getMeasureDate(), false) == 0) {
						mdv = value;
						break;
					}
				}
				if (mdv == null || Boolean.TRUE.equals(getOverwriteExisting())) {
					if (mdv == null) {
						mdv = new MarketDataValue();
						mdv.setInvestmentSecurity(security);
						mdv.setDataField(toDataField);
						mdv.setDataSource(underlyingValue.getDataSource());
						mdv.setMeasureDate(underlyingValue.getMeasureDate());
						// Only copy time if to field is time sensitive
						if (toDataField.isTimeSensitive()) {
							if (underlyingValue.getMeasureTime() != null) {
								mdv.setMeasureTime(underlyingValue.getMeasureTime());
							}
							else {
								// If Copying From a Field that isn't time sensitive - default to 12 AM
								mdv.setMeasureTime(new Time(0));
							}
						}
					}
					mdv.setMeasureValueAdjustmentFactor(underlyingValue.getMeasureValueAdjustmentFactor());
					mdv.setMeasureValue(underlyingValue.getMeasureValue());
					if (BooleanUtils.isTrue(getAlterValueForFXRate()) && security.getInstrument().getTradingCurrency() != null && security.getUnderlyingSecurity().getInstrument().getTradingCurrency() != null && !security.getInstrument().getTradingCurrency().equals(security.getUnderlyingSecurity().getInstrument().getTradingCurrency())) {
						ValidationUtils.assertNotNull(mdv.getDataSource(), "DataSource for MarketDataValue cannot be null.");
						String fromCurrency = security.getUnderlyingSecurity().getInstrument().getTradingCurrency().getSymbol();
						String toCurrency = security.getInstrument().getTradingCurrency().getSymbol();
						BigDecimal fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forDataSource(mdv.getDataSource().getName(), fromCurrency, toCurrency, underlyingValue.getMeasureDate()));
						mdv.setMeasureValue(MathUtils.multiply(mdv.getMeasureValue(), fxRate));
					}
					// Fix rounding
					mdv.setMeasureValue(MathUtils.round(mdv.getMeasureValue(), mdv.getDataField().getDecimalPrecision()));
					getMarketDataFieldService().saveMarketDataValue(mdv);
					count++;
				}
			}
		}
		return count;
	}


	private List<MarketDataValue> getMarketDataValueList(InvestmentSecurity security, short marketDataFieldId, Date startDate, Date endDate) {
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm(true);
		searchForm.setInvestmentSecurityId(security.getId());
		searchForm.setDataFieldId(marketDataFieldId);
		searchForm.setMinMeasureDate(DateUtils.addDays(startDate, -1));
		searchForm.setMaxMeasureDate(DateUtils.addDays(endDate, 1));
		searchForm.setDataSourceId(getDataSourceId());
		return getMarketDataFieldService().getMarketDataValueList(searchForm);
	}


	private List<InvestmentSecurity> getSecurityList() {
		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setInvestmentTypeId(getInvestmentTypeId());
		searchForm.setInvestmentTypeSubTypeId(getInvestmentTypeSubTypeId());
		searchForm.setInvestmentTypeSubType2Id(getInvestmentTypeSubType2Id());
		getPropertyOptionAsBoolean(getHierarchyIsOTC()).ifPresent(searchForm::setOtc);
		searchForm.setUnderlyingInvestmentTypeId(getUnderlyingInvestmentTypeId());
		getPropertyOptionAsBoolean(getUnderlyingIsOneSecurityPerInstrument()).ifPresent(searchForm::setUnderlyingIsOneSecurityPerInstrument);
		getPropertyOptionAsBoolean(getSecurityStartDateIsPopulated()).ifPresent(startDatePopulated -> {
			ComparisonConditions comp = startDatePopulated ? ComparisonConditions.IS_NOT_NULL : ComparisonConditions.IS_NULL;
			searchForm.addSearchRestriction(new SearchRestriction("startDate", comp, new Date()));
		});
		getPropertyOptionAsBoolean(getSecurityEndDateIsPopulated()).ifPresent(endDatePopulated -> {
			ComparisonConditions comp = endDatePopulated ? ComparisonConditions.IS_NOT_NULL : ComparisonConditions.IS_NULL;
			searchForm.addSearchRestriction(new SearchRestriction("endDate", comp, new Date()));
		});
		searchForm.setInvestmentGroupId(getInstrumentGroupId());
		searchForm.setBusinessCompanyId(getIssuingCompanyId());
		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	private Optional<Boolean> getPropertyOptionAsBoolean(String value) {
		if (StringUtils.isEmpty(value)) {
			return Optional.empty();
		}
		return Optional.of(BooleanUtils.isTrue(value));
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDaysBackIncludingToday() {
		return this.daysBackIncludingToday;
	}


	public void setDaysBackIncludingToday(Short daysBackIncludingToday) {
		this.daysBackIncludingToday = daysBackIncludingToday;
	}


	public Boolean getOverwriteExisting() {
		return this.overwriteExisting;
	}


	public void setOverwriteExisting(Boolean overwriteExisting) {
		this.overwriteExisting = overwriteExisting;
	}


	public Short getFromMarketDataFieldId() {
		return this.fromMarketDataFieldId;
	}


	public void setFromMarketDataFieldId(Short fromMarketDataFieldId) {
		this.fromMarketDataFieldId = fromMarketDataFieldId;
	}


	public Short getToMarketDataFieldId() {
		return this.toMarketDataFieldId;
	}


	public void setToMarketDataFieldId(Short toMarketDataFieldId) {
		this.toMarketDataFieldId = toMarketDataFieldId;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Boolean getAlterValueForFXRate() {
		return this.alterValueForFXRate;
	}


	public void setAlterValueForFXRate(Boolean alterValueForFXRate) {
		this.alterValueForFXRate = alterValueForFXRate;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getHierarchyIsOTC() {
		return this.hierarchyIsOTC;
	}


	public void setHierarchyIsOTC(String hierarchyIsOTC) {
		this.hierarchyIsOTC = hierarchyIsOTC;
	}


	public String getSecurityStartDateIsPopulated() {
		return this.securityStartDateIsPopulated;
	}


	public void setSecurityStartDateIsPopulated(String securityStartDateIsPopulated) {
		this.securityStartDateIsPopulated = securityStartDateIsPopulated;
	}


	public String getSecurityEndDateIsPopulated() {
		return this.securityEndDateIsPopulated;
	}


	public void setSecurityEndDateIsPopulated(String securityEndDateIsPopulated) {
		this.securityEndDateIsPopulated = securityEndDateIsPopulated;
	}


	public Short getUnderlyingInvestmentTypeId() {
		return this.underlyingInvestmentTypeId;
	}


	public void setUnderlyingInvestmentTypeId(Short underlyingInvestmentTypeId) {
		this.underlyingInvestmentTypeId = underlyingInvestmentTypeId;
	}


	public String getUnderlyingIsOneSecurityPerInstrument() {
		return this.underlyingIsOneSecurityPerInstrument;
	}


	public void setUnderlyingIsOneSecurityPerInstrument(String underlyingIsOneSecurityPerInstrument) {
		this.underlyingIsOneSecurityPerInstrument = underlyingIsOneSecurityPerInstrument;
	}


	public String getUnderlyingSecurityEndDateEqualsSecurityEndDate() {
		return this.underlyingSecurityEndDateEqualsSecurityEndDate;
	}


	public void setUnderlyingSecurityEndDateEqualsSecurityEndDate(String underlyingSecurityEndDateEqualsSecurityEndDate) {
		this.underlyingSecurityEndDateEqualsSecurityEndDate = underlyingSecurityEndDateEqualsSecurityEndDate;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public Short getInstrumentGroupId() {
		return this.instrumentGroupId;
	}


	public void setInstrumentGroupId(Short instrumentGroupId) {
		this.instrumentGroupId = instrumentGroupId;
	}


	public Integer getIssuingCompanyId() {
		return this.issuingCompanyId;
	}


	public void setIssuingCompanyId(Integer issuingCompanyId) {
		this.issuingCompanyId = issuingCompanyId;
	}
}
