package com.clifton.marketdata.updater.jobs;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import com.clifton.system.hierarchy.definition.SystemHierarchy;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.hierarchy.definition.SystemHierarchyDefinitionService;
import com.clifton.system.hierarchy.definition.search.SystemHierarchySearchForm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MarketDataSystemHierarchyUpdaterJob implements Task {

	private Short dataSourceId;
	/**
	 * Comma delimited list of market data field names.  First field with that returns a value wins.
	 */
	private String dataFieldList;
	/**
	 * The investment group used to get the list of securities that will be updated.
	 */
	private Short investmentGroupId;
	/**
	 * Only update active securities.
	 */
	private Boolean activeSecuritiesOnly;
	/**
	 * The system hierarchy category to which the links will be created.
	 */
	private Short hierarchyCategoryId;
	/**
	 * Indicates weather or not to update existing links.  If true, the values will be updated.
	 */
	private Boolean updateIfExists;

	private InvestmentInstrumentService investmentInstrumentService;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataSourceService marketDataSourceService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingService marketDataFieldMappingService;

	private SystemHierarchyDefinitionService systemHierarchyDefinitionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		ValidationUtils.assertNotNull(getDataSourceId(), "Cannot run job without a DataSource.");
		ValidationUtils.assertNotNull(getDataFieldList(), "Cannot run job without a list of fields to update.");

		SystemHierarchyCategory category = getSystemHierarchyDefinitionService().getSystemHierarchyCategory(getHierarchyCategoryId());
		ValidationUtils.assertNotNull(category, "Cannot run job without a hierarchy category.");

		MarketDataSource dataSource = getMarketDataSourceService().getMarketDataSource(getDataSourceId());
		MarketDataProvider<MarketDataValueGeneric<?>> dataProvider = getMarketDataProviderLocator().locate(dataSource);

		List<String> fieldNames = Arrays.asList(getDataFieldList().split(","));

		List<MarketDataField> fieldList = new ArrayList<>();
		// the map of fields to the value conversion table
		Map<MarketDataField, Map<String, String>> fieldToFieldValueMap = new HashMap<>();
		for (String fieldName : CollectionUtils.getIterable(fieldNames)) {
			MarketDataFieldSearchForm searchForm = new MarketDataFieldSearchForm();
			searchForm.setName(fieldName);
			List<MarketDataField> fl = getMarketDataFieldService().getMarketDataFieldList(searchForm);
			MarketDataField field = fl.size() == 1 ? fl.get(0) : null;
			if (field == null) {
				searchForm = new MarketDataFieldSearchForm();
				searchForm.setExternalFieldName(fieldName);
				fl = getMarketDataFieldService().getMarketDataFieldList(searchForm);
				ValidationUtils.assertTrue(fl.size() <= 1, "Cannot run job because more than one MarketDataField was found for [" + fieldName + "].");
				field = fl.get(0);
			}
			if (field != null) {
				fieldList.add(field);
				fieldToFieldValueMap.put(field, getMarketDataFieldMappingService().getMarketDataFieldValueMappingMap(field.getId(), dataSource.getId()));
			}
		}

		int updated = 0, errors = 0;
		int securitiesCalled = 0;
		int bloombergCalls = 0;
		List<InvestmentInstrument> instrumentList = getInvestmentInstrumentList();
		Status status = Status.ofMessage("Starting updates");
		for (InvestmentInstrument instrument : CollectionUtils.getIterable(instrumentList)) {

			// if a link for the instrument exists then skip it
			List<SystemHierarchyLink> linkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkList("InvestmentInstrument", instrument.getId(), category.getName());
			if (!getUpdateIfExists() && !CollectionUtils.isEmpty(linkList)) {
				continue;
			}

			// delete any existing links
			for (SystemHierarchyLink link : CollectionUtils.getIterable(linkList)) {
				getSystemHierarchyAssignmentService().deleteSystemHierarchyLink(link.getId());
			}

			SecuritySearchForm searchForm = new SecuritySearchForm();
			searchForm.setInstrumentId(instrument.getId());
			List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);

			for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
				securitiesCalled++;
				try {
					for (MarketDataField field : CollectionUtils.getIterable(fieldList)) {
						Map<String, String> fieldValueMap = fieldToFieldValueMap.get(field);
						// look up the field value
						MarketDataValueGeneric<?> value = getLatestMarketDataField(field, security, dataProvider, status);
						bloombergCalls++;
						if (value != null) {
							String fieldValue = (new ObjectToStringConverter()).convert(value.getMeasureValue());
							Integer intValue;
							// get the value from the conversion table
							if (fieldValueMap != null && fieldValueMap.containsKey(fieldValue)) {
								intValue = Integer.valueOf(fieldValueMap.get(fieldValue));
							}
							else {
								intValue = Integer.valueOf(fieldValue);
							}

							SystemHierarchy hierarchy = getSystemHierarchy(category, intValue);
							if (hierarchy != null) {
								getSystemHierarchyAssignmentService().saveSystemHierarchyLink("InvestmentInstrument", instrument.getId(), hierarchy.getId());
								updated++;
								break;
							}
						}
					}
				}
				catch (Throwable e) {
					// full log for first error and only causes for consecutive errors
					status.addError("Failed update security [" + security + "]." + "\nCAUSED BY " + ExceptionUtils.getDetailedMessage(e));
				}
			}
		}

		StringBuilder result = new StringBuilder();
		result.append("Update count = ");
		result.append(updated);
		result.append(". Securities called = ");
		result.append(securitiesCalled);
		result.append(". Bloomberg calls = ");
		result.append(bloombergCalls);
		result.append(". Error count = ");
		result.append(errors);

		status.setMessage(result.toString());
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataValueGeneric<?> getLatestMarketDataField(MarketDataField field, InvestmentSecurity security, MarketDataProvider<MarketDataValueGeneric<?>> dataProvider, Status status) {
		try {
			return dataProvider.getMarketDataValueLatest(DataFieldValueCommand.ofFieldAndSecurity(field, security)).getSingleMarketDataValueStrict();
		}
		catch (Throwable e) {
			// full log for first error and only causes for consecutive errors
			status.addError("Failed to find field for [" + security + "]." + "\nCAUSED BY " + ExceptionUtils.getDetailedMessage(e));
		}
		return null;
	}


	private SystemHierarchy getSystemHierarchy(SystemHierarchyCategory category, Integer value) {
		SystemHierarchySearchForm searchForm = new SystemHierarchySearchForm();
		searchForm.setCategoryId(category.getId());
		searchForm.setOrder(value);
		searchForm.setIgnoreNullParent(true);
		return CollectionUtils.getOnlyElement(getSystemHierarchyDefinitionService().getSystemHierarchyList(searchForm));
	}


	private List<InvestmentInstrument> getInvestmentInstrumentList() {
		InstrumentSearchForm searchForm = new InstrumentSearchForm();
		searchForm.setInvestmentGroupId(getInvestmentGroupId());
		if (getActiveSecuritiesOnly()) {
			searchForm.setInactive(false);
		}
		return getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataFieldList() {
		return this.dataFieldList;
	}


	public void setDataFieldList(String dataFieldList) {
		this.dataFieldList = dataFieldList;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Boolean getActiveSecuritiesOnly() {
		return this.activeSecuritiesOnly;
	}


	public void setActiveSecuritiesOnly(Boolean activeSecuritiesOnly) {
		this.activeSecuritiesOnly = activeSecuritiesOnly;
	}


	public Boolean getUpdateIfExists() {
		return this.updateIfExists;
	}


	public void setUpdateIfExists(Boolean updateIfExists) {
		this.updateIfExists = updateIfExists;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public SystemHierarchyDefinitionService getSystemHierarchyDefinitionService() {
		return this.systemHierarchyDefinitionService;
	}


	public void setSystemHierarchyDefinitionService(SystemHierarchyDefinitionService systemHierarchyDefinitionService) {
		this.systemHierarchyDefinitionService = systemHierarchyDefinitionService;
	}


	public Short getHierarchyCategoryId() {
		return this.hierarchyCategoryId;
	}


	public void setHierarchyCategoryId(Short hierarchyCategoryId) {
		this.hierarchyCategoryId = hierarchyCategoryId;
	}
}
