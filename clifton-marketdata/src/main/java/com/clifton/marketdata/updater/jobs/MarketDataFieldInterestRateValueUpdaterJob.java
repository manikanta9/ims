package com.clifton.marketdata.updater.jobs;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.marketdata.updater.MarketDataInterestRateUpdaterCommand;
import com.clifton.marketdata.updater.MarketDataUpdaterService;

import java.util.Date;
import java.util.Map;


/**
 * The <code>MarketDataFieldInterestRateValueUpdaterJob</code> class is a batch job that updates interest rate values for a given time period.
 */
public class MarketDataFieldInterestRateValueUpdaterJob implements Task {

	private MarketDataUpdaterService marketDataUpdaterService;

	/**
	 * The numbers of days back from Today (inclusive) to start getting values from
	 */
	private int daysBackIncludingTodayFrom;
	/**
	 * The numbers of days back from Today (inclusive) to stop getting values to
	 */
	private int daysBackIncludingTodayTo;

	/**
	 * Optionally restrict updates to this InvestmentInterestRateIndexGroup.
	 */
	private Short interestRateIndexGroupId;


	/**
	 * Do not get interest rates for indexes when the data already exists
	 */
	private boolean skipIfAlreadyExists;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Date now = new Date();
		Date startDate = DateUtils.clearTime(DateUtils.addDays(now, -getDaysBackIncludingTodayFrom()));
		Date endDate = DateUtils.getEndOfDay(DateUtils.addDays(now, -getDaysBackIncludingTodayTo()));

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, getInterestRateIndexGroupId(), startDate, endDate, isSkipIfAlreadyExists());
		return getMarketDataUpdaterService().updateMarketDataInterestRates(command);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataUpdaterService getMarketDataUpdaterService() {
		return this.marketDataUpdaterService;
	}


	public void setMarketDataUpdaterService(MarketDataUpdaterService marketDataUpdaterService) {
		this.marketDataUpdaterService = marketDataUpdaterService;
	}


	public int getDaysBackIncludingTodayFrom() {
		return this.daysBackIncludingTodayFrom;
	}


	public void setDaysBackIncludingTodayFrom(int daysBackIncludingTodayFrom) {
		this.daysBackIncludingTodayFrom = daysBackIncludingTodayFrom;
	}


	public int getDaysBackIncludingTodayTo() {
		return this.daysBackIncludingTodayTo;
	}


	public void setDaysBackIncludingTodayTo(int daysBackIncludingTodayTo) {
		this.daysBackIncludingTodayTo = daysBackIncludingTodayTo;
	}


	public Short getInterestRateIndexGroupId() {
		return this.interestRateIndexGroupId;
	}


	public void setInterestRateIndexGroupId(Short interestRateIndexGroupId) {
		this.interestRateIndexGroupId = interestRateIndexGroupId;
	}


	public boolean isSkipIfAlreadyExists() {
		return this.skipIfAlreadyExists;
	}


	public void setSkipIfAlreadyExists(boolean skipIfAlreadyExists) {
		this.skipIfAlreadyExists = skipIfAlreadyExists;
	}
}
