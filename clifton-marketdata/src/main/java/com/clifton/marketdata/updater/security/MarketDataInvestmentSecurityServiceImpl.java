package com.clifton.marketdata.updater.security;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingCommand;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingService;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomAware;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.schema.column.value.SystemColumnValueHolder;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MarketDataInvestmentSecurityServiceImpl implements MarketDataInvestmentSecurityService {

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSetupService investmentSetupService;

	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataFieldColumnMappingService marketDataFieldColumnMappingService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataSourceService marketDataSourceService;
	private SystemColumnValueService systemColumnValueService;

	private MarketDataInvestmentSecurityUtilHandler marketDataInvestmentSecurityUtilHandler;
	private SystemColumnValueHandler systemColumnValueHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void saveMarketDataInvestmentSecurityFieldUpdateResult(MarketDataInvestmentSecurityFieldUpdateResult fieldData) {
		Map<MarketDataFieldColumnMapping, Object> valueMap = new HashMap<>();
		MarketDataFieldColumnMapping mapping = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMapping(fieldData.getDataFieldColumnMapping().getId());
		valueMap.put(mapping, fieldData.getNewValue());

		List<String> errors = new ArrayList<>();
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(fieldData.getSecurity().getId());
		MarketDataInvestmentSecurityRetrieverCommand command = MarketDataInvestmentSecurityRetrieverCommand.ofSecurity(security, errors);
		if ("InvestmentInstrument".equals(mapping.getColumn().getTable().getName())) {
			InvestmentInstrument i = getMarketDataInvestmentSecurityUtilHandler().applyInvestmentInstrumentMarketDataValue(valueMap, true, command);
			getInvestmentInstrumentService().saveInvestmentInstrument(i);
		}
		else {
			InvestmentSecurity s = getMarketDataInvestmentSecurityUtilHandler().applyInvestmentSecurityMarketDataValue(valueMap, true, command);
			getInvestmentInstrumentService().saveInvestmentSecurity(s);
		}

		// throw the error if one occurred
		if (!CollectionUtils.isEmpty(errors)) {
			StringBuilder error = new StringBuilder();
			if (!errors.isEmpty()) {
				error.append("Error count = ");
				error.append(errors.size());
				for (String err : errors) {
					error.append("\n\n");
					error.append(err);
				}
			}
			throw new RuntimeException(error.toString());
		}
	}


	@Override
	public List<MarketDataInvestmentSecurityFieldUpdateResult> updateMarketDataInvestmentSecurityFieldData(MarketDataInvestmentSecurityRetrieverCommand retrieverCommand) {
		return doGetMarketDataInvestmentSecurityFieldDataPreview(retrieverCommand, true);
	}


	@Override
	public List<MarketDataInvestmentSecurityFieldUpdateResult> getMarketDataInvestmentSecurityFieldDataPreview(MarketDataInvestmentSecurityRetrieverCommand retrieverCommand) {
		return doGetMarketDataInvestmentSecurityFieldDataPreview(retrieverCommand, false);
	}


	@Override
	public MarketDataInvestmentSecurityResult getMarketDataInvestmentSecurity(MarketDataInvestmentSecurityRetrieverCommand command) {
		command = resolveMarketDataInvestmentSecurityRetrieverCommand(command);
		command.setApplyToSecurity(true);
		return doGetMarketDataInvestmentSecurity(command, null);
	}


	@Override
	public List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingList(MarketDataInvestmentSecurityRetrieverCommand command) {
		command = resolveMarketDataInvestmentSecurityRetrieverCommand(command);
		command.setErrors(new ArrayList<>());
		MarketDataFieldColumnMappingCommand<InvestmentInstrument> instrumentMappingCommand = getMarketDataInvestmentSecurityUtilHandler().getDataFieldColumnMappingCommand(command, InvestmentInstrument.class);
		List<MarketDataFieldColumnMapping> resultList = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingListForBean(instrumentMappingCommand);

		MarketDataFieldColumnMappingCommand<InvestmentSecurity> securityMappingCommand = getMarketDataInvestmentSecurityUtilHandler().getDataFieldColumnMappingCommand(command, InvestmentSecurity.class);
		resultList.addAll(getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingListForBean(securityMappingCommand));

		resultList.addAll(getMarketDataInvestmentSecurityUtilHandler().getMappingObjectForUnmappedColumns(command, resultList));

		List<MarketDataFieldColumnMapping> filteredList = new ArrayList<>();

		for (MarketDataFieldColumnMapping result : CollectionUtils.getIterable(resultList)) {
			if ((result.getColumn() instanceof SystemColumnStandard && !BeanUtils.isSystemManagedField(((SystemColumnStandard) result.getColumn()).getBeanPropertyName())) || result.getColumn() instanceof SystemColumnCustom || result.getTemplate() != null) {
				filteredList.add(result);
			}
		}
		return BeanUtils.sortWithFunction(filteredList, marketDataFieldColumnMapping -> (marketDataFieldColumnMapping.getColumn() == null ? null : marketDataFieldColumnMapping.getColumn().getName()), true);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private List<MarketDataInvestmentSecurityFieldUpdateResult> doGetMarketDataInvestmentSecurityFieldDataPreview(MarketDataInvestmentSecurityRetrieverCommand retrieverCommand, boolean saveSecurities) {
		ValidationUtils.assertFalse(retrieverCommand.getInvestmentGroupId() == null && retrieverCommand.getInstrumentHierarchyId() == null,
				"An investment group or hierarchy is required to update securities.");

		retrieverCommand = resolveMarketDataInvestmentSecurityRetrieverCommand(retrieverCommand);

		List<InvestmentSecurity> securityList;
		if ((retrieverCommand.getSecurity() != null) && !retrieverCommand.getSecurity().isNewBean()) {
			securityList = CollectionUtils.createList(retrieverCommand.getSecurity());
		}
		else {
			securityList = getInvestmentInstrumentService().getInvestmentSecurityList(retrieverCommand.getSecuritySearchForm());
		}
		List<MarketDataInvestmentSecurityFieldUpdateResult> result = new ArrayList<>();
		retrieverCommand.setErrors(new ArrayList<>());

		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			MarketDataInvestmentSecurityRetrieverCommand command = null;
			try {
				command = new MarketDataInvestmentSecurityRetrieverCommand();
				BeanUtils.copyProperties(retrieverCommand, command);
				command.setSecurity(security);
				command.setErrors(new ArrayList<>());
				command.setApplyToSecurity(saveSecurities);
				command = resolveMarketDataInvestmentSecurityRetrieverCommand(command);
				result.addAll(doGetMarketDataInvestmentSecurityFieldData(command));

				if (saveSecurities) {
					saveUpdatedInvestmentSecurity(security);
				}
			}
			// security level errors
			catch (Throwable e) {
				handleSecurityError(security, retrieverCommand.getErrors(), e);
			}

			if ((command != null) && (command.getErrors() != null)) {
				retrieverCommand.getErrors().addAll(command.getErrors());
			}
		}

		return result;
	}


	private void saveUpdatedInvestmentSecurity(InvestmentSecurity security) {
		getInvestmentInstrumentService().saveInvestmentInstrument(security.getInstrument());
		getInvestmentInstrumentService().saveInvestmentSecurity(security);
	}


	private List<MarketDataInvestmentSecurityFieldUpdateResult> doGetMarketDataInvestmentSecurityFieldData(MarketDataInvestmentSecurityRetrieverCommand command) {
		MarketDataFieldColumnMappingHolder columnMappingHolder = new MarketDataFieldColumnMappingHolder();
		MarketDataInvestmentSecurityResult securityResult = doGetMarketDataInvestmentSecurity(command, columnMappingHolder);
		return getMarketDataInvestmentSecurityFieldData(command, securityResult, columnMappingHolder);
	}


	private MarketDataInvestmentSecurityResult doGetMarketDataInvestmentSecurity(MarketDataInvestmentSecurityRetrieverCommand command, MarketDataFieldColumnMappingHolder columnMappingHolder) {
		command.setErrors(new ArrayList<>());
		if (columnMappingHolder == null) {
			columnMappingHolder = new MarketDataFieldColumnMappingHolder();
		}
		return getMarketDataInvestmentSecurityUtilHandler().getSecurityMarketDataValues(command, columnMappingHolder);
	}


	private List<MarketDataInvestmentSecurityFieldUpdateResult> getMarketDataInvestmentSecurityFieldData(MarketDataInvestmentSecurityRetrieverCommand command, MarketDataInvestmentSecurityResult securityResult, MarketDataFieldColumnMappingHolder columnMappingHolder) {
		List<MarketDataInvestmentSecurityFieldUpdateResult> result = new ArrayList<>();

		try {
			if (columnMappingHolder.getSecurityValueMap() != null) {
				for (MarketDataFieldColumnMapping mapping : columnMappingHolder.getSecurityValueMap().keySet()) {
					MarketDataInvestmentSecurityFieldUpdateResult fieldResult = getFieldResult(mapping, securityResult, command);
					if (fieldResult != null) {
						result.add(fieldResult);
					}
				}
			}
		}
		// security level errors
		catch (Throwable e) {
			handleSecurityError(command.getSecurity(), command.getErrors(), e);
		}
		return result;
	}


	private MarketDataInvestmentSecurityFieldUpdateResult getFieldResult(MarketDataFieldColumnMapping mapping, MarketDataInvestmentSecurityResult securityResult, MarketDataInvestmentSecurityRetrieverCommand command) {
		MarketDataInvestmentSecurityFieldUpdateResult fieldResult = null;
		try {
			SystemColumn column = mapping.getColumn() != null ? mapping.getColumn() : getMarketDataFieldColumnMappingService().resolveSystemColumnFromTemplate(mapping, securityResult.getExistingSecurity());
			if ("InvestmentInstrument".equals(column.getTable().getName())) {
				fieldResult = getFieldResultFromColumn(column, securityResult.getExistingSecurity().getInstrument(), securityResult.getNewSecurity().getInstrument(), command.isIncludeUnchanged());
			}
			else {
				fieldResult = getFieldResultFromColumn(column, securityResult.getExistingSecurity(), securityResult.getNewSecurity(), command.isIncludeUnchanged());
			}
			if (fieldResult != null) {
				fieldResult.setDataFieldColumnMapping(mapping);
				fieldResult.setSecurity(command.getSecurity());
				fieldResult.setId(mapping.getId() + "_" + command.getSecurity().getId());
			}
		}
		// field level errors
		catch (Throwable e) {
			handleSecurityError(command.getSecurity(), command.getErrors(), e);
		}
		return fieldResult;
	}


	private MarketDataInvestmentSecurityFieldUpdateResult getFieldResultFromColumn(SystemColumn column, SystemColumnCustomAware originalObject, SystemColumnCustomAware newObject, Boolean includeUnchanged) {
		MarketDataInvestmentSecurityFieldUpdateResult result = null;
		if (column.isCustomColumn()) {

			SystemColumnValueHolder originalValueHolder = getSystemColumnValueHandler().getSystemColumnValueHolderForEntity(originalObject, originalObject.getColumnGroupName(), column.getName(), true);
			SystemColumnValueHolder newValueHolder = getSystemColumnValueHandler().getSystemColumnValueHolderForEntity(newObject, newObject.getColumnGroupName(), column.getName(), true);

			String originalValue = originalValueHolder == null ? null : (new ObjectToStringConverter()).convert(originalValueHolder.getValue());
			String originalText = originalValueHolder == null ? null : originalValueHolder.getText();

			String newValue = newValueHolder == null ? null : (new ObjectToStringConverter()).convert(newValueHolder.getValue());
			String newText = newValueHolder == null ? null : newValueHolder.getText();

			if (includeUnchanged || (StringUtils.compare(originalValue, newValue) != 0)) {
				result = new MarketDataInvestmentSecurityFieldUpdateResult();
				result.setOriginalText(originalText);
				result.setOriginalValue(originalValue);
				result.setNewText(newText);
				result.setNewValue(newValue);
				return result;
			}
		}
		else {
			String propertyName = ((SystemColumnStandard) column).getBeanPropertyName();

			Object ov = BeanUtils.getPropertyValue(originalObject, propertyName, true);
			Object nv = BeanUtils.getPropertyValue(newObject, propertyName, true);

			String originalValue = ov != null ? (new ObjectToStringConverter()).convert(ov).trim() : null;
			String newValue = nv != null ? (new ObjectToStringConverter()).convert(nv).trim() : null;
			if (includeUnchanged || (!(StringUtils.isEmpty(newValue) && !StringUtils.isEmpty(originalValue)) && (StringUtils.compare(originalValue, newValue) != 0))) {
				result = new MarketDataInvestmentSecurityFieldUpdateResult();

				result.setOriginalValue(originalValue);
				result.setNewValue(newValue);
			}
		}
		return result;
	}


	private MarketDataInvestmentSecurityRetrieverCommand resolveMarketDataInvestmentSecurityRetrieverCommand(MarketDataInvestmentSecurityRetrieverCommand command) {
		command.setDataSource(getDataSourceFromCommand(command, true));

		if (command.getInvestmentSecurityId() != null && command.getSecurity() == null) {
			command.setSecurity(getInvestmentInstrumentService().getInvestmentSecurity(command.getInvestmentSecurityId()));
			applyExistingSystemColumnValueList(command.getSecurity());
		}
		if (command.getSecurity() != null) {
			command.setSymbol(command.getSecurity().getSymbol());
		}
		if (command.getInstrumentHierarchyId() != null && command.getHierarchy() == null) {
			command.setHierarchy(getInvestmentSetupService().getInvestmentInstrumentHierarchy(command.getInstrumentHierarchyId()));
		}
		else if (command.getSecurity() != null) {
			command.setHierarchy(command.getSecurity().getInstrument().getHierarchy());
		}
		if (command.getInstrumentId() != null && command.getInstrument() == null) {
			command.setInstrument(getInvestmentInstrumentService().getInvestmentInstrument(command.getInstrumentId()));
		}
		if (command.getSecurity() == null) {
			ValidationUtils.assertTrue(command.getHierarchy().isOneSecurityPerInstrument() || (command.getInstrument() != null),
					"Instrument is required when creating securities from instruments with more than one security (i.e. Futures).");
		}

		MarketDataFieldMapping mapping = null;
		if (command.getSecurity() != null) {
			mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(command.getSecurity().getInstrument(), command.getDataSource());
			if (mapping == null) {
				throw new ValidationException("Market Data Mapping is not defined for security [" + command.getSecurity().getSymbol() + "].  Unable to determine which market sector to lookup values from.");
			}
			command.setMarketSector(mapping.getMarketSector().getName());
			command.getSecurity().setColumnGroupName(InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);
		}
		else if ((command.getInstrument() != null) || (command.getHierarchy() != null)) {
			if (command.getInstrument() != null) {
				mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(command.getInstrument(), command.getDataSource());
			}
			else {
				mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByHierarchy(command.getHierarchy(), command.getDataSource());
			}
			if (mapping == null) {
				throw new ValidationException("Market Data Mapping is not defined for security [" + command.getSymbol() + "].  Unable to determine which market sector to lookup values from.");
			}
			command.setMarketSector(mapping.getMarketSector().getName());
		}

		if (command.getSecurity() == null) {
			InvestmentSecurity security = new InvestmentSecurity();
			security.setSymbol(command.getSymbol());
			if (command.getInstrument() != null) {
				security.setInstrument(command.getInstrument());
				command.setSymbol(getMarketDataInvestmentSecurityUtilHandler().removeExchangeCodeFromSymbol(security));
				security.setSymbol(command.getSymbol());
			}
			else if (command.getHierarchy() != null) {
				InvestmentInstrument instrument = new InvestmentInstrument();
				instrument.setHierarchy(command.getHierarchy());
				security.setInstrument(instrument);
			}
			command.setSecurity(security);
		}
		else {
			command.setSymbol(getMarketDataInvestmentSecurityUtilHandler().removeExchangeCodeFromSymbol(command.getSecurity()));
		}
		if ((mapping != null) && (mapping.getExchangeCodeType() != null)) {
			command.setExchangeCode(InvestmentExchangeTypes.COMPOSITE_OR_PRIMARY_EXCHANGE.getSecurityExchangeCode(command.getSecurity()));
		}
		return command;
	}


	private void applyExistingSystemColumnValueList(SystemColumnCustomValueAware bean) {
		if (!bean.isNewBean()) {
			SystemColumnValueSearchForm sf = new SystemColumnValueSearchForm();
			sf.setColumnGroupName(bean.getColumnGroupName());
			sf.setFkFieldId(BeanUtils.getIdentityAsInteger(bean));
			List<SystemColumnValue> columnValues = getSystemColumnValueService().getSystemColumnValueList(sf);
			bean.setColumnValueList(columnValues);
		}
	}


	/**
	 * Returns the MarketDataSource object for the specified name or default data source if the name is empty or null.
	 */
	private MarketDataSource getDataSourceFromCommand(MarketDataInvestmentSecurityRetrieverCommand command, boolean useDefault) {
		if (command.getMarketDataSourceId() != null) {
			return getMarketDataSourceService().getMarketDataSource(command.getMarketDataSourceId());
		}
		String dataSourceName = command.getMarketDataSourceName();
		if (StringUtils.isEmpty(dataSourceName)) {
			dataSourceName = getMarketDataProviderLocator().getDefaultDataSourceName();
		}
		MarketDataSource result = getMarketDataSourceService().getMarketDataSourceByName(dataSourceName);
		if (result == null && !useDefault) {
			throw new RuntimeException("No MarketDataSource found for name " + getMarketDataProviderLocator().getDefaultDataSourceName() + ", please create one.");
		}
		else if (result == null) {
			result = getMarketDataSourceService().getMarketDataSourceByName(getMarketDataProviderLocator().getDefaultDataSourceName());
		}
		return result;
	}


	private void handleSecurityError(InvestmentSecurity security, List<String> errors, Throwable e) {
		LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Failed to get fields for security [" + (security != null ? security.getLabel() : "unknown") + "]."));
		// full log for first error and only causes for consecutive errors
		StringBuilder error = new StringBuilder();
		error.append("Failed update security [").append(security).append("].");
		error.append("\nCAUSED BY ");
		error.append(ExceptionUtils.getDetailedMessage(e));
		errors.add(error.toString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataFieldColumnMappingService getMarketDataFieldColumnMappingService() {
		return this.marketDataFieldColumnMappingService;
	}


	public void setMarketDataFieldColumnMappingService(MarketDataFieldColumnMappingService marketDataFieldColumnMappingService) {
		this.marketDataFieldColumnMappingService = marketDataFieldColumnMappingService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataInvestmentSecurityUtilHandler getMarketDataInvestmentSecurityUtilHandler() {
		return this.marketDataInvestmentSecurityUtilHandler;
	}


	public void setMarketDataInvestmentSecurityUtilHandler(MarketDataInvestmentSecurityUtilHandler marketDataInvestmentSecurityUtilHandler) {
		this.marketDataInvestmentSecurityUtilHandler = marketDataInvestmentSecurityUtilHandler;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}
}
