package com.clifton.marketdata.updater.security;


import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>MarketDataInvestmentSecurityHolder</code> holds the result of the security lookup and an errors
 * that occurred while looking up fields for the specified security.
 *
 * @author mwacker
 */
public class MarketDataInvestmentSecurityResult {

	private InvestmentSecurity newSecurity;
	private InvestmentSecurity existingSecurity;
	private String error;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getNewSecurity() {
		return this.newSecurity;
	}


	public void setNewSecurity(InvestmentSecurity newSecurity) {
		this.newSecurity = newSecurity;
	}


	public String getError() {
		return this.error;
	}


	public void setError(String error) {
		this.error = error;
	}


	public InvestmentSecurity getExistingSecurity() {
		return this.existingSecurity;
	}


	public void setExistingSecurity(InvestmentSecurity existingSecurity) {
		this.existingSecurity = existingSecurity;
	}
}
