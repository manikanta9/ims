package com.clifton.marketdata.updater.jobs;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityService;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TreeSet;
import java.util.stream.Collectors;


/**
 * Batch job type for copying market data values between similar securities. Securities are deemed to be <i>similar</i>
 * when all non-excluded properties are equal.
 * <p>
 * This job is useful, for example, when there is a fee associated with determining a market data value for a security.
 * If there are multiple identical securities (given the <i>similar</i> criteria), this job will allow us to reduce
 * cost by only obtaining the market data value for a single one of these securities and then copying that value to the
 * other similar securities.
 *
 * @author MikeH
 */
public class MarketDataValueCopyFromSimilarSecuritiesJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private StatusHolderObject<Status> statusHolderObject;

	// Services
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataInvestmentSecurityService marketDataInvestmentSecurityService;
	private SystemColumnValueService systemColumnValueService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Security filters
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private Short investmentInstrumentHierarchyId;
	private Short investmentInstrumentGroupId;

	/**
	 * Number of days back (regular days, not calendar days) for which market data values should be copied.
	 */
	private Short daysBackIncludingToday;

	/**
	 * If true, overwrite existing market data values on each day with the latest-applied market data value for that
	 * day. Otherwise, only non-existent market data values will be created.
	 */
	private boolean overwriteExisting;

	/**
	 * If true, securities in similar (non-identical) instruments will qualify for grouping. Otherwise, only securities
	 * in the same exact instrument will qualify for grouping.
	 */
	private boolean groupSimilarInstruments;

	/**
	 * If true, include non-OTC securities. Otherwise, only OTC securities will be processed.
	 */
	private boolean includeNonOtcSecurities;

	/**
	 * The field group to restrict market data value fields which should be copied between similar securities, if
	 * present. If not present, all market data value fields will be copied.
	 */
	private Short marketDataFieldGroupId;

	/**
	 * The data source to restrict market data values which should be copied between similar securities, if present.
	 * If not present, all market data values will be copied.
	 */
	private Short marketDataSourceId;

	/**
	 * The list of fields to be excluded when comparing securities to determine if they qualify as <i>similar</i>.
	 */
	private List<String> excludeFromComparisonFields;

	/**
	 * The default list of configurable fields to be excluded when comparing securities to determine if they qualify as
	 * <i>similar</i>.
	 * <p>
	 * This list is used if the {@link #excludeFromComparisonFields} field is not configured.
	 */
	private static final List<String> DEFAULT_EXCLUDE_FROM_COMPARISON_FIELDS = CollectionUtils.createList(
			"businessCompany", "businessContract", "cusip", "description", "isin", "name", "occSymbol", "sedol", "symbol",
			"instrument.name", "instrument.description");

	/**
	 * The fields which will always be excluded when comparing securities to see if they qualify as <i>similar</i>.
	 */
	private static final List<String> ALWAYS_EXCLUDE_FROM_COMPARISON_FIELDS = CollectionUtils.createList(
			"columnGroupName", "columnValueList", "id", "instrument", "underlyingSecurity",
			"instrument.columnGroupName", "instrument.columnValueList", "instrument.identifierPrefix", "instrument.hierarchy",
			"instrument.tradingCurrency", "instrument.underlyingInstrument");

	/**
	 * The fields which are not first-level properties of the security bean but which should be compared regardless to
	 * test if securities qualify as <i>similar</i>.
	 */
	private static final List<String> ADDITIONAL_SECURITY_COMPARISON_FIELDS = CollectionUtils.createList(
			"instrument.id", "underlyingSecurity.id");

	/**
	 * The fields which are not first-level properties of the instrument bean but which should be compared regardless to
	 * test if securities qualify as <i>similar</i>.
	 * <p>
	 * These fields will only be applied if {@link #groupSimilarInstruments} is true.
	 */
	private static final List<String> ADDITIONAL_INSTRUMENT_COMPARISON_FIELDS = CollectionUtils.createList(
			"instrument.hierarchy.id", "instrument.tradingCurrency.id", "instrument.underlyingInstrument.id");


	static {
		// Ignore bean-managed fields
		List<String> beanManagedFields = CollectionUtils.createList(BeanUtils.getSystemManagedFields());
		ALWAYS_EXCLUDE_FROM_COMPARISON_FIELDS.addAll(beanManagedFields);
		ALWAYS_EXCLUDE_FROM_COMPARISON_FIELDS.addAll(
				CollectionUtils.getStream(beanManagedFields)
						.map("instrument."::concat)
						.collect(Collectors.toList()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertTrue(getInvestmentTypeId() != null
						|| getInvestmentInstrumentHierarchyId() != null
						|| getInvestmentInstrumentGroupId() != null,
				"An investment type, hierarchy, or instrument group must be provided.");
	}


	@Override
	public Status run(Map<String, Object> context) {
		StatusContext statusContext = new StatusContext(this.statusHolderObject);

		// 1. Get all securities
		List<InvestmentSecurity> securityList = getSecurityList(statusContext);

		// 2. Group securities by hierarchy for processing
		Collection<List<InvestmentSecurity>> securitiesByHierarchy = getSecuritiesByHierarchy(statusContext, securityList);
		statusContext.updateStatusMessage(false);
		for (List<InvestmentSecurity> securityHierarchy : securitiesByHierarchy) {
			statusContext.addCurrentHierarchyBeingProcessed(1);
			statusContext.updateStatusMessage(false);

			// 3. Populate custom column values
			populateCustomColumnValues(statusContext, securityHierarchy, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME);

			// 4. Group into collections of "similar" securities
			Collection<List<InvestmentSecurity>> similarSecuritiesGroups = getSimilarSecuritiesGroups(statusContext, securityHierarchy);

			// 5. Get market data values changes to apply
			List<MarketDataValue> dataValuesToSave = getMarketDataValuesToSave(statusContext, similarSecuritiesGroups);

			// 6. Save market data value changes
			saveMarketDataValues(statusContext, dataValuesToSave);
		}

		// Set status for job completion
		statusContext.setLastAction("Processing completed");
		statusContext.updateStatusMessage(false);
		this.statusHolderObject.getStatus().addErrors(statusContext.getErrors());

		return this.statusHolderObject.getStatus();
	}


	/**
	 * Gets the list of investment securities based on the specified security filters.
	 *
	 * @param statusContext the status context object
	 * @return the list of investment securities
	 */
	private List<InvestmentSecurity> getSecurityList(StatusContext statusContext) {
		statusContext.setLastAction("Retrieving securities");
		statusContext.updateStatusMessage(false);
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInvestmentTypeId(getInvestmentTypeId());
		securitySearchForm.setInvestmentTypeSubTypeId(getInvestmentTypeSubTypeId());
		securitySearchForm.setInvestmentTypeSubType2Id(getInvestmentTypeSubType2Id());
		securitySearchForm.setHierarchyId(getInvestmentInstrumentHierarchyId());
		securitySearchForm.setInvestmentGroupId(getInvestmentInstrumentGroupId());
		securitySearchForm.setOneSecurityPerInstrument(isGroupSimilarInstruments() ? null : false);
		securitySearchForm.setOtc(isIncludeNonOtcSecurities() ? null : true);
		// Allocations of securities are generated from data from other securities -- we do not want to copy market data values for these
		securitySearchForm.setAllocationOfSecurities(false);
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);
		statusContext.addSecuritiesCount(CollectionUtils.getSize(securityList));
		statusContext.updateStatusMessage(false);
		return securityList;
	}


	/**
	 * Gets the given securities, grouped into collections by common hierarchy.
	 *
	 * @param statusContext the status context object
	 * @param securityList  the list of all securities
	 * @return the collection of securities, grouped by hierarchy
	 */
	private Collection<List<InvestmentSecurity>> getSecuritiesByHierarchy(StatusContext statusContext, List<InvestmentSecurity> securityList) {
		Collection<List<InvestmentSecurity>> securitiesByHierarchy = CollectionUtils.getValues(
				BeanUtils.getBeansMap(securityList, security -> security.getInstrument().getHierarchy().getId()));
		statusContext.addHierarchiesCount(CollectionUtils.getSize(securitiesByHierarchy));
		return securitiesByHierarchy;
	}


	/**
	 * Populates the custom column field values for each entity in the given entity list for the given column group.
	 *
	 * @param statusContext   the status context object
	 * @param entityList      the entity list for which to obtain custom values
	 * @param columnGroupName the name of the column group
	 */
	private void populateCustomColumnValues(StatusContext statusContext, List<? extends SystemColumnCustomValueAware> entityList, String columnGroupName) {
		statusContext.setLastAction("Populating custom column values");
		statusContext.updateStatusMessage(false);

		// Obtain column values for all entities
		SystemColumnValueSearchForm systemColumnValueSearchForm = new SystemColumnValueSearchForm();
		systemColumnValueSearchForm.setColumnGroupName(columnGroupName);
		systemColumnValueSearchForm.setFkFieldIds(BeanUtils.getBeanIdentityArray(entityList, Integer.class));
		List<SystemColumnValue> systemColumnValueList = getSystemColumnValueService().getSystemColumnValueList(systemColumnValueSearchForm);

		// Group column values by entity ID
		Map<Integer, List<SystemColumnValue>> systemColumnValueMap = CollectionUtils.getStream(systemColumnValueList)
				.collect(Collectors.groupingBy(SystemColumnValue::getFkFieldId));

		// Attach data to entity
		for (SystemColumnCustomValueAware entity : CollectionUtils.getIterable(entityList)) {
			Integer entityId = (Integer) entity.getIdentity();
			entity.setColumnGroupName(columnGroupName);
			entity.setColumnValueList(systemColumnValueMap.get(entityId));
		}
	}


	/**
	 * Gets the buckets of similar securities within the given list. Securities are considered <i>similar</i>
	 * when each of their non-excluded properties are equal.
	 *
	 * @param statusContext the status context object
	 * @param securityList  the list of securities from which to find similar securities
	 * @return a collection containing groups of similar securities
	 */
	private Collection<List<InvestmentSecurity>> getSimilarSecuritiesGroups(StatusContext statusContext, List<InvestmentSecurity> securityList) {
		statusContext.setLastAction("Grouping similar securities");
		statusContext.updateStatusMessage(false);

		// Get property names to compare (sorted alphabetically with no duplicates)
		TreeSet<String> propertyNames = new TreeSet<>(String::compareTo);
		propertyNames.addAll(BeanUtils.getPropertyNames(InvestmentSecurity.class, true, true));
		propertyNames.addAll(ADDITIONAL_SECURITY_COMPARISON_FIELDS);
		if (isGroupSimilarInstruments()) {
			propertyNames.addAll(CollectionUtils.getStream(BeanUtils.getPropertyNames(InvestmentInstrument.class, true, true))
					.map("instrument."::concat)
					.collect(Collectors.toList()));
			propertyNames.addAll(ADDITIONAL_INSTRUMENT_COMPARISON_FIELDS);
		}
		propertyNames.removeIf(getExcludedFields()::contains);

		// Group securities
		Map<String, List<InvestmentSecurity>> similarSecuritiesGroupsMap = BeanUtils.getBeansMap(securityList, security -> {
			// Build key (for grouping) using all relevant property names/values
			StringJoiner keyBuilder = new StringJoiner(StringUtils.NEW_LINE);
			keyBuilder.add("[Property Values]");
			CollectionUtils.getStream(propertyNames)
					.sorted()
					.map(propertyName -> MapUtils.entry(propertyName, BeanUtils.getPropertyValue(security, propertyName, true)))
					// Shorten key-length by ignoring null properties
					.filter(propertyEntry -> propertyEntry.getValue() != null)
					.map(propertyEntry -> propertyEntry.getKey() + '=' + propertyEntry.getValue())
					.forEach(keyBuilder::add);
			keyBuilder.add("[Custom Column Values]");
			CollectionUtils.getStream(security.getColumnValueList())
					.sorted(Comparator.comparing(columnValue -> columnValue.getColumn().getName()))
					.map(columnValue -> columnValue.getColumn().getName() + '=' + columnValue.getValue())
					.forEach(keyBuilder::add);
			return keyBuilder.toString();
		});

		// Flatten map to list
		Collection<List<InvestmentSecurity>> similarSecuritiesGroups = CollectionUtils.getValues(similarSecuritiesGroupsMap);
		statusContext.addGroupsCount(CollectionUtils.getSize(similarSecuritiesGroups));
		statusContext.updateStatusMessage(false);

		return similarSecuritiesGroups;
	}


	/**
	 * Copies market data values between securities in each of the given security groups according to the configured constraints.
	 *
	 * @param statusContext           the status context object
	 * @param similarSecuritiesGroups the collection of groups of similar securities
	 */
	private List<MarketDataValue> getMarketDataValuesToSave(StatusContext statusContext, Collection<List<InvestmentSecurity>> similarSecuritiesGroups) {
		statusContext.setLastAction("Getting market data values to save");
		statusContext.updateStatusMessage(false);

		// Get non-trivial groups of securities
		Collection<List<InvestmentSecurity>> nonTrivialSimilarSecuritiesGroups = getNonTrivialSimilarSecuritiesGroups(statusContext, similarSecuritiesGroups);

		// Get all market values, grouped by security ID
		Map<Integer, List<MarketDataValue>> marketDataValues =
				getMarketDataValuesForSecurities(statusContext, CollectionUtils.combineCollectionOfCollections(nonTrivialSimilarSecuritiesGroups));

		// Determine new and changed market data values to save
		List<MarketDataValue> dataValuesToSave = new ArrayList<>();
		for (List<InvestmentSecurity> similarSecuritiesGroup : CollectionUtils.getIterable(nonTrivialSimilarSecuritiesGroups)) {
			statusContext.addCurrentGroupBeingProcessed(1);
			statusContext.updateStatusMessage(false);
			Collection<MarketDataValue> similarSecuritiesDataValues = getMarketDataValueMasterListForSecuritiesGroup(similarSecuritiesGroup, marketDataValues);
			for (InvestmentSecurity security : CollectionUtils.getIterable(similarSecuritiesGroup)) {
				for (MarketDataValue masterDataValue : CollectionUtils.getIterable(similarSecuritiesDataValues)) {
					// Guard-clause: Do not attempt to copy data to securities for non-active dates
					if (!InvestmentUtils.isSecurityActiveOn(security, masterDataValue.getMeasureDate())) {
						continue;
					}

					/*
					 * Search for the matching market data value for this security. "Matching" matching market data
					 * values have the same data field ID, data source, and measure date.
					 */
					List<MarketDataValue> securityDataValueList = marketDataValues.get(security.getIdentity());
					Optional<MarketDataValue> securityDataValueOpt = CollectionUtils.getStream(securityDataValueList)
							.filter(securityDataValue -> CompareUtils.isEqual(securityDataValue.getDataField().getIdentity(), masterDataValue.getDataField().getIdentity()))
							.filter(securityDataValue -> CompareUtils.isEqual(securityDataValue.getDataSource().getIdentity(), masterDataValue.getDataSource().getIdentity()))
							.filter(securityDataValue -> CompareUtils.isEqual(masterDataValue.getMeasureDate(), securityDataValue.getMeasureDate()))
							.findFirst();

					/*
					 * If a matching market data value for this security does not exist, this job will create one from
					 * the master market data value. Otherwise, if necessary, the matching market data value will be
					 * updated with the data from the master market data value.
					 */
					if (!securityDataValueOpt.isPresent()) {
						// No market data value exists; create one
						MarketDataValue newDataValue = BeanUtils.cloneBean(masterDataValue, false, false);
						newDataValue.setId(null);
						newDataValue.setInvestmentSecurity(security);
						dataValuesToSave.add(newDataValue);
						statusContext.addValuesCount(1);
						statusContext.addNewValuesCount(1);
					}
					else if (isOverwriteExisting()) {
						// A market data value already exists, and overwrite is enabled; overwrite it if necessary
						MarketDataValue securityDataValue = securityDataValueOpt.get();
						if (!CoreCompareUtils.isEqual(securityDataValue, masterDataValue,
								new String[]{"measureValue", "measureValueAdjustmentFactor"})) {
							securityDataValue.setMeasureValue(masterDataValue.getMeasureValue());
							securityDataValue.setMeasureValueAdjustmentFactor(masterDataValue.getMeasureValueAdjustmentFactor());
							securityDataValue.setMeasureTime(masterDataValue.getMeasureTime());
							dataValuesToSave.add(securityDataValue);
							statusContext.addValuesCount(1);
							statusContext.addOverwrittenValuesCount(1);
						}
					}
				}
				statusContext.addSecuritiesProcessedCount(1);
				statusContext.updateStatusMessage(false);
			}
		}
		return dataValuesToSave;
	}


	/**
	 * Get similar-securities groups in which the market data synchronization process is non-trivial.
	 *
	 * @param statusContext           the status context object
	 * @param similarSecuritiesGroups the collection of similar-securities groups
	 * @return a cloned collection of similar-securities groups which does not include trivial groups
	 */
	private Collection<List<InvestmentSecurity>> getNonTrivialSimilarSecuritiesGroups(StatusContext statusContext, Collection<List<InvestmentSecurity>> similarSecuritiesGroups) {
		Collection<List<InvestmentSecurity>> nonTrivialSimilarSecuritiesGroups = new ArrayList<>();
		for (List<InvestmentSecurity> similarSecuritiesGroup : CollectionUtils.getIterable(similarSecuritiesGroups)) {
			if (CollectionUtils.getSize(similarSecuritiesGroup) > 1) {
				nonTrivialSimilarSecuritiesGroups.add(similarSecuritiesGroup);
			}
			else {
				/*
				 * There are not multiple securities within the group so there are no values to synchronize. Ignore
				 * this group.
				 */
				int groupSecuritiesCount = CollectionUtils.getSize(similarSecuritiesGroup);
				statusContext.addSecuritiesProcessedCount(groupSecuritiesCount);
				statusContext.addUniqueSecurityId(CollectionUtils.getOnlyElementStrict(similarSecuritiesGroup).getId());
				statusContext.addCurrentGroupBeingProcessed(1);
				statusContext.updateStatusMessage(false);
			}
		}
		return nonTrivialSimilarSecuritiesGroups;
	}


	/**
	 * Gets a map of all market data values for the given securities, grouped by security ID. If constraints are set, then the results will be filtered
	 * accordingly.
	 *
	 * @param statusContext the status context object
	 * @param securityList  the list of securities
	 * @return a map of all market data values grouped by security ID
	 */
	private Map<Integer, List<MarketDataValue>> getMarketDataValuesForSecurities(StatusContext statusContext, List<InvestmentSecurity> securityList) {
		// Guard-clause: trivial case
		if (CollectionUtils.getSize(securityList) == 0) {
			return Collections.emptyMap();
		}

		// Create search form
		MarketDataValueSearchForm marketDataValueSearchForm = new MarketDataValueSearchForm(true);
		marketDataValueSearchForm.setInvestmentSecurityIds(BeanUtils.getBeanIdentityArray(securityList, Integer.class));
		Date endDate = new Date();
		Date startDate = DateUtils.addDays(endDate, getDaysBackIncludingToday() * -1);
		marketDataValueSearchForm.setMinMeasureDate(DateUtils.addDays(startDate, -1));
		marketDataValueSearchForm.setMaxMeasureDate(DateUtils.addDays(endDate, 1));
		marketDataValueSearchForm.setDataSourceId(getMarketDataSourceId());

		// Apply market data field group constraint
		Short[] dataFieldIds = null;
		if (getMarketDataFieldGroupId() != null) {
			List<MarketDataField> marketDataFieldListByGroup = getMarketDataFieldService().getMarketDataFieldListByGroup(getMarketDataFieldGroupId());
			dataFieldIds = BeanUtils.getBeanIdentityArray(marketDataFieldListByGroup, Short.class);
		}
		marketDataValueSearchForm.setDataFieldIds(dataFieldIds);

		// Get market data values which follow single-value-per-field/day/data source constraint
		List<MarketDataValue> marketDataValues = getMarketDataFieldService().getMarketDataValueList(marketDataValueSearchForm);
		CollectionUtils.asNonNullList(marketDataValues).removeIf(dataValue -> {
			boolean allowsMoreThanOnePerDay = !dataValue.getDataField().isUpToOnePerDay();
			if (allowsMoreThanOnePerDay) {
				statusContext.addSkippedField(dataValue.getDataField().getName());
				statusContext.updateStatusMessage(false);
				LogUtils.info(getClass(), "Skipping market data field [" + dataValue.getDataField().getName() + " (" + dataValue.getDataField().getId() + ")] for batch job. Fields can only be synchronized between securities if those fields allow only a single value per security/day/data source combination.");
			}
			return allowsMoreThanOnePerDay;
		});

		// Group market data values by security ID
		return CollectionUtils.getStream(marketDataValues)
				.collect(Collectors.groupingBy(dataValue -> dataValue.getInvestmentSecurity().getIdentity()));
	}


	/**
	 * Gets the <i>master</i> collection of market data values for the given group of <i>similar</i> investment
	 * securities.
	 * <p>
	 * The <i>master</i> collection contains all market data values for the given securities compiled into
	 * a single list. If multiple data values of the same type and source exist for the same day, the latest measured
	 * or updated data value will be used.
	 *
	 * @param similarSecuritiesGroup the group of similar securities
	 * @param marketDataValues       the map of data value lists, grouped by security ID
	 * @return the master collection of market data values for this group of similar securities
	 */
	private Collection<MarketDataValue> getMarketDataValueMasterListForSecuritiesGroup(List<InvestmentSecurity> similarSecuritiesGroup, Map<Integer, List<MarketDataValue>> marketDataValues) {
		Map<String, MarketDataValue> masterMarketDataValueMap = new HashMap<>();
		for (InvestmentSecurity security : CollectionUtils.getIterable(similarSecuritiesGroup)) {
			for (MarketDataValue dataValue : CollectionUtils.getIterable(marketDataValues.get(security.getIdentity()))) {
				// Compose key of "<date>_<data source ID>_<field ID>"
				String key = StringUtils.generateKeyWithDelimiter("_",
						dataValue.getMeasureDate(),
						dataValue.getDataSource().getIdentity(),
						dataValue.getDataField().getIdentity());

				// Determine master value to retain
				MarketDataValue oldValue = masterMarketDataValueMap.putIfAbsent(key, dataValue);
				if (oldValue != null) {
					// Retain the most recent value
					int lastUpdatedCompare = CompareUtils.compare(dataValue.getMeasureTime(), oldValue.getMeasureTime());
					if (lastUpdatedCompare == 0) {
						// The time can be null or non-existent, in which case the last-updated takes precedence
						lastUpdatedCompare = CompareUtils.compare(dataValue.getUpdateDate(), oldValue.getUpdateDate());
					}
					if (lastUpdatedCompare > 0) {
						masterMarketDataValueMap.put(key, dataValue);
					}
				}
			}
		}
		return masterMarketDataValueMap.values();
	}


	/**
	 * Saves the given market data values.
	 *
	 * @param statusContext    the status context object
	 * @param dataValuesToSave the market data values to save
	 */
	private void saveMarketDataValues(StatusContext statusContext, List<MarketDataValue> dataValuesToSave) {
		statusContext.setLastAction("Saving market data values");
		statusContext.updateStatusMessage(false);
		for (MarketDataValue dataValue : CollectionUtils.getIterable(dataValuesToSave)) {
			try {
				// Save market data value
				getMarketDataFieldService().saveMarketDataValue(dataValue);
				statusContext.addValuesSucceededCount(1);
			}
			catch (Exception e) {
				// Track exceptions
				statusContext.addValuesFailedCount(1);
				String message = "Error saving data value [" + dataValue.getLabel() + "]: " + e.getMessage();
				statusContext.addError(message);
				LogUtils.errorOrInfo(getClass(), message, e);
			}
			finally {
				statusContext.updateStatusMessage(false);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the collection of fields to exclude when determining whether securities are similar.
	 *
	 * @return the collection of fields to exclude
	 */
	private Collection<String> getExcludedFields() {
		Set<String> fieldsToExclude = new HashSet<>();
		fieldsToExclude.addAll(ALWAYS_EXCLUDE_FROM_COMPARISON_FIELDS);
		fieldsToExclude.addAll(ObjectUtils.coalesce(getExcludeFromComparisonFields(), DEFAULT_EXCLUDE_FROM_COMPARISON_FIELDS));
		if (isGroupSimilarInstruments()) {
			fieldsToExclude.add("instrument.id");
		}
		return fieldsToExclude;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Data Structures                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A class to store the status context for the current job.
	 */
	private static class StatusContext {

		private final StatusHolderObject<Status> statusHolderObject;

		private String lastAction = "Starting";
		private final List<Integer> uniqueSecurityIdList = new ArrayList<>();
		private final Set<String> skippedFields = new LinkedHashSet<>();
		private final List<String> errors = new ArrayList<>();

		private int hierarchiesCount;
		private int currentHierarchyBeingProcessed;

		private int groupsCount;
		private int currentGroupBeingProcessed;

		private int securitiesCount;
		private int securitiesProcessedCount;

		private int valuesCount;
		private int valuesSucceededCount;
		private int valuesFailedCount;
		private int newValuesCount;
		private int overwrittenValuesCount;


		public StatusContext(StatusHolderObject<Status> statusHolderObject) {
			this.statusHolderObject = statusHolderObject;
		}


		/**
		 * Updates the {@link StatusHolderObject} message using the current status information.
		 *
		 * @param includeErrors if {@code true}, include any existing error messages in the status message
		 */
		public void updateStatusMessage(boolean includeErrors) {
			String statusString = String.format("%s." +
							" Processed [%d] of [%d] securities ([%d] have no similar securities) in [%d] of [%d] groups and [%d] of [%d] hierarchies." +
							" Found [%d] values to create and [%d] values to update." +
							" Succeeded processing [%d] of [%d] market data value changes.",
					getLastAction(),
					getSecuritiesProcessedCount(), getSecuritiesCount(), getUniqueSecurityIdList().size(),
					getCurrentGroupBeingProcessed(), getGroupsCount(),
					getCurrentHierarchyBeingProcessed(), getHierarchiesCount(),
					getNewValuesCount(), getOverwrittenValuesCount(),
					getValuesSucceededCount(), getValuesCount());

			String uniqueSecurityIdListString = StringUtils.EMPTY_STRING;
			if (!CollectionUtils.isEmpty(getUniqueSecurityIdList())) {
				uniqueSecurityIdListString = String.format("%nIDs for securities with no similar securities: %s", StringUtils.join(getUniqueSecurityIdList(), String::valueOf, ", "));
			}

			String skippedFieldsString = StringUtils.EMPTY_STRING;
			if (!CollectionUtils.isEmpty(getSkippedFields())) {
				skippedFieldsString = String.format("%nSkipped fields: %s", StringUtils.join(getSkippedFields(), ", "));
			}

			String errorString = StringUtils.EMPTY_STRING;
			if (!includeErrors) {
				errorString = String.format("%nFailed [%d].", getValuesFailedCount());
			}
			else if (getValuesFailedCount() > 0) {
				// Can be expensive; only attach errors if parameter given
				errorString = String.format("%nFailed [%d]:%n%s", getValuesFailedCount(), StringUtils.join(getErrors(), StringUtils.NEW_LINE));
			}

			this.statusHolderObject.getStatus().setMessage(statusString + uniqueSecurityIdListString + skippedFieldsString + errorString);
		}


		////////////////////////////////////////////////////////////////////////////
		////////            Getter and Setter Methods                       ////////
		////////////////////////////////////////////////////////////////////////////


		public String getLastAction() {
			return this.lastAction;
		}


		public void setLastAction(String lastAction) {
			this.lastAction = lastAction;
		}


		public List<String> getErrors() {
			return this.errors;
		}


		public boolean addError(String error) {
			return this.errors.add(error);
		}


		public Set<String> getSkippedFields() {
			return this.skippedFields;
		}


		public boolean addSkippedField(String skippedField) {
			return this.skippedFields.add(skippedField);
		}


		public int getHierarchiesCount() {
			return this.hierarchiesCount;
		}


		public void addHierarchiesCount(int num) {
			this.hierarchiesCount += num;
		}


		public int getCurrentHierarchyBeingProcessed() {
			return this.currentHierarchyBeingProcessed;
		}


		public void addCurrentHierarchyBeingProcessed(int num) {
			this.currentHierarchyBeingProcessed += num;
		}


		public int getGroupsCount() {
			return this.groupsCount;
		}


		public void addGroupsCount(int num) {
			this.groupsCount += num;
		}


		public int getCurrentGroupBeingProcessed() {
			return this.currentGroupBeingProcessed;
		}


		public void addCurrentGroupBeingProcessed(int num) {
			this.currentGroupBeingProcessed += num;
		}


		public int getSecuritiesCount() {
			return this.securitiesCount;
		}


		public void addSecuritiesCount(int num) {
			this.securitiesCount += num;
		}


		public List<Integer> getUniqueSecurityIdList() {
			return this.uniqueSecurityIdList;
		}


		public void addUniqueSecurityId(int id) {
			this.uniqueSecurityIdList.add(id);
		}


		public int getSecuritiesProcessedCount() {
			return this.securitiesProcessedCount;
		}


		public void addSecuritiesProcessedCount(int num) {
			this.securitiesProcessedCount += num;
		}


		public int getValuesCount() {
			return this.valuesCount;
		}


		public void addValuesCount(int num) {
			this.valuesCount += num;
		}


		public int getValuesSucceededCount() {
			return this.valuesSucceededCount;
		}


		public void addValuesSucceededCount(int num) {
			this.valuesSucceededCount += num;
		}


		public int getValuesFailedCount() {
			return this.valuesFailedCount;
		}


		public void addValuesFailedCount(int num) {
			this.valuesFailedCount += num;
		}


		public int getNewValuesCount() {
			return this.newValuesCount;
		}


		public void addNewValuesCount(int num) {
			this.newValuesCount += num;
		}


		public int getOverwrittenValuesCount() {
			return this.overwrittenValuesCount;
		}


		public void addOverwrittenValuesCount(int num) {
			this.overwrittenValuesCount += num;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataInvestmentSecurityService getMarketDataInvestmentSecurityService() {
		return this.marketDataInvestmentSecurityService;
	}


	public void setMarketDataInvestmentSecurityService(MarketDataInvestmentSecurityService marketDataInvestmentSecurityService) {
		this.marketDataInvestmentSecurityService = marketDataInvestmentSecurityService;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentInstrumentHierarchyId() {
		return this.investmentInstrumentHierarchyId;
	}


	public void setInvestmentInstrumentHierarchyId(Short investmentInstrumentHierarchyId) {
		this.investmentInstrumentHierarchyId = investmentInstrumentHierarchyId;
	}


	public Short getInvestmentInstrumentGroupId() {
		return this.investmentInstrumentGroupId;
	}


	public void setInvestmentInstrumentGroupId(Short investmentInstrumentGroupId) {
		this.investmentInstrumentGroupId = investmentInstrumentGroupId;
	}


	public Short getDaysBackIncludingToday() {
		return this.daysBackIncludingToday;
	}


	public void setDaysBackIncludingToday(Short daysBackIncludingToday) {
		this.daysBackIncludingToday = daysBackIncludingToday;
	}


	public boolean isOverwriteExisting() {
		return this.overwriteExisting;
	}


	public void setOverwriteExisting(boolean overwriteExisting) {
		this.overwriteExisting = overwriteExisting;
	}


	public boolean isGroupSimilarInstruments() {
		return this.groupSimilarInstruments;
	}


	public void setGroupSimilarInstruments(boolean groupSimilarInstruments) {
		this.groupSimilarInstruments = groupSimilarInstruments;
	}


	public boolean isIncludeNonOtcSecurities() {
		return this.includeNonOtcSecurities;
	}


	public void setIncludeNonOtcSecurities(boolean includeNonOtcSecurities) {
		this.includeNonOtcSecurities = includeNonOtcSecurities;
	}


	public Short getMarketDataFieldGroupId() {
		return this.marketDataFieldGroupId;
	}


	public void setMarketDataFieldGroupId(Short marketDataFieldGroupId) {
		this.marketDataFieldGroupId = marketDataFieldGroupId;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public List<String> getExcludeFromComparisonFields() {
		return this.excludeFromComparisonFields;
	}


	public void setExcludeFromComparisonFields(List<String> excludeFromComparisonFields) {
		this.excludeFromComparisonFields = excludeFromComparisonFields;
	}
}
