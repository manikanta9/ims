package com.clifton.marketdata.updater.jobs;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyService;
import com.clifton.business.shared.Company;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.SimpleCacheHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.validation.ValidationAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocation;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationService;
import com.clifton.investment.instrument.calculator.InvestmentCalculatorService;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.api.pricing.MarketDataSecurityDataValue;
import com.clifton.marketdata.api.pricing.MarketDataSecurityDataValueApiService;
import com.clifton.marketdata.api.pricing.MarketDataSecurityDataValueCommand;
import com.clifton.marketdata.api.rates.FxRateLookupCache;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A Batch Job for pricing investment security baskets and saving this price to the basket security's market data.
 *
 * @author davidi
 */
public class MarketDataValueBenchmarkPricingJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	private final static String CONTEXT_KEY_BALANCE_DATE = "BalanceDate";
	private final static String CONTEXT_KEY_FX_SOURCE_COMPANY = "FxSourceCompany";
	private final static String CONTEXT_KEY_FROM_MARKET_DATA_FIELD = "FromMarketDataField";
	private final static String CONTEXT_KEY_TO_MARKET_DATA_FIELD = "ToMarketDataField";
	private final static String CONTEXT_KEY_MARKET_DATA_SOURCE = "MarketDataSource";
	private final static String CONTEXT_KEY_MARKET_DATA_SOURCE_FOR_SAVE = "MarketDataSourceForSave";
	private final static String CONTEXT_KEY_PRICE_CACHE = "PriceCache";
	private final static String CONTEXT_KEY_FX_CACHE = "FxCache";


	/**
	 * Date for which to attain prices.
	 * An InvestmentCalendar associated with the security being priced may influence the date for which prices are obtained.
	 */
	private Date balanceDate;

	/**
	 * The investment hierarchy for the investment securities being priced.
	 */
	private Short hierarchyId;

	/**
	 * The MarketDataSource instance needed for price lookups from a service or historical data.  If not specified, the data provider service will determine the MarketDataSource to use.
	 */
	private Short marketDataSourceId;

	/**
	 * The MarketDataSource ID to use when creating and saving the MarketDataValue with the calculated price.  Normally, this would be a MarketDataSource that indicates a calculated value, such as: Parametric Clifton.
	 * This field is required (for saving data) and is preset to the Parametric Clifton datasource ID.
	 */
	private Short marketDataSourceIdForSave;

	/**
	 * The ID of the MarketDataField used for price lookups from local market data.
	 */
	private Short fromMarketDataFieldId;

	/**
	 * The ID of the MarketDataField used for saving the retrieved prices.
	 */
	private Short toMarketDataFieldId;

	/**
	 * If set, the pricing will apply only to the security with the specified ID. This field is optional.
	 */
	private Integer investmentSecurityId;

	/**
	 * If set, the search parameters for securities to price, will include the instrument ID. This field is optional.
	 */
	private Integer investmentInstrumentId;

	/**
	 * The investment type ID of the security being priced.  This field is only required if there is no Investment Instrument Hierarchy specified.
	 */
	private Short investmentTypeId;

	/**
	 * The investment sub type ID.  This field is only required if there is no Investment Instrument Hierarchy specified.
	 */
	private Short investmentSubTypeId;

	/**
	 * The investment sub type 2 ID.  This field is optional, but can be used for additional specificity when looking up securities to be priced.
	 */
	private Short investmentSubType2Id;

	/**
	 * FX Source Company to use when calculating exchange rates.
	 */
	private Integer fxSourceCompanyId;

	/**
	 * If true, updates existing values when saving. If false, an existing value is not updated.
	 */
	private boolean updateExistingValues;

	/**
	 * If true, will use flexible lookups for FxRate.
	 */
	private boolean flexibleFxRate;

	/**
	 * If true, will use flexible price for local price lookups from market data.
	 */
	private boolean flexiblePrice;

	/**
	 * An optional property used when looking up securities.  If true, returns only securities that are active on the balance date.
	 */
	private boolean activeSecuritiesOnly;

	/**
	 * Optional property when using flex pricing, the maximum number of days to go back when looking up a price from market data.
	 */
	private Integer maximumDaysBackForFlexPriceLookups;

	private BusinessCompanyService businessCompanyService;
	private InvestmentCalculatorService investmentCalculatorService;
	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentSecurityAllocationService investmentSecurityAllocationService;
	private MarketDataService marketDataService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;
	private MarketDataSecurityDataValueApiService marketDataSecurityDataValueApiService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataSourceService marketDataSourceService;
	private StatusHolderObject<Status> statusHolder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		setContextValues(context);

		for (InvestmentSecurity swapBasketSecurity : CollectionUtils.getIterable(getBasketSwapSecurities(context))) {
			try {
				MarketDataValue basketPrice = getBasketPrice(swapBasketSecurity, context);
				if (basketPrice != null) {
					getMarketDataFieldService().saveMarketDataValueWithOptions(basketPrice, isUpdateExistingValues());
				}
			}
			catch (Exception exc) {
				getTaskStatus().addError(exc.getMessage());
			}
		}

		return getTaskStatus();
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject){
		this.statusHolder = statusHolderObject;
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getHierarchyId(), "An Instrument Hierarchy must be selected.");
		ValidationUtils.assertNotNull(getMarketDataSourceIdForSave(), "The \"Save Market Data Source\" field requires a value.");
		ValidationUtils.assertNotNull(getToMarketDataFieldId(), "The \"To Market Data Field\" field requires a value.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Set up key / values in the context for shared use amongst methods of this class.
	 */
	private void setContextValues(Map<String, Object> context) {
		context.put(CONTEXT_KEY_FX_CACHE, new FxRateLookupCache(getMarketDataExchangeRatesApiService()));
		context.put(CONTEXT_KEY_PRICE_CACHE, new SimpleCacheHandler<String, BigDecimal>());
		context.put(CONTEXT_KEY_BALANCE_DATE, ObjectUtils.coalesce(getBalanceDate(), new Date()));

		if (getFxSourceCompanyId() != null) {
			context.put(CONTEXT_KEY_FX_SOURCE_COMPANY, getBusinessCompanyService().getBusinessCompany(getFxSourceCompanyId()));
		}

		if (getMarketDataSourceId() != null) {
			context.put(CONTEXT_KEY_MARKET_DATA_SOURCE, getMarketDataSourceService().getMarketDataSource(getMarketDataSourceId()));
		}

		if (getFromMarketDataFieldId() != null) {
			context.put(CONTEXT_KEY_FROM_MARKET_DATA_FIELD, getMarketDataFieldService().getMarketDataField(getFromMarketDataFieldId()));
		}

		context.put(CONTEXT_KEY_MARKET_DATA_SOURCE_FOR_SAVE, getMarketDataSourceService().getMarketDataSource(getMarketDataSourceIdForSave()));
		context.put(CONTEXT_KEY_TO_MARKET_DATA_FIELD, getMarketDataFieldService().getMarketDataField(getToMarketDataFieldId()));
	}


	/**
	 * Returns a list of swap basket securities that will be priced.
	 */
	private List<InvestmentSecurity> getBasketSwapSecurities(Map<String, Object> context) {
		if (getInvestmentSecurityId() != null) {
			return CollectionUtils.createList(getInvestmentInstrumentService().getInvestmentSecurity(getInvestmentSecurityId()));
		}

		SecuritySearchForm searchForm = new SecuritySearchForm();
		if (isActiveSecuritiesOnly()) {
			searchForm.setActiveOnDate(getBalanceDate(context));
		}
		searchForm.setHierarchyId(getHierarchyId());
		searchForm.setInvestmentTypeId(getInvestmentTypeId());
		searchForm.setInvestmentTypeSubTypeId(getInvestmentSubTypeId());
		searchForm.setInvestmentTypeSubType2Id(getInvestmentSubType2Id());
		searchForm.setInstrumentId(getInvestmentInstrumentId());

		return getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
	}


	/**
	 * Calculates the price of the basket security by summing up calculated prices of all constituents and dividing the sum total by an index divisor.
	 *
	 * @param security InvestmentSecurity for the basket being priced.
	 * @return MarketDataValue containing the price.
	 */
	private MarketDataValue getBasketPrice(InvestmentSecurity security, Map<String, Object> context) {
		BigDecimal basketPrice = null;
		MarketDataValue marketDataValue = new MarketDataValue();
		Company fxSource = getFxSourceCompany(context) != null ? getFxSourceCompany(context).toCompany() : null;

		try {
			if (security != null) {
				Date balanceDt = getBalanceDate(context);
				BigDecimal indexDivisor = getInvestmentCalculatorService().getInvestmentSecurityNotionalMultiplier(security.getId(), balanceDt, balanceDt, false);

				SecurityAllocationSearchForm securityAllocationSearchForm = new SecurityAllocationSearchForm();
				securityAllocationSearchForm.setParentInvestmentSecurityId(security.getId());
				securityAllocationSearchForm.setActiveOnDate(getBalanceDate(context));

				// Temporary map with a key of securitySymbol and allocationExchange, and value of the price of the allocation.
				Map<String, BigDecimal> allocationPriceMap = new HashMap<>();
				BigDecimal priceSum = BigDecimal.ZERO;
				List<InvestmentSecurityAllocation> investmentSecurityAllocationList = getInvestmentSecurityAllocationService().getInvestmentSecurityAllocationList(securityAllocationSearchForm);
				if (CollectionUtils.isEmpty(investmentSecurityAllocationList)) {
					getTaskStatus().addError("No security allocations found for security: " + security.getLabel() + ".");
				}
				else {
					for (InvestmentSecurityAllocation allocation : CollectionUtils.getIterable(investmentSecurityAllocationList)) {
						InvestmentSecurity constituentSecurity = allocation.getInvestmentSecurity();
						BigDecimal marketPrice = getMarketDataPriceForSecurity(constituentSecurity, allocation.getOverrideExchange(), context);
						BigDecimal intermediatePrice = MathUtils.multiply(marketPrice, constituentSecurity.getPriceMultiplier());

						BigDecimal fxRate = getFxRate(fxSource, constituentSecurity, security, context);
						ValidationUtils.assertNotNull(fxRate, "Cannot determine FX Rate for security \"" + constituentSecurity.getLabel() + "\" and \"" + security.getLabel() + "\".");
						intermediatePrice = MathUtils.multiply(intermediatePrice, fxRate);
						intermediatePrice = MathUtils.multiply(intermediatePrice, allocation.getAllocationShares());

						allocationPriceMap.put(constituentSecurity.getSymbol() + "_" + allocation.getExchange().getExchangeCode(), intermediatePrice);
						priceSum = MathUtils.add(priceSum, intermediatePrice);
					}
					basketPrice = MathUtils.divide(priceSum, indexDivisor);
					getTaskStatus().addMessage("Priced benchmark " + security.getSymbol() + " with allocation prices: " + allocationPriceMap);
				}
			}
		}
		catch (Exception exc) {
			getTaskStatus().addError("Error while deriving constituent prices: " + exc.getMessage());
			return null;
		}

		if (basketPrice == null) {
			// This likely won't occur, but serves as final backstop check for a null price. Normally if price, or fxRate lookups fail, exceptions are thrown and handled.
			// Returning a null price is OK, as the calling routine will skip further processing for the affected security.
			getTaskStatus().addError("Price derivation failed for security: " + (security != null ? security.getLabel() : "undefined"));
			return null;
		}

		// Create a market data value for the price and return it to the calling routine so it can be saved.
		marketDataValue.setDataSource(getMarketDataSourceForSave(context));
		marketDataValue.setInvestmentSecurity(security);
		marketDataValue.setDataField(getToMarketDataField(context));
		basketPrice = MathUtils.round(basketPrice, 15);
		marketDataValue.setMeasureValue(basketPrice);
		marketDataValue.setMeasureDate(getBalanceDate(context));
		marketDataValue.setMeasureTime(new Time(0));

		return marketDataValue;
	}


	/**
	 * Calculates and returns the FX Rate between a constituent security and the basket security.
	 * Will throw a validation exception if the trading currencies associated with the constituent or basket security are not defined.
	 */
	BigDecimal getFxRate(Company fxSourceCompany, InvestmentSecurity constituentSecurity, InvestmentSecurity basketSecurity, Map<String, Object> context) {
		InvestmentSecurity constituentSecurityCurrency = constituentSecurity.getInstrument().getTradingCurrency();
		InvestmentSecurity basketSecurityCurrency = basketSecurity.getInstrument().getTradingCurrency();
		ValidationUtils.assertNotNull(constituentSecurityCurrency, "Cannot derive FX Rate for price calculation. Constituent security \"" + constituentSecurity.getLabel() + "\" has no trading currency defined.");
		ValidationUtils.assertNotNull(basketSecurityCurrency, "Cannot derive FX Rate for price calculation. Basket security \"" + basketSecurity.getLabel() + "\" has no trading currency defined.");
		return getFxRateLookupCache(context).getExchangeRate(fxSourceCompany, false, constituentSecurityCurrency.getSymbol(), basketSecurityCurrency.getSymbol(), getBalanceDate(context), isFlexibleFxRate());
	}


	/**
	 * Gets the price of the security from the MarketDataSecurityDataValueApi service for the constituent security passed in.
	 * The local priceCache is used for efficiency, to prevent repeated lookups of
	 *
	 * @return the price of the security as a BigDecimal or null if no price is found.
	 */
	private BigDecimal getMarketDataPriceForSecurity(InvestmentSecurity investmentSecurity, InvestmentExchange overrideExchange, Map<String, Object> context) {
		final String cacheKey = getCacheKey(investmentSecurity, overrideExchange, getBalanceDate(context));
		BigDecimal securityPrice = getPriceCache(context).get(getCacheName(), cacheKey);
		if (securityPrice != null) {
			return securityPrice;
		}

		if (investmentSecurity.isCurrency()) {
			securityPrice = BigDecimal.ONE;
		}
		else {
			MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withSecurityId(investmentSecurity.getId()).withRequestedDate(getBalanceDate(context)).withExceptionOnMissingData(true);
			if (overrideExchange != null && !investmentSecurity.getInstrument().getExchange().equals(overrideExchange)) {
				command.setExchangeName(overrideExchange.getName());
			}
			if (isFlexiblePrice()) {
				command.setFlexible(true);
				if (getMaximumDaysBackForFlexPriceLookups() != null) {
					command.setFlexibleMaxDaysBackAllowed(getMaximumDaysBackForFlexPriceLookups());
				}
			}
			if (getMarketDataSource(context) != null) {
				command.setMarketDataSourceName(getMarketDataSource(context).getName());
			}

			if (getFromMarketDataField(context) != null) {
				command.setMarketDataFieldName(getFromMarketDataField(context).getName());
			}

			MarketDataSecurityDataValue priceContainer = getMarketDataSecurityDataValueApiService().getMarketDataSecurityDataValue(command);
			if (priceContainer != null && priceContainer.getFieldValue() != null) {
				securityPrice = (BigDecimal) priceContainer.getFieldValue();
			}
		}
		if (securityPrice != null) {
			getPriceCache(context).put(getCacheName(), cacheKey, securityPrice);
		}
		return securityPrice;
	}


	private String getCacheKey(InvestmentSecurity investmentSecurity, InvestmentExchange overrideExchange, Date date) {
		return investmentSecurity.getId() + "_" + (overrideExchange != null ? overrideExchange.getId() : "") + "_" + date;
	}


	private String getCacheName() {
		return this.getClass().getName();
	}


	private FxRateLookupCache getFxRateLookupCache(Map<String, Object> context) {
		return (FxRateLookupCache) context.get(CONTEXT_KEY_FX_CACHE);
	}


	@SuppressWarnings({"cast", "unchecked"})
	private CacheHandler<String, BigDecimal> getPriceCache(Map<String, Object> context) {
		return (CacheHandler<String, BigDecimal>) context.get(CONTEXT_KEY_PRICE_CACHE);
	}


	private Date getBalanceDate(Map<String, Object> context) {
		return (Date) context.get(CONTEXT_KEY_BALANCE_DATE);
	}


	private BusinessCompany getFxSourceCompany(Map<String, Object> context) {
		return (BusinessCompany) context.get(CONTEXT_KEY_FX_SOURCE_COMPANY);
	}


	private MarketDataField getFromMarketDataField(Map<String, Object> context) {
		return (MarketDataField) context.get(CONTEXT_KEY_FROM_MARKET_DATA_FIELD);
	}


	private MarketDataField getToMarketDataField(Map<String, Object> context) {
		return (MarketDataField) context.get(CONTEXT_KEY_TO_MARKET_DATA_FIELD);
	}


	private MarketDataSource getMarketDataSourceForSave(Map<String, Object> context) {
		return (MarketDataSource) context.get(CONTEXT_KEY_MARKET_DATA_SOURCE_FOR_SAVE);
	}


	private MarketDataSource getMarketDataSource(Map<String, Object> context) {
		return (MarketDataSource) context.get(CONTEXT_KEY_MARKET_DATA_SOURCE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Status getTaskStatus() {
		return this.statusHolder.getStatus();
	}


	public Date getBalanceDate() {
		return this.balanceDate;
	}


	public void setBalanceDate(Date balanceDate) {
		this.balanceDate = balanceDate;
	}


	public Short getHierarchyId() {
		return this.hierarchyId;
	}


	public void setHierarchyId(Short hierarchyId) {
		this.hierarchyId = hierarchyId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentSubTypeId() {
		return this.investmentSubTypeId;
	}


	public void setInvestmentSubTypeId(Short investmentSubTypeId) {
		this.investmentSubTypeId = investmentSubTypeId;
	}


	public Short getInvestmentSubType2Id() {
		return this.investmentSubType2Id;
	}


	public void setInvestmentSubType2Id(Short investmentSubType2Id) {
		this.investmentSubType2Id = investmentSubType2Id;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Short getFromMarketDataFieldId() {
		return this.fromMarketDataFieldId;
	}


	public void setFromMarketDataFieldId(Short fromMarketDataFieldId) {
		this.fromMarketDataFieldId = fromMarketDataFieldId;
	}


	public Short getToMarketDataFieldId() {
		return this.toMarketDataFieldId;
	}


	public void setToMarketDataFieldId(Short toMarketDataFieldId) {
		this.toMarketDataFieldId = toMarketDataFieldId;
	}


	public Short getMarketDataSourceIdForSave() {
		return this.marketDataSourceIdForSave;
	}


	public void setMarketDataSourceIdForSave(Short marketDataSourceIdForSave) {
		this.marketDataSourceIdForSave = marketDataSourceIdForSave;
	}


	public Integer getFxSourceCompanyId() {
		return this.fxSourceCompanyId;
	}


	public void setFxSourceCompanyId(Integer fxSourceCompanyId) {
		this.fxSourceCompanyId = fxSourceCompanyId;
	}


	public boolean isFlexibleFxRate() {
		return this.flexibleFxRate;
	}


	public void setFlexibleFxRate(boolean flexibleFxRate) {
		this.flexibleFxRate = flexibleFxRate;
	}


	public boolean isFlexiblePrice() {
		return this.flexiblePrice;
	}


	public void setFlexiblePrice(boolean flexiblePrice) {
		this.flexiblePrice = flexiblePrice;
	}


	public boolean isUpdateExistingValues() {
		return this.updateExistingValues;
	}


	public void setUpdateExistingValues(boolean updateExistingValues) {
		this.updateExistingValues = updateExistingValues;
	}


	public boolean isActiveSecuritiesOnly() {
		return this.activeSecuritiesOnly;
	}


	public void setActiveSecuritiesOnly(boolean activeSecuritiesOnly) {
		this.activeSecuritiesOnly = activeSecuritiesOnly;
	}


	public Integer getMaximumDaysBackForFlexPriceLookups() {
		return this.maximumDaysBackForFlexPriceLookups;
	}


	public void setMaximumDaysBackForFlexPriceLookups(Integer maximumDaysBackForFlexPriceLookups) {
		this.maximumDaysBackForFlexPriceLookups = maximumDaysBackForFlexPriceLookups;
	}


	public BusinessCompanyService getBusinessCompanyService() {
		return this.businessCompanyService;
	}


	public void setBusinessCompanyService(BusinessCompanyService businessCompanyService) {
		this.businessCompanyService = businessCompanyService;
	}


	public InvestmentCalculatorService getInvestmentCalculatorService() {
		return this.investmentCalculatorService;
	}


	public void setInvestmentCalculatorService(InvestmentCalculatorService investmentCalculatorService) {
		this.investmentCalculatorService = investmentCalculatorService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentSecurityAllocationService getInvestmentSecurityAllocationService() {
		return this.investmentSecurityAllocationService;
	}


	public void setInvestmentSecurityAllocationService(InvestmentSecurityAllocationService investmentSecurityAllocationService) {
		this.investmentSecurityAllocationService = investmentSecurityAllocationService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public MarketDataService getMarketDataService() {
		return this.marketDataService;
	}


	public void setMarketDataService(MarketDataService marketDataService) {
		this.marketDataService = marketDataService;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}


	public MarketDataSecurityDataValueApiService getMarketDataSecurityDataValueApiService() {
		return this.marketDataSecurityDataValueApiService;
	}


	public void setMarketDataSecurityDataValueApiService(MarketDataSecurityDataValueApiService marketDataSecurityDataValueApiService) {
		this.marketDataSecurityDataValueApiService = marketDataSecurityDataValueApiService;
	}
}
