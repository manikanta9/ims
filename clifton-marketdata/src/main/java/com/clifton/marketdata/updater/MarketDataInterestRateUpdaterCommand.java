package com.clifton.marketdata.updater;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.util.Date;


/**
 * <code>MarketDataInterestRateUpdaterCommand</code> batch job constructed object used to request an update of Investment Interest Rate Index values.
 */
public class MarketDataInterestRateUpdaterCommand implements ValidationAware {

	/**
	 * If null, the default data provider will be used.
	 */
	private final String dataSourceName;

	private final Short interestRateIndexGroupId;

	private final Date startDate;
	private final Date endDate;

	private final boolean skipIfAlreadyExists;

	private final Status status = new Status();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRateUpdaterCommand(String dataSourceName, Short interestRateIndexGroupId, Date startDate, Date endDate, boolean skipIfAlreadyExists) {
		this.dataSourceName = dataSourceName;
		this.interestRateIndexGroupId = interestRateIndexGroupId;
		this.startDate = startDate;
		this.endDate = endDate;
		this.skipIfAlreadyExists = skipIfAlreadyExists;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getInterestRateIndexGroupId(), "Interest Rate Index Group Id command restriction is required.");
		ValidationUtils.assertNotNull(getStartDate(), "The Start Date command restriction is required.");
		ValidationUtils.assertNotNull(getEndDate(), "The End Date command restriction is required.");
		ValidationUtils.assertTrue(DateUtils.isDateAfterOrEqual(getStartDate(), getEndDate()), "The End Date must be after or equal to the Start Date.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public Short getInterestRateIndexGroupId() {
		return this.interestRateIndexGroupId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public boolean isSkipIfAlreadyExists() {
		return this.skipIfAlreadyExists;
	}


	public Status getStatus() {
		return this.status;
	}
}
