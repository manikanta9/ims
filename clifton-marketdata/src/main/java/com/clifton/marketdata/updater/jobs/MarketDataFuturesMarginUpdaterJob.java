package com.clifton.marketdata.updater.jobs;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.InstrumentSearchForm;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>MarketDataFuturesMarginUpdaterJob</code> class updates investment instrument margin fields from default market data provider.
 * If investmentGroupId is not specified, defaults to Futures investment type.
 * Only updates active instruments.
 *
 * @author akorver
 */
public class MarketDataFuturesMarginUpdaterJob implements Task, StatusHolderObjectAware<Status> {

	private static final String MAINTENANCE_HEDGER_MARGIN = "FUT_SEC_HEDGE_ML";
	private static final String MAINTENANCE_SPECULATOR_MARGIN = "FUT_SEC_SPEC_ML";
	private static final String INITIAL_SPECULATOR_MARGIN = "FUT_INIT_SPEC_ML";
	private static final String INITIAL_HEDGER_MARGIN = "FUT_INIT_HEDGE_ML";

	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////

	private MarketDataFieldService marketDataFieldService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private InvestmentInstrumentService investmentInstrumentService;

	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private StatusHolderObject<Status> statusHolder;

	private short investmentGroupId = -1;


	/////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		InstrumentSearchForm searchForm = new InstrumentSearchForm();
		searchForm.setInactive(false);
		if (getInvestmentGroupId() == -1) {
			searchForm.setInvestmentType(InvestmentType.FUTURES); // default to futures
		}
		else {
			searchForm.setInvestmentGroupId(getInvestmentGroupId());
		}
		List<InvestmentInstrument> activeFutureInstruments = getInvestmentInstrumentService().getInvestmentInstrumentList(searchForm);

		Date today = new Date();
		int recordsUpdated = 0, recordsLookedUp = 0, errorCount = 0;
		MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(null); // default market data provider

		MarketDataField hedgerInitial = getMarketDataFieldByExternalName(INITIAL_HEDGER_MARGIN);
		MarketDataField hedgerMaintenance = getMarketDataFieldByExternalName(MAINTENANCE_HEDGER_MARGIN);
		MarketDataField speculatorInitial = getMarketDataFieldByExternalName(INITIAL_SPECULATOR_MARGIN);
		MarketDataField speculatorMaintenance = getMarketDataFieldByExternalName(MAINTENANCE_SPECULATOR_MARGIN);

		Status status = this.statusHolder.getStatus();
		for (InvestmentInstrument instrument : CollectionUtils.getIterable(activeFutureInstruments)) {
			InvestmentSecurity activeSecurity = getInvestmentSecurityGroupService().getInvestmentSecurityCurrentByInstrument(instrument.getId(), today, false);
			if (activeSecurity == null) {
				continue;
			}
			// update all 4 fields if necessary
			List<String> errors = new ArrayList<>();
			DataFieldValueCommand command = DataFieldValueCommand.newBuilder()
					.addSecurity(activeSecurity, hedgerInitial, hedgerMaintenance, speculatorInitial, speculatorMaintenance)
					.setErrors(errors)
					.build();
			Map<MarketDataField, MarketDataValue> dataValuesMap = dataProvider.getMarketDataValueLatest(command)
					.getMarketDataValueMapForSecurity(activeSecurity);
			boolean updated = updateValueIfChanged(instrument, "hedgerInitialMarginPerUnit", dataValuesMap.get(hedgerInitial));
			updated = updateValueIfChanged(instrument, "hedgerSecondaryMarginPerUnit", dataValuesMap.get(hedgerMaintenance)) || updated;
			updated = updateValueIfChanged(instrument, "speculatorInitialMarginPerUnit", dataValuesMap.get(speculatorInitial)) || updated;
			updated = updateValueIfChanged(instrument, "speculatorSecondaryMarginPerUnit", dataValuesMap.get(speculatorMaintenance)) || updated;
			if (updated) {
				getInvestmentInstrumentService().saveInvestmentInstrument(instrument);
				recordsUpdated++;
			}
			recordsLookedUp++;
			errorCount += errors.size();
			status.setMessage("Total: " + activeFutureInstruments.size() + "; Updated " + recordsUpdated + "; Looked up " + recordsLookedUp + "; Errors: " + errorCount + "; Last Instrument: " + instrument);
			status.addErrors(errors);
		}

		return status;
	}


	private boolean updateValueIfChanged(InvestmentInstrument instrument, String propertyName, MarketDataValue marketDataValue) {
		if (marketDataValue == null) {
			return false; // don't update field
		}
		BigDecimal currentValue = BeanUtils.getFieldValue(instrument, propertyName);
		BigDecimal newValue = marketDataValue.getMeasureValue();

		if (MathUtils.isEqual(currentValue, newValue)) {
			return false; // don't update if value is the same
		}
		BeanUtils.setPropertyValue(instrument, propertyName, newValue);
		return true;
	}


	private MarketDataField getMarketDataFieldByExternalName(String externalFieldName) {
		MarketDataFieldSearchForm searchForm = new MarketDataFieldSearchForm();
		searchForm.setExternalFieldName(externalFieldName);
		List<MarketDataField> fieldList = getMarketDataFieldService().getMarketDataFieldList(searchForm);
		if (CollectionUtils.isEmpty(fieldList)) {
			throw new ValidationException("Cannot find Market Data Field by external name: " + externalFieldName);
		}
		if (CollectionUtils.getSize(fieldList) > 1) {
			throw new ValidationException("Cannot have more than one Market Data Field for external name: " + externalFieldName);
		}
		return fieldList.get(0);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}
}
