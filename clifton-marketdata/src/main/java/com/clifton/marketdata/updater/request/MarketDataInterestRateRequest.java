package com.clifton.marketdata.updater.request;

import com.clifton.core.util.AssertUtils;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;


/**
 * The <code>MarketDataInterestRateRequest</code> represents a single request against the data provider
 * for a specific index and date range.
 */
public class MarketDataInterestRateRequest {

	private final MarketDataInterestRateIndexMapping marketDataInterestRateIndexMapping;
	private LocalDate startDate;
	private LocalDate endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRateRequest(MarketDataInterestRateIndexMapping marketDataInterestRateIndexMapping, LocalDate startDate, LocalDate endDate) {
		AssertUtils.assertNotNull(marketDataInterestRateIndexMapping, "Investment Interest Rate Index mapping parameter is required.");
		AssertUtils.assertNotNull(startDate, "StartDate cannot be null.");
		AssertUtils.assertNotNull(endDate, "EndDate cannot be null.");
		AssertUtils.assertTrue(startDate.compareTo(endDate) <= 0, "End Date must be after the start date.");

		this.marketDataInterestRateIndexMapping = marketDataInterestRateIndexMapping;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean canExtend(LocalDate newEndingDate) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		return isSequential(newEndingDate);
	}


	public boolean isSequential(LocalDate newEndingDate) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		LocalDate next = getEndDate().plus(1, ChronoUnit.DAYS);
		return next.equals(newEndingDate);
	}


	public void extendEndingDate(LocalDate newEndingDate) {
		AssertUtils.assertNotNull(newEndingDate, "The LocaleDate parameter is required.");

		if (getEndDate().compareTo(newEndingDate) < 0) {
			this.endDate = newEndingDate;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRateIndexMapping getMarketDataInterestRateIndexMapping() {
		return this.marketDataInterestRateIndexMapping;
	}


	public LocalDate getStartDate() {
		return this.startDate;
	}


	public LocalDate getEndDate() {
		return this.endDate;
	}
}
