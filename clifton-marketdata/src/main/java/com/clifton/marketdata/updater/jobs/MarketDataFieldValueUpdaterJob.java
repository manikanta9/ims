package com.clifton.marketdata.updater.jobs;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;
import com.clifton.marketdata.updater.MarketDataUpdaterService;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;


/**
 * The <code>MarketDataFieldValueUpdaterJob</code> updates MarketDataValues
 * for securities that are not allocations of other securities.
 */
public class MarketDataFieldValueUpdaterJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private String dataSourceName;
	private Short dataFieldGroupId;

	/**
	 * If set to true, retrieves the latest values only (reference data as opposed to historic data).
	 * Other date parameters are ignored if latestValuesOnly == true.
	 */
	private boolean latestValuesOnly;

	/**
	 * If specified, for time sensitive fields, force retrieval of values with existing values older than the specified timestamp
	 * regardless of the retrieve latest or skip if exists.
	 */
	private Time latestValuesOverrideTimestamp;

	/**
	 * When latestValuesOnly == true, specifies whether the job should overwrite the date from run date to previous business day.
	 * For example, FUT_FAIR_VAL does not support historical data and new value is only available early next morning for previous weekday.
	 */
	private boolean latestValueIsForPreviousWeekday;
	/**
	 * The numbers of days back from Today (inclusive) to start getting values from
	 */
	private int daysBackIncludingTodayFrom;
	/**
	 * The numbers of days back from Today (inclusive) to stop getting values to
	 */
	private int daysBackIncludingTodayTo;
	private boolean skipLastWeekend = true;

	/**
	 * If true then requests will be batched by field group
	 */
	private boolean batchDataRequests;

	/**
	 * Do not get data for securities when the data already exists
	 */
	private boolean skipDataRequestIfAlreadyExists;

	/**
	 * Denotes whether the system should overwrite existing data or not
	 */
	private boolean overwriteExistingData;

	/**
	 * Optionally limit skipping to a single day of the week, depends on skipDataRequestIfAlreadyExists = true
	 * Uses the numbering scheme from CalendarWeekday (which matches java.util.Calendar).
	 */
	private Short skipOnWeekdayId;

	// filters to limit securities to get market data for
	private Short investmentTypeId;
	private Short investmentSubTypeId;
	private Short investmentSubType2Id;
	private Short investmentHierarchyId;
	private Short investmentGroupId;
	private Short[] securityGroupIds;
	private Short[] excludeSecurityGroupIds;

	private String marketDataProviderOverrideName;
	private Integer marketDataProviderTimeoutOverride;

	/**
	 * The number of securities to be requested at one time
	 */
	private Integer marketDataProviderBatchSizeOverride;

	private StatusHolderObject<Status> statusHolderObject;

	/**
	 * Placeholder field used for displaying instructions in the UI.
	 * <p>
	 * Will not be used.
	 */
	private boolean instructionField;

	/**
	 * Turns off shutting down the terminal after number of errors is received
	 */
	private boolean consecutiveErrorCountDisabled;

	/**
	 * Overrides the number of retries on the message
	 */
	private Integer retryCountOverride;

	////////////////////////////////////////////////////////////////////////////

	private MarketDataFieldService marketDataFieldService;
	private MarketDataUpdaterService marketDataUpdaterService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolderObject = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		Date startDate = null;
		Date endDate = null;

		if (!isLatestValuesOnly()) {
			Date now = new Date();
			startDate = DateUtils.clearTime(addDays(now, -getDaysBackIncludingTodayFrom(), isSkipLastWeekend()));
			endDate = DateUtils.getEndOfDay(addDays(now, -getDaysBackIncludingTodayTo(), isSkipLastWeekend()));
		}

		MarketDataFieldValueUpdaterCommand command = newMarketDataFieldValueUpdaterCommand();
		command.setDataSourceName(getDataSourceName());
		command.setDataFieldGroupId(getDataFieldGroupId());
		command.setInvestmentTypeId(getInvestmentTypeId());
		command.setInvestmentSubTypeId(getInvestmentSubTypeId());
		command.setInvestmentSubType2Id(getInvestmentSubType2Id());
		command.setInvestmentHierarchyId(getInvestmentHierarchyId());
		command.setInvestmentGroupId(getInvestmentGroupId());
		if (getSecurityGroupIds() != null) {
			if (getSecurityGroupIds().length == 1) {
				command.setSecurityGroupId(getSecurityGroupIds()[0]);
			}
			else {
				command.setSecurityGroupIds(getSecurityGroupIds());
			}
		}
		if (getExcludeSecurityGroupIds() != null) {
			if (getExcludeSecurityGroupIds().length == 1) {
				command.setExcludeSecurityGroupId(getExcludeSecurityGroupIds()[0]);
			}
			else {
				command.setExcludeSecurityGroupIds(getExcludeSecurityGroupIds());
			}
		}
		// no start or end dates when isLatestValuesOnly == true
		command.setStartDate(startDate);
		command.setEndDate(endDate);

		command.setLatestValueIsForPreviousWeekday(isLatestValueIsForPreviousWeekday());
		command.setBatchDataRequests(isBatchDataRequests());
		command.setSkipDataRequestIfAlreadyExists(isSkipDataRequestIfAlreadyExists());
		command.setOverwriteExistingData(isOverwriteExistingData());
		if (isLatestValuesOnly()) {
			// guarantee nothing happens with override timestamp unless 'is latest values' is selected.
			command.setLatestValuesOverrideTimestamp(getLatestValuesOverrideTimestamp());
		}
		command.setSkipOnWeekdayId(getSkipOnWeekdayId());
		command.setStatus(this.statusHolderObject.getStatus());
		command.setSynchronous(true);
		if (!StringUtils.isEmpty(getMarketDataProviderOverrideName())) {
			command.setMarketDataProviderOverride(MarketDataProviderOverrides.valueOf(getMarketDataProviderOverrideName()));
		}
		command.setMarketDataProviderTimeoutOverride(getMarketDataProviderTimeoutOverride());
		command.setMarketDataProviderBatchSizeOverride(getMarketDataProviderBatchSizeOverride());

		command.setConsecutiveErrorCountDisabled(isConsecutiveErrorCountDisabled());
		command.setRetryCountOverride(getRetryCountOverride());

		getMarketDataUpdaterService().updateMarketDataFieldValues(command);

		return this.statusHolderObject.getStatus();
	}


	@Override
	public void validate() {
		if (getSkipOnWeekdayId() != null && !isSkipDataRequestIfAlreadyExists()) {
			throw new ValidationException("[Skip If Already Exists] must be true when [Weekday To Skip] is specified.");
		}
		if (getLatestValuesOverrideTimestamp() != null && !isLatestValuesOnly()) {
			throw new ValidationException("[Latest Values Only] must be true when [Latest Values Override Timestamp] is specified.");
		}
	}


	/**
	 * This method can be overridden to return an instance of MarketDataFieldValueUpdaterCommand that has additional attributes/limitations.
	 * This can be used to enhance this batch job in other projects that can further limit securities (specific account holdings, etc.)
	 */
	protected MarketDataFieldValueUpdaterCommand newMarketDataFieldValueUpdaterCommand() {
		return new MarketDataFieldValueUpdaterCommand();
	}


	private Date addDays(Date date, int days, boolean excludeLastWeekend) {
		Date result = DateUtils.addDays(date, days);
		if (excludeLastWeekend) {
			int weekDay = DateUtils.getDayOfWeek(result);
			if (weekDay == Calendar.SATURDAY) {
				result = DateUtils.addDays(result, -1);
			}
			else if (weekDay == Calendar.SUNDAY) {
				result = DateUtils.addDays(result, -2);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public boolean isLatestValuesOnly() {
		return this.latestValuesOnly;
	}


	public void setLatestValuesOnly(boolean latestValuesOnly) {
		this.latestValuesOnly = latestValuesOnly;
	}


	public Time getLatestValuesOverrideTimestamp() {
		return this.latestValuesOverrideTimestamp;
	}


	public void setLatestValuesOverrideTimestamp(Time latestValuesOverrideTimestamp) {
		this.latestValuesOverrideTimestamp = latestValuesOverrideTimestamp;
	}


	public int getDaysBackIncludingTodayFrom() {
		return this.daysBackIncludingTodayFrom;
	}


	public void setDaysBackIncludingTodayFrom(int daysBackIncludingTodayFrom) {
		this.daysBackIncludingTodayFrom = daysBackIncludingTodayFrom;
	}


	public int getDaysBackIncludingTodayTo() {
		return this.daysBackIncludingTodayTo;
	}


	public void setDaysBackIncludingTodayTo(int daysBackIncludingTodayTo) {
		this.daysBackIncludingTodayTo = daysBackIncludingTodayTo;
	}


	public boolean isSkipLastWeekend() {
		return this.skipLastWeekend;
	}


	public void setSkipLastWeekend(boolean skipLastWeekend) {
		this.skipLastWeekend = skipLastWeekend;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentSubTypeId() {
		return this.investmentSubTypeId;
	}


	public void setInvestmentSubTypeId(Short investmentSubTypeId) {
		this.investmentSubTypeId = investmentSubTypeId;
	}


	public Short getInvestmentSubType2Id() {
		return this.investmentSubType2Id;
	}


	public void setInvestmentSubType2Id(Short investmentSubType2Id) {
		this.investmentSubType2Id = investmentSubType2Id;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short[] getSecurityGroupIds() {
		return this.securityGroupIds;
	}


	public void setSecurityGroupIds(Short[] securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}


	public Short[] getExcludeSecurityGroupIds() {
		return this.excludeSecurityGroupIds;
	}


	public void setExcludeSecurityGroupIds(Short[] excludeSecurityGroupIds) {
		this.excludeSecurityGroupIds = excludeSecurityGroupIds;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataUpdaterService getMarketDataUpdaterService() {
		return this.marketDataUpdaterService;
	}


	public void setMarketDataUpdaterService(MarketDataUpdaterService marketDataUpdaterService) {
		this.marketDataUpdaterService = marketDataUpdaterService;
	}


	public boolean isLatestValueIsForPreviousWeekday() {
		return this.latestValueIsForPreviousWeekday;
	}


	public void setLatestValueIsForPreviousWeekday(boolean latestValueIsForPreviousWeekday) {
		this.latestValueIsForPreviousWeekday = latestValueIsForPreviousWeekday;
	}


	public boolean isBatchDataRequests() {
		return this.batchDataRequests;
	}


	public void setBatchDataRequests(boolean batchDataRequests) {
		this.batchDataRequests = batchDataRequests;
	}


	public boolean isSkipDataRequestIfAlreadyExists() {
		return this.skipDataRequestIfAlreadyExists;
	}


	public void setSkipDataRequestIfAlreadyExists(boolean skipDataRequestIfAlreadyExists) {
		this.skipDataRequestIfAlreadyExists = skipDataRequestIfAlreadyExists;
	}


	public boolean isOverwriteExistingData() {
		return this.overwriteExistingData;
	}


	public void setOverwriteExistingData(boolean overwriteExistingData) {
		this.overwriteExistingData = overwriteExistingData;
	}


	public Short getSkipOnWeekdayId() {
		return this.skipOnWeekdayId;
	}


	public void setSkipOnWeekdayId(Short skipOnWeekdayId) {
		this.skipOnWeekdayId = skipOnWeekdayId;
	}


	public String getMarketDataProviderOverrideName() {
		return this.marketDataProviderOverrideName;
	}


	public void setMarketDataProviderOverrideName(String marketDataProviderOverrideName) {
		this.marketDataProviderOverrideName = marketDataProviderOverrideName;
	}


	public boolean isInstructionField() {
		return this.instructionField;
	}


	public void setInstructionField(boolean instructionField) {
		this.instructionField = instructionField;
	}


	public Integer getMarketDataProviderTimeoutOverride() {
		return this.marketDataProviderTimeoutOverride;
	}


	public void setMarketDataProviderTimeoutOverride(Integer marketDataProviderTimeoutOverride) {
		this.marketDataProviderTimeoutOverride = marketDataProviderTimeoutOverride;
	}


	public Integer getMarketDataProviderBatchSizeOverride() {
		return this.marketDataProviderBatchSizeOverride;
	}


	public void setMarketDataProviderBatchSizeOverride(Integer marketDataProviderBatchSizeOverride) {
		this.marketDataProviderBatchSizeOverride = marketDataProviderBatchSizeOverride;
	}


	public boolean isConsecutiveErrorCountDisabled() {
		return this.consecutiveErrorCountDisabled;
	}


	public void setConsecutiveErrorCountDisabled(boolean consecutiveErrorCountDisabled) {
		this.consecutiveErrorCountDisabled = consecutiveErrorCountDisabled;
	}


	public Integer getRetryCountOverride() {
		return this.retryCountOverride;
	}


	public void setRetryCountOverride(Integer retryCountOverride) {
		this.retryCountOverride = retryCountOverride;
	}
}
