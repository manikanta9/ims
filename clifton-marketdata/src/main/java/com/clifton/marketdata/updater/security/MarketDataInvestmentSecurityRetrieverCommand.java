package com.clifton.marketdata.updater.security;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.datasource.MarketDataSource;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@NonPersistentObject
public class MarketDataInvestmentSecurityRetrieverCommand {

	private Short marketDataSourceId;
	private String marketDataSourceName;
	private Short dataFieldGroupId;

	private Short investmentGroupId;
	private Short instrumentHierarchyId;

	private String symbol;
	private String marketSector;

	private Integer investmentSecurityId;
	private Integer instrumentId;
	private Integer columnId;
	private Integer templateId;

	private boolean updateInactiveSecurities = false;
	/**
	 * Indicates that the update should be applied to the existing security.
	 */
	private boolean applyToSecurity;

	private List<String> errors = new ArrayList<>();

	/**
	 * Include unchanged fields when retrieving a preview.
	 */
	private boolean includeUnchanged;

	////////////////////////////////////////////////////////////////////////////

	private MarketDataSource dataSource;
	private InvestmentSecurity security;
	private InvestmentInstrumentHierarchy hierarchy;
	private InvestmentInstrument instrument;

	private String exchangeCode;

	////////////////////////////////////////////////////////////////////////////


	public static MarketDataInvestmentSecurityRetrieverCommand ofSecurity(InvestmentSecurity security, List<String> errors) {
		MarketDataInvestmentSecurityRetrieverCommand result = new MarketDataInvestmentSecurityRetrieverCommand();
		result.setSecurity(security);
		result.setSymbol(security.getSymbol());
		result.setInstrument(security.getInstrument());
		result.setHierarchy(security.getInstrument().getHierarchy());
		result.setErrors(errors);
		return result;
	}


	public SecuritySearchForm getSecuritySearchForm() {
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setInvestmentGroupId(getInvestmentGroupId());
		securitySearchForm.setHierarchyId(getInstrumentHierarchyId());
		if (!isUpdateInactiveSecurities()) {
			securitySearchForm.setInstrumentIsInactive(false); // skip inactive instruments
			securitySearchForm.setActiveOnDate(new Date());
		}
		return securitySearchForm;
	}

	//////////////////////////////////////////////////////////////////////////


	public String getSymbol() {
		if (getSecurity() != null && !getSecurity().isNewBean()) {
			return getSecurity().getSymbol();
		}
		return this.symbol;
	}


	public InvestmentInstrument getInstrument() {
		if ((this.instrument == null) && (getSecurity() != null) && !getSecurity().isNewBean()) {
			return getSecurity().getInstrument();
		}
		return this.instrument;
	}


	public InvestmentInstrumentHierarchy getHierarchy() {
		if ((this.hierarchy == null) && (getSecurity() != null) && !getSecurity().isNewBean()) {
			return getSecurity().getInstrument().getHierarchy();
		}
		return this.hierarchy;
	}
	//////////////////////////////////////////////////////////////////////////


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getMarketSector() {
		return this.marketSector;
	}


	public void setMarketSector(String marketSector) {
		this.marketSector = marketSector;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public void setHierarchy(InvestmentInstrumentHierarchy hierarchy) {
		this.hierarchy = hierarchy;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public Integer getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}


	public boolean isUpdateInactiveSecurities() {
		return this.updateInactiveSecurities;
	}


	public void setUpdateInactiveSecurities(boolean updateInactiveSecurities) {
		this.updateInactiveSecurities = updateInactiveSecurities;
	}


	public List<String> getErrors() {
		return this.errors;
	}


	public void setErrors(List<String> errors) {
		this.errors = errors;
	}


	public boolean isIncludeUnchanged() {
		return this.includeUnchanged;
	}


	public void setIncludeUnchanged(boolean includeUnchanged) {
		this.includeUnchanged = includeUnchanged;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public boolean isApplyToSecurity() {
		return this.applyToSecurity;
	}


	public void setApplyToSecurity(boolean applyToSecurity) {
		this.applyToSecurity = applyToSecurity;
	}
}
