package com.clifton.marketdata.updater;


import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;

import java.util.Date;


public class MarketDataFieldValueUpdaterCommand implements ValidationAware {

	// if not set will default to default Data Source
	private String dataSourceName;

	// if neither is defined, will use default group from instrument's mapping
	private Short marketDataFieldId;
	private Short dataFieldGroupId;

	private Date startDate;
	private Date endDate;

	/**
	 * If true then data requests will be batched by field group.
	 * If false, then a request per security will be performed.
	 */
	private boolean batchDataRequests;


	/**
	 * Do not get data for securities when the data already exists
	 */
	private boolean skipDataRequestIfAlreadyExists;

	/**
	 * Denotes whether to overwrite the data that already exists in the system
	 */
	private boolean overwriteExistingData;

	/**
	 * Force retrieval of values with existing values older than the specified timestamp
	 */
	private Time latestValuesOverrideTimestamp;


	/**
	 * Optionally limit skipping to a single day of the week, depends on skipDataRequestIfAlreadyExists = true
	 */
	private Short skipOnWeekdayId;

	/**
	 * When latestValuesOnly == true (start/end dates are null), specifies whether the job should overwrite the date from run date to previous business day.
	 * For example, FUT_FAIR_VAL does not support historical data and new value is only available early next morning for previous weekday.
	 */
	private boolean latestValueIsForPreviousWeekday;

	/**
	 * Use "systemuser" for run as user (create and update user id). It should be safe to do this because the data is not loaded manually
	 * can cannot be manipulated. It is loaded directly from the market data provider.
	 */
	private boolean runAsSystemUser;

	// security restrictions
	private Integer investmentSecurityId;
	private Short investmentTypeId;
	private Short investmentSubTypeId;
	private Short investmentSubType2Id;
	private Short investmentHierarchyId;
	private Short investmentGroupId;
	private Short securityGroupId;
	private Short[] securityGroupIds;
	private Short excludeSecurityGroupId;
	private Short[] excludeSecurityGroupIds;

	private MarketDataProviderOverrides marketDataProviderOverride;

	// Specifies whether rebuild should be executed asynchronously (runners) or synchronously.
	private boolean synchronous;

	// allows to send "live" updates of rebuild status back to the caller (assume the caller sets this field)
	private Status status;

	/**
	 * If set, will override the default provider timeout
	 */
	private Integer marketDataProviderTimeoutOverride;

	/**
	 * The number of securities to be requested at one time
	 */
	private Integer marketDataProviderBatchSizeOverride;

	/**
	 * Turns off shutting down the terminal after number of errors is received
	 */
	private boolean consecutiveErrorCountDisabled;

	/**
	 * Turns off retrying a request on error
	 */
	private Integer retryCountOverride;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder(64);
		if (getDataSourceName() != null) {
			runId.append(getDataSourceName());
		}

		if (getMarketDataFieldId() != null) {
			runId.append("DF_").append(getMarketDataFieldId());
		}
		else if (getDataFieldGroupId() != null) {
			runId.append("FG_").append(getDataFieldGroupId());
		}

		if (getInvestmentGroupId() != null) {
			runId.append("IG_").append(getInvestmentGroupId());
		}
		if (getSecurityGroupId() != null) {
			runId.append("SG_").append(getSecurityGroupId());
		}
		if (getInvestmentHierarchyId() != null) {
			runId.append("IH_").append(getInvestmentHierarchyId());
		}
		if (getInvestmentSecurityId() != null) {
			runId.append("S_").append(getInvestmentSecurityId());
		}
		runId.append("-").append(DateUtils.fromDateShort(getStartDate()));
		runId.append("-").append(DateUtils.fromDateShort(getEndDate()));
		return runId.toString();
	}


	public SecuritySearchForm getSecuritySearchForm() {
		validate();
		SecuritySearchForm securitySearchForm = new SecuritySearchForm();
		securitySearchForm.setId(getInvestmentSecurityId());
		securitySearchForm.setInvestmentTypeId(getInvestmentTypeId());
		securitySearchForm.setInvestmentTypeSubTypeId(getInvestmentSubTypeId());
		securitySearchForm.setInvestmentTypeSubType2Id(getInvestmentSubType2Id());
		securitySearchForm.setHierarchyId(getInvestmentHierarchyId());
		securitySearchForm.setInvestmentGroupId(getInvestmentGroupId());
		securitySearchForm.setSecurityGroupId(getSecurityGroupId());
		securitySearchForm.setSecurityGroupIds(getSecurityGroupIds());
		securitySearchForm.setExcludeInvestmentSecurityGroupId(getExcludeSecurityGroupId());
		securitySearchForm.setExcludeInvestmentSecurityGroupIds(getExcludeSecurityGroupIds());
		securitySearchForm.setInstrumentIsInactive(false); // skip inactive instruments
		return securitySearchForm;
	}


	@Override
	public void validate() {
		// should never get all securities: too much data
		if (getInvestmentSecurityId() == null && getInvestmentTypeId() == null && getInvestmentSubTypeId() == null && getInvestmentSubType2Id() == null && getInvestmentHierarchyId() == null && getInvestmentGroupId() == null && getSecurityGroupId() == null && getSecurityGroupIds() == null) {
			throw new ValidationException("Security, Instrument Hierarchy, Investment Group or Security Group command restriction is required.");
		}
	}


	/**
	 * Specifies whether this security should be skipped. Default implementation includes all active securities.
	 * This method can be overridden in derived classes in order to add additional restrictions.
	 * For example, one can use this method to limit securities to those that have active positions in a particular account.
	 */
	public boolean isSecuritySkipped(InvestmentSecurity security, Date startDate) {
		Date terminationDate = ObjectUtils.coalesce(security.getEarlyTerminationDate(), security.getEndDate());
		if (terminationDate != null) {
			Date fromDate = startDate;
			if (fromDate == null) {
				fromDate = new Date();
			}
			fromDate = DateUtils.clearTime(fromDate);
			if (fromDate.after(terminationDate)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Determine if the value can be skipped for a specific date based on isSkipDataRequestIfAlreadyExists and skipOnWeekdayId.
	 * isSkipDataRequestIfAlreadyExists = false => false
	 * isSkipDataRequestIfAlreadyExists = true => skipOnWeekdayId == null => true
	 * isSkipDataRequestIfAlreadyExists = true => skipOnWeekdayId != null
	 * => valueDate day of week == skipOnWeekdayId => true
	 * => valueDate day of week != skipONWeekdayId => false
	 */
	public boolean calculateSkipIfAlreadyExists(Date valueDate, Time valueTimeStamp) {
		// day of week must be numeric from the Calendar class so the numbering scheme matches CalendarWeekday used by SystemBeanType
		return (isSkipDataRequestIfAlreadyExists() && (getSkipOnWeekdayId() == null || getSkipOnWeekdayId() == DateUtils.getDayOfWeek(valueDate))) &&
				(getLatestValuesOverrideTimestamp() == null || valueTimeStamp == null || valueTimeStamp.getMilliseconds() >= getLatestValuesOverrideTimestamp().getMilliseconds());
	}


	/**
	 * Determine if the value can be overwritten for a specific date based on isOverwriteExistingData and skipOnWeekdayId.
	 * isOverwriteExistingData = false => false
	 * isOverwriteExistingData = true => skipOnWeekdayId == null => true
	 * isOverwriteExistingData = true => skipOnWeekdayId != null
	 * => valueDate day of week == skipOnWeekdayId => true
	 * => valueDate day of week != skipONWeekdayId => false
	 */
	public boolean calculateOverwriteExistingData(Date valueDate, Time valueTimeStamp) {
		// day of week must be numeric from the Calendar class so the numbering scheme matches CalendarWeekday used by SystemBeanType
		return (isOverwriteExistingData() && (getSkipOnWeekdayId() == null || getSkipOnWeekdayId() == DateUtils.getDayOfWeek(valueDate))) ||
				(isOverwriteExistingData() && getLatestValuesOverrideTimestamp() != null && valueTimeStamp != null && !valueTimeStamp.equals(getLatestValuesOverrideTimestamp()));
	}
	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Short getMarketDataFieldId() {
		return this.marketDataFieldId;
	}


	public void setMarketDataFieldId(Short marketDataFieldId) {
		this.marketDataFieldId = marketDataFieldId;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentSubTypeId() {
		return this.investmentSubTypeId;
	}


	public void setInvestmentSubTypeId(Short investmentSubTypeId) {
		this.investmentSubTypeId = investmentSubTypeId;
	}


	public Short getInvestmentSubType2Id() {
		return this.investmentSubType2Id;
	}


	public void setInvestmentSubType2Id(Short investmentSubType2Id) {
		this.investmentSubType2Id = investmentSubType2Id;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Short[] getSecurityGroupIds() {
		return this.securityGroupIds;
	}


	public void setSecurityGroupIds(Short[] securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}


	public Short getExcludeSecurityGroupId() {
		return this.excludeSecurityGroupId;
	}


	public void setExcludeSecurityGroupId(Short excludeSecurityGroupId) {
		this.excludeSecurityGroupId = excludeSecurityGroupId;
	}


	public Short[] getExcludeSecurityGroupIds() {
		return this.excludeSecurityGroupIds;
	}


	public void setExcludeSecurityGroupIds(Short[] excludeSecurityGroupIds) {
		this.excludeSecurityGroupIds = excludeSecurityGroupIds;
	}


	public boolean isLatestValueIsForPreviousWeekday() {
		return this.latestValueIsForPreviousWeekday;
	}


	public void setLatestValueIsForPreviousWeekday(boolean latestValueIsForPreviousWeekday) {
		this.latestValueIsForPreviousWeekday = latestValueIsForPreviousWeekday;
	}


	public boolean isRunAsSystemUser() {
		return this.runAsSystemUser;
	}


	public void setRunAsSystemUser(boolean runAsSystemUser) {
		this.runAsSystemUser = runAsSystemUser;
	}


	public boolean isBatchDataRequests() {
		return this.batchDataRequests;
	}


	public void setBatchDataRequests(boolean batchDataRequests) {
		this.batchDataRequests = batchDataRequests;
	}


	public boolean isSkipDataRequestIfAlreadyExists() {
		return this.skipDataRequestIfAlreadyExists;
	}


	public void setSkipDataRequestIfAlreadyExists(boolean skipDataRequestIfAlreadyExists) {
		this.skipDataRequestIfAlreadyExists = skipDataRequestIfAlreadyExists;
	}


	public boolean isOverwriteExistingData() {
		return this.overwriteExistingData;
	}


	public void setOverwriteExistingData(boolean overwriteExistingData) {
		this.overwriteExistingData = overwriteExistingData;
	}


	public Time getLatestValuesOverrideTimestamp() {
		return this.latestValuesOverrideTimestamp;
	}


	public void setLatestValuesOverrideTimestamp(Time latestValuesOverrideTimestamp) {
		this.latestValuesOverrideTimestamp = latestValuesOverrideTimestamp;
	}


	public Short getSkipOnWeekdayId() {
		return this.skipOnWeekdayId;
	}


	public void setSkipOnWeekdayId(Short skipOnWeekdayId) {
		this.skipOnWeekdayId = skipOnWeekdayId;
	}


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public MarketDataProviderOverrides getMarketDataProviderOverride() {
		return this.marketDataProviderOverride;
	}


	public void setMarketDataProviderOverride(MarketDataProviderOverrides marketDataProviderOverride) {
		this.marketDataProviderOverride = marketDataProviderOverride;
	}


	public Integer getMarketDataProviderTimeoutOverride() {
		return this.marketDataProviderTimeoutOverride;
	}


	public void setMarketDataProviderTimeoutOverride(Integer marketDataProviderTimeoutOverride) {
		this.marketDataProviderTimeoutOverride = marketDataProviderTimeoutOverride;
	}


	public Integer getMarketDataProviderBatchSizeOverride() {
		return this.marketDataProviderBatchSizeOverride;
	}


	public void setMarketDataProviderBatchSizeOverride(Integer marketDataProviderBatchSizeOverride) {
		this.marketDataProviderBatchSizeOverride = marketDataProviderBatchSizeOverride;
	}


	public boolean isConsecutiveErrorCountDisabled() {
		return this.consecutiveErrorCountDisabled;
	}


	public void setConsecutiveErrorCountDisabled(boolean consecutiveErrorCountDisabled) {
		this.consecutiveErrorCountDisabled = consecutiveErrorCountDisabled;
	}


	public Integer getRetryCountOverride() {
		return this.retryCountOverride;
	}


	public void setRetryCountOverride(Integer retryCountOverride) {
		this.retryCountOverride = retryCountOverride;
	}
}
