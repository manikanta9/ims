package com.clifton.marketdata.updater.security;

import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;

import java.util.Map;


/**
 * @author mwacker
 */
public class MarketDataFieldColumnMappingHolder {

	private Map<MarketDataFieldColumnMapping, Object> securityValueMap;


	public Map<MarketDataFieldColumnMapping, Object> getSecurityValueMap() {
		return this.securityValueMap;
	}


	public void setSecurityValueMap(Map<MarketDataFieldColumnMapping, Object> securityValueMap) {
		this.securityValueMap = securityValueMap;
	}
}
