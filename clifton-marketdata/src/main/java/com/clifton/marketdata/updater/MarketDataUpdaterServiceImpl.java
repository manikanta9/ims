package com.clifton.marketdata.updater;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.holiday.CalendarHolidayDay;
import com.clifton.calendar.holiday.CalendarHolidayService;
import com.clifton.calendar.holiday.search.CalendarHolidayDaySearchForm;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.IntegerMap;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocationService;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.marketdata.rates.search.InterestRateIndexMappingSearchForm;
import com.clifton.marketdata.rates.search.InterestRateSearchForm;
import com.clifton.marketdata.updater.request.MarketDataFieldRequest;
import com.clifton.marketdata.updater.request.MarketDataFieldRequestCollection;
import com.clifton.marketdata.updater.request.MarketDataInterestRateRequest;
import com.clifton.marketdata.updater.request.MarketDataInterestRateRequestCollection;
import com.clifton.security.impersonation.SecurityImpersonationHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Service
public class MarketDataUpdaterServiceImpl implements MarketDataUpdaterService {

	private CalendarBusinessDayService calendarBusinessDayService;
	private CalendarHolidayService calendarHolidayService;
	private CalendarSetupService calendarSetupService;

	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentCalculator investmentCalculator;

	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataRatesService marketDataRatesService;
	private MarketDataSourceService marketDataSourceService;

	private SecurityImpersonationHandler securityImpersonationHandler;

	private SystemColumnValueHandler systemColumnValueHandler;

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	///////               Interest Rate Update Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void updateMarketDataInterestRateListForDataSource(String dataSourceName) {
		MarketDataSource dataSource = getDataSourceFromDB(dataSourceName);
		MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(dataSource);

		for (MarketDataInterestRateIndexMapping mapping : CollectionUtils.getIterable(getMarketDataRatesService().getMarketDataInterestRateIndexMappingListForDataSource(dataSource.getId(), true))) {
			MarketDataInterestRate providerRate = dataProvider.getMarketDataInterestRateLatest(mapping);
			saveOrUpdateInterestRate(dataSource, mapping, providerRate);
		}
	}


	@Override
	public Status updateMarketDataInterestRates(MarketDataInterestRateUpdaterCommand command) {
		AssertUtils.assertNotNull(command, "Command parameter is required.");
		AssertUtils.assertNotNull(command.getStartDate(), "Command start date is required.");
		AssertUtils.assertNotNull(command.getEndDate(), "Command end date is required.");

		MarketDataSource dataSource = getDataSourceFromDB(command.getDataSourceName());
		AssertUtils.assertNotNull(dataSource, "Market Data Source parameter is required.");

		if (command.isSkipIfAlreadyExists()) {
			doUpdateMarketDataInterestRates(command, dataSource);
		}
		else {
			IntegerMap<String> counts = new IntegerMap<>();
			counts.initialize(0, "Total", "Looked up", "Values Updated", "Failed");

			MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(dataSource);
			List<MarketDataInterestRateIndexMapping> mappingList = getMarketDataInterestRateIndexMapping(command, dataSource);
			for (MarketDataInterestRateIndexMapping mapping : CollectionUtils.getIterable(mappingList)) {
				try {
					List<MarketDataInterestRate> providerRates = dataProvider.getMarketDataInterestRateListForPeriod(mapping, command.getStartDate(), command.getEndDate());
					counts.add(providerRates.size(), "Total", "Looked up", "Values Updated");
					for (MarketDataInterestRate providerRate : CollectionUtils.getIterable(providerRates)) {
						saveOrUpdateInterestRate(dataSource, mapping, providerRate);
					}
				}
				catch (Throwable e) {
					command.getStatus().addError("Error updating Interest Rates for " + mapping.getSymbol() + ": " + ExceptionUtils.getDetailedMessage(e));
					counts.add("Failed", 1);
				}
			}
			command.getStatus().setMessage(counts.toString(map -> {
				List<String> values = new ArrayList<>();
				for (Map.Entry<String, Integer> entry : map.entrySet()) {
					values.add(entry.getKey() + " = " + entry.getValue());
				}
				return String.join(", ", values);
			}));
		}
		return command.getStatus();
	}


	/**
	 * Does selective fetching and updating of {@link MarketDataInterestRate} entities based on whether the data already exists.
	 */
	private void doUpdateMarketDataInterestRates(MarketDataInterestRateUpdaterCommand command, MarketDataSource dataSource) {
		IntegerMap<String> counts = new IntegerMap<>();
		counts.initialize(0, "Total", "Looked up", "Values Updated", "Existing Values Skipped", "Non-Business Day Values Skipped", "Failed");

		List<MarketDataInterestRateRequestCollection> marketDataInterestRateRequestCollections = getMarketDataInterestRateRequestCollections(command, dataSource, counts);

		MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(dataSource);
		for (MarketDataInterestRateRequestCollection marketDataInterestRateRequestCollection : marketDataInterestRateRequestCollections) {
			List<MarketDataInterestRateRequest> marketDataInterestRateRequests = marketDataInterestRateRequestCollection.populateRequests();
			for (MarketDataInterestRateRequest marketDataInterestRateRequest : marketDataInterestRateRequests) {
				MarketDataInterestRateIndexMapping mapping = marketDataInterestRateRequest.getMarketDataInterestRateIndexMapping();
				try {
					Date startDate = DateUtils.asUtilDate(marketDataInterestRateRequest.getStartDate());
					Date endDate = DateUtils.asUtilDate(marketDataInterestRateRequest.getEndDate());
					List<MarketDataInterestRate> providerRates = dataProvider.getMarketDataInterestRateListForPeriod(mapping, startDate, endDate);
					counts.add(providerRates.size(), "Looked up", "Values Updated");
					for (MarketDataInterestRate providerRate : CollectionUtils.getIterable(providerRates)) {
						getMarketDataRatesService().saveMarketDataInterestRate(providerRate);
					}
				}
				catch (Throwable e) {
					command.getStatus().addError("Error updating Interest Rates for " + mapping.getSymbol() + ": " + ExceptionUtils.getDetailedMessage(e));
					counts.add("Failed", 1);
				}
			}
		}
		command.getStatus().setMessage(counts.toString(map -> {
			List<String> values = new ArrayList<>();
			for (Map.Entry<String, Integer> entry : map.entrySet()) {
				values.add(entry.getKey() + " = " + entry.getValue());
			}
			return String.join(", ", values);
		}));
	}


	/**
	 * Fetch all active {@link MarketDataInterestRateIndexMapping} entities for a given {@link MarketDataSource} and
	 * {@link com.clifton.investment.rates.InvestmentInterestRateIndexGroup}
	 */
	private List<MarketDataInterestRateIndexMapping> getMarketDataInterestRateIndexMapping(MarketDataInterestRateUpdaterCommand command, MarketDataSource dataSource) {
		InterestRateIndexMappingSearchForm searchForm = new InterestRateIndexMappingSearchForm();
		searchForm.setDataSourceId(dataSource.getId());
		searchForm.setInterestRateIndexGroupId(command.getInterestRateIndexGroupId());
		searchForm.setInterestRateIndexActive(true);
		return getMarketDataRatesService().getMarketDataInterestRateIndexMappingList(searchForm);
	}


	/**
	 * Iterates through all dates within the {@link MarketDataInterestRateUpdaterCommand} parameter's start and ending dates and builds a list of requests with sequential date
	 * ranges for each {@link MarketDataInterestRateIndexMapping} with consideration taken for holidays, weekends, and existing data.
	 */
	private List<MarketDataInterestRateRequestCollection> getMarketDataInterestRateRequestCollections(MarketDataInterestRateUpdaterCommand command, MarketDataSource dataSource, IntegerMap<String> counts) {
		List<MarketDataInterestRateRequestCollection> results = new ArrayList<>();
		List<MarketDataInterestRateIndexMapping> mappingList = getMarketDataInterestRateIndexMapping(command, dataSource);

		Map<Integer, List<Date>> investmentInterestRateIndexListMap = getExistingMarketDataInterestRateIndexMapping(mappingList, command);
		LocalDate toDate = DateUtils.asLocalDate(command.getEndDate());
		for (MarketDataInterestRateIndexMapping mapping : CollectionUtils.getIterable(mappingList)) {
			LocalDate fromDate = DateUtils.asLocalDate(command.getStartDate());
			Calendar calendar = mapping.getReferenceOne().getCalendar() == null ? getCalendarSetupService().getDefaultCalendar() : mapping.getReferenceOne().getCalendar();
			List<Date> existingDates = CollectionUtils.asNonNullList(investmentInterestRateIndexListMap.get(mapping.getReferenceOne().getId()));
			MarketDataInterestRateRequestCollection marketDataInterestRateRequestCollection = new MarketDataInterestRateRequestCollection(mapping, fromDate, toDate, command.isSkipIfAlreadyExists());
			// from..to dates inclusive
			while (!fromDate.isAfter(toDate)) {
				Date tmpFromDate = DateUtils.asUtilDate(fromDate);
				if (DateUtils.isWeekday(tmpFromDate) && getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDate(tmpFromDate, calendar.getId()))) {
					if (existingDates.contains(tmpFromDate)) {
						counts.add("Existing Values Skipped", 1);
					}
					else {
						marketDataInterestRateRequestCollection.missingDate(fromDate);
					}
				}
				else {
					marketDataInterestRateRequestCollection.addNonBusinessDate(fromDate);
					counts.add("Non-Business Day Values Skipped", 1);
				}
				fromDate = fromDate.plus(1, ChronoUnit.DAYS);
				counts.add("Total", 1);
			}
			results.add(marketDataInterestRateRequestCollection);
		}
		return results;
	}


	/**
	 * For all existing {@link MarketDataInterestRate} entities within the provided date range (inclusive), build a map of {@link InvestmentInterestRateIndex} IDs to interest Rate Dates.
	 */
	private Map<Integer, List<Date>> getExistingMarketDataInterestRateIndexMapping(List<MarketDataInterestRateIndexMapping> mappingList, MarketDataInterestRateUpdaterCommand command) {
		List<Integer> indexIds = mappingList.stream().map(m -> m.getReferenceOne().getId()).collect(Collectors.toList());
		List<MarketDataInterestRate> marketDataInterestRateList = CollectionUtils.asNonNullList(getMarketDataRatesService()
				.getMarketDataInterestRateListForIndicesPeriod(indexIds, command.getStartDate(), command.getEndDate()));
		marketDataInterestRateList.forEach(r -> command.getStatus().addSkipped(r.getLabel()));
		return marketDataInterestRateList
				.stream()
				.collect(Collectors.groupingBy(r -> r.getInterestRateIndex().getId()
						, Collectors.mapping(MarketDataInterestRate::getInterestRateDate, Collectors.toList())));
	}


	/**
	 * Fetch the {@link MarketDataInterestRate} from the database, if it does not exist, insert a new record using the providerRate parameter, otherwise,
	 * update the existing record with the providerRate interest rate value if they differ.
	 */
	private void saveOrUpdateInterestRate(MarketDataSource dataSource, MarketDataInterestRateIndexMapping mapping, MarketDataInterestRate providerRate) {
		if (providerRate != null) {
			// check if existing rate needs to be update or new one created
			InterestRateSearchForm searchForm = new InterestRateSearchForm();
			searchForm.setDataSourceId(dataSource.getId());
			searchForm.setInterestRateIndexId(mapping.getReferenceOne().getId());
			searchForm.setInterestRateDate(DateUtils.clearTime(providerRate.getInterestRateDate()));
			List<MarketDataInterestRate> rateList = getMarketDataRatesService().getMarketDataInterestRateList(searchForm);
			int count = CollectionUtils.getSize(rateList);

			if (count == 0) {
				// no rate for the day, insert the new one
				getMarketDataRatesService().saveMarketDataInterestRate(providerRate);
			}
			else if (count == 1) {
				// update existing rate, if interest rate changed.
				MarketDataInterestRate existingRate = rateList.get(0);
				if (MathUtils.isNotEqual(existingRate.getInterestRate(), providerRate.getInterestRate())) {
					existingRate.setInterestRate(providerRate.getInterestRate());
					getMarketDataRatesService().saveMarketDataInterestRate(existingRate);
				}
			}
			else {
				throw new RuntimeException("Found " + count + " interest rates  for " + providerRate.getLabel() + " while up to 1 is allowed");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////                 Data Field Update Methods                    ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status updateMarketDataFieldValues(MarketDataFieldValueUpdaterCommand command) {
		if (command.isRunAsSystemUser()) {
			return getSecurityImpersonationHandler().runAsSecurityUserNameAndReturn(SecurityUser.SYSTEM_USER, () -> doUpdateMarketDataFieldValues(command));
		}
		return doUpdateMarketDataFieldValues(command);
	}


	private Status doUpdateMarketDataFieldValues(MarketDataFieldValueUpdaterCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofEmptyMessage();
			status.setMessage("Synchronously running for " + command);
			return doUpdateMarketDataFieldValuesSynchronous(command);
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		if (command.getStatus() == null) {
			command.setStatus(Status.ofMessage("Scheduled for " + DateUtils.fromDate(now)));
		}

		Runner runner = new AbstractStatusAwareRunner("MARKETDATA-IMPORT", runId, now, new StatusHolder(command.getStatus())) {

			@Override
			public void run() {
				try {
					doUpdateMarketDataFieldValuesSynchronous(command);
				}
				catch (Throwable e) {
					getStatus().setMessage("Error importing market data for " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error importing market data for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started Market Data Import: " + runId + ". Processing will be completed shortly.");
	}


	private Status doUpdateMarketDataFieldValuesSynchronous(MarketDataFieldValueUpdaterCommand command) {
		String statusMessage = "Running for " + command;
		Status status = (command.getStatus() != null) ? command.getStatus() : Status.ofMessage(statusMessage);

		MarketDataSource dataSource = getDataSourceFromDB(command.getDataSourceName());
		MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(dataSource);

		// get all securities that are being updated: either for the instrument or hierarchy
		SecuritySearchForm securitySearchForm = command.getSecuritySearchForm();

		List<MarketDataField> argumentFieldList = null;
		if (command.getDataFieldGroupId() != null) {
			argumentFieldList = getMarketDataFieldService().getMarketDataFieldListByGroup(command.getDataFieldGroupId());
		}
		if (command.getMarketDataFieldId() != null) {
			MarketDataField dataField = getMarketDataFieldService().getMarketDataField(command.getMarketDataFieldId());
			if (dataField != null) {
				if (argumentFieldList == null) {
					argumentFieldList = new ArrayList<>();
				}
				if (!argumentFieldList.contains(dataField)) {
					argumentFieldList.add(dataField);
				}
			}
		}

		Date overrideDate = null;
		if (command.getStartDate() == null && command.getEndDate() == null && command.isLatestValueIsForPreviousWeekday()) {
			overrideDate = DateUtils.getPreviousWeekday(DateUtils.clearTime(new Date()));
		}

		Time overrideTime = null;
		if (command.getStartDate() == null && command.getEndDate() == null && command.getLatestValuesOverrideTimestamp() != null) {
			overrideTime = command.getLatestValuesOverrideTimestamp();
		}

		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(securitySearchForm);

		MarketDataFieldHistoricalUpdateCommand historicalUpdateCommand = new MarketDataFieldHistoricalUpdateCommand(command, dataSource, dataProvider, securityList, argumentFieldList, status);
		List<MarketDataValue> dataValuesFromProvider = command.isBatchDataRequests() ? doUpdateMarketDataFieldValuesSynchronousWithBatching(historicalUpdateCommand) : doUpdateMarketDataFieldValuesSynchronousWithSkip(historicalUpdateCommand);
		status = historicalUpdateCommand.getStatus();

		if (CollectionUtils.isEmpty(dataValuesFromProvider)) {
			status.addMessage("No values returned");
		}
		else {
			Set<InvestmentSecurity> processedSecurities = new HashSet<>(securityList);
			//Skipped securities are already accounted for
			processedSecurities.removeAll(historicalUpdateCommand.getSkippedSecurityList());

			for (MarketDataValue dataValue : dataValuesFromProvider) {
				//It is possible that there are multiple data values for the same security
				// Here we are only checking if ANY data has been returned for the security
				processedSecurities.remove(dataValue.getInvestmentSecurity());
				try {
					if (overrideDate != null) {
						dataValue.setMeasureDate(overrideDate);
					}

					boolean updateIfExists = command.calculateOverwriteExistingData(dataValue.getMeasureDate(), dataValue.getMeasureTime());
					if (overrideTime != null && dataValue.getMeasureTime() != null && !dataValue.getMeasureTime().equals(overrideTime)) {
						dataValue.setMeasureTime(overrideTime);
					}
					if (getMarketDataFieldService().saveMarketDataValueWithOptions(dataValue, updateIfExists) != null) {
						historicalUpdateCommand.getRecordsUpdated().incrementAndGet();
					}
					else {
						historicalUpdateCommand.getRecordsSkipped().incrementAndGet();
					}
				}
				catch (Throwable e) {
					// record the error but continue processing
					LogUtils.errorOrInfo(getClass(), "Error saving data value: " + dataValue, e);
					status.addError(dataValue + ": " + ExceptionUtils.getDetailedMessage(e));
					historicalUpdateCommand.getErrorCount().incrementAndGet();
				}
			}

			if (!CollectionUtils.isEmpty(processedSecurities)) {
				Map<InvestmentInstrumentHierarchy, List<InvestmentSecurity>> hierarchySecurityListMap = new HashMap<>();

				//If there are any securities remaining in the set at this point that means no data was returned for them
				for (InvestmentSecurity security : processedSecurities) {
					List<InvestmentSecurity> hierarchySecurities = hierarchySecurityListMap.computeIfAbsent(security.getInstrument().getHierarchy(), k -> new ArrayList<>());
					hierarchySecurities.add(security);
					historicalUpdateCommand.getErrorCount().incrementAndGet();
				}

				StringBuilder errorMessage = new StringBuilder("Data was not returned for the following hierarchies and securities: ");
				for (Map.Entry<InvestmentInstrumentHierarchy, List<InvestmentSecurity>> entry : hierarchySecurityListMap.entrySet()) {
					errorMessage.append("\n").append(entry.getKey().getNameExpanded()).append("\n");
					errorMessage.append(StringUtils.collectionToCommaDelimitedString(entry.getValue(), InvestmentSecurity::getSymbol));
					errorMessage.append("\n\n");
				}

				status.addError(errorMessage.toString());
			}
		}

		statusMessage = "Securities: " + historicalUpdateCommand.getSecurityList().size() + "; Processed: " + historicalUpdateCommand.getProcessedSecurities().intValue() + "; Skipped: " + historicalUpdateCommand.getSkippedSecurityList().size() + "; Values Updated: " + historicalUpdateCommand.getRecordsUpdated().intValue() + "; Values Skipped: " + historicalUpdateCommand.getRecordsSkipped().intValue() + "; Failed: " + historicalUpdateCommand.getErrorCount().intValue();
		status.setMessage(statusMessage);
		return status;
	}


	/**
	 * Gets a list of {@link MarketDataField}s to be requested.
	 * <p>
	 * The map is included to cache data on the fly and reduce unneeded lookups
	 */
	private List<MarketDataField> getMarketDataFieldList(MarketDataFieldHistoricalUpdateCommand command, Map<Short, List<MarketDataField>> fieldGroupMarketDataFieldMap, Short fieldGroupId) {
		List<MarketDataField> fieldList = command.getArgumentFieldList();
		if (fieldList == null) {
			fieldList = fieldGroupMarketDataFieldMap.get(fieldGroupId);
			if (fieldList == null) {
				fieldList = getMarketDataFieldService().getMarketDataFieldListByGroup(fieldGroupId);
				// do not include data fields with no external name
				fieldList = BeanUtils.filterNotNull(fieldList, MarketDataField::getExternalFieldName);
				fieldGroupMarketDataFieldMap.put(fieldGroupId, fieldList);
			}
		}
		return fieldList;
	}


	/**
	 * Gets the list of {@link MarketDataValue}s without optimizing the data request. Groups {@link InvestmentSecurity}s by mapping - securities with common fields will be requested together.
	 * <p>
	 * I.e. Could request data that already exists and, depending on the circumstances, may not be advantageous to look up again
	 */
	private List<MarketDataValue> doUpdateMarketDataFieldValuesSynchronousWithBatching(MarketDataFieldHistoricalUpdateCommand command) {
		Map<Short, List<MarketDataField>> fieldGroupMarketDataFieldMap = new HashMap<>();
		Map<MarketDataFieldMapping, List<InvestmentSecurity>> marketDataFieldMappingSecurityListMap = new HashMap<>();
		Map<MarketDataFieldMapping, Set<MarketDataField>> marketDataFieldMappingFieldSetMap = new HashMap<>();
		Map<InvestmentInstrument, MarketDataFieldMapping> instrumentMarketDataFieldMappingMap = new HashMap<>();

		String statusMessage = "Ready to start data retrieval for " + command.getSecurityList().size() + " securities.";
		command.getStatus().setMessage(statusMessage);
		MarketDataFieldValueUpdaterCommand marketDataFieldValueUpdaterCommand = command.getMarketDataUpdaterCommand();
		Integer marketDataFieldProviderOverride = marketDataFieldValueUpdaterCommand.getMarketDataProviderTimeoutOverride();
		processSecuritiesAndFieldsForBatching(command, instrumentMarketDataFieldMappingMap, fieldGroupMarketDataFieldMap, marketDataFieldMappingFieldSetMap, marketDataFieldMappingSecurityListMap);

		List<String> errorList = new ArrayList<>();
		List<MarketDataValue> dataValuesFromProvider = new ArrayList<>();

		for (Map.Entry<MarketDataFieldMapping, List<InvestmentSecurity>> entry : marketDataFieldMappingSecurityListMap.entrySet()) {
			try {
				MarketDataFieldMapping mapping = entry.getKey();

				DataFieldValueCommand.DataFieldValueCommandBuilder fieldValueCommandBuilder = DataFieldValueCommand.newBuilder()
						.setMarketSector(mapping.getMarketSector().getName())
						.setPricingSource(mapping.getPricingSource() != null ? mapping.getPricingSource().getSymbol() : null)
						.setExchangeCodeType(mapping.getExchangeCodeType())
						.setStartDate(DateUtils.asUtilDate(marketDataFieldValueUpdaterCommand.getStartDate()))
						.setEndDate(DateUtils.asUtilDate(marketDataFieldValueUpdaterCommand.getEndDate()))
						.withProviderOverride(marketDataFieldValueUpdaterCommand.getMarketDataProviderOverride())
						.setProviderTimeoutOverride(marketDataFieldProviderOverride)
						.setProviderBatchSizeOverride(marketDataFieldValueUpdaterCommand.getMarketDataProviderBatchSizeOverride())
						.setConsecutiveErrorCountDisabled(marketDataFieldValueUpdaterCommand.isConsecutiveErrorCountDisabled())
						.setRetryCountOverride(marketDataFieldValueUpdaterCommand.getRetryCountOverride())
						.setErrors(errorList);
				Set<MarketDataField> fieldSet = marketDataFieldMappingFieldSetMap.get(mapping);
				for (InvestmentSecurity security : entry.getValue()) {
					fieldValueCommandBuilder.addSecurity(security, new ArrayList<>(fieldSet));
				}

				DataFieldValueCommand fieldValueCommand = fieldValueCommandBuilder.build();

				// get LATEST data if start and end dates are null
				if (fieldValueCommand.isLatest()) {
					command.getStatus().setMessage("Getting reference data for mapping: " + mapping.getLabel() + "; Securities: " + fieldValueCommand.getInvestmentSecurityList().size());
					DataFieldValueResponse<MarketDataValue> fieldValueResponse = command.getDataProvider().getMarketDataValueLatest(fieldValueCommand);
					dataValuesFromProvider.addAll(fieldValueResponse.getMarketDataValueListForSingleSecurityStrict());
					command.getStatus().addErrors(errorList);
				}
				else {
					command.getStatus().setMessage("Getting historical data for mapping: " + mapping.getLabel() + "; Securities: " + fieldValueCommand.getInvestmentSecurityList().size());
					//Historical request
					dataValuesFromProvider.addAll(
							CollectionUtils.asNonNullList(
									command.getDataProvider().getMarketDataValueListHistorical(fieldValueCommand, errorList)
							)
					);
				}
				command.getStatus().addErrors(errorList);
				command.getErrorCount().addAndGet(errorList.size());
				command.getProcessedSecurities().addAndGet(entry.getValue().size() - errorList.size());
				statusMessage = "Securities: " + command.getSecurityList().size() + "; Processed: " + command.getProcessedSecurities().intValue() + "; Skipped: " + command.getSkippedSecurityList().size() + "; Values Updated: " + command.getRecordsUpdated().intValue() + "; Values Skipped: " + command.getRecordsSkipped().intValue() + "; Failed: " + command.getErrorCount().intValue();
				command.getStatus().setMessage(statusMessage);
			}
			catch (Throwable e) {
				// record the error but continue processing
				command.getStatus().addError(ExceptionUtils.getDetailedMessage(e));
				command.getErrorCount().incrementAndGet();
			}
		}
		return dataValuesFromProvider;
	}


	/**
	 * Loops all securities in the command and populates the given maps appropriately to support batching requests to a Market Data Provider
	 */
	private void processSecuritiesAndFieldsForBatching(MarketDataFieldHistoricalUpdateCommand command, Map<InvestmentInstrument, MarketDataFieldMapping> instrumentMarketDataFieldMappingMap, Map<Short, List<MarketDataField>> fieldGroupMarketDataFieldMap, Map<MarketDataFieldMapping, Set<MarketDataField>> marketDataFieldMappingFieldSetMap, Map<MarketDataFieldMapping, List<InvestmentSecurity>> marketDataFieldMappingSecurityListMap) {
		MarketDataFieldValueUpdaterCommand marketDataFieldValueUpdaterCommand = command.getMarketDataUpdaterCommand();
		for (InvestmentSecurity security : CollectionUtils.getIterable(command.getSecurityList())) {
			MarketDataFieldMapping mapping = instrumentMarketDataFieldMappingMap.get(security.getInstrument());
			if (mapping == null) {
				mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), command.getDataSource());
				if (mapping != null) {
					instrumentMarketDataFieldMappingMap.put(security.getInstrument(), mapping);
				}
			}
			// skip excluded instruments
			if (mapping != null) {
				if (marketDataFieldValueUpdaterCommand.isSecuritySkipped(security, marketDataFieldValueUpdaterCommand.getStartDate())) {
					LogUtils.info(LogCommand.ofMessage(getClass(), "Skipping Security: ", security.getLabel(), " No data fields with externalFieldName found for group: ", mapping.getFieldGroup().getId()));
					command.getStatus().addSkipped(security.getUniqueLabel());
					command.addSkippedSecurity(security);
					continue;
				}

				List<MarketDataField> fieldList = getMarketDataFieldList(command, fieldGroupMarketDataFieldMap, mapping.getFieldGroup().getId());
				if (CollectionUtils.isEmpty(fieldList)) {
					// NOTE: "Empty Group" is valid
					LogUtils.info(LogCommand.ofMessage(getClass(), "No data fields with externalFieldName found for group: ", mapping.getFieldGroup().getId()));
					command.getStatus().addSkipped(security.getUniqueLabel() + " has no data fields in group " + mapping.getFieldGroup().getId());
					command.addSkippedSecurity(security);
					continue;
				}
				// make a copy to avoid removing fields for all securities
				List<MarketDataField> fieldsToRequest = new ArrayList<>(fieldList);

				MarketDataFieldRequestCollection marketDataFieldRequestCollection = new MarketDataFieldRequestCollection(security, fieldsToRequest, marketDataFieldValueUpdaterCommand);
				if (isSkipIfExistingDataPresent(marketDataFieldRequestCollection)) {
					LogUtils.info(LogCommand.ofMessage(getClass(), security.getUniqueLabel(), " skipped because existing data is present"));
					command.getStatus().addSkipped(security.getUniqueLabel() + " skipped because existing data is present");
					command.addSkippedSecurity(security);
					continue;
				}

				Set<MarketDataField> existingFields = marketDataFieldMappingFieldSetMap.computeIfAbsent(mapping, k -> new HashSet<>());
				existingFields.addAll(fieldsToRequest);

				List<InvestmentSecurity> existingSecurities = marketDataFieldMappingSecurityListMap.computeIfAbsent(mapping, k -> new ArrayList<>());
				existingSecurities.add(security);
			}
		}
	}


	/**
	 * Gets the list of {@link MarketDataValue}s while optimizing the data requested. A request will be sent for each {@link InvestmentSecurity}.
	 * <p>
	 * Creates the optimal number of requests such that data that already exists in the system will not be queried again.
	 */
	private List<MarketDataValue> doUpdateMarketDataFieldValuesSynchronousWithSkip(MarketDataFieldHistoricalUpdateCommand command) {
		List<MarketDataValue> values = new ArrayList<>();

		Map<Short, List<MarketDataField>> fieldMap = new HashMap<>();
		int totalSecurities = CollectionUtils.getSize(command.getSecurityList());
		MarketDataFieldValueUpdaterCommand marketDataFieldValueUpdaterCommand = command.getMarketDataUpdaterCommand();
		Integer marketDataFieldProviderOverride = marketDataFieldValueUpdaterCommand.getMarketDataProviderTimeoutOverride();
		String statusMessage = "Ready to start data retrieval for " + totalSecurities + " securities.";
		for (InvestmentSecurity security : CollectionUtils.getIterable(command.getSecurityList())) {
			MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), command.getDataSource());
			// skip excluded instruments
			if (mapping != null) {
				List<MarketDataField> fieldList = getMarketDataFieldList(command, fieldMap, mapping.getFieldGroup().getId());
				if (CollectionUtils.isEmpty(fieldList)) {
					// NOTE: "Empty Group" is valid
					LogUtils.info(LogCommand.ofMessage(getClass(), "Skipping Security: ", security.getLabel(), " No data fields with externalFieldName found for group: ", mapping.getFieldGroup().getId()));
					command.getStatus().addSkipped(security.getUniqueLabel() + " has not data fields in group " + mapping.getFieldGroup().getId());
					command.addSkippedSecurity(security);
					continue;
				}
				// make a copy to avoid removing fields for all securities
				fieldList = new ArrayList<>(fieldList);

				if (marketDataFieldValueUpdaterCommand.isSecuritySkipped(security, marketDataFieldValueUpdaterCommand.getStartDate())) {
					LogUtils.info(LogCommand.ofMessage(getClass(), security.getUniqueLabel(), " skipped due to start date"));
					command.getStatus().addSkipped(security.getUniqueLabel());
					command.addSkippedSecurity(security);
					continue;
				}

				MarketDataFieldRequestCollection marketDataFieldRequestCollection = new MarketDataFieldRequestCollection(security, fieldList, marketDataFieldValueUpdaterCommand);
				if (isSkipIfExistingDataPresent(marketDataFieldRequestCollection)) {
					LogUtils.info(LogCommand.ofMessage(getClass(), security.getUniqueLabel(), " skipped because existing data is present"));
					command.getStatus().addSkipped(security.getUniqueLabel() + " skipped because existing data is present");
					command.addSkippedSecurity(security);
					continue;
				}

				List<MarketDataValue> dataValuesFromProvider = new ArrayList<>();
				String marketSector = mapping.getMarketSector().getName();
				String pricingSource = mapping.getPricingSource() != null ? mapping.getPricingSource().getSymbol() : null;
				String exchangeCode = mapping.getExchangeCodeType() != null ? mapping.getExchangeCodeType().getSecurityExchangeCode(security) : null;
				try {
					// get LATEST data if start and end dates are null
					if (marketDataFieldValueUpdaterCommand.getStartDate() == null && marketDataFieldValueUpdaterCommand.getEndDate() == null) {
						LogUtils.info(LogCommand.ofMessage(getClass(), "Requesting latest value for security: ", security.getUniqueLabel()));

						List<String> errorList = new ArrayList<>();
						DataFieldValueCommand fieldValueCommand = DataFieldValueCommand.newBuilder()
								.addSecurity(security, fieldList)
								.setMarketSector(marketSector)
								.setPricingSource(pricingSource)
								.setExchangeCode(exchangeCode)
								.setErrors(errorList)
								.withProviderOverride(marketDataFieldValueUpdaterCommand.getMarketDataProviderOverride())
								.setProviderTimeoutOverride(marketDataFieldProviderOverride)
								.setConsecutiveErrorCountDisabled(marketDataFieldValueUpdaterCommand.isConsecutiveErrorCountDisabled())
								.setRetryCountOverride(marketDataFieldValueUpdaterCommand.getRetryCountOverride())
								.build();
						DataFieldValueResponse<MarketDataValue> fieldValueResponse = command.getDataProvider().getMarketDataValueLatest(fieldValueCommand);
						dataValuesFromProvider.addAll(fieldValueResponse.getMarketDataValueListForSingleSecurityStrict());
						command.getStatus().addErrors(errorList);
						command.getErrorCount().addAndGet(errorList.size());
					}
					else {
						for (MarketDataFieldRequest request : CollectionUtils.getIterable(marketDataFieldRequestCollection.populateRequests())) {
							List<String> errorList = new ArrayList<>();
							DataFieldValueCommand fieldValueCommand = DataFieldValueCommand.newBuilder()
									.addSecurity(request.getSecurity(), request.getFieldList())
									.setMarketSector(marketSector)
									.setPricingSource(pricingSource)
									.setExchangeCodeType(mapping.getExchangeCodeType())
									.setStartDate(DateUtils.asUtilDate(request.getStartDate()))
									.setEndDate(DateUtils.asUtilDate(request.getEndDate()))
									.withProviderOverride(marketDataFieldValueUpdaterCommand.getMarketDataProviderOverride())
									.setProviderTimeoutOverride(marketDataFieldProviderOverride)
									.setConsecutiveErrorCountDisabled(marketDataFieldValueUpdaterCommand.isConsecutiveErrorCountDisabled())
									.setRetryCountOverride(marketDataFieldValueUpdaterCommand.getRetryCountOverride())
									.build();
							dataValuesFromProvider.addAll(
									CollectionUtils.asNonNullList(
											command.getDataProvider().getMarketDataValueListHistorical(fieldValueCommand, errorList)
									)
							);
							command.getStatus().addErrors(errorList);
							command.getErrorCount().addAndGet(errorList.size());
						}
					}
				}
				catch (Throwable e) {
					// record the error but continue processing
					command.getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					command.getErrorCount().incrementAndGet();
				}

				if (CollectionUtils.isEmpty(dataValuesFromProvider)) {
					command.getStatus().addMessage(security.getUniqueLabel() + " no values returned");
				}
				else {
					values.addAll(dataValuesFromProvider);
				}

				command.getProcessedSecurities().incrementAndGet();
				statusMessage = "Securities: " + totalSecurities + "; Processed: " + command.getProcessedSecurities().intValue() + "; Skipped: " + command.getSkippedSecurityList().size() + "; Values Updated: " + command.getRecordsUpdated().intValue() + "; Values Skipped: " + command.getRecordsSkipped().intValue() + "; Failed: " + command.getErrorCount().intValue() + "; Last: " + security.getSymbol();
				command.getStatus().setMessage(statusMessage);
			}
		}

		if (command.getProcessedSecurities().intValue() == 0) {
			statusMessage = "Securities: " + totalSecurities + "; Processed: " + command.getProcessedSecurities().intValue() + "; Skipped: " + command.getSkippedSecurityList().size() + "; Values Updated: " + command.getRecordsUpdated().intValue() + "; Values Skipped: " + command.getRecordsSkipped().intValue() + "; Failed: " + command.getErrorCount().intValue();
		}
		command.getStatus().setMessage(statusMessage);

		return values;
	}


	/**
	 * If fieldList has more than 1 field, it is possible that some fields have data while others do not.  In this case, the method will remove
	 * fields with the data from the argument fieldList to avoid duplicate lookups.
	 */
	private boolean isSkipIfExistingDataPresent(MarketDataFieldRequestCollection marketDataFieldRequests) {
		if (!marketDataFieldRequests.isSkipIfAlreadyExists()) {
			// skipping not requested: do not skip
			return false;
		}
		// the calendar for the security
		Calendar calendar = getInvestmentCalculator().getInvestmentSecurityCalendar(marketDataFieldRequests.getSecurity());
		// make sure the value for each field exists
		boolean skip = true;
		// 'retrieve latest' does not set start or end dates, use now(), adjusted based on previous workday flag.
		LocalDate toDate = getEndDate(marketDataFieldRequests, calendar);
		// get the data source ID from the name.
		Short dataSourceId = getDataSourceFromDB(marketDataFieldRequests.getDataSourceName()).getId();
		// go through all available fields.
		for (MarketDataField field : CollectionUtils.getIterable(marketDataFieldRequests.getFields())) {
			// reset start date on each iteration
			LocalDate fromDate = getStartDate(marketDataFieldRequests, calendar);
			// make sure the field value for each day exists
			while (!fromDate.isAfter(toDate)) {
				// convert to util date
				Date tmpFromDate = DateUtils.asUtilDate(fromDate);
				// skip weekends and holidays
				if (DateUtils.isWeekday(tmpFromDate)) {
					String fieldName = (field.getValueOverrideDataField() == null) ? field.getName() : field.getValueOverrideDataField().getName();
					MarketDataValueHolder value = getMarketDataFieldService()
							.getMarketDataValueForDate(marketDataFieldRequests.getInvestmentSecurity(), tmpFromDate, fieldName, dataSourceId, false, false);
					// fetch data when its missing OR the day of week is not the skip day (if specified)
					if (value == null || !marketDataFieldRequests.calculateSkipIfAlreadyExists(tmpFromDate, value.getMeasureTime())) {
						skip = false;
						// add missing field to aggregator.
						marketDataFieldRequests.addMissingFieldValue(field, fromDate);
					}
				}
				else {
					marketDataFieldRequests.addNonBusinessDate(fromDate);
				}
				fromDate = fromDate.plus(1, ChronoUnit.DAYS);
			}
		}

		return skip;
	}


	/**
	 * get current date based on the 'Check to use Previous Weekday for Date stored in our system' configuration.
	 */
	private LocalDate getCurrentDate(boolean usePreviousWeekday, Calendar calendar) {
		if (usePreviousWeekday) {
			Date date = getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(new Date(), calendar.getId()));
			return DateUtils.asLocalDate(date);
		}
		else {
			return LocalDate.now();
		}
	}


	/**
	 * get start date based on the 'Check to use Previous Weekday for Date stored in our system' configuration.
	 */
	private LocalDate getStartDate(MarketDataFieldRequestCollection marketDataFieldRequests, Calendar calendar) {
		return marketDataFieldRequests.getStartDate() == null ? getCurrentDate(marketDataFieldRequests.isUsePreviousWeekday(), calendar) : marketDataFieldRequests.getStartDate();
	}


	/**
	 * get end date based on the 'Check to use Previous Weekday for Date stored in our system' configuration.
	 */
	private LocalDate getEndDate(MarketDataFieldRequestCollection marketDataFieldRequests, Calendar calendar) {
		return (marketDataFieldRequests.getEndDate() == null) ? getCurrentDate(marketDataFieldRequests.isUsePreviousWeekday(), calendar) : marketDataFieldRequests.getEndDate();
	}


	/**
	 * Returns the MarketDataSource object for the specified name or default data source if the name is empty or null.
	 */
	private MarketDataSource getDataSourceFromDB(String dataSourceName) {
		if (StringUtils.isEmpty(dataSourceName)) {
			dataSourceName = getMarketDataProviderLocator().getDefaultDataSourceName();
		}
		MarketDataSource result = getMarketDataSourceService().getMarketDataSourceByName(dataSourceName);
		if (result == null) {
			throw new RuntimeException("No MarketDataSource found for name " + getMarketDataProviderLocator().getDefaultDataSourceName() + ", please create one.");
		}

		return result;
	}


	/**
	 * Private class to allow reuse of common elements between the batching and skipping historical updating methods
	 */
	private class MarketDataFieldHistoricalUpdateCommand {

		private final MarketDataFieldValueUpdaterCommand marketDataUpdaterCommand;
		private final MarketDataSource dataSource;
		private final MarketDataProvider<MarketDataValue> dataProvider;
		private final List<InvestmentSecurity> securityList;
		private final List<MarketDataField> argumentFieldList;
		private final Status status;
		private final List<InvestmentSecurity> skippedSecurityList = new ArrayList<>();

		private final AtomicInteger errorCount = new AtomicInteger(0);
		private final AtomicInteger processedSecurities = new AtomicInteger(0);
		private final AtomicInteger recordsUpdated = new AtomicInteger(0);
		private final AtomicInteger recordsSkipped = new AtomicInteger(0);


		public MarketDataFieldHistoricalUpdateCommand(MarketDataFieldValueUpdaterCommand marketDataUpdaterCommand, MarketDataSource dataSource, MarketDataProvider<MarketDataValue> dataProvider, List<InvestmentSecurity> securityList, List<MarketDataField> argumentFieldList, Status status) {
			this.marketDataUpdaterCommand = marketDataUpdaterCommand;
			this.dataSource = dataSource;
			this.dataProvider = dataProvider;
			this.securityList = securityList;
			this.argumentFieldList = argumentFieldList;
			this.status = status;
		}


		public MarketDataFieldValueUpdaterCommand getMarketDataUpdaterCommand() {
			return this.marketDataUpdaterCommand;
		}


		public MarketDataSource getDataSource() {
			return this.dataSource;
		}


		public MarketDataProvider<MarketDataValue> getDataProvider() {
			return this.dataProvider;
		}


		public List<InvestmentSecurity> getSecurityList() {
			return this.securityList;
		}


		public List<MarketDataField> getArgumentFieldList() {
			return this.argumentFieldList;
		}


		public Status getStatus() {
			return this.status;
		}


		public List<InvestmentSecurity> getSkippedSecurityList() {
			return this.skippedSecurityList;
		}


		public void addSkippedSecurity(InvestmentSecurity security) {
			this.skippedSecurityList.add(security);
		}


		public AtomicInteger getErrorCount() {
			return this.errorCount;
		}


		public AtomicInteger getProcessedSecurities() {
			return this.processedSecurities;
		}


		public AtomicInteger getRecordsUpdated() {
			return this.recordsUpdated;
		}


		public AtomicInteger getRecordsSkipped() {
			return this.recordsSkipped;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////     Security Allocation Market Data Rebuild Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String rebuildMarketDataAllocatedValueForSecurity(int securityId, short dataSourceId, Date startDate, Date endDate, boolean asynchronous) {
		final InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		if (!security.isAllocationOfSecuritiesSystemCalculated()) {
			throw new ValidationException("Rebuild market data is only allowed for securities that are allocations of other securities and support system calculated market data.");
		}
		if (security.getStartDate() == null) {
			throw new ValidationException("Please enter a start date for this security in order to rebuild.");
		}

		Date overrideStartDate = (Date) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_ALLOCATED_SECURITY_CALCULATION_START_DATE, false);
		if (overrideStartDate != null) {
			ValidationUtils.assertTrue(DateUtils.compare(overrideStartDate, security.getStartDate(), false) > 0, "Security Allocation Calculation Start Date, when used, must come after Security start date.  If the same as the start date, then leave blank.");
		}

		if (startDate == null) {
			startDate = security.getStartDate();
		}
		if (overrideStartDate != null && DateUtils.compare(overrideStartDate, startDate, false) > 0) {
			startDate = overrideStartDate;
		}
		if (endDate == null || DateUtils.compare(endDate, new Date(), false) > 0) {
			endDate = new Date();
		}
		// If full rebuild, (rebuilding from startDate)
		if (DateUtils.compare(startDate, security.getStartDate(), false) == 0) {
			// Delete any market data that may have been previously entered if the start date was moved forward...
			MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm(true);
			searchForm.setInvestmentSecurityId(securityId);
			searchForm.setMaxMeasureDate(security.getStartDate());
			List<MarketDataValue> previousValueList = getMarketDataFieldService().getMarketDataValueList(searchForm);
			getMarketDataFieldService().deleteMarketDataValueList(previousValueList);

			// Delete any market data that is tied to a non business day (holiday) in case the calendar changed
			CalendarHolidayDaySearchForm holidayDaySearchForm = new CalendarHolidayDaySearchForm();
			holidayDaySearchForm.setCalendarId(getInvestmentCalculator().getInvestmentSecurityCalendar(security).getId());
			holidayDaySearchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, startDate));
			holidayDaySearchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN_OR_EQUALS, endDate));
			List<CalendarHolidayDay> holidayList = getCalendarHolidayService().getCalendarHolidayDayList(holidayDaySearchForm);
			if (!CollectionUtils.isEmpty(holidayList)) {
				MarketDataValueSearchForm holidayMarketDataSearchForm = new MarketDataValueSearchForm(true);
				holidayMarketDataSearchForm.setInvestmentSecurityId(securityId);
				holidayMarketDataSearchForm.setMeasureDates(BeanUtils.getPropertyValues(holidayList, "day.startDate", Date.class));
				List<MarketDataValue> holidayValueList = getMarketDataFieldService().getMarketDataValueList(holidayMarketDataSearchForm);
				getMarketDataFieldService().deleteMarketDataValueList(holidayValueList);
			}
		}
		return rebuildMarketDataAllocatedValueForSecurityImpl(security, dataSourceId, startDate, endDate, asynchronous);
	}


	private String rebuildMarketDataAllocatedValueForSecurityImpl(final InvestmentSecurity security, final Short dataSourceId, final Date startDate, final Date endDate, boolean asynchronous) {
		if (asynchronous) {
			// asynchronous support
			String runId = security.getId() + "_" + DateUtils.fromDateShort(startDate) + "_" + DateUtils.fromDateShort(endDate);
			final Date scheduledDate = new Date();

			Runner runner = new AbstractStatusAwareRunner("MARKET-DATA-ALLOCATED-VALUE", runId, scheduledDate) {

				@Override
				public void run() {
					String result = "Started rebuilding " + security.getSymbol() + " market data for date range " + DateUtils.fromDateRange(startDate, endDate, true);
					getStatus().setMessage(result);
					try {
						result = getMarketDataInvestmentSecurityAllocationService().calculateInvestmentSecurityAllocationMarketDataValues(CollectionUtils.createList(security), dataSourceId, startDate, endDate, null).getMessage();
					}
					catch (Exception e) {
						result = "Error rebuilding " + security.getSymbol() + " market data for date range " + DateUtils.fromDateRange(startDate, endDate, true) + ": " + e.getMessage();
						getStatus().addError(result);
						throw e;
					}
					finally {
						getStatus().setMessage(result);
					}
				}
			};
			getRunnerHandler().rescheduleRunner(runner);
			return "Market Data Value Rebuild has started and will complete shortly.";
		}
		return getMarketDataInvestmentSecurityAllocationService().calculateInvestmentSecurityAllocationMarketDataValues(CollectionUtils.createList(security), dataSourceId, startDate, endDate, null).getMessage();
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public CalendarHolidayService getCalendarHolidayService() {
		return this.calendarHolidayService;
	}


	public void setCalendarHolidayService(CalendarHolidayService calendarHolidayService) {
		this.calendarHolidayService = calendarHolidayService;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataInvestmentSecurityAllocationService getMarketDataInvestmentSecurityAllocationService() {
		return this.marketDataInvestmentSecurityAllocationService;
	}


	public void setMarketDataInvestmentSecurityAllocationService(MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService) {
		this.marketDataInvestmentSecurityAllocationService = marketDataInvestmentSecurityAllocationService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public SecurityImpersonationHandler getSecurityImpersonationHandler() {
		return this.securityImpersonationHandler;
	}


	public void setSecurityImpersonationHandler(SecurityImpersonationHandler securityImpersonationHandler) {
		this.securityImpersonationHandler = securityImpersonationHandler;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
