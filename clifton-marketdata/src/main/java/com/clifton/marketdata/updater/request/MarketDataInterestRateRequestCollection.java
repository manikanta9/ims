package com.clifton.marketdata.updater.request;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>MarketDataInterestRateRequestCollection</code> builds {@link MarketDataInterestRateRequest}
 * objects by consolidating requests by expanding the date period.
 * <p>
 * Assumptions:
 * Date periods may include weekends and holidays; these days do not count towards
 * query limits.
 */
public class MarketDataInterestRateRequestCollection {

	private MarketDataInterestRateIndexMapping mapping;
	private LocalDate startDate;
	private LocalDate endDate;
	private boolean skipIfAlreadyExists;

	private Set<LocalDate> nonBusinessDates = new HashSet<>();
	private List<LocalDate> missingDates = new ArrayList<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataInterestRateRequestCollection(MarketDataInterestRateIndexMapping mapping, LocalDate startDate, LocalDate endDate, boolean skipIfAlreadyExists) {
		ValidationUtils.assertNotNull(mapping, "The mapping index is required.");
		ValidationUtils.assertNotNull(startDate, "The start date is required.");
		ValidationUtils.assertNotNull(endDate, "The end date is required.");

		this.mapping = mapping;
		this.startDate = startDate;
		this.endDate = endDate;
		this.skipIfAlreadyExists = skipIfAlreadyExists;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void missingDate(LocalDate date) {
		this.missingDates.add(date);
	}


	public void addNonBusinessDate(LocalDate date) {
		this.nonBusinessDates.add(date);
	}


	public List<MarketDataInterestRateRequest> populateRequests() {
		final List<MarketDataInterestRateRequest> requests = new ArrayList<>();

		// return immediately if skipping is not turned on, one request for everything.
		if (!this.skipIfAlreadyExists) {
			requests.add(new MarketDataInterestRateRequest(this.mapping, this.startDate, this.endDate));
			return requests;
		}

		if (!this.missingDates.isEmpty()) {
			// get all missing field value dates.
			ArrayList<LocalDate> periodDates = new ArrayList<>(this.missingDates);
			Collections.sort(periodDates);
			// first day with missing field values
			LocalDate firstDate = CollectionUtils.getFirstElementStrict(periodDates);
			// add weekends and holidays.
			periodDates.addAll(this.nonBusinessDates);
			Collections.sort(periodDates);
			// add first request
			requests.add(new MarketDataInterestRateRequest(this.mapping, firstDate, firstDate));
			for (LocalDate currentDate : periodDates) {
				// the first date was already processed, go to the next.
				if (currentDate.isAfter(firstDate)) {
					MarketDataInterestRateRequest previousRequest = getLastElement(requests);
					if (previousRequest != null) {
						// a date with a missing value.
						if (this.missingDates.contains(currentDate)) {
							if (previousRequest.canExtend(currentDate)) {
								// date is sequential, extend ending date.
								previousRequest.extendEndingDate(currentDate);
							}
							else {
								// date is not sequential, create new request
								requests.add(new MarketDataInterestRateRequest(this.mapping, currentDate, currentDate));
							}
						}
						else if (previousRequest.isSequential(currentDate)) {
							// this is a non-business day and is sequential with previous request, extend it.
							// else case is skipped, non-sequential non-business days can be skipped.
							previousRequest.extendEndingDate(currentDate);
						}
					}
				}
			}
		}

		return requests;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <E> E getLastElement(List<E> list) {
		return (!CollectionUtils.isEmpty(list)) ? list.get(list.size() - 1) : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LocalDate getStartDate() {
		return this.startDate;
	}


	public LocalDate getEndDate() {
		return this.endDate;
	}
}
