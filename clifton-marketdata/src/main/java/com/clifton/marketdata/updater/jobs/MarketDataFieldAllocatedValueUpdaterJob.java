package com.clifton.marketdata.updater.jobs;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.investment.setup.group.InvestmentGroupService;
import com.clifton.investment.setup.group.InvestmentSecurityGroupService;
import com.clifton.marketdata.instrument.allocation.MarketDataInvestmentSecurityAllocationService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>MarketDataFieldValueAllocatedSecuritiesUpdaterJob</code> updates
 * MarketDataValues for securities that are allocations of other securities.
 *
 * @author Mary Anderson
 */
public class MarketDataFieldAllocatedValueUpdaterJob implements Task {


	private InvestmentInstrumentService investmentInstrumentService;
	private InvestmentGroupService investmentGroupService;
	private InvestmentSecurityGroupService investmentSecurityGroupService;

	private MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * For daily price rebuilds
	 */
	private int daysBackFromToday;

	/**
	 * For month end value rebuilds - go back further (10 days) so within 10 days after month end
	 * all data should be entered and market data rebuilt.
	 */
	private int monthEndDaysBackFromToday;

	/**
	 * Ability to exclude specific group of instruments from calculating market data
	 */
	private Short excludeInvestmentGroupId;


	/**
	 * Customized group of allocated securities.  These securities may be dependent on securities that are only priced monthly so we'll want to
	 * run them for the past 15 days since prices may not be available until then.
	 */
	private Short customInvestmentSecurityGroupId;

	private int customDaysBackFromToday = 0;

	/**
	 * Because a lot of the allocated securities depend on benchmarks which now allow for flexible price look ups
	 * because historically they weren't priced daily - some securities are being built for "today" using "yesterdays" prices.
	 * For the batch job, will just skip today as these run in the morning and we wouldn't have today's prices yet.
	 */
	private boolean includeTodayInRebuild;

	/**
	 * Datasource to use when saving the calculated prices.
	 * Defaults to Parametric Clifton
	 */
	private Short dataSourceId;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Date endDate = DateUtils.getEndOfDay(new Date());
		Date dailyStartDate = DateUtils.clearTime(DateUtils.addDays(endDate, getDaysBackFromToday() * -1));
		Date monthlyStartDate = DateUtils.clearTime(DateUtils.addDays(endDate, getMonthEndDaysBackFromToday() * -1));
		Date customStartDate = DateUtils.clearTime(DateUtils.addDays(endDate, getCustomDaysBackFromToday() * -1));
		if (!isIncludeTodayInRebuild()) {
			endDate = DateUtils.addDays(endDate, -1);
		}

		List<InvestmentSecurity> customSecurityGroupList = (getCustomInvestmentSecurityGroupId() == null ? null : getInvestmentSecurityGroupService().getInvestmentSecurityListForSecurityGroup(getCustomInvestmentSecurityGroupId(), null, null));
		InvestmentGroup excludeGroup = (getExcludeInvestmentGroupId() != null ? getInvestmentGroupService().getInvestmentGroup(getExcludeInvestmentGroupId()) : null);

		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setAllocationOfSecurities(true);
		List<InvestmentSecurity> securityList = getInvestmentInstrumentService().getInvestmentSecurityList(searchForm);
		// Filter Out Any Securities that are allocations, but not system calculated (not a real field so can't be included in the search form)
		securityList = BeanUtils.filter(securityList, InvestmentSecurity::isAllocationOfSecuritiesSystemCalculated);

		List<InvestmentSecurity> dailySecurityList = new ArrayList<>();
		List<InvestmentSecurity> monthlySecurityList = new ArrayList<>();
		List<InvestmentSecurity> customList = new ArrayList<>();

		Status status = Status.ofMessage("Processing " + CollectionUtils.getSize(securityList) + " securities.");
		for (InvestmentSecurity security : CollectionUtils.getIterable(securityList)) {
			if (excludeGroup == null || !getInvestmentGroupService().isInvestmentInstrumentInGroup(excludeGroup.getName(), security.getInstrument().getId())) {
				if (customSecurityGroupList != null && customSecurityGroupList.contains(security)) {
					customList.add(security);
				}
				else if (security.getInstrument().getHierarchy().getSecurityAllocationType().getCalculatedFieldType().isMonthEndOnly()) {
					monthlySecurityList.add(security);
				}
				else {
					dailySecurityList.add(security);
				}
			}
		}

		calculateInvestmentSecurityAllocationMarketDataValues(dailySecurityList, dailyStartDate, endDate, status);
		calculateInvestmentSecurityAllocationMarketDataValues(monthlySecurityList, monthlyStartDate, endDate, status);
		calculateInvestmentSecurityAllocationMarketDataValues(customList, customStartDate, endDate, status);

		return status;
	}


	private void calculateInvestmentSecurityAllocationMarketDataValues(List<InvestmentSecurity> securityList, Date startDate, Date endDate, Status status) {
		if (!CollectionUtils.isEmpty(securityList)) {
			getMarketDataInvestmentSecurityAllocationService().calculateInvestmentSecurityAllocationMarketDataValues(securityList, getDataSourceId(), startDate, endDate, status);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public int getDaysBackFromToday() {
		return this.daysBackFromToday;
	}


	public void setDaysBackFromToday(int daysBackFromToday) {
		this.daysBackFromToday = daysBackFromToday;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public InvestmentGroupService getInvestmentGroupService() {
		return this.investmentGroupService;
	}


	public void setInvestmentGroupService(InvestmentGroupService investmentGroupService) {
		this.investmentGroupService = investmentGroupService;
	}


	public InvestmentSecurityGroupService getInvestmentSecurityGroupService() {
		return this.investmentSecurityGroupService;
	}


	public void setInvestmentSecurityGroupService(InvestmentSecurityGroupService investmentSecurityGroupService) {
		this.investmentSecurityGroupService = investmentSecurityGroupService;
	}


	public MarketDataInvestmentSecurityAllocationService getMarketDataInvestmentSecurityAllocationService() {
		return this.marketDataInvestmentSecurityAllocationService;
	}


	public void setMarketDataInvestmentSecurityAllocationService(MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService) {
		this.marketDataInvestmentSecurityAllocationService = marketDataInvestmentSecurityAllocationService;
	}


	public int getMonthEndDaysBackFromToday() {
		return this.monthEndDaysBackFromToday;
	}


	public void setMonthEndDaysBackFromToday(int monthEndDaysBackFromToday) {
		this.monthEndDaysBackFromToday = monthEndDaysBackFromToday;
	}


	public boolean isIncludeTodayInRebuild() {
		return this.includeTodayInRebuild;
	}


	public void setIncludeTodayInRebuild(boolean includeTodayInRebuild) {
		this.includeTodayInRebuild = includeTodayInRebuild;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Short getCustomInvestmentSecurityGroupId() {
		return this.customInvestmentSecurityGroupId;
	}


	public void setCustomInvestmentSecurityGroupId(Short customInvestmentSecurityGroupId) {
		this.customInvestmentSecurityGroupId = customInvestmentSecurityGroupId;
	}


	public int getCustomDaysBackFromToday() {
		return this.customDaysBackFromToday;
	}


	public void setCustomDaysBackFromToday(int customDaysBackFromToday) {
		this.customDaysBackFromToday = customDaysBackFromToday;
	}


	public Short getExcludeInvestmentGroupId() {
		return this.excludeInvestmentGroupId;
	}


	public void setExcludeInvestmentGroupId(Short excludeInvestmentGroupId) {
		this.excludeInvestmentGroupId = excludeInvestmentGroupId;
	}
}
