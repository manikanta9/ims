package com.clifton.marketdata.updater.security;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;


@NonPersistentObject
public class MarketDataInvestmentSecurityFieldUpdateResult {

	private String id;

	private InvestmentSecurity security;
	private MarketDataFieldColumnMapping dataFieldColumnMapping;

	private String originalText;
	private String originalValue;
	private String newText;
	private String newValue;

	private boolean saved;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isValueDifferent() {
		return StringUtils.compare(this.newValue, this.originalValue) != 0;
	}


	public String getOriginalText() {
		return this.originalText;
	}


	public void setOriginalText(String originalText) {
		this.originalText = originalText;
	}


	public String getOriginalValue() {
		return this.originalValue;
	}


	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}


	public String getNewText() {
		return this.newText;
	}


	public void setNewText(String newText) {
		this.newText = newText;
	}


	public String getNewValue() {
		return this.newValue;
	}


	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}


	public MarketDataFieldColumnMapping getDataFieldColumnMapping() {
		return this.dataFieldColumnMapping;
	}


	public void setDataFieldColumnMapping(MarketDataFieldColumnMapping dataFieldColumnMapping) {
		this.dataFieldColumnMapping = dataFieldColumnMapping;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public boolean isSaved() {
		return this.saved;
	}


	public void setSaved(boolean saved) {
		this.saved = saved;
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}
}
