package com.clifton.marketdata.updater.security.jobs;


import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityFieldUpdateResult;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityRetrieverCommand;
import com.clifton.marketdata.updater.security.MarketDataInvestmentSecurityService;

import java.util.List;
import java.util.Map;


public class MarketDataInvestmentSecurityUpdaterJob implements Task {

	private String dataSourceName;

	/**
	 * The investment group used to get the list of securities that will be updated.
	 */
	private Short investmentGroupId;
	/**
	 * The data field group used to get the list of fields to update.
	 */
	private Short dataFieldGroupId;
	/**
	 * Indicates if inactive securities should be updated.
	 */
	private Boolean updateInactiveSecurities;

	private MarketDataInvestmentSecurityService marketDataInvestmentSecurityService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		MarketDataInvestmentSecurityRetrieverCommand updateCommand = new MarketDataInvestmentSecurityRetrieverCommand();
		updateCommand.setMarketDataSourceName(getDataSourceName());
		updateCommand.setDataFieldGroupId(getDataFieldGroupId());
		updateCommand.setInvestmentGroupId(getInvestmentGroupId());
		updateCommand.setUpdateInactiveSecurities(getUpdateInactiveSecurities());

		List<MarketDataInvestmentSecurityFieldUpdateResult> updateResult = getMarketDataInvestmentSecurityService().updateMarketDataInvestmentSecurityFieldData(updateCommand);

		Status status = Status.ofMessage("Update count = " + updateResult.size() + ". Error count = " + updateCommand.getErrors().size());
		status.addErrors(updateCommand.getErrors());
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public Boolean getUpdateInactiveSecurities() {
		return this.updateInactiveSecurities;
	}


	public void setUpdateInactiveSecurities(Boolean updateInactiveSecurities) {
		this.updateInactiveSecurities = updateInactiveSecurities;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public MarketDataInvestmentSecurityService getMarketDataInvestmentSecurityService() {
		return this.marketDataInvestmentSecurityService;
	}


	public void setMarketDataInvestmentSecurityService(MarketDataInvestmentSecurityService marketDataInvestmentSecurityService) {
		this.marketDataInvestmentSecurityService = marketDataInvestmentSecurityService;
	}
}
