package com.clifton.marketdata.updater.security;

import com.clifton.core.beans.IdentityObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingCommand;

import java.util.List;
import java.util.Map;


/**
 * @author mwacker
 */
public interface MarketDataInvestmentSecurityUtilHandler {


	public <T extends IdentityObject> MarketDataFieldColumnMappingCommand<T> getDataFieldColumnMappingCommand(MarketDataInvestmentSecurityRetrieverCommand command, Class<T> clazz);


	public List<MarketDataFieldColumnMapping> getMappingObjectForUnmappedColumns(MarketDataInvestmentSecurityRetrieverCommand command, List<MarketDataFieldColumnMapping> mappings);


	public MarketDataInvestmentSecurityResult getSecurityMarketDataValues(MarketDataInvestmentSecurityRetrieverCommand command, MarketDataFieldColumnMappingHolder columnMappingHolder);


	public InvestmentInstrument applyInvestmentInstrumentMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, boolean applyIfNotNull, MarketDataInvestmentSecurityRetrieverCommand command);


	public InvestmentSecurity applyInvestmentSecurityMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, boolean applyIfNotNull, MarketDataInvestmentSecurityRetrieverCommand command);


	public String removeExchangeCodeFromSymbol(InvestmentSecurity security);
}
