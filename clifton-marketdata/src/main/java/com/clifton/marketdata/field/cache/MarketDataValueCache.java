package com.clifton.marketdata.field.cache;

import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;

import java.util.Date;


/**
 * The <code>MarketDataValueCache</code> class caches MarketDataValueHolder objects by date/field/security and optional DataSource
 * <p>
 * Note: Data Source is optional and can be used to restrict results to that datasource only - Some prices require a specific datasource
 * so if the request requires the datasource it will be cached that way, otherwise the cache won't include the data source id in the key
 *
 * @author vgomelsky
 */
public interface MarketDataValueCache {


	public MarketDataValueHolder getMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate);


	public void setMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate, MarketDataValue bean);
}
