package com.clifton.marketdata.field.upload.converter;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.upload.converter.SystemUploadBeanListConverterImpl;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>MarketDataValueMultipleConverter</code> performs additional conversion for customized
 * Market Data Value uploads where column names can be the data field name so that each row could potentially
 * result in multiple MarketDataValue records.
 *
 * @author manderson
 */
@Component
public class MarketDataValueMultipleConverter extends SystemUploadBeanListConverterImpl<MarketDataValue> {

	/**
	 * Returns a list of {@link MarketDataValue} objects that are a copy of the original bean
	 * and different data fields and values based on the unmapped property map
	 * <p>
	 * Note: Does not validate real data fields, just set the name property for look ups later
	 */
	@Override
	protected List<MarketDataValue> populateBeans(MarketDataValue bean, Map<String, Object> unmappedPropertyMap) {
		List<MarketDataValue> beanList = new ArrayList<>();

		for (Map.Entry<String, Object> stringObjectEntry : unmappedPropertyMap.entrySet()) {
			MarketDataValue copy = BeanUtils.cloneBean(bean, false, false);
			MarketDataField field = new MarketDataField();
			field.setName(stringObjectEntry.getKey());
			copy.setDataField(field);
			Object value = stringObjectEntry.getValue();
			if (value instanceof BigDecimal) {
				copy.setMeasureValue((BigDecimal) value);
			}
			else {
				throw new ValidationException("Cannot set column [" + stringObjectEntry.getKey() + "] as a market data value because value [" + value + "] is not entered as a number.");
			}

			beanList.add(copy);
		}

		return beanList;
	}
}
