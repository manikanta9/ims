package com.clifton.marketdata.field.mapping.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataFieldValueMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "columnMappingList.column.id", searchFieldPath = "dataField", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer columnId;

	@SearchField(searchField = "dataField.id")
	private Short dataFieldId;

	@SearchField(searchField = "name", searchFieldPath = "dataField")
	private String dataFieldName;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchField = "name", searchFieldPath = "dataSource")
	private String dataSourceName;

	@SearchField
	private String fromValue;
	@SearchField(searchField = "fromValue", comparisonConditions = ComparisonConditions.EQUALS)
	private String fromValueEquals;

	@SearchField
	private String toValue;

	@SearchField
	private String toValueLabel;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getDataFieldId() {
		return this.dataFieldId;
	}


	public void setDataFieldId(Short dataFieldId) {
		this.dataFieldId = dataFieldId;
	}


	public String getDataFieldName() {
		return this.dataFieldName;
	}


	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getFromValue() {
		return this.fromValue;
	}


	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}


	public String getFromValueEquals() {
		return this.fromValueEquals;
	}


	public void setFromValueEquals(String fromValueEquals) {
		this.fromValueEquals = fromValueEquals;
	}


	public String getToValue() {
		return this.toValue;
	}


	public void setToValue(String toValue) {
		this.toValue = toValue;
	}


	public String getToValueLabel() {
		return this.toValueLabel;
	}


	public void setToValueLabel(String toValueLabel) {
		this.toValueLabel = toValueLabel;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}
}
