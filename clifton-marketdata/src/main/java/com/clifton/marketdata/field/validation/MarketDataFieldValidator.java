package com.clifton.marketdata.field.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.CoreValidationUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>MarketDataFieldValidator</code> validates on updates that if the field
 * belongs to at least one group that requires fieldOrder, than fieldOrder is populated
 * Note: Only validated on UPDATES where field order is cleared - Inserts wouldn't be used by any groups at that time
 *
 * @author manderson
 */
@Component
public class MarketDataFieldValidator extends SelfRegisteringDaoValidator<MarketDataField> {

	private MarketDataFieldService marketDataFieldService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(MarketDataField bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Market Data Fields is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Market Data Fields is not allowed.");
			}
		}
		if (config.isUpdate()) {
			MarketDataField originalBean = getOriginalBean(bean);
			CoreValidationUtils.assertDisallowedModifiedFields(bean, originalBean, "systemDefined");
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "System Defined Market Data Field Names cannot be changed.", "name");
			}
			if (bean.getFieldOrder() == null && originalBean.getFieldOrder() != null) {
				List<MarketDataFieldGroup> groupList = getMarketDataFieldService().getMarketDataFieldGroupListByField(bean.getId());
				groupList = BeanUtils.filter(groupList, MarketDataFieldGroup::isFieldOrderRequired);
				if (!CollectionUtils.isEmpty(groupList)) {
					throw new FieldValidationException("Field Order cannot be cleared because the following group(s) that this field belongs to require field order to be populated ["
							+ BeanUtils.getPropertyValues(groupList, "name", ",") + "].", "fieldOrder");
				}
			}
		}

		if (bean.getLiveValueCalculator() != null && (config.isInsert() || config.isUpdate())) {
			ValidationUtils.assertNotNull(bean.getLiveValueCalculatorCondition(), "Market Data Fields with a Live Value Calculator require a Live Value Calculator Condition");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
