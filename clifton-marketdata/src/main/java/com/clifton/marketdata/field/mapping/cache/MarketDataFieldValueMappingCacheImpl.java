package com.clifton.marketdata.field.mapping.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.marketdata.field.mapping.MarketDataFieldValueMapping;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class MarketDataFieldValueMappingCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataFieldValueMapping, String, Map<String, String>> implements MarketDataFieldValueMappingCache {


	@Override
	public Map<String, String> getMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId) {
		return getCacheHandler().get(getCacheName(), getBeanKey(dataFieldId, dataSourceId));
	}


	@Override
	public void setMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId, Map<String, String> value) {
		getCacheHandler().put(getCacheName(), getBeanKey(dataFieldId, dataSourceId), value);
	}


	private String getBeanKey(short dataFieldId, short dataSourceId) {
		StringBuilder key = new StringBuilder(24);
		key.append(dataFieldId);
		key.append('-');
		key.append(dataSourceId);
		return key.toString();
	}
}
