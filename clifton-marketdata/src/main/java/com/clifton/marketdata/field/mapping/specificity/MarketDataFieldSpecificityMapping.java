package com.clifton.marketdata.field.mapping.specificity;

import com.clifton.investment.specificity.BaseInvestmentSpecificityBean;


/**
 * <code>MarketDataFieldSpecificityMapping</code> is an investment specificity bean definition responsible for mapping
 * a collection of {@link com.clifton.marketdata.field.MarketDataField}s to an investment specificity scope.
 *
 * @author NickK
 */
public class MarketDataFieldSpecificityMapping extends BaseInvestmentSpecificityBean {

	public static final String MARKET_DATA_FIELD_MAPPING_SPECIFICITY_DEFINITION_NAME = "Investment Instrument Market Data Field Mapping";
	public static final String SUBSCRIPTION_MARKET_DATA_FIELD_MAPPING_SPECIFICITY_FIELD_NAME = "Subscription Market Data Field List";

	/**
	 * An optional market data source to define the source for the fields.
	 */
	private Short marketDataSource;
	/**
	 * Multiple market data fields that are used for an investment specificity scope.
	 * These fields can be linked to an investment specificity field for retrieval.
	 */
	private Short[] marketDataFields;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(Short marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public Short[] getMarketDataFields() {
		return this.marketDataFields;
	}


	public void setMarketDataFields(Short[] marketDataFields) {
		this.marketDataFields = marketDataFields;
	}
}
