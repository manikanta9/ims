package com.clifton.marketdata.field.mapping.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingCommand;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldMappingSearchForm;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author theodorez
 */

@Component
public class MarketDataFieldMappingSpecificityCache extends AbstractSpecificityCache<MarketDataFieldMappingCommand, MarketDataFieldMapping, MarketDataFieldMappingTargetHolder> {

	private MarketDataFieldMappingService marketDataFieldMappingService;
	private InvestmentSetupService investmentSetupService;

	private final ClearSpecificityCacheUpdater<MarketDataFieldMappingTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(MarketDataFieldMapping.class, InvestmentInstrumentHierarchy.class);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	MarketDataFieldMappingSpecificityCache() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	@Override
	protected boolean isMatch(MarketDataFieldMappingCommand source, MarketDataFieldMappingTargetHolder holder) {
		return true;
	}


	@Override
	protected MarketDataFieldMapping getTarget(MarketDataFieldMappingTargetHolder targetHolder) {
		return getMarketDataFieldMappingService().getMarketDataFieldMapping(targetHolder.getId());
	}


	@Override
	protected Collection<MarketDataFieldMappingTargetHolder> getTargetHolders() {
		List<MarketDataFieldMappingTargetHolder> holders = new ArrayList<>();

		//Get all of the MarketDataFieldMappings and create target holder instances for them to be placed in the cache
		List<MarketDataFieldMapping> mappingList = getMarketDataFieldMappingService().getMarketDataFieldMappingList(new MarketDataFieldMappingSearchForm());

		for (MarketDataFieldMapping mapping : CollectionUtils.getIterable(mappingList)) {
			holders.addAll(buildMarketDataFieldMappingHolderList(mapping));
		}

		return holders;
	}


	private void recursivelyAddHierarchyMapping(MarketDataFieldMapping mapping, MarketDataSource source, InvestmentInstrumentHierarchy hierarchy, List<MarketDataFieldMappingTargetHolder> holders) {
		if (hierarchy != null) {
			InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
			searchForm.setParentId(hierarchy.getId());
			List<InvestmentInstrumentHierarchy> hierarchyList = getInvestmentSetupService().getInvestmentInstrumentHierarchyList(searchForm);
			for (InvestmentInstrumentHierarchy investmentInstrumentHierarchy : CollectionUtils.getIterable(hierarchyList)) {
				MarketDataFieldMappingSearchForm mappingSearchForm = new MarketDataFieldMappingSearchForm();
				mappingSearchForm.setDataSourceId(mapping.getMarketDataSource().getId());
				mappingSearchForm.setInstrumentHierarchyId(investmentInstrumentHierarchy.getId());
				mappingSearchForm.setLimit(1);
				List<MarketDataFieldMapping> mappingList = getMarketDataFieldMappingService().getMarketDataFieldMappingList(mappingSearchForm);
				if (CollectionUtils.isEmpty(mappingList)) {
					MarketDataFieldMapping newMapping = BeanUtils.cloneBean(mapping, false, true);
					newMapping.setInstrumentHierarchy(investmentInstrumentHierarchy);
					holders.add(buildTargetHolder(newMapping));
				}
				recursivelyAddHierarchyMapping(mapping, source, investmentInstrumentHierarchy, holders);
			}
		}
	}


	private MarketDataFieldMappingTargetHolder buildTargetHolder(MarketDataFieldMapping target) {
		return new MarketDataFieldMappingTargetHolder(generateKey(target), target);
	}


	private List<MarketDataFieldMappingTargetHolder> buildMarketDataFieldMappingHolderList(MarketDataFieldMapping mapping) {
		List<MarketDataFieldMappingTargetHolder> holders = new ArrayList<>();
		MarketDataFieldMappingTargetHolder holder = buildTargetHolder(mapping);
		holders.add(holder);
		if (mapping.getInstrumentHierarchy() != null) {
			recursivelyAddHierarchyMapping(mapping, null, mapping.getInstrumentHierarchy(), holders);
		}
		return holders;
	}


	private String generateKey(MarketDataFieldMapping marketDataFieldMapping) {
		Serializable investmentTypeId = BeanUtils.getBeanIdentity(marketDataFieldMapping.getInvestmentType());
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(marketDataFieldMapping.getInvestmentTypeSubType());
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(marketDataFieldMapping.getInvestmentTypeSubType2());

		Object[] instrumentHierarchy = {BeanUtils.getBeanIdentity(marketDataFieldMapping.getInstrument()), BeanUtils.getBeanIdentity(marketDataFieldMapping.getInstrumentHierarchy()), null};
		Object[] investmentTypeHierarchy = {investmentTypeId, null};
		// NOTE: SUB TYPE 2 is more specific than SUB TYPE 1
		Object[] investmentTypeSubType2Hierarchy = {investmentTypeSubType2Id, null};
		Object[] investmentTypeSubTypeHierarchy = {investmentTypeSubTypeId, null};

		Object[] valuesToAppend = {BeanUtils.getBeanIdentity(marketDataFieldMapping.getMarketDataSource())};

		return getKeyGenerator().generateKeyForTarget(SpecificityKeySource.builder(instrumentHierarchy)
				.addHierarchy(investmentTypeHierarchy)
				.addHierarchy(investmentTypeSubType2Hierarchy)
				.addHierarchy(investmentTypeSubTypeHierarchy)
				.setValuesToAppend(valuesToAppend)
				.build());
	}


	@Override
	protected SpecificityKeySource getKeySource(MarketDataFieldMappingCommand source) {
		Serializable investmentTypeId = source.getInvestmentTypeId();
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : source.getInvestmentTypeSubTypeId();
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : source.getInvestmentTypeSubType2Id();

		Object[] instrumentHierarchy = {source.getInstrumentId(), source.getInstrumentHierarchyId(), null};
		Object[] investmentTypeHierarchy = {investmentTypeId, null};
		// NOTE: SUB TYPE 2 is more specific than SUB TYPE 1
		Object[] investmentTypeSubType2Hierarchy = {investmentTypeSubType2Id, null};
		Object[] investmentTypeSubTypeHierarchy = {investmentTypeSubTypeId, null};

		Object[] valuesToAppend = {source.getMarketDataSourceId()};

		return SpecificityKeySource.builder(instrumentHierarchy)
				.addHierarchy(investmentTypeHierarchy)
				.addHierarchy(investmentTypeSubType2Hierarchy)
				.addHierarchy(investmentTypeSubTypeHierarchy)
				.setValuesToAppend(valuesToAppend)
				.build();
	}


	@Override
	protected SpecificityCacheUpdater<MarketDataFieldMappingTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}
}
