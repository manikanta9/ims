package com.clifton.marketdata.field.column.cache;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingCommand;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingSearchForm;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingService;
import com.clifton.marketdata.field.search.MarketDataFieldGroupSearchForm;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Component
public class MarketDataFieldColumnMappingSpecificityCache extends
		AbstractSpecificityCache<MarketDataFieldColumnMappingCommand<?>, MarketDataFieldColumnMapping, MarketDataFieldColumnMappingTargetHolder> {

	private MarketDataFieldColumnMappingService marketDataFieldColumnMappingService;
	private MarketDataFieldService marketDataFieldService;
	private InvestmentSetupService investmentSetupService;

	private final ClearSpecificityCacheUpdater<MarketDataFieldColumnMappingTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(MarketDataFieldColumnMapping.class, InvestmentInstrumentHierarchy.class);

	private SystemColumnService systemColumnService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	MarketDataFieldColumnMappingSpecificityCache() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	/**
	 * Overridden from the superclass to filter out template columns that do not exist at a given hierarchy
	 *
	 * @param source the source for which to return matching target(s)
	 */
	@Override
	public List<MarketDataFieldColumnMapping> getMostSpecificResultList(MarketDataFieldColumnMappingCommand<?> source) {
		List<MarketDataFieldColumnMapping> originalMappings = super.getMostSpecificResultList(source);

		if (source.getInstrumentHierarchyId() != null) {
			List<MarketDataFieldColumnMapping> filteredMappings = new ArrayList<>();
			SystemColumnSearchForm columnSearchForm = new SystemColumnSearchForm();
			columnSearchForm.setLinkedValue(source.getInstrumentHierarchyId().toString());
			columnSearchForm.setTemplateAssigned(Boolean.TRUE);
			List<SystemColumnCustom> customSecurityFields = getSystemColumnService().getSystemColumnCustomList(columnSearchForm);
			Set<String> customSecurityFieldNamesSet = new HashSet<>(Arrays.asList(BeanUtils.getPropertyValues(customSecurityFields, "name", String.class)));

			//Do not include the mapping if the template is not in the security fields
			for (MarketDataFieldColumnMapping mapping : CollectionUtils.getIterable(originalMappings)) {
				if (mapping.getTemplate() != null && !customSecurityFieldNamesSet.contains(mapping.getTemplate().getName())) {
					continue;
				}
				filteredMappings.add(mapping);
			}
			return filteredMappings;
		}

		return originalMappings;
	}


	@Override
	protected MarketDataFieldColumnMapping getTarget(MarketDataFieldColumnMappingTargetHolder targetHolder) {
		return getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMapping(targetHolder.getId());
	}


	@Override
	protected Collection<MarketDataFieldColumnMappingTargetHolder> getTargetHolders() {
		List<MarketDataFieldColumnMappingTargetHolder> holders = new ArrayList<>();

		//Get all of the MarketDataFieldColumnMappings and create target holder instances for them to be placed in the cache
		List<MarketDataFieldColumnMapping> mappingList = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingList(new MarketDataFieldColumnMappingSearchForm());
		for (MarketDataFieldColumnMapping mapping : CollectionUtils.getIterable(mappingList)) {
			holders.addAll(buildMarketDataFieldColumnMappingHolderList(mapping));
		}

		/*
		 * Get all of the MarketDataFieldGroups and their associated MarketDataFieldColumnMappings and create target holder instances for them to be placed in the cache. (There is a
		 * many-to-many relationship between MarketDataFieldColumnMapping and MarketDataFieldGroup.)
		 */
		List<MarketDataFieldGroup> fieldGroups = getMarketDataFieldService().getMarketDataFieldGroupList(new MarketDataFieldGroupSearchForm());
		for (MarketDataFieldGroup group : CollectionUtils.getIterable(fieldGroups)) {
			holders.addAll(buildTargetHoldersForMarketDataFieldGroup(group));
		}

		return holders;
	}


	private List<MarketDataFieldColumnMappingTargetHolder> buildMarketDataFieldColumnMappingHolderList(MarketDataFieldColumnMapping mapping) {
		List<MarketDataFieldColumnMapping> mappings = new ArrayList<>();

		//if the mapping is a template, resolve the mappings
		if (mapping.getTemplate() != null) {
			resolveTemplateMappings(mapping, mappings);
		}
		else {
			mappings.add(mapping);
		}

		List<MarketDataFieldColumnMappingTargetHolder> result = new ArrayList<>();

		for (MarketDataFieldColumnMapping map : mappings) {
			MarketDataFieldColumnMappingTargetHolder holder = buildTargetHolder(map, null);
			if (mapping.getInstrumentHierarchy() != null) {
				recursivelyAddHierarchyMapping(mapping, map, map.getInstrumentHierarchy(), result, null);
			}
			result.add(holder);
		}

		return result;
	}


	private void resolveTemplateMappings(MarketDataFieldColumnMapping mapping, List<MarketDataFieldColumnMapping> mappings) {
		List<SystemColumnCustom> columns = getMarketDataFieldColumnMappingService().resolveSystemColumnListFromTemplate(mapping);

		//clone the original mapping and alter the systemColumn field
		for (SystemColumnCustom column : columns) {
			MarketDataFieldColumnMapping tempMapping = BeanUtils.cloneBean(mapping, false, true);
			tempMapping.setColumn(column);
			mappings.add(tempMapping);
		}
	}


	private List<MarketDataFieldColumnMappingTargetHolder> buildTargetHoldersForMarketDataFieldGroup(MarketDataFieldGroup group) {
		List<MarketDataFieldColumnMappingTargetHolder> holders = new ArrayList<>();

		MarketDataFieldColumnMappingSearchForm sf = new MarketDataFieldColumnMappingSearchForm();
		sf.setDataFieldGroupId(group.getId());
		List<MarketDataFieldColumnMapping> mappingList = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingList(sf);
		for (MarketDataFieldColumnMapping mapping : CollectionUtils.getIterable(mappingList)) {
			MarketDataFieldColumnMappingTargetHolder holder = buildTargetHolder(mapping, group);
			holders.add(holder);
			if (mapping.getInstrumentHierarchy() != null) {
				recursivelyAddHierarchyMapping(null, mapping, mapping.getInstrumentHierarchy(), holders, group);
			}
		}

		return holders;
	}


	private void recursivelyAddHierarchyMapping(MarketDataFieldColumnMapping originalMapping, MarketDataFieldColumnMapping mapping, InvestmentInstrumentHierarchy instrumentHierarchy, List<MarketDataFieldColumnMappingTargetHolder> holders, MarketDataFieldGroup group) {
		if (instrumentHierarchy != null) {
			InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
			searchForm.setParentId(instrumentHierarchy.getId());
			List<InvestmentInstrumentHierarchy> hierarchyList = getInvestmentSetupService().getInvestmentInstrumentHierarchyList(searchForm);
			for (InvestmentInstrumentHierarchy hierarchy : CollectionUtils.getIterable(hierarchyList)) {
				MarketDataFieldColumnMappingSearchForm mappingSearchForm = new MarketDataFieldColumnMappingSearchForm();
				mappingSearchForm.setDataSourceId(mapping.getDataSource().getId());
				mappingSearchForm.setDataFieldId(mapping.getDataField().getId());
				if (mapping.getTemplate() != null) {
					mappingSearchForm.setTemplateId(mapping.getTemplate().getId());
				}
				else {
					mappingSearchForm.setColumnId(mapping.getColumn().getId());
				}
				mappingSearchForm.setInstrumentHierarchyId(hierarchy.getId());
				mappingSearchForm.setLimit(1);
				List<MarketDataFieldColumnMapping> mappingList = getMarketDataFieldColumnMappingService().getMarketDataFieldColumnMappingList(mappingSearchForm);
				if (CollectionUtils.isEmpty(mappingList)) {
					MarketDataFieldColumnMapping newMapping = BeanUtils.cloneBean(mapping, false, true);
					newMapping.setInstrumentHierarchy(hierarchy);

					holders.add(buildTargetHolder(newMapping, group));
				}
				recursivelyAddHierarchyMapping(originalMapping, mapping, hierarchy, holders, group);
			}
		}
	}


	MarketDataFieldColumnMappingTargetHolder buildTargetHolder(MarketDataFieldColumnMapping target, MarketDataFieldGroup group) {
		return new MarketDataFieldColumnMappingTargetHolder(generateKey(target, group), target);
	}


	@Override
	protected SpecificityKeySource getKeySource(MarketDataFieldColumnMappingCommand<?> source) {
		Serializable investmentTypeId = source.getInvestmentTypeId();
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : source.getInvestmentTypeSubTypeId();
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : source.getInvestmentTypeSubType2Id();

		Object[] instrumentHierarchy = {source.getInstrumentHierarchyId(), null};
		Object[] investmentTypeIdHierarchy = {investmentTypeId, null};
		// NOTE: SUB TYPE 2 is more specific than SUB TYPE 1
		Object[] investmentTypeSubType2IdHierarchy = {investmentTypeSubType2Id, null};
		Object[] investmentTypeSubTypeIdHierarchy = {investmentTypeSubTypeId, null};
		Object[] valuesToAppend = {source.getTableName(), source.getDataSource().getId(), source.getDataFieldGroupId()};

		return SpecificityKeySource.builder(instrumentHierarchy).addHierarchy(investmentTypeIdHierarchy)
				.addHierarchy(investmentTypeSubType2IdHierarchy)
				.addHierarchy(investmentTypeSubTypeIdHierarchy).setValuesToAppend(valuesToAppend).build();
	}


	private String generateKey(MarketDataFieldColumnMapping mapping, MarketDataFieldGroup group) {
		Serializable investmentTypeId = BeanUtils.getBeanIdentity(mapping.getInvestmentType());
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(mapping.getInvestmentTypeSubType());
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(mapping.getInvestmentTypeSubType2());

		Object[] instrumentHierarchy = {BeanUtils.getBeanIdentity(mapping.getInstrumentHierarchy()), null};
		Object[] investmentTypeIdHierarchy = {investmentTypeId, null};
		Object[] investmentTypeSubType2IdHierarchy = {investmentTypeSubType2Id, null};
		Object[] investmentTypeSubTypeIdHierarchy = {investmentTypeSubTypeId, null};
		Object[] valuesToAppend = {getName(mapping.getColumn().getTable()), BeanUtils.getBeanIdentity(mapping.getDataSource()), BeanUtils.getBeanIdentity(group)};

		return getKeyGenerator().generateKeyForTarget(SpecificityKeySource.builder(instrumentHierarchy).addHierarchy(investmentTypeIdHierarchy)
				.addHierarchy(investmentTypeSubType2IdHierarchy)
				.addHierarchy(investmentTypeSubTypeIdHierarchy).setValuesToAppend(valuesToAppend).build());
	}


	private static Object getName(IdentityObject entity) {
		return BeanUtils.getPropertyValue(entity, "name", true);
	}


	@SuppressWarnings("unused")
	@Override
	protected boolean isMatch(MarketDataFieldColumnMappingCommand<?> source, MarketDataFieldColumnMappingTargetHolder holder) {
		/*
		 * At this point it is assumed source and holder have matching "specificities". In the case of
		 * MarketDataFieldColumnMappingSpecificityCache, no further comparison is needed.
		 */
		return true;
	}


	@Override
	protected SpecificityCacheUpdater<MarketDataFieldColumnMappingTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldColumnMappingService getMarketDataFieldColumnMappingService() {
		return this.marketDataFieldColumnMappingService;
	}


	public void setMarketDataFieldColumnMappingService(MarketDataFieldColumnMappingService marketDataFieldColumnMappingService) {
		this.marketDataFieldColumnMappingService = marketDataFieldColumnMappingService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public com.clifton.investment.setup.InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(com.clifton.investment.setup.InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}
}
