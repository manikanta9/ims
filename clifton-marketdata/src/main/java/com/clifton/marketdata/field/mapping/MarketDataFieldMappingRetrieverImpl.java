package com.clifton.marketdata.field.mapping;

import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class MarketDataFieldMappingRetrieverImpl implements MarketDataFieldMappingRetriever {

	private SpecificityCache<MarketDataFieldMappingCommand, MarketDataFieldMapping> marketDataFieldMappingSpecificityCache;
	private SpecificityCache<MarketDataPriceFieldMappingCommand, MarketDataPriceFieldMapping> marketDataPriceFieldMappingSpecificityCache;

	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataSourceService marketDataSourceService;

	////////////////////////////////////////////////////////////////////////////
	//////         Market Data Field Mapping Retrieval Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataFieldMapping getMarketDataFieldMappingByInstrument(InvestmentInstrument instrument, MarketDataSource dataSource) {
		return getMarketDataFieldMapping(instrument, null, dataSource);
	}


	@Override
	public MarketDataFieldMapping getMarketDataFieldMappingByHierarchy(InvestmentInstrumentHierarchy hierarchy, MarketDataSource dataSource) {
		return getMarketDataFieldMapping(null, hierarchy, dataSource);
	}


	/**
	 * If the instrument parameters is specified, then hierarchy will be ignored: will use specified instrument's hierarchy.
	 */
	private MarketDataFieldMapping getMarketDataFieldMapping(InvestmentInstrument instrument, InvestmentInstrumentHierarchy hierarchy, MarketDataSource dataSource) {
		if (dataSource == null) {
			dataSource = getDefaultDataSource();
		}
		ValidationUtils.assertNotNull(dataSource, "DataSource is required to find the most specific MarketDataFieldMapping for [" + (instrument != null ? instrument.getLabel() : "") + "] and [" + (hierarchy != null ? hierarchy.getLabelExpanded() : "") + "].");
		List<MarketDataFieldMapping> mappings = getMarketDataFieldMappingSpecificityCache().getMostSpecificResultList(generateMarketDataFieldMappingCommand(instrument, hierarchy, dataSource));
		return CollectionUtils.getOnlyElement(mappings);
	}


	private MarketDataFieldMappingCommand generateMarketDataFieldMappingCommand(InvestmentInstrument instrument, InvestmentInstrumentHierarchy hierarchy, MarketDataSource dataSource) {
		InvestmentInstrumentHierarchy instrumentHierarchy = hierarchy;

		MarketDataFieldMappingCommand source = new MarketDataFieldMappingCommand();
		if (instrument != null) {
			source.setInstrumentId(instrument.getId());
			instrumentHierarchy = instrument.getHierarchy();
		}

		InvestmentType type = instrumentHierarchy != null ? instrumentHierarchy.getInvestmentType() : null;
		InvestmentTypeSubType subType = type != null ? instrumentHierarchy.getInvestmentTypeSubType() : null;
		InvestmentTypeSubType2 subType2 = type != null ? instrumentHierarchy.getInvestmentTypeSubType2() : null;

		source.setInstrumentHierarchyId(instrumentHierarchy != null ? instrumentHierarchy.getId() : null);
		source.setInvestmentTypeId(type != null ? type.getId() : null);
		source.setInvestmentTypeSubTypeId(subType != null ? subType.getId() : null);
		source.setInvestmentTypeSubType2Id(subType2 != null ? subType2.getId() : null);

		source.setMarketDataSourceId(dataSource != null ? dataSource.getId() : null);

		return source;
	}


	////////////////////////////////////////////////////////////////////////////
	//////      Market Data Price Field Mapping Retrieval Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataPriceFieldMapping getMarketDataPriceFieldMappingByInstrument(InvestmentInstrument instrument) {
		List<MarketDataPriceFieldMapping> mappings = getMarketDataPriceFieldMappingSpecificityCache().getMostSpecificResultList(new MarketDataPriceFieldMappingCommand(instrument));
		return CollectionUtils.getOnlyElement(mappings);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private MarketDataSource getDefaultDataSource() {
		String dataSourceName = getMarketDataProviderLocator().getDefaultDataSourceName();
		return getMarketDataSourceService().getMarketDataSourceByName(dataSourceName);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public SpecificityCache<MarketDataFieldMappingCommand, MarketDataFieldMapping> getMarketDataFieldMappingSpecificityCache() {
		return this.marketDataFieldMappingSpecificityCache;
	}


	public void setMarketDataFieldMappingSpecificityCache(SpecificityCache<MarketDataFieldMappingCommand, MarketDataFieldMapping> marketDataFieldMappingSpecificityCache) {
		this.marketDataFieldMappingSpecificityCache = marketDataFieldMappingSpecificityCache;
	}


	public SpecificityCache<MarketDataPriceFieldMappingCommand, MarketDataPriceFieldMapping> getMarketDataPriceFieldMappingSpecificityCache() {
		return this.marketDataPriceFieldMappingSpecificityCache;
	}


	public void setMarketDataPriceFieldMappingSpecificityCache(SpecificityCache<MarketDataPriceFieldMappingCommand, MarketDataPriceFieldMapping> marketDataPriceFieldMappingSpecificityCache) {
		this.marketDataPriceFieldMappingSpecificityCache = marketDataPriceFieldMappingSpecificityCache;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
