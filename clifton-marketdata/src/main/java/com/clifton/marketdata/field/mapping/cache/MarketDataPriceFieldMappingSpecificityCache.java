package com.clifton.marketdata.field.mapping.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.specificity.AbstractSpecificityCache;
import com.clifton.core.cache.specificity.ClearSpecificityCacheUpdater;
import com.clifton.core.cache.specificity.SpecificityCacheTypes;
import com.clifton.core.cache.specificity.SpecificityCacheUpdater;
import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMappingCommand;
import com.clifton.marketdata.field.mapping.search.MarketDataPriceFieldMappingSearchForm;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * @author manderson
 */
@Component
public class MarketDataPriceFieldMappingSpecificityCache extends AbstractSpecificityCache<MarketDataPriceFieldMappingCommand, MarketDataPriceFieldMapping, MarketDataPriceFieldMappingTargetHolder> {

	private MarketDataFieldMappingService marketDataFieldMappingService;
	private InvestmentSetupService investmentSetupService;

	private final ClearSpecificityCacheUpdater<MarketDataPriceFieldMappingTargetHolder> cacheUpdater = new ClearSpecificityCacheUpdater<>(MarketDataPriceFieldMapping.class, InvestmentInstrumentHierarchy.class);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	MarketDataPriceFieldMappingSpecificityCache() {
		super(SpecificityCacheTypes.ONE_TO_ONE_RESULT, new SimpleOrderingSpecificityKeyGenerator());
	}


	@Override
	protected boolean isMatch(MarketDataPriceFieldMappingCommand source, MarketDataPriceFieldMappingTargetHolder holder) {
		return true;
	}


	@Override
	protected MarketDataPriceFieldMapping getTarget(MarketDataPriceFieldMappingTargetHolder targetHolder) {
		return getMarketDataFieldMappingService().getMarketDataPriceFieldMapping(targetHolder.getId());
	}


	@Override
	protected Collection<MarketDataPriceFieldMappingTargetHolder> getTargetHolders() {
		List<MarketDataPriceFieldMappingTargetHolder> holders = new ArrayList<>();

		//Get all of the MarketDataPriceFieldMappings and create target holder instances for them to be placed in the cache
		List<MarketDataPriceFieldMapping> mappingList = getMarketDataFieldMappingService().getMarketDataPriceFieldMappingList(new MarketDataPriceFieldMappingSearchForm());

		for (MarketDataPriceFieldMapping mapping : CollectionUtils.getIterable(mappingList)) {
			holders.addAll(buildMarketDataPriceFieldMappingHolderList(mapping));
		}

		return holders;
	}


	private void recursivelyAddHierarchyMapping(MarketDataPriceFieldMapping mapping, InvestmentInstrumentHierarchy hierarchy, List<MarketDataPriceFieldMappingTargetHolder> holders) {
		if (hierarchy != null) {
			InstrumentHierarchySearchForm searchForm = new InstrumentHierarchySearchForm();
			searchForm.setParentId(hierarchy.getId());
			List<InvestmentInstrumentHierarchy> hierarchyList = getInvestmentSetupService().getInvestmentInstrumentHierarchyList(searchForm);
			for (InvestmentInstrumentHierarchy investmentInstrumentHierarchy : CollectionUtils.getIterable(hierarchyList)) {
				MarketDataPriceFieldMappingSearchForm mappingSearchForm = new MarketDataPriceFieldMappingSearchForm();
				mappingSearchForm.setInstrumentHierarchyId(investmentInstrumentHierarchy.getId());
				mappingSearchForm.setLimit(1);
				List<MarketDataPriceFieldMapping> mappingList = getMarketDataFieldMappingService().getMarketDataPriceFieldMappingList(mappingSearchForm);
				if (CollectionUtils.isEmpty(mappingList)) {
					MarketDataPriceFieldMapping newMapping = BeanUtils.cloneBean(mapping, false, true);
					newMapping.setInstrumentHierarchy(investmentInstrumentHierarchy);
					holders.add(buildTargetHolder(newMapping));
				}
				recursivelyAddHierarchyMapping(mapping, investmentInstrumentHierarchy, holders);
			}
		}
	}


	private MarketDataPriceFieldMappingTargetHolder buildTargetHolder(MarketDataPriceFieldMapping target) {
		return new MarketDataPriceFieldMappingTargetHolder(generateKey(target), target);
	}


	private List<MarketDataPriceFieldMappingTargetHolder> buildMarketDataPriceFieldMappingHolderList(MarketDataPriceFieldMapping mapping) {
		List<MarketDataPriceFieldMappingTargetHolder> holders = new ArrayList<>();
		MarketDataPriceFieldMappingTargetHolder holder = buildTargetHolder(mapping);
		holders.add(holder);
		if (mapping.getInstrumentHierarchy() != null) {
			recursivelyAddHierarchyMapping(mapping, mapping.getInstrumentHierarchy(), holders);
		}
		return holders;
	}


	private String generateKey(MarketDataPriceFieldMapping marketDataPriceFieldMapping) {
		Serializable investmentTypeId = BeanUtils.getBeanIdentity(marketDataPriceFieldMapping.getInvestmentType());
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(marketDataPriceFieldMapping.getInvestmentTypeSubType());
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : BeanUtils.getBeanIdentity(marketDataPriceFieldMapping.getInvestmentTypeSubType2());

		Object[] instrumentHierarchy = {BeanUtils.getBeanIdentity(marketDataPriceFieldMapping.getInstrument()), BeanUtils.getBeanIdentity(marketDataPriceFieldMapping.getInstrumentHierarchy()), null};
		Object[] investmentTypeHierarchy = {investmentTypeId, null};
		// NOTE: SUB TYPE 2 is more specific than SUB TYPE 1
		Object[] investmentTypeSubType2Hierarchy = {investmentTypeSubType2Id, null};
		Object[] investmentTypeSubTypeHierarchy = {investmentTypeSubTypeId, null};


		return getKeyGenerator().generateKeyForTarget(SpecificityKeySource.builder(instrumentHierarchy)
				.addHierarchy(investmentTypeHierarchy)
				.addHierarchy(investmentTypeSubType2Hierarchy)
				.addHierarchy(investmentTypeSubTypeHierarchy)
				//.setValuesToAppend(valuesToAppend)
				.build());
	}


	@Override
	protected SpecificityKeySource getKeySource(MarketDataPriceFieldMappingCommand source) {
		Serializable investmentTypeId = source.getInvestmentTypeId();
		Serializable investmentTypeSubTypeId = investmentTypeId == null ? null : source.getInvestmentTypeSubTypeId();
		Serializable investmentTypeSubType2Id = investmentTypeId == null ? null : source.getInvestmentTypeSubType2Id();

		Object[] instrumentHierarchy = {source.getInstrumentId(), source.getInstrumentHierarchyId(), null};
		Object[] investmentTypeHierarchy = {investmentTypeId, null};
		Object[] investmentTypeSubTypeHierarchy = {investmentTypeSubTypeId, null};
		Object[] investmentTypeSubType2Hierarchy = {investmentTypeSubType2Id, null};

		//Object[] valuesToAppend = {source.getMarketDataSourceId()};

		return SpecificityKeySource.builder(instrumentHierarchy)
				.addHierarchy(investmentTypeHierarchy)
				.addHierarchy(investmentTypeSubType2Hierarchy)
				.addHierarchy(investmentTypeSubTypeHierarchy)
				//.setValuesToAppend(valuesToAppend)
				.build();
	}


	@Override
	protected SpecificityCacheUpdater<MarketDataPriceFieldMappingTargetHolder> getCacheUpdater() {
		return this.cacheUpdater;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}
}
