package com.clifton.marketdata.field.column.cache;

import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;


/**
 * Provides a cache scope at the template level when it exists otherwise it's scope is the column.  This will avoid duplicate cache results when using template columns.
 *
 * @author mwacker
 */
public class MarketDataFieldColumnMappingSpecificityScope implements SpecificityScope {

	private final int columnId;
	private final int templateId;
	private final int maxResults;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataFieldColumnMappingSpecificityScope(MarketDataFieldColumnMapping mapping, int maxResults) {
		this.columnId = mapping.getColumn() != null && mapping.getTemplate() == null ? mapping.getColumn().getId() : 0;
		this.templateId = mapping.getTemplate() != null ? mapping.getTemplate().getId() : 0;
		this.maxResults = maxResults;
	}


	public static MarketDataFieldColumnMappingSpecificityScope ofColumnMapping(MarketDataFieldColumnMapping mapping) {
		return ofColumnMappingWithMaxResults(mapping, 1);
	}


	public static MarketDataFieldColumnMappingSpecificityScope ofColumnMappingWithMaxResults(MarketDataFieldColumnMapping mapping, int maxResults) {
		return new MarketDataFieldColumnMappingSpecificityScope(mapping, maxResults);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isLimitToMaximumSpecificity() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "MarketDataFieldColumnMappingSpecificityScope [columnId=" + this.columnId + ", templateId=" + this.templateId + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.maxResults;
		result = prime * result + this.templateId;
		result = prime * result + this.columnId;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MarketDataFieldColumnMappingSpecificityScope other = (MarketDataFieldColumnMappingSpecificityScope) obj;
		if (this.maxResults != other.maxResults) {
			return false;
		}
		if (this.columnId != other.columnId) {
			return false;
		}
		if (this.templateId != other.templateId) {
			return false;
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getMaxResults() {
		return this.maxResults;
	}
}
