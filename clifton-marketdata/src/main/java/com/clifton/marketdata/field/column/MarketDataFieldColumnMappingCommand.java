package com.clifton.marketdata.field.column;


import com.clifton.core.beans.IdentityObject;
import com.clifton.marketdata.datasource.MarketDataSource;

import java.util.List;


/**
 * The <code>MarketDataFieldColumnMappingCommand</code> holds the command parameters need to
 * lookup security fields from a datasource.
 *
 * @param <T>
 * @author mwacker
 */
public class MarketDataFieldColumnMappingCommand<T extends IdentityObject> {

	private Short dataFieldGroupId;

	private String symbol;
	private String exchangeCode;
	private String marketSector;
	private Class<T> beanClass;
	private Integer securityId;
	private MarketDataSource dataSource;
	private Short instrumentHierarchyId;
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;
	private List<String> errors;
	private String tableName;

	private List<MarketDataFieldColumnMapping> columnMappingList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getMarketSector() {
		return this.marketSector;
	}


	public void setMarketSector(String marketSector) {
		this.marketSector = marketSector;
	}


	public Class<T> getBeanClass() {
		return this.beanClass;
	}


	public void setBeanClass(Class<T> beanClass) {
		this.beanClass = beanClass;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public List<String> getErrors() {
		return this.errors;
	}


	public void setErrors(List<String> errors) {
		this.errors = errors;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public List<MarketDataFieldColumnMapping> getColumnMappingList() {
		return this.columnMappingList;
	}


	public void setColumnMappingList(List<MarketDataFieldColumnMapping> columnMappingList) {
		this.columnMappingList = columnMappingList;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}
}
