package com.clifton.marketdata.field.mapping;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;


/**
 * The <code>MarketDataPriceFieldMapping</code> class maps market data price fields to corresponding investment instruments
 * <p>
 * If datasource is not specified for a price field, than any datasource can be used - however if datasource is specified than the ONLY price that can be used for that field must belong to the specified datasource
 *
 * @author manderson
 */
public class MarketDataPriceFieldMapping extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * Used to map field to specific investment types.
	 */
	private InvestmentType investmentType;
	/**
	 * Used to map field to specific investment sub types.
	 */
	private InvestmentTypeSubType investmentTypeSubType;
	/**
	 * Used to map field to specific investment sub type 2's
	 */
	private InvestmentTypeSubType2 investmentTypeSubType2;

	private InvestmentInstrumentHierarchy instrumentHierarchy;

	private InvestmentInstrument instrument;


	private String note;


	////////////////////////////////////////////////////////////////////////////////

	// If DataSource is specified for any of these fields, ONLY that datasource is used for price look ups

	/**
	 * Optional official price of a security (NAV Price from Fund Administrator).
	 * This field is usually either unavailable or only available on the last day of the month.
	 * If the value is available, use this value instead of "Closing Price".
	 */
	private MarketDataField officialPriceMarketDataField;
	private MarketDataSource officialPriceMarketDataSource;

	/**
	 * Used for End of Day pricing. For example, Settlement Price for futures.
	 * If the field is defined, and the value is available, use this value instead of "Latest Price".
	 */
	private MarketDataField closingPriceMarketDataField;
	private MarketDataSource closingPriceMarketDataSource;

	/**
	 * Latest price for a security at any point in time. Least accurate price field that is used
	 * only when Closing and Official prices are not available.
	 */
	private MarketDataField latestPriceMarketDataField;
	private MarketDataSource latestPriceMarketDataSource;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder lbl = new StringBuilder(20);
		if (getInstrument() != null) {
			lbl.append("Instrument: ").append(getInstrument().getLabel());
		}
		else if (getInstrumentHierarchy() != null) {
			lbl.append("Hierarchy: ").append(getInstrumentHierarchy().getLabelExpanded());
		}
		else if (getInvestmentType() != null) {
			lbl.append("Type: ").append(getInvestmentType().getName());
			if (getInvestmentTypeSubType() != null) {
				lbl.append(", SubType: ").append(getInvestmentTypeSubType().getName());
			}
			if (getInvestmentTypeSubType2() != null) {
				lbl.append(", SubType2: ").append(getInvestmentTypeSubType2().getName());
			}
		}
		return lbl.toString();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public MarketDataField getOfficialPriceMarketDataField() {
		return this.officialPriceMarketDataField;
	}


	public void setOfficialPriceMarketDataField(MarketDataField officialPriceMarketDataField) {
		this.officialPriceMarketDataField = officialPriceMarketDataField;
	}


	public MarketDataSource getOfficialPriceMarketDataSource() {
		return this.officialPriceMarketDataSource;
	}


	public void setOfficialPriceMarketDataSource(MarketDataSource officialPriceMarketDataSource) {
		this.officialPriceMarketDataSource = officialPriceMarketDataSource;
	}


	public MarketDataField getClosingPriceMarketDataField() {
		return this.closingPriceMarketDataField;
	}


	public void setClosingPriceMarketDataField(MarketDataField closingPriceMarketDataField) {
		this.closingPriceMarketDataField = closingPriceMarketDataField;
	}


	public MarketDataSource getClosingPriceMarketDataSource() {
		return this.closingPriceMarketDataSource;
	}


	public void setClosingPriceMarketDataSource(MarketDataSource closingPriceMarketDataSource) {
		this.closingPriceMarketDataSource = closingPriceMarketDataSource;
	}


	public MarketDataField getLatestPriceMarketDataField() {
		return this.latestPriceMarketDataField;
	}


	public void setLatestPriceMarketDataField(MarketDataField latestPriceMarketDataField) {
		this.latestPriceMarketDataField = latestPriceMarketDataField;
	}


	public MarketDataSource getLatestPriceMarketDataSource() {
		return this.latestPriceMarketDataSource;
	}


	public void setLatestPriceMarketDataSource(MarketDataSource latestPriceMarketDataSource) {
		this.latestPriceMarketDataSource = latestPriceMarketDataSource;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
