package com.clifton.marketdata.field.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataFieldGroupValidator</code> ...
 *
 * @author manderson
 */
@Component
public class MarketDataFieldGroupValidator extends SelfRegisteringDaoValidator<MarketDataFieldGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(MarketDataFieldGroup bean, DaoEventTypes config) throws ValidationException {
		if (bean.isSystemDefined()) {
			if (config.isDelete()) {
				throw new ValidationException("Deleting System Defined Market Data Field Groups is not allowed.");
			}
			if (config.isInsert()) {
				throw new ValidationException("Adding new System Defined Market Data Field Groups is not allowed.");
			}
		}
		if (config.isUpdate()) {
			MarketDataFieldGroup originalBean = getOriginalBean(bean);
			if (originalBean.isSystemDefined() != bean.isSystemDefined()) {
				throw new FieldValidationException("You cannot edit the System Defined field for Market Data Field Groups", "systemDefined");
			}
			if (originalBean.isSystemDefined()) {
				ValidationUtils.assertTrue(originalBean.getName().equals(bean.getName()), "System Defined Market Data Field Group Names cannot be changed.", "name");
			}
		}
	}
}
