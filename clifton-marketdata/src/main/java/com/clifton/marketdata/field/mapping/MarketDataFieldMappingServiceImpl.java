package com.clifton.marketdata.field.mapping;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.mapping.cache.MarketDataFieldValueMappingCache;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldMappingSearchForm;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldValueMappingSearchForm;
import com.clifton.marketdata.field.mapping.search.MarketDataPriceFieldMappingSearchForm;
import com.clifton.marketdata.field.mapping.validation.MarketDataPriceFieldMappingValidator;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author manderson
 */
@Service
public class MarketDataFieldMappingServiceImpl implements MarketDataFieldMappingService {

	private AdvancedUpdatableDAO<MarketDataFieldMapping, Criteria> marketDataFieldMappingDAO;
	private AdvancedUpdatableDAO<MarketDataPriceFieldMapping, Criteria> marketDataPriceFieldMappingDAO;
	private AdvancedUpdatableDAO<MarketDataFieldValueMapping, Criteria> marketDataFieldValueMappingDAO;

	private MarketDataFieldValueMappingCache marketDataFieldValueMappingCache;

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;


	////////////////////////////////////////////////////////////////////////////
	//////          Market Data Field Mapping Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataFieldMapping getMarketDataFieldMapping(int id) {
		return getMarketDataFieldMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataFieldMapping> getMarketDataFieldMappingList(MarketDataFieldMappingSearchForm searchForm) {
		return getMarketDataFieldMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<MarketDataFieldMapping> getMarketDataFieldMappingListByInstrument(int instrumentId) {
		InvestmentInstrument instrument = getInvestmentInstrumentService().getInvestmentInstrument(instrumentId);
		Set<MarketDataSource> dataSourceWithMappingSet = (BeanUtils.getBeansMap(getMarketDataFieldMappingDAO().findAll(), MarketDataFieldMapping::getMarketDataSource)).keySet();

		// For each Data Source - Get the Mapping and add to the List
		List<MarketDataFieldMapping> list = new ArrayList<>();
		for (MarketDataSource marketDataSource : CollectionUtils.getIterable(dataSourceWithMappingSet)) {
			MarketDataFieldMapping instrumentDataSourceMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(instrument, marketDataSource);
			if (instrumentDataSourceMapping != null) {
				list.add(instrumentDataSourceMapping);
			}
		}
		return list;
	}


	@Override
	public MarketDataFieldMapping saveMarketDataFieldMapping(MarketDataFieldMapping bean) {
		validateMarketDataFieldMappingSave(bean);
		return getMarketDataFieldMappingDAO().save(bean);
	}


	private void validateMarketDataFieldMappingSave(MarketDataFieldMapping bean) {
		List<MarketDataFieldMapping> existingList = new ArrayList<>();

		if (bean.getInstrument() != null) {
			ValidationUtils.assertFalse(bean.getInstrumentHierarchy() != null || bean.getInvestmentType() != null, "A data field mapping may only have one fo the following: an instrument, hierarchy, or combination of type/subtype/subtype2");
			existingList = getMarketDataFieldMappingDAO().findByField("instrument.id", bean.getInstrument().getId());
		}

		if (bean.getInstrumentHierarchy() != null) {
			ValidationUtils.assertFalse(bean.getInstrument() != null || bean.getInvestmentType() != null, "A data field mapping may only have one fo the following: an instrument, hierarchy, or combination of type/subtype/subtype2");
			existingList = getMarketDataFieldMappingDAO().findByField("instrumentHierarchy.id", bean.getInstrumentHierarchy().getId());
		}

		if (bean.getInvestmentType() != null) {
			ValidationUtils.assertFalse(bean.getInstrument() != null || bean.getInstrumentHierarchy() != null, "A data field mapping may only have one fo the following: an instrument, hierarchy, or combination of type/subtype/subtype2");
			MarketDataFieldMappingSearchForm searchForm = new MarketDataFieldMappingSearchForm();

			searchForm.setInvestmentTypeId(bean.getInvestmentType().getId());

			if (bean.getInvestmentTypeSubType() != null) {
				searchForm.setInvestmentTypeSubTypeId(bean.getInvestmentTypeSubType().getId());
			}
			else {
				searchForm.setNullInvestmentTypeSubType(true);
			}

			if (bean.getInvestmentTypeSubType2() != null) {
				searchForm.setInvestmentTypeSubType2Id(bean.getInvestmentTypeSubType2().getId());
			}
			else {
				searchForm.setNullInvestmentTypeSubType2(true);
			}
			existingList = getMarketDataFieldMappingList(searchForm);
		}

		// Only include mappings for the same data source
		existingList = BeanUtils.filter(existingList, MarketDataFieldMapping::getMarketDataSource, bean.getMarketDataSource());

		int count = CollectionUtils.getSize(existingList);
		if (bean.isNewBean()) {
			ValidationUtils.assertTrue(count == 0, "A data field mapping with the specified parameters already exists for datasource [" + bean.getMarketDataSource().getName() + "].");
		}
		else {
			ValidationUtils.assertTrue((count == 1 && existingList.get(0).equals(bean)) || count == 0, "A data field mapping with the specified parameters already exists for datasource [" + bean.getMarketDataSource().getName() + "].");
		}
	}


	@Override
	public void deleteMarketDataFieldMapping(int id) {
		getMarketDataFieldMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Market Data Price Field Mapping Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataPriceFieldMapping getMarketDataPriceFieldMapping(int id) {
		return getMarketDataPriceFieldMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataPriceFieldMapping getMarketDataPriceFieldMappingByInstrument(int instrumentId) {
		return getMarketDataFieldMappingRetriever().getMarketDataPriceFieldMappingByInstrument(getInvestmentInstrumentService().getInvestmentInstrument(instrumentId));
	}


	@Override
	public List<MarketDataPriceFieldMapping> getMarketDataPriceFieldMappingList(MarketDataPriceFieldMappingSearchForm searchForm) {
		return getMarketDataPriceFieldMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	/**
	 * See {@link MarketDataPriceFieldMappingValidator} for validation
	 */
	@Override
	public MarketDataPriceFieldMapping saveMarketDataPriceFieldMapping(MarketDataPriceFieldMapping bean) {
		return getMarketDataPriceFieldMappingDAO().save(bean);
	}


	@Override
	public void deleteMarketDataPriceFieldMapping(int id) {
		getMarketDataPriceFieldMappingDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////     Market Data Field Value Mapping Business Methods      /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataFieldValueMapping getMarketDataFieldValueMapping(short id) {
		return getMarketDataFieldValueMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataFieldValueMapping> getMarketDataFieldValueMappingList(MarketDataFieldValueMappingSearchForm searchForm) {
		return getMarketDataFieldValueMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataFieldValueMapping saveMarketDataFieldValueMapping(MarketDataFieldValueMapping bean) {
		return getMarketDataFieldValueMappingDAO().save(bean);
	}


	@Override
	public void deleteMarketDataFieldValueMapping(short id) {
		getMarketDataFieldValueMappingDAO().delete(id);
	}


	@Override
	public Map<String, String> getMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId) {
		Map<String, String> result = getMarketDataFieldValueMappingCache().getMarketDataFieldValueMappingMap(dataFieldId, dataSourceId);
		if (result == null) {
			MarketDataFieldValueMappingSearchForm sf = new MarketDataFieldValueMappingSearchForm();
			sf.setDataFieldId(dataFieldId);
			sf.setDataSourceId(dataSourceId);
			List<MarketDataFieldValueMapping> fieldValueMappingList = getMarketDataFieldValueMappingList(sf);
			result = new ConcurrentHashMap<>();
			for (MarketDataFieldValueMapping fieldValueMapping : CollectionUtils.getIterable(fieldValueMappingList)) {
				result.put(fieldValueMapping.getFromValue(), fieldValueMapping.getToValue());
			}
			getMarketDataFieldValueMappingCache().setMarketDataFieldValueMappingMap(dataFieldId, dataSourceId, result);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataFieldMapping, Criteria> getMarketDataFieldMappingDAO() {
		return this.marketDataFieldMappingDAO;
	}


	public void setMarketDataFieldMappingDAO(AdvancedUpdatableDAO<MarketDataFieldMapping, Criteria> marketDataFieldMappingDAO) {
		this.marketDataFieldMappingDAO = marketDataFieldMappingDAO;
	}


	public AdvancedUpdatableDAO<MarketDataPriceFieldMapping, Criteria> getMarketDataPriceFieldMappingDAO() {
		return this.marketDataPriceFieldMappingDAO;
	}


	public void setMarketDataPriceFieldMappingDAO(AdvancedUpdatableDAO<MarketDataPriceFieldMapping, Criteria> marketDataPriceFieldMappingDAO) {
		this.marketDataPriceFieldMappingDAO = marketDataPriceFieldMappingDAO;
	}


	public AdvancedUpdatableDAO<MarketDataFieldValueMapping, Criteria> getMarketDataFieldValueMappingDAO() {
		return this.marketDataFieldValueMappingDAO;
	}


	public void setMarketDataFieldValueMappingDAO(AdvancedUpdatableDAO<MarketDataFieldValueMapping, Criteria> marketDataFieldValueMappingDAO) {
		this.marketDataFieldValueMappingDAO = marketDataFieldValueMappingDAO;
	}


	public MarketDataFieldValueMappingCache getMarketDataFieldValueMappingCache() {
		return this.marketDataFieldValueMappingCache;
	}


	public void setMarketDataFieldValueMappingCache(MarketDataFieldValueMappingCache marketDataFieldValueMappingCache) {
		this.marketDataFieldValueMappingCache = marketDataFieldValueMappingCache;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
