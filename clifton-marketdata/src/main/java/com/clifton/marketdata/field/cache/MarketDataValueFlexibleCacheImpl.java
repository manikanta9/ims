package com.clifton.marketdata.field.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>MarketDataValueCacheImpl</code> class caches MarketDataValueHolder objects by date/field/security
 * using "flexible" lookup by date.  Must clear full security cache on update.
 * <p>
 * Cache Contains security id+field to Map of date to value
 * <p>
 * Two versions of the cache - optional data source can be used to restrict results to a specific datasource and are only used when a field request requires that specific datasource
 *
 * @author vgomelsky
 */
@Component
public class MarketDataValueFlexibleCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataValue, String, Map<Long, MarketDataValueHolder>> implements MarketDataValueCache {


	@Override
	public MarketDataValueHolder getMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate) {
		MarketDataValueHolder result = null;
		Map<Long, MarketDataValueHolder> map = getCacheHandler().get(getCacheName(), getBeanKey(securityId, dataFieldId, dataSourceId));
		if (map != null) {
			result = map.get(measureDate.getTime());
		}
		return result;
	}


	@Override
	public void setMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate, MarketDataValue bean) {
		Map<Long, MarketDataValueHolder> map = getCacheHandler().get(getCacheName(), getBeanKey(securityId, dataFieldId, dataSourceId));
		if (map == null) {
			map = new ConcurrentHashMap<>();
			getCacheHandler().put(getCacheName(), getBeanKey(securityId, dataFieldId, dataSourceId), map);
		}
		map.put(measureDate.getTime(), new MarketDataValueHolder(bean));
	}


	private String getBeanKey(int securityId, int dataFieldId, Short dataSourceId) {
		StringBuilder key = new StringBuilder(16);
		key.append(securityId);
		key.append('-');
		key.append(dataFieldId);
		if (dataSourceId != null) {
			key.append('-');
			key.append(dataSourceId);
		}
		return key.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<MarketDataValue> dao, DaoEventTypes event, MarketDataValue bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<MarketDataValue> dao, DaoEventTypes event, MarketDataValue bean, Throwable e) {
		if (e == null) {
			// clear all values for the security and field - both with the data source and without
			getCacheHandler().remove(getCacheName(), getBeanKey(bean.getInvestmentSecurity().getId(), bean.getDataField().getId(), bean.getDataSource().getId()));
			getCacheHandler().remove(getCacheName(), getBeanKey(bean.getInvestmentSecurity().getId(), bean.getDataField().getId(), null));
			if (event.isUpdate()) {
				// also clear all values for original bean in case security or data field changed
				MarketDataValue originalBean = getOriginalBean(dao, bean);
				getCacheHandler().remove(getCacheName(), getBeanKey(originalBean.getInvestmentSecurity().getId(), originalBean.getDataField().getId(), bean.getDataSource().getId()));
				getCacheHandler().remove(getCacheName(), getBeanKey(originalBean.getInvestmentSecurity().getId(), originalBean.getDataField().getId(), null));
			}
		}
	}
}
