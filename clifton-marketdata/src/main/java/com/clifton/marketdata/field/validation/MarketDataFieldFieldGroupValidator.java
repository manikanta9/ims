package com.clifton.marketdata.field.validation;


import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataFieldFieldGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataFieldFieldGroupValidator</code> ...
 *
 * @author manderson
 */
@Component
public class MarketDataFieldFieldGroupValidator extends SelfRegisteringDaoValidator<MarketDataFieldFieldGroup> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(MarketDataFieldFieldGroup bean, DaoEventTypes config) throws ValidationException {
		if (config.isInsert() || config.isUpdate()) {
			if (bean.getReferenceTwo().isFieldOrderRequired()) {
				ValidationUtils.assertNotNull(bean.getReferenceOne().getFieldOrder(), "Field Order is required for group [" + bean.getReferenceTwo().getName() + "].  Before Market Data Field ["
						+ bean.getReferenceOne().getName() + "] can be added to this group, the field order value must be populated.");
			}
		}
	}
}
