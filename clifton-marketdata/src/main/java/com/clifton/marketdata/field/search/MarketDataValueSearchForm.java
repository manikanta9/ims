package com.clifton.marketdata.field.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.math.BinaryOperators;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ValueSearchForm</code> class defines search configuration for MarketDataValue objects.
 *
 * @author vgomelsky
 */
public class MarketDataValueSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "dataField.id")
	private Short dataFieldId;

	@SearchField(searchField = "dataField.id")
	private Short[] dataFieldIds;

	@SearchField(searchField = "name", searchFieldPath = "dataField")
	private String dataFieldName;

	// Custom column for exists clause
	private String dataFieldGroupName;

	@SearchField(searchField = "fieldOrder", searchFieldPath = "dataField")
	private Short dataFieldOrder;

	@SearchField(searchField = "externalFieldName", searchFieldPath = "dataField")
	private String externalDataFieldName;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer[] investmentSecurityIds;

	@SearchField(searchField = "hierarchy.id", searchFieldPath = "investmentSecurity.instrument")
	private Short investmentHierarchyId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "investmentSecurity.instrument.hierarchy")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentTypeSubType.id", searchFieldPath = "investmentSecurity.instrument.hierarchy")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType2.id", searchFieldPath = "investmentSecurity.instrument.hierarchy")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "sedol", searchFieldPath = "investmentSecurity")
	private String sedol;

	@SearchField(searchField = "figi", searchFieldPath = "investmentSecurity")
	private String figi;

	@SearchField(searchField = "isin", searchFieldPath = "investmentSecurity")
	private String isin;

	@SearchField(searchField = "occSymbol", searchFieldPath = "investmentSecurity")
	private String occSymbol;

	@SearchField(searchField = "symbol", searchFieldPath = "investmentSecurity")
	private String symbol;

	@SearchField(searchField = "cusip", searchFieldPath = "investmentSecurity")
	private String cusip;

	@SearchField
	private Date measureDate;

	@SearchField(searchField = "measureDate", comparisonConditions = ComparisonConditions.IN)
	private Date[] measureDates;

	@SearchField
	private BigDecimal measureValue;
	@SearchField
	private BigDecimal measureValueAdjustmentFactor;

	@SearchField(searchField = "measureDate", comparisonConditions = ComparisonConditions.GREATER_THAN)
	private Date minMeasureDate;

	@SearchField(searchField = "measureDate", comparisonConditions = ComparisonConditions.LESS_THAN)
	private Date maxMeasureDate;

	/**
	 * Custom Search Filters - Used to find market data values for fields "tagged" with the selected option (i.e. hierarchy)
	 * i.e. "Prices", "KRD Points"  If set, will first get the list of data field ids and then
	 * pass that to the value lookup
	 */
	private Short dataFieldCategoryHierarchyId;

	/**
	 * Custom Search Filter: When populated, limits to securities whose instruments apply to the selected price field mappings
	 * Doesn't actually limit the data fields, just the instruments
	 */
	private Integer priceFieldMappingId;

	@SearchField(searchFieldPath = "dataField")
	private BinaryOperators adjustmentType;

	@SearchField(searchFieldPath = "dataField", searchField = "adjustmentType", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean adjustmentTypeDefined;

	@SearchField(searchField = "dataField.realTimeReferenceData")
	private Boolean realTimeReferenceData;

	// Custom search fields
	private Date maxMeasureDateAndTime;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataValueSearchForm() {
		this(false);
	}


	public MarketDataValueSearchForm(boolean skipDefaultSorting) {
		super();
		setSkipDefaultSorting(skipDefaultSorting);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		return true;
	}


	@Override
	public void validate() {
		// when Data Source filter is used, at least one more other filter must be specified
		SearchUtils.validateRequiredFilter(this, "dataSourceId");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getDataFieldId() {
		return this.dataFieldId;
	}


	public void setDataFieldId(Short dataFieldId) {
		this.dataFieldId = dataFieldId;
	}


	public Short[] getDataFieldIds() {
		return this.dataFieldIds;
	}


	public void setDataFieldIds(Short[] dataFieldIds) {
		this.dataFieldIds = dataFieldIds;
	}


	public String getDataFieldName() {
		return this.dataFieldName;
	}


	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}


	public String getDataFieldGroupName() {
		return this.dataFieldGroupName;
	}


	public void setDataFieldGroupName(String dataFieldGroupName) {
		this.dataFieldGroupName = dataFieldGroupName;
	}


	public Short getDataFieldOrder() {
		return this.dataFieldOrder;
	}


	public void setDataFieldOrder(Short dataFieldOrder) {
		this.dataFieldOrder = dataFieldOrder;
	}


	public String getExternalDataFieldName() {
		return this.externalDataFieldName;
	}


	public void setExternalDataFieldName(String externalDataFieldName) {
		this.externalDataFieldName = externalDataFieldName;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer[] getInvestmentSecurityIds() {
		return this.investmentSecurityIds;
	}


	public void setInvestmentSecurityIds(Integer[] investmentSecurityIds) {
		this.investmentSecurityIds = investmentSecurityIds;
	}


	public Short getInvestmentHierarchyId() {
		return this.investmentHierarchyId;
	}


	public void setInvestmentHierarchyId(Short investmentHierarchyId) {
		this.investmentHierarchyId = investmentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getSedol() {
		return this.sedol;
	}


	public void setSedol(String sedol) {
		this.sedol = sedol;
	}


	public String getFigi() {
		return this.figi;
	}


	public void setFigi(String figi) {
		this.figi = figi;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public String getOccSymbol() {
		return this.occSymbol;
	}


	public void setOccSymbol(String occSymbol) {
		this.occSymbol = occSymbol;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Date[] getMeasureDates() {
		return this.measureDates;
	}


	public void setMeasureDates(Date[] measureDates) {
		this.measureDates = measureDates;
	}


	public BigDecimal getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(BigDecimal measureValue) {
		this.measureValue = measureValue;
	}


	public BigDecimal getMeasureValueAdjustmentFactor() {
		return this.measureValueAdjustmentFactor;
	}


	public void setMeasureValueAdjustmentFactor(BigDecimal measureValueAdjustmentFactor) {
		this.measureValueAdjustmentFactor = measureValueAdjustmentFactor;
	}


	public Date getMinMeasureDate() {
		return this.minMeasureDate;
	}


	public void setMinMeasureDate(Date minMeasureDate) {
		this.minMeasureDate = minMeasureDate;
	}


	public Date getMaxMeasureDate() {
		return this.maxMeasureDate;
	}


	public void setMaxMeasureDate(Date maxMeasureDate) {
		this.maxMeasureDate = maxMeasureDate;
	}


	public Short getDataFieldCategoryHierarchyId() {
		return this.dataFieldCategoryHierarchyId;
	}


	public void setDataFieldCategoryHierarchyId(Short dataFieldCategoryHierarchyId) {
		this.dataFieldCategoryHierarchyId = dataFieldCategoryHierarchyId;
	}


	public Integer getPriceFieldMappingId() {
		return this.priceFieldMappingId;
	}


	public void setPriceFieldMappingId(Integer priceFieldMappingId) {
		this.priceFieldMappingId = priceFieldMappingId;
	}


	public BinaryOperators getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(BinaryOperators adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public Boolean getAdjustmentTypeDefined() {
		return this.adjustmentTypeDefined;
	}


	public void setAdjustmentTypeDefined(Boolean adjustmentTypeDefined) {
		this.adjustmentTypeDefined = adjustmentTypeDefined;
	}


	public Boolean getRealTimeReferenceData() {
		return this.realTimeReferenceData;
	}


	public void setRealTimeReferenceData(Boolean realTimeReferenceData) {
		this.realTimeReferenceData = realTimeReferenceData;
	}


	public Date getMaxMeasureDateAndTime() {
		return this.maxMeasureDateAndTime;
	}


	public void setMaxMeasureDateAndTime(Date maxMeasureDateAndTime) {
		this.maxMeasureDateAndTime = maxMeasureDateAndTime;
	}
}
