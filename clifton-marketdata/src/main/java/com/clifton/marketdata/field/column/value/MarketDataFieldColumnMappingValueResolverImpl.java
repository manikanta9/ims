package com.clifton.marketdata.field.column.value;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.mvc.WebMethodHandler;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.SystemDataTypeConverter;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.value.SystemColumnValue;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class MarketDataFieldColumnMappingValueResolverImpl implements MarketDataFieldColumnMappingValueResolver {

	/**
	 * A list of table names with default lookup fields.
	 * <p>
	 * Can this be put somewhere else?
	 */
	private static final Map<String, String> DEFAULT_TABLE_NAME_TO_LOOKUP_FIELDS;


	static {
		DEFAULT_TABLE_NAME_TO_LOOKUP_FIELDS = new HashMap<>();
		DEFAULT_TABLE_NAME_TO_LOOKUP_FIELDS.put("SystemListItem", "value");
	}


	private MarketDataFieldMappingService marketDataFieldMappingService;
	private DaoLocator daoLocator;
	private WebMethodHandler webMethodHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject, R> R resolve(ColumnMappingValueResolverCommand<T> command) {
		command.setConvertedValue(convertReceivedValue(command));

		R result;
		if (command.getColumn().isCustomColumn() && command.getTargetObject() instanceof SystemColumnCustomValueAware) {
			result = (R) getCustomColumnValue((SystemColumnCustom) command.getColumn(), command);
		}
		else {
			result = (R) getColumnValue((SystemColumnStandard) command.getColumn(), command);
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private <T extends IdentityObject> String convertReceivedValue(ColumnMappingValueResolverCommand<T> command) {
		Map<String, String> fieldValueMap = getColumnValueMapping(command.getColumnMapping());
		// convert the received value to a string for looking up from field map, SystemDataTypeConverter used later requires a string
		String convertedValue = (new ObjectToStringConverter()).convert(command.getReceivedValue());

		// translate the value into something IMS can understand.
		if (fieldValueMap.containsKey(convertedValue)) {
			convertedValue = fieldValueMap.get(convertedValue);
		}
		return convertedValue;
	}


	private <T extends IdentityObject> SystemColumnValue getCustomColumnValue(SystemColumnCustom customColumn, ColumnMappingValueResolverCommand<T> command) {
		SystemColumnValue scv = null;
		String value = command.getConvertedValue();
		String text = command.getConvertedValue();
		if (!command.getTargetObject().isNewBean()) {
			scv = getExistingColumnValue(command.getColumn(), command.getColumnValueList());
		}
		if (scv == null) {
			scv = new SystemColumnValue();
		}
		else if (!command.isApplyIfNotNull()) {
			// only override values that are not null
			return null;
		}
		scv.setColumn(customColumn);
		if (!command.getTargetObject().isNewBean()) {
			scv.setFkFieldId(BeanUtils.getIdentityAsInteger(command.getTargetObject()));
		}

		// if the custom column links to a value table then lookup the value
		if (customColumn.getValueTable() != null) {
			IdentityObject foreignKeyValue = lookupSystemColumnCustomForeignKey(customColumn, command.getColumnMapping().getBeanPropertyNameForLookup(), command.getConvertedValue());
			if (foreignKeyValue == null) {
				throw new FieldValidationException("No mapping found for retrieved value of '" + command.getConvertedValue() + "' to '" + customColumn.getLabel() + "' field.", customColumn.getName());
			}
			else if (foreignKeyValue instanceof SystemListItem) {
				value = ((SystemListItem) foreignKeyValue).getValue();
				text = ((SystemListItem) foreignKeyValue).getText();
			}
			else {
				value = new ObjectToStringConverter().convert(foreignKeyValue.getIdentity());
				if (foreignKeyValue instanceof NamedEntity) {
					//noinspection rawtypes
					text = ((NamedEntity) foreignKeyValue).getName();
				}
			}
		}

		scv.setValue(value);
		scv.setText(text);
		return scv;
	}


	private <T extends IdentityObject> Object getColumnValue(SystemColumnStandard column, ColumnMappingValueResolverCommand<T> command) {
		Object currentValue = BeanUtils.getPropertyValue(command.getTargetObject(), column.getBeanPropertyName(), true);
		// only override values that are not null unless specified
		if (command.isApplyIfNotNull() || (currentValue == null)) {
			Object value = command.getConvertedValue();
			String beanPropertyName = column.getBeanPropertyName();
			if (!StringUtils.isEmpty(command.getColumnMapping().getBeanPropertyNameForLookup()) || !StringUtils.isEmpty(column.getValueListUrl())) {
				IdentityObject foreignKeyValue = lookupSystemColumnStandardForeignKey(column, command);
				ValidationUtils.assertNotNull(foreignKeyValue,
						"Failed to lookup foreign key value for [" + beanPropertyName + "] with a value of [" + value + "] in field [" + command.getColumnMapping().getBeanPropertyNameForLookup() + "].");
				value = foreignKeyValue;
			}
			else {
				value = (new SystemDataTypeConverter(column.getDataType())).convert(command.getConvertedValue());
			}
			return value;
		}
		return null;
	}


	private Map<String, String> getColumnValueMapping(MarketDataFieldColumnMapping columnMap) {
		return getMarketDataFieldMappingService().getMarketDataFieldValueMappingMap(columnMap.getDataField().getId(), columnMap.getDataSource().getId());
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> T lookupSystemColumnStandardForeignKey(SystemColumnStandard column, ColumnMappingValueResolverCommand<T> command) {
		String beanPropertyName = column.getBeanPropertyName();
		String lookupPropertyName = command.getColumnMapping().getBeanPropertyNameForLookup();
		Object target = command.getTargetObject();
		if (!StringUtils.isEmpty(column.getValueListUrl())) {
			Class<?> clazz = BeanUtils.getPropertyType(target.getClass(), beanPropertyName);
			ReadOnlyDAO<T> dao = getDAO((Class<T>) clazz);
			if (dao == null) {
				return null;
			}
			String tableName = dao.getConfiguration().getTableName();
			// use converted data for enumeration lookup:  "International" -> "INT"
			return lookupForeignKeyUsingServiceUrl(tableName, column.getValueListUrl(), lookupPropertyName, command.getConvertedValue());
		}
		else if (!StringUtils.isEmpty(lookupPropertyName)) {
			// received value retains the correct data type
			return lookupForeignKeyUsingDAO(beanPropertyName, lookupPropertyName, command.getReceivedValue(), target);
		}
		return null;
	}


	private <T extends IdentityObject> T lookupSystemColumnCustomForeignKey(SystemColumnCustom column, String lookupPropertyName, Object value) {
		return lookupForeignKeyUsingServiceUrl(column.getValueTable().getName(), column.getValueListUrl(), lookupPropertyName, value);
	}


	private <T extends IdentityObject> T lookupForeignKeyUsingServiceUrl(String tableName, String valueListUrl, String lookupPropertyName, Object value) {
		Map<String, String> params = new HashMap<>();
		if (StringUtils.isEmpty(lookupPropertyName) && DEFAULT_TABLE_NAME_TO_LOOKUP_FIELDS.containsKey(tableName)) {
			lookupPropertyName = DEFAULT_TABLE_NAME_TO_LOOKUP_FIELDS.get(tableName);
		}
		ValidationUtils.assertTrue(!StringUtils.isEmpty(lookupPropertyName), "Lookup property name must be specified to lookup value from url [" + valueListUrl + "].");
		params.put(lookupPropertyName, value.toString());
		List<T> valueList = getWebMethodHandler().executeServiceCall(valueListUrl, params);

		int valueCount = CollectionUtils.getSize(valueList);
		if (valueCount > 1) {
			// Try to find an exact match before failing with more than one match.
			// If case sensitivity is an issue, the marketdata field column mappings should be configured for external value to our value.
			for (T possibleObject : valueList) {
				Object possibleValue = BeanUtils.getPropertyValue(possibleObject, lookupPropertyName);
				if (CompareUtils.isEqualWithSmartLogic(possibleValue, value)) {
					return possibleObject;
				}
			}
		}
		return CollectionUtils.getOnlyElement(valueList);
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> T lookupForeignKeyUsingDAO(String beanPropertyName, String lookupPropertyName, Object value, Object target) {
		Class<?> clazz = BeanUtils.getPropertyType(target.getClass(), beanPropertyName);
		ReadOnlyDAO<T> dao = getDAO((Class<T>) clazz);
		if (dao == null) {
			return null;
		}
		return dao.findOneByField(lookupPropertyName, value);
	}


	private <T extends IdentityObject> ReadOnlyDAO<T> getDAO(Class<T> dtoClass) {
		AssertUtils.assertNotNull(getDaoLocator(), "daoLocator is missing: cannot locate " + dtoClass);
		try {
			return getDaoLocator().locate(dtoClass);
		}
		catch (Throwable e) {
			LogUtils.info(getClass(), "Failed to find dao for class [" + dtoClass + "].", e);
		}
		return null;
	}


	private SystemColumnValue getExistingColumnValue(SystemColumn column, List<SystemColumnValue> columnValueList) {
		if (columnValueList != null) {
			return CollectionUtils.getOnlyElement(BeanUtils.filter(columnValueList, SystemColumnValue::getColumn, column));
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public WebMethodHandler getWebMethodHandler() {
		return this.webMethodHandler;
	}


	public void setWebMethodHandler(WebMethodHandler webMethodHandler) {
		this.webMethodHandler = webMethodHandler;
	}
}
