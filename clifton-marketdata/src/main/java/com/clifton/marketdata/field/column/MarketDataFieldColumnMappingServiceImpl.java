package com.clifton.marketdata.field.column;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.specificity.SpecificityCache;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldFieldGroup;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.field.column.cache.MarketDataFieldColumnMappingSystemColumnCache;
import com.clifton.marketdata.field.column.value.ColumnMappingValueResolverCommand;
import com.clifton.marketdata.field.column.value.MarketDataFieldColumnMappingValueResolver;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnCustomValueAware;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.search.SystemColumnValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class MarketDataFieldColumnMappingServiceImpl implements MarketDataFieldColumnMappingService {


	private AdvancedUpdatableDAO<MarketDataFieldColumnMapping, Criteria> marketDataFieldColumnMappingDAO;

	private SystemColumnValueService systemColumnValueService;
	private SystemColumnService systemColumnService;

	private InvestmentSetupService investmentSetupService;
	private InvestmentInstrumentService investmentInstrumentService;

	private DaoLocator daoLocator;
	private MarketDataProviderLocator marketDataProviderLocator;

	private SpecificityCache<MarketDataFieldColumnMappingCommand<?>, MarketDataFieldColumnMapping> marketDataFieldColumnMappingSpecificityCache;
	private MarketDataFieldColumnMappingSystemColumnCache marketDataFieldColumnMappingSystemColumnCache;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataFieldColumnMappingValueResolver marketDataFieldColumnMappingValueResolver;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataFieldColumnMapping getMarketDataFieldColumnMapping(int id) {
		return getMarketDataFieldColumnMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingList(final MarketDataFieldColumnMappingSearchForm searchForm) {
		// If linked value is null and explicitly not including null linked values, return nothing - nothing will match so don't bother going to the database
		if (searchForm.getLinkedValue() == null && (searchForm.getIncludeNullLinkedValue() != null && searchForm.getIncludeNullLinkedValue())) {
			return null;
		}

		HibernateSearchFormConfigurer searchConfig = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);
				if (searchForm.getLinkedValue() != null) {
					String columnAlias = getPathAlias("column", criteria);
					if (searchForm.getIncludeNullLinkedValue() != null && searchForm.getIncludeNullLinkedValue()) {
						criteria.add(Restrictions.or(Restrictions.isNull(columnAlias + ".linkedValue"), Restrictions.eq(columnAlias + ".linkedValue", searchForm.getLinkedValue())));
					}
					else {
						criteria.add(Restrictions.eq(columnAlias + ".linkedValue", searchForm.getLinkedValue()));
					}
				}
				if (searchForm.getDataFieldGroupId() != null) {
					String fieldAlias = getPathAlias("dataField", criteria);

					DetachedCriteria ne = DetachedCriteria.forClass(MarketDataFieldFieldGroup.class, "fg");
					ne.setProjection(Projections.property("id"));
					ne.createAlias("referenceOne", "g");
					ne.add(Restrictions.eqProperty("g.id", fieldAlias + ".id"));
					ne.add(Restrictions.eq("referenceTwo.id", searchForm.getDataFieldGroupId()));
					criteria.add(Subqueries.exists(ne));
				}
			}
		};
		return getMarketDataFieldColumnMappingDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public MarketDataFieldColumnMapping saveMarketDataFieldColumnMapping(MarketDataFieldColumnMapping bean) {
		validateMarketDataFieldColumnMappingSave(bean);
		return getMarketDataFieldColumnMappingDAO().save(bean);
	}


	private void validateMarketDataFieldColumnMappingSave(MarketDataFieldColumnMapping bean) {
		//Check to make sure the bean only has either the System Column or Template filled, but not both
		ValidationUtils.assertFalse((bean.getColumn() != null) && (bean.getTemplate() != null), "A column mapping may use either a System Column or a System Column Template but not both.");

		//check to make sure the bean only has type and subtype and/or subtype2 populated OR the hierarchy is populated, but not both
		// to have a subtype or subtype2 the type must be populated
		ValidationUtils.assertFalse(bean.getInstrumentHierarchy() != null && bean.getInvestmentType() != null, "A column mapping may use either a hierarchy or a combination of type, subtype, and subtype2, but not both.");

		MarketDataFieldColumnMappingSearchForm searchForm = new MarketDataFieldColumnMappingSearchForm();
		if (bean.getColumn() != null) {
			searchForm.setColumnId(bean.getColumn().getId());
		}

		if (bean.getTemplate() != null) {
			searchForm.setTemplateId(bean.getTemplate().getId());
		}

		if (bean.getInstrumentHierarchy() != null) {
			searchForm.setInstrumentHierarchyId(bean.getInstrumentHierarchy().getId());
		}
		else {
			searchForm.setNullInstrumentHierarchy(true);
		}

		if (bean.getInvestmentType() != null) {
			searchForm.setInvestmentTypeId(bean.getInvestmentType().getId());
		}
		else {
			searchForm.setNullInvestmentType(true);
		}

		if (bean.getInvestmentTypeSubType() != null) {
			searchForm.setInvestmentTypeSubTypeId(bean.getInvestmentTypeSubType().getId());
		}
		else {
			searchForm.setNullInvestmentTypeSubType(true);
		}

		if (bean.getInvestmentTypeSubType2() != null) {
			searchForm.setInvestmentTypeSubType2Id(bean.getInvestmentTypeSubType2().getId());
		}
		else {
			searchForm.setNullInvestmentTypeSubType2(true);
		}


		List<MarketDataFieldColumnMapping> existingList = getMarketDataFieldColumnMappingList(searchForm);
		if (bean.isNewBean()) {
			ValidationUtils.assertTrue(existingList.isEmpty(), "A column mapping with the specified parameters already exists.");
		}
		else {
			ValidationUtils.assertTrue((existingList.size() == 1 && existingList.get(0).equals(bean)) || existingList.isEmpty(), "A column mapping with the specified parameters already exists.");
		}


		//if the column is a custom column, validate the column is linked to the selected
		//investment hierarchy
		if (bean.getColumn() instanceof SystemColumnCustom && bean.getInstrumentHierarchy() != null) {
			SystemColumnCustom column = (SystemColumnCustom) bean.getColumn();

			ValidationUtils.assertNotNull(column.getLinkedValue(), "The custom column: " + column.getLabel() + " does not have a linked value.");
			Integer linkedValue = Integer.parseInt(column.getLinkedValue());
			ValidationUtils.assertEquals(bean.getInstrumentHierarchy().getId().intValue(), linkedValue, "The custom column is not linked to the selected investment hierarchy.");

			//check the investment type and sub type to make sure they match up with the hierarchy
			InvestmentInstrumentHierarchy hierarchy = getInvestmentSetupService().getInvestmentInstrumentHierarchy(bean.getInstrumentHierarchy().getId());
			if (bean.getInvestmentType() != null) {
				ValidationUtils.assertEquals(bean.getInvestmentType(), hierarchy.getInvestmentType(), "The selected Investment Type is not linked to the selected hierarchy.");
			}

			if (bean.getInvestmentTypeSubType() != null) {
				ValidationUtils.assertEquals(bean.getInvestmentTypeSubType(), hierarchy.getInvestmentTypeSubType(), "The selected Investment Sub Type is not linked to the selected hierarchy.");
			}

			if (bean.getInvestmentTypeSubType2() != null) {
				ValidationUtils.assertEquals(bean.getInvestmentTypeSubType2(), hierarchy.getInvestmentTypeSubType2(), "The selected Investment Sub Type 2 is not linked to the selected hierarchy.");
			}
		}
	}


	@Override
	public void deleteMarketDataFieldColumnMapping(int id) {
		getMarketDataFieldColumnMappingDAO().delete(id);
	}


	@Override
	public <T extends IdentityObject> List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingListForBean(MarketDataFieldColumnMappingCommand<T> command) {
		DAOConfiguration<?> config = getDaoLocator().locate(command.getBeanClass()).getConfiguration();
		command.setTableName(config.getTableName());
		return getMarketDataFieldColumnMappingSpecificityCache().getMostSpecificResultList(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> Map<MarketDataFieldColumnMapping, Object> getMarketDataFieldValueMap(MarketDataFieldColumnMappingCommand<T> command) {
		MarketDataProvider<MarketDataValueGeneric<?>> dataProvider = getMarketDataProviderLocator().locate(command.getDataSource());

		// get the column mappings
		List<MarketDataFieldColumnMapping> columnMappings = command.getColumnMappingList() != null ? command.getColumnMappingList() : getMarketDataFieldColumnMappingListForBean(command);
		if (CollectionUtils.isEmpty(columnMappings)) {
			return null;
		}

		List<MarketDataField> dataFieldList = new ArrayList<>();
		Map<MarketDataField, List<MarketDataFieldColumnMapping>> fieldToColumnMapMap = new HashMap<>();
		for (MarketDataFieldColumnMapping columnMapping : CollectionUtils.getIterable(columnMappings)) {
			if (!columnMapping.isColumnExcluded()) {
				MarketDataField dataField = columnMapping.getDataField();
				if (!CollectionUtils.contains(dataFieldList, dataField)) {
					dataFieldList.add(dataField);
				}
				List<MarketDataFieldColumnMapping> mappings = !CollectionUtils.isEmpty(fieldToColumnMapMap.get(columnMapping.getDataField())) ?
						fieldToColumnMapMap.get(columnMapping.getDataField())
						: new ArrayList<>();

				mappings.add(columnMapping);
				fieldToColumnMapMap.put(dataField, mappings);
			}
		}

		if (CollectionUtils.isEmpty(dataFieldList)) {
			return new HashMap<>();
		}

		InvestmentSecurity securityToAdd = new InvestmentSecurity();

		securityToAdd.setSymbol(getSecuritySymbol(command));

		DataFieldValueCommand fieldValueCommand = DataFieldValueCommand.newBuilder()
				.addSecurity(securityToAdd, dataFieldList)
				.setMarketSector(command.getMarketSector())
				.setExchangeCode(command.getExchangeCode())
				.setErrors(command.getErrors())
				.build();
		DataFieldValueResponse<?> fieldValueResponse = dataProvider.getMarketDataValueLatest(fieldValueCommand);

		// create the field map from the response
		Map<MarketDataFieldColumnMapping, Object> result = new HashMap<>();
		fieldValueResponse.getMarketDataValueListForSecurity(securityToAdd).forEach(fieldValue -> {
			for (MarketDataFieldColumnMapping columnMapping : CollectionUtils.getIterable(fieldToColumnMapMap.get(fieldValue.getDataField()))) {
				result.put(columnMapping, fieldValue.getMeasureValue());
			}
		});
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> T applyMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, T object, List<String> errors, boolean applyIfNotNull) {
		if (valueMap == null) {
			return object;
		}

		// get the list of existing columns
		List<SystemColumnValue> columnValueList = null;
		if (object instanceof SystemColumnCustomValueAware) {
			columnValueList = getExistingColumnValueList((SystemColumnCustomValueAware) object);
			if (columnValueList == null) {
				columnValueList = new ArrayList<>();
			}
		}

		// for each column mapping apply the value to the object
		for (Map.Entry<MarketDataFieldColumnMapping, Object> marketDataFieldColumnMappingObjectEntry : valueMap.entrySet()) {
			if (!(marketDataFieldColumnMappingObjectEntry.getKey()).isColumnExcluded()) {
				SystemColumn column = resolveSystemColumnFromTemplate(marketDataFieldColumnMappingObjectEntry.getKey(), object);
				if (column == null) {
					column = (marketDataFieldColumnMappingObjectEntry.getKey()).getColumn();
				}
				if (column != null) {
					ColumnMappingValueResolverCommand<T> command = new ColumnMappingValueResolverCommand<>(columnValueList, column, marketDataFieldColumnMappingObjectEntry.getKey(), applyIfNotNull, marketDataFieldColumnMappingObjectEntry.getValue(), object);
					resolveAndSaveProperty(command, errors);
				}
			}
		}

		if (object instanceof SystemColumnCustomValueAware) {
			((SystemColumnCustomValueAware) object).setColumnValueList(columnValueList);
		}

		return object;
	}


	@Override
	public List<SystemColumnCustom> resolveSystemColumnListFromTemplate(MarketDataFieldColumnMapping mapping) {
		//look up all the columns that use the template
		SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
		searchForm.setTemplateId(mapping.getTemplate().getId());
		searchForm.setColumnGroupId(mapping.getTemplate().getSystemColumnGroup().getId());
		//TODO: find better way of doing this

		//handle investment securities and instruments
		if ("InvestmentSecurity".equals(mapping.getTemplate().getSystemColumnGroup().getTable().getName())) {
			if (mapping.getInstrumentHierarchy() != null) {
				searchForm.setLinkedValue(mapping.getInstrumentHierarchy().getId().toString());
			}
			else {
				// query for a list of hierarchies with the type and sub type if set and do an 'in' on the linked value
				InstrumentHierarchySearchForm hierarchySearchForm = new InstrumentHierarchySearchForm();
				if (mapping.getInvestmentType() != null) {
					hierarchySearchForm.setInvestmentTypeId(mapping.getInvestmentType().getId());
				}

				if (mapping.getInvestmentTypeSubType() != null) {
					hierarchySearchForm.setInvestmentTypeSubTypeId(mapping.getInvestmentTypeSubType().getId());
				}

				if (mapping.getInvestmentTypeSubType2() != null) {
					hierarchySearchForm.setInvestmentTypeSubType2Id(mapping.getInvestmentTypeSubType2().getId());
				}

				List<InvestmentInstrumentHierarchy> hierarchies = getInvestmentSetupService().getInvestmentInstrumentHierarchyList(hierarchySearchForm);
				if (!CollectionUtils.isEmpty(hierarchies)) {
					List<String> linkedValues = new ArrayList<>();
					for (InvestmentInstrumentHierarchy hierarchy : hierarchies) {
						linkedValues.add(hierarchy.getId().toString());
					}
					String[] valueArray = new String[linkedValues.size()];
					valueArray = linkedValues.toArray(valueArray);
					searchForm.setLinkedValues(valueArray);
				}
			}
		}
		else {
			if (mapping.getInvestmentType() != null || mapping.getInstrumentHierarchy() != null) {
				searchForm.setLinkedValue(mapping.getInvestmentType() != null ? mapping.getInvestmentType().getId().toString() : mapping.getInstrumentHierarchy().getInvestmentType().getId().toString());
			}
		}

		return getSystemColumnService().getSystemColumnCustomList(searchForm);
	}


	/**
	 * Evaluates the given mapping for the given object to determine the system column to use for the data.
	 *
	 * @param mapping the mapping object
	 * @param object  the object upon which the mapping will be applied
	 * @param <T>     the type of the object upon which the mapping will be applied
	 * @return the system column resolved via the mapping evaluation
	 */
	@Override
	public <T extends IdentityObject> SystemColumn resolveSystemColumnFromTemplate(MarketDataFieldColumnMapping mapping, T object) {
		SystemColumn column = null;
		if (mapping.getTemplate() != null) {
			Integer templateId = mapping.getTemplate().getId();
			Short columnGroupId = mapping.getTemplate().getSystemColumnGroup().getId();
			String linkedValue = null;
			Object beanPropertyValue = BeanUtils.getPropertyValue(object, mapping.getTemplate().getSystemColumnGroup().getLinkedBeanProperty());
			if (beanPropertyValue != null) {
				linkedValue = new ObjectToStringConverter().convert(beanPropertyValue);
			}

			// Attempt to retrieve from cache
			column = getMarketDataFieldColumnMappingSystemColumnCache().getSystemColumnForMapping(templateId, columnGroupId, linkedValue);
			if (column == null) {
				// Not cached; search for value and update cache
				SystemColumnSearchForm searchForm = new SystemColumnSearchForm();
				searchForm.setTemplateId(templateId);
				searchForm.setColumnGroupId(columnGroupId);
				searchForm.setLinkedValue(linkedValue);
				try {
					column = CollectionUtils.getOnlyElement(getSystemColumnService().getSystemColumnList(searchForm));
					getMarketDataFieldColumnMappingSystemColumnCache().setSystemColumnForMapping(templateId, columnGroupId, linkedValue, column);
				}
				catch (Throwable e) {
					throw new RuntimeException("Failed to resolve column for template [" + mapping.getTemplate().getLabel() + "].", e);
				}
			}
		}
		return column;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject> void resolveAndSaveProperty(ColumnMappingValueResolverCommand<T> command, List<String> errors) {
		try {
			if (command.getColumn().isCustomColumn() && command.getTargetObject() instanceof SystemColumnCustomValueAware) {
				SystemColumnValue scv = getMarketDataFieldColumnMappingValueResolver().resolve(command);
				if (scv != null && (scv.isNewBean() || !command.getColumnValueList().contains(scv))) {
					command.getColumnValueList().add(scv);
				}
			}
			else {
				Object value = getMarketDataFieldColumnMappingValueResolver().resolve(command);
				BeanUtils.setPropertyValue(command.getTargetObject(), ((SystemColumnStandard) command.getColumn()).getBeanPropertyName(), value);
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to load field [" + command.getColumn().getName() + "].", e);

			// full log for first error and only causes for consecutive errors
			StringBuilder error = (new StringBuilder())
					.append("Failed populate column [").append(command.getColumn().getName()).append("].")
					.append("\nCAUSED BY ")
					.append(ExceptionUtils.getDetailedMessage(e));
			errors.add(error.toString());
		}
	}


	private List<SystemColumnValue> getExistingColumnValueList(SystemColumnCustomValueAware bean) {
		if (!bean.isNewBean()) {
			SystemColumnValueSearchForm sf = new SystemColumnValueSearchForm();
			sf.setColumnGroupName(bean.getColumnGroupName());
			sf.setFkFieldId(BeanUtils.getIdentityAsInteger(bean));
			return getSystemColumnValueService().getSystemColumnValueList(sf);
		}
		return null;
	}


	private <T extends IdentityObject> String getSecuritySymbol(MarketDataFieldColumnMappingCommand<T> command) {

		StringBuilder stringBuilder = new StringBuilder(command.getSymbol());

		if (command.getExchangeCode() != null) {
			stringBuilder.append(" ").append(command.getExchangeCode());
		}
		else if (command.getSecurityId() != null) {
			InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(command.getSecurityId());
			MarketDataFieldMapping fieldMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), command.getDataSource());
			if (fieldMapping != null && fieldMapping.getExchangeCodeType() != null) {
				stringBuilder.append(" ").append(fieldMapping.getExchangeCodeType().getSecurityExchangeCode(security));
			}
		}
		return stringBuilder.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataFieldColumnMapping, Criteria> getMarketDataFieldColumnMappingDAO() {
		return this.marketDataFieldColumnMappingDAO;
	}


	public void setMarketDataFieldColumnMappingDAO(AdvancedUpdatableDAO<MarketDataFieldColumnMapping, Criteria> marketDataFieldColumnMappingDAO) {
		this.marketDataFieldColumnMappingDAO = marketDataFieldColumnMappingDAO;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public InvestmentSetupService getInvestmentSetupService() {
		return this.investmentSetupService;
	}


	public void setInvestmentSetupService(InvestmentSetupService investmentSetupService) {
		this.investmentSetupService = investmentSetupService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public SpecificityCache<MarketDataFieldColumnMappingCommand<?>, MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingSpecificityCache() {
		return this.marketDataFieldColumnMappingSpecificityCache;
	}


	public void setMarketDataFieldColumnMappingSpecificityCache(SpecificityCache<MarketDataFieldColumnMappingCommand<?>, MarketDataFieldColumnMapping> marketDataFieldColumnMappingSpecificityCache) {
		this.marketDataFieldColumnMappingSpecificityCache = marketDataFieldColumnMappingSpecificityCache;
	}


	public MarketDataFieldColumnMappingSystemColumnCache getMarketDataFieldColumnMappingSystemColumnCache() {
		return this.marketDataFieldColumnMappingSystemColumnCache;
	}


	public void setMarketDataFieldColumnMappingSystemColumnCache(MarketDataFieldColumnMappingSystemColumnCache marketDataFieldColumnMappingSystemColumnCache) {
		this.marketDataFieldColumnMappingSystemColumnCache = marketDataFieldColumnMappingSystemColumnCache;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataFieldColumnMappingValueResolver getMarketDataFieldColumnMappingValueResolver() {
		return this.marketDataFieldColumnMappingValueResolver;
	}


	public void setMarketDataFieldColumnMappingValueResolver(MarketDataFieldColumnMappingValueResolver marketDataFieldColumnMappingValueResolver) {
		this.marketDataFieldColumnMappingValueResolver = marketDataFieldColumnMappingValueResolver;
	}
}

