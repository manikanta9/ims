package com.clifton.marketdata.field.column;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnTemplate;


/**
 * The <code>MarketDataFieldColumnMapping</code> maps source market data fields to target system columns.
 * Data fields will be used to populate column values from corresponding market data source.
 * <p>
 * The mapping also defines the scope (investment type/subtype/hierarchy and exclusion) that should be used
 * to make mappings for specific to different investment securities.
 *
 * @author mwacker
 */
public class MarketDataFieldColumnMapping extends BaseEntity<Integer> {

	private MarketDataSource dataSource;
	private MarketDataField dataField;

	private SystemColumn column;

	private SystemColumnTemplate template;
	/**
	 * The bean property name used to look up the object if it's a foreign key.
	 * <p>
	 * For example, to look up trading currency the field would be symbol.
	 */
	private String beanPropertyNameForLookup;

	////////////////////////////////////////////////////////////////////////////
	/////////////////////////  SCOPE DEFINITION  ///////////////////////////////

	/**
	 * Used to map field to specific investment types.
	 * <p>
	 * NOTE: Takes precedence over a mapping without hierarchy and/or investment type. Both can be defined for the same column.
	 */
	private InvestmentType investmentType;
	/**
	 * Used to map field to specific investment sub types.
	 */
	private InvestmentTypeSubType investmentTypeSubType;
	/**
	 * Used to map field to specific investment sub type 2's
	 */
	private InvestmentTypeSubType2 investmentTypeSubType2;
	/**
	 * Nullable InvestmentInstrumentHierarchy used to map fields to specific hierarchies.
	 * <p>
	 * NOTE: Takes precedence over a mapping without hierarchy and/or investment type. Both can be defined for the same column.
	 */
	private InvestmentInstrumentHierarchy instrumentHierarchy;
	/**
	 * Indicates that the mapped column should be excluded.
	 */
	private boolean columnExcluded;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to get the table from the template or custom column depending on which is set
	 *
	 * @return The table of the template or column
	 */
	public SystemTable getTable() {
		return getTemplate() != null ? getTemplate().getSystemColumnGroup().getTable() : (getColumn() != null ? getColumn().getTable() : null);
	}


	public String getTableName() {
		return getTable() != null ? getTable().getName() : null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataField getDataField() {
		return this.dataField;
	}


	public void setDataField(MarketDataField dataField) {
		this.dataField = dataField;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public SystemColumn getColumn() {
		return this.column;
	}


	public void setColumn(SystemColumn column) {
		this.column = column;
	}


	public String getBeanPropertyNameForLookup() {
		return this.beanPropertyNameForLookup;
	}


	public void setBeanPropertyNameForLookup(String beanPropertyNameForLookup) {
		this.beanPropertyNameForLookup = beanPropertyNameForLookup;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public boolean isColumnExcluded() {
		return this.columnExcluded;
	}


	public void setColumnExcluded(boolean columnExcluded) {
		this.columnExcluded = columnExcluded;
	}


	public SystemColumnTemplate getTemplate() {
		return this.template;
	}


	public void setTemplate(SystemColumnTemplate template) {
		this.template = template;
	}
}
