package com.clifton.marketdata.field.column.cache;

import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.system.schema.column.SystemColumn;


/**
 * The {@link MarketDataFieldColumnMappingSystemColumnCache} class provides caching for {@link SystemColumn}
 * objects mapped via {@link MarketDataFieldMapping} mappings.
 *
 * @author MikeH
 */
public interface MarketDataFieldColumnMappingSystemColumnCache {

	/**
	 * Gets the system column from the cache for the mapping specified by the provided arguments.
	 *
	 * @param templateId    the template ID for the mapping
	 * @param columnGroupId the column group ID for the mapping
	 * @param linkedValue   the linked value for the mapping
	 * @return the cached item, or {@code null} if no cached item exists
	 */
	public SystemColumn getSystemColumnForMapping(Integer templateId, Short columnGroupId, String linkedValue);


	/**
	 * Sets the system column within the cache for the mapping specified by the provided arguments.
	 *
	 * @param templateId    the template ID for the mapping
	 * @param columnGroupId the column group ID for the mapping
	 * @param linkedValue   the linked value for the mapping
	 * @param column        the item to cache
	 */
	public void setSystemColumnForMapping(Integer templateId, Short columnGroupId, String linkedValue, SystemColumn column);
}
