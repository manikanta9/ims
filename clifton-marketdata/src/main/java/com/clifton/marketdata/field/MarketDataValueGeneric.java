package com.clifton.marketdata.field;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;

import java.util.Date;


/**
 * The <code>MarketDataValueGeneric</code> class represents entities that periodically capture values for various market data fields.
 * Most common use case is to capture prices. But the value can also store Duration, Delta, Shares Outstanding, or any security attributes.
 * <p>
 * This class can be extended by typed implementations: BigDecimal, Date, etc.
 *
 * @author vgomelsky
 */
public class MarketDataValueGeneric<T> extends BaseEntity<Integer> implements LabeledObject {

	private MarketDataField dataField;
	private MarketDataSource dataSource;
	private InvestmentSecurity investmentSecurity;

	private Date measureDate;
	/**
	 * Applies only when dataField.timeSensitive = true
	 */
	private Time measureTime;
	private T measureValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append((this.dataField == null) ? "???" : this.dataField.getName());
		result.append(" for ");
		result.append((this.investmentSecurity == null) ? "???" : this.investmentSecurity.getSymbol());
		result.append(" from ");
		result.append((this.dataSource == null) ? "???" : this.dataSource.getName());
		result.append(" on ");
		if (this.measureTime == null) {
			result.append(DateUtils.fromDateShort(this.measureDate));
		}
		else {
			result.append(DateUtils.fromDate(DateUtils.setTime(this.measureDate, this.measureTime), DateUtils.DATE_FORMAT_SHORT));
		}
		result.append(" was ");
		result.append(this.measureValue == null);
		return result.toString();
	}


	public Date getMeasureDateWithTime() {
		if (this.measureTime == null) {
			return this.measureDate;
		}
		return DateUtils.setTime(this.measureDate, this.measureTime);
	}


	public MarketDataField getDataField() {
		return this.dataField;
	}


	public void setDataField(MarketDataField dataField) {
		this.dataField = dataField;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public T getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(T measureValue) {
		this.measureValue = measureValue;
	}


	public Time getMeasureTime() {
		return this.measureTime;
	}


	public void setMeasureTime(Time measureTime) {
		this.measureTime = measureTime;
	}
}
