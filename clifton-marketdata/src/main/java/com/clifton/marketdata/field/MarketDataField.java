package com.clifton.marketdata.field;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.util.math.BinaryOperators;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.hierarchy.definition.SystemHierarchyCategory;
import com.clifton.system.schema.SystemDataType;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;


/**
 * The <code>MarketDataField</code> class defines a single field/measure of market data.
 * For example, Trading Volume, Last Trade Price, Settlement Price, Delta, Duration, etc.
 *
 * @author vgomelsky
 */
@CacheByName
public class MarketDataField extends NamedEntity<Short> implements SystemEntityModifyConditionAwareAdminRequired {

	/**
	 * The {@link SystemHierarchyCategory} name for tags which may apply to {@link MarketDataField} entities.
	 */
	public static final String MARKET_DATA_TAG_CATEGORY_NAME = "Market Data Tags";

	public static final String FIELD_CREDIT_DURATION = "Credit Duration";
	public static final String FIELD_DELTA = "Delta";
	public static final String FIELD_DURATION = "Duration";
	public static final String FIELD_DURATION_SPOT = "Duration (Spot)";
	public static final String FIELD_DV01 = "DV01";
	public static final String FIELD_DV01_SPOT = "DV01 (Spot)";
	public static final String FIELD_FUTURE_FAIR_VALUE = "Future Fair Value";
	public static final String FIELD_LAST_TRADE_PRICE = "Last Trade Price";
	public static final String FIELD_INDEX_RATIO = "Value";
	public static final String FIELD_INDEX_RATIO_CPI = "Index Ratio - CPI";
	public static final String FIELD_DEBT_OUTSTANDING = "Debt Outstanding";
	public static final String FIELD_SHARES_OUTSTANDING = "Shares Outstanding";
	public static final String FIELD_BLACK_SCHOLES_PRICE = "Black-Scholes Price";
	public static final String FIELD_PRICE_CHANGE_PERCENT_YTD = "Price Change Percent - YTD";
	public static final String FIELD_PRICE_CHANGE_PERCENT_1DAY = "Price Change Percent - 1 Day";
	public static final String FIELD_EARNINGS_REPORT_DATE = "Earnings Report Date";

	public static final String FIELD_MONTHLY_RETURN = "Monthly Return";

	// Fields used for theoretical value calculations such as pricing FLEX Options for DeltaShift
	public static final String FIELD_VOLATILITY = "Volatility";
	public static final String FIELD_OPTION_UNDERLYING_DIVIDEND_YIELD = "Option Underlying Dividend Yield";
	public static final String FIELD_OPTION_FINANCE_RATE = "Option Finance Rate";
	public static final String FIELD_DIVIDEND_SCHEDULE = "Dividend Schedule";

	// MarketDataField external field names used for subscription processing.
	public static final String SUBSCRIPTION_EXTERNAL_FIELD_VOLATILITY = "IVOL_MID_RT";
	public static final String SUBSCRIPTION_EXTERNAL_FIELD_LAST_PRICE = "LAST_PRICE";
	public static final String SUBSCRIPTION_EXTERNAL_FIELD_DELTA = "DELTA_MID_RT";

	////////////////////////////////////////////////////////////////////////////

	/**
	 * For some fields the "order" field has special meaning, i.e. for KRD Points, it is the number
	 * of months represented by the KRD Point - 6 Month KRD = 6, 5 Year KRD = 60
	 * <p>
	 * When associated with a FieldOrderGroup, the group may require fieldOrder on the MarketData field to ensure
	 * it is populated when needed.
	 */
	private Short fieldOrder;

	/**
	 * Optionally specifies if values for this data field should be stored under valueOverrideDataField.
	 * For example, there maybe different formulas used to calculate the same data field for different securities:
	 * "Delta", "OTC Equity Options Delta", "OTC Currency Options Delta", etc.  Even though they are defined as
	 * different data fields, they represent the same value "Delta".  If we always store this data fields as "Delta",
	 * the this will significantly simplify code logic that needs the "Delta" field.
	 */
	private MarketDataField valueOverrideDataField;

	/**
	 * Optional {@link com.clifton.marketdata.live.MarketDataLiveFieldValueCalculator} that can be used to retrieve a live value
	 * as an alternative to the standard {@link com.clifton.marketdata.provider.MarketDataProvider}.
	 */
	private SystemBean liveValueCalculator;
	/**
	 * Condition to be used to test if the {@link #liveValueCalculatorCondition} can be applied to a particular security.
	 * A use case for this is in live pricing for Delta, Ask, Bid of Options. Flex options can use a calculator instead of from MarketData source.
	 */
	private SystemCondition liveValueCalculatorCondition;

	/**
	 * The number of decimal places allowed for values
	 */
	private int decimalPrecision;

	/**
	 * Allows up to one value from a data source per day
	 */
	private boolean upToOnePerDay;

	/**
	 * Store only the latest value (Bid/Ask): updates historic values instead of adding new.
	 */
	private boolean latestValueOnly;

	/**
	 * Specifies whether time component of the measure is applicable and should be captured.
	 * Daily measures (settlement price, volume, etc.) do not require time.
	 */
	private boolean timeSensitive;

	/**
	 * Don't store value for this data field if it's the same as previous value: update date instead.
	 * Store only new values: factor changes, etc.
	 */
	private boolean captureChangesOnly;

	/**
	 * If <code>true</code>, this field is used to store real-time reference data that is used by other entities. This data can be referenced by entities such as trades or position
	 * transfers and through mechanisms such as trade market data fields. Data for these fields is typically not used for reporting or other standard market data lookups.
	 */
	private boolean realTimeReferenceData;

	/**
	 * What this field is called in an external system.  For now this will be Bloomberg field names.
	 */
	private String externalFieldName;

	/**
	 * Optional field overrides for market request for latest data (Bloomberg reference data).
	 * Must be in the following format: "field1=value1,field2=value2"
	 */
	private String externalFieldOverridesForLatest;
	/**
	 * Optional field overrides for market request for historic data (Bloomberg historic data).
	 * Must be in the following format: "field1=value1,field2=value2".
	 * Can use the following dynamic values:
	 * - ${DATE}
	 * <p>
	 * For example for 11/28/2012 valuation date, the following string "SETTLE_DT=${DATE},SW_CURVE_DT=${DATE}"
	 * will be converted into "SETTLE_DT=20121128,SW_CURVE_DT=20121128"
	 */
	private String externalFieldOverridesForHistoric;
	/**
	 * Optionally base the value of this field off of an expression.
	 * This expression contains other fields and the operations to perform on them.
	 * <p>
	 * Ex. Swap Price - IRS ValueExpression: ((${SW_VAL_PREMIUM}+${SW_PAY_NOTL_AMT})/${SW_PAY_NOTL_AMT})/${investmentSecurity.instrument.priceMultiplier}
	 * Ex. Mid - Calculated ValueExpression: (${PX_ASK}+${PX_BID})/2
	 */
	private String valueExpression;
	/**
	 * The data type of the field.
	 */
	private SystemDataType dataType;
	/**
	 * For fields with values that support adjustment factor, specifies the operator (MULTIPLY, DIVIDE, etc.) to use when adjusting historical values.
	 * If not set, then no adjustment is needed.  Stock Split example: DIVIDE prides and MULTIPLY shares outstanding.
	 */
	private BinaryOperators adjustmentType;
	/**
	 * Use symbolOverrideLookupField and symbolOverrideMarketSector to look up new security symbol and replace
	 * the existing one with it for the rest of processing.
	 * <p>
	 * Ex.  Look up market data for a security where the lookup symbol is actually that securities underlying security symbol
	 */
	private String symbolOverrideLookupField;
	private MarketDataSourceSector symbolOverrideMarketSector;

	/**
	 * Defines the timeout (in milliseconds) for request made to external systems for this field.
	 */
	private Integer requestTimeoutMillis;

	/**
	 * If true, names cannot be edited.
	 */
	private boolean systemDefined;

	private SystemCondition entityModifyCondition;

	/**
	 * Can be used to skip inserts of new values that match the condition.
	 * For example, KRD Points we can ignore 0 values as the system already assumes 0 value
	 * when not present.  This accounts for thousands of records daily that are unnecessary and take additional processing time (especially during uploads)
	 */
	private SystemCondition ignoreValueCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataField() {
		super();
	}


	public MarketDataField(String externalFieldName) {
		this.externalFieldName = externalFieldName;
	}


	// Allow only Admins to modify system defined entities
	@Override
	public boolean isEntityModifyConditionRequiredForNonAdmins() {
		return isSystemDefined();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}


	public MarketDataField getValueOverrideDataField() {
		return this.valueOverrideDataField;
	}


	public void setValueOverrideDataField(MarketDataField valueOverrideDataField) {
		this.valueOverrideDataField = valueOverrideDataField;
	}


	public SystemBean getLiveValueCalculator() {
		return this.liveValueCalculator;
	}


	public void setLiveValueCalculator(SystemBean liveValueCalculator) {
		this.liveValueCalculator = liveValueCalculator;
	}


	public SystemCondition getLiveValueCalculatorCondition() {
		return this.liveValueCalculatorCondition;
	}


	public void setLiveValueCalculatorCondition(SystemCondition liveValueCalculatorCondition) {
		this.liveValueCalculatorCondition = liveValueCalculatorCondition;
	}


	public int getDecimalPrecision() {
		return this.decimalPrecision;
	}


	public void setDecimalPrecision(int decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public boolean isUpToOnePerDay() {
		return this.upToOnePerDay;
	}


	public void setUpToOnePerDay(boolean upToOnePerDay) {
		this.upToOnePerDay = upToOnePerDay;
	}


	public boolean isLatestValueOnly() {
		return this.latestValueOnly;
	}


	public void setLatestValueOnly(boolean latestValueOnly) {
		this.latestValueOnly = latestValueOnly;
	}


	public boolean isTimeSensitive() {
		return this.timeSensitive;
	}


	public void setTimeSensitive(boolean timeSensitive) {
		this.timeSensitive = timeSensitive;
	}


	public boolean isCaptureChangesOnly() {
		return this.captureChangesOnly;
	}


	public void setCaptureChangesOnly(boolean captureChangesOnly) {
		this.captureChangesOnly = captureChangesOnly;
	}


	public boolean isRealTimeReferenceData() {
		return this.realTimeReferenceData;
	}


	public void setRealTimeReferenceData(boolean realTimeReferenceData) {
		this.realTimeReferenceData = realTimeReferenceData;
	}


	public String getExternalFieldName() {
		return this.externalFieldName;
	}


	public void setExternalFieldName(String externalFieldName) {
		this.externalFieldName = externalFieldName;
	}


	public String getExternalFieldOverridesForLatest() {
		return this.externalFieldOverridesForLatest;
	}


	public void setExternalFieldOverridesForLatest(String externalFieldOverridesForLatest) {
		this.externalFieldOverridesForLatest = externalFieldOverridesForLatest;
	}


	public String getExternalFieldOverridesForHistoric() {
		return this.externalFieldOverridesForHistoric;
	}


	public void setExternalFieldOverridesForHistoric(String externalFieldOverridesForHistoric) {
		this.externalFieldOverridesForHistoric = externalFieldOverridesForHistoric;
	}


	public String getValueExpression() {
		return this.valueExpression;
	}


	public void setValueExpression(String valueExpression) {
		this.valueExpression = valueExpression;
	}


	public SystemDataType getDataType() {
		return this.dataType;
	}


	public void setDataType(SystemDataType dataType) {
		this.dataType = dataType;
	}


	public BinaryOperators getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(BinaryOperators adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public String getSymbolOverrideLookupField() {
		return this.symbolOverrideLookupField;
	}


	public void setSymbolOverrideLookupField(String symbolOverrideLookupField) {
		this.symbolOverrideLookupField = symbolOverrideLookupField;
	}


	public MarketDataSourceSector getSymbolOverrideMarketSector() {
		return this.symbolOverrideMarketSector;
	}


	public void setSymbolOverrideMarketSector(MarketDataSourceSector symbolOverrideMarketSector) {
		this.symbolOverrideMarketSector = symbolOverrideMarketSector;
	}


	public Integer getRequestTimeoutMillis() {
		return this.requestTimeoutMillis;
	}


	public void setRequestTimeoutMillis(Integer requestTimeoutMillis) {
		this.requestTimeoutMillis = requestTimeoutMillis;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	@Override
	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public SystemCondition getIgnoreValueCondition() {
		return this.ignoreValueCondition;
	}


	public void setIgnoreValueCondition(SystemCondition ignoreValueCondition) {
		this.ignoreValueCondition = ignoreValueCondition;
	}
}
