package com.clifton.marketdata.field.mapping;

import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;


/**
 * MarketDataPriceFieldTypes defines the types of prices we can have for each security: Official, Closing, Latest
 * and how to retrieve the data fields and data sources from a MarketDataPriceFieldMapping object
 * <p>
 * Handles null mappings
 * and returns Default Price Field (Last Trade Price) for Latest and Closing if mapping is null, or that field is not defined on the mapping
 *
 * @author manderson
 */
public enum MarketDataPriceFieldTypes {

	LATEST {
		@Override
		public MarketDataField getPriceField(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			if (priceFieldMapping == null || priceFieldMapping.getLatestPriceMarketDataField() == null) {
				return marketDataFieldService.getMarketDataPriceFieldDefault();
			}
			return priceFieldMapping.getLatestPriceMarketDataField();
		}


		@Override
		public MarketDataField getPriceFieldIfDifferent(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			String officialPriceKey = OFFICIAL.getPriceKey(priceFieldMapping, marketDataFieldService);
			String closingPriceKey = CLOSING.getPriceKey(priceFieldMapping, marketDataFieldService);
			String latestPriceKey = getPriceKey(priceFieldMapping, marketDataFieldService);
			if (latestPriceKey.contains(officialPriceKey) || latestPriceKey.contains(closingPriceKey)) {
				return null;
			}
			return getPriceField(priceFieldMapping, marketDataFieldService);
		}


		@Override
		public MarketDataSource getPriceDataSource(MarketDataPriceFieldMapping priceFieldMapping) {
			if (priceFieldMapping == null) {
				return null;
			}
			return priceFieldMapping.getLatestPriceMarketDataSource();
		}
	},

	CLOSING {
		@Override
		public MarketDataField getPriceField(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			if (priceFieldMapping == null || priceFieldMapping.getClosingPriceMarketDataField() == null) {
				return marketDataFieldService.getMarketDataPriceFieldDefault();
			}
			return priceFieldMapping.getClosingPriceMarketDataField();
		}


		@Override
		public MarketDataField getPriceFieldIfDifferent(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			String officialPriceKey = OFFICIAL.getPriceKey(priceFieldMapping, marketDataFieldService);
			String closingPriceKey = getPriceKey(priceFieldMapping, marketDataFieldService);
			if (closingPriceKey.contains(officialPriceKey)) {
				return null;
			}
			return getPriceField(priceFieldMapping, marketDataFieldService);
		}


		@Override
		public MarketDataSource getPriceDataSource(MarketDataPriceFieldMapping priceFieldMapping) {
			if (priceFieldMapping == null) {
				return null;
			}
			return priceFieldMapping.getClosingPriceMarketDataSource();
		}
	},

	OFFICIAL {
		@Override
		public MarketDataField getPriceField(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			if (priceFieldMapping == null || priceFieldMapping.getOfficialPriceMarketDataField() == null) {
				return null; // No Default for Official
			}
			return priceFieldMapping.getOfficialPriceMarketDataField();
		}


		@Override
		public MarketDataField getPriceFieldIfDifferent(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
			// Official Price is ALWAYS the first attempt and most specific - so just return it
			return getPriceField(priceFieldMapping, marketDataFieldService);
		}


		@Override
		public MarketDataSource getPriceDataSource(MarketDataPriceFieldMapping priceFieldMapping) {
			if (priceFieldMapping == null) {
				return null;
			}
			return priceFieldMapping.getOfficialPriceMarketDataSource();
		}
	};

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public abstract MarketDataField getPriceField(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService);


	/**
	 * Returns the price field if it would require a second look up because it's different than it's more specific types
	 * Also compares data sources which could require an additional look up if one is more restrictive
	 */
	public abstract MarketDataField getPriceFieldIfDifferent(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService);


	public abstract MarketDataSource getPriceDataSource(MarketDataPriceFieldMapping priceFieldMapping);


	public Short getPriceDataSourceId(MarketDataPriceFieldMapping priceFieldMapping) {
		MarketDataSource dataSource = getPriceDataSource(priceFieldMapping);
		return (dataSource != null ? dataSource.getId() : null);
	}


	protected String getPriceKey(MarketDataPriceFieldMapping priceFieldMapping, MarketDataFieldService marketDataFieldService) {
		MarketDataField dataField = getPriceField(priceFieldMapping, marketDataFieldService);
		if (dataField != null) {
			Short dataSourceId = getPriceDataSourceId(priceFieldMapping);
			if (dataSourceId != null) {
				return dataField.getId() + "_" + dataSourceId;
			}
			return dataField.getId() + "";
		}
		return "NONE";
	}
}
