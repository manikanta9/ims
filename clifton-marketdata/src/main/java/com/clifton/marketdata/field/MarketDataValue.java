package com.clifton.marketdata.field;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>MarketDataValue</code> class stores numeric (BigDecimal) market data values.
 *
 * @author vgomelsky
 */
public class MarketDataValue extends MarketDataValueGeneric<BigDecimal> {

	/**
	 * Adjusted value is usually the same as measureValue (adjustmentFactor == 1).
	 * It is updated by the system to adjust for splits (stock splits), so that
	 * pre-split values are comparable to post-split values.
	 */
	private BigDecimal measureValueAdjustmentFactor = BigDecimal.ONE;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(64);
		result.append((getDataField() == null) ? "???" : getDataField().getName());
		result.append(" for ");
		result.append((getInvestmentSecurity() == null) ? "???" : getInvestmentSecurity().getSymbol());
		result.append(" from ");
		result.append((getDataSource() == null) ? "???" : getDataSource().getName());
		result.append(" on ");
		if (getMeasureTime() == null) {
			result.append(DateUtils.fromDateShort(getMeasureDate()));
		}
		else {
			result.append(DateUtils.fromDate(DateUtils.setTime(getMeasureDate(), getMeasureTime()), DateUtils.DATE_FORMAT_SHORT));
		}
		result.append(" was ");
		// format scale: doesn't change the scale but returns new or existing BigDecimal with proper scale
		result.append((getMeasureValue() == null) ? null : CoreMathUtils.formatNumberDecimal(getMeasureValue()));
		return result.toString();
	}


	/**
	 * Adjusted Value = Measure Value * Adjustment Factor
	 */
	public BigDecimal getMeasureValueAdjusted() {
		BigDecimal result = getMeasureValue();
		if (!BigDecimal.ONE.equals(getMeasureValueAdjustmentFactor())) {
			result = MathUtils.multiply(result, getMeasureValueAdjustmentFactor(), getDataField() == null ? 0 : getDataField().getDecimalPrecision());
		}
		return result;
	}


	public BigDecimal getMeasureValueAdjustmentFactor() {
		return this.measureValueAdjustmentFactor;
	}


	public void setMeasureValueAdjustmentFactor(BigDecimal measureValueAdjustmentFactor) {
		this.measureValueAdjustmentFactor = measureValueAdjustmentFactor;
	}
}
