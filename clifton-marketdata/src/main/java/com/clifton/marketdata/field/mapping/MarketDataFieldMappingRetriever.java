package com.clifton.marketdata.field.mapping;

import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.marketdata.datasource.MarketDataSource;


/**
 * The <code>MarketDataFieldMappingRetriever</code> class handles retrieval of various Market Data Mapping objects, specifically those retrieval types methods that utilize cache look ups
 *
 * @author manderson
 */
public interface MarketDataFieldMappingRetriever {

	////////////////////////////////////////////////////////////////////////////
	//////         Market Data Field Mapping Retrieval Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up the mapping by the instrument.  If none is found, it then looks at the hierarchy of the instrument to see if there is a mapping.
	 * Then by Investment Sub-Type and Sub-Type 2 and finally by Investment Type.  Uses specificity cache.
	 */
	public MarketDataFieldMapping getMarketDataFieldMappingByInstrument(InvestmentInstrument instrument, MarketDataSource dataSource);


	public MarketDataFieldMapping getMarketDataFieldMappingByHierarchy(InvestmentInstrumentHierarchy hierarchy, MarketDataSource dataSource);


	////////////////////////////////////////////////////////////////////////////
	//////      Market Data Price Field Mapping Retrieval Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up the mapping by the instrument.  Uses specificity cache
	 * 1. Instrument Specific
	 * 2. Hierarchy Specific
	 * 3. Type/SubType/SubType2 Specific
	 * 4. Type/SubType2 Specific
	 * 5. Type/SubType Specific
	 * 6. Type Specific
	 */
	public MarketDataPriceFieldMapping getMarketDataPriceFieldMappingByInstrument(InvestmentInstrument instrument);
}
