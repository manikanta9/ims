package com.clifton.marketdata.field.upload;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.MarketDataValue;


public interface MarketDataFieldUploadService {

	/**
	 * Save method for MarketDataValue to be used by all uploads for MarketDataValue (simple or regular)
	 * Defaults Measure Adjustment Factor to 1 if null and Time (if required by the field type) and missing, will set it to 12 AM
	 */
	public MarketDataValue saveMarketDataValueFromUpload(MarketDataValue bean);


	/**
	 * Uploads MarketDataValue(s) into the system
	 * Performs proper default setting for fields, and validation
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public void uploadMarketDataValueUploadFile(MarketDataValueUploadCommand uploadCommand);
}
