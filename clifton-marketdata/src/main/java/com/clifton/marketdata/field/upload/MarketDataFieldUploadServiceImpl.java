package com.clifton.marketdata.field.upload;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.upload.SystemUploadHandler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>MarketDataFieldUploadServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class MarketDataFieldUploadServiceImpl implements MarketDataFieldUploadService {

	private MarketDataFieldService marketDataFieldService;
	private MarketDataSourceService marketDataSourceService;
	private SystemUploadHandler systemUploadHandler;

	private InvestmentInstrumentService investmentInstrumentService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataValue saveMarketDataValueFromUpload(MarketDataValue bean) {
		// Two defaults when coming from Uploads for all cases - if no measure value adjustment factor, then use 1
		if (bean.getMeasureValueAdjustmentFactor() == null) {
			bean.setMeasureValueAdjustmentFactor(BigDecimal.ONE);
		}

		// If field requires time, but none specified, use 12:00 AM
		MarketDataField field = bean.getDataField();
		ValidationUtils.assertNotNull(field, "Market Data Field is required for " + bean.getLabel(), "dataField");
		if (field.isTimeSensitive() && bean.getMeasureTime() == null) {
			bean.setMeasureTime(new Time(0));
		}

		// Strange Case for upload where sometimes the value in Excel looks normal, but comes
		// into the system with a bunch of zeroes and then 1 or a bunch of 9s.  Editing the file
		// and re-entering the value manually seems to fix it, however for these we can just round
		// and that should fix it for saves
		String stringValue = bean.getMeasureValue().toPlainString();
		int decimalPlaces = (stringValue.indexOf('.') == -1) ? 0 : stringValue.length() - 1 - stringValue.indexOf('.');
		if (field.getDecimalPrecision() < decimalPlaces) {
			bean.setMeasureValue(MathUtils.round(bean.getMeasureValue(), field.getDecimalPrecision()));
		}

		return getMarketDataFieldService().saveMarketDataValueWithOptions(bean, true);
	}


	@Override
	public void uploadMarketDataValueUploadFile(MarketDataValueUploadCommand uploadCommand) {
		MarketDataSource defaultDataSource = uploadCommand.getSimpleMarketDataSource();
		MarketDataField defaultDataField = uploadCommand.getSimpleMarketDataField();

		List<IdentityObject> beanList = getSystemUploadHandler().convertSystemUploadFileToBeanList(uploadCommand, !uploadCommand.isSimple());

		int count = 0;
		List<MarketDataValue> dvList = new ArrayList<>();
		for (IdentityObject obj : CollectionUtils.getIterable(beanList)) {
			MarketDataValue dv = (MarketDataValue) obj;

			// Process Simple Look ups
			if (uploadCommand.isSimple()) {
				setInvestmentSecurityForMarketDataValue(dv);

				if (dv.getMeasureDate() == null && uploadCommand.getSimpleMeasureDate() != null) {
					dv.setMeasureDate(uploadCommand.getSimpleMeasureDate());
				}

				MarketDataSource mds = dv.getDataSource();
				if (mds == null || StringUtils.isEmpty(mds.getName())) {
					dv.setDataSource(defaultDataSource);
				}
				else {
					dv.setDataSource(getMarketDataSourceService().getMarketDataSourceByName(mds.getName()));
				}
				ValidationUtils.assertFalse(dv.getDataSource() == null || dv.getDataSource().getId() == null, "Data Source is required for all rows, or a default data source must be selected.");

				MarketDataField mdf = dv.getDataField();
				if (mdf == null || StringUtils.isEmpty(mdf.getName())) {
					dv.setDataField(defaultDataField);
				}
				else {
					dv.setDataField(getMarketDataFieldService().getMarketDataFieldByName(mdf.getName()));
				}
				ValidationUtils.assertFalse(dv.getDataField() == null || dv.getDataField().getId() == null, "Data Field is required for all rows, or a default data source must be selected.");
			}
			dvList.add(dv);
			count++;
		}
		saveMarketDataValueList(dvList);
		uploadCommand.getUploadResult().addUploadResults("MarketDataValue", count, true);
	}


	@Transactional
	protected void saveMarketDataValueList(List<MarketDataValue> list) {
		for (MarketDataValue dv : CollectionUtils.getIterable(list)) {
			saveMarketDataValueFromUpload(dv);
		}
	}


	private void setInvestmentSecurityForMarketDataValue(MarketDataValue mdv) {
		InvestmentSecurity security = mdv.getInvestmentSecurity();
		if (security == null || StringUtils.isEmpty(security.getSymbol())) {
			throw new ValidationException("Security Symbol is required for all Market Data Values.");
		}

		try {
			security = getInvestmentInstrumentService().getInvestmentSecurityBySymbol(security.getSymbol(), null);
		}
		catch (IllegalArgumentException e) {
			throw new ValidationException("Multiple securities with the symbol [" + mdv.getInvestmentSecurity().getSymbol()
					+ "] were found in the database. Please use the full Trade import functionality to properly define with security the market data is for.");
		}
		if (security == null) {
			throw new ValidationException("Cannot find Security in the system with symbol [" + mdv.getInvestmentSecurity().getSymbol() + "]");
		}
		mdv.setInvestmentSecurity(security);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public SystemUploadHandler getSystemUploadHandler() {
		return this.systemUploadHandler;
	}


	public void setSystemUploadHandler(SystemUploadHandler systemUploadHandler) {
		this.systemUploadHandler = systemUploadHandler;
	}
}
