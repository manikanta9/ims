package com.clifton.marketdata.field.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.util.math.BinaryOperators;
import com.clifton.system.hierarchy.assignment.search.BaseAuditableSystemHierarchyItemSearchForm;


public class MarketDataFieldSearchForm extends BaseAuditableSystemHierarchyItemSearchForm {

	@SearchField(searchField = "name,externalFieldName,description,valueExpression")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private Short fieldOrder;

	@SearchField
	private BinaryOperators adjustmentType;

	@SearchField(searchField = "name", searchFieldPath = "valueOverrideDataField")
	private String valueOverrideDataField;

	@SearchField
	private String externalFieldName;

	@SearchField
	private String valueExpression;


	@SearchField
	private Integer decimalPrecision;

	@SearchField
	private Boolean upToOnePerDay;

	@SearchField
	private Boolean latestValueOnly;

	@SearchField
	private Boolean timeSensitive;

	@SearchField
	private Boolean captureChangesOnly;

	@SearchField
	private Boolean realTimeReferenceData;

	@SearchField
	private Boolean systemDefined;

	@SearchField(searchField = "liveValueCalculator.id")
	private Integer liveValueCalculatorId;

	@SearchField(searchField = "liveValueCalculatorCondition.id")
	private Integer liveValueCalculatorConditionId;

	@SearchField(searchFieldPath = "ignoreValueCondition", searchField = "name")
	private String ignoreValueConditionName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDaoTableName() {
		return "MarketDataField";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Short getFieldOrder() {
		return this.fieldOrder;
	}


	public void setFieldOrder(Short fieldOrder) {
		this.fieldOrder = fieldOrder;
	}


	public BinaryOperators getAdjustmentType() {
		return this.adjustmentType;
	}


	public void setAdjustmentType(BinaryOperators adjustmentType) {
		this.adjustmentType = adjustmentType;
	}


	public String getValueOverrideDataField() {
		return this.valueOverrideDataField;
	}


	public void setValueOverrideDataField(String valueOverrideDataField) {
		this.valueOverrideDataField = valueOverrideDataField;
	}


	public String getExternalFieldName() {
		return this.externalFieldName;
	}


	public void setExternalFieldName(String externalFieldName) {
		this.externalFieldName = externalFieldName;
	}


	public String getValueExpression() {
		return this.valueExpression;
	}


	public void setValueExpression(String valueExpression) {
		this.valueExpression = valueExpression;
	}


	public Integer getDecimalPrecision() {
		return this.decimalPrecision;
	}


	public void setDecimalPrecision(Integer decimalPrecision) {
		this.decimalPrecision = decimalPrecision;
	}


	public Boolean getUpToOnePerDay() {
		return this.upToOnePerDay;
	}


	public void setUpToOnePerDay(Boolean upToOnePerDay) {
		this.upToOnePerDay = upToOnePerDay;
	}


	public Boolean getLatestValueOnly() {
		return this.latestValueOnly;
	}


	public void setLatestValueOnly(Boolean latestValueOnly) {
		this.latestValueOnly = latestValueOnly;
	}


	public Boolean getTimeSensitive() {
		return this.timeSensitive;
	}


	public void setTimeSensitive(Boolean timeSensitive) {
		this.timeSensitive = timeSensitive;
	}


	public Boolean getCaptureChangesOnly() {
		return this.captureChangesOnly;
	}


	public void setCaptureChangesOnly(Boolean captureChangesOnly) {
		this.captureChangesOnly = captureChangesOnly;
	}


	public Boolean getRealTimeReferenceData() {
		return this.realTimeReferenceData;
	}


	public void setRealTimeReferenceData(Boolean realTimeReferenceData) {
		this.realTimeReferenceData = realTimeReferenceData;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Integer getLiveValueCalculatorId() {
		return this.liveValueCalculatorId;
	}


	public void setLiveValueCalculatorId(Integer liveValueCalculatorId) {
		this.liveValueCalculatorId = liveValueCalculatorId;
	}


	public Integer getLiveValueCalculatorConditionId() {
		return this.liveValueCalculatorConditionId;
	}


	public void setLiveValueCalculatorConditionId(Integer liveValueCalculatorConditionId) {
		this.liveValueCalculatorConditionId = liveValueCalculatorConditionId;
	}


	public String getIgnoreValueConditionName() {
		return this.ignoreValueConditionName;
	}


	public void setIgnoreValueConditionName(String ignoreValueConditionName) {
		this.ignoreValueConditionName = ignoreValueConditionName;
	}
}
