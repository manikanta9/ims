package com.clifton.marketdata.field.mapping.validation;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>MarketDataPriceFieldMappingValidator</code> class validates on inserts and updates that there isn't another mapping
 * for the same type/subtype/subtype2/hierarchy/instrument for an overlapping date range
 *
 * @author manderson
 */
@Component
public class MarketDataPriceFieldMappingValidator extends SelfRegisteringDaoValidator<MarketDataPriceFieldMapping> {


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(MarketDataPriceFieldMapping bean, DaoEventTypes config) throws ValidationException {
		// NOT USED: USES DAO METHOD INSTEAD
	}


	@Override
	public void validate(MarketDataPriceFieldMapping bean, DaoEventTypes config, ReadOnlyDAO<MarketDataPriceFieldMapping> dao) throws ValidationException {
		validateSpecificitySelections(bean);

		// NOTE: DO WE WANT TO ENFORCE Latest Price Field is Always required and remove the "default" price field of "Latest Trade Price"
		ValidationUtils.assertTrue(bean.getLatestPriceMarketDataField() != null || bean.getClosingPriceMarketDataField() != null || bean.getOfficialPriceMarketDataField() != null, "At least one price field is required.");

		// Get the List of all Market Data Price Field Mappings based on the Specificity
		// NOTE: UX WOULD CATCH THIS, BUT THIS MESSAGE WILL GIVE THEM A FRIENDLIER MESSAGE THAT MAKES SENSE TO THE USER
		List<MarketDataPriceFieldMapping> existingList = getExistingList(bean, dao);
		// Iterate through existing and validate no overlap in date range
		for (MarketDataPriceFieldMapping existingMapping : CollectionUtils.getIterable(existingList)) {
			if (bean.isNewBean() || !bean.equals(existingMapping)) {
				throw new ValidationException("There already exists a mapping for the same specificity [ID: " + existingMapping.getId() + ", Label: " + existingMapping.getLabel() + "]");
			}
		}
	}


	/**
	 * Screen should prevent this, but logic is:
	 * If Instrument is Selected - Nothing else is selected
	 * If Hierarchy is Selected - Nothing else is selected
	 * SubType and Sub Type 2 can only be selected with Type selection
	 */
	private void validateSpecificitySelections(MarketDataPriceFieldMapping bean) {
		if (bean.getInvestmentType() == null) {
			ValidationUtils.assertNull(bean.getInvestmentTypeSubType(), "Sub Type can only be selected if Investment Type is selected");
			ValidationUtils.assertNull(bean.getInvestmentTypeSubType2(), "Sub Type 2 can only be selected if Investment Type is selected");
		}

		if (bean.getInstrument() != null) {
			ValidationUtils.assertNull(bean.getInstrumentHierarchy(), "Hierarchy can not be selected if a specific instrument is selected");
			ValidationUtils.assertNull(bean.getInvestmentType(), "Investment Type can not be selected if a specific instrument is selected");
			// Sub Types already required to be null because of Investment Type
		}
		else if (bean.getInstrumentHierarchy() != null) {
			// Instrument is already validated as NULL
			ValidationUtils.assertNull(bean.getInvestmentType(), "Investment Type can not be selected if a specific hierarchy is selected");
			// Sub Types already required to be null because of Investment Type
		}
		else if (bean.getInvestmentType() == null) {
			throw new ValidationException("At least Investment Type, Instrument Hierarchy, or Instrument is Required.");
		}
	}


	private List<MarketDataPriceFieldMapping> getExistingList(MarketDataPriceFieldMapping bean, ReadOnlyDAO<MarketDataPriceFieldMapping> dao) {
		if (bean.getInstrument() != null) {
			return dao.findByField("instrument.id", bean.getInstrument().getId());
		}
		if (bean.getInstrumentHierarchy() != null) {
			return dao.findByField("instrumentHierarchy.id", bean.getInstrumentHierarchy().getId());
		}
		return dao.findByFields(new String[]{"investmentType.id", "investmentTypeSubType.id", "investmentTypeSubType2.id"}, new Object[]{bean.getInvestmentType().getId(), (bean.getInvestmentTypeSubType() != null ? bean.getInvestmentTypeSubType().getId() : null), (bean.getInvestmentTypeSubType2() != null ? bean.getInvestmentTypeSubType2().getId() : null)});
	}
}
