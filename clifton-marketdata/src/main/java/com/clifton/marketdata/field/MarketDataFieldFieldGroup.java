package com.clifton.marketdata.field;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>MarketDataFieldFieldGroup</code> class specifies which fields belong to which group.
 * See MarketDataFieldGroup for details.
 *
 * @author vgomelsky
 */
public class MarketDataFieldFieldGroup extends ManyToManyEntity<MarketDataField, MarketDataFieldGroup, Integer> {

	// Nothing here
}
