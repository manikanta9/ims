package com.clifton.marketdata.field.column.value;

import com.clifton.core.beans.IdentityObject;


/**
 * Defines a method used to resolve the value or lookup the foreign key object for a column mapping.
 *
 * @author mwacker
 */
public interface MarketDataFieldColumnMappingValueResolver {

	public <T extends IdentityObject, R> R resolve(ColumnMappingValueResolverCommand<T> command);
}
