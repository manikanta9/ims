package com.clifton.marketdata.field;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.search.MarketDataFieldGroupSearchForm;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataFieldService</code> interface defines methods for working with market data fields
 * and related objects.
 *
 * @author vgomelsky
 */
public interface MarketDataFieldService {

	//////////////////////////////////////////////////////////////////////////// 
	//////             Market Data Field Business Methods             ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataField getMarketDataField(short id);


	public MarketDataField getMarketDataFieldByName(String name);


	/**
	 * Returns Last Trade Price which is the default to use for latest and closing price fields
	 */
	public MarketDataField getMarketDataPriceFieldDefault();


	/**
	 * Returns the MarketDataField that should be used to lookup live latest prices for the given security
	 * that stores LATEST price info.
	 * <p>
	 * Pricing fields are set via MarketDataFieldMapping section.  Default price field is "Last Trade Price" if
	 * there isn't one specifically mapped
	 */
	public MarketDataField getMarketDataLiveLatestPriceFieldForSecurity(InvestmentSecurity security);


	/**
	 * Returns the MarketDataField that should be used to lookup live closing prices for the given security
	 * that stores SETTLEMENT/CLOSING official price info. For example, futures or cleared swaps.
	 * <p>
	 * Pricing fields are set via MarketDataFieldMapping section.  Default price field is Last Trade Price if
	 * there isn't one specifically mapped
	 */
	public MarketDataField getMarketDataLiveClosingPriceFieldForSecurity(InvestmentSecurity security);


	public List<MarketDataField> getMarketDataFieldList(MarketDataFieldSearchForm searchForm);


	public List<MarketDataField> getMarketDataFieldListByGroup(final short dataFieldGroupId);


	public MarketDataField saveMarketDataField(MarketDataField bean);


	public void deleteMarketDataField(short id);


	//////////////////////////////////////////////////////////////////////////// 
	//////                 Market Data Value Business Methods          ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataValue getMarketDataValue(int id);


	/**
	 * Returns the last MarketDataValue stored for the given security, fieldName on the given date
	 */
	public MarketDataValueHolder getMarketDataValueForDate(int securityId, Date date, String fieldName);


	/**
	 * Returns the last MarketDataValue stored for the given security, fieldName on the given date
	 * <p>
	 * If the Security's Instrument has a Big Instrument, will expect the security to have BigSecurity set and
	 * will lookup data for the BigSecurity, not for the given security
	 */
	public MarketDataValueHolder getMarketDataValueForDateNormalized(int securityId, Date date, String fieldName);


	/**
	 * Returns the last MarketDataValue stored for the given security, fieldName with MeasureDate <= given date.
	 * <p>
	 * For example, Durations are updated monthly, so gets the last one entered before given date.
	 * <p>
	 * IF the Security's Instrument has a Big Instrument, will expect the security to have BigSecurity set and
	 * will lookup data for the BigSecurity, not for the given security
	 */
	public MarketDataValueHolder getMarketDataValueForDateFlexibleNormalized(int securityId, Date date, String fieldName);


	/**
	 * Returns the last MarketDataValue stored for the given security, fieldName with MeasureDate <= given date.
	 * <p>
	 * For example, Durations are updated monthly, so gets the last one entered before given date.
	 */
	public MarketDataValueHolder getMarketDataValueForDateFlexible(int securityId, Date date, String fieldName);


	/**
	 * Returns the last MarketDataValue stored for the given security, fieldName with MeasureDate <= given date.
	 * The fastest impl because it doesn't need to lookup security by id.
	 * DataSourceID is optional, but when specified then requires value to be from the specified datasource
	 */
	@DoNotAddRequestMapping
	public MarketDataValueHolder getMarketDataValueForDate(InvestmentSecurity security, Date date, String fieldName, Short dataSourceId, boolean flexible, boolean normalized);


	public List<MarketDataValue> getMarketDataValueList(MarketDataValueSearchForm searchForm);


	public MarketDataValue saveMarketDataValue(MarketDataValue bean);


	/**
	 * Saves the specified MarketDataValue in the database: inserts or updates based on the
	 * field type and specified parameters.
	 *
	 * @param bean
	 * @param updateIfExists : update existing bean if exists instead of throwing ValidationException
	 * @return the new or modified entity if any actions were performed, or <code>null</code> otherwise
	 */
	@DoNotAddRequestMapping
	public MarketDataValue saveMarketDataValueWithOptions(MarketDataValue bean, boolean updateIfExists);


	/**
	 * Saves the specified value (which must already exist in the database) bypassing any validation.
	 * Use this method very rarely and carefully because it's possible to violate data integrity.
	 * For example, use it to update historical adjustment factors, etc.
	 */
	@DoNotAddRequestMapping
	public void updateMarketDataValueWithoutValidation(MarketDataValue bean);


	public void deleteMarketDataValue(int id);


	public void deleteMarketDataValueList(List<MarketDataValue> valueList);


	//////////////////////////////////////////////////////////////////////////// 
	//////          Market Data Field Group Business Methods           ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldGroup getMarketDataFieldGroup(short id);


	public MarketDataFieldGroup getMarketDataFieldGroupByName(String name);


	public List<MarketDataFieldGroup> getMarketDataFieldGroupList(MarketDataFieldGroupSearchForm searchForm);


	public List<MarketDataFieldGroup> getMarketDataFieldGroupListByField(short dataFieldId);


	public MarketDataFieldGroup saveMarketDataFieldGroup(MarketDataFieldGroup bean);


	public void deleteMarketDataFieldGroup(short id);


	////////////////////////////////////////////////////////////////////////////
	////////      MarketDataFieldFieldGroup Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = MarketDataFieldGroup.class)
	public List<MarketDataFieldFieldGroup> getMarketDataFieldFieldGroupListByGroup(String groupName);


	@SecureMethod(dtoClass = MarketDataFieldGroup.class)
	public boolean isMarketDataFieldInGroup(String groupName, short dataFieldId);


	@SecureMethod(dtoClass = MarketDataFieldGroup.class)
	public MarketDataFieldFieldGroup linkMarketDataFieldToGroup(short dataFieldId, short dataFieldGroupId);


	@SecureMethod(dtoClass = MarketDataFieldGroup.class, permissions = SecurityPermission.PERMISSION_WRITE)
	public void deleteMarketDataFieldFieldGroup(short dataFieldId, short dataFieldGroupId);
}
