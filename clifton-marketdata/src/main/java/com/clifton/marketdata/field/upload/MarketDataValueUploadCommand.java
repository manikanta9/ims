package com.clifton.marketdata.field.upload;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.file.upload.FileUploadExistingBeanActions;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.system.upload.SystemUploadCommand;

import java.util.Date;


/**
 * The <code>MarketDataValueUploadCommand</code> extends the System Upload
 * however has some market data value specific fields that will be applied
 * to the values during the insert process.
 *
 * @author Mary Anderson
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class MarketDataValueUploadCommand extends SystemUploadCommand {

	/**
	 * If simple is true, this can optionally be set to set the data source for all
	 * values in the file where not set explicitly
	 */
	private MarketDataSource simpleMarketDataSource;

	/**
	 * If simple is true, this can optionally be set to set the data field for all
	 * values in the file where not set explicitly
	 */
	private MarketDataField simpleMarketDataField;

	private Date simpleMeasureDate;

	private boolean applyUnmappedColumnNamesAsSeparateDataFields;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	@ValueIgnoringGetter
	public String getTableName() {
		return "MarketDataValue";
	}


	@Override
	@ValueIgnoringGetter
	public String getSystemUploadBeanListConverterBeanName() {
		if (isApplyUnmappedColumnNamesAsSeparateDataFields()) {
			return "marketDataValueMultipleConverter";
		}
		// Uses the default
		return null;
	}


	// Overrides - Market Data Value Import is partial uploads allowed, do not insert fk beans, and always inserts, never updates (service bean handles updateIfExists properly)


	@Override
	@ValueIgnoringGetter
	public boolean isPartialUploadAllowed() {
		return true;
	}


	@Override
	@ValueIgnoringGetter
	public boolean isInsertMissingFKBeans() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public FileUploadExistingBeanActions getExistingBeans() {
		return FileUploadExistingBeanActions.INSERT;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public MarketDataSource getSimpleMarketDataSource() {
		return this.simpleMarketDataSource;
	}


	public void setSimpleMarketDataSource(MarketDataSource simpleMarketDataSource) {
		this.simpleMarketDataSource = simpleMarketDataSource;
	}


	public MarketDataField getSimpleMarketDataField() {
		return this.simpleMarketDataField;
	}


	public void setSimpleMarketDataField(MarketDataField simpleMarketDataField) {
		this.simpleMarketDataField = simpleMarketDataField;
	}


	public boolean isApplyUnmappedColumnNamesAsSeparateDataFields() {
		return this.applyUnmappedColumnNamesAsSeparateDataFields;
	}


	public void setApplyUnmappedColumnNamesAsSeparateDataFields(boolean applyUnmappedColumnNamesAsSeparateDataFields) {
		this.applyUnmappedColumnNamesAsSeparateDataFields = applyUnmappedColumnNamesAsSeparateDataFields;
	}


	public Date getSimpleMeasureDate() {
		return this.simpleMeasureDate;
	}


	public void setSimpleMeasureDate(Date simpleMeasureDate) {
		this.simpleMeasureDate = simpleMeasureDate;
	}
}
