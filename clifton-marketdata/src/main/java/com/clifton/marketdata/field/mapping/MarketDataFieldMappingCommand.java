package com.clifton.marketdata.field.mapping;

import com.clifton.core.dataaccess.dao.NonPersistentObject;

import java.util.List;


/**
 * @author theodorez
 */
@NonPersistentObject
public class MarketDataFieldMappingCommand {

	private Short marketDataSourceId;
	private Integer instrumentId;
	private Short instrumentHierarchyId;
	private Short investmentTypeId;
	private Short investmentTypeSubTypeId;
	private Short investmentTypeSubType2Id;

	private List<String> errors;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public List<String> getErrors() {
		return this.errors;
	}


	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
}
