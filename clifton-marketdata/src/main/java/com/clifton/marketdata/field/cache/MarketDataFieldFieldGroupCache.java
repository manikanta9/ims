package com.clifton.marketdata.field.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.marketdata.field.MarketDataFieldFieldGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataFieldFieldGroupCache</code> caches list of {@link MarketDataFieldFieldGroup} beans
 * by its group "referenceTwo.id"
 * <p/>
 * Example: For every security used by an LDI account for KRD points, we need to look up this list
 *
 * @author manderson
 */
@Component
public class MarketDataFieldFieldGroupCache extends SelfRegisteringSingleKeyDaoListCache<MarketDataFieldFieldGroup, Short> {


	@Override
	protected String getBeanKeyProperty() {
		return "referenceTwo.id";
	}


	@Override
	protected Short getBeanKeyValue(MarketDataFieldFieldGroup bean) {
		if (bean.getReferenceTwo() != null) {
			return bean.getReferenceTwo().getId();
		}
		return null;
	}
}
