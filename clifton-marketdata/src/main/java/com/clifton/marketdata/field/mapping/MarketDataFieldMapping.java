package com.clifton.marketdata.field.mapping;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourcePricingSource;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldGroup;


/**
 * The <code>MarketDataFieldMapping</code> class maps market data source and fields to corresponding investment instruments.
 * <p>
 * Mappings can apply in the following specificity order - and there would only be one result per datasource/instrument:
 * 1. Instrument
 * 2. Hierarchy
 * 3. Type/SubType/SubType2
 * 4. Type/SubType2
 * 5. Type/SubType
 * 6. Type
 * <p>
 * Note that this class serves 3 distinct purposes (lookup of live price fields, data source specific market sector and data field group to price).
 * For Price Field Mappings, see {@link MarketDataPriceFieldMapping}
 *
 * @author vgomelsky
 */
public class MarketDataFieldMapping extends BaseEntity<Integer> implements LabeledObject {

	/**
	 * Used to map field to specific investment types.
	 * <p>
	 * NOTE: Takes precedence over a mapping without hierarchy and/or investment type. Both can be defined for the same field.
	 */
	private InvestmentType investmentType;
	/**
	 * Used to map field to specific investment sub types.
	 */
	private InvestmentTypeSubType investmentTypeSubType;
	/**
	 * Used to map field to specific investment sub type 2's
	 */
	private InvestmentTypeSubType2 investmentTypeSubType2;

	private InvestmentInstrumentHierarchy instrumentHierarchy;

	private InvestmentInstrument instrument;


	////////////////////////////////////////////////////////////////////////////////

	private MarketDataFieldGroup fieldGroup;

	private MarketDataSource marketDataSource;
	private MarketDataSourceSector marketSector;

	private MarketDataSourcePricingSource pricingSource;
	/**
	 * If specified, will use corresponding (primary or composite) exchange code in addition to security symbol when
	 * making a request to the market data provider.
	 * For example, "IBM" vs "IBM UN" for New York Stock Exchange.
	 */
	private InvestmentExchangeTypes exchangeCodeType;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Used for Live Price Look ups when Latest Price is Not Available (Market is Closed)
	 */
	private MarketDataField liveClosingPriceMarketDataField;

	/**
	 * Used for Live Price Look ups to get latest price
	 */
	private MarketDataField liveLatestPriceMarketDataField;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(20);
		result.append(getSpecificityLabel());
		result.append(": ");
		if (getFieldGroup() != null) {
			result.append(getFieldGroup().getName());
			result.append(", ");
		}

		if (getMarketDataSource() != null) {
			result.append(getMarketDataSource().getName());
			if (getMarketSector() != null) {
				result.append("-");
				result.append(getMarketSector().getLabel());
			}
		}
		return result.toString();
	}


	public String getSpecificityLabel() {
		StringBuilder result = new StringBuilder(20);
		if (getInstrument() != null) {
			result.append(getInstrument().getLabel());
		}
		else if (getInstrumentHierarchy() != null) {
			result.append(getInstrumentHierarchy().getLabelExpanded());
		}
		else if (getInvestmentType() != null) {
			result.append(getInvestmentType().getName());
			if (getInvestmentTypeSubType() != null) {
				result.append("-").append(getInvestmentTypeSubType().getName());
			}
			if (getInvestmentTypeSubType2() != null) {
				result.append("-").append(getInvestmentTypeSubType2().getName());
			}
		}
		return result.toString();
	}


	////////////////////////////////////////////////////////////////////////////////


	public InvestmentInstrument getInstrument() {
		return this.instrument;
	}


	public void setInstrument(InvestmentInstrument instrument) {
		this.instrument = instrument;
	}


	public InvestmentInstrumentHierarchy getInstrumentHierarchy() {
		return this.instrumentHierarchy;
	}


	public void setInstrumentHierarchy(InvestmentInstrumentHierarchy instrumentHierarchy) {
		this.instrumentHierarchy = instrumentHierarchy;
	}


	public MarketDataFieldGroup getFieldGroup() {
		return this.fieldGroup;
	}


	public void setFieldGroup(MarketDataFieldGroup fieldGroup) {
		this.fieldGroup = fieldGroup;
	}


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public MarketDataSourceSector getMarketSector() {
		return this.marketSector;
	}


	public void setMarketSector(MarketDataSourceSector marketSector) {
		this.marketSector = marketSector;
	}


	public MarketDataField getLiveClosingPriceMarketDataField() {
		return this.liveClosingPriceMarketDataField;
	}


	public void setLiveClosingPriceMarketDataField(MarketDataField liveClosingPriceMarketDataField) {
		this.liveClosingPriceMarketDataField = liveClosingPriceMarketDataField;
	}


	public MarketDataField getLiveLatestPriceMarketDataField() {
		return this.liveLatestPriceMarketDataField;
	}


	public void setLiveLatestPriceMarketDataField(MarketDataField liveLatestPriceMarketDataField) {
		this.liveLatestPriceMarketDataField = liveLatestPriceMarketDataField;
	}


	public MarketDataSourcePricingSource getPricingSource() {
		return this.pricingSource;
	}


	public void setPricingSource(MarketDataSourcePricingSource pricingSource) {
		this.pricingSource = pricingSource;
	}


	public InvestmentExchangeTypes getExchangeCodeType() {
		return this.exchangeCodeType;
	}


	public void setExchangeCodeType(InvestmentExchangeTypes exchangeCodeType) {
		this.exchangeCodeType = exchangeCodeType;
	}


	public InvestmentTypeSubType2 getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(InvestmentTypeSubType2 investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public InvestmentTypeSubType getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(InvestmentTypeSubType investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public InvestmentType getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(InvestmentType investmentType) {
		this.investmentType = investmentType;
	}
}
