package com.clifton.marketdata.field.mapping.cache;

import java.util.Map;


/**
 * @author manderson
 */
public interface MarketDataFieldValueMappingCache {


	public Map<String, String> getMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId);


	public void setMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId, Map<String, String> value);
}
