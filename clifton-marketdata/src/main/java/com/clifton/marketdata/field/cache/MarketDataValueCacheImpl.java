package com.clifton.marketdata.field.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * The <code>MarketDataValueCacheImpl</code> class caches MarketDataValueHolder objects by date/field/security
 *
 * @author vgomelsky
 */
@Component
public class MarketDataValueCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataValue, String, MarketDataValueHolder> implements MarketDataValueCache {


	@Override
	public MarketDataValueHolder getMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate) {
		return getCacheHandler().get(getCacheName(), getBeanKey(securityId, dataFieldId, dataSourceId, measureDate));
	}


	@Override
	public void setMarketDataValueHolder(int securityId, int dataFieldId, Short dataSourceId, Date measureDate, MarketDataValue bean) {
		getCacheHandler().put(getCacheName(), getBeanKey(securityId, dataFieldId, dataSourceId, measureDate), new MarketDataValueHolder(bean));
	}


	private String getBeanKey(MarketDataValue bean, boolean includeDataSource) {
		return getBeanKey(bean.getInvestmentSecurity().getId(), bean.getDataField().getId(), (includeDataSource ? bean.getDataSource().getId() : null), bean.getMeasureDate());
	}


	private String getBeanKey(int securityId, int dataFieldId, Short dataSourceId, Date measureDate) {
		StringBuilder key = new StringBuilder(24);
		key.append(securityId);
		key.append('-');
		key.append(dataFieldId);
		key.append('-');
		if (dataSourceId != null) {
			key.append(dataSourceId);
			key.append('-');
		}
		key.append(measureDate.getTime());
		return key.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void beforeMethodCallImpl(ReadOnlyDAO<MarketDataValue> dao, DaoEventTypes event, MarketDataValue bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<MarketDataValue> dao, DaoEventTypes event, MarketDataValue bean, Throwable e) {
		if (e == null) {
			String keyWithoutDataSource = getBeanKey(bean, false);
			String keyWithDataSource = getBeanKey(bean, true);
			if (event.isUpdate()) {
				MarketDataValue originalBean = getOriginalBean(dao, bean);
				String originalKeyWithoutDataSource = getBeanKey(originalBean, false);
				String originalKeyWithDataSource = getBeanKey(originalBean, true);
				if (!keyWithoutDataSource.equals(originalKeyWithoutDataSource)) {
					getCacheHandler().remove(getCacheName(), originalKeyWithoutDataSource);
				}
				if (!keyWithDataSource.equals(originalKeyWithDataSource)) {
					getCacheHandler().remove(getCacheName(), originalKeyWithDataSource);
				}
			}
			getCacheHandler().remove(getCacheName(), keyWithoutDataSource);
			getCacheHandler().remove(getCacheName(), keyWithDataSource);
		}
	}
}
