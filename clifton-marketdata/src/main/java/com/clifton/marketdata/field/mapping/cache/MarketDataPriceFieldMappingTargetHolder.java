package com.clifton.marketdata.field.mapping.cache;

import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.marketdata.field.mapping.MarketDataPriceFieldMapping;


/**
 * The <code>MarketDataPriceFieldMappingTargetHolder</code> contains the meta data for a MarketDataPriceFieldMapping
 * and is used in the MarketDataPriceFieldMappingSpecificityCache.
 *
 * @author manderson
 */
public class MarketDataPriceFieldMappingTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final int id;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataPriceFieldMappingTargetHolder(String key, MarketDataPriceFieldMapping mapping) {
		this.key = key;
		this.id = mapping.getId();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.id;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MarketDataPriceFieldMappingTargetHolder other = (MarketDataPriceFieldMappingTargetHolder) obj;
		if (this.id != other.id) {
			return false;
		}
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		return true;
	}


	@Override
	public SpecificityScope getScope() {
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getId() {
		return this.id;
	}


	@Override
	public String getKey() {
		return this.key;
	}
}
