package com.clifton.marketdata.field;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentUtils;
import com.clifton.marketdata.field.cache.MarketDataValueCache;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.search.MarketDataFieldGroupSearchForm;
import com.clifton.marketdata.field.search.MarketDataFieldSearchForm;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.hierarchy.assignment.SystemHierarchyAssignmentService;
import com.clifton.system.hierarchy.assignment.SystemHierarchyLink;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.IntegerType;
import org.hibernate.type.Type;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>MarketDataFieldServiceImpl</code> class provides basic implementation of the MarketDataFieldService interface.
 *
 * @author vgomelsky
 */
@Service
public class MarketDataFieldServiceImpl implements MarketDataFieldService {

	private static final String DEFAULT_PRICE_MARKET_DATA_FIELD_NAME = MarketDataField.FIELD_LAST_TRADE_PRICE;

	private AdvancedUpdatableDAO<MarketDataField, Criteria> marketDataFieldDAO;
	private AdvancedUpdatableDAO<MarketDataFieldGroup, Criteria> marketDataFieldGroupDAO;
	private AdvancedUpdatableDAO<MarketDataFieldFieldGroup, Criteria> marketDataFieldFieldGroupDAO;

	private AdvancedUpdatableDAO<MarketDataValue, Criteria> marketDataValueDAO;

	private DaoNamedEntityCache<MarketDataField> marketDataFieldCache;
	private DaoNamedEntityCache<MarketDataFieldGroup> marketDataFieldGroupCache;
	private DaoSingleKeyListCache<MarketDataFieldFieldGroup, Short> marketDataFieldFieldGroupCache;

	private MarketDataValueCache marketDataValueCache;
	private MarketDataValueCache marketDataValueFlexibleCache;

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SystemHierarchyAssignmentService systemHierarchyAssignmentService;

	////////////////////////////////////////////////////////////////////////////
	//////             Market Data Field Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataField getMarketDataField(short id) {
		return getMarketDataFieldDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataField getMarketDataFieldByName(String name) {
		return getMarketDataFieldCache().getBeanForKeyValueStrict(getMarketDataFieldDAO(), name);
	}


	@Override
	public MarketDataField getMarketDataPriceFieldDefault() {
		return getMarketDataFieldByName(DEFAULT_PRICE_MARKET_DATA_FIELD_NAME);
	}


	@Override
	public MarketDataField getMarketDataLiveLatestPriceFieldForSecurity(InvestmentSecurity security) {
		MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), null);
		MarketDataField priceField = mapping != null ? mapping.getLiveLatestPriceMarketDataField() : null;
		if (priceField == null) {
			return getMarketDataPriceFieldDefault();
		}
		return priceField;
	}


	@Override
	public MarketDataField getMarketDataLiveClosingPriceFieldForSecurity(InvestmentSecurity security) {
		MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), null);
		MarketDataField priceField = mapping != null ? mapping.getLiveClosingPriceMarketDataField() : null;
		if (priceField == null) {
			return getMarketDataPriceFieldDefault();
		}
		return priceField;
	}


	@Override
	public List<MarketDataField> getMarketDataFieldList(MarketDataFieldSearchForm searchForm) {
		return getMarketDataFieldDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<MarketDataField> getMarketDataFieldListByGroup(final short dataFieldGroupId) {
		HibernateSearchConfigurer searchConfigurer = new HibernateSearchConfigurer() {

			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.createAlias("groupList", "g").add(Restrictions.eq("g.id", dataFieldGroupId));
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.addOrder(Order.asc("fieldOrder"));
				criteria.addOrder(Order.asc("name"));
				return true;
			}
		};
		return getMarketDataFieldDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public MarketDataField saveMarketDataField(MarketDataField bean) {
		if (StringUtils.isEmpty(bean.getValueExpression()) && !StringUtils.isEmpty(bean.getExternalFieldName())) {
			ValidationUtils.assertFalse(bean.getExternalFieldName().contains(","), "A comma delimited list of external fields is only allowed when value expression is used.");
		}
		ValidationUtils.assertTrue(StringUtils.isEmpty(bean.getSymbolOverrideLookupField()) == (bean.getSymbolOverrideMarketSector() == null), "Must specify both Symbol External Name Override and Symbol Market Sector Override");
		return getMarketDataFieldDAO().save(bean);
	}


	@Override
	public void deleteMarketDataField(short id) {
		getMarketDataFieldDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Market Data Value Business Methods            /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataValue getMarketDataValue(int id) {
		return getMarketDataValueDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataValueHolder getMarketDataValueForDate(int securityId, Date date, String fieldName) {
		return getMarketDataValueForDateImpl(securityId, null, date, fieldName, null, false, false);
	}


	@Override
	public MarketDataValueHolder getMarketDataValueForDateNormalized(int securityId, Date date, String fieldName) {
		return getMarketDataValueForDateImpl(securityId, null, date, fieldName, null, false, true);
	}


	@Override
	public MarketDataValueHolder getMarketDataValueForDateFlexibleNormalized(int securityId, Date date, String fieldName) {
		return getMarketDataValueForDateImpl(securityId, null, date, fieldName, null, true, true);
	}


	@Override
	public MarketDataValueHolder getMarketDataValueForDateFlexible(int securityId, Date date, String fieldName) {
		return getMarketDataValueForDateImpl(securityId, null, date, fieldName, null, true, false);
	}


	@Override
	public MarketDataValueHolder getMarketDataValueForDate(InvestmentSecurity security, Date date, String fieldName, Short dataSourceId, boolean flexible, boolean normalized) {
		return getMarketDataValueForDateImpl(security.getId(), security, date, fieldName, dataSourceId, flexible, normalized);
	}


	protected MarketDataValueHolder getMarketDataValueForDateImpl(int securityId, InvestmentSecurity security, Date date, String fieldName, Short dataSourceId, boolean flexible, boolean normalized) {
		if (normalized) {
			if (security == null) {
				security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
			}
			if (security.getInstrument().getBigInstrument() != null) {
				// Big security is only required if the Big Instrument is active
				if (!security.getInstrument().getBigInstrument().isInactive()) {
					ValidationUtils.assertNotNull(security.getBigSecurity(), "Unable to get normalized value for security [" + security.getLabel() + "] and field [" + fieldName + "] because the big security is not set.");
				}
				if (security.getBigSecurity() != null && security.getBigSecurity().isActiveOn(date)) {
					securityId = security.getBigSecurity().getId();
				}
			}
		}

		if (date == null) {
			throw new ValidationException("Date parameter is required for security: " + security + " and field " + fieldName);
		}

		// try cache lookup first
		short dataFieldId = getMarketDataFieldByName(fieldName).getId();
		MarketDataValueHolder result = flexible ? getMarketDataValueFlexibleCache().getMarketDataValueHolder(securityId, dataFieldId, dataSourceId, date) : getMarketDataValueCache().getMarketDataValueHolder(securityId, dataFieldId, dataSourceId, date);
		if (result != null) {
			if (result.getMeasureValue() == null) {
				// if the value is null this means there is no value defined: null cache value for no result lookup
				return null;
			}
			return result;
		}

		final int finalSecurityId = securityId;
		List<MarketDataValue> valueList = getMarketDataValueDAO().findBySearchCriteria(new HibernateSearchConfigurer() {
			@Override
			public void configureCriteria(Criteria criteria) {
				// Optimization that avoids unnecessary SQL key look-ups by using only fully covered index to get single row first.
				// Without this change, it will get all results first, then do a key lookup for each row, then sort and finally will do top 1.
				DetachedCriteria subQuery = DetachedCriteria.forClass(MarketDataValue.class);
				subQuery.setProjection(Projections.sqlProjection("TOP 1 MarketDataValueID", new String[]{"MarketDataValueID"}, new Type[]{new IntegerType()}));
				subQuery.add(Restrictions.eq("investmentSecurity.id", finalSecurityId));
				subQuery.add(Restrictions.eq("dataField.id", dataFieldId));
				if (dataSourceId != null) {
					subQuery.add(Restrictions.eq("dataSource.id", dataSourceId));
				}
				subQuery.add(flexible ? Restrictions.le("measureDate", date) : Restrictions.eq("measureDate", date));
				subQuery.addOrder(Order.desc("measureDate"));
				subQuery.addOrder(Order.desc("measureTime"));
				criteria.add(Subqueries.propertyEq("id", subQuery));
			}


			@Override
			public boolean configureOrderBy(Criteria criteria) {
				return true;
			}
		});

		MarketDataValue value = CollectionUtils.getFirstElement(valueList);

		// update cache: store null values in cache too
		if (flexible) {
			getMarketDataValueFlexibleCache().setMarketDataValueHolder(securityId, dataFieldId, dataSourceId, date, value);
		}
		else {
			getMarketDataValueCache().setMarketDataValueHolder(securityId, dataFieldId, dataSourceId, date, value);
		}

		if (value != null) {
			result = new MarketDataValueHolder(value);
		}
		return result;
	}


	@Override
	public List<MarketDataValue> getMarketDataValueList(MarketDataValueSearchForm searchForm) {
		if (searchForm.getDataFieldCategoryHierarchyId() != null || searchForm.getDataFieldGroupName() != null) {
			Short[] fieldIds = null;

			if (searchForm.getDataFieldCategoryHierarchyId() != null) {
				List<SystemHierarchyLink> fieldLinkList = getSystemHierarchyAssignmentService().getSystemHierarchyLinkListByTableAndHierarchy("MarketDataField", searchForm.getDataFieldCategoryHierarchyId());
				if (CollectionUtils.isEmpty(fieldLinkList)) {
					return null;
				}
				fieldIds = BeanUtils.getPropertyValues(fieldLinkList, systemHierarchyLink -> MathUtils.getNumberAsShort(systemHierarchyLink.getFkFieldId()), Short.class);
			}
			// Filter by MarketDataFieldGroup name
			if (searchForm.getDataFieldGroupName() != null) {
				List<MarketDataField> dataFieldList = CollectionUtils.getConverted(getMarketDataFieldFieldGroupListByGroup(searchForm.getDataFieldGroupName()), MarketDataFieldFieldGroup::getReferenceOne);
				if (CollectionUtils.isEmpty(dataFieldList)) {
					return null;
				}
				fieldIds = (fieldIds == null ? BeanUtils.getBeanIdentityArray(dataFieldList, Short.class) : ArrayUtils.intersect(fieldIds, BeanUtils.getBeanIdentityArray(dataFieldList, Short.class)));
			}
			searchForm.setDataFieldIds(fieldIds);
		}

		if (searchForm.getDataFieldName() != null && searchForm.getDataFieldId() == null) {
			// use id to avoid extra join and improve performance
			searchForm.setDataFieldId(getMarketDataFieldByName(searchForm.getDataFieldName()).getId());
			searchForm.setDataFieldName(null);
		}

		// Default sorting to Measure Date/Time desc, then Field Order, Name ASC
		if (!searchForm.isSkipDefaultSorting() && StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("measureDate:desc#dataFieldOrder:asc#dataFieldName:asc");
		}

		return getMarketDataValueDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm, false, searchForm.isSkipDefaultSorting()) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				// Apply max date/time criteria
				if (searchForm.getMaxMeasureDateAndTime() != null) {
					LogicalExpression currentDateCondition = Restrictions.and(
							Restrictions.eq("measureDate", DateUtils.clearTime(searchForm.getMaxMeasureDateAndTime())),
							Restrictions.le("measureTime", Time.valueOf(searchForm.getMaxMeasureDateAndTime()))
					);
					SimpleExpression priorDateCondition = Restrictions.lt("measureDate", searchForm.getMaxMeasureDateAndTime());
					criteria.add(Restrictions.or(currentDateCondition, priorDateCondition));
				}

				if (searchForm.getPriceFieldMappingId() != null) {
					String sql = "EXISTS (SELECT s.InvestmentInstrumentID FROM InvestmentSecurity s " + //
							"INNER JOIN InvestmentInstrument i ON s.InvestmentInstrumentID = i.InvestmentInstrumentID " + //
							"INNER JOIN InvestmentInstrumentHierarchy h ON i.InvestmentInstrumentHierarchyID = h.InvestmentInstrumentHierarchyID " + //
							"LEFT JOIN MarketDataPriceFieldMapping im ON i.InvestmentInstrumentID = im.InvestmentInstrumentID " + //
							"LEFT JOIN MarketDataPriceFieldMapping hm ON i.InvestmentInstrumentHierarchyID = hm.InvestmentInstrumentHierarchyID " + //
							"LEFT JOIN MarketDataPriceFieldMapping st12m ON h.InvestmentTypeSubTypeID = st12m.InvestmentTypeSubTypeID AND h.InvestmentTypeSubType2ID = st12m.InvestmentTypeSubType2ID " + //
							"LEFT JOIN MarketDataPriceFieldMapping st2m ON h.InvestmentTypeSubType2ID = st2m.InvestmentTypeSubType2ID AND st2m.InvestmentTypeSubTypeID IS NULL " + //
							"LEFT JOIN MarketDataPriceFieldMapping st1m ON h.InvestmentTypeSubTypeID = st1m.InvestmentTypeSubTypeID AND st1m.InvestmentTypeSubType2ID IS NULL " + //
							"LEFT JOIN MarketDataPriceFieldMapping tm ON h.InvestmentTypeID = tm.InvestmentTypeID AND tm.InvestmentTypeSubTypeID IS NULL AND tm.InvestmentTypeSubType2ID IS NULL " + //
							"INNER JOIN MarketDataPriceFieldMapping m ON m.MarketDataPriceFieldMappingID = COALESCE(im.MarketDataPriceFieldMappingID, hm.MarketDataPriceFieldMappingID, st12m.MarketDataPriceFieldMappingID, st2m.MarketDataPriceFieldMappingID, st1m.MarketDataPriceFieldMappingID, tm.MarketDataPriceFieldMappingID) " + //
							"WHERE {alias}.InvestmentSecurityID = s.InvestmentSecurityID AND " + //
							"m.MarketDataPriceFieldMappingID = " + searchForm.getPriceFieldMappingId() + ")";

					criteria.add(Restrictions.sqlRestriction(sql));
				}
			}
		});
	}


	@Override
	public MarketDataValue saveMarketDataValue(MarketDataValue bean) {
		return doSaveMarketDataValue(bean, false);
	}


	@Override
	public MarketDataValue saveMarketDataValueWithOptions(MarketDataValue bean, boolean updateIfExists) {
		return doSaveMarketDataValue(bean, updateIfExists);
	}


	@Override
	public void updateMarketDataValueWithoutValidation(MarketDataValue bean) {
		if (bean == null || bean.isNewBean()) {
			throw new IllegalArgumentException("MarketDataValue updates without validation are allowed only for existing values: " + bean);
		}
		getMarketDataValueDAO().save(bean);
	}


	@Override
	public void deleteMarketDataValue(int id) {
		getMarketDataValueDAO().delete(id);
	}


	@Override
	public void deleteMarketDataValueList(List<MarketDataValue> valueList) {
		getMarketDataValueDAO().deleteList(valueList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional
	protected MarketDataValue doSaveMarketDataValue(MarketDataValue bean, boolean updateIfExists) {
		ValidationUtils.assertNotNull(bean.getDataSource(), "Market Data Source is required to save Market Data Value");
		ValidationUtils.assertTrue(bean.getDataSource().isMarketDataValueSupported(), () -> "Market Data Source selected does not support Market Data Values: " + bean.getDataSource().getName());

		// Normalize properties
		MarketDataValue mdv = bean;
		// Re-retrieve entities to guarantee accuracy of configuration
		mdv.setDataField(getMarketDataField(ObjectUtils.coalesce(mdv.getDataField().getValueOverrideDataField(), mdv.getDataField()).getId()));
		mdv.setInvestmentSecurity(getInvestmentInstrumentService().getInvestmentSecurity(mdv.getInvestmentSecurity().getId()));
		mdv.setMeasureDate(DateUtils.clearTime(mdv.getMeasureDate())); // clear time because it's saved in its own field

		// always save market data initially for newly created securities (which may not be active yet)
		if (!InvestmentUtils.isSecurityCreatedToday(mdv.getInvestmentSecurity())) {
			ValidationUtils.assertTrue(InvestmentUtils.isSecurityActiveOn(mdv.getInvestmentSecurity(), mdv.getMeasureDate()), "Security must be active on measure date: " + mdv);
		}
		ValidationUtils.assertNotNull(mdv.getMeasureValue(), "Measure Value field is required", "measureValue");
		ValidationUtils.assertMaxDecimalPrecision(mdv.getMeasureValue(), mdv.getDataField().getDecimalPrecision(), "measureValue");
		if (mdv.getDataField().isTimeSensitive()) {
			ValidationUtils.assertNotNull(mdv.getMeasureTime(), "The field '" + mdv.getDataField().getLabel() + "' requires time to be for " + mdv.getInvestmentSecurity(), "measureTime");
		}
		else {
			ValidationUtils.assertNull(mdv.getMeasureTime(), "The field '" + mdv.getDataField().getLabel() + "' does not allow time to be for " + mdv.getInvestmentSecurity(), "measureTime");
		}

		// Validate Ignore Value
		if (mdv.getDataField().getIgnoreValueCondition() != null) {
			EvaluationResult ignoreResult = getSystemConditionEvaluationHandler().evaluateCondition(mdv.getDataField().getIgnoreValueCondition(), mdv);
			if (ignoreResult.isResult()) {
				// Ignore this Value - Do Nothing
				LogUtils.info(getClass(), "No value saved because matches ignore condition (" + ignoreResult.getMessage() + "): " + mdv);
				return null;
			}
		}

		// validate upToOnePerDay
		if (mdv.getDataField().isUpToOnePerDay()) {
			// make sure no records exist from the same data source for this day
			MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
			searchForm.setDataFieldId(mdv.getDataField().getId());
			searchForm.setDataSourceId(mdv.getDataSource().getId());
			searchForm.setMeasureDate(mdv.getMeasureDate());
			searchForm.setInvestmentSecurityId(mdv.getInvestmentSecurity().getId());
			List<MarketDataValue> existingList = getMarketDataValueList(searchForm);
			int count = CollectionUtils.getSize(existingList);
			ValidationUtils.assertFalse(count > 1, "Cannot have more than one measure for field '" + mdv.getDataField().getLabel() + "' from the same data source on the same date. Security: " + mdv.getInvestmentSecurity());
			MarketDataValue existingBean = CollectionUtils.getFirstElement(existingList);
			if (existingBean != null) {
				ValidationUtils.assertTrue(updateIfExists || existingBean.getId().equals(mdv.getId()), "Cannot add another measure for field '" + mdv.getDataField().getLabel()
						+ "' on the same date. Update existing measure instead.");
				if (MathUtils.isEqual(mdv.getMeasureValue(), existingBean.getMeasureValue()) && CompareUtils.isEqual(mdv.getMeasureTime(), existingBean.getMeasureTime())
						&& CompareUtils.isEqual(mdv.getMeasureValueAdjustmentFactor(), existingBean.getMeasureValueAdjustmentFactor())) {
					// no reason to update if all properties are the same
					LogUtils.info(getClass(), "No update. Identical date already exists: " + existingBean);
					return null;
				}
				existingBean.setMeasureTime(mdv.getMeasureTime());
				existingBean.setMeasureValue(mdv.getMeasureValue());
				existingBean.setMeasureValueAdjustmentFactor(mdv.getMeasureValueAdjustmentFactor());
				mdv = existingBean; //after setting the values, point to the existing for saving
			}
		}

		// validate latestValueOnly
		if (mdv.getDataField().isLatestValueOnly()) {
			MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
			searchForm.setDataFieldId(mdv.getDataField().getId());
			searchForm.setDataSourceId(mdv.getDataSource().getId());
			searchForm.setInvestmentSecurityId(mdv.getInvestmentSecurity().getId());
			List<MarketDataValue> existingList = getMarketDataValueList(searchForm);
			int count = CollectionUtils.getSize(existingList);
			ValidationUtils.assertFalse(count > 1, "Cannot have more than one measure for field '" + mdv.getDataField().getLabel() + "' from the same data source. Security: " + mdv.getInvestmentSecurity());
			if (count == 1) {
				MarketDataValue existingBean = existingList.get(0);
				if (mdv.equals(existingBean)) {
					LogUtils.info(getClass(), "Cannot add new measure for field '" + mdv.getDataField().getLabel() + "'. Updating existing measure instead.");
				}
				if (existingBean != null) { //note, need to do this here, because if you do it anywhere else you need a detached DTO, since this save does a select, Hibernate will issue an update.
					ValidationUtils.assertFalse(mdv.getMeasureDateWithTime().before(existingBean.getMeasureDateWithTime()),
							"Cannot add new measure for field '" + mdv.getDataField().getLabel() + ".  A measure with date " + DateUtils.fromDate(existingBean.getMeasureDateWithTime())
									+ " exists which is later than " + DateUtils.fromDate(mdv.getMeasureDateWithTime()));
					existingBean.setMeasureDate(mdv.getMeasureDate());
					existingBean.setMeasureTime(mdv.getMeasureTime());
					existingBean.setMeasureValue(mdv.getMeasureValue());
					mdv = existingBean; //after setting the values, point to the existing for saving
				}
			}
		}

		// Only do this check and copy properties if it's a new bean.  If explicitly updating a bean, don't do this check
		if (mdv.getDataField().isCaptureChangesOnly() && mdv.isNewBean()) {
			// find previous value
			MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
			searchForm.setDataFieldId(mdv.getDataField().getId());
			searchForm.setDataSourceId(mdv.getDataSource().getId());
			searchForm.setInvestmentSecurityId(mdv.getInvestmentSecurity().getId());
			searchForm.setMaxMeasureDateAndTime(mdv.getMeasureDateWithTime());
			searchForm.setOrderBy("measureDate:desc#measureTime:desc");
			searchForm.setLimit(1);
			MarketDataValue previousValue = CollectionUtils.getFirstElement(getMarketDataValueList(searchForm));

			//  if it's the same, replace passed value with previous and only update the UpdateDate/User fields
			if (previousValue != null && previousValue.getMeasureValue().compareTo(mdv.getMeasureValue()) == 0) {
				BeanUtils.copyProperties(previousValue, mdv);
			}
		}

		return getMarketDataValueDAO().save(mdv);
	}

	////////////////////////////////////////////////////////////////////////////
	//////          Market Data Field Group Business Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataFieldGroup getMarketDataFieldGroup(short id) {
		return getMarketDataFieldGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataFieldGroup getMarketDataFieldGroupByName(String name) {
		return getMarketDataFieldGroupCache().getBeanForKeyValueStrict(getMarketDataFieldGroupDAO(), name);
	}


	@Override
	public List<MarketDataFieldGroup> getMarketDataFieldGroupList(MarketDataFieldGroupSearchForm searchForm) {
		return getMarketDataFieldGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<MarketDataFieldGroup> getMarketDataFieldGroupListByField(final short dataFieldId) {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("fieldList", "f").add(Restrictions.eq("f.id", dataFieldId));
		return getMarketDataFieldGroupDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public MarketDataFieldGroup saveMarketDataFieldGroup(MarketDataFieldGroup bean) {
		return getMarketDataFieldGroupDAO().save(bean);
	}


	@Override
	public void deleteMarketDataFieldGroup(short id) {
		getMarketDataFieldGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////      MarketDataFieldFieldGroup Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<MarketDataFieldFieldGroup> getMarketDataFieldFieldGroupListByGroup(String groupName) {
		MarketDataFieldGroup group = getMarketDataFieldGroupByName(groupName);
		return getMarketDataFieldFieldGroupCache().getBeanListForKeyValue(getMarketDataFieldFieldGroupDAO(), group.getId());
	}


	@Override
	public boolean isMarketDataFieldInGroup(String groupName, short dataFieldId) {
		MarketDataFieldGroup group = getMarketDataFieldGroupByName(groupName);
		return !CollectionUtils.isEmpty(getMarketDataFieldFieldGroupDAO().findByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{dataFieldId, group.getId()}));
	}


	@Override
	public MarketDataFieldFieldGroup linkMarketDataFieldToGroup(short dataFieldId, short dataFieldGroupId) {
		MarketDataFieldFieldGroup link = new MarketDataFieldFieldGroup();
		MarketDataField dataField = getMarketDataFieldDAO().findByPrimaryKey(dataFieldId);
		MarketDataFieldGroup fieldGroup = getMarketDataFieldGroupDAO().findByPrimaryKey(dataFieldGroupId);
		link.setReferenceOne(dataField);
		link.setReferenceTwo(fieldGroup);
		getMarketDataFieldFieldGroupDAO().save(link);
		return link;
	}


	@Override
	public void deleteMarketDataFieldFieldGroup(short dataFieldId, short dataFieldGroupId) {
		MarketDataFieldFieldGroup link = getMarketDataFieldFieldGroupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{dataFieldId, dataFieldGroupId});
		getMarketDataFieldFieldGroupDAO().delete(link);
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataField, Criteria> getMarketDataFieldDAO() {
		return this.marketDataFieldDAO;
	}


	public void setMarketDataFieldDAO(AdvancedUpdatableDAO<MarketDataField, Criteria> marketDataFieldDAO) {
		this.marketDataFieldDAO = marketDataFieldDAO;
	}


	public AdvancedUpdatableDAO<MarketDataValue, Criteria> getMarketDataValueDAO() {
		return this.marketDataValueDAO;
	}


	public void setMarketDataValueDAO(AdvancedUpdatableDAO<MarketDataValue, Criteria> marketDataValueDAO) {
		this.marketDataValueDAO = marketDataValueDAO;
	}


	public AdvancedUpdatableDAO<MarketDataFieldGroup, Criteria> getMarketDataFieldGroupDAO() {
		return this.marketDataFieldGroupDAO;
	}


	public void setMarketDataFieldGroupDAO(AdvancedUpdatableDAO<MarketDataFieldGroup, Criteria> marketDataFieldGroupDAO) {
		this.marketDataFieldGroupDAO = marketDataFieldGroupDAO;
	}


	public AdvancedUpdatableDAO<MarketDataFieldFieldGroup, Criteria> getMarketDataFieldFieldGroupDAO() {
		return this.marketDataFieldFieldGroupDAO;
	}


	public void setMarketDataFieldFieldGroupDAO(AdvancedUpdatableDAO<MarketDataFieldFieldGroup, Criteria> marketDataFieldFieldGroupDAO) {
		this.marketDataFieldFieldGroupDAO = marketDataFieldFieldGroupDAO;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public DaoNamedEntityCache<MarketDataField> getMarketDataFieldCache() {
		return this.marketDataFieldCache;
	}


	public void setMarketDataFieldCache(DaoNamedEntityCache<MarketDataField> marketDataFieldCache) {
		this.marketDataFieldCache = marketDataFieldCache;
	}


	public MarketDataValueCache getMarketDataValueCache() {
		return this.marketDataValueCache;
	}


	public void setMarketDataValueCache(MarketDataValueCache marketDataValueCache) {
		this.marketDataValueCache = marketDataValueCache;
	}


	public MarketDataValueCache getMarketDataValueFlexibleCache() {
		return this.marketDataValueFlexibleCache;
	}


	public void setMarketDataValueFlexibleCache(MarketDataValueCache marketDataValueFlexibleCache) {
		this.marketDataValueFlexibleCache = marketDataValueFlexibleCache;
	}


	public DaoNamedEntityCache<MarketDataFieldGroup> getMarketDataFieldGroupCache() {
		return this.marketDataFieldGroupCache;
	}


	public void setMarketDataFieldGroupCache(DaoNamedEntityCache<MarketDataFieldGroup> marketDataFieldGroupCache) {
		this.marketDataFieldGroupCache = marketDataFieldGroupCache;
	}


	public SystemHierarchyAssignmentService getSystemHierarchyAssignmentService() {
		return this.systemHierarchyAssignmentService;
	}


	public void setSystemHierarchyAssignmentService(SystemHierarchyAssignmentService systemHierarchyAssignmentService) {
		this.systemHierarchyAssignmentService = systemHierarchyAssignmentService;
	}


	public DaoSingleKeyListCache<MarketDataFieldFieldGroup, Short> getMarketDataFieldFieldGroupCache() {
		return this.marketDataFieldFieldGroupCache;
	}


	public void setMarketDataFieldFieldGroupCache(DaoSingleKeyListCache<MarketDataFieldFieldGroup, Short> marketDataFieldFieldGroupCache) {
		this.marketDataFieldFieldGroupCache = marketDataFieldFieldGroupCache;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}
}
