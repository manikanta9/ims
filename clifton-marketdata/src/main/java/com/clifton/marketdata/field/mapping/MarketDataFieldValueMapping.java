package com.clifton.marketdata.field.mapping;


import com.clifton.core.beans.BaseEntity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;


/**
 * The <code>MarketDataFieldValueMapping</code> used to remap incoming market data provider values to
 * the values that can be understood by our system. This is used to automate security creation process:
 * security attributes can be auto-populated from external market data provider fields.
 * <p>
 * For example, Bloomberg's values for "Coupon Frequency" are numeric and are mapped to our enums:
 * 1 => Annual
 * 2 => SemiAnnual
 * 4 => Quarterly
 *
 * @author mwacker
 */
public class MarketDataFieldValueMapping extends BaseEntity<Short> {

	private MarketDataField dataField;
	private MarketDataSource dataSource;
	/**
	 * Value received from the market data source.
	 */
	private String fromValue;

	/**
	 * The value used and understood by our system.
	 */
	private String toValue;
	private String toValueLabel;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataField getDataField() {
		return this.dataField;
	}


	public void setDataField(MarketDataField dataField) {
		this.dataField = dataField;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public String getFromValue() {
		return this.fromValue;
	}


	public void setFromValue(String fromValue) {
		this.fromValue = fromValue;
	}


	public String getToValue() {
		return this.toValue;
	}


	public void setToValue(String toValue) {
		this.toValue = toValue;
	}


	public String getToValueLabel() {
		return this.toValueLabel;
	}


	public void setToValueLabel(String toValueLabel) {
		this.toValueLabel = toValueLabel;
	}
}
