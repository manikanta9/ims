package com.clifton.marketdata.field.column.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnGroup;
import org.springframework.stereotype.Component;

import java.util.StringJoiner;


/**
 * Implementation of {@link MarketDataFieldColumnMappingSystemColumnCache}.
 * <p>
 * This is registered as a {@link BaseDaoEventObserver} in order to invalidate its cache on necessary DAO actions.
 *
 * @param <T> the type of the entities managed by the DAO event observer portion of the class
 * @author MikeH
 */
@Component
public class MarketDataFieldColumnMappingSystemColumnCacheImpl<T extends IdentityObject>
		extends BaseDaoEventObserver<T>
		implements CustomCache<String, SystemColumn>, MarketDataFieldColumnMappingSystemColumnCache {

	private CacheHandler<String, SystemColumn> cacheHandler;


	@Override
	public SystemColumn getSystemColumnForMapping(Integer templateId, Short columnGroupId, String linkedValue) {
		return getCacheHandler().get(getCacheName(), getConstructedKey(templateId, columnGroupId, linkedValue));
	}


	@Override
	public void setSystemColumnForMapping(Integer templateId, Short columnGroupId, String linkedValue, SystemColumn column) {
		getCacheHandler().put(getCacheName(), getConstructedKey(templateId, columnGroupId, linkedValue), column);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the constructed key given the provided arguments.
	 *
	 * @param templateId    the template ID used for this key
	 * @param columnGroupId the column group ID used for this key
	 * @param linkedValue   the linked value used for this key
	 * @return the constructed key
	 */
	private static String getConstructedKey(Integer templateId, Short columnGroupId, String linkedValue) {
		StringJoiner key = new StringJoiner(",");
		key.add(String.valueOf(templateId));
		key.add(String.valueOf(columnGroupId));
		if (linkedValue != null) {
			key.add(String.valueOf(linkedValue));
		}
		return key.toString();
	}


	/**
	 * Gets the constructed key given the provided arguments.
	 *
	 * @param systemColumn the column for which the key will be constructed
	 * @return the constructed key
	 */
	private static String getConstructedKey(SystemColumn systemColumn) {
		// Guard-clause: No template associated with column
		if (systemColumn.getTemplate() == null) {
			return null;
		}

		String linkedValue = null;
		if (systemColumn instanceof SystemColumnCustom) {
			linkedValue = ((SystemColumnCustom) systemColumn).getLinkedValue();
		}
		return getConstructedKey(systemColumn.getTemplate().getId(), systemColumn.getTemplate().getSystemColumnGroup().getId(), linkedValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Observer Methods                                ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// Guard-clause: Ignore failed transactions
		if (e != null) {
			return;
		}

		// Evict cache entries for changed/removed entries
		if (bean instanceof SystemColumnGroup) {
			/*
			 * Logic could be implemented here to clear caches only as necessary. More specifically, when a column
			 * group is deleted then all cached items for that column group should be removed.
			 *
			 * However, column groups should not change often (if at all), so invalidating the whole cache here is
			 * sufficient.
			 */
			if (event.isDelete()) {
				getCacheHandler().clear(getCacheName());
			}
		}
		else if (bean instanceof SystemColumn) {
			// Evict entry at given column's current key
			String key = getConstructedKey((SystemColumn) bean);
			getCacheHandler().remove(getCacheName(), key);

			// Evict entry at updated column's old key
			SystemColumn originalColumn = (SystemColumn) getOriginalBean(dao, bean);
			if (originalColumn != null) {
				String originalKey = getConstructedKey(originalColumn);
				if (!StringUtils.isEqual(key, originalKey)) {
					getCacheHandler().remove(getCacheName(), originalKey);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, SystemColumn> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, SystemColumn> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}
}
