package com.clifton.marketdata.field;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>MarketDataFieldGroup</code> class represents a named set of market data fields.
 * Multiple fields can be grouped and used together in other parts of the system.
 * For example, a field group can be mapped to a specific hierarchy of investment instruments
 * so that all fields are retrieved from a market data source for that hierarchy.
 *
 * @author vgomelsky
 */
@CacheByName
public class MarketDataFieldGroup extends NamedEntity<Short> {

	public static final String GROUP_KEY_RATE_DURATION = "Key Rate Duration Points";
	public static final String GROUP_DISCOUNT_CURVE_YEAR_RATES = "Discount Curve Year Rates";

	////////////////////////////////////////////////////////////////////////////

	/**
	 * If true, then the name of the group cannot be changed.
	 */
	private boolean systemDefined;

	/**
	 * If true, then field order on fields used in this group requires order to be populated
	 */
	private boolean fieldOrderRequired;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isFieldOrderRequired() {
		return this.fieldOrderRequired;
	}


	public void setFieldOrderRequired(boolean fieldOrderRequired) {
		this.fieldOrderRequired = fieldOrderRequired;
	}
}
