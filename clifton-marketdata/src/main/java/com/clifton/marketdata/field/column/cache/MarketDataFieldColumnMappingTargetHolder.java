package com.clifton.marketdata.field.column.cache;


import com.clifton.core.cache.specificity.SpecificityScope;
import com.clifton.core.cache.specificity.SpecificityTargetHolder;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;


/**
 * The <code>MarketDataFieldColumnMappingTargetHolder</code> contains the meta data for a AccountingCommissionDefinition
 * and is used in the AccountingCommissionDefinitionSpecificityCache.
 *
 * @author jgommels
 */
class MarketDataFieldColumnMappingTargetHolder implements SpecificityTargetHolder {

	private final String key;
	private final int id;
	private final SpecificityScope scope;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldColumnMappingTargetHolder(String key, MarketDataFieldColumnMapping mapping) {
		this.key = key;
		this.id = mapping.getId();
		this.scope = MarketDataFieldColumnMappingSpecificityScope.ofColumnMapping(mapping);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.id;
		result = prime * result + ((this.key == null) ? 0 : this.key.hashCode());
		result = prime * result + (this.scope.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		MarketDataFieldColumnMappingTargetHolder other = (MarketDataFieldColumnMappingTargetHolder) obj;
		if (this.id != other.id) {
			return false;
		}
		if (this.key == null) {
			if (other.key != null) {
				return false;
			}
		}
		else if (!this.key.equals(other.key)) {
			return false;
		}
		if (!this.scope.equals(other.scope)) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getId() {
		return this.id;
	}


	@Override
	public String getKey() {
		return this.key;
	}


	@Override
	public SpecificityScope getScope() {
		return this.scope;
	}
}
