package com.clifton.marketdata.field.column.value;

import com.clifton.core.beans.IdentityObject;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.value.SystemColumnValue;

import java.util.List;


/**
 * Holder for the objects need to resolve the value or lookup the foreign key object for a column mapping.
 *
 * @author mwacker
 */
public class ColumnMappingValueResolverCommand<T extends IdentityObject> {

	private final List<SystemColumnValue> columnValueList;
	private final SystemColumn column;
	private final MarketDataFieldColumnMapping columnMapping;
	private final boolean applyIfNotNull;
	private final Object receivedValue;
	private String convertedValue;
	private final T targetObject;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ColumnMappingValueResolverCommand(List<SystemColumnValue> columnValueList, SystemColumn column, MarketDataFieldColumnMapping columnMapping, boolean applyIfNotNull, Object receivedValue, T targetObject) {
		this.columnValueList = columnValueList;
		this.column = column;
		this.columnMapping = columnMapping;
		this.applyIfNotNull = applyIfNotNull;
		this.receivedValue = receivedValue;
		this.targetObject = targetObject;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumn getColumn() {
		return this.column == null ? this.columnMapping.getColumn() : this.column;
	}


	public List<SystemColumnValue> getColumnValueList() {
		return this.columnValueList;
	}


	public MarketDataFieldColumnMapping getColumnMapping() {
		return this.columnMapping;
	}


	public boolean isApplyIfNotNull() {
		return this.applyIfNotNull;
	}


	public Object getReceivedValue() {
		return this.receivedValue;
	}


	public T getTargetObject() {
		return this.targetObject;
	}


	public String getConvertedValue() {
		return this.convertedValue;
	}


	public void setConvertedValue(String convertedValue) {
		this.convertedValue = convertedValue;
	}
}
