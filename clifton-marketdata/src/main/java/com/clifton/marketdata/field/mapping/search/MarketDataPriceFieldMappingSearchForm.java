package com.clifton.marketdata.field.mapping.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataPriceFieldMappingSearchForm extends BaseAuditableEntitySearchForm {


	@SearchField(leftJoin = true, searchField = "investmentType.name,investmentTypeSubType.name,investmentTypeSubType2.name,instrumentHierarchy.name,instrument.name,")
	private String searchPattern;

	@SearchField(searchField = "instrument.id")
	private Integer instrumentId;

	@SearchField(searchField = "instrumentHierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentType")
	private String investmentType;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType2")
	private String investmentTypeSubType2;

	@SearchField(searchField = "officialPriceMarketDataField.id", sortField = "officialPriceMarketDataField.name")
	private Short officialPriceMarketDataFieldId;

	@SearchField(searchField = "officialPriceMarketDataSource.id", sortField = "officialPriceMarketDataSource.name")
	private Short officialPriceMarketDataSourceId;

	@SearchField(searchField = "closingPriceMarketDataField.id", sortField = "closingPriceMarketDataField.name")
	private Short closingPriceMarketDataFieldId;

	@SearchField(searchField = "closingPriceMarketDataSource.id", sortField = "closingPriceMarketDataSource.name")
	private Short closingPriceMarketDataSourceId;

	@SearchField(searchField = "latestPriceMarketDataField.id", sortField = "latestPriceMarketDataField.name")
	private Short latestPriceMarketDataFieldId;

	@SearchField(searchField = "latestPriceMarketDataSource.id", sortField = "latestPriceMarketDataSource.name")
	private Short latestPriceMarketDataSource;

	@SearchField
	private String note;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public Short getOfficialPriceMarketDataFieldId() {
		return this.officialPriceMarketDataFieldId;
	}


	public void setOfficialPriceMarketDataFieldId(Short officialPriceMarketDataFieldId) {
		this.officialPriceMarketDataFieldId = officialPriceMarketDataFieldId;
	}


	public Short getOfficialPriceMarketDataSourceId() {
		return this.officialPriceMarketDataSourceId;
	}


	public void setOfficialPriceMarketDataSourceId(Short officialPriceMarketDataSourceId) {
		this.officialPriceMarketDataSourceId = officialPriceMarketDataSourceId;
	}


	public Short getClosingPriceMarketDataFieldId() {
		return this.closingPriceMarketDataFieldId;
	}


	public void setClosingPriceMarketDataFieldId(Short closingPriceMarketDataFieldId) {
		this.closingPriceMarketDataFieldId = closingPriceMarketDataFieldId;
	}


	public Short getClosingPriceMarketDataSourceId() {
		return this.closingPriceMarketDataSourceId;
	}


	public void setClosingPriceMarketDataSourceId(Short closingPriceMarketDataSourceId) {
		this.closingPriceMarketDataSourceId = closingPriceMarketDataSourceId;
	}


	public Short getLatestPriceMarketDataFieldId() {
		return this.latestPriceMarketDataFieldId;
	}


	public void setLatestPriceMarketDataFieldId(Short latestPriceMarketDataFieldId) {
		this.latestPriceMarketDataFieldId = latestPriceMarketDataFieldId;
	}


	public Short getLatestPriceMarketDataSource() {
		return this.latestPriceMarketDataSource;
	}


	public void setLatestPriceMarketDataSource(Short latestPriceMarketDataSource) {
		this.latestPriceMarketDataSource = latestPriceMarketDataSource;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
