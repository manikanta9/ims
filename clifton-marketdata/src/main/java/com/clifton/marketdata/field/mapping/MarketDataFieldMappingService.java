package com.clifton.marketdata.field.mapping;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldMappingSearchForm;
import com.clifton.marketdata.field.mapping.search.MarketDataFieldValueMappingSearchForm;
import com.clifton.marketdata.field.mapping.search.MarketDataPriceFieldMappingSearchForm;

import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
public interface MarketDataFieldMappingService {


	////////////////////////////////////////////////////////////////////////////
	//////          Market Data Field Mapping Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldMapping getMarketDataFieldMapping(int id);


	public List<MarketDataFieldMapping> getMarketDataFieldMappingList(MarketDataFieldMappingSearchForm searchForm);


	/**
	 * Returns the most specific mapping for the instrument for each data source that has a mapping
	 */
	public List<MarketDataFieldMapping> getMarketDataFieldMappingListByInstrument(int instrumentId);


	public MarketDataFieldMapping saveMarketDataFieldMapping(MarketDataFieldMapping bean);


	public void deleteMarketDataFieldMapping(int id);


	////////////////////////////////////////////////////////////////////////////
	//////       Market Data Price Field Mapping Business Methods        ///////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataPriceFieldMapping getMarketDataPriceFieldMapping(int id);


	/**
	 * Returns the most specific {@link MarketDataPriceFieldMapping} that the given instrument uses.
	 */
	public MarketDataPriceFieldMapping getMarketDataPriceFieldMappingByInstrument(int instrumentId);


	public List<MarketDataPriceFieldMapping> getMarketDataPriceFieldMappingList(MarketDataPriceFieldMappingSearchForm searchForm);


	public MarketDataPriceFieldMapping saveMarketDataPriceFieldMapping(MarketDataPriceFieldMapping bean);


	public void deleteMarketDataPriceFieldMapping(int id);


	////////////////////////////////////////////////////////////////////////////
	////////     Market Data Field Value Mapping Business Methods     //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldValueMapping getMarketDataFieldValueMapping(short id);


	public List<MarketDataFieldValueMapping> getMarketDataFieldValueMappingList(MarketDataFieldValueMappingSearchForm searchForm);


	public MarketDataFieldValueMapping saveMarketDataFieldValueMapping(MarketDataFieldValueMapping bean);


	public void deleteMarketDataFieldValueMapping(short id);


	/**
	 * Returns a Map of "from value" to "to value" mappings for the specified data field and data source.
	 */
	@SecureMethod(dtoClass = MarketDataFieldValueMapping.class)
	public Map<String, String> getMarketDataFieldValueMappingMap(short dataFieldId, short dataSourceId);
}
