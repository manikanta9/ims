package com.clifton.marketdata.field;


import com.clifton.core.util.date.Time;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataValueHolder</code> objects holds key fields of the MarketDataValue object.
 * It is a light weight version of MarketDataValue object used to minimize memory footprint
 * when caching market data values.
 *
 * @author vgomelsky
 */
public class MarketDataValueHolder implements Serializable {

	private BigDecimal measureValue;

	/**
	 * Measure value adjusted by the adjustmentFactor - Most cases this is 1
	 */
	private BigDecimal measureValueAdjusted;

	private BigDecimal measureValueAdjustmentFactor;

	private Date measureDate;

	private Time measureTime;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataValueHolder(MarketDataValue value) {
		if (value != null) {
			this.measureValue = value.getMeasureValue();
			this.measureValueAdjusted = value.getMeasureValueAdjusted();
			this.measureDate = value.getMeasureDate();
			this.measureTime = value.getMeasureTime();
			this.measureValueAdjustmentFactor = value.getMeasureValueAdjustmentFactor();
		}
	}


	public BigDecimal getMeasureValue() {
		return this.measureValue;
	}


	public void setMeasureValue(BigDecimal measureValue) {
		this.measureValue = measureValue;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Time getMeasureTime() {
		return this.measureTime;
	}


	public void setMeasureTime(Time measureTime) {
		this.measureTime = measureTime;
	}


	public BigDecimal getMeasureValueAdjusted() {
		return this.measureValueAdjusted;
	}


	public void setMeasureValueAdjusted(BigDecimal measureValueAdjusted) {
		this.measureValueAdjusted = measureValueAdjusted;
	}


	public BigDecimal getMeasureValueAdjustmentFactor() {
		return this.measureValueAdjustmentFactor;
	}


	public void setMeasureValueAdjustmentFactor(BigDecimal measureValueAdjustmentFactor) {
		this.measureValueAdjustmentFactor = measureValueAdjustmentFactor;
	}
}
