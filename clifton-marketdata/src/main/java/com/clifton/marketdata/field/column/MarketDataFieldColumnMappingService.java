package com.clifton.marketdata.field.column;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;

import java.util.List;
import java.util.Map;


public interface MarketDataFieldColumnMappingService {

	//////////////////////////////////////////////////////////////////////////// 
	//////              Market Data Field Column Mapping               ///////// 
	////////////////////////////////////////////////////////////////////////////


	public MarketDataFieldColumnMapping getMarketDataFieldColumnMapping(int id);


	public List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingList(MarketDataFieldColumnMappingSearchForm searchForm);


	public <T extends IdentityObject> List<MarketDataFieldColumnMapping> getMarketDataFieldColumnMappingListForBean(MarketDataFieldColumnMappingCommand<T> command);


	public MarketDataFieldColumnMapping saveMarketDataFieldColumnMapping(MarketDataFieldColumnMapping bean);


	public void deleteMarketDataFieldColumnMapping(int id);


	////////////////////////////////////////////////////////////////////////////  
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public <T extends IdentityObject> T applyMarketDataValue(Map<MarketDataFieldColumnMapping, Object> valueMap, T object, List<String> errors, boolean applyIfNotNull);


	@DoNotAddRequestMapping
	public <T extends IdentityObject> SystemColumn resolveSystemColumnFromTemplate(MarketDataFieldColumnMapping mapping, T object);


	@DoNotAddRequestMapping
	public List<SystemColumnCustom> resolveSystemColumnListFromTemplate(MarketDataFieldColumnMapping mapping);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Populate an object using the market data field mappings.
	 */
	@DoNotAddRequestMapping
	public <T extends IdentityObject> Map<MarketDataFieldColumnMapping, Object> getMarketDataFieldValueMap(MarketDataFieldColumnMappingCommand<T> command);
}
