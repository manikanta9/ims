package com.clifton.marketdata.field.mapping.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.investment.exchange.InvestmentExchangeTypes;


public class MarketDataFieldMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(leftJoin = true, searchField = "investmentType.name,investmentTypeSubType.name,investmentTypeSubType2.name,instrumentHierarchy.name,instrument.name,")
	private String searchPattern;

	@SearchField(searchField = "instrument.id")
	private Integer instrumentId;

	@SearchField(searchField = "instrument.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInstrumentId;

	@SearchField(searchField = "instrumentHierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "instrumentHierarchy.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInstrumentHierarchy;

	@SearchField(searchField = "marketDataSource.id", sortField = "marketDataSource.name")
	private Short dataSourceId;

	@SearchField(searchField = "fieldGroup.id")
	private Short fieldGroupId;

	@SearchField(searchField = "marketSector.id")
	private Short marketSectorId;

	@SearchField(searchField = "name,label", searchFieldPath = "marketSector")
	private String marketSector;

	@SearchField(searchField = "pricingSource.id")
	private Short pricingSourceId;

	@SearchField(searchField = "name", searchFieldPath = "pricingSource")
	private String pricingSource;

	@SearchField
	private InvestmentExchangeTypes exchangeCodeType;


	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentType")
	private String investmentType;

	@SearchField(searchField = "investmentType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentType;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "name", searchFieldPath = "investmentTypeSubType2")
	private String investmentTypeSubType2;

	@SearchField(searchField = "investmentTypeSubType2.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType2;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public void setInstrumentId(Integer instrumentId) {
		this.instrumentId = instrumentId;
	}


	public Boolean getNullInstrumentId() {
		return this.nullInstrumentId;
	}


	public void setNullInstrumentId(Boolean nullInstrumentId) {
		this.nullInstrumentId = nullInstrumentId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Boolean getNullInstrumentHierarchy() {
		return this.nullInstrumentHierarchy;
	}


	public void setNullInstrumentHierarchy(Boolean nullInstrumentHierarchy) {
		this.nullInstrumentHierarchy = nullInstrumentHierarchy;
	}


	public Short getFieldGroupId() {
		return this.fieldGroupId;
	}


	public void setFieldGroupId(Short fieldGroupId) {
		this.fieldGroupId = fieldGroupId;
	}


	public Short getMarketSectorId() {
		return this.marketSectorId;
	}


	public void setMarketSectorId(Short marketSectorId) {
		this.marketSectorId = marketSectorId;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getMarketSector() {
		return this.marketSector;
	}


	public void setMarketSector(String marketSector) {
		this.marketSector = marketSector;
	}


	public Short getPricingSourceId() {
		return this.pricingSourceId;
	}


	public void setPricingSourceId(Short pricingSourceId) {
		this.pricingSourceId = pricingSourceId;
	}


	public String getPricingSource() {
		return this.pricingSource;
	}


	public void setPricingSource(String pricingSource) {
		this.pricingSource = pricingSource;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Boolean getNullInvestmentType() {
		return this.nullInvestmentType;
	}


	public void setNullInvestmentType(Boolean nullInvestmentType) {
		this.nullInvestmentType = nullInvestmentType;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Boolean getNullInvestmentTypeSubType() {
		return this.nullInvestmentTypeSubType;
	}


	public void setNullInvestmentTypeSubType(Boolean nullInvestmentTypeSubType) {
		this.nullInvestmentTypeSubType = nullInvestmentTypeSubType;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Boolean getNullInvestmentTypeSubType2() {
		return this.nullInvestmentTypeSubType2;
	}


	public void setNullInvestmentTypeSubType2(Boolean nullInvestmentTypeSubType2) {
		this.nullInvestmentTypeSubType2 = nullInvestmentTypeSubType2;
	}


	public String getInvestmentType() {
		return this.investmentType;
	}


	public void setInvestmentType(String investmentType) {
		this.investmentType = investmentType;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}


	public InvestmentExchangeTypes getExchangeCodeType() {
		return this.exchangeCodeType;
	}


	public void setExchangeCodeType(InvestmentExchangeTypes exchangeCodeType) {
		this.exchangeCodeType = exchangeCodeType;
	}
}
