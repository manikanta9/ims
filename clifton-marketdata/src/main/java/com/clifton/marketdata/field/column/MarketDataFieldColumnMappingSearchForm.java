package com.clifton.marketdata.field.column;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataFieldColumnMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "dataField.id")
	private Short dataFieldId;

	@SearchField(searchField = "name", searchFieldPath = "dataField")
	private String dataFieldName;

	// Custom Search Field
	private Short dataFieldGroupId;

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(searchField = "name", searchFieldPath = "dataSource")
	private String dataSourceName;

	@SearchField(searchField = "column.id")
	private Integer columnId;

	@SearchField(searchField = "name", searchFieldPath = "column")
	private String columnName;

	@SearchField(searchField = "template.id")
	private Integer templateId;

	@SearchField(searchField = "name", searchFieldPath = "template")
	private String templateName;

	@SearchField
	private String beanPropertyNameForLookup;

	@SearchField(searchField = "table.id", searchFieldPath = "column")
	private Short tableId;

	@SearchField(searchField = "name", searchFieldPath = "column.table")
	private String tableName;

	@SearchField(searchField = "instrumentHierarchy.id")
	private Short instrumentHierarchyId;

	@SearchField(searchField = "instrumentHierarchy.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInstrumentHierarchy;

	@SearchField(searchField = "investmentType.id")
	private Short investmentTypeId;

	@SearchField(searchField = "investmentType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentType;

	@SearchField(searchField = "investmentTypeSubType.id")
	private Short investmentTypeSubTypeId;

	@SearchField(searchField = "investmentTypeSubType.name")
	private String investmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType;

	@SearchField(searchField = "investmentTypeSubType2.id")
	private Short investmentTypeSubType2Id;

	@SearchField(searchField = "investmentTypeSubType2.name")
	private String investmentTypeSubType2;

	@SearchField(searchField = "investmentTypeSubType2.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean nullInvestmentTypeSubType2;

	// Custom Search Fields
	private String linkedValue;

	// include null can be used when a linked value is supplied, but
	// also want all fields that have no linked value.
	private Boolean includeNullLinkedValue;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDataFieldId() {
		return this.dataFieldId;
	}


	public void setDataFieldId(Short dataFieldId) {
		this.dataFieldId = dataFieldId;
	}


	public String getDataFieldName() {
		return this.dataFieldName;
	}


	public void setDataFieldName(String dataFieldName) {
		this.dataFieldName = dataFieldName;
	}


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Integer getColumnId() {
		return this.columnId;
	}


	public void setColumnId(Integer columnId) {
		this.columnId = columnId;
	}


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public String getBeanPropertyNameForLookup() {
		return this.beanPropertyNameForLookup;
	}


	public void setBeanPropertyNameForLookup(String beanPropertyNameForLookup) {
		this.beanPropertyNameForLookup = beanPropertyNameForLookup;
	}


	public Short getTableId() {
		return this.tableId;
	}


	public void setTableId(Short tableId) {
		this.tableId = tableId;
	}


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getLinkedValue() {
		return this.linkedValue;
	}


	public void setLinkedValue(String linkedValue) {
		this.linkedValue = linkedValue;
	}


	public Boolean getIncludeNullLinkedValue() {
		return this.includeNullLinkedValue;
	}


	public void setIncludeNullLinkedValue(Boolean includeNullLinkedValue) {
		this.includeNullLinkedValue = includeNullLinkedValue;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getDataFieldGroupId() {
		return this.dataFieldGroupId;
	}


	public void setDataFieldGroupId(Short dataFieldGroupId) {
		this.dataFieldGroupId = dataFieldGroupId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public void setInvestmentTypeSubTypeId(Short investmentTypeSubTypeId) {
		this.investmentTypeSubTypeId = investmentTypeSubTypeId;
	}


	public Integer getTemplateId() {
		return this.templateId;
	}


	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}


	public String getTemplateName() {
		return this.templateName;
	}


	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}


	public Boolean getNullInstrumentHierarchy() {
		return this.nullInstrumentHierarchy;
	}


	public void setNullInstrumentHierarchy(Boolean nullInstrumentHierarchy) {
		this.nullInstrumentHierarchy = nullInstrumentHierarchy;
	}


	public Boolean getNullInvestmentType() {
		return this.nullInvestmentType;
	}


	public void setNullInvestmentType(Boolean nullInvestmentType) {
		this.nullInvestmentType = nullInvestmentType;
	}


	public Boolean getNullInvestmentTypeSubType() {
		return this.nullInvestmentTypeSubType;
	}


	public void setNullInvestmentTypeSubType(Boolean nullInvestmentTypeSubType) {
		this.nullInvestmentTypeSubType = nullInvestmentTypeSubType;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}


	public void setInvestmentTypeSubType2Id(Short investmentTypeSubType2Id) {
		this.investmentTypeSubType2Id = investmentTypeSubType2Id;
	}


	public Boolean getNullInvestmentTypeSubType2() {
		return this.nullInvestmentTypeSubType2;
	}


	public void setNullInvestmentTypeSubType2(Boolean nullInvestmentTypeSubType2) {
		this.nullInvestmentTypeSubType2 = nullInvestmentTypeSubType2;
	}


	public String getInvestmentTypeSubType() {
		return this.investmentTypeSubType;
	}


	public void setInvestmentTypeSubType(String investmentTypeSubType) {
		this.investmentTypeSubType = investmentTypeSubType;
	}


	public String getInvestmentTypeSubType2() {
		return this.investmentTypeSubType2;
	}


	public void setInvestmentTypeSubType2(String investmentTypeSubType2) {
		this.investmentTypeSubType2 = investmentTypeSubType2;
	}
}
