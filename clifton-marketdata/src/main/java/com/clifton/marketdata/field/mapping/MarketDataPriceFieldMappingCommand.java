package com.clifton.marketdata.field.mapping;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;


/**
 * @author manderson
 */
@NonPersistentObject
public class MarketDataPriceFieldMappingCommand {

	private final Integer instrumentId;
	private final Short instrumentHierarchyId;
	private final Short investmentTypeId;
	private final Short investmentTypeSubTypeId;
	private final Short investmentTypeSubType2Id;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataPriceFieldMappingCommand(InvestmentInstrument investmentInstrument) {
		ValidationUtils.assertNotNull(investmentInstrument, "Instrument Is Required");

		this.instrumentId = investmentInstrument.getId();
		InvestmentInstrumentHierarchy hierarchy = investmentInstrument.getHierarchy();

		this.instrumentHierarchyId = hierarchy.getId();
		this.investmentTypeId = (hierarchy.getInvestmentType() != null) ? hierarchy.getInvestmentType().getId() : null;
		this.investmentTypeSubTypeId = (hierarchy.getInvestmentTypeSubType() != null) ? hierarchy.getInvestmentTypeSubType().getId() : null;
		this.investmentTypeSubType2Id = (hierarchy.getInvestmentTypeSubType2() != null) ? hierarchy.getInvestmentTypeSubType2().getId() : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getInstrumentId() {
		return this.instrumentId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public Short getInvestmentTypeSubTypeId() {
		return this.investmentTypeSubTypeId;
	}


	public Short getInvestmentTypeSubType2Id() {
		return this.investmentTypeSubType2Id;
	}
}
