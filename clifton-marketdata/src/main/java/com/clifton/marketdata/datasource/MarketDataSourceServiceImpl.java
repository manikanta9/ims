package com.clifton.marketdata.datasource;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.shared.Company;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.cache.MarketDataSourceCache;
import com.clifton.marketdata.datasource.cache.MarketDataSourcePriceMultiplierCache;
import com.clifton.marketdata.datasource.cache.MarketDataSourceSecurityCache;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.search.MarketDataSourcePriceMultiplierSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourcePricingSourceSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourceSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourceSecuritySearchForm;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * The <code>MarketDataSourceServiceImpl</code> class provides basic implementation of the MarketDataSourceService interface.
 *
 * @author vgomelsky
 */
@Service
public class MarketDataSourceServiceImpl implements MarketDataSourceService {

	private AdvancedUpdatableDAO<MarketDataSource, Criteria> marketDataSourceDAO;
	private AdvancedUpdatableDAO<MarketDataSourceSector, Criteria> marketDataSourceSectorDAO;
	private AdvancedUpdatableDAO<MarketDataSourceSecurity, Criteria> marketDataSourceSecurityDAO;
	private AdvancedUpdatableDAO<MarketDataSourcePriceMultiplier, Criteria> marketDataSourcePriceMultiplierDAO;
	private AdvancedUpdatableDAO<MarketDataSourcePricingSource, Criteria> marketDataSourcePricingSourceDAO;

	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	private MarketDataSourceCache marketDataSourceCache;
	private MarketDataSourcePriceMultiplierCache marketDataSourcePriceMultiplierCache;
	private MarketDataSourceSecurityCache marketDataSourceSecurityCache;
	private DaoSingleKeyListCache<MarketDataSourceSecurity, Integer> marketDataSourceSecurityListBySecurityCache;


	////////////////////////////////////////////////////////////////////////////
	//////             Market Data Source Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getMarketDataSourceNameStrict(Company company) {
		MarketDataSource dataSource = getMarketDataSourceByCompanyInternal(company, true, 0);
		return dataSource.getName();
	}


	@Override
	public String getMarketDataSourceName(BusinessCompany company) {
		if (company != null) {
			MarketDataSource dataSource = getMarketDataSourceByCompanyInternal(company.toCompany(), false, 0);
			return dataSource != null ? dataSource.getName() : null;
		}
		return null;
	}


	protected MarketDataSource getMarketDataSourceByCompanyInternal(Company company, boolean strict, int depth) {
		// TODO: it's better to match based on company id tied to data source: account for parent and cash
		MarketDataSource result = null;
		if (company != null && depth <= 20) {
			result = getMarketDataSourceByNameInternal(company.getName());
			if (result == null && company.getParentCompany() != null) {
				depth++;
				return getMarketDataSourceByCompanyInternal(company.getParentCompany(), strict, depth);
			}
		}
		if (strict && result == null) {
			throw new EntityNotFoundValidationException("Unable to find Market Data Source with Name [" + (company != null ? company.getName() : "") + "]", MarketDataSource.class.getName());
		}
		return result;
	}


	@Override
	public MarketDataSource getMarketDataSourceByCompany(BusinessCompany company) {
		return getMarketDataSourceByCompanyInternal(company.toCompany(), false, 0);
	}


	@Override
	public MarketDataSource getMarketDataSource(short id) {
		return getMarketDataSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataSource getMarketDataSourceByName(String marketDatasourceName) {
		MarketDataSource dataSource = getMarketDataSourceByNameInternal(marketDatasourceName);
		if (dataSource == null) {
			throw new EntityNotFoundValidationException("Unable to find Market Data Source with Name [" + marketDatasourceName + "]", MarketDataSource.class.getName());
		}
		return dataSource;
	}


	protected MarketDataSource getMarketDataSourceByNameInternal(String marketDatasourceName) {
		ObjectWrapper<MarketDataSource> wrappedDataSource = getMarketDataSourceCache().getMarketDataSource(marketDatasourceName);
		MarketDataSource dataSource;
		if (wrappedDataSource == null) {
			dataSource = getMarketDataSourceDAO().findOneByField("name", marketDatasourceName);
			getMarketDataSourceCache().setMarketDataSource(dataSource, marketDatasourceName);
		}
		else {
			dataSource = wrappedDataSource.getObject();
		}


		return dataSource;
	}


	@Override
	public List<MarketDataSource> getMarketDataSourceList(MarketDataSourceSearchForm searchForm) {
		return getMarketDataSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSource saveMarketDataSource(MarketDataSource bean) {
		return getMarketDataSourceDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSource(short id) {
		getMarketDataSourceDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Company> getMarketDataCompanyForExchangeRatesList() {
		MarketDataSourceSearchForm searchForm = new MarketDataSourceSearchForm();
		searchForm.setExchangeRateSupported(true);
		List<MarketDataSource> fxSourceList = getMarketDataSourceList(searchForm);
		return CollectionUtils.getStream(fxSourceList).map(dataSource -> dataSource.getCompany().toCompany()).sorted(Comparator.comparing(Company::getName)).collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	//////            Market Data Sector Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourceSector getMarketDataSourceSector(short id) {
		return getMarketDataSourceSectorDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataSourceSector getMarketDataSourceSectorByNameAndDataSourceName(String marketDataSourceSectorName, String marketDatasourceName) {
		return getMarketDataSourceSectorDAO().findOneByFields(new String[]{"name", "dataSource.name"}, new Object[]{marketDataSourceSectorName, marketDatasourceName});//("dataSource.id", dataSourceId);
	}


	@Override
	public MarketDataSourceSector getMarketDataSourceSectorByNameAndDataSourceId(String marketDataSourceSectorName, Short marketDataSourceId) {
		return getMarketDataSourceSectorDAO().findOneByFields(new String[]{"name", "dataSource.id"}, new Object[]{marketDataSourceSectorName, marketDataSourceId});//("dataSource.id", dataSourceId);
	}


	@Override
	public List<MarketDataSourceSector> getMarketDataSourceSectorListByDataSource(short dataSourceId) {
		return getMarketDataSourceSectorDAO().findByField("dataSource.id", dataSourceId);
	}


	@Override
	public MarketDataSourceSector saveMarketDataSourceSector(MarketDataSourceSector bean) {
		return getMarketDataSourceSectorDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourceSector(short id) {
		getMarketDataSourceSectorDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////           Market Data Security Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourceSecurity getMarketDataSourceSecurity(int id) {
		return getMarketDataSourceSecurityDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityListByDataSource(short dataSourceId) {
		return getMarketDataSourceSecurityDAO().findByField("dataSource.id", dataSourceId);
	}


	@Override
	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityListBySecurity(int securityId) {
		return CollectionUtils.asNonNullList(getMarketDataSourceSecurityListBySecurityCache().getBeanListForKeyValue(getMarketDataSourceSecurityDAO(), securityId));
	}


	@Override
	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityList(MarketDataSourceSecuritySearchForm searchForm) {
		return getMarketDataSourceSecurityDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourceSecurity saveMarketDataSourceSecurity(MarketDataSourceSecurity bean) {
		return getMarketDataSourceSecurityDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourceSecurity(int id) {
		getMarketDataSourceSecurityDAO().delete(id);
	}


	@Override
	public MarketDataSourceSecurityHolder getMarketDataSourceSecuritySymbol(String securitySymbol, String dataSourceName, String marketSector) {
		Map<String, MarketDataSourceSecurityHolder> symbolMapping = getMarketDataSourceSecurityCache().getDataSourceCache(dataSourceName);
		if (symbolMapping == null) {
			// retrieve and cache to improve performance
			MarketDataSource dataSource = getMarketDataSourceByName(dataSourceName);
			List<MarketDataSourceSecurity> securityOverrideList = getMarketDataSourceSecurityListByDataSource(dataSource.getId());
			// cache if empty to avoid future look ups when no overrides exist
			symbolMapping = new ConcurrentHashMap<>();
			for (MarketDataSourceSecurity mapping : CollectionUtils.getIterable(securityOverrideList)) {
				InvestmentSecurity security = mapping.getSecurity();
				MarketDataFieldMapping fieldMapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(security.getInstrument(), dataSource);
				if (fieldMapping == null) {
					// no data field mapping exists for this security: error out later
					symbolMapping.put(security.getSymbol() + '-' + marketSector, new MarketDataSourceSecurityHolder(security.getSymbol()));
				}
				else {
					symbolMapping.put(security.getSymbol() + '-' + fieldMapping.getMarketSector().getName(), new MarketDataSourceSecurityHolder(mapping));
				}
			}
			getMarketDataSourceSecurityCache().setDataSourceCache(dataSourceName, symbolMapping);
		}

		MarketDataSourceSecurityHolder result = symbolMapping.get(securitySymbol + '-' + marketSector);
		if (result == null) {
			// if no data source specific override found, then the symbols are the same
			return new MarketDataSourceSecurityHolder(securitySymbol);
		}
		if (result.getDataSourceSymbol() == null) {
			// there is symbol override for this data source, however data field mapping is missing for this security: invalid configuration
			throw new ValidationException("Cannot find MarketDataFieldMapping for " + securitySymbol + " for " + dataSourceName + " (" + marketSector + ")");
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	//////       Market Data Price Multiplier Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplier(int id) {
		return getMarketDataSourcePriceMultiplierDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplierByCompany(InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose) {
		String dataSourceName = getMarketDataSourceName(company);
		return getMarketDataSourcePriceMultiplierByDataSource(instrument, dataSourceName, purpose);
	}


	@Override
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplierByDataSource(InvestmentInstrument instrument, String dataSourceName, MarketDataSourcePurpose purpose) {
		if ((dataSourceName == null) || dataSourceName.isEmpty()) {
			return null;
		}
		MarketDataSourcePriceMultiplier result = doGetMarketDataSourcePriceMultiplierByDataSource(instrument, dataSourceName, purpose);
		// check for a multiplier without the purpose
		if (result == null && purpose != null) {
			result = doGetMarketDataSourcePriceMultiplierByDataSource(instrument, dataSourceName, null);
		}
		return result;
	}


	private MarketDataSourcePriceMultiplier doGetMarketDataSourcePriceMultiplierByDataSource(final InvestmentInstrument instrument, final String dataSourceName, final MarketDataSourcePurpose purpose) {
		MarketDataSourcePriceMultiplier result = getMarketDataSourcePriceMultiplierCache().getMarketDataSourcePriceMultiplier(instrument, dataSourceName, purpose);
		if (result == null) {
			HibernateSearchConfigurer config = criteria -> {
				criteria.add(Restrictions.eq("investmentInstrument.id", instrument.getId()));
				if (purpose == null) {
					criteria.add(Restrictions.isNull("marketDataSourcePurpose.id"));
				}
				else {
					criteria.add(Restrictions.eq("marketDataSourcePurpose.id", purpose.getId()));
				}
				Criteria m = criteria.createAlias("marketDataSource", "m");
				m.add(Restrictions.eq("m.name", dataSourceName));
			};
			result = CollectionUtils.getOnlyElement(getMarketDataSourcePriceMultiplierDAO().findBySearchCriteria(config));
			if (result != null) {
				getMarketDataSourcePriceMultiplierCache().setMarketDataSourcePriceMultiplier(result);
			}
		}
		return result;
	}


	@Override
	public List<MarketDataSourcePriceMultiplier> getMarketDataSourcePriceMultiplierList(MarketDataSourcePriceMultiplierSearchForm searchForm) {
		return getMarketDataSourcePriceMultiplierDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourcePriceMultiplier saveMarketDataSourcePriceMultiplier(MarketDataSourcePriceMultiplier bean) {
		return getMarketDataSourcePriceMultiplierDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourcePriceMultiplier(int id) {
		getMarketDataSourcePriceMultiplierDAO().delete(id);
	}


	@Override
	public BigDecimal adjustIncomingMarketDataSourcePrice(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose) {
		MarketDataSourcePriceMultiplier multiplier = getMarketDataSourcePriceMultiplierByCompany(instrument, company, purpose);
		if (multiplier != null) {
			return price.multiply(multiplier.getAdditionalPriceMultiplier());
		}
		return price;
	}


	@Override
	public BigDecimal adjustOutgoingMarketDataSourcePrice(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose) {
		MarketDataSourcePriceMultiplier multiplier = getMarketDataSourcePriceMultiplierByCompany(instrument, company, purpose);
		if (multiplier != null) {
			return MathUtils.divide(price, multiplier.getAdditionalPriceMultiplier());
		}
		return price;
	}


	//////////////////////////////////////////////////////////////////////////////////
	//////     Market Data Price Source Pricing Source Business Methods      /////////
	//////////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourcePricingSource getMarketDataSourcePricingSource(short id) {
		return getMarketDataSourcePricingSourceDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataSourcePricingSource> getMarketDataSourcePricingSourceList(MarketDataSourcePricingSourceSearchForm searchForm) {
		return getMarketDataSourcePricingSourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourcePricingSource saveMarketDataSourcePricingSource(MarketDataSourcePricingSource bean) {
		return getMarketDataSourcePricingSourceDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourcePricingSource(short id) {
		getMarketDataSourcePricingSourceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataSource, Criteria> getMarketDataSourceDAO() {
		return this.marketDataSourceDAO;
	}


	public void setMarketDataSourceDAO(AdvancedUpdatableDAO<MarketDataSource, Criteria> marketDataSourceDAO) {
		this.marketDataSourceDAO = marketDataSourceDAO;
	}


	public AdvancedUpdatableDAO<MarketDataSourceSector, Criteria> getMarketDataSourceSectorDAO() {
		return this.marketDataSourceSectorDAO;
	}


	public void setMarketDataSourceSectorDAO(AdvancedUpdatableDAO<MarketDataSourceSector, Criteria> marketDataSourceSectorDAO) {
		this.marketDataSourceSectorDAO = marketDataSourceSectorDAO;
	}


	public AdvancedUpdatableDAO<MarketDataSourceSecurity, Criteria> getMarketDataSourceSecurityDAO() {
		return this.marketDataSourceSecurityDAO;
	}


	public void setMarketDataSourceSecurityDAO(AdvancedUpdatableDAO<MarketDataSourceSecurity, Criteria> marketDataSourceSecurityDAO) {
		this.marketDataSourceSecurityDAO = marketDataSourceSecurityDAO;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public AdvancedUpdatableDAO<MarketDataSourcePriceMultiplier, Criteria> getMarketDataSourcePriceMultiplierDAO() {
		return this.marketDataSourcePriceMultiplierDAO;
	}


	public void setMarketDataSourcePriceMultiplierDAO(AdvancedUpdatableDAO<MarketDataSourcePriceMultiplier, Criteria> marketDataSourcePriceMultiplierDAO) {
		this.marketDataSourcePriceMultiplierDAO = marketDataSourcePriceMultiplierDAO;
	}


	public MarketDataSourcePriceMultiplierCache getMarketDataSourcePriceMultiplierCache() {
		return this.marketDataSourcePriceMultiplierCache;
	}


	public void setMarketDataSourcePriceMultiplierCache(MarketDataSourcePriceMultiplierCache marketDataSourcePriceMultiplierCache) {
		this.marketDataSourcePriceMultiplierCache = marketDataSourcePriceMultiplierCache;
	}


	public AdvancedUpdatableDAO<MarketDataSourcePricingSource, Criteria> getMarketDataSourcePricingSourceDAO() {
		return this.marketDataSourcePricingSourceDAO;
	}


	public void setMarketDataSourcePricingSourceDAO(AdvancedUpdatableDAO<MarketDataSourcePricingSource, Criteria> marketDataSourcePricingSourceDAO) {
		this.marketDataSourcePricingSourceDAO = marketDataSourcePricingSourceDAO;
	}


	public MarketDataSourceCache getMarketDataSourceCache() {
		return this.marketDataSourceCache;
	}


	public void setMarketDataSourceCache(MarketDataSourceCache marketDataSourceCache) {
		this.marketDataSourceCache = marketDataSourceCache;
	}


	public MarketDataSourceSecurityCache getMarketDataSourceSecurityCache() {
		return this.marketDataSourceSecurityCache;
	}


	public void setMarketDataSourceSecurityCache(MarketDataSourceSecurityCache marketDataSourceSecurityCache) {
		this.marketDataSourceSecurityCache = marketDataSourceSecurityCache;
	}


	public DaoSingleKeyListCache<MarketDataSourceSecurity, Integer> getMarketDataSourceSecurityListBySecurityCache() {
		return this.marketDataSourceSecurityListBySecurityCache;
	}


	public void setMarketDataSourceSecurityListBySecurityCache(DaoSingleKeyListCache<MarketDataSourceSecurity, Integer> marketDataSourceSecurityListBySecurityCache) {
		this.marketDataSourceSecurityListBySecurityCache = marketDataSourceSecurityListBySecurityCache;
	}
}
