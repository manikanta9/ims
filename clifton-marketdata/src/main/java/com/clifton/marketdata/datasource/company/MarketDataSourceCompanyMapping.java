package com.clifton.marketdata.datasource.company;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;


/**
 * The <code>MarketDataSourceCompanyMapping</code> class represents data source specific company mappings.
 * When receiving data feeds from various data sources (intraday trade files, etc.), each data source may
 * use slightly different names to reference other data sources (clearing b brokers, etc.)
 * <p>
 * We need this information in order to remap data source specific data to our format.
 * <p>
 * For example, Goldman Sachs references "Citigroup Global Markets Inc." as "CITIGROUP".
 */
public class MarketDataSourceCompanyMapping extends BaseEntity<Integer> implements SystemEntityModifyConditionAware {

	private MarketDataSource marketDataSource;
	private MarketDataSourcePurpose marketDataSourcePurpose;
	private BusinessCompany businessCompany;
	private String externalCompanyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getMarketDataSourcePurpose() != null ? getMarketDataSourcePurpose().getCompanyMappingModifyCondition() : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public BusinessCompany getBusinessCompany() {
		return this.businessCompany;
	}


	public void setBusinessCompany(BusinessCompany businessCompany) {
		this.businessCompany = businessCompany;
	}


	public String getExternalCompanyName() {
		return this.externalCompanyName;
	}


	public void setExternalCompanyName(String externalCompanyName) {
		this.externalCompanyName = externalCompanyName;
	}


	public MarketDataSourcePurpose getMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}


	public void setMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		this.marketDataSourcePurpose = marketDataSourcePurpose;
	}
}
