package com.clifton.marketdata.datasource.purpose;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourcePurposeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	public String name;

	@SearchField
	public String description;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
}
