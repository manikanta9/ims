package com.clifton.marketdata.datasource.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourceSecuritySearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "dataSource.id")
	private Short dataSourceId;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String dataSourceSymbol;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getDataSourceId() {
		return this.dataSourceId;
	}


	public void setDataSourceId(Short dataSourceId) {
		this.dataSourceId = dataSourceId;
	}


	public String getDataSourceSymbol() {
		return this.dataSourceSymbol;
	}


	public void setDataSourceSymbol(String dataSourceSymbol) {
		this.dataSourceSymbol = dataSourceSymbol;
	}
}
