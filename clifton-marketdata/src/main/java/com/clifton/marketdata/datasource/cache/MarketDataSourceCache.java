package com.clifton.marketdata.datasource.cache;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.marketdata.datasource.MarketDataSource;


/**
 * @author manderson
 */
public interface MarketDataSourceCache {

	public ObjectWrapper<MarketDataSource> getMarketDataSource(String key);


	public void setMarketDataSource(MarketDataSource bean, String key);
}
