package com.clifton.marketdata.datasource;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>MarketDataSourceSecurity</code> class represents Data Source specific investment security overrides.
 * Different market data providers (data sources) use the same security symbols and other attributes.
 * In rare cases, when a different symbol is used by a particular data source, this entity can map
 * it to our security which uses default data source conventions (symbol, etc.).
 * <p/>
 * The symbol must be unique per data source.  For example, for Bloomberg this means that dataSourceSymbol is
 * concatenation of Bloomberg ticker and yellow key.
 *
 * @author vgomelsky
 */
public class MarketDataSourceSecurity extends BaseEntity<Integer> {

	private MarketDataSource dataSource;
	private InvestmentSecurity security;

	private String dataSourceSymbol;

	/**
	 * Optional field overrides for market request for latest data (Bloomberg reference data).
	 * Must be in the following format: "field1=value1,field2=value2"
	 */
	private String externalFieldOverridesForLatest;
	/**
	 * Optional field overrides for market request for historic data (Bloomberg historic data).
	 * Must be in the following format: "field1=value1,field2=value2".
	 * Can use the following dynamic values:
	 * - ${DATE}
	 * <p/>
	 * For example for 11/28/2012 valuation date, the following string "SETTLE_DT=${DATE},SW_CURVE_DT=${DATE}"
	 * will be converted into "SETTLE_DT=20121128,SW_CURVE_DT=20121128"
	 */
	private String externalFieldOverridesForHistoric;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public String getDataSourceSymbol() {
		return this.dataSourceSymbol;
	}


	public void setDataSourceSymbol(String dataSourceSymbol) {
		this.dataSourceSymbol = dataSourceSymbol;
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}


	public String getExternalFieldOverridesForLatest() {
		return this.externalFieldOverridesForLatest;
	}


	public void setExternalFieldOverridesForLatest(String externalFieldOverridesForLatest) {
		this.externalFieldOverridesForLatest = externalFieldOverridesForLatest;
	}


	public String getExternalFieldOverridesForHistoric() {
		return this.externalFieldOverridesForHistoric;
	}


	public void setExternalFieldOverridesForHistoric(String externalFieldOverridesForHistoric) {
		this.externalFieldOverridesForHistoric = externalFieldOverridesForHistoric;
	}
}
