package com.clifton.marketdata.datasource.exchange;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>MarketDataSourceExchangeImpl</code> class provides basic implementation of the MarketDataSourceExchange interface.
 *
 * @author mwacker
 */
@Service
public class MarketDataSourceExchangeServiceImpl implements MarketDataSourceExchangeService {

	private AdvancedUpdatableDAO<MarketDataSourceExchangeMapping, Criteria> marketDataSourceExchangeMappingDAO;


	/////////////////////////////////////////////////////////////////////////////
	/////////      MarketDataSourceExchangeMapping Business Methods     /////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourceExchangeMapping getMarketDataSourceExchangeMapping(short id) {
		return getMarketDataSourceExchangeMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataSourceExchangeMapping> getMarketDataSourceExchangeMappingList(MarketDataSourceExchangeMappingSearchForm searchForm) {
		return getMarketDataSourceExchangeMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourceExchangeMapping saveMarketDataSourceExchangeMapping(MarketDataSourceExchangeMapping bean) {
		return getMarketDataSourceExchangeMappingDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourceExchangeMapping(short id) {
		getMarketDataSourceExchangeMappingDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataSourceExchangeMapping, Criteria> getMarketDataSourceExchangeMappingDAO() {
		return this.marketDataSourceExchangeMappingDAO;
	}


	public void setMarketDataSourceExchangeMappingDAO(AdvancedUpdatableDAO<MarketDataSourceExchangeMapping, Criteria> marketDataSourceExchangeMappingDAO) {
		this.marketDataSourceExchangeMappingDAO = marketDataSourceExchangeMappingDAO;
	}
}
