package com.clifton.marketdata.datasource;


import java.io.Serializable;


/**
 * The <code>MarketDataSourceSecurityHolder</code> class is used to store data source to security mapping
 * and corresponding override parameters. It can be used for caching or more efficient data transfer.
 *
 * @author vgomelsky
 */
public class MarketDataSourceSecurityHolder implements Serializable {

	private final String originalSymbol;
	private String dataSourceSymbol;
	private String externalFieldOverridesForLatest;
	private String externalFieldOverridesForHistoric;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceSecurityHolder(String originalSymbol, String dataSourceSymbol, String externalFieldOverridesForLatest, String externalFieldOverridesForHistoric) {
		this.originalSymbol = originalSymbol;
		this.dataSourceSymbol = dataSourceSymbol;
		this.externalFieldOverridesForLatest = externalFieldOverridesForLatest;
		this.externalFieldOverridesForHistoric = externalFieldOverridesForHistoric;
	}


	public MarketDataSourceSecurityHolder(MarketDataSourceSecurity mapping) {
		this.originalSymbol = mapping.getSecurity().getSymbol();
		this.dataSourceSymbol = mapping.getDataSourceSymbol();
		this.externalFieldOverridesForLatest = mapping.getExternalFieldOverridesForLatest();
		this.externalFieldOverridesForHistoric = mapping.getExternalFieldOverridesForHistoric();
	}


	public MarketDataSourceSecurityHolder(String originalSymbol) {
		this.originalSymbol = originalSymbol;
	}


	public String getSymbol() {
		return this.dataSourceSymbol == null ? this.originalSymbol : this.dataSourceSymbol;
	}


	public String getOriginalSymbol() {
		return this.originalSymbol;
	}


	public String getDataSourceSymbol() {
		return this.dataSourceSymbol;
	}


	public String getExternalFieldOverridesForLatest() {
		return this.externalFieldOverridesForLatest;
	}


	public String getExternalFieldOverridesForHistoric() {
		return this.externalFieldOverridesForHistoric;
	}
}
