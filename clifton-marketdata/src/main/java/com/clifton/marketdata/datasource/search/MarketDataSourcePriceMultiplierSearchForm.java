package com.clifton.marketdata.datasource.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourcePriceMultiplierSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "marketDataSource.name,marketDataSource.company.name,investmentInstrument.name")
	private String searchPattern;

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "marketDataSourcePurpose.id")
	private Short marketDataSourcePurposeId;

	@SearchField(searchField = "marketDataSourcePurpose.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean marketDataSourcePurposeIsNull;

	@SearchField(searchField = "marketDataSourcePurpose.id", comparisonConditions = ComparisonConditions.EQUALS_OR_IS_NULL)
	private Short marketDataSourcePurposeIdOrNull;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSourcePurpose")
	private String marketDataSourcePurposeName;

	@SearchField(searchField = "investmentInstrument.id")
	private Integer investmentInstrumentId;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSource.company")
	private String companyName;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSource")
	private String dataSourceName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public Integer getInvestmentInstrumentId() {
		return this.investmentInstrumentId;
	}


	public void setInvestmentInstrumentId(Integer investmentInstrumentId) {
		this.investmentInstrumentId = investmentInstrumentId;
	}


	public String getCompanyName() {
		return this.companyName;
	}


	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public Short getMarketDataSourcePurposeId() {
		return this.marketDataSourcePurposeId;
	}


	public void setMarketDataSourcePurposeId(Short marketDataSourcePurposeId) {
		this.marketDataSourcePurposeId = marketDataSourcePurposeId;
	}


	public Boolean getMarketDataSourcePurposeIsNull() {
		return this.marketDataSourcePurposeIsNull;
	}


	public void setMarketDataSourcePurposeIsNull(Boolean marketDataSourcePurposeIsNull) {
		this.marketDataSourcePurposeIsNull = marketDataSourcePurposeIsNull;
	}


	public Short getMarketDataSourcePurposeIdOrNull() {
		return this.marketDataSourcePurposeIdOrNull;
	}


	public void setMarketDataSourcePurposeIdOrNull(Short marketDataSourcePurposeIdOrNull) {
		this.marketDataSourcePurposeIdOrNull = marketDataSourcePurposeIdOrNull;
	}


	public String getMarketDataSourcePurposeName() {
		return this.marketDataSourcePurposeName;
	}


	public void setMarketDataSourcePurposeName(String marketDataSourcePurposeName) {
		this.marketDataSourcePurposeName = marketDataSourcePurposeName;
	}
}
