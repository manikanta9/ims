package com.clifton.marketdata.datasource.purpose;


import java.util.List;


public interface MarketDataSourcePurposeService {

	/////////////////////////////////////////////////////////////////////////////
	/////////      MarketDataSourcePurposeService Business Methods      /////////
	/////////////////////////////////////////////////////////////////////////////


	public MarketDataSourcePurpose getMarketDataSourcePurpose(short id);


	public MarketDataSourcePurpose getMarketDataSourcePurposeByName(String name);


	public List<MarketDataSourcePurpose> getMarketDataSourcePurposeList(MarketDataSourcePurposeSearchForm searchForm);


	public MarketDataSourcePurpose saveMarketDataSourcePurpose(MarketDataSourcePurpose bean);


	public void deleteMarketDataSourcePurpose(short id);
}
