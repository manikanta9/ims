package com.clifton.marketdata.datasource.purpose;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class MarketDataSourcePurposeServiceImpl implements MarketDataSourcePurposeService {

	private AdvancedUpdatableDAO<MarketDataSourcePurpose, Criteria> marketDataSourcePurposeDAO;


	//////////////////////////////////////////////////////////////////////////// 
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourcePurpose getMarketDataSourcePurpose(short id) {
		return getMarketDataSourcePurposeDAO().findByPrimaryKey(id);
	}


	@Override
	public MarketDataSourcePurpose getMarketDataSourcePurposeByName(String name) {
		return getMarketDataSourcePurposeDAO().findOneByField("name", name);
	}


	@Override
	public List<MarketDataSourcePurpose> getMarketDataSourcePurposeList(MarketDataSourcePurposeSearchForm searchForm) {
		return getMarketDataSourcePurposeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourcePurpose saveMarketDataSourcePurpose(MarketDataSourcePurpose bean) {
		return getMarketDataSourcePurposeDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourcePurpose(short id) {
		getMarketDataSourcePurposeDAO().delete(id);
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataSourcePurpose, Criteria> getMarketDataSourcePurposeDAO() {
		return this.marketDataSourcePurposeDAO;
	}


	public void setMarketDataSourcePurposeDAO(AdvancedUpdatableDAO<MarketDataSourcePurpose, Criteria> marketDataSourcePurposeDAO) {
		this.marketDataSourcePurposeDAO = marketDataSourcePurposeDAO;
	}
}
