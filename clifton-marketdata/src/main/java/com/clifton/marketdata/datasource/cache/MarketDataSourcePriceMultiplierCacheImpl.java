package com.clifton.marketdata.datasource.cache;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.marketdata.datasource.MarketDataSourcePriceMultiplier;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>MarketDataSourcePriceMultiplierCacheImpl</code> class is a cache of data source specific additional price multipliers: MarketDataSourcePriceMultiplier.
 * <p>
 * Cache key is the data source name.  Each value is a Map of investment instrument id to corresponding additional multiplier.
 */
@Component
public class MarketDataSourcePriceMultiplierCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataSourcePriceMultiplier, String, Map<String, MarketDataSourcePriceMultiplier>> implements MarketDataSourcePriceMultiplierCache {


	@Override
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplier(InvestmentInstrument instrument, String dataSourceName, MarketDataSourcePurpose purpose) {
		Map<String, MarketDataSourcePriceMultiplier> cache = getCacheHandler().get(getCacheName(), dataSourceName);
		if (cache != null) {
			return cache.get(getKey(instrument, purpose));
		}
		return null;
	}


	@Override
	public void setMarketDataSourcePriceMultiplier(MarketDataSourcePriceMultiplier bean) {
		Map<String, MarketDataSourcePriceMultiplier> cache = getCacheHandler().get(getCacheName(), bean.getMarketDataSource().getName());
		if (cache == null) {
			cache = new ConcurrentHashMap<>();
			getCacheHandler().put(getCacheName(), bean.getMarketDataSource().getName(), cache);
		}
		cache.put(getKey(bean.getInvestmentInstrument(), bean.getMarketDataSourcePurpose()), bean);
	}


	public String getKey(InvestmentInstrument instrument, MarketDataSourcePurpose purpose) {
		return instrument.getId() + (purpose != null ? ":" + purpose.getId() : "");
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<MarketDataSourcePriceMultiplier> dao, @SuppressWarnings("unused") DaoEventTypes event,
	                                MarketDataSourcePriceMultiplier bean, Throwable e) {
		if (e == null) {
			getCacheHandler().remove(getCacheName(), bean.getMarketDataSource().getName());
		}
	}
}
