package com.clifton.marketdata.datasource;


import com.clifton.business.company.BusinessCompany;
import com.clifton.business.shared.Company;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.marketdata.datasource.search.MarketDataSourcePriceMultiplierSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourcePricingSourceSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourceSearchForm;
import com.clifton.marketdata.datasource.search.MarketDataSourceSecuritySearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>MarketDataSourceService</code> defines the methods for working with data source
 * related entities.
 *
 * @author vgomelsky
 */
public interface MarketDataSourceService {

	////////////////////////////////////////////////////////////////////////////
	//////             Market Data Source Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Recursively look for the data source associated with the specified company or it's closest parent that has a valid data source.
	 *
	 * @throws EntityNotFoundValidationException if that data source for the specified company is not found
	 */
	@DoNotAddRequestMapping
	public String getMarketDataSourceNameStrict(Company company);


	/**
	 * Recursively look for the data source associated with the specified company or it's closest parent that has a valid data source.
	 */
	@ModelAttribute("result")
	public String getMarketDataSourceName(BusinessCompany company);

	@ModelAttribute("data")
	public MarketDataSource getMarketDataSourceByCompany(BusinessCompany company);


	public MarketDataSource getMarketDataSource(short id);


	/**
	 * @throws EntityNotFoundValidationException if that data source with the specified name is not found
	 */
	public MarketDataSource getMarketDataSourceByName(String marketDatasourceName) throws EntityNotFoundValidationException;


	public List<MarketDataSource> getMarketDataSourceList(MarketDataSourceSearchForm searchForm);


	public MarketDataSource saveMarketDataSource(MarketDataSource bean);


	public void deleteMarketDataSource(short id);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a list of companies that Exchange Rate data sources are linked to.
	 */
	@SecureMethod(dtoClass = MarketDataSource.class)
	public List<Company> getMarketDataCompanyForExchangeRatesList();

	////////////////////////////////////////////////////////////////////////////
	//////            Market Data Sector Business Methods              /////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceSector getMarketDataSourceSector(short id);


	public MarketDataSourceSector getMarketDataSourceSectorByNameAndDataSourceName(String marketDataSourceSectorName, String marketDatasourceName);


	public MarketDataSourceSector getMarketDataSourceSectorByNameAndDataSourceId(String marketDataSourceSectorName, Short marketDataSourceId);


	public List<MarketDataSourceSector> getMarketDataSourceSectorListByDataSource(short dataSourceId);


	public MarketDataSourceSector saveMarketDataSourceSector(MarketDataSourceSector bean);


	public void deleteMarketDataSourceSector(short id);


	////////////////////////////////////////////////////////////////////////////
	//////       Market Data Source Security Business Methods          /////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceSecurity getMarketDataSourceSecurity(int id);


	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityListByDataSource(short dataSourceId);



	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityListBySecurity(int securityId);


	public List<MarketDataSourceSecurity> getMarketDataSourceSecurityList(MarketDataSourceSecuritySearchForm searchForm);


	public MarketDataSourceSecurity saveMarketDataSourceSecurity(MarketDataSourceSecurity bean);


	public void deleteMarketDataSourceSecurity(int id);


	/**
	 * Returns data source specific symbol for the specified security symbol and data source.
	 * If not override is defined via MarketDataSourceSecurity, returns the argument securitySymbol.
	 */
	@SecureMethod(dtoClass = MarketDataSourceSecurity.class)
	public MarketDataSourceSecurityHolder getMarketDataSourceSecuritySymbol(String securitySymbol, String dataSourceName, String marketSector);


	////////////////////////////////////////////////////////////////////////////
	//////       Market Data Price Multiplier Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplier(int id);


	@DoNotAddRequestMapping
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplierByCompany(InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose);


	@DoNotAddRequestMapping
	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplierByDataSource(InvestmentInstrument instrument, String dataSourceName, MarketDataSourcePurpose purpose);


	public List<MarketDataSourcePriceMultiplier> getMarketDataSourcePriceMultiplierList(MarketDataSourcePriceMultiplierSearchForm searchForm);


	public MarketDataSourcePriceMultiplier saveMarketDataSourcePriceMultiplier(MarketDataSourcePriceMultiplier bean);


	public void deleteMarketDataSourcePriceMultiplier(int id);


	@DoNotAddRequestMapping
	public BigDecimal adjustIncomingMarketDataSourcePrice(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose);


	@DoNotAddRequestMapping
	public BigDecimal adjustOutgoingMarketDataSourcePrice(BigDecimal price, InvestmentInstrument instrument, BusinessCompany company, MarketDataSourcePurpose purpose);


	//////////////////////////////////////////////////////////////////////////////////
	//////     Market Data Price Source Pricing Source Business Methods      /////////
	//////////////////////////////////////////////////////////////////////////////////


	public MarketDataSourcePricingSource getMarketDataSourcePricingSource(short id);


	public MarketDataSourcePricingSource saveMarketDataSourcePricingSource(MarketDataSourcePricingSource bean);


	public List<MarketDataSourcePricingSource> getMarketDataSourcePricingSourceList(MarketDataSourcePricingSourceSearchForm searchForm);


	public void deleteMarketDataSourcePricingSource(short id);
}
