package com.clifton.marketdata.datasource.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "company.name")
	private String company;

	@SearchField(searchField = "securitySystem.name")
	private String securitySystem;

	@SearchField(searchField = "fxFixingTime.id")
	private Integer fxFixingTimeId;

	@SearchField(searchField = "fxFixingTime.text")
	private String fxFixingTime;

	@SearchField(searchField = "fxFixingTime", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean exchangeRateSupported;

	@SearchField
	private Boolean interestRateSupported;

	@SearchField
	private Boolean marketDataValueSupported;

	@SearchField
	private Boolean defaultDataSource;

	@SearchField
	private Boolean marketSectorSupported;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCompany() {
		return this.company;
	}


	public void setCompany(String company) {
		this.company = company;
	}


	public String getSecuritySystem() {
		return this.securitySystem;
	}


	public void setSecuritySystem(String securitySystem) {
		this.securitySystem = securitySystem;
	}


	public Integer getFxFixingTimeId() {
		return this.fxFixingTimeId;
	}


	public void setFxFixingTimeId(Integer fxFixingTimeId) {
		this.fxFixingTimeId = fxFixingTimeId;
	}


	public String getFxFixingTime() {
		return this.fxFixingTime;
	}


	public void setFxFixingTime(String fxFixingTime) {
		this.fxFixingTime = fxFixingTime;
	}


	public Boolean getExchangeRateSupported() {
		return this.exchangeRateSupported;
	}


	public void setExchangeRateSupported(Boolean exchangeRateSupported) {
		this.exchangeRateSupported = exchangeRateSupported;
	}


	public Boolean getInterestRateSupported() {
		return this.interestRateSupported;
	}


	public void setInterestRateSupported(Boolean interestRateSupported) {
		this.interestRateSupported = interestRateSupported;
	}


	public Boolean getMarketDataValueSupported() {
		return this.marketDataValueSupported;
	}


	public void setMarketDataValueSupported(Boolean marketDataValueSupported) {
		this.marketDataValueSupported = marketDataValueSupported;
	}


	public Boolean getDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(Boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}


	public Boolean getMarketSectorSupported() {
		return this.marketSectorSupported;
	}


	public void setMarketSectorSupported(Boolean marketSectorSupported) {
		this.marketSectorSupported = marketSectorSupported;
	}
}
