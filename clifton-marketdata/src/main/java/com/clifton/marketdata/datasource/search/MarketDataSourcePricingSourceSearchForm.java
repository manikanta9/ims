package com.clifton.marketdata.datasource.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourcePricingSourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSource")
	private String marketDataSourceName;

	@SearchField
	private String symbol;

	@SearchField(searchField = "symbol,name")
	private String searchPattern;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}
}
