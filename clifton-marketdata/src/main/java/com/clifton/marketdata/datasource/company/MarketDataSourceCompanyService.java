package com.clifton.marketdata.datasource.company;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;

import java.util.List;


/**
 * The <code>MarketDataSourceCompanyMappingService</code> defines the methods for working with data market data company mappings.
 *
 * @author vgomelsky
 */
public interface MarketDataSourceCompanyService {

	/////////////////////////////////////////////////////////////////////////////
	/////////      MarketDataSourceCompanyMapping Business Methods      /////////
	/////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceCompanyMapping getMarketDataSourceCompanyMapping(int id);


	public List<MarketDataSourceCompanyMapping> getMarketDataSourceCompanyMappingList(MarketDataSourceCompanyMappingSearchForm searchForm);


	public MarketDataSourceCompanyMapping saveMarketDataSourceCompanyMapping(MarketDataSourceCompanyMapping bean);


	public void deleteMarketDataSourceCompanyMapping(int id);


	@DoNotAddRequestMapping
	public BusinessCompany getMappedCompanyByCode(MarketDataSource marketDataSource, MarketDataSourcePurpose marketDataSourcePurpose, String companyCode);


	@DoNotAddRequestMapping
	public BusinessCompany getMappedCompanyByCodeStrict(MarketDataSource marketDataSource, MarketDataSourcePurpose marketDataSourcePurpose, String companyCode);
}
