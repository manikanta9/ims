package com.clifton.marketdata.datasource.exchange;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;


/**
 * The <code>MarketDataSourceExchange</code> class represents data source specific exchange mappings.
 * When receiving data feeds from various data sources (intraday trade files, electronic trading via FIX, etc.), each data source may
 * use slightly different codes to reference exchanges.
 *
 * @author mwacker
 */
public class MarketDataSourceExchangeMapping extends BaseEntity<Short> {

	private MarketDataSource marketDataSource;
	private MarketDataSourcePurpose marketDataSourcePurpose;
	private InvestmentExchange exchange;
	private String exchangeCode;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public InvestmentExchange getExchange() {
		return this.exchange;
	}


	public void setExchange(InvestmentExchange exchange) {
		this.exchange = exchange;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public MarketDataSourcePurpose getMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}


	public void setMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		this.marketDataSourcePurpose = marketDataSourcePurpose;
	}
}
