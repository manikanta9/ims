package com.clifton.marketdata.datasource.company;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourceCompanyMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSource")
	private String marketDataSourceName;


	@SearchField(searchField = "marketDataSourcePurpose.id")
	private Short marketDataSourcePurposeId;

	@SearchField(searchField = "marketDataSourcePurpose.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean marketDataSourcePurposeIsNull;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSourcePurpose")
	private String marketDataSourcePurposeName;


	@SearchField(searchField = "businessCompany.id")
	private Integer businessCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "businessCompany")
	private String businessCompanyName;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String externalCompanyName;

	@SearchField(searchField = "externalCompanyName", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean externalCompanyNameIsNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}


	public Integer getBusinessCompanyId() {
		return this.businessCompanyId;
	}


	public void setBusinessCompanyId(Integer businessCompanyId) {
		this.businessCompanyId = businessCompanyId;
	}


	public String getBusinessCompanyName() {
		return this.businessCompanyName;
	}


	public void setBusinessCompanyName(String businessCompanyName) {
		this.businessCompanyName = businessCompanyName;
	}


	public String getExternalCompanyName() {
		return this.externalCompanyName;
	}


	public void setExternalCompanyName(String externalCompanyName) {
		this.externalCompanyName = externalCompanyName;
	}


	public Short getMarketDataSourcePurposeId() {
		return this.marketDataSourcePurposeId;
	}


	public void setMarketDataSourcePurposeId(Short marketDataSourcePurposeId) {
		this.marketDataSourcePurposeId = marketDataSourcePurposeId;
	}


	public Boolean getMarketDataSourcePurposeIsNull() {
		return this.marketDataSourcePurposeIsNull;
	}


	public void setMarketDataSourcePurposeIsNull(Boolean marketDataSourcePurposeIsNull) {
		this.marketDataSourcePurposeIsNull = marketDataSourcePurposeIsNull;
	}


	public String getMarketDataSourcePurposeName() {
		return this.marketDataSourcePurposeName;
	}


	public void setMarketDataSourcePurposeName(String marketDataSourcePurposeName) {
		this.marketDataSourcePurposeName = marketDataSourcePurposeName;
	}


	public Boolean getExternalCompanyNameIsNull() {
		return this.externalCompanyNameIsNull;
	}


	public void setExternalCompanyNameIsNull(Boolean externalCompanyNameIsNull) {
		this.externalCompanyNameIsNull = externalCompanyNameIsNull;
	}
}
