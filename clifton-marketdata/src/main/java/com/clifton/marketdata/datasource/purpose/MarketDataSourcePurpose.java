package com.clifton.marketdata.datasource.purpose;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>MarketDataSourcePurpose</code> defines a purpose for used for mapping objects like InvestmentExchange
 * to data sources.
 *
 * @author mwacker
 */
//
public class MarketDataSourcePurpose extends NamedEntity<Short> {

	//
	public static final String MARKET_DATA_SOURCE_PURPOSE_NAME_TRADE_IMPORT = "Integration - Trade Import";
	public static final String MARKET_DATA_SOURCE_PURPOSE_NAME_INTRADAY = "Integration - Intraday Trades";
	public static final String MARKET_DATA_SOURCE_PURPOSE_NAME_POSITIONS = "Integration - Positions";
	public static final String MARKET_DATA_SOURCE_PURPOSE_NAME_ACTIVITY = "Integration - Activity";
	public static final String MARKET_DATA_SOURCE_PURPOSE_NAME_FIX = "FIX Messaging";

	/**
	 * This optional system modify condition that can be configured to provide additional restrictions for non-admin users when updating a MarketDataSourceCompanyMapping entity associated with this MarketDataSourcePurpose.
	 */
	private SystemCondition companyMappingModifyCondition;

	/**
	 * This optional system modify condition that can be configured to provide additional restrictions for non-admin users when updating a MarketDataSourcePriceMultiplier entity associated with this MarketDataSourcePurpose.
	 */
	private SystemCondition priceMultiplierModifyCondition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getCompanyMappingModifyCondition() {
		return this.companyMappingModifyCondition;
	}


	public void setCompanyMappingModifyCondition(SystemCondition companyMappingModifyCondition) {
		this.companyMappingModifyCondition = companyMappingModifyCondition;
	}


	public SystemCondition getPriceMultiplierModifyCondition() {
		return this.priceMultiplierModifyCondition;
	}


	public void setPriceMultiplierModifyCondition(SystemCondition priceMultiplierModifyCondition) {
		this.priceMultiplierModifyCondition = priceMultiplierModifyCondition;
	}
}
