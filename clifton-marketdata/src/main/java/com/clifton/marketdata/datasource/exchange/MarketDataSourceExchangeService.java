package com.clifton.marketdata.datasource.exchange;


import java.util.List;


public interface MarketDataSourceExchangeService {

	/////////////////////////////////////////////////////////////////////////////
	/////////      MarketDataSourceExchangeMapping Business Methods     /////////
	/////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceExchangeMapping getMarketDataSourceExchangeMapping(short id);


	public List<MarketDataSourceExchangeMapping> getMarketDataSourceExchangeMappingList(MarketDataSourceExchangeMappingSearchForm searchForm);


	public MarketDataSourceExchangeMapping saveMarketDataSourceExchangeMapping(MarketDataSourceExchangeMapping bean);


	public void deleteMarketDataSourceExchangeMapping(short id);
}
