package com.clifton.marketdata.datasource.cache;

import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.marketdata.datasource.MarketDataSourcePriceMultiplier;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;


/**
 * The <code>MarketDataSourcePriceMultiplierCache</code> class is a cache of data source specific additional price multipliers: MarketDataSourcePriceMultiplier.
 * <p>
 * Cache key is the data source name.  Each value is a Map of investment instrument id to corresponding additional multiplier.
 */
public interface MarketDataSourcePriceMultiplierCache {


	public MarketDataSourcePriceMultiplier getMarketDataSourcePriceMultiplier(InvestmentInstrument instrument, String dataSourceName, MarketDataSourcePurpose purpose);


	public void setMarketDataSourcePriceMultiplier(MarketDataSourcePriceMultiplier bean);
}
