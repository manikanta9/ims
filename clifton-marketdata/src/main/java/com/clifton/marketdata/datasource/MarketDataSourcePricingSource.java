package com.clifton.marketdata.datasource;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>MarketDataSourcePricingSource</code> stores the pricing source for a given MarketDataSource.
 * <p>
 * For example when pricing from Bloomberg, we might need to use the Bloomberg Valuation Service (the code is BVAL) to do that the
 * symbol that we send must be "CUSIP@BVAL Sector" and example is "JP1120071632@BVAL Government".
 *
 * @author mwacker
 */
public class MarketDataSourcePricingSource extends NamedEntity<Short> {

	private MarketDataSource marketDataSource;

	private String symbol;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return this.getName() + " (" + this.getSymbol() + ")";
	}


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
