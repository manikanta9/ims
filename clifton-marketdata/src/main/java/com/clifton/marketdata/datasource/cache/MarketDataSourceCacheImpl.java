package com.clifton.marketdata.datasource.cache;


import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.marketdata.datasource.MarketDataSource;
import org.springframework.stereotype.Component;


/**
 * The <code>MarketDataSourceCacheImpl</code> class provides caching and retrieval from cache of
 * MarketDataSource objects by name.
 *
 * @author vgomelsky
 */
@Component
public class MarketDataSourceCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataSource, String, ObjectWrapper<MarketDataSource>> implements MarketDataSourceCache {


	@Override
	public ObjectWrapper<MarketDataSource> getMarketDataSource(String key) {
		return getCacheHandler().get(getCacheName(), key);
	}


	@Override
	public void setMarketDataSource(MarketDataSource bean, String key) {
		getCacheHandler().put(getCacheName(), key, new ObjectWrapper<>(bean));
	}
}
