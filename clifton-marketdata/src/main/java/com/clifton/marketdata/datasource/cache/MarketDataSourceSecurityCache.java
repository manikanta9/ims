package com.clifton.marketdata.datasource.cache;

import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;

import java.util.Map;


/**
 * The <code>MarketDataSourceSecurityCache</code> class provides caching and retrieval from cache of
 * default (ours) to data source specific security symbol mappings by data source name.
 * <p>
 * Cache is keyed by data source name.  Cache values are a map of symbol overrides: our symbol to data source specific symbol.
 *
 * @author vgomelsky
 */
public interface MarketDataSourceSecurityCache {

	/**
	 * Returns a Map of security symbol mappings from our symbol to data source specific symbol.
	 * If a mapping doesn't exist then we're using the same symbol convention.
	 */
	public Map<String, MarketDataSourceSecurityHolder> getDataSourceCache(String dataSourceName);


	public void setDataSourceCache(String dataSourceName, Map<String, MarketDataSourceSecurityHolder> symbolMapping);
}
