package com.clifton.marketdata.datasource;


import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;

import java.math.BigDecimal;


/**
 * The <code>MarketDataSourcePriceMultiplier</code> class allows overriding of industry standard price multipliers.
 * Some market data sources may use non-industry-standard price multipliers. Our system will apply the following
 * additional multipliers to their prices sent to us.
 * <p>
 * For example, for "AD" futures, Goldman Sachs uses 10 multiplier instead of 1,000 defined by the exchange.
 * In this case, the mapping will have 100 as additional multiplier: 10 * 100 = 1,000.
 *
 * @author mwacker
 */
public class MarketDataSourcePriceMultiplier extends BaseEntity<Integer> implements SystemEntityModifyConditionAware {

	private MarketDataSource marketDataSource;
	private MarketDataSourcePurpose marketDataSourcePurpose;
	private InvestmentInstrument investmentInstrument;
	private BigDecimal additionalPriceMultiplier;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SystemCondition getEntityModifyCondition() {
		return getMarketDataSourcePurpose() != null ? getMarketDataSourcePurpose().getPriceMultiplierModifyCondition() : null;
	}



	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}


	public void setMarketDataSource(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}


	public InvestmentInstrument getInvestmentInstrument() {
		return this.investmentInstrument;
	}


	public void setInvestmentInstrument(InvestmentInstrument investmentInstrument) {
		this.investmentInstrument = investmentInstrument;
	}


	public BigDecimal getAdditionalPriceMultiplier() {
		return this.additionalPriceMultiplier;
	}


	public void setAdditionalPriceMultiplier(BigDecimal additionalPriceMultiplier) {
		this.additionalPriceMultiplier = additionalPriceMultiplier;
	}


	public MarketDataSourcePurpose getMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}


	public void setMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		this.marketDataSourcePurpose = marketDataSourcePurpose;
	}
}
