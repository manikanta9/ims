package com.clifton.marketdata.datasource.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import org.springframework.stereotype.Component;


/**
 * @author AbhinayaM
 */
@Component
public class MarketDataSourceSecurityListBySecurityCache extends SelfRegisteringSingleKeyDaoListCache<MarketDataSourceSecurity, Integer> {

	@Override
	protected String getBeanKeyProperty() {
		return "security.id";
	}


	@Override
	protected Integer getBeanKeyValue(MarketDataSourceSecurity bean) {
		if (bean.getSecurity() != null) {
			return bean.getSecurity().getId();
		}
		return null;
	}
}
