package com.clifton.marketdata.datasource;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>MarketDataSourceSector</code> class represents a Market Sector DTO: Equity, Currency, Government Bonds, etc.
 * Each data source has unique set market sectors.
 *
 * @author vgomelsky
 */
public class MarketDataSourceSector extends NamedEntity<Short> {

	private MarketDataSource dataSource;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceSector() {
		super();
	}


	public MarketDataSourceSector(MarketDataSource dataSource, String sectorName) {
		super();
		setDataSource(dataSource);
		setName(sectorName);
	}


	public MarketDataSource getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(MarketDataSource dataSource) {
		this.dataSource = dataSource;
	}
}
