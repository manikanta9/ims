package com.clifton.marketdata.datasource.company;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>MarketDataSourceCompanyServiceImpl</code> class provides basic implementation of the MarketDataSourceCompanyService interface.
 *
 * @author mwacker
 */
@Service
public class MarketDataSourceCompanyServiceImpl implements MarketDataSourceCompanyService {

	private AdvancedUpdatableDAO<MarketDataSourceCompanyMapping, Criteria> marketDataSourceCompanyMappingDAO;


	/////////////////////////////////////////////////////////////////////////////
	/////////      MarketDataSourceCompanyMapping Business Methods      /////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataSourceCompanyMapping getMarketDataSourceCompanyMapping(int id) {
		return getMarketDataSourceCompanyMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<MarketDataSourceCompanyMapping> getMarketDataSourceCompanyMappingList(MarketDataSourceCompanyMappingSearchForm searchForm) {
		return getMarketDataSourceCompanyMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public MarketDataSourceCompanyMapping saveMarketDataSourceCompanyMapping(MarketDataSourceCompanyMapping bean) {
		return getMarketDataSourceCompanyMappingDAO().save(bean);
	}


	@Override
	public void deleteMarketDataSourceCompanyMapping(int id) {
		getMarketDataSourceCompanyMappingDAO().delete(id);
	}


	@Override
	public BusinessCompany getMappedCompanyByCode(MarketDataSource marketDataSource, MarketDataSourcePurpose marketDataSourcePurpose, String companyCode) {
		return getMappedCompanyByCode(marketDataSource, marketDataSourcePurpose, companyCode, false);
	}


	@Override
	public BusinessCompany getMappedCompanyByCodeStrict(MarketDataSource marketDataSource, MarketDataSourcePurpose marketDataSourcePurpose, String companyCode) {
		return getMappedCompanyByCode(marketDataSource, marketDataSourcePurpose, companyCode, true);
	}


	private BusinessCompany getMappedCompanyByCode(MarketDataSource marketDataSource, MarketDataSourcePurpose marketDataSourcePurpose, String companyCode, boolean exceptionIfMoreThanOneOrNone) {
		ValidationUtils.assertNotNull(marketDataSource, "Market source is required.", "marketDataSource");
		ValidationUtils.assertNotEmpty(companyCode, "Company code is required.", "brokerCode");

		MarketDataSourceCompanyMappingSearchForm searchForm = new MarketDataSourceCompanyMappingSearchForm();
		searchForm.setMarketDataSourceId(marketDataSource.getId());
		searchForm.setMarketDataSourcePurposeId(marketDataSourcePurpose.getId());
		searchForm.setExternalCompanyName(companyCode);
		List<MarketDataSourceCompanyMapping> mappingList = getMarketDataSourceCompanyMappingList(searchForm);
		if (exceptionIfMoreThanOneOrNone) {
			ValidationUtils.assertTrue(mappingList.size() == 1, "Could not find executing broker with code [" + companyCode + "] for data source [" + marketDataSource.getName() + "] and data source purpose [" + marketDataSourcePurpose.getName() + "].");
		}
		return !mappingList.isEmpty() ? mappingList.get(0).getBusinessCompany() : null;
	}
	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<MarketDataSourceCompanyMapping, Criteria> getMarketDataSourceCompanyMappingDAO() {
		return this.marketDataSourceCompanyMappingDAO;
	}


	public void setMarketDataSourceCompanyMappingDAO(AdvancedUpdatableDAO<MarketDataSourceCompanyMapping, Criteria> marketDataSourceCompanyMappingDAO) {
		this.marketDataSourceCompanyMappingDAO = marketDataSourceCompanyMappingDAO;
	}
}
