package com.clifton.marketdata.datasource.exchange;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class MarketDataSourceExchangeMappingSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "marketDataSource.id")
	private Short marketDataSourceId;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSource")
	private String marketDataSourceName;


	@SearchField(searchField = "marketDataSourcePurpose.id")
	private Short marketDataSourcePurposeId;

	@SearchField(searchField = "marketDataSourcePurpose.id", comparisonConditions = ComparisonConditions.IS_NULL)
	private Boolean marketDataSourcePurposeIsNull;

	@SearchField(searchField = "name", searchFieldPath = "marketDataSourcePurpose")
	private String marketDataSourcePurposeName;


	@SearchField(searchField = "exchange.id")
	private Short exchangeId;

	@SearchField(searchField = "name", searchFieldPath = "exchange")
	private String exchangeName;

	@SearchField
	private String exchangeCode;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getMarketDataSourceId() {
		return this.marketDataSourceId;
	}


	public void setMarketDataSourceId(Short marketDataSourceId) {
		this.marketDataSourceId = marketDataSourceId;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}


	public Short getExchangeId() {
		return this.exchangeId;
	}


	public void setExchangeId(Short exchangeId) {
		this.exchangeId = exchangeId;
	}


	public String getExchangeName() {
		return this.exchangeName;
	}


	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public Short getMarketDataSourcePurposeId() {
		return this.marketDataSourcePurposeId;
	}


	public void setMarketDataSourcePurposeId(Short marketDataSourcePurposeId) {
		this.marketDataSourcePurposeId = marketDataSourcePurposeId;
	}


	public Boolean getMarketDataSourcePurposeIsNull() {
		return this.marketDataSourcePurposeIsNull;
	}


	public void setMarketDataSourcePurposeIsNull(Boolean marketDataSourcePurposeIsNull) {
		this.marketDataSourcePurposeIsNull = marketDataSourcePurposeIsNull;
	}


	public String getMarketDataSourcePurposeName() {
		return this.marketDataSourcePurposeName;
	}


	public void setMarketDataSourcePurposeName(String marketDataSourcePurposeName) {
		this.marketDataSourcePurposeName = marketDataSourcePurposeName;
	}
}
