package com.clifton.marketdata.datasource;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntity;
import com.clifton.security.system.SecuritySystem;
import com.clifton.system.list.SystemListItem;


/**
 * The <code>MarketDataSource</code> class represents sources of market data: Bloomberg, Goldman, Citigroup, etc.
 * Data sources can be used for different purposes: a source of market data, a source of exchange rates,
 * vendor specific mappings (additional price multipliers, FIX company mappings)
 *
 * @author vgomelsky
 */
public class MarketDataSource extends NamedEntity<Short> {

	public static final String MARKET_DATA_SOURCE_NAME_PARAMETRIC_CLIFTON = "Parametric Clifton";

	///////////////////////////////////////////////////////////////////////////

	private BusinessCompany company;

	/**
	 * Optional SecuritySystem linked to this MarketDataSource.
	 * Example: Bloomberg MarketDataSource is linked to Bloomberg SecuritySystem to look up Bloomberg user properties.
	 */
	private SecuritySystem securitySystem;

	/**
	 * If defined, this Data Source is available as a source for Exchange Rates (validation and filtering).
	 * Specifies the time when exchange rates are recorded: usually "London Close" or "US Close".
	 */
	private SystemListItem fxFixingTime;

	/**
	 * Set to true if this data source supports {@link com.clifton.marketdata.rates.MarketDataInterestRate} objects.
	 * This parameter is used for validation and filtering.
	 */
	private boolean interestRateSupported;

	/**
	 * Set to true if this data source supports {@link com.clifton.marketdata.field.MarketDataValue} objects.
	 * This parameter is used for validation and filtering.
	 */
	private boolean marketDataValueSupported;

	/**
	 * Specifies whether this data source supports market sectors (Bloomberg does).
	 */
	private boolean marketSectorSupported;

	private boolean defaultDataSource;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSource() {
		super();
	}


	public MarketDataSource(String name) {
		this();
		setName(name);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExchangeRateSupported() {
		return getFxFixingTime() != null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BusinessCompany getCompany() {
		return this.company;
	}


	public void setCompany(BusinessCompany company) {
		this.company = company;
	}


	public SecuritySystem getSecuritySystem() {
		return this.securitySystem;
	}


	public void setSecuritySystem(SecuritySystem securitySystem) {
		this.securitySystem = securitySystem;
	}


	public SystemListItem getFxFixingTime() {
		return this.fxFixingTime;
	}


	public void setFxFixingTime(SystemListItem fxFixingTime) {
		this.fxFixingTime = fxFixingTime;
	}


	public boolean isInterestRateSupported() {
		return this.interestRateSupported;
	}


	public void setInterestRateSupported(boolean interestRateSupported) {
		this.interestRateSupported = interestRateSupported;
	}


	public boolean isMarketDataValueSupported() {
		return this.marketDataValueSupported;
	}


	public void setMarketDataValueSupported(boolean marketDataValueSupported) {
		this.marketDataValueSupported = marketDataValueSupported;
	}


	public boolean isMarketSectorSupported() {
		return this.marketSectorSupported;
	}


	public void setMarketSectorSupported(boolean marketSectorSupported) {
		this.marketSectorSupported = marketSectorSupported;
	}


	public boolean isDefaultDataSource() {
		return this.defaultDataSource;
	}


	public void setDefaultDataSource(boolean defaultDataSource) {
		this.defaultDataSource = defaultDataSource;
	}
}
