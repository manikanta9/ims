package com.clifton.marketdata.datasource.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceSecurityHolder;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The <code>MarketDataSourceSecurityCacheImpl</code> class provides caching and retrieval from cache of
 * default (ours) to data source specific security symbol mappings by data source name.
 * <p>
 * Cache is keyed by data source name.  Cache values are a map of symbol overrides: our symbol to data source specific symbol.
 *
 * @author vgomelsky
 */
@Component
public class MarketDataSourceSecurityCacheImpl extends SelfRegisteringSimpleDaoCache<MarketDataSourceSecurity, String, Map<String, MarketDataSourceSecurityHolder>> implements MarketDataSourceSecurityCache {


	@Override
	public Map<String, MarketDataSourceSecurityHolder> getDataSourceCache(String dataSourceName) {
		return getCacheHandler().get(getCacheName(), dataSourceName);
	}


	@Override
	public void setDataSourceCache(String dataSourceName, Map<String, MarketDataSourceSecurityHolder> symbolMapping) {
		getCacheHandler().put(getCacheName(), dataSourceName, symbolMapping);
	}
}
