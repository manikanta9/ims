package com.clifton.marketdata.datasource.search;

import com.clifton.business.company.search.CompanySearchForm;
import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.BooleanUtils;
import com.clifton.marketdata.datasource.MarketDataSource;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Component;


/**
 * The MarketDataBusinessCompanySearchFormRestrictionConfigurer class allows market data project specific search restriction
 * to be added to the {@BusinessCompany} that resides in a lower dependency project.
 *
 * @author vgomelsky
 */
@Component
public class MarketDataBusinessCompanySearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {

	@Override
	public Class<?> getSearchFormClass() {
		return CompanySearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "limitToMarketDataSourceCompanies";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (BooleanUtils.isTrue(restriction.getValue())) {
			// limit to companies that have a DataSource referencing them
			DetachedCriteria exists = DetachedCriteria.forClass(MarketDataSource.class, "ds");
			exists.setProjection(Projections.property("id"));
			exists.add(Restrictions.eqProperty("ds.company.id", criteria.getAlias() + ".id"));
			criteria.add(Subqueries.exists(exists));
		}
	}
}
