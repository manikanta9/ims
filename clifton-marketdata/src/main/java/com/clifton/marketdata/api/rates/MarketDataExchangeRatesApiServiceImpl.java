package com.clifton.marketdata.api.rates;

import com.clifton.business.shared.Company;
import com.clifton.core.validation.EntityNotFoundValidationException;
import com.clifton.investment.api.account.InvestmentAccountApiService;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
@Component
public class MarketDataExchangeRatesApiServiceImpl implements MarketDataExchangeRatesApiService {

	private InvestmentAccountApiService investmentAccountApiService;
	private MarketDataRatesRetriever marketDataRatesRetriever;
	private MarketDataSourceService marketDataSourceService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getExchangeRateDataSourceForClientAccount(int clientAccountId) {
		ClientAccount clientAccount = getInvestmentAccountApiService().getClientAccount(clientAccountId);
		return getExchangeRateDataSourceForClientAccount(clientAccount);
	}


	@Override
	public String getExchangeRateDataSourceForClientAccount(ClientAccount clientAccount) {
		String result = MarketDataRatesRetriever.MARKET_DATASOURCE_EXCHANGE_RATES;
		if (clientAccount != null && clientAccount.getDefaultExchangeRateSourceCompany() != null) {
			return getMarketDataSourceService().getMarketDataSourceNameStrict(clientAccount.getDefaultExchangeRateSourceCompany());
		}
		return result;
	}


	@Override
	public String getExchangeRateDataSourceForHoldingAccount(int holdingAccountId, boolean exceptionIfNotFound) {
		HoldingAccount holdingAccount = getInvestmentAccountApiService().getHoldingAccount(holdingAccountId);
		return getExchangeRateDataSourceForHoldingAccount(holdingAccount, exceptionIfNotFound);
	}


	@Override
	public String getExchangeRateDataSourceForHoldingAccount(HoldingAccount holdingAccount, boolean exceptionIfNotFound) {
		Company fxSourceCompany = null;
		if (holdingAccount != null) {
			// try explicit override first and then the issuer
			fxSourceCompany = holdingAccount.getDefaultExchangeRateSourceCompany();
			if (fxSourceCompany == null) {
				fxSourceCompany = holdingAccount.getIssuer();
			}
			else {
				// when explicit FX Source override is defined, always throw an exception if it's not valid
				exceptionIfNotFound = true;
			}
		}
		return getExchangeRateDataSourceForCompany(fxSourceCompany, exceptionIfNotFound);
	}


	@Override
	public String getExchangeRateDataSourceForCompany(Company fxSourceCompany, boolean exceptionIfNotFound) {
		String result = MarketDataRatesRetriever.MARKET_DATASOURCE_EXCHANGE_RATES;
		if (fxSourceCompany != null) {
			try {
				// validate that the data source actually exists
				result = getMarketDataSourceService().getMarketDataSourceNameStrict(fxSourceCompany);
			}
			catch (EntityNotFoundValidationException e) {
				if (exceptionIfNotFound) {
					throw e;
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getExchangeRate(FxRateLookupCommand command) {
		String dataSourceName = getDataSourceName(command);

		if (command.isFlexibleLookup()) {
			if (command.isDominantCurrency()) {
				return getMarketDataRatesRetriever().getDominantCurrencyExchangeRateForDataSourceFlexible(command.getFromCurrency(), command.getToCurrency(), dataSourceName, command.getLookupDate(), command.isFallbackToDefaultDataSource(), command.isExceptionIfRateIsMissing());
			}
			return getMarketDataRatesRetriever().getExchangeRateForDataSourceFlexible(command.getFromCurrency(), command.getToCurrency(), dataSourceName, command.getLookupDate(), command.isFallbackToDefaultDataSource(), command.isExceptionIfRateIsMissing());
		}
		if (command.isDominantCurrency()) {
			throw new IllegalArgumentException("Non flexible Exchange Rate lookup is not supported for Dominant Currency lookup.");
		}
		return getMarketDataRatesRetriever().getExchangeRateForDataSource(command.getFromCurrency(), command.getToCurrency(), dataSourceName, command.getLookupDate(), command.isFallbackToDefaultDataSource(), command.isExceptionIfRateIsMissing());
	}


	private String getDataSourceName(FxRateLookupCommand command) {
		String dataSourceName = command.getDataSourceName();
		if (dataSourceName == null) {
			Company fxSourceCompany = command.getFxSourceCompany();
			if (fxSourceCompany != null) {
				dataSourceName = getExchangeRateDataSourceForCompany(fxSourceCompany, !command.isFallbackToDefaultDataSource());
			}
			if (dataSourceName == null) {
				ClientAccount clientAccount = command.getClientAccount();
				if (clientAccount == null && command.getClientAccountId() != null) {
					clientAccount = getInvestmentAccountApiService().getClientAccount(command.getClientAccountId());
				}
				if (clientAccount != null) {
					dataSourceName = getExchangeRateDataSourceForClientAccount(clientAccount);
				}
				else {
					HoldingAccount holdingAccount = command.getHoldingAccount();
					if (holdingAccount == null && command.getHoldingAccountId() != null) {
						holdingAccount = getInvestmentAccountApiService().getHoldingAccount(command.getHoldingAccountId());
					}
					dataSourceName = getExchangeRateDataSourceForHoldingAccount(holdingAccount, !command.isFallbackToDefaultDataSource());
				}
			}
		}
		return dataSourceName;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods                ////////////
	////////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountApiService getInvestmentAccountApiService() {
		return this.investmentAccountApiService;
	}


	public void setInvestmentAccountApiService(InvestmentAccountApiService investmentAccountApiService) {
		this.investmentAccountApiService = investmentAccountApiService;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
