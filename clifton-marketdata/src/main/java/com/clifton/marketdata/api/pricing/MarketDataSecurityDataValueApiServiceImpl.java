package com.clifton.marketdata.api.pricing;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The implementation of the {@link MarketDataSecurityDataValueApiService}
 * This service is intended for the lookup of stored or live market data values, and supports field data lookup for securities that have an overridden investment exchange specified.
 * The service will first attempt to look up data from locally stored market data, and if not found, will attempt to look up historical or live data from a live provider service such as Bloomberg.
 *
 * @author davidi
 */
@Service
public class MarketDataSecurityDataValueApiServiceImpl implements MarketDataSecurityDataValueApiService {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentCalculator investmentCalculator;
	private InvestmentExchangeService investmentExchangeService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataFieldMappingService marketDataFieldMappingService;
	private MarketDataLiveService marketDataLiveService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataSourceService marketDataSourceService;

	////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////
	////                    Field Value Lookup Methods                      ////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SecureMethod(dtoClass = MarketDataValue.class) // Added here to avoid adding core to the API source set
	public MarketDataSecurityDataValue getMarketDataSecurityDataValue(MarketDataSecurityDataValueCommand command) {
		ValidationUtils.assertNotNull(command, "The command parameter cannot be null");
		validateMarketDataSecurityDataValueCommand(command);
		return doGetSecurityDataWithCommand(command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Method to derive market data.  It processes live requests first if the Command.isLiveValue is set.  If not, it looks first in
	 * locally stored market data for a price.  If not found, it looks the historical or current price up via a provider based on
	 * the market data source.
	 *
	 * @param command - command that contains lookup configuration.
	 */
	private MarketDataSecurityDataValue doGetSecurityDataWithCommand(MarketDataSecurityDataValueCommand command) {
		final InvestmentSecurity investmentSecurity = getInvestmentInstrumentService().getInvestmentSecurity(command.getSecurityId());
		ValidationUtils.assertNotNull(investmentSecurity, "Investment security for not found for ID:" + command.getSecurityId());

		InvestmentExchange investmentExchange = null;
		if (command.getExchangeName() != null) {
			investmentExchange = getInvestmentExchangeService().getInvestmentExchangeByName(command.getExchangeName());
			ValidationUtils.assertNotNull(investmentExchange, "Investment Exchange: \"" + command.getExchangeName() + "\" was not found in the IMS Database.");
		}

		StringBuilder errorDetailText = new StringBuilder();
		List<String> errorList = new ArrayList<>();
		final Date today = DateUtils.clearTime(new Date());
		final Date requestedDate = command.getRequestedDate() != null ? DateUtils.clearTime(command.getRequestedDate()) : null;

		try {
			// Live request
			if (command.isLiveValue() || command.getRequestedDate() == null) {
				MarketDataSecurityDataValue marketDataSecurityDataValue = getMarketDataLive(command, investmentExchange);
				if (marketDataSecurityDataValue != null) {
					return marketDataSecurityDataValue;
				}
			}
			else {
				// derive a lookup date which may be offset depending on whether or not the date is on a business holiday.
				Date lookupDate = requestedDate != null ? requestedDate : today;
				final Calendar investmentCalendar = getInvestmentCalendar(investmentSecurity, investmentExchange);
				if (investmentCalendar != null) {
					final CalendarBusinessDayCommand calendarBusinessDayCommand = new CalendarBusinessDayCommand();
					calendarBusinessDayCommand.setDate(lookupDate);
					calendarBusinessDayCommand.setCalendarIds(new Short[]{investmentCalendar.getId()});
					lookupDate = DateUtils.clearTime(getBusinessDayOrPrevious(lookupDate, investmentCalendar));
				}

				// Lookup current or historical data from locally stored market data.  Depending on lookup time, the local market data may contain data for the current date.
				MarketDataValueHolder valueHolder = getMarketDataLocalFieldValue(command, lookupDate, investmentSecurity, investmentExchange);
				if (valueHolder != null && valueHolder.getMeasureValue() != null) {
					// note ValueHolder always carries a value of BigDecimal for measureValue, so our DataTypeName will be DECIMAL as well.
					return getMarketDataSecurityDataValue(command, investmentExchange, DataTypeNames.DECIMAL, valueHolder.getMeasureDate(), valueHolder.getMeasureValueAdjusted());
				}

				// Lookup historical data from provider
				if (DateUtils.isDateAfter(today, lookupDate)) {
					final DataFieldValueCommand historicalDataFieldValueCommand = getDataFieldValueCommand(command, lookupDate, investmentSecurity, investmentExchange);
					MarketDataSecurityDataValue marketDataSecurityDataValue = getHistoricalRequest(command, historicalDataFieldValueCommand, getMarketDataProvider(command, investmentSecurity), investmentExchange, errorList);
					if (marketDataSecurityDataValue != null) {
						return marketDataSecurityDataValue;
					}
				}

				// try live price if the data for the current date (or after) and was not found in local data or provider historical data.
				if (DateUtils.isDateAfterOrEqual(lookupDate, today)) {
					MarketDataSecurityDataValue marketDataSecurityDataValue = getMarketDataLive(command, investmentExchange);
					if (marketDataSecurityDataValue != null) {
						return marketDataSecurityDataValue;
					}
				}
			}
		}
		catch (Exception e) {
			errorDetailText.append(e.getMessage());
		}

		// Handle case where data was not found for specified criteria.
		String fieldName = command.getMarketDataFieldName();
		if (StringUtils.isEmpty(fieldName)) {
			MarketDataField marketDataField = getDefaultPriceField(investmentSecurity);
			if (marketDataField != null) {
				fieldName = marketDataField.getName();
			}
		}

		StringBuilder errorMessage = new StringBuilder();
		if (!StringUtils.isEmpty(command.getExceptionPrefix())) {
			errorMessage.append(command.getExceptionPrefix());
			errorMessage.append(" ");
		}
		errorMessage.append("No market data results were found for field \"");
		errorMessage.append(fieldName != null ? fieldName : "unspecified field");
		errorMessage.append("\" of investment security: ");
		errorMessage.append(investmentSecurity.getLabel());
		errorMessage.append(".");

		if (!CollectionUtils.isEmpty(errorList)) {
			if (!StringUtils.isEmpty(errorDetailText)) {
				errorDetailText.append("; ");
			}
			errorDetailText.append(StringUtils.join(errorList, "; "));
		}

		if (!StringUtils.isEmpty(errorDetailText)) {
			errorMessage.append(" Errors: ");
			errorMessage.append(errorDetailText);
			errorMessage.append(".");
		}
		LogUtils.error(getClass(), errorMessage.toString());
		if (command.isExceptionOnMissingData()) {
			throw new ValidationException(errorMessage.toString());
		}

		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calls the MarketDataLive service to get the latest price for a security.
	 *
	 * @param command            required, and must minimally specify an InvestmentSecurity ID. Other fields are optional.
	 * @param investmentExchange if the command specifies an exchange an exchange instance must be passed in to avoid repeated DB lookups.
	 * @return MarketDataSecurityDataValue - container with field / price results.  May be null if data is not found.
	 */
	private MarketDataSecurityDataValue getMarketDataLive(MarketDataSecurityDataValueCommand command, InvestmentExchange investmentExchange) {

		MarketDataLive marketDataLive = null;

		MarketDataLiveCommand marketDataLiveCommand = getMarketDataLiveCommand(command, investmentExchange);
		if (StringUtils.isEmpty(command.getMarketDataFieldName())) {
			marketDataLive = getMarketDataLiveService().getMarketDataLivePrice(command.getSecurityId(), marketDataLiveCommand);
		}
		else {
			List<MarketDataLive> marketDataLiveList = getMarketDataLiveService().getMarketDataLiveFieldValuesWithCommand(marketDataLiveCommand);
			if (!CollectionUtils.isEmpty(marketDataLiveList)) {
				marketDataLive = CollectionUtils.getOnlyElementStrict(marketDataLiveList);
			}
		}

		if (marketDataLive != null && marketDataLive.getValue() != null) {
			return getMarketDataSecurityDataValue(command, investmentExchange, marketDataLive.getDataTypeName(), marketDataLive.getMeasureDate(), marketDataLive.getValue());
		}

		return null;
	}


	/**
	 * Performs a local DB lookup on local market data.  MarketDataFieldService is used to perform the lookup of local data, which is also used by MarketDataRetrieverImpl.
	 * Note:  This method uses the MarketDataField service, which requires a MarketDataSource, and field name.
	 * If not defined in the command, the default "Bloomberg" MarketDataSource is used.
	 */
	private MarketDataValueHolder getMarketDataLocalFieldValue(MarketDataSecurityDataValueCommand command, Date lookupDate, InvestmentSecurity investmentSecurity, InvestmentExchange investmentExchange) {
		if (investmentExchange != null && !investmentSecurity.getInstrument().getExchange().getId().equals(investmentExchange.getId())) {
			return null;
		}

		MarketDataSource marketDataSource = getMarketDataSource(command, investmentSecurity);
		ValidationUtils.assertNotNull(marketDataSource, "Cannot find Market Data Source in command nor using the investment security.");

		String marketDataFieldName = command.getMarketDataFieldName();
		if (StringUtils.isEmpty(marketDataFieldName)) {
			MarketDataField marketDataField = getDefaultPriceField(investmentSecurity);
			if (marketDataField != null) {
				marketDataFieldName = marketDataField.getName();
			}
		}

		ValidationUtils.assertNotNull(marketDataFieldName, "Cannot find Market Data Field in \"MarketDataSecurityDataValue\" command nor via the investment security.");

		if (investmentExchange != null && !investmentSecurity.getInstrument().getExchange().getId().equals(investmentExchange.getId())) {
			return null;
		}

		MarketDataValueHolder marketDataValueHolder = getMarketDataFieldService().getMarketDataValueForDate(investmentSecurity, lookupDate, marketDataFieldName, marketDataSource.getId(), command.isFlexible(), command.isNormalizedPrice());

		if (command.getFlexibleMaxDaysBackAllowed() != null && marketDataValueHolder != null && marketDataValueHolder.getMeasureDate() != null) {
			int daysDifference = DateUtils.getDaysDifference(lookupDate, marketDataValueHolder.getMeasureDate());
			if (daysDifference > command.getFlexibleMaxDaysBackAllowed()) {
				return null;
			}
		}
		return marketDataValueHolder;
	}


	/**
	 * Retrieves historical or the latest reference data from the specified MarketDataProvider.  Returns null if the value was not found.
	 */
	private MarketDataSecurityDataValue getHistoricalRequest(MarketDataSecurityDataValueCommand command, DataFieldValueCommand dataFieldValueCommand,
	                                                         MarketDataProvider<MarketDataValueGeneric<?>> dataProvider, InvestmentExchange investmentExchange, List<String> errorList) {

		ValidationUtils.assertNotNull(dataProvider, "Data provider must not be null.");

		List<MarketDataValueGeneric<?>> marketDataValueList = dataProvider.getMarketDataValueListHistorical(dataFieldValueCommand, errorList);
		if (!CollectionUtils.isEmpty(marketDataValueList) && marketDataValueList.get(0).getMeasureValue() != null) {
			MarketDataValueGeneric<?> marketDataValue = marketDataValueList.get(0);
			final DataTypeNames dataTypeName = getDataTypeName(marketDataValue.getMeasureValue());
			return getMarketDataSecurityDataValue(command, investmentExchange, dataTypeName, marketDataValue.getMeasureDate(), marketDataValue.getMeasureValue());
		}

		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a MarketDataProvider for the MarketDataSource name specified in the MarketDataSecurityDataValueCommand.  If no
	 * MarketDataSource name is set in the command, uses the default MarketDataSource for the Investment Security.
	 */
	private MarketDataProvider<MarketDataValueGeneric<?>> getMarketDataProvider(MarketDataSecurityDataValueCommand command, InvestmentSecurity investmentSecurity) {
		ValidationUtils.assertNotNull(investmentSecurity, "Investment Security cannot be null.");
		final MarketDataSource marketDataSource = command.getMarketDataSourceName() != null ? getMarketDataSourceService().getMarketDataSourceByName(command.getMarketDataSourceName()) : getDefaultMarketDataSource(investmentSecurity);
		ValidationUtils.assertNotNull(marketDataSource, "Cannot determine Market Data Source for security: " + investmentSecurity.getLabel());

		final MarketDataProvider<MarketDataValueGeneric<?>> dataProvider = getMarketDataProviderLocator().locate(marketDataSource);
		ValidationUtils.assertNotNull(dataProvider, "Could not find a data provider for data source: " + marketDataSource.getLabel());

		return dataProvider;
	}


	/**
	 * Returns the MarketDataSource from market data source name in the command, or, if not set on the command, returns the default data source for the InvestmentSecurity.
	 */
	private MarketDataSource getMarketDataSource(MarketDataSecurityDataValueCommand command, InvestmentSecurity investmentSecurity) {
		MarketDataSource marketDataSource;
		if (!StringUtils.isEmpty(command.getMarketDataSourceName())) {
			marketDataSource = getMarketDataSourceService().getMarketDataSourceByName(command.getMarketDataSourceName());
		}
		else {
			marketDataSource = getDefaultMarketDataSource(investmentSecurity);
		}
		return marketDataSource;
	}


	/**
	 * Returns the default MarketDataSource for the Investment Security.
	 */
	private MarketDataSource getDefaultMarketDataSource(InvestmentSecurity investmentSecurity) {
		List<MarketDataFieldMapping> fieldMappingList = getMarketDataFieldMappingService().getMarketDataFieldMappingListByInstrument(investmentSecurity.getInstrument().getId());
		if (!CollectionUtils.isEmpty(fieldMappingList)) {
			return fieldMappingList.get(0).getMarketDataSource();
		}
		return null;
	}


	/**
	 * Returns the default MarketDataField for the Investment Security.
	 */
	private MarketDataField getDefaultPriceField(InvestmentSecurity investmentSecurity) {
		return ObjectUtils.coalesce(getMarketDataFieldService().getMarketDataLiveLatestPriceFieldForSecurity(investmentSecurity),
				getMarketDataFieldService().getMarketDataLiveClosingPriceFieldForSecurity(investmentSecurity));
	}


	/**
	 * Creates, configures, and returns a DataFieldValueCommand from a MarketDataSecurityDataValueCommand for historical data lookups from a data provider.
	 */
	private DataFieldValueCommand getDataFieldValueCommand(MarketDataSecurityDataValueCommand command, Date lookupDate, InvestmentSecurity investmentSecurity, InvestmentExchange investmentExchange) {
		DataFieldValueCommand.DataFieldValueCommandBuilder commandBuilder = DataFieldValueCommand.newBuilder()
				.setProviderTimeoutOverride(command.getTimeoutOverride());

		String marketSectorName = command.getMarketSectorName();
		InvestmentExchange exchange = ObjectUtils.coalesce(investmentExchange, investmentSecurity.getInstrument().getExchange());
		if (exchange != null) {
			investmentSecurity.getInstrument().setExchange(exchange);
			commandBuilder.setExchangeCode(exchange.getExchangeCode());
			commandBuilder.setExchangeCodeType(exchange.isCompositeExchange() ? InvestmentExchangeTypes.COMPOSITE_EXCHANGE : InvestmentExchangeTypes.PRIMARY_EXCHANGE);
		}

		if (StringUtils.isEmpty(marketSectorName)) {
			MarketDataFieldMapping mapping = getMarketDataFieldMappingRetriever().getMarketDataFieldMappingByInstrument(investmentSecurity.getInstrument(), getMarketDataSource(command, investmentSecurity));
			ValidationUtils.assertNotNull(mapping, "Cannot locate market data field mapping for investment security: " + investmentSecurity.getLabel());
			marketSectorName = mapping.getMarketSector() != null ? mapping.getMarketSector().getName() : null;
		}

		MarketDataField marketDataField = StringUtils.isEmpty(command.getMarketDataFieldName()) ? getDefaultPriceField(investmentSecurity) : getMarketDataFieldService().getMarketDataFieldByName(command.getMarketDataFieldName());
		ValidationUtils.assertNotNull(marketDataField, "Cannot find a market data field with name \"" + command.getMarketDataFieldName() + "\"  nor the default market data data field for security: " + investmentSecurity.getLabel());

		commandBuilder.addSecurity(investmentSecurity, CollectionUtils.createList(marketDataField));
		commandBuilder.setMarketSector(marketSectorName);
		commandBuilder.setPricingSource(command.getPricingSourceName());

		// configure for historical date lookup (single day range - 00:00 to 18:00), if this is a historical lookup.  This matches local historical market data.
		if (lookupDate != null) {
			commandBuilder.setStartDate(lookupDate);
			commandBuilder.setEndDate(DateUtils.addMinutes(lookupDate, 18 * 60));
		}

		return commandBuilder.build();
	}


	/**
	 * Converts the MarketDataSecurityDataValueCommand to a MarketDataValue command used for live data lookups.
	 */
	private MarketDataLiveCommand getMarketDataLiveCommand(MarketDataSecurityDataValueCommand command, InvestmentExchange exchange) {
		MarketDataLiveCommand marketDataLiveCommand = new MarketDataLiveCommand();
		marketDataLiveCommand.setSecurityIds(command.getSecurityId());
		marketDataLiveCommand.setFieldNames(command.getMarketDataFieldName());
		if (exchange != null) {
			marketDataLiveCommand.setExchangeId(exchange.getId());
		}
		marketDataLiveCommand.setMarketSectorName(command.getMarketSectorName());
		marketDataLiveCommand.setMaxAgeInSeconds(command.getMaxAgeInSeconds());
		marketDataLiveCommand.setProviderTimeoutOverride(command.getTimeoutOverride());
		marketDataLiveCommand.setIgnoreErrors(command.isIgnoreErrors());

		if (!StringUtils.isEmpty(command.getProviderOverrideName())) {
			MarketDataProviderOverrides providerOverride = MarketDataProviderOverrides.valueOf(command.getProviderOverrideName());
			marketDataLiveCommand.setProviderOverride(providerOverride);
		}

		return marketDataLiveCommand;
	}


	/**
	 * Looks up the investment calendar for the security, considering the override exchange calendar first.
	 * <p>
	 * Note:  The derivation of the calendar follows the technique used in MarketDataRetrieverImpl.
	 *
	 * @param investmentSecurity investment security to which the calendar will apply
	 * @param overrideExchange   an instance of the exchange to get the calendar for.  May be null.
	 */
	private Calendar getInvestmentCalendar(InvestmentSecurity investmentSecurity, InvestmentExchange overrideExchange) {

		if (overrideExchange != null && overrideExchange.getCalendar() != null) {
			return overrideExchange.getCalendar();
		}

		Calendar investmentCalendar = null;
		InvestmentInstrument underlying = investmentSecurity.getInstrument().getUnderlyingInstrument();

		if (underlying != null && underlying.getExchange() != null) {
			investmentCalendar = underlying.getExchange().getCalendar();
		}

		if (investmentCalendar == null) {
			investmentCalendar = getInvestmentCalculator().getInvestmentSecurityCalendar(investmentSecurity);
		}

		return investmentCalendar;
	}


	/**
	 * Returns the nearest business day in the past if the date in the method argument is a business holiday.
	 *
	 * @param date               the date for which to get the current or previous business day.
	 * @param investmentCalendar an investment calendar to use for the date calculation.
	 */
	private Date getBusinessDayOrPrevious(Date date, Calendar investmentCalendar) {
		Date businessDate = date;
		if (investmentCalendar != null) {
			CalendarBusinessDayCommand calendarBusinessDayCommand = new CalendarBusinessDayCommand();
			calendarBusinessDayCommand.setDate(date);
			calendarBusinessDayCommand.setCalendarIds(new Short[]{investmentCalendar.getId()});
			if (!getCalendarBusinessDayService().isBusinessDay(calendarBusinessDayCommand)) {
				businessDate = getCalendarBusinessDayService().getNearestBusinessDayFrom(calendarBusinessDayCommand, -1);
			}
		}
		return businessDate;
	}


	/**
	 * A helper method to determine the {@link DataTypeNames} for a given object instance.
	 */
	private DataTypeNames getDataTypeName(Object marketDataMeasureValue) {
		if (marketDataMeasureValue instanceof Integer || marketDataMeasureValue instanceof Short || marketDataMeasureValue instanceof Long || marketDataMeasureValue instanceof BigInteger || marketDataMeasureValue instanceof Byte) {
			return DataTypeNames.INTEGER;
		}

		if (marketDataMeasureValue instanceof BigDecimal || marketDataMeasureValue instanceof Double || marketDataMeasureValue instanceof Float) {
			return DataTypeNames.DECIMAL;
		}

		if (marketDataMeasureValue instanceof Boolean) {
			return DataTypeNames.BOOLEAN;
		}

		if (marketDataMeasureValue instanceof Character || marketDataMeasureValue instanceof String) {
			return DataTypeNames.STRING;
		}

		if (marketDataMeasureValue instanceof Date) {
			return DataTypeNames.DATE;
		}

		return null;
	}


	/**
	 * A helper method to create and return a MarketDataSecurityDataValue instance.  This instance contains the result of a public lookup method.
	 */
	private MarketDataSecurityDataValue getMarketDataSecurityDataValue(MarketDataSecurityDataValueCommand command, InvestmentExchange investmentExchange, DataTypeNames dataTypeName, Date measureDate, Object measureValue) {
		MarketDataSecurityDataValue marketDataSecurityDataValue = new MarketDataSecurityDataValue(command.getSecurityId(), dataTypeName, command.getMarketDataFieldName(), measureValue);
		if (investmentExchange != null) {
			marketDataSecurityDataValue = marketDataSecurityDataValue.withExchangeName(investmentExchange.getName()).withExchangeCode(investmentExchange.getExchangeCode());
		}
		if (command.getRequestedDate() != null) {
			marketDataSecurityDataValue = marketDataSecurityDataValue.withRequestedDate(command.getRequestedDate());
		}
		if (measureDate != null) {
			marketDataSecurityDataValue = marketDataSecurityDataValue.withMeasureDate(measureDate);
		}
		if (!StringUtils.isEmpty(command.getMarketDataSourceName())) {
			marketDataSecurityDataValue = marketDataSecurityDataValue.withDataSourceName(command.getMarketDataSourceName());
		}

		return marketDataSecurityDataValue;
	}


	/**
	 * Validates the {@link MarketDataSecurityDataValueCommand} command.  Cannot have validation method on
	 */
	private void validateMarketDataSecurityDataValueCommand(MarketDataSecurityDataValueCommand command) {
		ValidationUtils.assertNotNull(command.getSecurityId(), "Security ID value cannot be null.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldMappingRetriever getMarketDataFieldMappingRetriever() {
		return this.marketDataFieldMappingRetriever;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public void setMarketDataFieldMappingRetriever(MarketDataFieldMappingRetriever marketDataFieldMappingRetriever) {
		this.marketDataFieldMappingRetriever = marketDataFieldMappingRetriever;
	}


	public MarketDataFieldMappingService getMarketDataFieldMappingService() {
		return this.marketDataFieldMappingService;
	}


	public void setMarketDataFieldMappingService(MarketDataFieldMappingService marketDataFieldMappingService) {
		this.marketDataFieldMappingService = marketDataFieldMappingService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}
}
