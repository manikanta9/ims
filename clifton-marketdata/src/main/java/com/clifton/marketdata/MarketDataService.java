package com.clifton.marketdata;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.MarketDataValue;
import org.springframework.web.bind.annotation.ResponseBody;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataService</code> interface defines common utility methods for MarketData objects.
 *
 * @author Mary Anderson
 */
public interface MarketDataService {

	////////////////////////////////////////////////////////////////////////////
	///////          Investment Security Market Data Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@ResponseBody
	@SecureMethod(dtoClass = MarketDataValue.class)
	public BigDecimal getMarketDataPriceFlexibleForSecurity(int securityId, Date date, Boolean exceptionIfMissing);


	@ResponseBody
	@SecureMethod(dtoClass = MarketDataValue.class)
	public BigDecimal getMarketDataIndexRatioForSecurity(int securityId, Date date);


	@DoNotAddRequestMapping
	public BigDecimal getMarketDataIndexRatioForSecurity(int securityId, Date date, boolean exceptionIfMissing);
}
