package com.clifton.marketdata.calculator.indexratio;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueHolder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;


/**
 * Returns an Index Ratio based on the two methodologies:
 * First try to lookup Index Ratio value from market data of security identified by "Index Ratio" custom field.
 * If not defined, calculate it as Current CPI/Base CPI of security identified by "Inflation Index" custom field.
 * Base CPI value can optionally be overridden by "Base Inflation Value" custom field.
 *
 * @author davidi
 */
public class MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever extends BaseMarketDataIndexRatioMultiplierRetriever implements NotionalMultiplierRetriever, ValidationAware {

	/**
	 * The name of the custom field on the investment security containing a reference to the linked security associated with the inflation index values.
	 */
	private String linkedSecurityField;

	/**
	 * The name of the market data field on the investment security containing a reference to the linked security used for inflation index lookup from market data.
	 */
	private String linkedSecurityMarketDataField;

	/**
	 * A comma-delimited string of country codes that use Quarterly Reporting Periods (not monthly)
	 */
	private List<String> countriesUsingQuarterlyReportingPeriods;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal retrieveNotionalMultiplier(InvestmentSecurity security, Supplier<Date> dateSupplier, boolean exceptionIfMissing) {
		final InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

		if (StringUtils.isEmpty(getLinkedSecurityField())) {
			setLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX);
		}

		if (hierarchy.isIndexRatioAdjusted()) {
			Integer inflationSecurityId = getInflationSecurityIdByCustomField(security, getLinkedSecurityField());
			if (inflationSecurityId == null) {
				String errorMsg = "Could not retrieve the linked inflation security for investment security: " + security.getLabel();
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				return BigDecimal.ONE;
			}

			// Get inflation reporting periods (INFLATION_LAG)
			Integer inflationReportingPeriods = getInflationReportingPeriods(security, getCountriesUsingQuarterlyReportingPeriods());
			if (inflationReportingPeriods == null) {
				String errorMsg = "Inflation-linked security: " + security.getLabel() + " is missing a value for field \"Inflation Reporting Periods.\"";
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				inflationReportingPeriods = 0;
			}

			// Check security for pre-configured Base inflation index
			BigDecimal baseRPI = getBaseInflationRateFromSecurity(security);
			if (baseRPI == null) {
				if (security.getStartDate() == null) {
					String errorMsg = "Investment security: " + security.getLabel() + " is missing a Start Date, which is required for lookup of the base inflation index value.";
					ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
					LogUtils.error(getClass(), errorMsg);
					return BigDecimal.ONE;
				}

				baseRPI = getInterpolatedInflationIndex(inflationSecurityId, security.getStartDate(), inflationReportingPeriods, exceptionIfMissing);
				if (baseRPI == null) {
					String errorMsg = "Could not derive base inflation index via interpolation.";
					ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
					LogUtils.error(getClass(), errorMsg);
					return BigDecimal.ONE;
				}
			}

			if (MathUtils.isEqual(BigDecimal.ZERO, baseRPI)) {
				ValidationUtils.assertFalse(exceptionIfMissing, "The Base inflation index value cannot be 0.00 for security: " + security.getLabel());
				return BigDecimal.ONE;
			}

			Date currentDate = dateSupplier.get();
			BigDecimal referenceRPI;
			if (hierarchy.isIndexRatioCalculatedUsingMonthStartAfterPrevCoupon()) {
				Date previousCouponPaymentDate = getPreviousCashCouponPaymentDate(security, currentDate, exceptionIfMissing);
				if (previousCouponPaymentDate == null) {
					return BigDecimal.ONE;
				}
				currentDate = DateUtils.addMonths(DateUtils.getFirstDayOfMonth(previousCouponPaymentDate), 1);
				referenceRPI = getInterpolatedInflationIndex(inflationSecurityId, currentDate, 0, exceptionIfMissing);
			}
			else {
				referenceRPI = getInterpolatedInflationIndex(inflationSecurityId, currentDate, inflationReportingPeriods, exceptionIfMissing);
			}

			if (referenceRPI == null) {
				String errorMsg = "The reference inflation index value could not be derived for security: " + security.getLabel();
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				return BigDecimal.ONE;
			}

			BigDecimal indexRatio = MathUtils.divide(referenceRPI, baseRPI);

			if (hierarchy.getIndexRatioPrecision() != null) {
				indexRatio = MathUtils.round(indexRatio, hierarchy.getIndexRatioPrecision());
			}

			return indexRatio;
		}

		return BigDecimal.ONE;
	}


	@Override
	public void validate(InvestmentInstrumentHierarchy hierarchy) {
		if (!hierarchy.isIndexRatioAdjusted()) {
			throw new ValidationException("The selected Notional Multiplier Retriever calculates an Index Ratio and should only be used for inflation-linked securities.");
		}
	}


	@Override
	public void validate() {
		// currently no validations - all fields optional with no current restrictions, country codes are in selection combo presenting valid list if SystemListItem of countries and are thereby guaranteed to be valid.
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up the starting and ending RPI values based on the targetDate, and offset by the inflationReportingPeriods (as months) then applies these values
	 * to the along with the targetDate to interpolate a new RPI value.
	 */
	private BigDecimal getInterpolatedInflationIndex(Integer inflationIndexSecurityId, Date targetDate, int inflationReportingPeriods, boolean exceptionIfMissing) {

		if (StringUtils.isEmpty(getLinkedSecurityMarketDataField())) {
			setLinkedSecurityMarketDataField(MarketDataField.FIELD_INDEX_RATIO);
		}

		Date startingDate = DateUtils.getFirstDayOfMonth(targetDate);
		Date endingDate = DateUtils.addMonths(startingDate, 1);

		// These dates will be used to lookup the RPI values for the month.  For older values, the RPI may be available on a daily basis, allowing direct lookup,
		// yet more recent entries have a single RPI point at the end of the month, which then applies to the entire month.
		Date adjustedStartingDate = DateUtils.addMonths(startingDate, -inflationReportingPeriods);
		Date adjustedEndingDate = DateUtils.addMonths(endingDate, -inflationReportingPeriods);

		MarketDataValueHolder dataPointBefore = getMarketDataFieldService().getMarketDataValueForDate(inflationIndexSecurityId, adjustedStartingDate, getLinkedSecurityMarketDataField());
		MarketDataValueHolder dataPointAfter = getMarketDataFieldService().getMarketDataValueForDate(inflationIndexSecurityId, adjustedEndingDate, getLinkedSecurityMarketDataField());

		if (dataPointBefore == null) {
			adjustedStartingDate = DateUtils.getLastDayOfMonth(adjustedStartingDate);
			dataPointBefore = getMarketDataFieldService().getMarketDataValueForDate(inflationIndexSecurityId, adjustedStartingDate, getLinkedSecurityMarketDataField());
		}

		if (dataPointAfter == null) {
			adjustedEndingDate = DateUtils.getLastDayOfMonth(adjustedEndingDate);
			dataPointAfter = getMarketDataFieldService().getMarketDataValueForDate(inflationIndexSecurityId, adjustedEndingDate, getLinkedSecurityMarketDataField());
		}

		if (dataPointBefore == null) {
			String errorMsg = "No inflation index data found for the specified date: " + adjustedStartingDate + " for investment security with ID:" + inflationIndexSecurityId + ".";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		if (dataPointBefore.getMeasureValue() == null) {
			String errorMsg = "No inflation index data measurement for the  MarketDataValue entry before the specified date: " + adjustedStartingDate + " for investment security with ID:" + inflationIndexSecurityId + ".";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		if (dataPointAfter == null) {
			String errorMsg = "No inflation index data found for the specified date: " + adjustedEndingDate + " for investment security with ID:" + inflationIndexSecurityId + ".";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		if (dataPointAfter.getMeasureValue() == null) {
			String errorMsg = "No inflation index data measurement for the MarketDataValue entry for the specified date: " + adjustedEndingDate + " for investment security with ID:" + inflationIndexSecurityId + ".";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		//   Performs a 2 point interpolation to derive an intermediate inflation index value on the target date.
		BigDecimal daysDiffForTargetDate = BigDecimal.valueOf(DateUtils.getDaysDifference(targetDate, startingDate));
		BigDecimal daysBetweenEndPoints = BigDecimal.valueOf(DateUtils.getDaysDifference(endingDate, startingDate));
		BigDecimal inflationIndexDiff = MathUtils.subtract(dataPointAfter.getMeasureValue(), dataPointBefore.getMeasureValue());
		BigDecimal slope = MathUtils.divide(inflationIndexDiff, daysBetweenEndPoints);
		return MathUtils.add(dataPointBefore.getMeasureValue(), MathUtils.multiply(daysDiffForTargetDate, slope));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLinkedSecurityField() {
		return this.linkedSecurityField;
	}


	public void setLinkedSecurityField(String linkedSecurityField) {
		this.linkedSecurityField = linkedSecurityField;
	}


	public String getLinkedSecurityMarketDataField() {
		return this.linkedSecurityMarketDataField;
	}


	public void setLinkedSecurityMarketDataField(String linkedSecurityMarketDataField) {
		this.linkedSecurityMarketDataField = linkedSecurityMarketDataField;
	}


	public List<String> getCountriesUsingQuarterlyReportingPeriods() {
		return this.countriesUsingQuarterlyReportingPeriods;
	}


	public void setCountriesUsingQuarterlyReportingPeriods(List<String> countriesUsingQuarterlyReportingPeriods) {
		this.countriesUsingQuarterlyReportingPeriods = countriesUsingQuarterlyReportingPeriods;
	}
}
