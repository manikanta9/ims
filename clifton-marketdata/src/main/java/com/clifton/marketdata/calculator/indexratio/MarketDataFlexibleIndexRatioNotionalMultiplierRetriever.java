package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueHolder;

import java.math.BigDecimal;
import java.util.Date;
import java.util.function.Supplier;


/**
 * The MarketDataIndexRatioNotionalMultiplierRetriever class implements the logic that retrieves the value of Index Ratio
 * for the specified {@link InvestmentSecurity} on the specified date.  It is used by Inflation Linked Securities: TIPS, etc.
 *
 * @author vgomelsky
 */
public class MarketDataFlexibleIndexRatioNotionalMultiplierRetriever extends BaseMarketDataIndexRatioMultiplierRetriever implements NotionalMultiplierRetriever, ValidationAware {

	private IndexRatioCalculationMethods primaryIndexRatioCalculationMethod;
	private String primaryLinkedSecurityField;
	private String primaryLinkedSecurityMarketDataField;

	private IndexRatioCalculationMethods secondaryIndexRatioCalculationMethod;
	private String secondaryLinkedSecurityField;
	private String secondaryLinkedSecurityMarketDataField;


	public enum IndexRatioCalculationMethods {
		FIELD_VALUE_RETRIEVAL,
		INFLATION_INDEX_BASED
	}

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal retrieveNotionalMultiplier(InvestmentSecurity security, Supplier<Date> dateSupplier, boolean exceptionIfMissing) {

		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (!hierarchy.isIndexRatioAdjusted()) {
			return BigDecimal.ONE;
		}

		BigDecimal indexRatio = null;
		ValidationException lastValidationException = null;
		try {
			indexRatio = executeCalculationMethod(security, dateSupplier.get(), getPrimaryLinkedSecurityField(), getPrimaryLinkedSecurityMarketDataField(), exceptionIfMissing, getPrimaryIndexRatioCalculationMethod());
		}
		catch (ValidationException exception) {
			if (getSecondaryIndexRatioCalculationMethod() == null) {
				throw (exception);
			}
			lastValidationException = exception;
		}

		if (indexRatio == null) {
			if (getSecondaryIndexRatioCalculationMethod() != null) {
				indexRatio = executeCalculationMethod(security, dateSupplier.get(), getSecondaryLinkedSecurityField(), getSecondaryLinkedSecurityMarketDataField(), exceptionIfMissing, getSecondaryIndexRatioCalculationMethod());
			}

			if (indexRatio == null) {
				if (lastValidationException != null) {
					throw lastValidationException;
				}
				indexRatio = BigDecimal.ONE;
			}
		}

		// round if necessary (Japanese bonds round to 3 decimals, etc.)
		if (hierarchy.getIndexRatioPrecision() != null) {
			indexRatio = MathUtils.round(indexRatio, hierarchy.getIndexRatioPrecision());
		}

		return indexRatio;
	}


	@Override
	public void validate(InvestmentInstrumentHierarchy hierarchy) {
		if (!hierarchy.isIndexRatioAdjusted()) {
			throw new ValidationException("The selected Notional Multiplier Retriever calculates an Index Ratio and should only be used for inflation-linked securities.");
		}
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getPrimaryIndexRatioCalculationMethod(), "An index ratio calculation method must be selected.");
		ValidationUtils.assertNotEquals(getPrimaryIndexRatioCalculationMethod(), getSecondaryIndexRatioCalculationMethod(), "The secondary calculation method must be different from the selected primary calculation method.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private BigDecimal executeCalculationMethod(InvestmentSecurity security, Date date, String securityFieldName, String marketDataFieldName, boolean exceptionIfMissing, IndexRatioCalculationMethods method) {
		if (method == IndexRatioCalculationMethods.FIELD_VALUE_RETRIEVAL) {
			return getIndexRatioByFieldValueRetrieval(security, date, securityFieldName, marketDataFieldName, exceptionIfMissing);
		}
		else if (method == IndexRatioCalculationMethods.INFLATION_INDEX_BASED) {
			return getIndexRatioByInflationIndexCalculation(security, date, securityFieldName, marketDataFieldName, exceptionIfMissing);
		}
		String errorMsg = "Cannot lookup index ratio for security " + security.getLabel() + ".  Missing or unsupported Index Ratio Calculation method.";
		ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
		LogUtils.error(getClass(), errorMsg);
		return null;
	}


	/**
	 * Retrieves the Index Ratio by direct lookup from market data containing index ratios associated with a linked security.
	 * Default Values:  For SecurityFieldName it is "Index Ratio" and for the MarketDataFieldName it is "Value".
	 */
	private BigDecimal getIndexRatioByFieldValueRetrieval(InvestmentSecurity security, Date date, String securityFieldName, String marketDataFieldName, boolean exceptionIfMissing) {
		BigDecimal indexRatio = null;

		if (StringUtils.isEmpty(securityFieldName)) {
			securityFieldName = InvestmentSecurity.CUSTOM_FIELD_INDEX_RATIO;
		}

		if (StringUtils.isEmpty(marketDataFieldName)) {
			marketDataFieldName = MarketDataField.FIELD_INDEX_RATIO;
		}

		Integer inflationSecurityId = getInflationSecurityIdByCustomField(security, securityFieldName);
		if (inflationSecurityId != null) {
			// index ratio value is retrieved by market data provider (TIPS); don't need to calculate
			MarketDataValueHolder value = getMarketDataFieldService().getMarketDataValueForDateFlexible(inflationSecurityId, date, marketDataFieldName);
			if (value == null || value.getMeasureValue() == null) {
				String errorMsg = "Missing index ratio for security [" + security.getLabel() + "] on or before [" + DateUtils.fromDateShort(date) + "].";
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
			}
			else {
				indexRatio = value.getMeasureValue();
			}
		}

		return indexRatio;
	}


	/**
	 * Uses a inflation index market data values associated with the linked security (e.g. CPI, LDI, or whatever the security has configured for its inflation index security) to calculate an Index Ratio.
	 * The calculation does not change based on the inflation index, and is basically  reference_inflation_index / base_inflation_index.
	 * Default Values:  For SecurityFieldName it is "Inflation Index" and for the MarketDataFieldName it is "Value".
	 */
	private BigDecimal getIndexRatioByInflationIndexCalculation(InvestmentSecurity security, Date date, String securityFieldName, String marketDataFieldName, boolean exceptionIfMissing) {
		if (StringUtils.isEmpty(securityFieldName)) {
			securityFieldName = InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX;
		}

		if (StringUtils.isEmpty(marketDataFieldName)) {
			marketDataFieldName = MarketDataField.FIELD_INDEX_RATIO;
		}

		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();

		Integer inflationSecurityId = getInflationSecurityIdByCustomField(security, securityFieldName);
		if (inflationSecurityId == null) {
			String errorMsg = "Cannot calculate Index Ratio because no Inflation Index is defined for " + security.getLabel();
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		// check if Base Index Value is explicitly set (French bonds use different date than Issue Date of the bond so this value must be explicitly set).
		BigDecimal baseInflationValue = getBaseInflationRateFromSecurity(security);
		if (baseInflationValue == null) {
			// not set explicitly: look it up from market data
			if (security.getStartDate() == null) {
				String errorMsg = "Cannot calculate Index Ratio because no Start Date is defined for " + security.getLabel();
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				return null;
			}
			MarketDataValueHolder baseValue = getMarketDataFieldService().getMarketDataValueForDateFlexible(inflationSecurityId, security.getStartDate(), marketDataFieldName);
			if (baseValue == null || baseValue.getMeasureValue() == null) {
				String errorMsg = "Missing index ratio for security [" + security.getLabel() + "] on or before [" + DateUtils.fromDateShort(security.getStartDate()) + "].";
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				return null;
			}
			baseInflationValue = baseValue.getMeasureValue();
		}

		Date currentDate = date;
		if (hierarchy.isIndexRatioCalculatedUsingMonthStartAfterPrevCoupon()) {
			// lookup previous coupon period and get first day of the following month
			Date couponPaymentDate = getPreviousCashCouponPaymentDate(security, currentDate, exceptionIfMissing);
			if (couponPaymentDate == null) {
				String errorMsg = "Cannot calculate Index Ratio because no previous coupon period is defined for " + security.getLabel() + " on " + DateUtils.fromDateShort(currentDate);
				ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
				LogUtils.error(getClass(), errorMsg);
				return null;
			}
			currentDate = DateUtils.addMonths(couponPaymentDate, 1);
		}
		MarketDataValueHolder currentValue = getMarketDataFieldService().getMarketDataValueForDateFlexible(inflationSecurityId, currentDate, marketDataFieldName);
		if (currentValue == null || currentValue.getMeasureValue() == null) {
			String errorMsg = "Missing index ratio for security [" + security.getLabel() + "] on or before [" + DateUtils.fromDateShort(currentDate) + "].";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
			return null;
		}

		// Index Ratio = Settlement Date Inflation Index Value / Issue Date Inflation Index Value
		return MathUtils.divide(currentValue.getMeasureValue(), baseInflationValue);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IndexRatioCalculationMethods getPrimaryIndexRatioCalculationMethod() {
		return this.primaryIndexRatioCalculationMethod;
	}


	public void setPrimaryIndexRatioCalculationMethod(IndexRatioCalculationMethods primaryIndexRatioCalculationMethod) {
		this.primaryIndexRatioCalculationMethod = primaryIndexRatioCalculationMethod;
	}


	public String getPrimaryLinkedSecurityField() {
		return this.primaryLinkedSecurityField;
	}


	public void setPrimaryLinkedSecurityField(String primaryLinkedSecurityField) {
		this.primaryLinkedSecurityField = primaryLinkedSecurityField;
	}


	public String getPrimaryLinkedSecurityMarketDataField() {
		return this.primaryLinkedSecurityMarketDataField;
	}


	public void setPrimaryLinkedSecurityMarketDataField(String primaryLinkedSecurityMarketDataField) {
		this.primaryLinkedSecurityMarketDataField = primaryLinkedSecurityMarketDataField;
	}


	public IndexRatioCalculationMethods getSecondaryIndexRatioCalculationMethod() {
		return this.secondaryIndexRatioCalculationMethod;
	}


	public void setSecondaryIndexRatioCalculationMethod(IndexRatioCalculationMethods secondaryIndexRatioCalculationMethod) {
		this.secondaryIndexRatioCalculationMethod = secondaryIndexRatioCalculationMethod;
	}


	public String getSecondaryLinkedSecurityField() {
		return this.secondaryLinkedSecurityField;
	}


	public void setSecondaryLinkedSecurityField(String secondaryLinkedSecurityField) {
		this.secondaryLinkedSecurityField = secondaryLinkedSecurityField;
	}


	public String getSecondaryLinkedSecurityMarketDataField() {
		return this.secondaryLinkedSecurityMarketDataField;
	}


	public void setSecondaryLinkedSecurityMarketDataField(String secondaryLinkedSecurityMarketDataField) {
		this.secondaryLinkedSecurityMarketDataField = secondaryLinkedSecurityMarketDataField;
	}
}


