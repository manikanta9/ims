package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.system.schema.column.search.SystemColumnDatedValueSearchForm;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;
import com.clifton.system.schema.column.value.SystemColumnValueService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.function.Supplier;


/**
 * The InvestmentSecurityCustomFieldNotionalMultiplierRetriever class implements the logic that returns the value
 * of the specified {@link InvestmentSecurity} custom field. For example, "FV Notional Multiplier" for Zero Coupon Swaps.
 *
 * @author vgomelsky
 */
public class InvestmentSecurityFieldNotionalMultiplierRetriever implements NotionalMultiplierRetriever {

	/**
	 * An enumeration of supported field types used in this retriever.  The CUSTOM type refers to custom fields on an InvestmentSecurity,
	 * and MarketData refers to fields that are used in MarketData to store static values for retrieval.
	 */
	public enum FieldTypes {
		CUSTOM,
		MARKETDATA,
		CUSTOM_DATED,
		MARKETDATA_FLEXIBLE
	}


	/**
	 * The field type of the field referenced by the property notionalMultiplierFieldName.
	 * This value can be CUSTOM for custom fields on the InvestmentSecurity or MARKETDATA for a MarketData field name for
	 * market data associated with the InvestmentSecurity that contains a static notional multiplier value.
	 */
	private FieldTypes fieldType;


	/**
	 * Custom column on {@link InvestmentSecurity} that has the value of security specific Notional Multiplier.
	 * This value does NOT change over time. For example, "FV Notional Multiplier" for Zero Coupon Swaps.
	 */
	private String notionalMultiplierFieldName;

	////////////////////////////////////////////////////////////////////////////

	private SystemColumnValueHandler systemColumnValueHandler;

	private SystemColumnValueService systemColumnValueService;

	private MarketDataFieldService marketDataFieldService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal retrieveNotionalMultiplier(InvestmentSecurity security, Supplier<Date> dateSupplier, boolean exceptionIfMissing) {
		BigDecimal result = null;

		if (FieldTypes.CUSTOM == getFieldType()) {
			result = (BigDecimal) getSystemColumnValueHandler().getSystemColumnValueForEntity(security,
					InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, getNotionalMultiplierFieldName(), false);
		}
		else if (FieldTypes.MARKETDATA == getFieldType()) {
			//result = (BigDecimal)
			MarketDataValueHolder valueHolder = getMarketDataFieldService().getMarketDataValueForDate(security.getId(), dateSupplier.get(), getNotionalMultiplierFieldName());
			if (valueHolder != null && valueHolder.getMeasureValue() != null) {
				result = valueHolder.getMeasureValue();
			}
		}
		else if (FieldTypes.CUSTOM_DATED == getFieldType()) {
			SystemColumnDatedValueSearchForm sf = new SystemColumnDatedValueSearchForm();
			sf.setFkFieldId(security.getId());
			sf.setColumnGroupName(InvestmentSecurity.SECURITY_DATED_CUSTOM_FIELDS_GROUP_NAME);
			sf.setActiveOnDate(dateSupplier.get());
			sf.setColumnName(getNotionalMultiplierFieldName());
			List<SystemColumnDatedValue> values = getSystemColumnValueService().getSystemColumnDatedValueList(sf);
			if (CollectionUtils.getSize(values) == 1) {
				result = new BigDecimal(CollectionUtils.getOnlyElement(values).getValue());
			}
			else {
				result = BigDecimal.ONE;
			}
		}
		else if (FieldTypes.MARKETDATA_FLEXIBLE == getFieldType()) {
			MarketDataValueHolder valueHolder = getMarketDataFieldService().getMarketDataValueForDateFlexible(security.getId(), dateSupplier.get(), getNotionalMultiplierFieldName());
			if (valueHolder != null && valueHolder.getMeasureValue() != null) {
				result = valueHolder.getMeasureValue();
			}
		}

		if (result == null) {
			String errorMsg = "Cannot retrieve Notional Multiplier for " + security + " because '" + getNotionalMultiplierFieldName() + "' field value is not defined.";
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(), errorMsg);
		}

		return result;
	}


	@Override
	public void validate(InvestmentInstrumentHierarchy hierarchy) {
		// currently no validation
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public String getNotionalMultiplierFieldName() {
		return this.notionalMultiplierFieldName;
	}


	public void setNotionalMultiplierFieldName(String notionalMultiplierFieldName) {
		this.notionalMultiplierFieldName = notionalMultiplierFieldName;
	}


	public FieldTypes getFieldType() {
		return this.fieldType;
	}


	public void setFieldType(FieldTypes fieldType) {
		this.fieldType = fieldType;
	}


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public SystemColumnValueService getSystemColumnValueService() {
		return this.systemColumnValueService;
	}


	public void setSystemColumnValueService(SystemColumnValueService systemColumnValueService) {
		this.systemColumnValueService = systemColumnValueService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}
}
