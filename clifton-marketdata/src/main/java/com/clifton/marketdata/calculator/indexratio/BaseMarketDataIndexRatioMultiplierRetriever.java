package com.clifton.marketdata.calculator.indexratio;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.InvestmentSecurityEventType;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.system.list.SystemListItem;
import com.clifton.system.schema.column.value.SystemColumnValueHandler;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * An abstract base class for MarketDataIndexRatioNotionalRetriever classes containing methods that are commonly used in these calculators.
 * Note that these methods were originally fround within the getIndexRatioFlexible() formerly located in the MarketDataRetriever class.
 *
 * @author davidi
 */
public abstract class BaseMarketDataIndexRatioMultiplierRetriever {

	private InvestmentSecurityEventService investmentSecurityEventService;

	private SystemColumnValueHandler systemColumnValueHandler;

	private MarketDataFieldService marketDataFieldService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks the specified custom field of the security for a linked security ID used to look inflation index values via the linked security's market data.
	 */
	public Integer getInflationSecurityIdByCustomField(InvestmentSecurity security, String customFieldName) {
		return (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
				customFieldName, false);
	}


	/**
	 * Looks up the BaseInflationRate from a custom property on the security.  Returns this rate or null if not defined.
	 */
	public BigDecimal getBaseInflationRateFromSecurity(InvestmentSecurity security) {
		return (BigDecimal) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME,
				InvestmentSecurity.CUSTOM_FIELD_BASE_INFLATION_VALUE, false);
	}


	/**
	 * Returns the last cash coupon payment date for a given investment security.
	 *
	 * @return the last coupon payment date (if found) or null if not found and exceptionIfMissing == False
	 */
	public Date getPreviousCashCouponPaymentDate(InvestmentSecurity security, Date currentDate, boolean exceptionIfMissing) {
		// lookup previous coupon period and get first day of the following month
		InvestmentSecurityEvent couponPayment = getInvestmentSecurityEventService().getInvestmentSecurityEventPreviousForAccrualEndDate(security.getId(),
				InvestmentSecurityEventType.CASH_COUPON_PAYMENT, currentDate);
		if (couponPayment == null) {
			String errorMsg = "Cannot calculate Index Ratio because no previous coupon period is defined for " + security.getLabel() + " on " + DateUtils.fromDateShort(currentDate);
			ValidationUtils.assertFalse(exceptionIfMissing, errorMsg);
			LogUtils.error(getClass(),errorMsg);
			return null;
		}
		return couponPayment.getEventDate();
	}


	/**
	 * Returns the Number of inflation reporting periods, denoted in months, between the effective period of the inflation rate and the period in which the associated determination date occurs.
	 * For Australia and New Zealand, where inflation is reported on a quarterly basis, the value is converted from quarters to months by applying a multiplier of 3.  Returns null
	 * if the "Inflation Reporting Periods" value is not defined on the InvestmentSecurity entity.
	 */
	protected Integer getInflationReportingPeriods(InvestmentSecurity security, List<String> countriesUsingQuarterlyUnits) {
		int monthMultiplier = 1;
		Set<String> countriesUsingQuarterlyUnitsSet = countriesUsingQuarterlyUnits != null ? new HashSet<>(countriesUsingQuarterlyUnits) : new HashSet<>();

		SystemListItem countryOfRisk = security.getInstrument().getCountryOfRisk();

		// The period is quarterly for AU and NZ, so we convert the quarters to months for securities issues by these countries.
		if (countryOfRisk != null && countriesUsingQuarterlyUnitsSet.contains(countryOfRisk.getValue())) {
			monthMultiplier = 3;
		}

		Integer inflationReportingPeriods = (Integer) getSystemColumnValueHandler().getSystemColumnValueForEntity(security, InvestmentSecurity.SECURITY_CUSTOM_FIELDS_GROUP_NAME, InvestmentSecurity.CUSTOM_FIELD_INFLATION_REPORTING_PERIODS, false);
		if (inflationReportingPeriods == null) {
			return null;
		}
		return (inflationReportingPeriods * monthMultiplier);
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemColumnValueHandler getSystemColumnValueHandler() {
		return this.systemColumnValueHandler;
	}


	public void setSystemColumnValueHandler(SystemColumnValueHandler systemColumnValueHandler) {
		this.systemColumnValueHandler = systemColumnValueHandler;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}
}
