package com.clifton.marketdata.live.theoretical;

import com.clifton.core.logging.LogUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveFieldValueCalculator;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * <code>MarketDataLiveTheoreticalValueCalculator</code> is a {@link MarketDataLiveFieldValueCalculator} that calculates
 * live market data values up for theoretically priced options.
 *
 * @author NickK
 */
public class MarketDataLiveTheoreticalValueCalculator implements MarketDataLiveFieldValueCalculator {

	private MarketDataLiveService marketDataLiveService;
	private MarketDataTheoreticalValueService marketDataTheoreticalValueService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive calculate(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand marketDataLiveCommand) {
		if (security.getReferenceSecurity() == null) {
			LogUtils.warn(getClass(), String.format("Market Data Live values cannot be looked up for theoretically priced security [%s] because it is missing a reference security.", security.getName()));
			return null;
		}

		MarketDataOptionTheoreticalValueCommand command = new MarketDataOptionTheoreticalValueCommand();
		command.setSecurity(security);
		String lowerCaseFieldName = field.getName().toLowerCase();
		MarketDataLive value;
		if (lowerCaseFieldName.contains("delta")) {
			value = getMarketDataTheoreticalValueService().getMarketDataOptionTheoreticalDelta(command);
		}
		else if (lowerCaseFieldName.contains("price")) {
			value = getMarketDataTheoreticalValueService().getMarketDataOptionTheoreticalPrice(command);
		}
		else {
			// Attempt to look the value up for the reference security
			value = getMarketDataLiveService().getMarketDataLiveFieldValue(security.getReferenceSecurity().getId(), field.getName(), marketDataLiveCommand);
		}

		if (value != null) {
			// add field name to the theoretically priced MarketDataLive
			BigDecimal roundedValue = MathUtils.round(value.asNumericValue(), field.getDecimalPrecision());
			return new MarketDataLive(value.getSecurityId(), field.getName(), field.getDataType().asDataTypeNames(), roundedValue, value.getMeasureDate());
		}
		return null;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}


	public MarketDataTheoreticalValueService getMarketDataTheoreticalValueService() {
		return this.marketDataTheoreticalValueService;
	}


	public void setMarketDataTheoreticalValueService(MarketDataTheoreticalValueService marketDataTheoreticalValueService) {
		this.marketDataTheoreticalValueService = marketDataTheoreticalValueService;
	}
}
