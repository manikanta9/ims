package com.clifton.marketdata.live.theoretical;

import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;


/**
 * <code>MarketDataOptionTheoreticalValueCommand</code> is an object containing properties for calculating theoretic
 * values for Option securities (e.g. FLEX and OTC).
 *
 * @author NickK
 */
public class MarketDataOptionTheoreticalValueCommand {

	/**
	 * The {@link InvestmentSecurity} that is to have a theoretical value calculated for.
	 * The security must have a reference security defined.
	 *
	 * @see InvestmentSecurity#referenceSecurity
	 */
	private InvestmentSecurity security;

	private Boolean call;
	private BigDecimal strikePrice;
	private OptionStyleTypes optionStyle;
	private BigDecimal yearsToMaturity;
	/**
	 * The finance rate for {@link #security} based on its expiration date
	 */
	private BigDecimal riskFreeRate;
	/**
	 * The dividend yield is a percentage payout of {@link #security}'s underlying security index or stock dividend based on spot price.
	 * It is dividend divided by underlying security spot price; where dividend is 12 month yield for the underlying security.
	 */
	private BigDecimal dividendYield;
	/**
	 * Price of the underlying security of {@link #security}
	 */
	private BigDecimal underlyingPrice;
	/**
	 * The implied volatility of {@link #security}'s reference security for calculating price
	 */
	private BigDecimal volatility;
	/**
	 * Placeholder for calculated theoretical price. May be defined to calculate another value, such as volatility.
	 */
	private BigDecimal price;
	/**
	 * Optional provider override to use when looking up necessary MarketData
	 */
	private MarketDataProviderOverrides providerOverride;
	/**
	 * Table of cash dividends containing ex date and amount of each dividend.
	 */
	private Map<Date, BigDecimal> dividendSchedule;
	/**
	 * The number of time steps to use in the binomial method calculation (i.e. height of the binomial tree).
	 */
	private Integer steps;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public MarketDataOptionTheoreticalValueCommand withSecurity(InvestmentSecurity security) {
		setSecurity(security);
		return this;
	}


	public Boolean getCall() {
		return this.call;
	}


	public void setCall(Boolean call) {
		this.call = call;
	}


	public MarketDataOptionTheoreticalValueCommand withCall(Boolean call) {
		setCall(call);
		return this;
	}


	public BigDecimal getStrikePrice() {
		return this.strikePrice;
	}


	public void setStrikePrice(BigDecimal strikePrice) {
		this.strikePrice = strikePrice;
	}


	public MarketDataOptionTheoreticalValueCommand withStrikePrice(BigDecimal strikePrice) {
		setStrikePrice(strikePrice);
		return this;
	}


	public OptionStyleTypes getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(OptionStyleTypes optionStyle) {
		this.optionStyle = optionStyle;
	}


	public MarketDataOptionTheoreticalValueCommand withOptionStyle(OptionStyleTypes optionStyle) {
		setOptionStyle(optionStyle);
		return this;
	}


	public BigDecimal getYearsToMaturity() {
		return this.yearsToMaturity;
	}


	public void setYearsToMaturity(BigDecimal yearsToMaturity) {
		this.yearsToMaturity = yearsToMaturity;
	}


	public MarketDataOptionTheoreticalValueCommand withYearsToMaturity(BigDecimal yearsToMaturity) {
		setYearsToMaturity(yearsToMaturity);
		return this;
	}


	public BigDecimal getRiskFreeRate() {
		return this.riskFreeRate;
	}


	public void setRiskFreeRate(BigDecimal riskFreeRate) {
		this.riskFreeRate = riskFreeRate;
	}


	public MarketDataOptionTheoreticalValueCommand withRiskFreeRate(BigDecimal riskFreeRate) {
		setRiskFreeRate(riskFreeRate);
		return this;
	}


	public BigDecimal getDividendYield() {
		return this.dividendYield;
	}


	public void setDividendYield(BigDecimal dividendYield) {
		this.dividendYield = dividendYield;
	}


	public MarketDataOptionTheoreticalValueCommand withDividendYield(BigDecimal dividendYield) {
		setDividendYield(dividendYield);
		return this;
	}


	public BigDecimal getUnderlyingPrice() {
		return this.underlyingPrice;
	}


	public void setUnderlyingPrice(BigDecimal underlyingPrice) {
		this.underlyingPrice = underlyingPrice;
	}


	public MarketDataOptionTheoreticalValueCommand withUnderlyingPrice(BigDecimal underlyingPrice) {
		setUnderlyingPrice(underlyingPrice);
		return this;
	}


	public BigDecimal getVolatility() {
		return this.volatility;
	}


	public void setVolatility(BigDecimal volatility) {
		this.volatility = volatility;
	}


	public MarketDataOptionTheoreticalValueCommand withVolatility(BigDecimal volatility) {
		setVolatility(volatility);
		return this;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public MarketDataOptionTheoreticalValueCommand withPrice(BigDecimal price) {
		setPrice(price);
		return this;
	}


	public MarketDataProviderOverrides getProviderOverride() {
		return this.providerOverride;
	}


	public void setProviderOverride(MarketDataProviderOverrides providerOverride) {
		this.providerOverride = providerOverride;
	}


	public Map<Date, BigDecimal> getDividendSchedule() {
		return this.dividendSchedule;
	}


	public void setDividendSchedule(Map<Date, BigDecimal> dividendSchedule) {
		this.dividendSchedule = dividendSchedule;
	}


	public MarketDataOptionTheoreticalValueCommand withDividendSchedule(Map<Date, BigDecimal> dividendSchedule) {
		setDividendSchedule(dividendSchedule);
		return this;
	}


	public Integer getSteps() {
		return this.steps;
	}


	public void setSteps(Integer steps) {
		this.steps = steps;
	}


	public MarketDataOptionTheoreticalValueCommand withSteps(int steps) {
		setSteps(steps);
		return this;
	}
}
