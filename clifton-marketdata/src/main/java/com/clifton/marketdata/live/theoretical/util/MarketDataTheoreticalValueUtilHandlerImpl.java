package com.clifton.marketdata.live.theoretical.util;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.event.InvestmentSecurityEvent;
import com.clifton.investment.instrument.event.InvestmentSecurityEventService;
import com.clifton.investment.instrument.event.search.InvestmentSecurityEventSearchForm;
import com.clifton.investment.instrument.options.InvestmentSecurityOptionTypes;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveCommand;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.live.theoretical.MarketDataOptionTheoreticalValueCommand;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * <code>MarketDataTheoreticalValueUtilHandlerImpl</code> is the utility handler implementation for preparing and calculating theoretical security values.
 *
 * @author NickK
 */
@Component
public class MarketDataTheoreticalValueUtilHandlerImpl implements MarketDataTheoreticalValueUtilHandler {

	private InvestmentSecurityEventService investmentSecurityEventService;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataLiveService marketDataLiveService;

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the calculated theoretical price for a security using Black-Scholes.
	 * <p>
	 * The provided {@link MarketDataOptionTheoreticalValueCommand} is validated for required values prior to calculation.
	 */
	@Override
	public BigDecimal getMarketDataOptionPrice(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand) {
		if (theoreticalValueCommand.getOptionStyle() == OptionStyleTypes.EUROPEAN) {
			MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand blackScholesCommand = getBlackScholesCommandForTheoreticalValueCommand(theoreticalValueCommand);
			return BigDecimal.valueOf(MarketDataTheoreticalValueUtil.getEuropeanOptionBlackScholesPrice(blackScholesCommand));
		}
		else if (theoreticalValueCommand.getOptionStyle() == OptionStyleTypes.AMERICAN) {
			MarketDataTheoreticalValueUtil.MarketDataBinomialCommand binomialCommand = getBinomialCommandForTheoreticalValueCommand(theoreticalValueCommand);
			return BigDecimal.valueOf(MarketDataTheoreticalValueUtil.getAmericanOptionBinomialPrice(binomialCommand));
		}
		throw new RuntimeException("Unable to calculate theoretical option price due to unsupported option style: " + theoreticalValueCommand.getOptionStyle());
	}


	@Override
	public BigDecimal getMarketDataOptionBlackScholesDelta(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand) {
		MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand blackScholesCommand = getBlackScholesCommandForTheoreticalValueCommand(theoreticalValueCommand);

		if (theoreticalValueCommand.getOptionStyle() == OptionStyleTypes.EUROPEAN) {
			return BigDecimal.valueOf(MarketDataTheoreticalValueUtil.getEuropeanOptionBlackScholesDelta(blackScholesCommand));
		}
		// TODO calculate non-European style options
		return BigDecimal.valueOf(MarketDataTheoreticalValueUtil.getEuropeanOptionBlackScholesDelta(blackScholesCommand));
	}


	@Override
	public void populateMarketDataOptionTheoreticalValueCommandDefaults(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand) {
		ValidationUtils.assertNotNull(theoreticalValueCommand.getSecurity(), "A valid security must be provided to get a theoretical price.");
		validateTheoreticallyPricedSecurityReferenceSecurity(theoreticalValueCommand.getSecurity());

		Date today = DateUtils.clearTime(new Date());
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getCall,
				c -> c.setCall(c.getSecurity().getOptionType() == InvestmentSecurityOptionTypes.CALL));
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getStrikePrice,
				c -> c.setStrikePrice(c.getSecurity().getOptionStrikePrice()));
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getOptionStyle,
				c -> c.setOptionStyle(c.getSecurity().getOptionStyle()));
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getYearsToMaturity,
				c -> c.setYearsToMaturity(MathUtils.divide(DateUtils.getDaysDifference(c.getSecurity().getLastDeliveryOrEndDate(), today), 365)));
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getUnderlyingPrice,
				// Theoretical option value calculations require accurate underlying spot prices; get relatively live value
				c -> {
					MarketDataField latestPriceField = getMarketDataFieldService().getMarketDataLiveLatestPriceFieldForSecurity(c.getSecurity().getUnderlyingSecurity());
					MarketDataLive liveValue = Optional.ofNullable(getMarketDataLiveFieldValue(c.getSecurity().getUnderlyingSecurity(), latestPriceField.getName(), theoreticalValueCommand.getProviderOverride(), 30))
							.orElseGet(() -> {
								MarketDataField closingPriceField = getMarketDataFieldService().getMarketDataLiveClosingPriceFieldForSecurity(c.getSecurity().getUnderlyingSecurity());
								return getMarketDataLiveFieldValue(c.getSecurity().getUnderlyingSecurity(), closingPriceField.getName(), theoreticalValueCommand.getProviderOverride(), 30);
							});
					Optional.ofNullable(liveValue)
							.map(MarketDataLive::asNumericValue)
							.ifPresent(c::setUnderlyingPrice);
				});
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getRiskFreeRate,
				c -> Optional.ofNullable(getMarketDataLiveFieldValue(c.getSecurity().getReferenceSecurity(), MarketDataField.FIELD_OPTION_FINANCE_RATE, theoreticalValueCommand.getProviderOverride(), 43200))
						.ifPresent(liveValue -> c.setRiskFreeRate(liveValue.asNumericValue())));
		defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getVolatility,
				// lookup volatility and cache for 4 hours; allows us to get an intraday update for more accurate calculations
				c -> Optional.ofNullable(getMarketDataLiveFieldValue(c.getSecurity().getReferenceSecurity(), MarketDataField.FIELD_VOLATILITY, theoreticalValueCommand.getProviderOverride(), 24400))
						.ifPresent(liveValue -> {
							BigDecimal volatility = liveValue.asNumericValue();
							if (MathUtils.isGreaterThanOrEqual(volatility, BigDecimal.ONE)) {
								// Volatility is represented as a percentage; normalize it to decimal. Markit provides volatility has decimal (e.g. 0.256) while Bloomberg provides it as a percentage (e.g. 25.6).
								volatility = MathUtils.divide(volatility, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
							}
							c.setVolatility(volatility);
						}));
		if (theoreticalValueCommand.getOptionStyle() == OptionStyleTypes.EUROPEAN) {
			defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getDividendYield,
					c -> {
						try {
							Optional.ofNullable(getMarketDataLiveFieldValue(c.getSecurity(), MarketDataField.FIELD_DIVIDEND_SCHEDULE, theoreticalValueCommand.getProviderOverride(), 43200))
									.ifPresent(liveValue -> {
										Date dayYearAgo = DateUtils.addDays(today, -365);
										Object value = liveValue.getValue();
										if (value instanceof DataTable) {
											DataTable dividendSchedule = (DataTable) value;
											BigDecimal dividend12M = CollectionUtils.getStream(dividendSchedule.getRowList())
													.filter(row -> {
														Date exDate = (Date) row.getValue("Ex Date");
														return DateUtils.isDateAfterOrEqual(exDate, dayYearAgo) && DateUtils.isDateBefore(exDate, today, false);
													})
													.map(row -> (BigDecimal) row.getValue("Amount"))
													.reduce(BigDecimal.ZERO, MathUtils::add);
											c.setDividendYield(MathUtils.divide(dividend12M, c.getUnderlyingPrice()));
										}
									});
						}
						catch (Exception e) {
							// if value lookup fails for the security, ignore it and look up security events
						}

						if (c.getDividendYield() == null) {
							// calculate dividend yield by security events
							InvestmentSecurityEventSearchForm eventSearchForm = new InvestmentSecurityEventSearchForm();
							eventSearchForm.setSecurityId(c.getSecurity().getUnderlyingSecurity().getId());
							eventSearchForm.addSearchRestriction(new SearchRestriction("exDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, DateUtils.addDays(today, -365)));
							eventSearchForm.addSearchRestriction(new SearchRestriction("exDate", ComparisonConditions.LESS_THAN_OR_EQUALS, today));
							List<InvestmentSecurityEvent> securityEventList = getInvestmentSecurityEventService().getInvestmentSecurityEventList(eventSearchForm);
							BigDecimal dividend12M = CollectionUtils.getStream(securityEventList)
									.map(InvestmentSecurityEvent::getAfterEventValue)
									.reduce(BigDecimal.ZERO, MathUtils::add);
							c.setDividendYield(MathUtils.divide(dividend12M, c.getUnderlyingPrice()));
						}
					});
		}
		else if (theoreticalValueCommand.getOptionStyle() == OptionStyleTypes.AMERICAN) {
			defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getDividendSchedule,
					c -> {
						MarketDataLiveCommand liveCommand = new MarketDataLiveCommand();
						liveCommand.setSecurityIds(c.getSecurity().getId());
						liveCommand.setFieldNames(MarketDataField.FIELD_DIVIDEND_SCHEDULE);
						liveCommand.setMaxAgeInSeconds(43200); // 12 hours
						liveCommand.setProviderOverride(MarketDataProviderOverrides.TERMINAL);
						Optional.ofNullable(getMarketDataLiveService().getMarketDataLiveFieldValuesWithCommand(liveCommand))
								.ifPresent(liveValueList -> CollectionUtils.getStream(liveValueList)
										.filter(liveValue -> c.getSecurity().getId().equals(liveValue.getSecurityId()))
										.findFirst()
										.ifPresent(liveValue -> {
											Object value = liveValue.getValue();
											if (value instanceof DataTable) {
												DataTable dividendScheduleData = (DataTable) value;
												Map<Date, BigDecimal> dividendSchedule = CollectionUtils.getStream(dividendScheduleData.getRowList())
														.filter(row -> {
															Date exDate = (Date) row.getValue("Ex Date");
															return DateUtils.isDateAfterOrEqual(exDate, today);
														}).collect(Collectors.toMap(
																row -> (Date) row.getValue("Ex Date"),
																row -> (BigDecimal) row.getValue("Amount")
														));
												c.setDividendSchedule(dividendSchedule);
											}
										}));
					});
			defaultPropertyIfNotPresent(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getSteps,
					c -> c.setSteps(c.getYearsToMaturity().multiply(BigDecimal.valueOf(365.0)).intValue())); // default to one step per day
		}
	}

	///////////////////////////////////////////////////////////////////////////


	private void validateTheoreticallyPricedSecurityReferenceSecurity(InvestmentSecurity security) {
		InvestmentSecurity referenceSecurity = security.getReferenceSecurity();
		ValidationUtils.assertNotNull(referenceSecurity, "The provided security (" + security + ") does not have a reference security defined for theoretical calculations", "referenceSecurity");
		ValidationUtils.assertTrue(DateUtils.isDateAfter(referenceSecurity.getLastDeliveryDate(), new Date()), "The provided security's (" + security + ") reference security (" + referenceSecurity + ") is expired.");
	}


	private <T> void defaultPropertyIfNotPresent(MarketDataOptionTheoreticalValueCommand command, Function<MarketDataOptionTheoreticalValueCommand, T> getter, Consumer<MarketDataOptionTheoreticalValueCommand> setter) {
		if (getter.apply(command) == null) {
			setter.accept(command);
		}
	}


	private MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand getBlackScholesCommandForTheoreticalValueCommand(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand) {
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getCall, () -> "call");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getStrikePrice, () -> "strikePrice");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getYearsToMaturity, () -> "yearsToMaturity");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getUnderlyingPrice, () -> "underlyingPrice");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getRiskFreeRate, () -> "riskFreeRate");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getDividendYield, () -> "dividendYield");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getVolatility, () -> "volatility");

		ValidationUtils.assertTrue(MathUtils.isGreaterThan(theoreticalValueCommand.getYearsToMaturity(), BigDecimal.ZERO), "Unable to calculate a Black-Scholes price for an expired security: " + theoreticalValueCommand.getSecurity());

		return MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand.createFromMarketDataOptionTheoreticalValueCommand(theoreticalValueCommand);
	}


	private MarketDataTheoreticalValueUtil.MarketDataBinomialCommand getBinomialCommandForTheoreticalValueCommand(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand) {
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getCall, () -> "call");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getStrikePrice, () -> "strikePrice");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getYearsToMaturity, () -> "yearsToMaturity");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getUnderlyingPrice, () -> "underlyingPrice");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getRiskFreeRate, () -> "riskFreeRate");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getVolatility, () -> "volatility");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getDividendSchedule, () -> "dividendSchedule");
		validateRequiredProperty(theoreticalValueCommand, MarketDataOptionTheoreticalValueCommand::getSteps, () -> "steps");


		ValidationUtils.assertTrue(MathUtils.isGreaterThan(theoreticalValueCommand.getYearsToMaturity(), BigDecimal.ZERO), "Unable to calculate a Black-Scholes price for an expired security: " + theoreticalValueCommand.getSecurity());

		return MarketDataTheoreticalValueUtil.MarketDataBinomialCommand.createFromMarketDataOptionTheoreticalValueCommand(theoreticalValueCommand);
	}


	private <T> void validateRequiredProperty(MarketDataOptionTheoreticalValueCommand command, Function<MarketDataOptionTheoreticalValueCommand, T> getter, Supplier<String> fieldNameSupplier) {
		ValidationUtils.assertNotNull(getter.apply(command), "Unable to calculate theoretical value because a required property is missing and could not be defaulted", fieldNameSupplier.get());
	}


	private MarketDataLive getMarketDataLiveFieldValue(InvestmentSecurity security, String fieldName, MarketDataProviderOverrides providerOverride, int maxAgeInSeconds) {
		MarketDataLiveCommand liveCommand = new MarketDataLiveCommand();
		liveCommand.setSecurityIds(security.getId());
		liveCommand.setFieldNames(fieldName);
		liveCommand.setMaxAgeInSeconds(maxAgeInSeconds);
		liveCommand.setProviderOverride(providerOverride);
		List<MarketDataLive> liveValueList = getMarketDataLiveService().getMarketDataLiveFieldValuesWithCommand(liveCommand);
		return CollectionUtils.getStream(liveValueList)
				.filter(liveValue -> fieldName.equals(liveValue.getFieldName()))
				.findFirst()
				.orElse(null);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentSecurityEventService getInvestmentSecurityEventService() {
		return this.investmentSecurityEventService;
	}


	public void setInvestmentSecurityEventService(InvestmentSecurityEventService investmentSecurityEventService) {
		this.investmentSecurityEventService = investmentSecurityEventService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public MarketDataLiveService getMarketDataLiveService() {
		return this.marketDataLiveService;
	}


	public void setMarketDataLiveService(MarketDataLiveService marketDataLiveService) {
		this.marketDataLiveService = marketDataLiveService;
	}
}
