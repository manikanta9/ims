package com.clifton.marketdata.live.theoretical;

import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.theoretical.util.MarketDataTheoreticalValueUtilHandler;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author NickK
 */
@Service
public class MarketDataTheoreticalValueServiceImpl implements MarketDataTheoreticalValueService {

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataTheoreticalValueUtilHandler marketDataTheoreticalValueUtilHandler;

	///////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive getMarketDataOptionTheoreticalPrice(MarketDataOptionTheoreticalValueCommand command) {
		populateCommandForTheoreticalValueProcessing(command);
		BigDecimal price = getMarketDataTheoreticalValueUtilHandler().getMarketDataOptionPrice(command);
		ValidationUtils.assertNotNull(price, "Unable to calculate theoretical value for security " + command.getSecurity());
		return new MarketDataLive(command.getSecurity().getId(), price, new Date());
	}


	@Override
	public MarketDataLive getMarketDataOptionTheoreticalDelta(MarketDataOptionTheoreticalValueCommand command) {
		populateCommandForTheoreticalValueProcessing(command);
		BigDecimal delta = getMarketDataTheoreticalValueUtilHandler().getMarketDataOptionBlackScholesDelta(command);
		ValidationUtils.assertNotNull(delta, "Unable to calculate theoretical delta for security " + command.getSecurity());
		return new MarketDataLive(command.getSecurity().getId(), delta, new Date());
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void populateCommandForTheoreticalValueProcessing(MarketDataOptionTheoreticalValueCommand command) {
		if (command.getSecurity() != null && command.getSecurity().getId() != null) {
			// ensure security is fully populated
			command.setSecurity(getInvestmentInstrumentService().getInvestmentSecurity(command.getSecurity().getId()));
		}
		ValidationUtils.assertNotNull(command.getSecurity(), "A valid security must be provided to get a theoretical price.");
		getMarketDataTheoreticalValueUtilHandler().populateMarketDataOptionTheoreticalValueCommandDefaults(command);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataTheoreticalValueUtilHandler getMarketDataTheoreticalValueUtilHandler() {
		return this.marketDataTheoreticalValueUtilHandler;
	}


	public void setMarketDataTheoreticalValueUtilHandler(MarketDataTheoreticalValueUtilHandler marketDataTheoreticalValueUtilHandler) {
		this.marketDataTheoreticalValueUtilHandler = marketDataTheoreticalValueUtilHandler;
	}
}
