package com.clifton.marketdata.live;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataLiveExchangeRate</code> class represents immutable instance of a
 * market data exchange rate value (from - to currency) at the specified measure date.
 * <p>
 * It extends the MarketDataLive object, with an added field noting the securityId exchange rate value to which currency securityId
 *
 * @author manderson
 */
public class MarketDataLiveExchangeRate extends MarketDataLive {

	private final int toSecurityId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataLiveExchangeRate(int fromSecurityId, int toSecurityId, BigDecimal value, Date measureDate) {
		super(fromSecurityId, value, measureDate);
		this.toSecurityId = toSecurityId;
	}


	public int getToSecurityId() {
		return this.toSecurityId;
	}
}
