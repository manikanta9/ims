package com.clifton.marketdata.live;


import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValueGeneric;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataLive</code> class represents immutable instance of a
 * market data value (price, volume, etc.) at the specified measure date.
 *
 * @author vgomelsky
 */
public class MarketDataLive implements Serializable {

	private final int securityId;
	private final String fieldName;
	/**
	 * The {@link DataTypeNames} of the value. The data type is taken from the {@link com.clifton.marketdata.field.MarketDataField} the value is for.
	 * If this field is null, the value is assumed to be numeric
	 */
	private final DataTypeNames dataTypeName;
	/**
	 * The generic value of this market data value reference. The type is determined from {@link #getDataTypeName()}.
	 * If the value is serialized from IMS, it will be either the numeric or string representation.
	 */
	private final Object value;
	private final Date measureDate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Shorthand constructor for numeric values. The value must be either {@link BigDecimal} or {@link Integer}.
	 * This exists for backward compatibility for numeric values to avoid looking up the {@link DataTypeNames}.
	 *
	 * @throws RuntimeException when value is null
	 */
	public MarketDataLive(int securityId, Object value, Date measureDate) {
		this(securityId, null, null, value, measureDate);
	}


	/**
	 * Constructor for defining the {@link DataTypeNames} for the value.
	 *
	 * @throws RuntimeException when value is null
	 */
	public MarketDataLive(int securityId, String fieldName, DataTypeNames dataTypeName, Object value, Date measureDate) {
		this.securityId = securityId;
		this.fieldName = fieldName;
		this.dataTypeName = dataTypeName;
		AssertUtils.assertNotNull(value, "Cannot create a Market Data Live object with a null value");
		this.value = value;
		this.measureDate = measureDate;
	}


	public static MarketDataLive ofMarketDataValue(MarketDataValueGeneric<?> marketDataValue) {
		MarketDataField field = marketDataValue.getDataField();
		DataTypeNames dataTypeName = field.getDataType().asDataTypeNames();
		return new MarketDataLive(marketDataValue.getInvestmentSecurity().getId(), field.getName(), dataTypeName, marketDataValue.getMeasureValue(), marketDataValue.getMeasureDateWithTime());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getSecurityId() {
		return this.securityId;
	}


	/**
	 * The name of the field the value is for.
	 * <p>
	 * The field name may be null if this object is numeric and the request did not specify a field name such as with getting security live prices.
	 */
	public String getFieldName() {
		return this.fieldName;
	}


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}


	public boolean isNumeric() {
		return getDataTypeName() == null ? (this.value instanceof Number) : getDataTypeName().isNumeric();
	}


	/**
	 * Returns the value that can be converted based on the data type returned from {@link #getDataTypeName()}.
	 * If the data type is null, it is numeric and you can use {@link #asNumericValue()}.
	 */
	public Object getValue() {
		return this.value;
	}


	/**
	 * Returns the {@link #getValue()} value as a BigDecimal. If the value is null or {@link #isNumeric()} is false, returns null.
	 * <p>
	 * The method name does not follow our standard getter syntax to avoid serialization.
	 */
	public BigDecimal asNumericValue() {
		ValidationUtils.assertTrue(isNumeric(), () -> "Value [" + getValue() + "] is not numeric");
		// Only two numeric data types are supported: INTEGER and DECIMAL
		return (this.value instanceof Integer) ? new BigDecimal((Integer) this.value) : (BigDecimal) this.value;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}
}
