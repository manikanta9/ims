package com.clifton.marketdata.live.theoretical.util;

import com.clifton.marketdata.live.theoretical.MarketDataOptionTheoreticalValueCommand;

import java.math.BigDecimal;


/**
 * <code>MarketDataTheoreticalValueUtilHandler</code> is an interface defining utility methods used for preparing  and calculating theoretical security values.
 *
 * @author NickK
 */
public interface MarketDataTheoreticalValueUtilHandler {

	/**
	 * Populates the provided {@link MarketDataOptionTheoreticalValueCommand} with defaults for those properties that are currently null.
	 */
	public void populateMarketDataOptionTheoreticalValueCommandDefaults(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand);


	/**
	 * Returns the calculated price for the provided {@link MarketDataOptionTheoreticalValueCommand} based upon the {@link MarketDataOptionTheoreticalValueCommand#optionStyle}.
	 */
	public BigDecimal getMarketDataOptionPrice(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand);


	/**
	 * Returns the Black-Scholes calculated delta for the provided {@link MarketDataOptionTheoreticalValueCommand}.
	 */
	public BigDecimal getMarketDataOptionBlackScholesDelta(MarketDataOptionTheoreticalValueCommand theoreticalValueCommand);
}
