package com.clifton.marketdata.live;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.rates.MarketDataExchangeRate;

import java.util.List;


/**
 * The {@link MarketDataLiveService} interface declares methods used to retrieve <i>live</i> market data. "Live" market data is market data that is retrieved within a certain
 * configurable time span.
 * <p>
 * Several methods declared by this interface use the {@link MarketDataLiveCommand} parameter object to compose arguments such as the {@link MarketDataLiveCommand#maxAgeInSeconds
 * maximum age of retrieved data} and a {@link MarketDataLiveCommand#providerOverride provider override}. In order to improve performance, the {@link
 * MarketDataLiveCommand#maxAgeInSeconds maximum age} argument may be increased above the {@link #DEFAULT_AGE_IN_SECONDS default value}. This will increase the maximum allowed age
 * for values queried from the existing cache.
 * <p>
 * <b>Note: Live data is retrieved from the cache or the data provider directly and is not persisted to the database.</b>
 *
 * @author vgomelsky
 */
public interface MarketDataLiveService {

	/**
	 * If 'maxAgeInSeconds" parameter is not passed, use this value to determine whether "live" data
	 * is retrieved from cache (newer than this value) or it's truly live data.
	 */
	public static int DEFAULT_AGE_IN_SECONDS = 30 * 60; // 30 minutes


	//////////////////////////////////////////////////////////////////////////// 
	////////                   Latest Price Methods                    ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the latest price for the specified {@link InvestmentSecurity} ID. A {@link MarketDataLiveCommand} may optionally be included to specify parameters such as a {@link
	 * MarketDataLiveCommand#maxAgeInSeconds maximum age of retrieved data} or a {@link MarketDataLiveCommand#providerOverride provider override}.
	 * <p>
	 * <b>Note: Avoid using small values for {@link MarketDataLiveCommand#maxAgeInSeconds maximum age} to improve performance and scalability.</b>
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public MarketDataLive getMarketDataLivePrice(int securityId, MarketDataLiveCommand command);


	/**
	 * Returns the latest price for each {@link InvestmentSecurity} ID included in the provided command. A {@link MarketDataLiveCommand} may optionally be included to specify
	 * parameters such as a {@link MarketDataLiveCommand#maxAgeInSeconds maximum age of retrieved data} or a {@link MarketDataLiveCommand#providerOverride provider override}.
	 * <p>
	 * <b>Note: Avoid using small values for {@link MarketDataLiveCommand#maxAgeInSeconds maximum age} to improve performance and scalability.</b>
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public List<MarketDataLive> getMarketDataLivePricesWithCommand(MarketDataLiveCommand command);


	////////////////////////////////////////////////////////////////////////////
	/////                   Latest Field Value Methods                    ////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the latest field value for the specified {@link InvestmentSecurity} ID and {@link MarketDataField} name. A {@link MarketDataLiveCommand} may optionally be included
	 * to specify parameters such as a {@link MarketDataLiveCommand#maxAgeInSeconds maximum age of retrieved data} or a {@link MarketDataLiveCommand#providerOverride provider
	 * override}.
	 * <p>
	 * <b>Note: Avoid using small values for {@link MarketDataLiveCommand#maxAgeInSeconds maximum age} to improve performance and scalability.</b>
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public MarketDataLive getMarketDataLiveFieldValue(int securityId, String fieldName, MarketDataLiveCommand command);


	/**
	 * Returns the latest field value for the specified {@link InvestmentSecurity} ID and {@link MarketDataField} name. A {@link MarketDataLiveCommand} may optionally be included
	 * to specify parameters such as a {@link MarketDataLiveCommand#maxAgeInSeconds maximum age of retrieved data} or a {@link MarketDataLiveCommand#providerOverride provider
	 * override}.
	 * <p>
	 * No guarantees are made as to the order of the returned values.
	 * <p>
	 * <b>Note: Avoid using small values for {@link MarketDataLiveCommand#maxAgeInSeconds maximum age} to improve performance and scalability.</b>
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public List<MarketDataLive> getMarketDataLiveFieldValuesWithCommand(MarketDataLiveCommand command);

	//////////////////////////////////////////////////////////////////////////// 
	////////              Latest Exchange Rate Methods                 ///////// 
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns latest exchange rate from the fromCurrency to the toCurrency.  The exchange rate will be no older than the
	 * specified maxAgeInSeconds.  This argument is optional and if null, will use service's default value.
	 * <p>
	 * NOTE: avoid using small values for maxAgeInSeconds to improve performance and scalability.
	 */
	@SecureMethod(dtoClass = MarketDataExchangeRate.class)
	public MarketDataLiveExchangeRate getMarketDataLiveExchangeRate(int fromCurrencyId, int toCurrencyId, Integer maxAgeInSeconds);
}
