package com.clifton.marketdata.live;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.marketdata.provider.MarketDataProviderOverrides;


/**
 * <code>MarketDataLiveCommand</code> is a command object for making requests to the {@link MarketDataLive} service.
 *
 * @author NickK
 */
@NonPersistentObject
public class MarketDataLiveCommand {

	/**
	 * The security ID(s) to find {@link com.clifton.marketdata.field.MarketDataField} value(s) for.
	 */
	private int[] securityIds;
	/**
	 * The {@link com.clifton.marketdata.field.MarketDataField} name(s) to look up value(s) for.
	 */
	private String[] fieldNames;

	/**
	 * InvestmentExchange ID to specify the of the exchange used for pricing.
	 */
	private Short exchangeId;
	/**
	 * Market sector name that specifies the Market Sector of the security.  Used in conjunction with the exchange name so a proper symbol my be built for live data lookups.
	 */
	private String marketSectorName;
	/**
	 * Optional max age in seconds of the value to look up. If null, the {@link MarketDataLiveServiceImpl#DEFAULT_AGE_IN_SECONDS} is used.
	 */
	private Integer maxAgeInSeconds;
	/**
	 * The optional provider override to use when making a request for {@link MarketDataLive} values.
	 */
	private MarketDataProviderOverrides providerOverride;
	/**
	 * The optional timeout override (in milliseconds) to use when making a request for {@link MarketDataLive} values.
	 */
	private Integer providerTimeoutOverride;
	/**
	 * Optional flag to ignore errors for continuing to process live values. If an error occurs when this value is true, the error will be logged instead of thrown.
	 */
	private boolean ignoreErrors;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataLiveCommand() {
		// Default constructor
	}


	public MarketDataLiveCommand(MarketDataLiveCommand other) {
		this.securityIds = other.securityIds;
		this.fieldNames = other.fieldNames;
		this.exchangeId= other.exchangeId;
		this.marketSectorName = other.marketSectorName;
		this.maxAgeInSeconds = other.maxAgeInSeconds;
		this.providerOverride = other.providerOverride;
		this.providerTimeoutOverride = other.providerTimeoutOverride;
		this.ignoreErrors = other.ignoreErrors;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Custom setter used to allow varargs instead of requiring array literals
	public void setSecurityIds(int... securityIds) {
		this.securityIds = securityIds;
	}


	// Custom setter used to allow varargs instead of requiring array literals
	public void setFieldNames(String... fieldNames) {
		this.fieldNames = fieldNames;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public int[] getSecurityIds() {
		return this.securityIds;
	}


	public String[] getFieldNames() {
		return this.fieldNames;
	}


	public Short getExchangeId() {
		return this.exchangeId;
	}


	public void setExchangeId(Short exchangeId) {
		this.exchangeId = exchangeId;
	}


	public String getMarketSectorName() {
		return this.marketSectorName;
	}


	public void setMarketSectorName(String marketSectorName) {
		this.marketSectorName = marketSectorName;
	}


	public Integer getMaxAgeInSeconds() {
		return this.maxAgeInSeconds;
	}


	public void setMaxAgeInSeconds(Integer maxAgeInSeconds) {
		this.maxAgeInSeconds = maxAgeInSeconds;
	}


	public MarketDataProviderOverrides getProviderOverride() {
		return this.providerOverride;
	}


	public void setProviderOverride(MarketDataProviderOverrides providerOverride) {
		this.providerOverride = providerOverride;
	}


	public Integer getProviderTimeoutOverride() {
		return this.providerTimeoutOverride;
	}


	public void setProviderTimeoutOverride(Integer providerTimeoutOverride) {
		this.providerTimeoutOverride = providerTimeoutOverride;
	}


	public boolean isIgnoreErrors() {
		return this.ignoreErrors;
	}


	public void setIgnoreErrors(boolean ignoreErrors) {
		this.ignoreErrors = ignoreErrors;
	}
}
