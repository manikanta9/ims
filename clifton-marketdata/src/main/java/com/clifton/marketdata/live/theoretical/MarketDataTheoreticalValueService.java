package com.clifton.marketdata.live.theoretical;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.live.MarketDataLive;


/**
 * <code>MarketDataTheoreticalValueService</code> is a service interface for calculating theoretical security values.
 *
 * @author NickK
 */
public interface MarketDataTheoreticalValueService {

	/**
	 * Returns a {@link MarketDataLive} price for the provided {@link MarketDataOptionTheoreticalValueCommand}. If the theoretically
	 * priced Option in the command is a European style, the price is calculated using Black-Scholes.
	 * <p>
	 * If required properties of the command are not defined, an attempt will be made to default them prior to validating and calculating the value.
	 * Default values are looked up from values in the system based on previous day or caches.
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public MarketDataLive getMarketDataOptionTheoreticalPrice(MarketDataOptionTheoreticalValueCommand command);


	/**
	 * Returns a {@link MarketDataLive} delta for the provided {@link MarketDataOptionTheoreticalValueCommand}. If the theoretically
	 * priced Option in the command is a European style, the price is calculated using Black-Scholes.
	 * <p>
	 * If required properties of the command are not defined, an attempt will be made to default them prior to validating and calculating the value.
	 * Default values are looked up from values in the system based on previous day or caches.
	 */
	@SecureMethod(dtoClass = MarketDataValue.class)
	public MarketDataLive getMarketDataOptionTheoreticalDelta(MarketDataOptionTheoreticalValueCommand command);
}
