package com.clifton.marketdata.live;

import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;


/**
 * <code>MarketDataLiveFieldValueCalculator</code> is a custom calculator {@link com.clifton.system.bean.SystemBeanGroup}
 * capable of calculating or retrieving {@link MarketDataLive} field values as a n alternative to using the standard {@link com.clifton.marketdata.provider.MarketDataProvider}.
 * <p>
 * A use case for this is with the projected Stock Ex Date and Dividends. The calculator obtains a matrix of projected values that can be
 * used for projecting the next dividend Ex Date and corresponding dividend for a Stock and also projected dividends for the remaining live of an option.
 *
 * @author NickK
 */
public interface MarketDataLiveFieldValueCalculator {

	public MarketDataLive calculate(InvestmentSecurity security, MarketDataField field, MarketDataLiveCommand command);
}
