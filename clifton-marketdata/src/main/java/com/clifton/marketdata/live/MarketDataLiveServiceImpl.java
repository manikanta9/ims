package com.clifton.marketdata.live;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.exchange.InvestmentExchange;
import com.clifton.investment.exchange.InvestmentExchangeService;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.stream.Collectors;


/**
 * The <code>MarketDataLiveServiceImpl</code> class provides MarketDataProvider based market data look ups.
 * Uses caching in order to improve performance and scalability when possible.
 *
 * @author vgomelsky
 */
@Service
public class MarketDataLiveServiceImpl implements MarketDataLiveService {

	public static final String CACHE_LIVE_VALUE = "MARKET-DATA-LIVE-VALUE";
	public static final String CACHE_LIVE_EXCHANGE_RATE = "MARKET-DATA-LIVE-EXCHANGE-RATE";

	private static final String CACHE_LIVE_PRICE_FIELD_NAME = "Live Price";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CacheHandler<Object, MarketDataLive> cacheHandler;

	private InvestmentExchangeService investmentExchangeService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataProviderLocator marketDataProviderLocator;
	private MarketDataFieldService marketDataFieldService;
	private MarketDataRatesService marketDataRatesService;
	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////                   Latest Price Methods                    /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive getMarketDataLivePrice(int securityId, MarketDataLiveCommand command) {
		MarketDataLiveCommand marketDataLiveCommand = command != null ? new MarketDataLiveCommand(command) : new MarketDataLiveCommand();
		marketDataLiveCommand.setSecurityIds(securityId);
		List<MarketDataLive> results = getMarketDataLivePricesWithCommand(marketDataLiveCommand);
		return CollectionUtils.getFirstElement(results);
	}


	@Override
	public List<MarketDataLive> getMarketDataLivePricesWithCommand(MarketDataLiveCommand command) {
		List<MarketDataLive> result = new ArrayList<>();
		if (!ArrayUtils.isEmpty(command.getSecurityIds())) {
			// Look up the values for each security from cache. For securities without cached values, look up their values
			// from the underlying service in batches and cache the results.
			List<InvestmentSecurity> securityListNotCached = populateSecurityLiveValuesFromCache(result, command.getSecurityIds(), id -> getMarketDataLivePriceFromCache(id, command.getExchangeId(), command.getMaxAgeInSeconds()));
			if (!securityListNotCached.isEmpty()) {
				Consumer<MarketDataLive> cachePopulator = live -> cacheMarketDataLivePrice(live.getSecurityId(), command.getExchangeId(), live);
				// get live prices for each security - to reduce the number of requests made, requests are made in batches such that
				// each unique price field will result in a request containing the field and a list of securities using the field
				Map<InvestmentSecurity, List<MarketDataField>> securityToLatestPriceFieldListMap = CollectionUtils.getMapped(securityListNotCached, security -> Collections.singletonList(getMarketDataFieldService().getMarketDataLiveLatestPriceFieldForSecurity(security)));
				Map<InvestmentSecurity, List<MarketDataField>> securityToMissingFieldListMap = populateSecurityLiveValuesForSecurityToFieldListMap(result, securityToLatestPriceFieldListMap, cachePopulator, command);
				Collection<InvestmentSecurity> securityListWithNoLastPrice = securityToMissingFieldListMap.keySet();
				if (!CollectionUtils.isEmpty(securityListWithNoLastPrice)) {
					// filter and get by closing price field - when the market is closed may return no value: look up last closing price then
					Map<InvestmentSecurity, List<MarketDataField>> securityToClosingPriceFieldListMap = new HashMap<>();
					for (InvestmentSecurity security : securityListWithNoLastPrice) {
						MarketDataField closingPriceField = getMarketDataFieldService().getMarketDataLiveClosingPriceFieldForSecurity(security);
						// Don't look it up again if the fields are the same
						if (!Objects.equals(CollectionUtils.getOnlyElement(securityToLatestPriceFieldListMap.get(security)), closingPriceField)) {
							securityToClosingPriceFieldListMap.put(security, Collections.singletonList(closingPriceField));
						}
					}
					populateSecurityLiveValuesForSecurityToFieldListMap(result, securityToClosingPriceFieldListMap, cachePopulator, command);
				}
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Latest Field Value Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLive getMarketDataLiveFieldValue(int securityId, String fieldName, MarketDataLiveCommand command) {
		MarketDataLiveCommand marketDataLiveCommand = command != null ? new MarketDataLiveCommand(command) : new MarketDataLiveCommand();
		marketDataLiveCommand.setSecurityIds(securityId);
		marketDataLiveCommand.setFieldNames(fieldName);
		List<MarketDataLive> results = getMarketDataLiveFieldValuesWithCommand(marketDataLiveCommand);
		return CollectionUtils.getFirstElement(results);
	}


	@Override
	public List<MarketDataLive> getMarketDataLiveFieldValuesWithCommand(MarketDataLiveCommand command) {
		AssertUtils.assertFalse(ArrayUtils.isEmpty(command.getSecurityIds()), "At least one Security ID is required to get a Market Data Value");
		AssertUtils.assertFalse(ArrayUtils.isEmpty(command.getFieldNames()), "At least one Market Data Field Name or ID is required to get a Market Data Value");

		List<MarketDataLive> result = new ArrayList<>();
		// Look up the values for each security from cache. For securities without cached values, look up their values
		// from the underlying service in batches and cache the results.
		Consumer<MarketDataLive> cachePopulator = live -> cacheMarketDataLiveFieldValue(live.getSecurityId(), command.getExchangeId(), live.getFieldName(), live);
		Map<InvestmentSecurity, List<MarketDataField>> securityToNonCachedFieldListMap = new HashMap<>();
		for (String fieldName : command.getFieldNames()) {
			IntFunction<MarketDataLive> cacheExtractor = securityId -> getMarketDataLiveFieldValueFromCache(securityId, command.getExchangeId(), fieldName, command.getMaxAgeInSeconds());
			List<InvestmentSecurity> securityListNotCached = populateSecurityLiveValuesFromCache(result, command.getSecurityIds(), cacheExtractor);
			if (!securityListNotCached.isEmpty()) {
				MarketDataField marketDataField = getMarketDataFieldService().getMarketDataFieldByName(fieldName);
				for (InvestmentSecurity security : securityListNotCached) {
					securityToNonCachedFieldListMap.computeIfAbsent(security, s -> new ArrayList<>()).add(marketDataField);
				}
			}
		}
		populateSecurityLiveValuesForSecurityToFieldListMap(result, securityToNonCachedFieldListMap, cachePopulator, command);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Latest Exchange Rate Methods                 /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MarketDataLiveExchangeRate getMarketDataLiveExchangeRate(int fromCurrencyId, int toCurrencyId, Integer maxAgeInSeconds) {
		MarketDataLiveExchangeRate result = getMarketDataLiveExchangeRateFromCache(fromCurrencyId, toCurrencyId, maxAgeInSeconds);
		if (result == null) {
			InvestmentSecurity fromCurrency = getInvestmentInstrumentService().getInvestmentSecurity(fromCurrencyId);
			InvestmentSecurity toCurrency = getInvestmentInstrumentService().getInvestmentSecurity(toCurrencyId);
			MarketDataProvider<MarketDataValue> dataProvider = getMarketDataProviderLocator().locate(null);
			MarketDataExchangeRate value = dataProvider.getMarketDataExchangeRateLatest(fromCurrency, toCurrency);
			if (value == null) {
				// when the market is closed may return no value: look up last value in Market Data
				value = getMarketDataRatesService().getMarketDataExchangeRateForDataSourceAndDateFlexible(null, fromCurrencyId, toCurrencyId, new Date());
			}

			if (value != null) {
				result = new MarketDataLiveExchangeRate(fromCurrencyId, toCurrencyId, value.getExchangeRate(), value.getRateDate());
			}
			cacheMarketDataLiveExchangeRate(fromCurrencyId, toCurrencyId, result);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                      Helper Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Looks up a cached {@link MarketDataLive} value for each security ID using the provided cache extraction function.
	 * If a value is returned from the cache extracting function, the value is added to the the provided list of values.
	 * If there is not a value returned from the cache extracting function, the {@link InvestmentSecurity} for the security
	 * ID is looked up and added to a list of non-cached securities that will be returned.
	 *
	 * @param valueListToPopulate the list of values to add cached values to
	 * @param securityIds         the list of security IDs to look up cached values for
	 * @param cacheExtractor      the function to provide a security id to to obtain a cached value
	 * @return the list of {@link InvestmentSecurity}s that did not return a value from the cache extracting function
	 */
	private List<InvestmentSecurity> populateSecurityLiveValuesFromCache(List<MarketDataLive> valueListToPopulate, int[] securityIds, IntFunction<MarketDataLive> cacheExtractor) {
		return Arrays.stream(securityIds)
				.distinct()
				.filter(id -> {
					MarketDataLive cachedResult = cacheExtractor.apply(id);
					if (cachedResult != null) {
						valueListToPopulate.add(cachedResult);
						return false;
					}
					return true;
				})
				.mapToObj(id -> getInvestmentInstrumentService().getInvestmentSecurity(id))
				.collect(Collectors.toList());
	}


	/**
	 * Populates the given <code>valueListToPopulate</code> with {@link MarketDataLive} values for all of the given calculated {@link MarketDataField fields} for each {@link
	 * InvestmentSecurity}. This returns the map of securities to fields for which values could not be retrieved.
	 */
	private Map<InvestmentSecurity, List<MarketDataField>> populateSecurityLiveValuesForSecurityToFieldListMap(List<MarketDataLive> valueListToPopulate, Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListMap, Consumer<MarketDataLive> cachePopulator, MarketDataLiveCommand command) {
		// Partition calculated and non-calculated fields per security
		Map<InvestmentSecurity, List<MarketDataField>> securityToCalculatedFieldListMap = new HashMap<>();
		Map<InvestmentSecurity, List<MarketDataField>> securityToRetrievedFieldListMap = new HashMap<>();
		securityToFieldListMap.forEach((security, fieldList) -> {
			for (MarketDataField field : fieldList) {
				boolean useCalculator = field.getLiveValueCalculator() != null
						&& field.getLiveValueCalculatorCondition() != null
						&& getSystemConditionEvaluationHandler().isConditionTrue(field.getLiveValueCalculatorCondition(), security);
				Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListTrackingMap = useCalculator
						? securityToCalculatedFieldListMap
						: securityToRetrievedFieldListMap;
				securityToFieldListTrackingMap.computeIfAbsent(security, s -> new ArrayList<>()).add(field);
			}
		});

		// Apply command defaults
		MarketDataLiveCommand marketDataLiveCommand = new MarketDataLiveCommand(command);
		if (command.getMaxAgeInSeconds() == null) {
			marketDataLiveCommand.setMaxAgeInSeconds(DEFAULT_AGE_IN_SECONDS);
		}

		// Get calculated and retrieved values
		Map<InvestmentSecurity, List<MarketDataField>> securityToMissingFieldListMap = new HashMap<>();
		securityToMissingFieldListMap.putAll(populateCalculatedSecurityFieldValueList(valueListToPopulate, securityToCalculatedFieldListMap, cachePopulator, marketDataLiveCommand));
		securityToMissingFieldListMap.putAll(populatedRetrievedSecurityFieldValueList(valueListToPopulate, securityToRetrievedFieldListMap, cachePopulator, marketDataLiveCommand));
		return securityToMissingFieldListMap;
	}


	/**
	 * Populates the given <code>valueListToPopulate</code> with {@link MarketDataLive} values for all of the given calculated {@link MarketDataField fields} for each {@link
	 * InvestmentSecurity}. This returns the map of securities to fields for which values could not be retrieved.
	 */
	private Map<InvestmentSecurity, List<MarketDataField>> populateCalculatedSecurityFieldValueList(List<MarketDataLive> valueListToPopulate, Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListMap, Consumer<MarketDataLive> cachePopulator, MarketDataLiveCommand command) {
		Map<InvestmentSecurity, List<MarketDataField>> securityToMissingFieldListMap = new HashMap<>();
		Map<MarketDataField, MarketDataLiveFieldValueCalculator> fieldToCalculatorMap = new HashMap<>();
		for (Map.Entry<InvestmentSecurity, List<MarketDataField>> entry : securityToFieldListMap.entrySet()) {
			InvestmentSecurity security = entry.getKey();
			for (MarketDataField field : entry.getValue()) {
				try {
					MarketDataLiveFieldValueCalculator calculator = fieldToCalculatorMap.computeIfAbsent(field, mdf -> (MarketDataLiveFieldValueCalculator) getSystemBeanService().getBeanInstance(mdf.getLiveValueCalculator()));
					MarketDataLive value = calculator.calculate(security, field, command);
					if (value != null) {
						cachePopulator.accept(value);
						valueListToPopulate.add(value);
					}
					else {
						securityToMissingFieldListMap.computeIfAbsent(security, s -> new ArrayList<>()).add(field);
					}
				}
				catch (Exception e) {
					String errorMessage = String.format("Live field value lookup from calculator failed for field [%s] and security [%s].", field, security);
					if (command.isIgnoreErrors()) {
						LogUtils.warn(getClass(), errorMessage, e);
					}
					else {
						throw new RuntimeException(errorMessage, e);
					}
				}
			}
		}
		return securityToMissingFieldListMap;
	}


	/**
	 * Populates the given <code>valueListToPopulate</code> with {@link MarketDataLive} values for all of the given calculated {@link MarketDataField fields} for each {@link
	 * InvestmentSecurity}. This returns the map of securities to fields for which values could not be retrieved.
	 */
	private Map<InvestmentSecurity, List<MarketDataField>> populatedRetrievedSecurityFieldValueList(List<MarketDataLive> valueListToPopulate, Map<InvestmentSecurity, List<MarketDataField>> securityToRetrievedFieldListMap, Consumer<MarketDataLive> cachePopulator, MarketDataLiveCommand command) {
		Map<InvestmentSecurity, List<MarketDataField>> securityToMissingRetrievedFieldListMap = new HashMap<>();
		try {
			DataFieldValueResponse<MarketDataValueGeneric<?>> dataValueResponse = getMarketDataValueLatestForSecurityToFieldListMap(securityToRetrievedFieldListMap, command);
			for (Map.Entry<InvestmentSecurity, List<MarketDataField>> entry : securityToRetrievedFieldListMap.entrySet()) {
				InvestmentSecurity security = entry.getKey();
				for (MarketDataField field : entry.getValue()) {
					MarketDataValueGeneric<?> value = dataValueResponse.getMarketDataValue(security, field);
					if (!appendAndCacheRetrievedLiveValue(valueListToPopulate, cachePopulator, value)) {
						securityToMissingRetrievedFieldListMap.computeIfAbsent(security, s -> new ArrayList<>()).add(field);
					}
				}
			}
		}
		catch (Exception e) {
			StringJoiner sj = new StringJoiner("\n\t", "Live field value lookup failed for security/field combinations:\n\t", StringUtils.EMPTY_STRING);
			securityToRetrievedFieldListMap.forEach((security, fieldListForSecurity) -> sj.add(String.format("[%s]: %s", security.getLabel(), fieldListForSecurity)));
			String errorMessage = sj.toString();
			if (command.isIgnoreErrors()) {
				LogUtils.warn(getClass(), errorMessage, e);
			}
			else {
				throw new RuntimeException(errorMessage, e);
			}
		}
		return securityToMissingRetrievedFieldListMap;
	}


	/**
	 * Retrieves a data response containing market data values for each given {@link MarketDataField} for each given {@link InvestmentSecurity}.
	 */
	private DataFieldValueResponse<MarketDataValueGeneric<?>> getMarketDataValueLatestForSecurityToFieldListMap(Map<InvestmentSecurity, List<MarketDataField>> securityToFieldListMap, MarketDataLiveCommand command) {
		MarketDataProvider<MarketDataValueGeneric<?>> dataProvider = getMarketDataProviderLocator().locate(null);
		DataFieldValueCommand.DataFieldValueCommandBuilder commandBuilder = DataFieldValueCommand.newBuilder()
				.withProviderOverride(command.getProviderOverride())
				.setProviderTimeoutOverride(command.getProviderTimeoutOverride());
		if (command.getExchangeId() != null) {
			InvestmentExchange exchange = getInvestmentExchangeService().getInvestmentExchange(command.getExchangeId());
			ValidationUtils.assertNotNull(exchange, "Investment Exchange: \"" + exchange + "\" was not found.");
			commandBuilder.setExchangeCode(exchange.getExchangeCode());
			commandBuilder.setExchangeCodeType(exchange.isCompositeExchange() ? InvestmentExchangeTypes.COMPOSITE_EXCHANGE : InvestmentExchangeTypes.PRIMARY_EXCHANGE);
		}
		if (!StringUtils.isEmpty(command.getMarketSectorName())) {
			commandBuilder.setMarketSector(command.getMarketSectorName());
		}

		securityToFieldListMap.forEach(commandBuilder::addSecurity);
		return dataProvider.getMarketDataValueLatest(commandBuilder.build());
	}


	/**
	 * If the provided value is not null, it is converted to a {@link MarketDataLive} and the result is appended to the provided list and cached.
	 *
	 * @param valueListToPopulate list of resulting values to append to
	 * @param cachePopulator      consumer to provide the value to for caching
	 * @param marketDataValue     the value
	 * @return true if the value is not null and added to the provided result list and cache, false otherwise
	 */
	private boolean appendAndCacheRetrievedLiveValue(List<MarketDataLive> valueListToPopulate, Consumer<MarketDataLive> cachePopulator, MarketDataValueGeneric<?> marketDataValue) {
		if (marketDataValue == null) {
			return false;
		}

		// for stale data checking in caches, make sure the date includes time
		if (DateUtils.getMilliseconds(marketDataValue.getMeasureDateWithTime()) == 0) {
			marketDataValue.setMeasureTime(new Time(new Date()));
		}
		MarketDataLive liveValue = MarketDataLive.ofMarketDataValue(marketDataValue);
		cachePopulator.accept(liveValue);
		valueListToPopulate.add(liveValue);
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                  Cache Helper Methods                   //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns MarketDataLive Price Value from cache only if cached value is no longer than the specified max age (use default max age if null is passed).
	 * May return null even when cache has the value: stale for this specific call.
	 */
	private MarketDataLive getMarketDataLivePriceFromCache(int securityId, Short exchangeId, Integer maxAgeInSeconds) {
		return getMarketDataLiveFieldValueFromCache(securityId, exchangeId, CACHE_LIVE_PRICE_FIELD_NAME, maxAgeInSeconds);
	}


	private void cacheMarketDataLivePrice(int securityId, Short exchangeId, MarketDataLive result) {
		cacheMarketDataLiveFieldValue(securityId, exchangeId, CACHE_LIVE_PRICE_FIELD_NAME, result);
	}


	/**
	 * Returns MarketDataLive Field Value from cache only if cached value is no longer than the specified max age (use default max age if null is passed).
	 * May return null even when cache has the value: stale for this specific call.
	 */
	private MarketDataLive getMarketDataLiveFieldValueFromCache(int securityId, Short exchangeId, String fieldName, Integer maxAgeInSeconds) {
		return getMarketDataLiveFromCache(CACHE_LIVE_VALUE, getKeyValue(securityId, exchangeId, fieldName), maxAgeInSeconds);
	}


	private void cacheMarketDataLiveFieldValue(int securityId, Short exchangeId, String fieldName, MarketDataLive result) {
		getCacheHandler().put(CACHE_LIVE_VALUE, getKeyValue(securityId, exchangeId, fieldName), result);
	}


	private String getKeyValue(int securityId, Short exchangeId, String fieldName) {
		return exchangeId != null ? securityId + '_' + exchangeId + '_' + fieldName : securityId + '_' + fieldName;
	}


	/**
	 * Returns MarketDataLive Exchange Rate Value from cache only if cached value is no longer than the specified max age (use default max age if null is passed).
	 * May return null even when cache has the value: stale for this specific call.
	 */
	private MarketDataLiveExchangeRate getMarketDataLiveExchangeRateFromCache(int fromCurrencyId, int toCurrencyId, Integer maxAgeInSeconds) {
		return (MarketDataLiveExchangeRate) getMarketDataLiveFromCache(CACHE_LIVE_EXCHANGE_RATE, fromCurrencyId + "_" + toCurrencyId, maxAgeInSeconds);
	}


	private void cacheMarketDataLiveExchangeRate(int fromCurrencyId, int toCurrencyId, MarketDataLiveExchangeRate result) {
		getCacheHandler().put(CACHE_LIVE_EXCHANGE_RATE, fromCurrencyId + "_" + toCurrencyId, result);
	}


	/**
	 * Returns MarketDataLive from cache only if cached value is no longer than the specified max age (use default max age if null is passed).
	 * May return null even when cache has the value: stale for this specific call.
	 */
	private MarketDataLive getMarketDataLiveFromCache(String cacheName, Object identifier, Integer maxAgeInSeconds) {
		MarketDataLive result = getCacheHandler().get(cacheName, identifier);
		if (result != null) {
			// check if cache value is stale
			int maxAge = (maxAgeInSeconds == null) ? DEFAULT_AGE_IN_SECONDS : maxAgeInSeconds;
			if (DateUtils.getSecondsDifference(new Date(), result.getMeasureDate()) > maxAge) {
				result = null;
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataProviderLocator getMarketDataProviderLocator() {
		return this.marketDataProviderLocator;
	}


	public void setMarketDataProviderLocator(MarketDataProviderLocator marketDataProviderLocator) {
		this.marketDataProviderLocator = marketDataProviderLocator;
	}


	public InvestmentExchangeService getInvestmentExchangeService() {
		return this.investmentExchangeService;
	}


	public void setInvestmentExchangeService(InvestmentExchangeService investmentExchangeService) {
		this.investmentExchangeService = investmentExchangeService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public MarketDataFieldService getMarketDataFieldService() {
		return this.marketDataFieldService;
	}


	public void setMarketDataFieldService(MarketDataFieldService marketDataFieldService) {
		this.marketDataFieldService = marketDataFieldService;
	}


	public CacheHandler<Object, MarketDataLive> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Object, MarketDataLive> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public MarketDataRatesService getMarketDataRatesService() {
		return this.marketDataRatesService;
	}


	public void setMarketDataRatesService(MarketDataRatesService marketDataRatesService) {
		this.marketDataRatesService = marketDataRatesService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
