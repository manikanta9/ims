Clifton.marketdata.field.DataFieldValueMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Field Value Mapping',
	iconCls: 'numbers',
	height: 480,

	items: [{
		xtype: 'formpanel',
		instructions: 'Different market data sources may use different values to represent the same data. This section can be used to map data source specific values to values that our system understands.',
		url: 'marketDataFieldValueMapping.json',
		items: [
			{fieldLabel: 'Data Field', name: 'dataField.name', hiddenName: 'dataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},

			{fieldLabel: 'Source Value', name: 'fromValue'},
			{fieldLabel: 'Our Value', name: 'toValue'},
			{fieldLabel: 'Our Value Label', name: 'toValueLabel'}
		]
	}]
});
