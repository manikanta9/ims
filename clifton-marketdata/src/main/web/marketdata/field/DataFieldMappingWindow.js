Clifton.marketdata.field.DataFieldMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Field Mapping',
	iconCls: 'numbers',
	height: 570,

	items: [{
		xtype: 'formpanel',
		instructions: 'Instrument mappings map market data sources, field groups and market sectors to corresponding investment instruments. Direct instrument mapping will override less specific (hierarchy, sub-type, type) mappings.',
		url: 'marketDataFieldMapping.json',
		labelWidth: 150,
		items: [
			{xtype: 'sectionheaderfield', header: 'Specificity Scope'},
			{
				fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true,
				url: 'investmentTypeList.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
				mutuallyExclusiveFields: ['instrumentHierarchy.labelExpanded', 'instrument.name']
			},
			{
				fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo',
				url: 'investmentTypeSubTypeListByType.json', loadAll: true,
				requiredFields: ['investmentType.name'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', displayField: 'name', xtype: 'combo',
				url: 'investmentTypeSubType2ListByType.json', loadAll: true,
				requiredFields: ['investmentType.name'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
					}
				}
			},
			{
				fieldLabel: 'Investment Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo',
				url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
				mutuallyExclusiveFields: ['instrument.name', 'investmentType.name']
			},
			{
				fieldLabel: 'Investment Instrument', name: 'instrument.name', hiddenName: 'instrument.id', xtype: 'combo', displayField: 'label',
				url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
				mutuallyExclusiveFields: ['instrumentHierarchy.labelExpanded', 'investmentType.name']
			},


			{xtype: 'sectionheaderfield', header: 'Data Mappings'},
			{fieldLabel: 'Data Field Group', name: 'fieldGroup.name', hiddenName: 'fieldGroup.id', xtype: 'combo', url: 'marketDataFieldGroupListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldGroupWindow', qtip: 'Used by pricing batch jobs to identifies what Data Fields to retrieve for a particular security'},
			{
				fieldLabel: 'Data Source',
				name: 'marketDataSource.name',
				hiddenName: 'marketDataSource.id',
				xtype: 'combo',
				url: 'marketDataSourceListFind.json',
				detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow',
				listeners: {
					beforeselect: function(combo, record) {
						const form = combo.getParentForm().getForm();
						const sector = form.findField('marketSector.label');
						const pricingSource = form.findField('pricingSource.label');
						sector.clearAndReset();
						pricingSource.clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Market Sector',
				name: 'marketSector.label',
				hiddenName: 'marketSector.id',
				xtype: 'combo',
				displayField: 'label',
				loadAll: true,
				url: 'marketDataSourceSectorListByDataSource.json',
				detailPageClass: 'Clifton.marketdata.datasource.MarketSectorWindow',
				requiredFields: ['marketDataSource.name'],
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					cmb.store.baseParams = {
						dataSourceId: f.findField('marketDataSource.id').getValue()
					};
				}
			},
			{
				fieldLabel: 'Pricing Source', name: 'pricingSource.label', hiddenName: 'pricingSource.id', xtype: 'combo', loadAll: true, url: 'marketDataSourcePricingSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePricingSourceWindow',
				beforequery: function(queryEvent) {
					const cmb = queryEvent.combo;
					const f = TCG.getParentFormPanel(cmb).getForm();
					cmb.store.baseParams = {
						marketDataSourceId: f.findField('marketDataSource.id').getValue()
					};
				}
			},
			{
				fieldLabel: 'Exchange Code Type', name: 'exchangeCodeType', hiddenName: 'exchangeCodeType', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
				qtip: 'Optional type of exchange code to be added to security symbol for each market data provider request.',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: [
						['Use Primary Exchange Code', 'PRIMARY_EXCHANGE', 'The most important stock exchange in a given country. Common characteristics of a primary exchange include a long history, primary listings of the country\'s top companies, listings of many important foreign corporations, large total market capitalization and a large trade value. A country may have other important stock exchanges in addition to its primary exchange.'],
						['Use Composite Exchange Code', 'COMPOSITE_EXCHANGE', 'Composite Exchange is not a real exchange. It is used to calculate average or standardized prices for a security across all exchanges where it trades. Brokers and Custodians usually use composite (as opposed to primary) exchange pricing.'],
						['Use Composite or Primary Exchange Code', 'COMPOSITE_OR_PRIMARY_EXCHANGE', 'If Composite Exchange is defined, use Composite Exchange Code. If not, but Primary Exchange is defined, use Primary Exchange Code. Otherwise do not use any exchange code: security symbol only.']
					]
				})
			},


			{xtype: 'sectionheaderfield', header: 'Live Price Field Overrides'},
			{fieldLabel: 'Live Latest Price Field', name: 'liveLatestPriceMarketDataField.name', hiddenName: 'liveLatestPriceMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'Used for live look ups to find the latest price for a security at any point in time. If one is not explicitly defined, "Last Trade Price" field will be used. If price is not available, will use Live Closing Price field to find the latest closing price.'},
			{fieldLabel: 'Live Closing Price Field', name: 'liveClosingPriceMarketDataField.name', hiddenName: 'liveClosingPriceMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'Optional official End of Day closing price field. Used for Live look ups when the latest price is not available (market is closed)'}
		]
	}]
});
