Clifton.marketdata.field.DataFieldWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Field',
	iconCls: 'numbers',
	width: 900,
	height: 630,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'marketDataField.json',
					labelWidth: 170,
					instructions: 'Market data field defines a single field/measure of market data. For example, Trading Volume, Close Price, Settlement Price, Delta, etc.',
					listeners: {
						afterload: function() {
							const systemDefined = this.getFormValue('systemDefined');
							const entityModifyConditionField = this.getForm().findField('entityModifyCondition.name');
							entityModifyConditionField.setRequireConditionForNonAdmin(systemDefined);
						}
					},
					items: [
						{fieldLabel: 'Field Name', name: 'name'},
						{fieldLabel: 'External Field Name', name: 'externalFieldName', qtip: 'Corresponding field name in external system (Bloomberg, etc.) that is used to map external fields to ours.'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', grow: true, growMin: 50},
						{fieldLabel: 'Data Type', name: 'dataType.name', hiddenName: 'dataType.id', xtype: 'combo', url: 'systemDataTypeList.json', detailPageClass: 'Clifton.system.schema.DataTypeWindow', loadAll: true},
						{fieldLabel: 'Decimal Precision', name: 'decimalPrecision', xtype: 'spinnerfield', minValue: 0},
						{fieldLabel: 'Field Order', name: 'fieldOrder', qtip: 'Some fields have special meaning for the order field.  For KRD Points, for example, the order field represents the number of months that point represents. i.e. 5 Year KRD = 60 months'},
						{fieldLabel: 'Request Timeout Millis', name: 'requestTimeoutMillis', xtype: 'integerfield', minValue: 0, qtip: 'Number of milliseconds to wait for a response from an external system, like Bloomberg, for this field values. Leave blank to use default value.'},
						{fieldLabel: 'Adjustment Type', name: 'adjustmentType', xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.marketdata.field.AdjustmentTypes}, qtip: 'For fields with values that support adjustment factor, specifies the operator (MULTIPLY, DIVIDE, etc.) to use when adjusting historical values. If not set, then no adjustment is needed.  Stock Split example: DIVIDE prides and MULTIPLY shares outstanding.'},
						{
							fieldLabel: 'Ignore Value Condition', name: 'ignoreValueCondition.name', hiddenName: 'ignoreValueCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=MarketDataValue',
							qtip: 'When set, if the Market Data Value passes the ignore condition, the value will not be saved. For example, we ignore 0 values for KRD Points.'
						},
						{xtype: 'system-entity-modify-condition-combo'},
						{boxLabel: 'Allow no more than one value per data source per day', name: 'upToOnePerDay', xtype: 'checkbox', mutuallyExclusiveFields: ['latestValueOnly']},
						{boxLabel: 'Do not keep history but overwrite latest value', name: 'latestValueOnly', xtype: 'checkbox', mutuallyExclusiveFields: ['captureChangesOnly', 'upToOnePerDay']},
						{boxLabel: 'Capture changes to the value only and do not store repeat values', name: 'captureChangesOnly', xtype: 'checkbox', mutuallyExclusiveFields: ['latestValueOnly']},
						{boxLabel: 'Capture the time stamp of the valuation time', name: 'timeSensitive', xtype: 'checkbox'},
						{boxLabel: 'Values are used as real-time reference data only', name: 'realTimeReferenceData', xtype: 'checkbox', qtip: 'If enabled, this indicates that these fields are used as real-time reference data only. Such fields may be linked to by Trade Market Data Fields or other real-time reference-based market data value entities and are not typically used for reporting or other regular market data lookups.'},
						{boxLabel: 'System Defined (Name cannot be edited)', name: 'systemDefined', xtype: 'checkbox', disabled: true},

						{xtype: 'sectionheaderfield', header: 'Overrides'},
						{fieldLabel: 'Symbol Override Field', name: 'symbolOverrideLookupField', qtip: 'Look up new Bloomberg symbol by querying the our symbol against this Bloomberg field. This works in conjunction with symbol market sector override.'},
						{
							fieldLabel: 'Symbol Override Market Sector', name: 'symbolOverrideMarketSector.label', hiddenName: 'symbolOverrideMarketSector.id', xtype: 'combo', displayField: 'label', loadAll: true, url: 'marketDataSourceSectorListByDataSource.json', detailPageClass: 'Clifton.marketdata.datasource.MarketSectorWindow', requiredFields: ['symbolOverrideLookupField'], qtip: 'Look up new Bloomberg symbol by querying our symbol with this sector and the symbol external name override.',
							beforequery: function(queryEvent) {
								const dataSource = TCG.data.getData('marketDataSourceByName.json?requestedPropertiesRoot=data&requestedProperties=id&marketDatasourceName=Bloomberg', queryEvent.combo);
								const dataSourceId = TCG.getValue('id', dataSource);
								queryEvent.combo.store.baseParams = {
									dataSourceId: dataSourceId
								};
							}
						},

						{fieldLabel: 'Value Override Field', name: 'valueOverrideDataField.name', hiddenName: 'valueOverrideDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'When market data values are saved for this field, optionally remap the field to this Value Override Field.'},
						{fieldLabel: 'Overrides for Latest', name: 'externalFieldOverridesForLatest', qtip: 'Optional field overrides for market request for latest data (Bloomberg reference data). Must be in the following format: "field1=value1,field2=value2"'},
						{fieldLabel: 'Overrides for Historic', name: 'externalFieldOverridesForHistoric', qtip: 'Optional field overrides for market request for historic data (Bloomberg historic data). Must be in the following format: "field1=value1,field2=value2". Can use the following dynamic values: ${DATE}<br />For example for 11/28/2012 valuation date, the following string "SETTLE_DT=${SETTLEMENT_DATE},SW_CURVE_DT=${DATE}" will be converted into "SETTLE_DT=20121130,SW_CURVE_DT=20121128"'},
						{fieldLabel: 'Value Expression', name: 'valueExpression', qtip: 'A freemarker expression used to create higher level fields.  The value of this field will be the fields specified in the expression and the operations on them.  Note that if the name of a field you want to reference begins with a digit (0-9), when writing the expression you should prefix the field with "__" (Double underscore). Ex. (${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.priceMultiplier}', xtype: 'textarea', grow: true, growMin: 50},
						{
							fieldLabel: 'Live Value Calculator', name: 'liveValueCalculator.name', hiddenName: 'liveValueCalculator.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Market Date Live Field Value Calculator',
							detailPageClass: 'Clifton.system.bean.BeanWindow', qtip: 'An optional calculator used to retrieve live values instead of using the usual Market Data Provider.',
							getDefaultData: function() {
								return {type: {group: {name: 'Market Date Live Field Value Calculator', alias: 'Live Field Value Calculator'}}};
							}
						},
						{
							fieldLabel: 'Live Value Calculator Condition', name: 'liveValueCalculatorCondition.name', hiddenName: 'liveValueCalculatorCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=InvestmentSecurity',
							qtip: 'The condition to apply when determining if the Live Value Calculator should be used for live value retrieval for a security. This condition is required when Live Value Calculator is used.'
						},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'MarketDataField',
							hierarchyCategoryName: 'Market Data Tags'
						}
					]
				}]
			},


			{
				title: 'Field Groups',
				items: [{
					name: 'marketDataFieldGroupListByField',
					xtype: 'gridpanel',
					instructions: 'Market data field groups that selected data field belongs to.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 120, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					getLoadParams: function() {
						return {
							dataFieldId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.marketdata.field.DataFieldGroupWindow'
					}
				}]
			},


			{
				title: 'Field Values',
				items: [{
					name: 'marketDataValueListFind',
					xtype: 'gridpanel',
					importTableName: 'MarketDataValue',
					instructions: 'Market data field values for selected data field.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Security', width: 200, dataIndex: 'investmentSecurity.label', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Data Source', width: 120, dataIndex: 'dataSource.label', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
						{header: 'Date', width: 80, dataIndex: 'measureDateWithTime', type: 'date', filter: {searchFieldName: 'measureDate'}},
						{header: 'Time', width: 60, dataIndex: 'measureTime', filter: false},
						{header: 'Value', width: 80, dataIndex: 'measureValue', type: 'float'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 30 days of data
							this.setFilterValue('measureDateWithTime', {'after': new Date().add(Date.DAY, -30)});
						}
						return {
							dataFieldId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataValueWindow'
					}
				}]
			},


			{
				title: 'Value Mappings',
				items: [{
					xtype: 'marketdata-field-value-mapping-grid',
					getLoadParams: function(firstLoad) {
						return {
							dataFieldId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldValueMappingWindow',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								dataField: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'System Column Mappings',
				items: [{
					xtype: 'marketdata-field-column-mapping-grid',
					getLoadParams: function(firstLoad) {
						return {
							dataFieldId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.column.DataFieldColumnMappingWindow',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								dataField: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Import Data',
				items: [{
					xtype: 'formpanel',
					controlWindowModified: false,
					instructions: 'This section allows importing daily market data measures for specific market data fields of investment securities. If the field is not selected, will import the data for all fields mapped to the specified security.',
					listeners: {
						afterrender: function() {
							this.updateWarningMessage('Carefully select filters in order to avoid excessive data requests and unintentional updates to existing market data.');
						}
					},
					getDefaultData: function(form) {
						const f = this.getWindow().getMainForm().formValues;
						return {marketDataFieldId: f.id, marketDataField: f.name};
					},
					items: [
						{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceName', value: 'Bloomberg', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', allowBlank: false},
						{fieldLabel: 'Data Field', name: 'marketDataField', hiddenName: 'marketDataFieldId', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'If the field is not selected, will import the data for all fields mapped to the specified security.'},
						{fieldLabel: 'Security', name: 'investmentSecurityId', hiddenName: 'investmentSecurityId', valueField: 'id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json', allowBlank: false, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'From Date', name: 'startDate', xtype: 'datefield', allowBlank: false, value: Clifton.calendar.getBusinessDayFrom(-1)},
						{fieldLabel: 'To Date', name: 'endDate', xtype: 'datefield', allowBlank: false, value: Clifton.calendar.getBusinessDayFrom(-1)},
						{name: 'batchDataRequests', xtype: 'checkbox', boxLabel: 'Batch Data Requests'},
						{name: 'skipDataRequestIfAlreadyExists', xtype: 'checkbox', checked: true, boxLabel: 'Do not retrieve data if it already exists (minimizes calls to the data source but will not get changes to historically retrieved data)'},
						{name: 'overwriteExistingData', xtype: 'checkbox', boxLabel: 'Overwrites existing data if it already exists'},
						{name: 'runAsSystemUser', xtype: 'checkbox', checked: true, boxLabel: 'Run As "systemuser": will use systemuser for Created By and Updated By instead of current user (YOU).'},

						{
							xtype: 'fieldset',
							name: 'marketDataProviderFieldSet',
							collapsed: true,
							title: 'Provider Options',
							instructions: 'Used to specify the Market Data Provider to pull data from',
							items: [
								{
									fieldLabel: 'Provider Override', name: 'marketDataProviderOverride', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'marketDataProviderOverride',
									store: new Ext.data.ArrayStore({
										fields: ['name', 'valueField', 'description'],
										data: Clifton.marketdata.provider.OVERRIDE_OPTIONS
									})
								},
								{fieldLabel: 'Provider Timeout', name: 'marketDataProviderTimeoutOverride', qtip: 'Timeout (in milliseconds) that will override the default provider timeout'},
								{fieldLabel: 'Batch Size', name: 'marketDataProviderBatchSizeOverride', xtype: 'integerfield', minValue: 0, qtip: 'The number of securities to request at one time. If not specified then a request will be made for each security. If the value is 0 then all securities will be requested at once.'}
							]
						}

					],
					buttons: [{
						text: 'Import Data',
						iconCls: 'run',
						width: 150,
						handler: function() {
							const panel = TCG.getParentFormPanel(this);
							const form = panel.getForm();
							form.submit(Ext.applyIf({
								url: 'marketDataFieldValuesUpdate.json?synchronous=true',
								waitMsg: 'Updating Market Data ...',
								success: function(form, action) {
									TCG.createComponent('Clifton.core.RunnerStatusWindow', {
										defaultData: {status: action.result.result}
									});
								}
							}, Ext.applyIf({timeout: 300}, TCG.form.submitDefaults)));
						}
					}]
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'MarketDataField'
				}]
			}
		]
	}]
});
