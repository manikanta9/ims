Clifton.marketdata.field.upload.MarketDataValueUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'marketDataValueUploadWindow',
	title: 'Market Data Value Import',
	iconCls: 'import',
	height: 600,
	hideOKButton: true,

	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('MarketDataValue', false, this);
		}
	}, '-', {
		text: 'Sample (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			Clifton.system.downloadSampleUploadFile('MarketDataValue', true, this);
		}
	}, '-', {
		text: 'Sample (Simple)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for Simple Market Data Value Uploads',
		handler: function() {
			TCG.openFile('marketdata/field/upload/SimpleMarketDataValueUploadFileSample.xls');
		}
	}, '-', {
		text: 'Sample (Simple w/ Multiple)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File for Simple Market Data Value Uploads with Multiple Values In Each Row',
		handler: function() {
			TCG.openFile('marketdata/field/upload/SimpleMarketDataValueMultipleUploadFileSample.xls');
		}
	}, {
		xtype: 'tbfill'
	}, {
		text: 'Generic Import',
		iconCls: 'import',
		tooltip: 'Use generic upload window',
		handler: function() {
			TCG.createComponent('Clifton.system.upload.UploadWindow', {
				defaultData: {tableName: 'MarketDataValue'}
			});
		}
	}],
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Market Data Value Uploads allow you to import market data from files directly into the system.  Use the options below to customize how to upload your data.',

		items: [
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},

			{
				xtype: 'fieldset-checkbox',
				title: 'Simple Market Data Value Uploads',
				checkboxName: 'simple',
				instructions: 'Simple Market Data Value Uploads allow setting less columns in the upload file and the system will make certain assumptions in order to correctly lookup data based on less information. Data Source and/or Data Field selection below is optional and can be supplied instead of setting each row in the file.',
				items: [
					// Entity Modify Fields
					{fieldLabel: 'Data Source', name: 'simpleMarketDataSource.name', hiddenName: 'simpleMarketDataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'},
					{
						fieldLabel: '', boxLabel: 'Multiple: Apply Unmapped Columns as Separate Field Values for Each Row', name: 'applyUnmappedColumnNamesAsSeparateDataFields', xtype: 'checkbox'
						, qtip: 'Use this option (see Simple Multiple Sample File) to upload for each row, multiple values where the column names represent the data field names.'
						, mutuallyExclusiveFields: ['simpleMarketDataField.name']
					},
					{
						fieldLabel: 'Data Field', name: 'simpleMarketDataField.name', hiddenName: 'simpleMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json'
						, mutuallyExclusiveFields: ['applyUnmappedColumnNamesAsSeparateDataFields']
					},
					{fieldLabel: 'Measure Date', name: 'simpleMeasureDate', xtype: 'datefield'}
				]
			},
			{
				xtype: 'fieldset', title: 'Import Results:', layout: 'fit',
				items: [
					{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
				]
			}
		],

		getSaveURL: function() {
			return 'marketDataValueUploadFileUpload.json?enableValidatingBinding=true&disableValidatingBindingValidation=true';
		}

	}]
});
