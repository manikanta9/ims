Clifton.marketdata.field.DataValueWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Value',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'This section collects daily market data measures for specific market data fields of investment securities',
		url: 'marketDataValue.json',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Data Field', name: 'dataField.name', hiddenName: 'dataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Investment Security', name: 'investmentSecurity.label', hiddenName: 'investmentSecurity.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Date', name: 'measureDate', xtype: 'datefield'},
			{fieldLabel: 'Time', name: 'measureTime', xtype: 'timefield'},
			{fieldLabel: 'Value', name: 'measureValue'},
			{fieldLabel: 'Adjusted Value', name: 'measureValueAdjusted', xtype: 'displayfield', qtip: 'historic values maybe adjusted for stock splits, etc.'},
			{fieldLabel: 'Adjustment Factor', name: 'measureValueAdjustmentFactor', xtype: 'displayfield'}
		]
	}]
});
