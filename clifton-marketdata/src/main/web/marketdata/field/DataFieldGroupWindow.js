Clifton.marketdata.field.DataFieldGroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Field Group',
	iconCls: 'numbers',
	width: 800,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Market data field groups group related data fields that apply to the same investment instruments. They improve usability when mapping to investment instruments.',
					url: 'marketDataFieldGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: '', boxLabel: 'System Defined', xtype: 'checkbox', qtip: 'System Defined groups are referenced in the system by name, and their names cannot change', name: 'systemDefined', readOnly: true},
						{fieldLabel: '', boxLabel: 'Field Order Required', xtype: 'checkbox', name: 'fieldOrderRequired'}
					]
				}]
			},


			{
				title: 'Data Fields',
				items: [{
					name: 'marketDataFieldListByGroup',
					xtype: 'gridpanel',
					instructions: 'Selected group has the following market data fields. To add another field to this group, select desired field from the list and click the [Add] button.',
					getLoadParams: function() {
						return {dataFieldGroupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Field Name', width: 120, dataIndex: 'name'},
						{header: 'Order', width: 50, dataIndex: 'fieldOrder'},
						{header: 'External Name', width: 120, dataIndex: 'externalFieldName'},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Precision', width: 80, dataIndex: 'decimalPrecision', type: 'int'},
						{header: 'One Per Day', width: 80, dataIndex: 'upToOnePerDay', type: 'boolean'},
						{header: 'Latest Only', width: 80, dataIndex: 'latestValueOnly', type: 'boolean'},
						{header: 'Changes Only', width: 80, dataIndex: 'captureChangesOnly', type: 'boolean'},
						{header: 'Time', width: 60, dataIndex: 'timeSensitive', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldWindow',
						deleteURL: 'marketDataFieldFieldGroupDelete.json',
						getDeleteParams: function(selectionModel) {
							return {
								dataFieldGroupId: this.getWindow().getMainFormId(),
								dataFieldId: selectionModel.getSelected().id
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'field', url: 'marketDataFieldListFind.json', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add the field to selected group',
								iconCls: 'add',
								handler: function() {
									const fieldId = TCG.getChildByName(toolBar, 'field').getValue();
									if (fieldId === '') {
										TCG.showError('You must first select desired user Field from the list.');
									}
									else {
										const groupId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {dataFieldGroupId: groupId, dataFieldId: fieldId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'field').reset();
											}
										});
										loader.load('marketDataFieldToGroupLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			}
		]
	}]
});
