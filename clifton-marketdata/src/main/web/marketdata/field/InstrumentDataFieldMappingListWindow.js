// Displays Instrument Market Data Field Mappings and Instrument Market Data Price Field Mappings for specific instrument based on specificity filters
Clifton.marketdata.field.InstrumentDataFieldMappingListWindow = Ext.extend(TCG.app.CloseWindow, {
	titlePrefix: 'Instrument Mappings',
	iconCls: 'numbers',
	layout: 'border',
	defaultDataIsReal: true,
	height: 600,
	width: 900,

	items: [
		{
			title: 'Instrument Field Mappings',
			name: 'marketDataFieldMappingListByInstrument',
			xtype: 'gridpanel',
			region: 'north',
			height: 250,
			instructions: 'There can only be one field mapping that applies PER DATASOURCE.  Details listed below.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Data Source', width: 70, dataIndex: 'marketDataSource.label'},
				{header: 'Mapping', width: 150, dataIndex: 'specificityLabel'},
				{header: 'Field Group', width: 100, dataIndex: 'fieldGroup.label'},
				{header: 'Market Sector', width: 70, dataIndex: 'marketSector.label'},
				{header: 'Pricing Source', width: 60, dataIndex: 'pricingSource.label'},
				{header: 'Exchange Code Type', width: 70, dataIndex: 'exchangeCodeType'},
				{header: 'Live Latest Price Field', width: 70, dataIndex: 'liveLatestPriceMarketDataField.name', hidden: true},
				{header: 'Live Closing Price Field', width: 70, dataIndex: 'liveClosingPriceMarketDataField.name', hidden: true}
			],
			editor: {
				detailPageClass: 'Clifton.marketdata.field.DataFieldMappingWindow'
			},
			getLoadParams: function() {
				return this.getWindow().defaultData;
			}
		},

		{
			title: 'Instrument Price Field Mapping',
			xtype: 'formpanel',
			tbar: [
				{
					text: 'Add',
					tooltip: 'Add New Price Field Mapping.',
					iconCls: 'add',
					handler: function() {
						const fp = this.ownerCt.ownerCt;
						const className = 'Clifton.marketdata.field.DataPriceFieldMappingWindow';
						const cmpId = TCG.getComponentId(className);
						TCG.createComponent(className, {
							id: cmpId,
							openerCt: fp
						});
					}
				}, '-',
				{
					text: 'Reload',
					tooltip: 'Reload price field mapping for selected instrument.',
					iconCls: 'table-refresh',
					handler: function() {
						const fp = this.ownerCt.ownerCt;
						fp.reload();
					}
				}
			],
			readOnly: true,
			labelFieldName: 'instrumentLabel',
			region: 'center',
			instructions: 'There can only be one price field mapping that applies.  Details listed below.  If mapping is not found the default is to use <i>Last Trade Price</i> field for Latest and Closing - no restriction on data source and no Official price field.',
			items: [
				{fieldLabel: 'Mapping', xtype: 'linkfield', name: 'label', detailIdField: 'id', detailPageClass: 'Clifton.marketdata.field.DataPriceFieldMappingWindow'},
				{
					xtype: 'formtable',
					columns: 3,
					items: [
						{html: '&nbsp;', cellWidth: '115px'},
						{html: '<b>Price Field</b>'},
						{html: '<b>Data Source</b>'},

						{html: 'Latest Price:'},
						{name: 'latestPriceMarketDataField.label', xtype: 'linkfield', detailIdField: 'latestPriceMarketDataField.id', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
						{name: 'latestPriceMarketDataSource.name', xtype: 'linkfield', detailIdField: 'latestPriceMarketDataSource.id', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},

						{html: 'Closing Price:'},
						{name: 'closingPriceMarketDataField.label', xtype: 'linkfield', detailIdField: 'closingPriceMarketDataField.id', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
						{name: 'closingPriceMarketDataSource.name', xtype: 'linkfield', detailIdField: 'closingPriceMarketDataSource.id', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},

						{html: 'Official Price:'},
						{name: 'officialPriceMarketDataField.label', xtype: 'linkfield', detailIdField: 'closingPriceMarketDataField.id', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},
						{name: 'officialPriceMarketDataSource.name', xtype: 'linkfield', detailIdField: 'closingPriceMarketDataSource.id', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'}
					]
				},
				{fieldLabel: 'Note', xtype: 'textarea', name: 'note', height: 50}
			],
			editor: {
				detailPageClass: 'Clifton.marketdata.field.DataPriceFieldMappingWindow'
			},
			updateTitle: function() {
				const w = this.getWindow();
				const instrumentId = w.defaultData.instrumentId;
				TCG.data.getDataPromise('investmentInstrument.json?id=' + instrumentId, this)
						.then(function(data) {
							w.setTitle(data.label);
						});
			},
			reload: function() {
				const w = this.getWindow();
				this.getForm().setValues(this.getPriceMappingForInstrument(w.defaultData.instrumentId), true);
			},
			prepareDefaultData: function(defaultData) {
				if (defaultData && defaultData.instrumentId && defaultData.instrumentId !== '') {
					return this.getPriceMappingForInstrument(defaultData.instrumentId);
				}
				defaultData.label = 'No Price Mapping Found.';
				return defaultData;
			},
			getPriceMappingForInstrument: function(instrumentId) {
				let priceFieldMapping = TCG.data.getData('marketDataPriceFieldMappingByInstrument.json?instrumentId=' + instrumentId, this);
				if (!priceFieldMapping) {
					priceFieldMapping = {label: 'No Price Mapping Found.'};
				}
				return priceFieldMapping;
			}
		}
	]
});
