Clifton.marketdata.field.DataPriceFieldMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Price Field Mapping',
	iconCls: 'numbers',
	width: 900,
	height: 570,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Instrument mappings map price fields to corresponding investment instruments. Direct instrument mapping will override less specific (hierarchy, sub-type, type) mappings. If a data source is specified for any of the price fields, ONLY prices for that data source will be used.  Default price field for Latest Price and Settlement Price is Last Trade Price when not specified.',
					url: 'marketDataPriceFieldMapping.json',
					labelWidth: 150,
					items: [
						{xtype: 'sectionheaderfield', header: 'Specificity Scope'},
						{
							fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true,
							url: 'investmentTypeList.json', detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
							mutuallyExclusiveFields: ['instrumentHierarchy.labelExpanded', 'instrument.name']
						},
						{
							fieldLabel: 'Investment Sub Type', name: 'investmentTypeSubType.name', hiddenName: 'investmentTypeSubType.id', displayField: 'name', xtype: 'combo',
							url: 'investmentTypeSubTypeListByType.json', loadAll: true,
							requiredFields: ['investmentType.name'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.setBaseParam('investmentTypeId', f.findField('investmentType.name').getValue());
								}
							}
						},
						{
							fieldLabel: 'Investment Sub Type 2', name: 'investmentTypeSubType2.name', hiddenName: 'investmentTypeSubType2.id', displayField: 'name', xtype: 'combo',
							url: 'investmentTypeSubType2ListByType.json', loadAll: true,
							requiredFields: ['investmentType.name'],
							listeners: {
								beforequery: function(queryEvent) {
									const combo = queryEvent.combo;
									const f = combo.getParentForm().getForm();
									combo.store.setBaseParam('investmentTypeId', f.findField('investmentType.name').getValue());
								}
							}
						},
						{
							fieldLabel: 'Investment Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchy.id', displayField: 'labelExpanded', xtype: 'combo',
							url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow',
							mutuallyExclusiveFields: ['instrument.name', 'investmentType.name']
						},
						{
							fieldLabel: 'Investment Instrument', name: 'instrument.name', hiddenName: 'instrument.id', xtype: 'combo', displayField: 'label',
							url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
							mutuallyExclusiveFields: ['instrumentHierarchy.labelExpanded', 'investmentType.name']
						},

						{xtype: 'sectionheaderfield', header: 'Price Field Overrides'},
						{fieldLabel: 'Latest Price Field', name: 'latestPriceMarketDataField.name', hiddenName: 'latestPriceMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'Latest price for a security at any point in time. If one is not explicitly defined, "Last Trade Price" field will be used.'},
						{fieldLabel: 'Latest Price Data Source', name: 'latestPriceMarketDataSource.name', hiddenName: 'latestPriceMarketDataSource.id', xtype: 'combo', requiredFields: ['latestPriceMarketDataField.name'], url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', qtip: 'If specified, ONLY Latest Prices from this data source will be used - leave blank to use the specific field from any data source.'},
						{fieldLabel: 'Closing Price Field', name: 'closingPriceMarketDataField.name', hiddenName: 'closingPriceMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'Optional official End of Day closing price field. For example, Settlement Price for futures or cleared swaps. If defined, takes precedence over Latest Price Field. If not applicable, then Latest Price Field will be used.'},
						{fieldLabel: 'Closing Price Data Source', name: 'closingPriceMarketDataSource.name', hiddenName: 'closingPriceMarketDataSource.id', xtype: 'combo', requiredFields: ['closingPriceMarketDataField.name'], url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', qtip: 'If specified, ONLY Closing Prices from this data source will be used - leave blank to use the specific field from any data source.'},
						{fieldLabel: 'Official Price Field', name: 'officialPriceMarketDataField.name', hiddenName: 'officialPriceMarketDataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'Optional official price of a security (usually "NAV Price" from Fund Administrator). This field is usually either unavailable or only available on the last day of the month. If the value is available, it will be used for End of Day valuations instead of Closing Price Field or Latest Price Field.'},
						{fieldLabel: 'Official Price Data Source', name: 'officialPriceMarketDataSource.name', hiddenName: 'officialPriceMarketDataSource.id', xtype: 'combo', requiredFields: ['officialPriceMarketDataField.name'], url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', qtip: 'If specified, ONLY Official Prices from this data source will be used - leave blank to use the specific field from any data source.'},

						{xtype: 'sectionheaderfield', header: 'Note'},
						{xtype: 'textarea', name: 'note', height: 50}
					]
				}]
			},


			{
				title: 'Instruments',
				items: [{
					xtype: 'gridpanel',
					name: 'investmentInstrumentListFind',
					instructions: 'The following instruments are mapping to this price field mapping.',

					onBeforeReturnFilterData: function(filters) {
						filters.push({
							field: 'priceFieldMappingId',
							data: {comparison: 'EQUALS', value: this.gridPanel.getWindow().getMainFormId()}
						});
					},
					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Investment Type', width: 60, dataIndex: 'hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}, hidden: true},
						{header: 'Sub Type', width: 60, dataIndex: 'hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 130, dataIndex: 'hierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}, hidden: true},
						{header: 'Prefix', width: 70, dataIndex: 'identifierPrefix'},
						{header: 'Instrument Name', width: 100, dataIndex: 'name'},
						{header: 'Underlying Symbol', width: 70, dataIndex: 'underlyingInstrument.identifierPrefix', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}},
						{header: 'Underlying Name', width: 100, hidden: true, dataIndex: 'underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}},
						{header: 'CCY Denomination', width: 80, dataIndex: 'tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Primary Exchange', width: 100, hidden: true, dataIndex: 'exchange.label', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'Primary Exchange Code', width: 50, dataIndex: 'exchange.exchangeCode', filter: {type: 'combo', searchFieldName: 'exchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=false'}},
						{header: 'Composite Exchange', hidden: true, width: 100, dataIndex: 'compositeExchange.label', filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true'}},
						{header: 'Composite Exchange Code', width: 50, dataIndex: 'compositeExchange.exchangeCode', filter: {type: 'combo', searchFieldName: 'compositeExchangeId', displayField: 'label', url: 'investmentExchangeListFind.json?compositeExchange=true'}},
						{header: 'MIC Code', width: 100, dataIndex: 'exchange.marketIdentifierCode', filter: false, sortable: false},

						{header: 'Settlement Calendar', width: 100, hidden: true, dataIndex: 'settlementCalendar.name', filter: {type: 'combo', searchFieldName: 'settlementCalendarId', url: 'calendarListFind.json'}},
						{header: 'Settlement Calendar Code', width: 50, dataIndex: 'settlementCalendar.code', filter: {type: 'combo', searchFieldName: 'settlementCalendarId', url: 'calendarListFind.json'}},

						{header: 'Inactive', hidden: true, width: 50, dataIndex: 'inactive', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.investment.instrument.InstrumentWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'Market Data',
				items: [{
					name: 'marketDataValueListFind',
					xtype: 'gridpanel',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Instrument ID', width: 15, dataIndex: 'investmentSecurity.instrument.id', hidden: true},
						{header: 'Security ID', width: 15, dataIndex: 'investmentSecurity.id', hidden: true},
						{header: 'Investment Type', width: 70, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentType.name', filter: false, sortable: false},
						{header: 'Sub Type', width: 60, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType.name', filter: false, sortable: false},
						{header: 'Sub Type 2', width: 60, hidden: true, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType2.name', filter: false, sortable: false},
						{header: 'Investment Hierarchy', hidden: true, width: 210, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Identifier Prefix', hidden: true, width: 60, dataIndex: 'investmentSecurity.instrument.identifierPrefix', filter: false, sortable: false},
						{header: 'Symbol', width: 80, dataIndex: 'investmentSecurity.symbol', filter: {searchFieldName: 'symbol'}},
						{header: 'Security Name', width: 80, hidden: true, dataIndex: 'investmentSecurity.name', filter: false, sortable: false},
						{header: 'Underlying Symbol', width: 80, dataIndex: 'investmentSecurity.underlyingSecurity.symbol', filter: false, sortable: false},
						{header: 'Underlying Name', width: 80, hidden: true, dataIndex: 'investmentSecurity.underlyingSecurity.name', filter: false, sortable: false},
						{header: 'CCY Denomination', width: 80, dataIndex: 'investmentSecurity.instrument.tradingCurrency.name', filter: false, sortable: false},
						{header: 'Field', width: 100, dataIndex: 'dataField.name', filter: {type: 'combo', searchFieldName: 'dataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
						{header: 'Date', width: 80, dataIndex: 'measureDate'},
						{
							header: 'Value', width: 80, dataIndex: 'measureValue', type: 'float',
							renderer: function(v, metaData, r) {
								const value = TCG.numberFormat(v, '0,000', true);
								if (r.json.createDate !== r.json.updateDate) {
									return '<div class="amountAdjusted" qtip="Market Data was updated. See Audit Trail for details.">' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Adjusted Value', width: 80, dataIndex: 'measureValueAdjusted', type: 'float', hidden: true, tooltip: 'historic values maybe adjusted for stock splits, etc.', nonPersistentField: true},
						{header: 'Adjustment Factor', width: 80, dataIndex: 'measureValueAdjustmentFactor', type: 'float', hidden: true},
						{header: 'External Field Name', width: 100, hidden: true, dataIndex: 'dataField.externalFieldName', filter: {searchFieldName: 'externalDataFieldName'}},
						{header: 'Precision', width: 100, hidden: true, dataIndex: 'dataField.decimalPrecision', filter: false, sortable: false},
						{header: 'One Per Day', width: 50, hidden: true, dataIndex: 'dataField.upToOnePerDay', type: 'checkbox', filter: false, sortable: false},
						{header: 'Latest Only', width: 50, hidden: true, dataIndex: 'dataField.latestValueOnly', type: 'checkbox', filter: false, sortable: false},
						{header: 'Changes Only', width: 50, hidden: true, dataIndex: 'dataField.captureChangesOnly', type: 'checkbox', filter: false, sortable: false},
						{header: 'System Defined', width: 50, hidden: true, dataIndex: 'dataField.systemDefined', type: 'checkbox', filter: false, sortable: false}
					],
					getLoadParams: function(firstLoad) {
						const lp = {};
						lp.priceFieldMappingId = this.getWindow().getMainFormId();
						if (firstLoad) {
							this.setFilterValue('measureDate', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
						}

						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (tag.getValue() !== '') {
							lp.dataFieldCategoryHierarchyId = tag.getValue();
						}
						return lp;
					},

					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Market Data Tags'});
						return filters;
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataValueWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
