Clifton.marketdata.field.column.DataFieldColumnMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Field Column Mapping',
	iconCls: 'numbers',
	height: 500,

	items: [{
		xtype: 'formpanel',
		getDefaultData: function(form) {
			const hierarchyId = this.getWindow().defaultData.instrumentHierarchyId;
			if (hierarchyId) {
				this.getWindow().defaultData.instrumentHierarchy = TCG.data.getData('investmentInstrumentHierarchy.json?id=' + hierarchyId, this);
			}
			this.getWindow().defaultData.dataSource = TCG.data.getData('marketDataSourceByName.json?marketDatasourceName=Bloomberg', this);

			return this.getWindow().defaultData;
		},
		instructions: 'Maps selected market data field to corresponding system column. This mapping is used to pre-populate data (usually security fields) from market data providers. Hierarchy specific mappings take precedence over Type specific mappings which take precedence over Global mappings.',
		url: 'marketDataFieldColumnMapping.json',
		labelWidth: 150,
		labelFieldName: 'dataField.name',
		items: [
			{xtype: 'sectionheaderfield', header: 'Source Field'},
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Data Field', name: 'dataField.name', hiddenName: 'dataField.id', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'},

			{xtype: 'sectionheaderfield', header: 'Target Column'},
			{
				fieldLabel: 'System Table', name: 'table.name', hiddenName: 'table.id', submitValue: false, xtype: 'combo', url: 'systemTableListFind.json?defaultDataSource=true',
				detailPageClass: 'Clifton.system.schema.TableWindow',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.getForm().findField('column.name').clearAndReset();
						fp.getForm().findField('template.name').clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'System Column Template',
				name: 'template.name',
				hiddenName: 'template.id',
				displayField: 'label',
				xtype: 'combo',
				url: 'systemColumnTemplateListFind.json',
				disableAddNewItem: true,
				detailPageClass: 'Clifton.system.schema.TemplateColumnWindow',
				requiredFields: ['table.name'],
				//mutuallyExclusiveFields: ['column.id'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						const groups = TCG.data.getData('systemColumnGroupListByTable.json?tableId=' + f.findField('table.id').getValue(), queryEvent.combo);
						const colIds = [];
						if (groups.length > 0) {
							for (let i = 0; i < groups.length; i++) {
								colIds[i] = groups[i].id;
							}
						}
						else {
							colIds[0] = 0;
						}

						combo.store.baseParams = {
							systemColumnGroupIds: colIds
						};
					}
				}
			},
			{
				fieldLabel: 'System Column',
				name: 'column.name',
				hiddenName: 'column.id',
				displayField: 'customLabel',
				xtype: 'combo',
				url: 'systemColumnListFind.json',
				disableAddNewItem: true,
				detailPageClass: 'Clifton.system.schema.ColumnWindow',
				requiredFields: ['table.name'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams = {
							tableId: f.findField('table.id').getValue(),
							excludeCustomColumnType: 'COLUMN_DATED_VALUE'
						};
						const linkedValue = f.findField('instrumentHierarchy.labelExpanded').getValue();
						if (linkedValue) {
							combo.store.baseParams.linkedValue = linkedValue;
							combo.store.baseParams.includeNullLinkedValue = true;
						}
					}
				}
			},
			{fieldLabel: 'Property Name for Lookup', name: 'beanPropertyNameForLookup', qtip: 'Optional field to use for Foreign Key lookups. For example, "symbol" to look up currency denomination.'},

			{xtype: 'sectionheaderfield', header: 'Scope'},
			{
				fieldLabel: 'Investment Type', name: 'investmentType.name', hiddenName: 'investmentType.id', displayField: 'name', xtype: 'combo', loadAll: true,
				url: 'investmentTypeList.json',
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeWindow',
				disableAddNewItem: true,
				listeners: {
					//TODO Consider refactoring these sections to be consistent and use clearAndReset() for all needed fields.
					beforeselect: function(combo, record) {
						const f = combo.getParentForm().getForm();
						const subType = f.findField('investmentTypeSubType.name');
						subType.clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Investment Sub Type',
				name: 'investmentTypeSubType.name',
				hiddenName: 'investmentTypeSubType.id',
				displayField: 'name',
				xtype: 'combo',
				loadAll: true,
				requiredFields: ['investmentType.name'],
				url: 'investmentTypeSubTypeListByType.json',
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubTypeWindow',
				disableAddNewItem: true,
				//mutuallyExclusiveFields: ['investmentTypeSubType2.id'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
					}
				}
			}, {
				fieldLabel: 'Investment Sub Type 2',
				name: 'investmentTypeSubType2.name',
				hiddenName: 'investmentTypeSubType2.id',
				displayField: 'name',
				xtype: 'combo',
				loadAll: true,
				requiredFields: ['investmentType.name'],
				url: 'investmentTypeSubType2ListByType.json',
				detailPageClass: 'Clifton.investment.setup.InvestmentTypeSubType2Window',
				disableAddNewItem: true,
				//mutuallyExclusiveFields: ['investmentTypeSubType.id'],
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						combo.store.baseParams = {investmentTypeId: f.findField('investmentType.name').getValue()};
					}
				}
			},
			{
				fieldLabel: 'Investment Hierarchy',
				name: 'instrumentHierarchy.labelExpanded',
				hiddenName: 'instrumentHierarchy.id',
				displayField: 'labelExpanded',
				xtype: 'combo',
				url: 'investmentInstrumentHierarchyListFind.json',
				detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'
			},
			{fieldLabel: '', boxLabel: 'Exclude Column (usually an override for less specific scope)', xtype: 'checkbox', name: 'columnExcluded'}
		]
	}]
});
