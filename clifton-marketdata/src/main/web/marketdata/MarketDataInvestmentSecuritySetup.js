Clifton.marketdata.MarketDataInvestmentSecuritySetup = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Investment Security Setup',
	iconCls: 'numbers',
	height: 280,

	items: [{
		xtype: 'formpanel',
		instructions: 'Map a column to a data field for a data source.',
		url: 'marketDataInvestmentSecurity.json',
		loadValidation: false,
		buttonAlign: 'right',
		buttons: [{
			text: 'Update/Create Security',
			handler: function() {
				const owner = this.findParentByType('formpanel');
				const form = owner.getForm();

				form.submit(Ext.applyIf({
					url: 'marketDataInvestmentSecurity.json',
					waitMsg: 'Generating...',
					success: function(form, action) {
						const config = {
							defaultData: action.result.data,
							defaultDataIsReal: true
						};
						TCG.createComponent('Clifton.investment.instrument.SecurityWindow', config);
					}
				}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
			}
		}],
		items: [
			{fieldLabel: 'Symbol', name: 'symbol', allowBlank: false},
			{
				fieldLabel: 'Yellow Key (Not Required)', name: 'yellowKey', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'marketSector',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'valueField'],
					data: [['Government', 'Govt'], ['CorpBonds', 'Corp'], ['Municipal', 'Muni'], ['Index', 'Index'], ['Equity', 'Equity'], ['MoneyMarket', 'M-Mkt'], ['Currency', 'Curncy'], ['Client', 'Client'], ['PreferredStock', 'Pfd'], ['Commodity', 'comdty'], ['Mortgage', 'Mtge']]
				})
			},
			{fieldLabel: 'Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchyId', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'marketDataSourceId', displayField: 'label', allowBlank: true, xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'}
		]
	}]
});
