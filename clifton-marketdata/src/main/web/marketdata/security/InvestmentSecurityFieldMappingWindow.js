Clifton.marketdata.security.InvestmentSecurityFieldMappingWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Security Field Mappings',
	iconCls: 'stock-chart',
	width: 1100,

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);
		const win = this;
		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},
	items: [{
		xtype: 'gridpanel',
		limitRequestedProperties: false,
		name: 'marketDataInvestmentSecurityFieldMappingGrid',
		forceLocalFiltering: true,
		getLoadURL: function() {
			return 'marketDataFieldColumnMappingList.json';
		},

		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{
				header: 'Table', width: 60, dataIndex: 'column.table.label',
				renderer: function(value, metaData, r) {
					const json = r.json;
					if (json.template) {
						return json.template.systemColumnGroup.table.label;
					}
					else {
						return json.column.table.label;
					}
				}
			},
			{header: 'Template', width: 45, dataIndex: 'template.label'},
			{header: 'Column', width: 50, dataIndex: 'column.label'},
			{header: 'Property Name', width: 50, dataIndex: 'column.beanPropertyName'},
			{header: 'Lookup Property Name', width: 50, dataIndex: 'beanPropertyNameForLookup', hidden: true},
			{header: 'Field', width: 50, dataIndex: 'dataField.name'},
			{header: 'External Name', width: 50, dataIndex: 'dataField.externalFieldName'},
			{header: 'Expression', width: 100, dataIndex: 'dataField.valueExpression'},
			{header: 'Excluded', width: 30, dataIndex: 'columnExcluded', type: 'boolean'},
			{header: 'Entity List URL', width: 120, dataIndex: 'column.valueListUrl'}
		],
		getLoadParams: function(firstLoad) {
			return this.getWindow().defaultData;
		},
		isPagingEnabled: function() {
			return false;
		},
		editor: {
			detailPageClass: 'Clifton.marketdata.field.column.DataFieldColumnMappingWindow',
			getDeleteURL: function() {
				return 'marketDataFieldColumnMappingDelete.json';
			},
			getDefaultData: function(gridPanel, row) {
				const result = {instrumentHierarchyId: gridPanel.getWindow().defaultData.instrumentHierarchyId};

				const selected = gridPanel.getRowSelectionModel().getSelected();
				if (selected) {
					const data = selected.json;

					if (data.dataField) {
						result.dataField = data.dataField;
					}

					if (data.template) {
						result.template = data.template;

						result.column = {
							table: data.template.systemColumnGroup.table
						};
					}

					if (data.column) {
						result.column = data.column;
						result.beanPropertyNameForLookup = data.column.beanPropertyName;
					}
				}

				return result;
			}
		}
	}]
});
