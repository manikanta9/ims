Clifton.marketdata.security.InvestmentSecurityUpdaterPreviewWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Security Field Preview',
	iconCls: 'stock-chart',
	width: 1000,

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);
		const win = this;
		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},

	items: [{
		xtype: 'gridpanel',
		limitRequestedProperties: false,
		name: 'marketDataInvestmentSecurityUpdateResultGrid',

		getLoadURL: function() {
			return 'marketDataInvestmentSecurityFieldDataPreview.json?includeUnchanged=true';
		},
		columns: [
			{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Security', width: 30, dataIndex: 'security.symbol', defaultSortColumn: true, defaultSortDirection: 'ASC'},
			{header: 'Table', width: 50, dataIndex: 'dataFieldColumnMapping.column.table.label'},
			{header: 'Column', width: 50, dataIndex: 'dataFieldColumnMapping.column.label'},
			{header: 'Property Name', width: 50, dataIndex: 'dataFieldColumnMapping.column.beanPropertyName'},
			{header: 'Lookup Property Name', width: 50, dataIndex: 'dataFieldColumnMapping.beanPropertyNameForLookup', hidden: true},
			{header: 'Field', width: 50, dataIndex: 'dataFieldColumnMapping.dataField.name'},
			{header: 'External Name', width: 50, dataIndex: 'dataFieldColumnMapping.dataField.externalFieldName', hidden: true},
			{header: 'Expression', width: 100, dataIndex: 'dataFieldColumnMapping.dataField.valueExpression', hidden: true},
			{header: 'Orig. Text', width: 50, dataIndex: 'originalText', hidden: true},
			{
				header: 'Orig. Value', width: 100, dataIndex: 'originalValue',
				renderer: function(v, metaData, r) {
					if (r.json.valueDifferent === true) {
						metaData.css = 'ruleViolation';
					}
					return v;
				}
			},
			{header: 'New Text', width: 50, dataIndex: 'newText', hidden: true},
			{
				header: 'New Value', width: 100, dataIndex: 'newValue',
				renderer: function(v, metaData, r) {
					if (r.json.valueDifferent === true) {
						metaData.css = 'ruleViolation';
					}
					return v;
				}
			}
		],
		getLoadParams: function() {
			return this.getWindow().defaultData;
		},
		isPagingEnabled: function() {
			return false;
		},
		editor: {
			drillDownOnly: true,
			addEnabled: false
		}
	}]
});
