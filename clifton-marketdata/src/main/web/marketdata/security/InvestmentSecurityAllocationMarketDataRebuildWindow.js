Clifton.marketdata.security.InvestmentSecurityAllocationMarketDataRebuildWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Rebuild Allocated Security Market Data',
	iconCls: 'numbers',
	layout: 'vbox',
	height: 500,
	width: 750,

	layoutConfig: {align: 'stretch'},
	items: [
		{
			xtype: 'formpanel',
			labelWidth: 180,
			loadValidation: false, // using the form only to get background color/padding
			height: 230,
			buttonAlign: 'right',
			instructions: 'Enter an optional start/end date.  Leave dates blank to perform a full rebuild on the security.',

			items: [
				{xtype: 'hidden', name: 'securityId'},
				{fieldLabel: 'Security', name: 'securityLabel', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'securityId'},
				{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceId', xtype: 'combo', url: 'marketDataSourceListFind.json', qtip: 'Data Source to use for saving calculated values.'},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
				{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
				{xtype: 'hidden', name: 'asynchronous', value: true}
			],

			buttons: [
				{
					text: 'Rebuild Market Data',
					iconCls: 'run',
					width: 150,
					handler: function() {
						const formPanel = this.findParentByType('formpanel');
						formPanel.getForm().submit(Ext.applyIf({
							url: 'marketDataAllocatedValueForSecurityRebuild.json',
							waitMsg: 'Rebuilding...',
							success: function(form, action) {
								Ext.Msg.alert('Market Data Rebuild Started', action.result.data, function() {
									const grid = formPanel.ownerCt.items.get(1);
									grid.reloadRepeated(5000, 120000); // every 5s for 2 minutes
								});
							}
						}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
					}
				}
			]
		},

		{
			xtype: 'core-scheduled-runner-grid',
			typeName: 'MARKET-DATA-ALLOCATED-VALUE',
			instantRunner: true,
			instructions: 'The following allocated securities are being rebuilt right now.',
			title: 'Current Market Data Rebuilds',
			flex: 1
		}
	]
});

