Clifton.marketdata.security.InvestmentSecurityAllocationMarketDataPreviewWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Preview Allocated Security Market Data',
	iconCls: 'numbers',
	layout: 'vbox',
	height: 500,
	width: 900,

	layoutConfig: {align: 'stretch'},
	items: [
		{
			xtype: 'formpanel',
			labelWidth: 180,
			loadValidation: false, // using the form only to get background color/padding
			height: 230,
			buttonAlign: 'right',
			instructions: 'Enter preview date.',

			items: [
				{xtype: 'hidden', name: 'securityId'},
				{fieldLabel: 'Security', name: 'securityLabel', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'securityId'},
				{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceId', xtype: 'combo', url: 'marketDataSourceListFind.json', qtip: 'Data Source to use for saving calculated values.'},
				{xtype: 'label', html: '<hr/>'},
				{fieldLabel: 'Preview Date', name: 'previewDate', xtype: 'datefield'}
			],

			buttons: [
				{
					text: 'Preview Market Data',
					iconCls: 'run',
					width: 150,
					handler: function() {
						const formPanel = this.findParentByType('formpanel');
						formPanel.getForm().submit(Ext.applyIf({
							url: 'marketDataAllocatedValueForSecurityPreview.json',
							waitMsg: 'Previewing...',
							success: function(form, action) {
								const result = Ext.decode(action.response.responseText);
								const previewResultArea = TCG.getChildByName(form.ownerCt, 'previewResult');
								previewResultArea.setValue(result.data);
							}
						}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
					}
				}
			]
		},
		{xtype: 'textarea', height: 250, width: 660, readOnly: true, name: 'previewResult'}
	]
});

