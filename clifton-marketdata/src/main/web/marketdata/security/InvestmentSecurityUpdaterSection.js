Clifton.marketdata.security.InvestmentSecurityUpdaterSection = {
	title: 'Security Updater',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [
		{
			xtype: 'formpanel',
			labelWidth: 140,
			loadValidation: false, // using the form only to get background color/padding
			buttonAlign: 'right',
			instructions: 'Load investment security properties from selected market data provider that are different from these in the system. After verifying the data, click Save to update selected attributes.',
			listeners: {
				afterrender: function() {
					const form = this.getForm();
					const loader = new TCG.data.JsonLoader({
						params: {
							tableName: 'InvestmentSecurity'
						},
						onLoad: function(record, conf) {
							const field = form.findField('column.table.name');
							if (field) {
								field.setValue({value: record.id, text: record.name});
								field.originalValue = field.getValue();
							}
						}
					});
					loader.load('systemTableByName.json', form);
				}
			},
			items: [
				{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceName', value: 'Bloomberg', displayField: 'name', valueField: 'name', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', allowBlank: false},
				{fieldLabel: 'Data Field Group', name: 'dataFieldGroupName', hiddenName: 'dataFieldGroupId', xtype: 'combo', url: 'marketDataFieldGroupListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldGroupWindow'},
				{fieldLabel: 'Investment Hierarchy', name: 'instrumentHierarchy.labelExpanded', hiddenName: 'instrumentHierarchyId', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow'},
				{fieldLabel: 'Instrument Group', name: 'investmentGroup.name', hiddenName: 'investmentGroupId', xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
				{
					fieldLabel: 'System Table',
					name: 'column.table.name',
					hiddenName: 'column.table.id',
					submitValue: false,
					xtype: 'combo',
					url: 'systemTableListFind.json?defaultDataSource=true',
					listeners: {
						select: function(combo, record, index) {
							const fp = combo.getParentForm();
							fp.getForm().findField('column.name').clearAndReset();
							fp.getForm().findField('template.name').clearAndReset();
						}
					}
				},
				{
					fieldLabel: 'System Column Template',
					name: 'template.name',
					hiddenName: 'templateId',
					displayField: 'label',
					xtype: 'combo',
					url: 'systemColumnTemplateListFind.json',
					disableAddNewItem: true,
					detailPageClass: 'Clifton.system.schema.TemplateColumnWindow',
					mutuallyExclusiveFields: ['column.name'],
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							const groups = TCG.data.getData('systemColumnGroupListByTable.json?tableId=' + f.findField('column.table.id').getValue(), queryEvent.combo);
							const colIds = [];
							if (groups.length > 0) {
								for (let i = 0; i < groups.length; i++) {
									colIds[i] = groups[i].id;
								}
							}
							else {
								colIds[0] = 0;
							}

							combo.store.baseParams = {
								systemColumnGroupIds: colIds
							};
						}
					}
				},
				{
					fieldLabel: 'System Column',
					name: 'column.name',
					hiddenName: 'columnId',
					displayField: 'customLabel',
					xtype: 'combo',
					url: 'systemColumnListFind.json',
					disableAddNewItem: true,
					detailPageClass: 'Clifton.system.schema.ColumnWindow',
					mutuallyExclusiveFields: ['template.name'],
					listeners: {
						beforequery: function(queryEvent) {
							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							combo.store.baseParams = {
								tableId: f.findField('column.table.id').getValue()
							};
							const linkedValue = f.findField('instrumentHierarchy.labelExpanded').getValue();
							if (linkedValue) {
								combo.store.baseParams.linkedValue = linkedValue;
								combo.store.baseParams.includeNullLinkedValue = true;
							}
						}
					}
				},

				{fieldLabel: '', boxLabel: 'Include inactive securities', name: 'updateInactiveSecurities', xtype: 'checkbox', mutuallyExclusiveFields: ['updateSecurities']}
			],
			buttons: [{
				text: 'Load Values',
				iconCls: 'run',
				width: 150,
				handler: function() {
					const owner = this.findParentByType('formpanel');
					const form = owner.getForm();
					const grid = Ext.getCmp('marketDataInvestmentSecurityUpdateResultGrid');
					const store = grid.grid.store;

					form.submit(Ext.applyIf({
						url: 'marketDataInvestmentSecurityFieldDataPreview.json?includeUnchanged=false',
						waitMsg: 'Loading...',
						success: function(form, action) {
							store.removeAll();
							store.loadData(action.result, true);
						}
					}, Ext.applyIf({timeout: 900}, TCG.form.submitDefaults)));
				}
			}]
		},

		{
			flex: 1,
			xtype: 'panel',
			layout: 'fit',
			layoutConfig: {align: 'stretch'},
			items: [{
				layout: 'vbox',
				xtype: 'gridpanel',
				id: 'marketDataInvestmentSecurityUpdateResultGrid',
				rowSelectionModel: 'checkbox',

				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Security', width: 30, dataIndex: 'security.symbol', defaultSortColumn: true, defaultSortDirection: 'ASC'},
					{header: 'Field', width: 50, dataIndex: 'dataFieldColumnMapping.dataField.name'},
					{header: 'Column', width: 50, dataIndex: 'dataFieldColumnMapping.column.name'},
					{header: 'Orig. Text', width: 50, dataIndex: 'originalText', hidden: true},
					{header: 'Orig. Value', width: 50, dataIndex: 'originalValue'},
					{header: 'New Text', width: 50, dataIndex: 'newText', hidden: true},
					{header: 'New Value', width: 50, dataIndex: 'newValue'}
				],
				isPagingEnabled: function() {
					return false;
				},
				saveFields: function() {
					const gridPanel = this;
					const grid = gridPanel.grid;
					const itemList = [];

					const sm = grid.getSelectionModel();
					const ut = sm.getSelections();

					for (let i = 0; i < ut.length; i++) {
						const data = ut[i].json;
						const item = {};

						item.id = ut[i].id;
						item['newValue'] = data.newValue;
						if (data.newText) {
							item['newText'] = data.newText;
						}

						item['originalValue'] = data.originalValue;
						if (data.origonalText) {
							item['originalText'] = data.originalText;
						}

						item['security.id'] = data.security.id;
						item['dataFieldColumnMapping.id'] = data.dataFieldColumnMapping.id;

						itemList.push(item);
					}

					this.doSaveFields(itemList, 0, gridPanel);
				},
				doSaveFields: function(itemList, count, gridPanel) {
					if (itemList.length > 0) {
						const item = itemList[count];
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Saving...',
							waitTarget: this,
							params: item,
							timeout: 120000,
							onLoad: function(record, conf) {
								count++;
								gridPanel.grid.getStore().removeAt(gridPanel.grid.getStore().indexOfId(item.id));
								if (count === itemList.length) { // refresh after all trades were transitioned
									gridPanel.grid.getSelectionModel().clearSelections(true);
								}
								else {
									gridPanel.doSaveFields(itemList, count, gridPanel);
								}
								gridPanel.updateCount();
							},
							onFailure: function() {
								// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
							}
						});
						loader.load('marketDataInvestmentSecurityFieldUpdateResultSave.json');
					}
				},
				createStore: function(fields) {
					const gridPanel = this;
					const storeConf = {
						fields: fields,
						listeners: {
							datachanged: function(store, records, opts) {
								gridPanel.updateCount();
							}
							, load: function(store, records, opts) {
								// do nothing
							}, scope: this
						}
					};
					const storeClass = TCG.data.JsonStore;

					return new storeClass(storeConf);
				},
				editor: {
					drillDownOnly: true,
					addEnabled: false,
					addEditButtons: function(t, gridPanel) {
						t.add({
							text: 'Save',
							tooltip: '',
							iconCls: 'disk',
							scope: this,
							handler: function() {
								gridPanel.saveFields();
							}
						});
						t.add('-');

						t.add({
							text: 'Remove',
							tooltip: 'Remove selected item(s)',
							iconCls: 'remove',
							scope: this,
							handler: function() {
								const grid = gridPanel.grid;

								const sm = grid.getSelectionModel();
								const ut = sm.getSelections();

								for (let i = 0; i < ut.length; i++) {
									gridPanel.grid.getStore().removeAt(grid.getStore().indexOfId(ut[i].id));
								}
								gridPanel.updateCount();
							}
						});
						t.add('-');

						TCG.grid.GridEditor.prototype.addEditButtons.call(this, t);
					}
				}
			}]
		}
	]
};
