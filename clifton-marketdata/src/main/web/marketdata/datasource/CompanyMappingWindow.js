Clifton.marketdata.datasource.CompanyMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Company Mapping',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Mappings for data-source-specific company names can be entered here. For example, on intraday trade files, executing brokers are specified by various names.  We can connect those names to the correct internal companies via a mapping',
		url: 'marketDataSourceCompanyMapping.json',
		labelFieldName: 'dataSourceSymbol',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Data Source', name: 'marketDataSource.name', xtype: 'displayfield'},
			{xtype: 'hidden', name: 'marketDataSource.id'},
			{fieldLabel: 'Data Source Purpose', name: 'marketDataSourcePurpose.name', hiddenName: 'marketDataSourcePurpose.id', xtype: 'combo', url: 'marketDataSourcePurposeListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow'},
			{fieldLabel: 'Company', name: 'businessCompany.name', hiddenName: 'businessCompany.id', xtype: 'combo', url: 'businessCompanyListFind.json?tradingAllowed=true', disableAddNewItem: true, detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'External Company Name', name: 'externalCompanyName'}
		]
	}]
});
