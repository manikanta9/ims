Clifton.marketdata.datasource.SecuritySymbolOverrideWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Source Symbol Override',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Different market data providers (data sources) use the same security symbols and other attributes. In rare cases, when a different symbol is used by a particular data source, this entity can map it to our security which uses default data source conventions (symbol, etc.).',
		url: 'marketDataSourceSecurity.json',
		labelFieldName: 'dataSourceSymbol',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Data Source', name: 'dataSource.name', hiddenName: 'dataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Security', name: 'security.label', hiddenName: 'security.id', xtype: 'combo', displayField: 'label', url: 'investmentSecurityListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Data Source Symbol', name: 'dataSourceSymbol'},
			{fieldLabel: 'Overrides for Latest', name: 'externalFieldOverridesForLatest'},
			{fieldLabel: 'Overrides for Historic', name: 'externalFieldOverridesForHistoric'}
		]
	}]
});
