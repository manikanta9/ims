Clifton.marketdata.datasource.DataSourcePurposeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Source Purpose',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Purposed for data-source-specific mappings, such as MarketDataSourceExchangeMapping and MarketDataSourceCompanyMapping, can be entered here.',
		url: 'marketDataSourcePurpose.json',
		labelWidth: 200,
		items: [
			{fieldLabel: 'Purpose Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Company Mapping Modify Condition', name: 'companyMappingModifyCondition.name', hiddenName: 'companyMappingModifyCondition.id', xtype: 'system-entity-modify-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=MarketDataSourceCompanyMapping',
				optionalConditionForNonAdminQtip: 'This optional modify condition provides additional restrictions for non-admin users when updating a Market Data Source Company Mapping entity associated with this Market Data Source Purpose.'},
			{fieldLabel: 'Price Multiplier Modify Condition', name: 'priceMultiplierModifyCondition.name', hiddenName: 'priceMultiplierModifyCondition.id', xtype: 'system-entity-modify-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=MarketDataSourcePriceMultiplier',
				optionalConditionForNonAdminQtip: 'This optional modify condition provides additional restrictions for non-admin users when updating a Market Data Source Price Multiplier entity associated with this Market Data Source Purpose.'}
		]
	}]
});
