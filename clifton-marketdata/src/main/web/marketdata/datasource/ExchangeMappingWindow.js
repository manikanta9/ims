Clifton.marketdata.datasource.ExchangeMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Exchange Mapping',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Mappings for data-source-specific exchange code can be entered here.',
		url: 'marketDataSourceExchangeMapping.json',
		labelFieldName: 'dataSourceSymbol',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Data Source', name: 'marketDataSource.name', xtype: 'displayfield'},
			{xtype: 'hidden', name: 'marketDataSource.id'},
			{fieldLabel: 'Data Source Purpose', name: 'marketDataSourcePurpose.name', hiddenName: 'marketDataSourcePurpose.id', xtype: 'combo', url: 'marketDataSourcePurposeListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow'},
			{fieldLabel: 'Exchange', name: 'exchange.label', hiddenName: 'exchange.id', xtype: 'combo', displayField: 'label', url: 'investmentExchangeListFind.json', disableAddNewItem: true, detailPageClass: 'Clifton.investment.setup.ExchangeWindow'},
			{fieldLabel: 'Exchange Code', name: 'exchangeCode'}
		]
	}]
});
