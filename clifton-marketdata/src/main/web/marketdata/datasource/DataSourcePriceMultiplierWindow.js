Clifton.marketdata.datasource.DataSourcePriceMultiplierWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Source Price Multiplier',

	items: [{
		xtype: 'formpanel',
		url: 'marketDataSourcePriceMultiplier.json',
		labelWidth: 160,
		instructions: 'Some market data sources may use non-industry-standard price multipliers. Our system will apply the following additional multipliers to their prices sent to us.',
		items: [
			{fieldLabel: 'Data Source', name: 'marketDataSource.name', hiddenName: 'marketDataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Data Source Purpose', name: 'marketDataSourcePurpose.name', hiddenName: 'marketDataSourcePurpose.id', xtype: 'combo', url: 'marketDataSourcePurposeListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow'},
			{fieldLabel: 'Instrument', name: 'investmentInstrument.name', hiddenName: 'investmentInstrument.id', xtype: 'combo', displayField: 'label', url: 'investmentInstrumentListFind.json', detailPageClass: 'Clifton.investment.instrument.InstrumentWindow'},
			{fieldLabel: 'Additional Price Multiplier', name: 'additionalPriceMultiplier', xtype: 'floatfield'}
		]
	}]
});


