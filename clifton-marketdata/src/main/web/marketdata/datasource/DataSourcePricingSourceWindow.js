Clifton.marketdata.datasource.DataSourcePricingSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Source Pricing Source',

	items: [{
		xtype: 'formpanel',
		url: 'marketDataSourcePricingSource.json',
		instructions: 'Some market data sources may optionally provide access to different pricing sources. This section defines all of these options and different parts of our system can be configured to use them as opposed to default pricing source.',
		items: [
			{fieldLabel: 'Pricing Source', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'Data Source', name: 'marketDataSource.name', hiddenName: 'marketDataSource.id', xtype: 'combo', url: 'marketDataSourceListFind.json', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Symbol', name: 'symbol'}
		]
	}]
});


