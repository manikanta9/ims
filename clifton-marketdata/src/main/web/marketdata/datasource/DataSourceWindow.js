Clifton.marketdata.datasource.DataSourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Data Source',
	iconCls: 'numbers',
	width: 1100,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Data Source',
				items: [{
					xtype: 'formpanel',
					instructions: 'Market data source represents a company or a tool that we use to obtain financial information like investment security prices and/or exchange rates.',
					url: 'marketDataSource.json',
					items: [
						{fieldLabel: 'Company', name: 'company.name', hiddenName: 'company.id', xtype: 'combo', url: 'businessCompanyListFind.json', detailPageClass: 'Clifton.business.company.CompanyWindow'},
						{fieldLabel: 'Security System', name: 'securitySystem.name', hiddenName: 'securitySystem.id', xtype: 'combo', url: 'securitySystemListFind.json', detailPageClass: 'Clifton.security.system.SystemWindow', qtip: 'Optional SecuritySystem linked to this MarketDataSource. Example: Bloomberg MarketDataSource is linked to Bloomberg SecuritySystem to look up Bloomberg user properties.'},
						{fieldLabel: 'Data Source Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'FX Fixing Time', name: 'fxFixingTime.text', hiddenName: 'fxFixingTime.id', valueField: 'id', xtype: 'system-list-combo', listName: 'Market Data: FX Fixing Time', qtip: 'If defined, this Data Source is available as a source for Exchange Rates (validation and filtering). Specifies the time when exchange rates are recorded: usually "London Close" or "US Close".'},
						{boxLabel: 'Allow Interest Rates for this Data Source', name: 'interestRateSupported', xtype: 'checkbox'},
						{boxLabel: 'Allow Market Data Values for this Data Source', name: 'marketDataValueSupported', xtype: 'checkbox'},
						{boxLabel: 'This data source organizes securities into market sectors', name: 'marketSectorSupported', xtype: 'checkbox'},
						{boxLabel: 'This is Default data source used by the system', name: 'defaultDataSource', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Market Sectors',
				items: [{
					name: 'marketDataSourceSectorListByDataSource',
					xtype: 'gridpanel',
					instructions: 'Market sectors define different market sectors available for selected data source: Equity, Currency, Government Bonds, etc.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Sector Name', width: 100, dataIndex: 'name'},
						{header: 'Label', width: 100, dataIndex: 'label'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					getLoadParams: function() {
						return {dataSourceId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.MarketSectorWindow',
						deleteURL: 'marketDataSourceSectorDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								dataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Symbol Overrides',
				items: [{
					name: 'marketDataSourceSecurityListByDataSource',
					xtype: 'gridpanel',
					instructions: 'Different market data providers (data sources) use the same security symbols and other attributes. In rare cases, when a different symbol is used by a particular data source, this entity can map it to our security which uses default data source conventions (symbol, etc.).',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Hierarchy', width: 250, dataIndex: 'security.instrument.hierarchy.labelExpanded'},
						{header: 'Symbol', width: 120, dataIndex: 'security.symbol'},
						{header: 'Security Name', width: 200, dataIndex: 'security.name'},
						{header: 'Data Source Symbol', width: 120, dataIndex: 'dataSourceSymbol'}
					],
					getLoadParams: function() {
						return {dataSourceId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.SecuritySymbolOverrideWindow',
						deleteURL: 'marketDataSourceSecurityDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								dataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Pricing Sources',
				items: [{
					name: 'marketDataSourcePricingSourceListFind',
					xtype: 'gridpanel',
					instructions: 'Some market data sources may optionally provide access to different pricing sources. This section defines all of these options and different parts of our system can be configured to use them as opposed to default pricing source.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Pricing Source', width: 150, dataIndex: 'name'},
						{header: 'Symbol', width: 100, dataIndex: 'symbol'}
					],
					getLoadParams: function() {
						return {marketDataSourceId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePricingSourceWindow',
						deleteURL: 'marketDataSourcePricingSourceDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								marketDataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Price Multipliers',
				items: [{
					name: 'marketDataSourcePriceMultiplierListFind',
					xtype: 'gridpanel',
					instructions: 'Some market data sources may use non-industry-standard price multipliers. Our system will apply the following additional multipliers to their prices sent to us.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Instrument', width: 200, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Additional Multiplier', width: 100, dataIndex: 'additionalPriceMultiplier', type: 'float'},
						{header: 'Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {type: 'combo', searchFieldName: 'marketDataSourcePurposeId', displayField: 'label', url: 'marketDataSourcePurposeListFind.json'}}
					],
					getLoadParams: function() {
						return {marketDataSourceId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePriceMultiplierWindow',
						deleteURL: 'marketDataSourcePriceMultiplierDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								marketDataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Company Mappings',
				items: [{
					xtype: 'marketdata-datasource-company-mapping-grid'
				}]
			},


			{
				title: 'Exchange Mappings',
				items: [{
					xtype: 'marketdata-datasource-exchange-mapping-grid'
				}]
			},


			{
				title: 'Field Value Mappings',
				items: [{
					xtype: 'marketdata-field-value-mapping-grid',
					getLoadParams: function(firstLoad) {
						return {
							dataSourceId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldValueMappingWindow',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								dataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Column Mappings',
				items: [{
					xtype: 'marketdata-field-column-mapping-grid',
					getLoadParams: function(firstLoad) {
						return {
							dataSourceId: this.getWindow().getMainFormId()
						};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.column.DataFieldColumnMappingWindow',
						getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
							return {
								dataSource: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			}
		]
	}]
});
