Clifton.marketdata.datasource.MarketSectorWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Market Sector',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Market sectors define different market sectors available for selected data source: Equity, Currency, Government Bonds, etc.',
		url: 'marketDataSourceSector.json',
		items: [
			{fieldLabel: 'Data Source', name: 'dataSource.label', xtype: 'displayfield'},
			{xtype: 'hidden', name: 'dataSource.id'},
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Label', name: 'label'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
		]
	}]
});
