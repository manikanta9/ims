Clifton.marketdata.rates.ExchangeRateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Exchange Rate',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'This section collects historic exchange rates from one currency to another.<br /><br />&nbsp; 1 From Currency Unit = Exchange Rate * 1 To Currency Unit',
		url: 'marketDataExchangeRate.json',
		items: [
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json?exchangeRateSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'From', name: 'fromCurrency.label', hiddenName: 'fromCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'To', name: 'toCurrency.label', hiddenName: 'toCurrency.id', displayField: 'label', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
			{fieldLabel: 'Date', name: 'rateDate', xtype: 'datefield'},
			{fieldLabel: 'Exchange Rate', name: 'exchangeRate'}
		]
	}]
});
