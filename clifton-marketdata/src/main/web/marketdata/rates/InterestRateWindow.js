Clifton.marketdata.rates.InterestRateWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate',
	iconCls: 'numbers',

	items: [{
		xtype: 'formpanel',
		instructions: 'Interest rates that were reported by the specified source on the specified date.',
		url: 'marketDataInterestRate.json',
		items: [
			{fieldLabel: 'Index', name: 'interestRateIndex.label', hiddenName: 'interestRateIndex.id', displayField: 'label', xtype: 'combo', loadAll: true, url: 'investmentInterestRateIndexListFind.json', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow'},
			{fieldLabel: 'Data Source', name: 'dataSource.label', hiddenName: 'dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json?interestRateSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
			{fieldLabel: 'Date', name: 'interestRateDate', xtype: 'datefield'},
			{fieldLabel: 'Rate Percent', name: 'interestRate'}
		]
	}]
});
