Clifton.marketdata.rates.InterestRateIndexMappingWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Index Mapping',
	iconCls: 'numbers',
	items: [{
		xtype: 'formpanel',
		instructions: 'Maps an Interest rate index to a specific data source/sector to load reported data.  Symbol can be used to override the internal default symbol if different.',
		url: 'marketDataInterestRateIndexMapping.json',
		labelWidth: 150,
		items: [
			{fieldLabel: 'Index', name: 'referenceOne.label', hiddenName: 'referenceOne.id', displayField: 'label', xtype: 'combo', url: 'investmentInterestRateIndexListFind.json?active=true', detailPageClass: 'Clifton.investment.rates.InterestRateIndexWindow'},
			{fieldLabel: 'Data Source', name: 'referenceTwo.dataSource.label', hiddenName: 'referenceTwo.dataSource.id', displayField: 'label', xtype: 'combo', url: 'marketDataSourceListFind.json?interestRateSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', submitValue: false},
			{
				fieldLabel: 'Market Sector', name: 'referenceTwo.label', hiddenName: 'referenceTwo.id', displayField: 'label', xtype: 'combo', loadAll: true, url: 'marketDataSourceSectorListByDataSource.json', detailPageClass: 'Clifton.marketdata.datasource.MarketSectorWindow',
				requiredFields: ['referenceTwo.dataSource.id'],
				beforequery: function(queryEvent) {
					const form = TCG.getParentFormPanel(queryEvent.combo).getForm();
					queryEvent.combo.store.baseParams = {dataSourceId: form.findField('referenceTwo.dataSource.id').value};
				},
				getDefaultData: function(f) { // defaults table for "add new" drill down
					const t = f.findField('referenceTwo.dataSource.id');
					return {
						dataSource: {
							id: t.getValue(),
							name: t.getRawValue()
						}
					};
				}
			},
			{fieldLabel: 'Symbol Override', name: 'dataSourceSymbol', qtip: 'Optional override of the index symbol if different for this datasource sector mapping.'},
			{fieldLabel: 'Value Transformation Bean', name: 'valueTransformationBean.name', hiddenName: 'valueTransformationBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Market Data Value Transformer', detailPageClass: 'Clifton.system.bean.BeanWindow'}
		]
	}]
});
