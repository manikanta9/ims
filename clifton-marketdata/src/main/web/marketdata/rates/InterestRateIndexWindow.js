// Override of the Clifton.investment.rates.BaseInterestRateIndexWindow so we can show actual rates
Clifton.marketdata.rates.InterestRateIndexWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Interest Rate Index',
	iconCls: 'numbers',
	width: 700,
	height: 400,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'investmentInterestRateIndex.json',

					getWarningMessage: function(form) {
						let msg = undefined;
						if (!TCG.isTrue(form.formValues.active)) {
							msg = 'This index is currently inactive.';
						}
						return msg;
					},
					items: [
						{fieldLabel: 'Index Name', name: 'name'},
						{fieldLabel: 'ISDA Name', name: 'isdaName', qtip: 'The International Swaps and Derivatives Association (ISDA) Definition for the interest or swap rate reference'},
						{
							xtype: 'columnpanel',
							columns: [
								{
									rows: [
										{fieldLabel: 'Ticker Symbol', name: 'symbol', xtype: 'textfield', qtip: 'Default or Internal Ticker Symbol for this index.  When mapping to market data source sectors this symbol can be overridden for data source specific symbols.'},
										{fieldLabel: 'Fixing Term', name: 'fixingTerm', xtype: 'textfield', qtip: 'The maturity of the interest or swap rate e.g. 28D, 1M, 3M, 6M, 1Y'},
										{fieldLabel: 'CCY Denomination', name: 'currencyDenomination.name', hiddenName: 'currencyDenomination.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
										{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'}
									]
								},
								{
									rows: [
										{fieldLabel: 'Order', name: 'order', xtype: 'spinnerfield', maxValue: 100000},
										{fieldLabel: 'Days', name: 'numberOfDays', xtype: 'spinnerfield', maxValue: 100000},
										{fieldLabel: 'Calendar', name: 'calendar.name', hiddenName: 'calendar.id', xtype: 'combo', url: 'calendarListFind.json', detailPageClass: 'Clifton.calendar.setup.CalendarWindow'},
										{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
									]
								}
							]
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{boxLabel: 'Index rates are compounded', name: 'compounded', xtype: 'checkbox', qtip: 'A compounded index indicates its rates are compounded often from the start of the index. For example, the Secured Overnight Financing Rate (SOFR) compounded index has a daily rate compounded since April 2, 2018.'}
					]
				}]
			},


			{
				title: 'Interest Rates',
				items: [{
					xtype: 'gridpanel',
					name: 'marketDataInterestRateListFind',
					instructions: 'Interest rates that were reported on the specified date.',
					importTableName: 'MarketDataInterestRate',
					getLoadParams: function() {
						return {
							interestRateIndexId: this.getWindow().getMainFormId()
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'CCY Denomination', width: 100, dataIndex: 'interestRateIndex.currencyDenomination.label', filter: {searchFieldName: 'currencyLabel'}},
						{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {searchFieldName: 'dataSourceName'}},
						{header: 'Date', width: 50, dataIndex: 'interestRateDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Rate %', width: 50, dataIndex: 'interestRate', type: 'float'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.rates.InterestRateWindow',
						deleteURL: 'marketDataInterestRateDelete.json',
						getDefaultData: function(gridPanel) {
							return {
								interestRateIndex: gridPanel.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			},


			{
				title: 'Market Data Mappings',
				items: [{
					name: 'marketDataInterestRateIndexMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Interest rate mappings map interest rate indices to corresponding data source market sectors. If the data source uses a different symbol for the interest rate, the symbol can be overridden here.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 100, dataIndex: 'referenceTwo.dataSource.label', filter: {searchFieldName: 'dataSourceName'}, defaultSortColumn: true},
						{header: 'Market Sector', width: 100, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'dataSourceSectorName'}},
						{header: 'Symbol Override', width: 60, dataIndex: 'dataSourceSymbol', tooltip: 'If not defined, uses the index symbol'},
						{header: 'Value Transformation Bean', width: 60, dataIndex: 'valueTransformationBean.name', hidden: true, filter: {type: 'combo', searchFieldName: 'valueTransformationBeanId', url: 'systemBeanListFind.json?groupName=Market Data Value Transformer'}},
						{header: 'Active', width: 50, dataIndex: 'referenceOne.active', type: 'boolean', filter: {searchFieldName: 'interestRateIndexActive'}, tooltip: 'Interest Rate Index Active Today'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.rates.InterestRateIndexMappingWindow',
						deleteURL: 'marketDataInterestRateIndexMappingDelete.json',
						getDefaultData: function(gridPanel) { // defaults interestRateIndex for the detail page
							return {
								referenceOne: gridPanel.getWindow().getMainForm().formValues
							};
						}
					},
					getLoadParams: function(firstLoad) {
						return {interestRateIndexId: this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
