Clifton.marketdata.MarketDataWindow = Ext.extend(TCG.app.Window, {
	id: 'marketDataWindow',
	title: 'Market Data',
	iconCls: 'numbers',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Market Data',
				items: [{
					name: 'marketDataValueListFind',
					xtype: 'gridpanel',
					instructions: 'This section collects historic market data measures for specific market data fields of investment securities',
					importTableName: 'MarketDataValue',
					importComponentName: 'Clifton.marketdata.field.upload.MarketDataValueUploadWindow',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Type', width: 70, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeSubTypeId', displayField: 'name', url: 'investmentTypeSubTypeListFind.json', loadAll: true}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'investmentSecurity.instrument.hierarchy.investmentTypeSubType2.name', filter: {type: 'combo', searchFieldName: 'investmentTypeSubType2Id', displayField: 'name', url: 'investmentTypeSubType2ListFind.json', loadAll: true}, hidden: true},
						{header: 'Investment Hierarchy', width: 210, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}},
						{header: 'Symbol', width: 80, dataIndex: 'investmentSecurity.symbol', filter: {searchFieldName: 'symbol'}},
						{header: 'CUSIP', width: 70, dataIndex: 'investmentSecurity.cusip', filter: {searchFieldName: 'cusip'}},
						{header: 'ISIN', width: 70, dataIndex: 'investmentSecurity.isin', filter: {searchFieldName: 'isin'}, hidden: true},
						{header: 'SEDOL', width: 70, dataIndex: 'investmentSecurity.sedol', filter: {searchFieldName: 'sedol'}, hidden: true},
						{
							header: 'FIGI', width: 70, dataIndex: 'investmentSecurity.figi', filter: {searchFieldName: 'figi'}, hidden: true,
							tooltip: 'Financial Instrument Global Identifier (FIGI; also known as the Bloomberg Global Identifier (BBGID)) is an open standard, 12 character unique identifier assigned to all financial instruments and will not change once issued. For equity instruments, an identifier is issued per trading venue.'
						},
						{header: 'OCC Symbol', width: 70, dataIndex: 'investmentSecurity.occSymbol', filter: {searchFieldName: 'occSymbol'}, hidden: true},
						{header: 'Date', width: 50, dataIndex: 'measureDateWithTime', type: 'date', filter: {searchFieldName: 'measureDate'}},
						{header: 'Time', width: 80, dataIndex: 'measureTime', align: 'center', hidden: true, filter: false},
						{
							header: 'Value', width: 70, dataIndex: 'measureValue', type: 'float',
							renderer: function(v, metaData, r) {
								const value = TCG.numberFormat(v, '0,000', true);
								if (r.json.createDate !== r.json.updateDate) {
									return '<div class="amountAdjusted" qtip="Market Data was updated. See Audit Trail for details.">' + value + '</div>';
								}
								return value;
							}
						},
						{header: 'Field', width: 70, dataIndex: 'dataField.name', filter: {type: 'combo', searchFieldName: 'dataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Field Order', width: 50, dataIndex: 'dataField.fieldOrder', filter: {searchFieldName: 'dataFieldOrder'}, hidden: true},
						{header: 'Data Source', hidden: true, width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
						{header: 'Real-Time Data', width: 30, dataIndex: 'dataField.realTimeReferenceData', hidden: true, type: 'boolean', filter: {searchFieldName: 'realTimeReferenceData'}}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Market Data Tags'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad && !this.isFilterValueSet('measureDateWithTime')) {
							// default to last 2 business days of data
							this.setFilterValue('measureDateWithTime', {'after': Clifton.calendar.getBusinessDayFrom(-2)});
						}
						const lp = {};
						const v = TCG.getChildByName(this.getTopToolbar(), 'investmentGroupId').getValue();
						if (v) {
							lp.investmentGroupId = v;
						}

						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							lp.dataFieldCategoryHierarchyId = tag.getValue();
						}
						return lp;
					},
					configureToolsMenu: function(menu) {
						menu.add('-');
						menu.add({
							text: 'Market Data Setup',
							iconCls: 'numbers',
							scope: this,
							handler: function() {
								TCG.createComponent('Clifton.marketdata.MarketDataSetupWindow', {openerCt: this});
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataValueWindow',
						allowToDeleteMultiple: true
					}
				}]
			},


			{
				title: 'Missing Data',
				items: [{
					name: 'investmentSecurityListFind',
					xtype: 'gridpanel',
					instructions: 'This section identifies securities that are active on the specified date (uses Daily Positions Snapshot) and do not have market data on that date for the specified field.  Also checks and excludes securities if the given date is a holiday for the security',
					importTableName: 'MarketDataValue',
					importComponentName: 'Clifton.marketdata.field.upload.MarketDataValueUploadWindow',
					reloadOnRender: false,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Type', width: 60, dataIndex: 'instrument.hierarchy.investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', displayField: 'name', url: 'investmentTypeList.json', loadAll: true}},
						{header: 'Sub Type', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}, hidden: true},
						{header: 'Sub Type 2', width: 60, dataIndex: 'instrument.hierarchy.investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}, hidden: true},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'instrument.hierarchy.labelExpanded', defaultSortColumn: true, filter: {searchFieldName: 'hierarchyName'}},
						{header: 'Underlying', width: 100, dataIndex: 'instrument.underlyingInstrument.name', filter: {type: 'combo', searchFieldName: 'underlyingInstrumentId', displayField: 'name', url: 'investmentInstrumentListFind.json'}, hidden: true},
						{header: 'Counterparty', width: 100, dataIndex: 'businessCompany.name', filter: {type: 'combo', searchFieldName: 'businessCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Symbol', width: 50, dataIndex: 'symbol'},
						{header: 'CUSIP', width: 50, dataIndex: 'cusip'},
						{header: 'Security Name', width: 100, dataIndex: 'name'},
						{header: 'CCY Denomination', width: 50, dataIndex: 'instrument.tradingCurrency.name', filter: {type: 'combo', searchFieldName: 'tradingCurrencyId', displayField: 'name', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'First Trade', width: 40, dataIndex: 'startDate', hidden: true},
						{header: 'Last Trade', width: 40, dataIndex: 'endDate'},
						{header: 'Early Termination Date', width: 50, dataIndex: 'earlyTerminationDate', hidden: true}
					],
					getLoadParams: function(first) {
						const tb = this.getTopToolbar();
						const dateFilter = TCG.getChildByName(tb, 'activeOnDate');
						if (TCG.isBlank(dateFilter.getValue())) {
							TCG.showError('Date is required.  Please enter a specific date to find missing market data on.');
							return false;
						}
						const dateVal = dateFilter.getValue().format('m/d/Y');
						const fieldType = TCG.getChildByName(tb, 'noMarketDataFieldOnDateType').getValue().getGroupValue();
						const fieldVal = TCG.getChildByName(tb, 'noMarketDataFieldOnDateId').getValue();

						if (TCG.isBlank(dateVal) || TCG.isBlank(fieldType)) {
							TCG.showError('You must first select Date and either Closing Price, Latest Price, or a specific Field that will be checked.');
							return false;
						}
						const activePosOnly = TCG.isTrue(TCG.getChildByName(tb, 'activePositionsOnly').getValue());
						const secGroupId = TCG.getChildByName(tb, 'investmentGroupId').getValue();

						const params = {
							activeOnDate: dateVal,
							noHolidayOnDate: dateVal,
							noMarketDataFieldOnDateId: fieldVal,
							noMarketDataFieldOnDateType: fieldType,
							investmentGroupId: secGroupId
						};
						if (activePosOnly) {
							params.activePositionsOnDate = dateVal;
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = [];
						filters.push({fieldLabel: 'Date', xtype: 'datefield', name: 'activeOnDate', width: 80, allowBlank: false, value: Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y')});

						filters.push({
							xtype: 'radiogroup', columns: 3, width: 280, allowBlank: false, fieldLabel: '', name: 'noMarketDataFieldOnDateType',
							items: [
								{boxLabel: 'Closing Price', xtype: 'radio', name: 'noMarketDataFieldOnDateType', inputValue: 'Closing', checked: true},
								{boxLabel: 'Latest Price', xtype: 'radio', name: 'noMarketDataFieldOnDateType', inputValue: 'Latest'},
								{boxLabel: 'Specific Field:', xtype: 'radio', name: 'noMarketDataFieldOnDateType', inputValue: 'Specified'}
							],
							listeners: {
								change: function(rg, r) {
									const sel = r.getGroupValue();
									if (sel === 'Specified') {
										TCG.getChildByName(toolbar, 'noMarketDataFieldOnDateId').setDisabled(false);
									}
									else {
										TCG.getChildByName(toolbar, 'noMarketDataFieldOnDateId').reset();
										TCG.getChildByName(toolbar, 'noMarketDataFieldOnDateId').setDisabled(true);
									}
								}
							}
						});
						filters.push({xtype: 'combo', name: 'noMarketDataFieldOnDateId', width: 150, url: 'marketDataFieldListFind.json'});
						filters.push({fieldLabel: 'Instrument Group', xtype: 'combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'});
						filters.push({fieldLabel: 'Active Positions Only', xtype: 'checkbox', name: 'activePositionsOnly'});
						return filters;
					},
					editor: {
						detailPageClass: 'Clifton.investment.instrument.SecurityWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Exchange Rates',
				items: [{
					name: 'marketDataExchangeRateListFind',
					xtype: 'gridpanel',
					instructions: 'This section collects historic exchange rates from one currency to another.',
					importTableName: 'MarketDataExchangeRate',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 150, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?exchangeRateSupported=true'}},
						{header: 'From', width: 150, dataIndex: 'fromCurrency.label', filter: {type: 'combo', searchFieldName: 'fromCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'To', width: 150, dataIndex: 'toCurrency.label', filter: {type: 'combo', searchFieldName: 'toCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
						{header: 'Date', width: 80, dataIndex: 'rateDate'},
						{header: 'Value', width: 80, dataIndex: 'exchangeRate', type: 'currency', numberFormat: '0,000.000000000000'},
						{
							header: '1 / Value', width: 80, dataIndex: 'exchangeRate', type: 'float', filter: false,
							renderer: function(v) {
								return Ext.util.Format.number(1 / v, '0,000.000000000000');
							}
						}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of rates
							this.setFilterValue('rateDate', {'after': new Date().add(Date.DAY, -7)});
						}
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.rates.ExchangeRateWindow',
						allowToDeleteMultiple: true
					}
				}]
			},


			{
				title: 'Interest Rates',
				items: [{
					name: 'marketDataInterestRateListFind',
					xtype: 'gridpanel',
					instructions: 'Interest rate index is an index that tracks a rate that is charged or paid for the use of money. Index rates change over time. Rates of major indices are used to derive valuation of various investment securities.',
					importTableName: 'MarketDataInterestRate',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Index', width: 110, dataIndex: 'interestRateIndex.label', filter: {searchFieldName: 'interestRateIndexLabel'}},
						{header: 'CCY Denomination', width: 50, dataIndex: 'interestRateIndex.currencyDenomination.label', filter: {searchFieldName: 'currencyLabel'}},
						{header: 'Calendar', width: 80, dataIndex: 'interestRateIndex.calendar.name', filter: {searchFieldName: 'calendarName'}},
						{header: 'Days', width: 40, dataIndex: 'interestRateIndex.numberOfDays', type: 'int', filter: {searchFieldName: 'interestRateIndexDays'}},
						{header: 'Order', width: 40, dataIndex: 'interestRateIndex.order', type: 'int', hidden: true, filter: {searchFieldName: 'interestRateIndexOrder'}},
						{header: 'Data Source', width: 70, dataIndex: 'dataSource.name', filter: {searchFieldName: 'dataSourceName'}},
						{header: 'Date', width: 40, dataIndex: 'interestRateDate'},
						{
							header: 'Rate %', width: 40, dataIndex: 'interestRate', type: 'float',
							renderer: function(v, metaData, r) {
								if (r.json.createDate !== r.json.updateDate && r.json.updateUserId !== 1) {
									return '<div class="amountAdjusted" qtip="Market Data was updated. See Audit Trail for details.">' + v + '</div>';
								}
								return v;
							}
						}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.rates.InterestRateWindow',
						deleteURL: 'marketDataInterestRateDelete.json',
						allowToDeleteMultiple: true
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 7 days of rates
							this.setFilterValue('interestRateDate', {'after': new Date().add(Date.DAY, -7)});
						}
						return {};
					}
				}]
			},


			{
				title: 'Import Data',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false,
						validatedOnLoad: false,
						labelWidth: 130,
						buttonAlign: 'right',
						listeners: {
							afterrender: function() {
								this.updateWarningMessage('Carefully select filters in order to avoid excessive data requests and unintentional updates to existing market data.');
							}
						},
						items: [
							{xtype: 'sectionheaderfield', header: 'Data Field(s)', fieldLabel: ''},
							{fieldLabel: 'Data Source', name: 'dataSourceName', hiddenName: 'dataSourceName', value: 'Bloomberg', displayField: 'name', valueField: 'name', xtype: 'combo', url: 'marketDataSourceListFind.json?marketDataValueSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow', allowBlank: false},
							{fieldLabel: 'Data Field Group', name: 'dataFieldGroupName', hiddenName: 'dataFieldGroupId', xtype: 'combo', url: 'marketDataFieldGroupListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldGroupWindow'},
							{fieldLabel: 'Data Field', name: 'marketDataField', hiddenName: 'marketDataFieldId', xtype: 'combo', url: 'marketDataFieldListFind.json', detailPageClass: 'Clifton.marketdata.field.DataFieldWindow', qtip: 'If the field and group are not selected, will import the data for all fields mapped to the specified security.'},

							{xtype: 'sectionheaderfield', header: 'Investment Security(ies)', fieldLabel: ''},
							{fieldLabel: 'Instrument Group', name: 'investmentGroupName', hiddenName: 'investmentGroupId', xtype: 'combo', url: 'investmentGroupListFind.json', mutuallyExclusiveFields: ['investmentHierarchyName', 'securityGroupName', 'investmentSecurityName']},
							{fieldLabel: 'Security Group', name: 'securityGroupName', hiddenName: 'securityGroupId', xtype: 'combo', loadAll: true, url: 'investmentSecurityGroupListFind.json', mutuallyExclusiveFields: ['investmentGroupName', 'investmentHierarchyName', 'investmentSecurityName']},
							{fieldLabel: 'Investment Hierarchy', name: 'investmentHierarchyName', hiddenName: 'investmentHierarchyId', displayField: 'labelExpanded', xtype: 'combo', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', detailPageClass: 'Clifton.investment.setup.InstrumentHierarchyWindow', mutuallyExclusiveFields: ['investmentGroupName', 'securityGroupName', 'investmentSecurityName']},
							{fieldLabel: 'Investment Security', name: 'investmentSecurityName', hiddenName: 'investmentSecurityId', displayField: 'label', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json', mutuallyExclusiveFields: ['investmentGroupName', 'securityGroupName', 'investmentHierarchyName']},

							{xtype: 'sectionheaderfield', header: 'Date Range', fieldLabel: ''},
							{fieldLabel: 'From Date', name: 'startDate', xtype: 'datefield', allowBlank: false, value: Clifton.calendar.getBusinessDayFrom(-1)},
							{fieldLabel: 'To Date', name: 'endDate', xtype: 'datefield', allowBlank: false, value: Clifton.calendar.getBusinessDayFrom(-1)},

							{xtype: 'sectionheaderfield', header: 'Additional Options', fieldLabel: ''},
							{name: 'batchDataRequests', xtype: 'checkbox', boxLabel: 'Batch Data Requests'},
							{name: 'skipDataRequestIfAlreadyExists', xtype: 'checkbox', checked: true, boxLabel: 'Do not retrieve data if it already exists (minimizes calls to the data source but will not get changes to historically retrieved data)'},
							{name: 'overwriteExistingData', xtype: 'checkbox', boxLabel: 'Overwrites existing data if it already exists'},
							{name: 'runAsSystemUser', xtype: 'checkbox', checked: true, boxLabel: 'Run As "systemuser": will use systemuser for Created By and Updated By instead of current user (YOU).'},
							{
								xtype: 'fieldset',
								name: 'marketDataProviderFieldSet',
								collapsed: true,
								title: 'Provider Options',
								instructions: 'Used to specify the Market Data Provider to pull data from',
								items: [
									{
										fieldLabel: 'Provider Override', name: 'marketDataProviderOverride', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'marketDataProviderOverride',
										store: new Ext.data.ArrayStore({
											fields: ['name', 'valueField', 'description'],
											data: Clifton.marketdata.provider.OVERRIDE_OPTIONS
										})
									},
									{fieldLabel: 'Provider Timeout', name: 'marketDataProviderTimeoutOverride', qtip: 'Timeout (in milliseconds) that will override the default provider timeout'},
									{fieldLabel: 'Batch Size', name: 'marketDataProviderBatchSizeOverride', xtype: 'integerfield', minValue: 0, qtip: 'The number of securities to request at one time. If not specified then a request will be made for each security. If the value is 0 then all securities will be requested at once.'}
								]
							}
						],
						buttons: [{
							text: 'Import Data',
							iconCls: 'run',
							width: 150,
							handler: function() {
								const owner = this.findParentByType('formpanel');
								const form = owner.getForm();
								form.submit(Ext.applyIf({
									url: 'marketDataFieldValuesUpdate.json?synchronous=false',
									waitMsg: 'Updating Market Data ...',
									success: function(form, action) {
										Ext.Msg.alert('Processing Started', action.result.result.message, function() {
											const grid = owner.ownerCt.items.get(1);
											grid.reload.defer(300, grid);
										});
									}
								}, Ext.applyIf({timeout: 300}, TCG.form.submitDefaults)));
							}
						}]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'MARKETDATA-IMPORT',
						instantRunner: true,
						title: 'Current Processing',
						flex: 1
					}
				]
			},


			{
				title: 'External Loads',
				items: [{
					xtype: 'integration-importRunEventGrid',
					targetApplicationName: 'IMS',
					importEventName: 'Market Data Broker Daily Exchange Rates'
				}]
			},

			{
				title: 'Active Subscriptions',
				items: [{
					name: 'marketDataActiveUserSubscriptionList',
					xtype: 'gridpanel',
					rowSelectionModel: 'multiple',
					instructions: 'Market data subscription list showing active user subscriptions',
					wikiPage: 'IT/Bloomberg+BPipe+Subscriptions',
					appendStandardColumns: false,
					remoteSort: false,
					forceLocalFiltering: true,
					viewNames: ['By User', 'By Security'],
					defaultViewName: 'By User',
					groupField: 'securityUser.displayName',
					groupTextTpl: '<div ext:qtip="Right click to display the grouping context menu with additional features">{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Subscriptions" : "Subscription"]}</div>',

					switchToViewBeforeReload: function(viewName) {
						const groupPropertyName = viewName === this.defaultViewName ? 'securityUser.displayName' : 'security.symbol';
						this.groupField = groupPropertyName;
						this.ds.setDefaultSort(groupPropertyName, 'asc');
						this.grid.getStore().groupBy(groupPropertyName, false);
					},

					listeners: {
						afterrender: function(gridPanel) {
							this.setDefaultView(this.defaultViewName);
							gridPanel.addGridGroupSelectionContextMenu(gridPanel);
						}
					},

					editor: {
						addEditButtons: function(t, gridPanel) {
							t.add({
								iconCls: 'expand-all',
								tooltip: 'Expand or Collapse all groups',
								scope: this,
								handler: function() {
									gridPanel.expandCollapseGroups.call(gridPanel);
								}
							});

							t.add({
								name: 'securityUser.name', detailIdField: 'securityUser.id', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', emptyText: '< Select User >', displayField: 'displayName', minListWidth: '120',
								qtip: 'Select a user to manage subscriptions for. If a user is selected, actions will be performed for the user instead of by selections in the grid.'
							});

							const activeSubscriptionHandler = async (url, waitMsg, all) => {
								const processAll = TCG.isTrue(all);
								const requestParams = {processAll: processAll};
								let confirmMessage = 'Are you sure you want to performing ' + (url.includes('Unsubscribe') ? 'Unsubscribe' : 'Unlock') + ' for ';
								if (processAll) {
									confirmMessage += 'ALL Users.';
								}
								else {
									const userCombo = TCG.getChildByName(gridPanel.getTopToolbar(), 'securityUser.name');
									if (TCG.isNotBlank(userCombo.getValue())) {
										requestParams['securityUser.id'] = userCombo.getValue();
										confirmMessage += 'User (' + userCombo.lastSelectionText + ').';
									}
									else {
										const selectedRecords = gridPanel.grid.getSelectionModel().getSelections();
										if (selectedRecords.length === 0) {
											TCG.showError('Please select a subscription to unsubscribe.', 'No Row(s) Selected');
											return;
										}

										// create map of user to security list
										const subscriptionsToCancel = [];
										let i = 0;
										const length = selectedRecords.length;
										for (; i < length; i++) {
											const record = selectedRecords[i];
											const subscription = {class: 'com.clifton.marketdata.provider.subscription.MarketDataActiveUserSubscription'};
											subscription['securityUser.id'] = record.get('securityUser.id');
											subscription['security.id'] = record.get('security.id');
											subscription['clientIpAddress'] = record.get('clientIpAddress');
											subscriptionsToCancel.push(subscription);
										}
										requestParams['beanList'] = JSON.stringify(subscriptionsToCancel);
										confirmMessage += 'selected rows.';
									}
								}

								if ('yes' === await TCG.showConfirm(confirmMessage, 'Manage Active Subscriptions')) {
									// post request to unsubscribe
									TCG.data.getDataPromise(url + '?requestedPropertiesToExcludeGlobally=beanList', gridPanel, {
										params: requestParams,
										waitTarget: gridPanel,
										waitMsg: waitMsg + '...'
									}).then(function() {
										gridPanel.reload();
									});
								}
							};

							t.add({
								text: 'Unsubscribe',
								xtype: 'splitbutton',
								iconCls: 'run',
								tooltip: 'Unsubscribe selected active user subscriptions',
								scope: gridPanel,
								handler: function() {
									activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnsubscribe.json', 'Unsubscribing');
								},
								menu: new Ext.menu.Menu({
									items: [{
										text: 'Unsubscribe Selected',
										tooltip: 'Unsubscribe selected active user subscriptions',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnsubscribe.json', 'Unsubscribing');
										}
									}, {
										text: 'Unsubscribe All',
										tooltip: 'Force unsubscribing of all active user subscriptions',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnsubscribe.json', 'Unsubscribing', true);
										}
									}]
								})
							}, '-');
							t.add({
								text: 'Unlock',
								xtype: 'splitbutton',
								iconCls: 'run',
								tooltip: 'Unlock selected active user subscriptions that are locked',
								scope: gridPanel,
								handler: function() {
									activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnlock.json', 'Unlocking');
								},
								menu: new Ext.menu.Menu({
									items: [{
										text: 'Unlock Selected',
										tooltip: 'Unlock selected active user subscriptions that are locked',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnlock.json', 'Unlocking');
										}
									}, {
										text: 'Unlock All',
										tooltip: 'Force unlocking of all active user subscriptions',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											activeSubscriptionHandler('marketDataActiveUserSubscriptionListUnlock.json', 'Unlocking', true);
										}
									}]
								})
							}, '-');
							t.add({
								text: 'Provider Subscriptions',
								tooltip: 'Launch the subscription window for direct Market Data Provider management',
								iconCls: 'numbers',
								scope: gridPanel,
								handler: function() {
									TCG.createComponent('Clifton.marketdata.MarketDataProviderSubscriptionsWindow');
								}
							}, '-');
							t.add({
								text: 'Service Management',
								tooltip: 'Launch the provider service management window',
								iconCls: 'run',
								scope: gridPanel,
								handler: function() {
									TCG.createComponent('Clifton.marketdata.MarketDataProviderServiceAdministrationWindow');
								}
							}, '-');
						}
					},

					columns: [
						{header: 'User ID', dataIndex: 'securityUser.id', width: 40, type: 'int', hidden: true},
						{header: 'User', dataIndex: 'securityUser.displayName', width: 40, hidden: true, viewNames: ['By Security']},
						{header: 'Security ID', dataIndex: 'security.id', width: 40, type: 'int', hidden: true},
						{header: 'Security Symbol', dataIndex: 'security.symbol', width: 40, hidden: true, viewNames: ['By User'], tooltip: 'The symbol of the security in the system. If the value is blank, the subscription is active for a security that cannot be mapped back to a security in the system.'},
						{header: 'Market Data Symbol', dataIndex: 'marketDataSecuritySymbol', width: 40, tooltip: 'The full market data security symbol.'},
						{header: 'IP Address', dataIndex: 'clientIpAddress', width: 40, tooltip: 'The IP address of the user\'s client machine'},
						{header: 'Reference Count', dataIndex: 'referenceCount', width: 20, type: 'int', tooltip: 'The number of occurrences the security was subscribed to. The count could be greater than one if other securities use it as a referenced security (e.g. underlying or reference security)'},
						{header: 'Locked', dataIndex: 'locked', width: 10, type: 'boolean', tooltip: 'User subscription references use locking to allow a single request from being made at a time per user and security combination. If checked, the user reference is locked because a subscription request has not received a response. It can be unlocked if there are no waiting requests on the lock.'}
					],

					addGridGroupSelectionContextMenu: function(gridPanel) {
						// register reset of group selection state on load
						gridPanel.grid.store.on('load', () => gridPanel.groupSelectionState = {});
						// register group context menu
						gridPanel.grid.on('groupcontextmenu', function(grid, groupField, groupValue, event) {
							event.preventDefault();
							const gridPanel = this;
							gridPanel.groupValue = groupValue;
							if (!grid.drillDown) {
								// set context menu on grid once rather than on every right-click
								grid.drillDown = new Ext.menu.Menu({
									items: [
										{
											text: '(De)Select all rows in this group.',
											iconCls: 'copy',
											handler: function() {
												const gp = this;
												if (!gp.groupSelectionState) {
													gp.groupSelectionState = {};
												}
												const select = !TCG.isTrue(gp.groupSelectionState[gp.groupValue]);
												gp.groupSelectionState[gp.groupValue] = select;
												const grid = gp.grid;
												grid.getStore().each(function(record, index) {
													if (TCG.isEquals(record.get(gp.groupField), gp.groupValue)) {
														const selectionModel = grid.getSelectionModel();
														if (select) {
															selectionModel.selectRow(index, true);
														}
														else {
															selectionModel.deselectRow(index);
														}
													}
												});
											},
											scope: gridPanel
										},
										{
											text: 'Expand or Collapse all groups.',
											iconCls: 'copy',
											handler: function() {
												this.expandCollapseGroups.call(this);
											},
											scope: gridPanel
										}
									]
								});
							}
							grid.drillDown.showAt(event.getXY());
						}, gridPanel);
					},

					expandCollapseGroups: function() {
						this.grid.collapsed = !this.grid.collapsed;
						this.grid.view.toggleAllGroups(!this.grid.collapsed);
					}
				}]
			}
		]
	}]
});

Clifton.marketdata.MarketDataProviderSubscriptionsWindow = Ext.extend(TCG.app.Window, {
	id: 'marketDataProviderSubscriptionWindow',
	title: 'Market Data Provider Subscriptions',
	iconCls: 'numbers',
	width: 700,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Active Subscriptions',
				items: [{
					name: 'marketDataSubscriptionOperationProcess',
					xtype: 'gridpanel',
					rowSelectionModel: 'multiple',
					instructions: 'Market data subscription list showing active user subscriptions',
					appendStandardColumns: false,
					remoteSort: false,
					forceLocalFiltering: true,
					additionalPropertiesToRequest: 'userIdentitySet.securityUserId|userIdentitySet.clientIpAddress',

					getLoadParams: function(firstLoad) {
						return {
							subscriptionOperation: 'LIST'
						};
					},

					editor: {
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Clear Subscriptions',
								iconCls: 'run',
								tooltip: 'Clear active subscriptions and authenticated sessions on the provider',
								scope: gridPanel,
								handler: function() {
									Ext.Msg.confirm('Clear All Subscriptions', 'Would you like to unsubscribe all active subscriptions and cancel all authenticated sessions on the subscription provider? This will affect all users and will require each user to reestablish subscriptions.', function(a) {
										if (a === 'yes') {
											TCG.data.getDataPromise(gridPanel.getLoadURL() + '?', gridPanel, {
												params: {subscriptionOperation: 'CLEAR_SUBSCRIPTIONS'},
												waitTarget: gridPanel,
												waitMsg: 'Clearing subscriptions...'
											}).then(function() {
												gridPanel.reload();
											});
										}
									});
								}
							}, '-');
						}
					},

					columns: [
						{header: 'Market Data Symbol', dataIndex: 'securitySymbol', width: 50, tooltip: 'The full market data security symbol.'},
						{header: 'Status', dataIndex: 'status', width: 20},
						{
							header: 'Users', dataIndex: 'users', width: 100,
							renderer: function(value, metaData, record) {
								let returnValue = value;
								if (TCG.isBlank(returnValue)) {
									const users = TCG.getValue('userIdentitySet', record.json);
									returnValue = users.map(user => '{userId: ' + user.securityUserId + ', ipAddress: ' + user.clientIpAddress + '}').join(',<br/>');
									record.data['users'] = returnValue;
								}
								return TCG.renderTextNowrapWithTooltip(returnValue, metaData);
							}
						}
					]
				}]
			}
		]
	}]
});

Clifton.marketdata.MarketDataProviderServiceAdministrationWindow = Ext.extend(TCG.app.Window, {
	id: 'marketDataProviderServiceAdministrationWindow',
	title: 'Market Data Provider Service Administration',
	iconCls: 'run',
	width: 500,
	height: 125,

	items: [{
		xtype: 'formpanel',
		items: [
			{
				fieldLabel: 'Provider Name', name: 'providerName', displayField: 'name', valueField: 'valueField', xtype: 'combo', mode: 'local', hiddenName: 'providerName',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'valueField', 'description'],
					data: Clifton.marketdata.provider.OVERRIDE_OPTIONS
				})
			}
		],
		buttons: [{
			text: 'Clear Service Caches',
			handler: function() {
				const form = this.findParentByType('formpanel').getForm();
				form.submit(Ext.applyIf({
					url: 'bloombergClearCachesForProvider.json'
				}, Ext.applyIf({timeout: 200}, TCG.form.submitDefaults)));
			}
		}]
	}]
});
