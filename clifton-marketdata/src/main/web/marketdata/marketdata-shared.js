Ext.ns('Clifton.marketdata.datasource', 'Clifton.marketdata.field', 'Clifton.marketdata.field.column', 'Clifton.marketdata.field.upload', 'Clifton.marketdata.security', 'Clifton.marketdata.rates', 'Clifton.marketdata.provider');

//Marketdata Provider Override Options
Clifton.marketdata.provider.OVERRIDE_OPTIONS = [
	['B-Pipe', 'BPIPE', 'Uses the B-Pipe provider'],
	['BackOffice', 'BACKOFFICE', 'Uses the BackOffice provider'],
	['Terminal', 'TERMINAL', 'Uses the Terminal provider'],
	['MDS', 'MDS', 'Uses the Market Data Service provider']
];


//Simple Upload Windows
Clifton.system.schema.SimpleUploadWindows['MarketDataValue'] = 'Clifton.marketdata.field.upload.MarketDataValueUploadWindow';

//use to override interest rate index window in market data project so interest rates/chart can be added to the window
//also adds a tab for mappings
Clifton.investment.rates.InterestRateIndexWindowOverride = 'Clifton.marketdata.rates.InterestRateIndexWindow';


Clifton.investment.setup.InvestmentListWindowAdditionalTabs[Clifton.investment.setup.InvestmentListWindowAdditionalTabs.length] = function(w) {
	TCG.use('Clifton.marketdata.security.InvestmentSecurityUpdaterSection');
	return Clifton.marketdata.security.InvestmentSecurityUpdaterSection;
};

Clifton.investment.instrument.SecurityAdditionalTabsByType['DEFAULT'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['DEFAULT'], {
	title: 'Market Data',
	items: [{xtype: 'marketdata-security-datavalue-grid'}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Bond'], {
	title: 'Market Data',
	items: [{xtype: 'marketdata-security-datavalue-grid'}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['Currency'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Currency'],
	{
		title: 'Exchange Rates',
		items: [{
			name: 'marketDataExchangeRateListFind',
			xtype: 'gridpanel',
			instructions: 'This section collects historic exchange rates from selected currency.',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?exchangeRateSupported=true'}},
				{header: 'From', width: 80, dataIndex: 'fromCurrency.symbol', filter: {type: 'combo', searchFieldName: 'fromCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'To', width: 80, dataIndex: 'toCurrency.symbol', filter: {type: 'combo', searchFieldName: 'toCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Date', width: 80, dataIndex: 'rateDate'},
				{header: 'Value', width: 100, dataIndex: 'exchangeRate', type: 'float'},
				{
					header: '1/Value', width: 100, dataIndex: 'exchangeRate', type: 'float', filter: false,
					renderer: function(v) {
						return Ext.util.Format.number(1 / v, '0,000.00000000');
					}
				}
			],
			editor: {
				detailPageClass: 'Clifton.marketdata.rates.ExchangeRateWindow',
				getDefaultData: function(gridPanel) {
					return {
						fromCurrency: gridPanel.getWindow().getMainForm().formValues
					};
				}
			},
			getLoadParams: function(firstLoad) {
				if (firstLoad) {
					this.setFilterValue('rateDate', {'after': new Date().add(Date.DAY, -30)});
				}
				return {'fromCurrencyId': this.getWindow().getMainFormId()};
			}
		}]
	},
	{
		title: 'Currency Conventions',
		items: [{
			name: 'investmentCurrencyConventionListFind',
			xtype: 'gridpanel',
			instructions: 'FX Rate between USD and GBP is quoted as 1.5 meaning 1 USD = 1 GBP / 1.5 [multiplyFromByFX = false], but FX rate between USD and JPY is quoted as 80 meaning 1 USD = 1 JPY * 80 [multiplyFromByFX = true]',
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'From Currency', width: 100, dataIndex: 'fromCurrency.label', filter: {type: 'combo', searchFieldName: 'fromCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}, defaultSortColumn: true},
				{header: 'To Currency', width: 100, dataIndex: 'toCurrency.label', filter: {type: 'combo', searchFieldName: 'toCurrencyId', displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'}},
				{header: 'Multiply From By FX Rate', width: 100, dataIndex: 'multiplyFromByFX', type: 'boolean'},
				{header: 'Dominant Currency', width: 100, dataIndex: 'dominantCurrency.label', filter: false, sortable: false},
				{header: 'Days to Settle', width: 50, dataIndex: 'daysToSettle', type: 'int', useNull: true}
			],
			editor: {
				detailPageClass: 'Clifton.investment.instrument.currency.CurrencyConventionWindow'
			},
			getLoadParams: function(firstLoad) {
				return {'fromOrToCurrencyId': this.getWindow().getMainFormId()};
			}
		}]
	}
);
Clifton.investment.instrument.SecurityAdditionalTabsByType['OTC'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['OTC'], {
	title: 'Market Data',
	items: [{xtype: 'marketdata-security-datavalue-grid'}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['Proprietary'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Proprietary'], {
	title: 'Market Data',
	items: [{xtype: 'marketdata-security-datavalue-grid'}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['Stock'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Stock'], {
	title: 'Market Data',
	items: [{
		xtype: 'marketdata-security-datavalue-grid',
		showAdjustedValue: true
	}]
});
Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'] = TCG.appendToArrayWithInitialization(Clifton.investment.instrument.SecurityAdditionalTabsByType['Swap'], {
	title: 'Market Data',
	items: [{xtype: 'marketdata-security-datavalue-grid'}]
});

// column window tabs
Clifton.system.schema.ColumnWindowAdditionalTabs[Clifton.system.schema.ColumnWindowAdditionalTabs.length] = {
	title: 'Data Source Value Mappings',
	items: [{
		xtype: 'marketdata-field-value-mapping-grid',
		getLoadParams: function(firstLoad) {
			return {
				columnId: this.getWindow().getMainFormId()
			};
		}
	}]
};

Clifton.system.schema.ColumnWindowAdditionalTabs[Clifton.system.schema.ColumnWindowAdditionalTabs.length] = {
	title: 'Data Source Column Mappings',
	items: [{
		xtype: 'marketdata-field-column-mapping-grid',
		getLoadParams: function(firstLoad) {
			return {
				columnId: this.getWindow().getMainFormId(),
				requestedMaxDepth: 3
			};
		},
		editor: {
			detailPageClass: 'Clifton.marketdata.field.column.DataFieldColumnMappingWindow',
			getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
				return {
					column: gridPanel.getWindow().getMainForm().formValues
				};
			}
		}
	}]
};

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['SHARE_PRICE'] = [];
Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['SHARE_PRICE'].push({
	title: 'Allocations',
	items: [
		{xtype: 'marketdata-shareprice-security-allocations-grid'}
	]
});

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['SHARE_PRICE'].push({
	title: 'Rebalance Dates',
	items: [
		{
			xtype: 'investment-security-allocations-rebalanceDates-grid',
			rebalancingSupported: true
		}
	]
});

Clifton.investment.instrument.allocation.SecurityAllocationAdditionalTabs['SHARE_PRICE'].push({
	title: 'Security Events',
	items: [
		{xtype: 'investment-security-allocations-events-grid'}
	]
});


Clifton.marketdata.field.AdjustmentTypes = [
	['MULTIPLY', 'MULTIPLY', 'Multiply adjustment factor by adjustment value: 2 for 1 stock split => multiply historical adjustment factors for Shares Outstanding by 2'],
	['DIVIDE', 'DIVIDE', 'Divide adjustment factor by adjustment value: 2 for 1 stock split => divide historical adjustment factors for Prices by 2']
];

Clifton.marketdata.datasource.MarketDataFieldValueMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataFieldValueMappingListFind',
	xtype: 'gridpanel',
	instructions: 'Different market data sources may use different values to represent the same data. This section can be used to map data source specific values to values that our system understands.',
	importTableName: 'MarketDataFieldValueMapping',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json'}},
		{header: 'Data Field', width: 100, dataIndex: 'dataField.name', filter: {type: 'combo', searchFieldName: 'dataFieldId', url: 'marketDataFieldListFind.json'}},
		{header: 'Source Value', width: 100, dataIndex: 'fromValue'},
		{header: 'Our Value', width: 100, dataIndex: 'toValue'},
		{header: 'Our Value Label', width: 100, dataIndex: 'toValueLabel'}
	],
	editor: {
		detailPageClass: 'Clifton.marketdata.field.DataFieldValueMappingWindow'
	}
});
Ext.reg('marketdata-field-value-mapping-grid', Clifton.marketdata.datasource.MarketDataFieldValueMappingGrid);


Clifton.marketdata.datasource.MarketDataFieldColumnMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataFieldColumnMappingListFind',
	xtype: 'gridpanel',
	instructions: 'Maps market data provider fields to corresponding system columns. This mapping is used to pre-populate data (usually security fields) from market data providers. Hierarchy specific mappings take precedence over Type specific mappings which take precedence over Global mappings.',
	importTableName: 'MarketDataFieldColumnMapping',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json'}},
		{header: 'Data Field', width: 150, dataIndex: 'dataField.name', filter: {type: 'combo', searchFieldName: 'dataFieldId', url: 'marketDataFieldListFind.json'}},
		{header: 'System Table', width: 150, dataIndex: 'column.table.name', filter: {type: 'combo', searchFieldName: 'tableId', url: 'systemTableListFind.json?defaultDataSource=true'}},
		{header: 'System Column Template', width: 150, dataIndex: 'template.name', filter: {type: 'combo', searchFieldName: 'templateId', displayField: 'label', url: 'systemColumnTemplateListFind.json'}},
		{header: 'System Column', width: 150, dataIndex: 'column.name', filter: {searchFieldName: 'columnName'}},
		{header: 'Property Name for Lookup', width: 150, dataIndex: 'beanPropertyNameForLookup'},
		{header: 'Investment Type', width: 150, dataIndex: 'investmentType.name', filter: {type: 'combo', searchFieldName: 'investmentTypeId', loadAll: true, url: 'investmentTypeList.json'}},
		{header: 'Investment Sub Type', width: 150, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
		{header: 'Investment Sub Type 2', width: 150, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
		{header: 'Investment Hierarchy', width: 500, dataIndex: 'instrumentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json', queryParam: 'labelExpanded', listWidth: 600}},
		{header: 'Excluded', width: 140, dataIndex: 'columnExcluded', type: 'boolean'}
	],
	editor: {
		detailPageClass: 'Clifton.marketdata.field.column.DataFieldColumnMappingWindow'
	}
});
Ext.reg('marketdata-field-column-mapping-grid', Clifton.marketdata.datasource.MarketDataFieldColumnMappingGrid);


Clifton.marketdata.datasource.SecurityOverrideGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataSourceSecurityListBySecurity',
	xtype: 'gridpanel',
	instructions: 'Different market data providers (data sources) use the same security symbols and other attributes. In rare cases, when a different symbol is used by a particular data source, this entity can map it to our security which uses default data source conventions (symbol, etc.).',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Data Source', width: 150, dataIndex: 'dataSource.label'},
		{header: 'Data Source Symbol', width: 120, dataIndex: 'dataSourceSymbol'},
		{header: 'Overrides for Latest', width: 200, dataIndex: 'externalFieldOverridesForLatest'},
		{header: 'Overrides for Historic', width: 200, dataIndex: 'externalFieldOverridesForHistoric'}
	],
	getLoadParams: function() {
		return {securityId: this.getWindow().getMainFormId()};
	},
	editor: {
		detailPageClass: 'Clifton.marketdata.datasource.SecuritySymbolOverrideWindow',
		deleteURL: 'marketDataSourceSecurityDelete.json',
		getDefaultData: function(gridPanel) {
			return {
				security: gridPanel.getWindow().getMainForm().formValues
			};
		}
	}
});
Ext.reg('marketdata-datasource-security-override-grid', Clifton.marketdata.datasource.SecurityOverrideGrid);


Clifton.marketdata.datasource.DataSourceCompanyMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataSourceCompanyMappingListFind.json',
	xtype: 'gridpanel',
	instructions: 'Mappings for data-source-specific company names can be entered here. For example, on intraday trade files, executing brokers are specified by various names.  We can connect those names to the correct internal companies via a mapping',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Data Source', width: 100, dataIndex: 'marketDataSource.name', filter: {searchFieldName: 'marketDataSourceName'}, hidden: true},
		{header: 'External Company Name', width: 100, dataIndex: 'externalCompanyName'},
		{header: 'Company', width: 100, dataIndex: 'businessCompany.name', filter: {searchFieldName: 'businessCompanyName'}},
		{header: 'Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {searchFieldName: 'marketDataSourcePurposeName'}}
	],
	getLoadParams: function() {
		return {marketDataSourceId: this.getWindow().getMainFormId()};
	},
	editor: {
		detailPageClass: 'Clifton.marketdata.datasource.CompanyMappingWindow',
		deleteURL: 'marketDataSourceCompanyMappingDelete.json',
		getDefaultData: function(gridPanel) {
			return {
				marketDataSource: gridPanel.getWindow().getMainForm().formValues
			};
		}
	}
});
Ext.reg('marketdata-datasource-company-mapping-grid', Clifton.marketdata.datasource.DataSourceCompanyMappingGrid);


Clifton.marketdata.datasource.DataSourceExchangeMappingGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataSourceExchangeMappingListFind.json',
	xtype: 'gridpanel',
	instructions: 'Mappings for data-source-specific exchange codes can be entered here.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Data Source', width: 100, dataIndex: 'marketDataSource.name', filter: {searchFieldName: 'marketDataSourceName'}, hidden: true},
		{header: 'Exchange Code', width: 100, dataIndex: 'exchangeCode'},
		{header: 'Exchange', width: 100, dataIndex: 'exchange.label', filter: {searchFieldName: 'exchangeName'}},
		{header: 'Purpose', width: 100, dataIndex: 'marketDataSourcePurpose.name', filter: {searchFieldName: 'marketDataSourcePurposeName'}}
	],
	getLoadParams: function() {
		return {marketDataSourceId: this.getWindow().getMainFormId()};
	},
	editor: {
		detailPageClass: 'Clifton.marketdata.datasource.ExchangeMappingWindow',
		deleteURL: 'marketDataSourceExchangeMappingDelete.json',
		getDefaultData: function(gridPanel) {
			return {
				marketDataSource: gridPanel.getWindow().getMainForm().formValues
			};
		}
	}
});
Ext.reg('marketdata-datasource-exchange-mapping-grid', Clifton.marketdata.datasource.DataSourceExchangeMappingGrid);


Clifton.marketdata.SecurityDataValueGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'marketDataValueListFind',
	importTableName: 'MarketDataValue',
	importComponentName: 'Clifton.marketdata.field.upload.MarketDataValueUploadWindow',
	instructions: 'This section collects historic market data measures for specific market data fields.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Field', width: 100, dataIndex: 'dataField.name', filter: {type: 'combo', searchFieldName: 'dataFieldId', url: 'marketDataFieldListFind.json'}},
		{header: 'Field Order', width: 50, dataIndex: 'dataField.fieldOrder', filter: {searchFieldName: 'dataFieldOrder'}, hidden: true},
		{header: 'Data Source', width: 100, dataIndex: 'dataSource.name', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
		{header: 'Date', width: 80, dataIndex: 'measureDateWithTime', type: 'date', filter: {searchFieldName: 'measureDate'}},
		{header: 'Time', width: 80, dataIndex: 'measureTime', align: 'center', hidden: true, filter: false},
		{
			header: 'Value', width: 80, dataIndex: 'measureValue', type: 'float',
			renderer: function(v, metaData, r) {
				const value = TCG.numberFormat(v, '0,000', true);
				if (TCG.isNotEquals(r.json.createDate, r.json.updateDate)) {
					return '<div class="amountAdjusted" qtip="Market Data was updated. See Audit Trail for details.">' + value + '</div>';
				}
				return value;
			}
		},
		{
			header: 'Adjusted Value', width: 80, dataIndex: 'measureValueAdjusted', type: 'float', hidden: true, tooltip: 'Historic values maybe adjusted for stock splits, etc. This is normalized value used by the system.', nonPersistentField: true,
			renderer: function(v, metaData, r) {
				const value = TCG.numberFormat(v, '0,000', true);
				if (TCG.isNotEquals(r.json.measureValue, v)) {
					return '<div class="amountAdjusted" qtip="Market Data was adjusted for Stock Splits.">' + value + '</div>';
				}
				return value;
			}
		},
		{header: 'Adjustment Factor', width: 80, dataIndex: 'measureValueAdjustmentFactor', type: 'float', hidden: true},
		{header: 'Real-Time Data', width: 40, dataIndex: 'dataField.realTimeReferenceData', type: 'boolean', hidden: true, filter: {searchFieldName: 'realTimeReferenceData'}}
	],
	plugins: {ptype: 'groupsummary', showGrandTotal: false},
	initComponent: function() {
		Clifton.marketdata.SecurityDataValueGrid.superclass.initComponent.call(this, arguments);
		if (this.showAdjustedValue === true) {
			const cm = this.getColumnModel();
			cm.setHidden(cm.findColumnIndex('measureValueAdjusted'), false);
		}
	},
	getLoadParams: function(firstLoad) {
		const lp = {};
		lp.investmentSecurityId = this.getWindow().getMainFormId();
		const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
		if (TCG.isNotBlank(tag.getValue())) {
			lp.dataFieldCategoryHierarchyId = tag.getValue();
		}
		if (firstLoad) {
			this.setFilterValue('dataField.realTimeReferenceData', false);
		}
		return lp;
	},

	getTopToolbarFilters: function(toolbar) {
		const filters = [];
		filters.push({fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Market Data Tags'});
		return filters;
	},
	editor: {
		detailPageClass: 'Clifton.marketdata.field.DataValueWindow',
		allowToDeleteMultiple: true,
		getDefaultData: function(gridPanel) {
			return {
				investmentSecurity: gridPanel.getWindow().getMainForm().formValues
			};
		},
		addEditButtons: function(toolBar, gridPanel) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
			const secId = gridPanel.getWindow().getMainFormId();

			// Only show Rebuild and Preview if the system can calculate market data values
			const allocationOfSecuritiesSystemCalculated = TCG.getValue('allocationOfSecuritiesSystemCalculated', gridPanel.getWindow().getMainForm().formValues);
			if (TCG.isTrue(allocationOfSecuritiesSystemCalculated)) {

				toolBar.add({
					text: 'Rebuild',
					tooltip: 'Rebuild allocated market data values for this security for a date range',
					iconCls: 'run',
					scope: this,
					handler: function() {
						const dd = {};
						dd.securityId = secId;
						dd.securityLabel = TCG.getValue('label', gridPanel.getWindow().getMainForm().formValues);
						dd.startDate = Clifton.calendar.getBusinessDayFrom(-1).format('Y-m-d 00:00:00');
						dd.endDate = dd.startDate;
						const pc = TCG.data.getData('marketDataSourceByName.json?marketDatasourceName=Parametric Clifton', toolBar, 'Clifton.marketdata.dataSource.Parametric Clifton');
						if (pc) {
							dd.dataSourceId = pc.id;
							dd.dataSourceName = pc.name;
						}
						TCG.createComponent('Clifton.marketdata.security.InvestmentSecurityAllocationMarketDataRebuildWindow', {
							defaultData: dd,
							openerCt: gridPanel
						});
					}
				});
				toolBar.add('-');

				toolBar.add({
					text: 'Preview',
					tooltip: 'Preview rebuild of allocated market data values for this security on preview date.',
					iconCls: 'preview',
					scope: this,
					handler: function() {
						const dd = {};
						dd.securityId = secId;
						dd.securityLabel = TCG.getValue('label', gridPanel.getWindow().getMainForm().formValues);
						dd.previewDate = Clifton.calendar.getBusinessDayFrom(-1).format('Y-m-d 00:00:00');
						const pc = TCG.data.getData('marketDataSourceByName.json?marketDatasourceName=Parametric Clifton', toolBar, 'Clifton.marketdata.dataSource.Parametric Clifton');
						if (pc) {
							dd.dataSourceId = pc.id;
							dd.dataSourceName = pc.name;
						}
						TCG.createComponent('Clifton.marketdata.security.InvestmentSecurityAllocationMarketDataPreviewWindow', {
							defaultData: dd,
							openerCt: gridPanel
						});
					}
				});
				toolBar.add('-');
			}
		}

	}
});
Ext.reg('marketdata-security-datavalue-grid', Clifton.marketdata.SecurityDataValueGrid);


Clifton.marketdata.security.SharePriceSecurityAllocationsGrid = Ext.extend(Clifton.investment.instrument.allocation.SecurityAllocationsGrid, {
	name: 'marketDataInvestmentSecurityAllocationListFind',
	instructions: 'Allocations...', // Dynamically overwritten with Allocation Type & Hierarchy Description

	defaultView: 'Current View',

	viewNames: ['Default View', 'Security View', 'Instrument View', 'Expanded View', 'Current View'],

	getDefaultActiveOnDate: async function() {
		// Default to previous business day (from current date) so we have market data (override of base method).
		const businessPrevDate = await TCG.data.getDataValuePromise('calendarBusinessDayPrevious.json');
		return new Date(businessPrevDate).format('m/d/Y');
	},
	getDefaultFxRateMarketDataSource: async function() {
		const defaultFxSourceName = 'WM/Reuters';
		return TCG.data.getDataPromiseUsingCaching('marketDataSourceByName.json?marketDatasourceName=' + defaultFxSourceName, this, 'SECURITY_ALLOCATION_GRID_DEFAULT_FX_SOURCE_NAME');
	},

	configureToolsMenu: function(menu) {
		let excelMenu = menu.findBy(function(m) {
			return TCG.isEquals(m.text, 'Export to Excel');
		});
		if (excelMenu && excelMenu.length === 1) {
			const gp = this;
			excelMenu = excelMenu[0].menu;
			excelMenu.add('-');
			excelMenu.add({
				text: 'Export Weights Across Date Range',
				iconCls: 'excel',
				tooltip: 'Export current weights for each allocation across date range.',
				scope: this,
				handler: function() {
					const security = gp.getWindow().getMainForm().formValues;

					TCG.createComponent('TCG.app.OKCancelWindow', {
						title: 'Export Weights Across Date Range',
						iconCls: 'excel',
						height: 220,
						width: 500,
						modal: true,

						okButtonText: 'Export',
						okButtonTooltip: 'Export Security allocations using selected options',

						items: [{
							xtype: 'formpanel',
							loadValidation: false,
							instructions: 'Enter a start/end date to export allocation weights for.  If start date is blank, or before security start date, it will be pushed forward to security start date.  If end date is blank, or after today, it will be pushed back to today.',
							items: [
								{fieldLabel: 'Start Date', xtype: 'datefield', name: 'startDate'},
								{fieldLabel: 'End Date', xtype: 'datefield', name: 'endDate'}
							]
						}],
						saveWindow: function(closeOnSuccess, forceSubmit) {
							const fp = this.getMainFormPanel();
							let sd = fp.getForm().findField('startDate').getValue();
							if (TCG.isNotBlank(sd)) {
								sd = sd.format('m/d/Y');
							}
							let ed = fp.getForm().findField('endDate').getValue();
							if (TCG.isNotBlank(ed)) {
								ed = ed.format('m/d/Y');
							}
							const params = {parentInvestmentSecurityId: security.id, startDate: sd, endDate: ed, outputFormat: 'xlsx', fileName: security.label + ' Allocation Weight History'};
							TCG.downloadFile('marketDataInvestmentSecurityAllocationWeightListForDateRange.json', params, this);
							this.closeWindow();
						}
					});
				}
			});
		}
	},
	remoteSort: false,
	getLoadParams: async function(firstLoad) {
		const params = await Clifton.marketdata.security.SharePriceSecurityAllocationsGrid.superclass.getLoadParams.call(this, firstLoad);
		const c = TCG.getChildByName(this.getTopToolbar(), 'securityConvention').getValue();
		if (c && TCG.isNotEquals(c, 'Default')) {
			params.securityConventionDataSource = c;
		}

		const t = this.getTopToolbar();
		const fxSourceCombo = TCG.getChildByName(t, 'fxRateMarketDataSource');
		if (TCG.isNotBlank(fxSourceCombo) && TCG.isNotBlank(fxSourceCombo.getValue())) {
			params.fxRateMarketDataSourceId = fxSourceCombo.getValue();
		}
		else if (TCG.isTrue(firstLoad)) {
			const defaultFxDataSource = await this.getDefaultFxRateMarketDataSource();
			if (TCG.isNotBlank(defaultFxDataSource)) {
				fxSourceCombo.setValue({value: defaultFxDataSource.id, text: defaultFxDataSource.name});
				params.fxRateMarketDataSourceId = defaultFxDataSource.id;
			}
		}

		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		const filters = Clifton.marketdata.security.SharePriceSecurityAllocationsGrid.superclass.getTopToolbarFilters.call(this, toolbar);
		filters.unshift(
				{fieldLabel: 'FX Source', xtype: 'toolbar-combo', width: 100, name: 'fxRateMarketDataSource', url: 'marketDataSourceListFind.json?exchangeRateSupported=true', detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'},
				{
					fieldLabel: 'Convention', xtype: 'toolbar-combo', name: 'securityConvention', value: 'Default', displayField: 'name', valueField: 'name', mode: 'local', width: 100,
					store: new Ext.data.ArrayStore({
					fields: ['name', 'description'],
					data: [['Default', 'Display security symbol and name.'], ['Bloomberg', 'User Bloomberg\'s security symbol convention. For example: "AAPL UW Equity"']]
				})
		});

		return filters;
	},

	columns: [
		{header: 'Security/Instrument', width: 300, dataIndex: 'allocationLabelExpanded', viewNames: ['Default View'], sortable: false, filter: false},
		{header: 'Security', width: 200, dataIndex: 'investmentSecurity.label', hidden: true, viewNames: ['Security View', 'Expanded View'], filter: {searchFieldName: 'investmentSecurityLabel'}},
		{
			header: 'Instrument', width: 200, dataIndex: 'investmentInstrument.label', hidden: true, viewNames: ['Instrument View', 'Expanded View'], filter: {searchFieldName: 'investmentInstrumentLabel'},
			tooltip: 'For cases where a many-to-many security is needed you can optionally select the instrument instead and a current security calculator.'
		},
		{header: 'Exchange', width: 50, dataIndex: 'exchange.exchangeCode', filter: false},
		{
			header: 'Override Exchange', width: 100, dataIndex: 'overrideExchange.exchangeCode', hidden: false, viewNames: ['Expanded View'], filter: {searchFieldName: 'overrideExchangeId', type: 'combo', url: 'investmentExchangeListFind.json?compositeExchange=false'},
			tooltip: 'An optional exchange used to override the override a security\'s exchange for use during price lookup.'
		},
		{header: 'CCY Denom', width: 100, dataIndex: 'tradingCurrency.symbol', hidden: true, viewNames: ['Current View', 'Expanded View'], filter: false},
		{
			header: 'Current Security Calculator', width: 150, dataIndex: 'currentSecurityCalculatorBean.name', hidden: true, viewNames: ['Instrument View', 'Expanded View'], filter: {searchFieldName: 'currencySecurityCalculatorBeanName'},
			tooltip: 'Current Security Calculator defines the behavior of how the <i>current</i> security is selected for a specific date and selected instrument so we know which security price to use.  Used with instrument selection only.  Default behavior is to first check the <b><i>Current Securities</i></b> security group, otherwise the first active security ordered by end date ascending.  <br/><br/>Note: This does not apply if a specific security is selected for the allocation.'
		},
		{header: 'FX Rate', width: 85, dataIndex: 'currencyExchangeRate', hidden: true, type: 'float', useNull: true, filter: false, tooltip: 'The exchange rate between the allocation security\'s trading currency of the trading currency of the parent security.'},
		{header: 'Current Security', width: 150, dataIndex: 'currentSecurityLabel', viewNames: ['Current View'], hidden: true, filter: false, defaultSortColumn: true},
		{header: 'Current Local Price', width: 60, dataIndex: 'currentPrice', type: 'float', useNull: true, viewNames: ['Current View'], hidden: true, filter: false},

		{header: 'Shares', width: 60, dataIndex: 'allocationShares', type: 'float', summaryType: 'sum'},
		{header: 'Base Weight', width: 55, dataIndex: 'allocationWeight', type: 'percent', useNull: true, summaryType: 'sum', hidden: true},
		{header: 'Current Weight', width: 60, dataIndex: 'currentAllocationWeight', type: 'percent', useNull: true, viewNames: ['Current View'], summaryType: 'sum'},
		{header: 'Difference', width: 45, dataIndex: 'weightDifference', type: 'percent', summaryType: 'sum', useNull: true, hidden: true},
		{header: 'Start Date', width: 45, dataIndex: 'startDate', type: 'date'},
		{header: 'End Date', width: 45, dataIndex: 'endDate', type: 'date'},
		{header: 'Exchange Overridden', width: 50, dataIndex: 'exchangeOverridden', type: 'boolean', filter: {searchFieldName: 'isExchangeOverridden'}, tooltip: 'Indicates that the security\'s primary exchange has been overridden with another exchange.'},
		{header: 'Note', width: 100, hidden: true, dataIndex: 'note'}
	]
});
Ext.reg('marketdata-shareprice-security-allocations-grid', Clifton.marketdata.security.SharePriceSecurityAllocationsGrid);


/* Asynchronously looks up the provided live field value for the specified security ID and updates the provided field
 * on the form panel when the value is obtained. The liveDataField can be left blank, in which case the live price will
 * be fetched. Supports an array of security IDs and fields to update. If an array is used, make sure the array of
 * security IDs and fields to update are the same length and in the correct order. Default value for maxAgeInSeconds is 300 (5 minutes).
 */
Clifton.marketdata.field.updateSecurityLiveMarketDataValues = function(formPanel, securityId, fieldToUpdate, liveDataField, options = {}) {
	if (formPanel) {
		const form = formPanel.getForm();
		if (securityId) {
			const postProcessor = Ext.isArray(securityId)
				? function(records) {
					if (records) {
						let i = 0;
						const len = records.length;
						for (; i < len; i++) {
							let j = 0;
							const innerLen = securityId.length;
							for (; j < innerLen; j++) {
								if (TCG.isEquals(records[i].securityId, securityId[j])) {
									// update the matching field for the security (matched by index)
									const field = form.findField(fieldToUpdate[j]);
									if (field) {
										field.setValue(records[i].value);
									}
									break;
								}
							}
						}
					}
				}
				: function(record) {
					if (record) {
						const field = form.findField(fieldToUpdate);
						if (field) {
							field.setValue(record.value);
						}
					}
				};
			// Make request for data and update the form panel fields according to the post processor
			Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityId, liveDataField, options).then(postProcessor);
		}
		else {
			// there is no security, clear the current values
			const clearProcess = function(fieldName) {
				const field = form.findField(fieldName);
				if (field) {
					field.setValue('');
				}
			};
			if (Ext.isArray(fieldToUpdate)) {
				for (let i = 0, len = fieldToUpdate.length; i < len; i++) {
					clearProcess(fieldToUpdate[i]);
				}
			}
			else {
				clearProcess(fieldToUpdate);
			}
		}
	}
};

/**
 * Asynchronously looks up the provided live field value for the specified security ID and returns a Promise
 * for the response. The liveDataField can be left blank, in which case the live price will be fetched.
 * Supports an array of security IDs for the liveDataField. Default value for maxAgeInSeconds is 300 (5 minutes).
 *
 * @param securityId the security ID to look up a value for; can be an array of security IDs to return multiple values for the same field
 * @param liveDataField the optional field name to look up a value for; if undefined will look up latest price
 * @param options an object containing additional optional parameters to include in the request: maxAgeInSeconds, providerOverride, ignoreErrors, etc.
 * @returns {*}
 */
Clifton.marketdata.field.getSecurityLiveMarketDataValue = async function(securityId, liveDataField, options = {}) {
	if (securityId) {
		const params = {
			securityIds: securityId,
			requestedPropertiesToExcludeGlobally: 'marketDataLiveCommand',
			maxAgeInSeconds: (options && TCG.isNotBlank(options.maxAgeInSeconds) ? options.maxAgeInSeconds : 5 * 60),
			providerOverride: options ? options.providerOverride : void 0,
			ignoreErrors: options ? TCG.isTrue(options.ignoreErrors) : false
		};
		const requestConfig = {params: params};
		let url;
		if (liveDataField) {
			url = 'marketDataLiveFieldValuesWithCommand.json';
			params['fieldNames'] = liveDataField;
		}
		else {
			url = 'marketDataLivePricesWithCommand.json';
		}
		// Make the request and return the promise
		// Since command lookups are always arrays, flatten the result when a single security for convenience
		const result = await TCG.data.getDataPromise(url, null, requestConfig);
		return Array.isArray(securityId) ? result : (result && Array.isArray(result)) ? result[0] : result;
	}
};

/**
 * Asynchronously looks up the live ASK or BID price for the specified security ID depending on whether the security is being bought or sold.
 * Returns a Promise for the response. Supports an array of security IDs. Default value for maxAgeInSeconds is 300 (5 minutes).
 *
 * @param securityId the security ID to look up a value for; can be an array of security IDs to return multiple values for the same field
 * @param buy if buy is true, looks up the ASK price, otherwise BID price
 * @param options an object containing additional optional parameters to include in the request: maxAgeInSeconds, providerOverride, ignoreErrors, etc.
 * @returns {*}
 */
Clifton.marketdata.field.getSecurityLivePriceForBuySell = function(securityId, buy, options = {}) {
	if (securityId) {
		return Clifton.marketdata.field.getSecurityLiveMarketDataValue(securityId, TCG.isTrue(buy) ? 'Ask Price' : 'Bid Price', options);
	}
	return Promise.resolve(void 0);
};

/**
 * Requests IMS to asynchronously import Bloomberg market data for the provided security ID and date.
 * This function is useful to load price data for newly created securities prior to performing other
 * actions with the security (e.g. creating trades). If the security's instrument ID is provided,
 * verification will be done to ensure the security supports getting Bloomberg market data to avoid an error.
 */
Clifton.marketdata.field.importBloombergMarketDataForSecurityOnDate = function(securityId, date, instrumentId) {
	if (TCG.isNotBlank(securityId) && TCG.isNotBlank(date)) {
		if (instrumentId) {
			// verify that the security has Bloomberg field mappings before trying to import data
			TCG.data.getDataPromise('marketDataFieldMappingListByInstrument.json', null, {
				params: {
					instrumentId: instrumentId,
					requestedPropertiesRoot: 'data',
					requestedProperties: 'id|marketDataSource.name'
				}
			}).then(function(fieldMappings) {
				if (fieldMappings) {
					let supportsBloombergImport = false;
					let i = 0;
					const len = fieldMappings.length;
					for (; i < len; i++) {
						const marketDataSourceName = (fieldMappings[i].marketDataSource) ? fieldMappings[i].marketDataSource.name : '';
						if (marketDataSourceName && fieldMappings[i].marketDataSource.name.includes('Bloomberg')) {
							supportsBloombergImport = true;
						}
					}
					if (TCG.isTrue(supportsBloombergImport)) {
						TCG.data.getDataPromise('marketDataFieldValuesUpdate.json?synchronous=false', null, {
							params: {
								investmentSecurityId: securityId,
								startDate: date,
								endDate: date,
								skipIfAlreadyExists: true
							}
						});
					}
				}
			});
		}
		else {
			// try import without check for Bloomberg field mappings - may produce error
			TCG.data.getDataPromise('marketDataFieldValuesUpdate.json?synchronous=false', null, {
				params: {
					investmentSecurityId: securityId,
					startDate: date,
					endDate: date,
					skipDataRequestIfAlreadyExists: true
				}
			});
		}
	}
};

/**
 * Class to manage active real-time MarketData security subscriptions across different windows and grids for the client.
 */
Clifton.marketdata.SecuritySubscriptionHandler = new class extends TCG.websocket.WebSocketSubscriptionHandler {

	constructor() {
		super('market-data', '/user/topic/market-data');
		// Local object of security IDs and their subscription counts.
		this.activeSecurityIdCounts = {};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Internal method to updates the current  state using the provided message
	 *
	 * Overrides the super to modify how the subscription data is captured
	 *
	 * @protected
	 */
	updateEntityList(message) {
		const securityIds = Object.keys(message);
		securityIds.forEach(id => this.entityMap[id] = message[id]);
		this.entityBuffer.next();
	}


	/**
	 * Internal method to invoke registered callbacks upon updates or expired delay
	 *
	 * Overrides the super to modify how the subscription data is applied to registered callbacks
	 *
	 * @protected
	 */
	applyEntityList() {
		if (this.listenerCallbacks.length > 0) {
			const entityList = this.getAndClearSubscriptionData();
			this.listenerCallbacks.forEach(listener => listener(entityList));
		}
	}


	/**
	 * Returns an object of security ID to array of field data since the last time data was obtained.
	 * The internal data set is cleared to prepare for the next batch of data updates
	 *
	 * @returns {*}
	 * @private
	 */
	getAndClearSubscriptionData() {
		this.initializeSubscription();
		const data = {};
		const securityIds = Object.keys(this.entityMap);
		securityIds.forEach(id => {
			data[id] = this.entityMap[id];
			delete this.entityMap[id];
		});
		return data;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Returns a list of security IDs with active subscriptions
	 * @returns {number[]}
	 */
	getActiveSecurityIds() {
		return Object.keys(this.activeSecurityIdCounts).map(id => parseInt(id));
	}


	/**
	 * Makes an asynchronous request to subscribe to the provided security IDs and returns a list of all security IDs actively subscribed to.
	 * If an active subscription already exists for one more provided security IDs, those security IDs will not be resubscribed to.
	 *
	 * @param securityIdList list of security IDs to subscribe to
	 * @param scope entity to use for the scope of the subscription action
	 * @returns {Promise<*>} a promise for the request with no result
	 */
	async subscribe(securityIdList, scope) {
		return this.performSubscription(securityIdList, true, scope);
	}


	/**
	 * Makes an asynchronous request to unsubscribe to the provided security IDs and returns a list of all security IDs actively subscribed to.
	 * If an active subscription does not exist for one more provided security IDs, those security IDs will not be unsubscribed to.
	 *
	 * @param securityIdList list of security IDs to subscribe to
	 * @param scope entity to use for the scope of the subscription action
	 * @returns {Promise<*>} a promise for the request with no result
	 */
	async unsubscribe(securityIdList, scope) {
		return this.performSubscription(securityIdList, false, scope);
	}


	/**
	 * Makes an asynchronous request to unsubscribe all active security subscriptions. This action affects subscriptions for the current user across all browser tabs using subscriptions.
	 *
	 * @param scope entity to use for the scope of the subscription action
	 * @returns {Promise<*>}
	 */
	async forceUnsubscribeAll(scope) {
		const activeSecurityIds = this.getActiveSecurityIds();
		for (const activeSecurityId of activeSecurityIds) {
			this.activeSecurityIdCounts[activeSecurityId] = 1;
		}
		try {
			return this.performSubscription(activeSecurityIds, false, scope, true);
		}
		finally {
			this.applyReferenceCountAdjustment(activeSecurityIds);
		}
	}


	/**
	 * [Un]Subscribes to new security IDs that are not already active. A reference count for each security ID is tracked to know when
	 * to [un]subscribe to when multiple windows and/or grids are using the same security.
	 *
	 * @param securityIdList list of security IDs to [un]subscribe to
	 * @param subscribe boolean to indicate whether to subscribe (true) or unsubscribe (false)
	 * @param scope entity to use for the scope of the subscription action
	 * @param force forces the request for the security IDs provided
	 * @returns {*} a promise for the subscription action
	 * @private
	 */
	performSubscription(securityIdList, subscribe, scope, force) {
		if (Array.isArray(securityIdList)) {
			const securityIdsToSubscribeTo = securityIdList.filter(id => TCG.isTrue(force) || (subscribe ? TCG.isNull(this.activeSecurityIdCounts[id]) : this.activeSecurityIdCounts[id] === 1));

			if (securityIdsToSubscribeTo.length > 0) {
				const securityIds = securityIdsToSubscribeTo.map(securityId => TCG.isNumber(securityId) ? securityId : parseInt(securityId));

				return TCG.data.getDataPromise('marketDataSubscriptionCommandProcess.json?requestedPropertiesToExcludeGlobally=beanList', scope, {
					waitTarget: scope,
					waitMsg: subscribe ? 'Subscribing...' : 'Unsubscribing...',
					params: {
						subscribe: subscribe, force: TCG.isTrue(force),
						securityIds: Ext.util.JSON.encode(securityIds)
					}
				}).then(() => this.applyReferenceCountAdjustment(securityIdList, subscribe));
			}
			else if (TCG.isTrue(force) && TCG.isFalse(subscribe)) {
				const currentUser = TCG.getCurrentUser();
				if (currentUser) {
					return TCG.data.getDataPromise('marketDataActiveUserSubscriptionListUnsubscribe.json?requestedPropertiesToExcludeGlobally=beanList', scope, {
						waitTarget: scope,
						waitMsg: 'Unsubscribing...',
						params: {'securityUser.id': currentUser.id}
					}).then(() => this.applyReferenceCountAdjustment(securityIdList, subscribe));
				}
			}
			else {
				// If there are no securities to subscribe to, avoid the request and just adjust the reference counts
				this.applyReferenceCountAdjustment(securityIdList, subscribe);
			}
		}
		return Promise.resolve(void 0);
	}


	/**
	 * Increments the local reference count of each security if subscribe is true, decrements each reference count if subscribe is not true.
	 * If subscribe is not true and the reference count is 1 or less, the local reference for the security will be removed.
	 * @private
	 */
	applyReferenceCountAdjustment(securityIdList, subscribe) {
		for (const id of securityIdList) {
			const references = this.activeSecurityIdCounts[id];
			if (subscribe) {
				this.activeSecurityIdCounts[id] = references ? references + 1 : 1;
			}
			else {
				if (TCG.isNull(references) || references === 1) {
					delete this.activeSecurityIdCounts[id];
				}
				else {
					this.activeSecurityIdCounts[id] = references - 1;
				}
			}
		}
	}

};
