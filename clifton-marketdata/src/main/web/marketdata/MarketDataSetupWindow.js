Clifton.marketdata.MarketDataSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'marketDataSetupWindow',
	title: 'Market Data Setup',
	iconCls: 'numbers',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Data Sources',
				items: [{
					name: 'marketDataSourceListFind',
					xtype: 'gridpanel',
					instructions: 'Market data source represents a company or a tool that we use to obtain financial information like investment security prices and/or exchange rates.',
					importTableName: 'MarketDataSource',
					topToolbarSearchParameter: 'searchPattern',
					forceLocalFiltering: true,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Company', width: 100, dataIndex: 'company.name', filter: {searchFieldName: 'company'}},
						{header: 'Security System', width: 70, dataIndex: 'securitySystem.name', filter: {searchFieldName: 'securitySystem'}},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'FX Fixing Time', width: 50, dataIndex: 'fxFixingTime.text', filter: {searchFieldName: 'fxFixingTime'}, tooltip: 'If defined, this Data Source is available as a source for Exchange Rates (validation and filtering). Specifies the time when exchange rates are recorded: usually "London Close" or "US Close".'},
						{header: 'FX Rate', width: 35, dataIndex: 'exchangeRateSupported', type: 'boolean', tooltip: 'Check to allow Exchange Rates for this Data Source.'},
						{header: 'Interest Rate', width: 35, dataIndex: 'interestRateSupported', type: 'boolean', tooltip: 'Check to allow Interest Rates for this Data Source.'},
						{header: 'Market Data', width: 35, dataIndex: 'marketDataValueSupported', type: 'boolean', tooltip: 'Check to allow Market Data Values for this Data Source.'},
						{header: 'Market Sectors', width: 45, dataIndex: 'marketSectorSupported', type: 'boolean'},
						{header: 'Default', width: 30, dataIndex: 'defaultDataSource', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourceWindow'
					}
				}]
			},


			{
				title: 'Data Fields',
				items: [{
					name: 'marketDataFieldListFind',
					xtype: 'gridpanel',
					instructions: 'Market data field defines a single field/measure of market data. For example, Trading Volume, Close Price, Settlement Price, Delta, etc.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Field Name', width: 150, dataIndex: 'name'},
						{header: 'External Name', width: 150, dataIndex: 'externalFieldName'},
						{header: 'Field Order', width: 60, dataIndex: 'fieldOrder', hidden: true, useNull: true, type: 'int'},
						{header: 'Data Type', width: 70, dataIndex: 'dataType.name'},
						{header: 'Adjustment Type', width: 80, dataIndex: 'adjustmentType', filter: {type: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.marketdata.field.AdjustmentTypes}}},
						{header: 'Override Field', width: 100, dataIndex: 'valueOverrideDataField.name', filter: {searchFieldName: 'valueOverrideDataField'}},
						{header: 'Overrides for Latest', width: 200, dataIndex: 'externalFieldOverridesForLatest', hidden: true},
						{header: 'Overrides for Historic', width: 200, dataIndex: 'externalFieldOverridesForHistoric', hidden: true},
						{header: 'Value Expression', width: 200, dataIndex: 'valueExpression', hidden: true},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Precision', width: 60, dataIndex: 'decimalPrecision', type: 'int'},
						{header: 'Timeout Millis', width: 80, dataIndex: 'requestTimeoutMillis', type: 'int', useNull: true},
						{header: 'One Per Day', width: 75, dataIndex: 'upToOnePerDay', type: 'boolean'},
						{header: 'Latest Only', width: 75, dataIndex: 'latestValueOnly', type: 'boolean'},
						{header: 'Changes Only', width: 80, dataIndex: 'captureChangesOnly', type: 'boolean'},
						{header: 'Time', width: 50, dataIndex: 'timeSensitive', type: 'boolean'},
						{header: 'Real-Time', width: 50, dataIndex: 'realTimeReferenceData', type: 'boolean'},
						{header: 'Live Value Calculator', width: 100, dataIndex: 'liveValueCalculator.name', idDataIndex: 'liveValueCalculator.id', hidden: true, filter: {searchFieldName: 'liveValueCalculatorId', type: 'combo', url: 'systemBeanListFind.json?groupName=Market Date Live Field Value Calculator'}},
						{header: 'Live Value Calculator Condition', width: 100, dataIndex: 'liveValueCalculatorCondition.name', idDataIndex: 'liveValueCalculatorCondition.id', hidden: true, filter: {searchFieldName: 'liveValueCalculatorConditionId', type: 'combo', url: 'systemConditionListFind.json?optionalSystemTableName=InvestmentSecurity'}},
						{header: 'System', width: 55, dataIndex: 'systemDefined', type: 'boolean', filter: {searchFieldName: 'systemDefined'}},
						{header: 'Ignore Value Condition', width: 100, dataIndex: 'ignoreValueCondition.name', hidden: true, filter: {searchFieldName: 'ignoreValueConditionName'}}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', url: 'systemHierarchyListFind.json?categoryName=Market Data Tags'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							return {
								categoryName: 'Market Data Tags',
								categoryHierarchyId: tag.getValue()
							};
						}
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldWindow'
					}
				}]
			},


			{
				title: 'Data Field Groups',
				items: [{
					name: 'marketDataFieldGroupListFind',
					xtype: 'gridpanel',
					topToolbarSearchParameter: 'searchPattern',
					forceLocalFiltering: true,
					instructions: 'Market data field groups group related data fields that apply to the same investment instruments. They improve usability when mapping to investment instruments. The same data field may belong to multiple groups.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'},
						{header: 'Order Required', width: 40, dataIndex: 'fieldOrderRequired', type: 'boolean'},
						{header: 'System', width: 30, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldGroupWindow'
					}
				}]
			},


			{
				title: 'Instrument Field Mappings',
				items: [{
					name: 'marketDataFieldMappingListFind',
					xtype: 'gridpanel',
					importTableName: 'MarketDataFieldMapping',
					instructions: 'Instrument mappings map market data sources, field groups and market sectors to corresponding investment instruments. Direct instrument mapping will override less specific (hierarchy, sub-type, type) mappings. This information is used to lookup what price fields or market sector to use for a specific investment instrument.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Data Source', xtype: 'toolbar-combo', name: 'dataSourceId', width: 150, url: 'marketDataSourceListFind.json', linkedFilter: 'marketDataSource.label'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', width: 75, dataIndex: 'investmentType.name', filter: {searchFieldName: 'investmentType'}},
						{header: 'Sub Type', width: 90, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
						{header: 'Sub Type 2', width: 75, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
						{header: 'Hierarchy', width: 120, dataIndex: 'instrumentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true'}, defaultSortColumn: true},
						{header: 'Instrument', width: 100, dataIndex: 'instrument.label', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Field Group', width: 100, dataIndex: 'fieldGroup.label', filter: {type: 'combo', searchFieldName: 'fieldGroupId', url: 'marketDataFieldGroupListFind.json'}},
						{header: 'Data Source', width: 80, dataIndex: 'marketDataSource.label', filter: {type: 'combo', searchFieldName: 'dataSourceId', url: 'marketDataSourceListFind.json'}},
						{header: 'Market Sector', width: 80, dataIndex: 'marketSector.label', filter: {searchFieldName: 'marketSector'}},
						{header: 'Pricing Source', width: 60, dataIndex: 'pricingSource.label', filter: {searchFieldName: 'pricingSource'}, hidden: true},
						{header: 'Exchange Code Type', width: 60, dataIndex: 'exchangeCodeType', hidden: true},
						{header: 'Live Latest Price Field', width: 100, dataIndex: 'liveLatestPriceMarketDataField.name', hidden: true},
						{header: 'Live Closing Price Field', width: 100, dataIndex: 'liveClosingPriceMarketDataField.name', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataFieldMappingWindow'
					}
				}]
			},


			{
				title: 'Instrument Price Field Mappings',
				items: [{
					name: 'marketDataPriceFieldMappingListFind',
					xtype: 'gridpanel',
					importTableName: 'MarketDataPriceFieldMapping',
					instructions: 'Instrument price field mappings map instruments to fields to use for Latest, Closing, and/or Official Prices. Direct instrument mapping will override less specific (hierarchy, sub-type, type) mappings.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type', width: 75, dataIndex: 'investmentType.name', filter: {searchFieldName: 'investmentType'}},
						{header: 'Sub Type', width: 90, dataIndex: 'investmentTypeSubType.name', filter: {searchFieldName: 'investmentTypeSubType'}},
						{header: 'Sub Type 2', width: 75, dataIndex: 'investmentTypeSubType2.name', filter: {searchFieldName: 'investmentTypeSubType2'}},
						{header: 'Hierarchy', width: 120, dataIndex: 'instrumentHierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'instrumentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true'}, defaultSortColumn: true},
						{header: 'Instrument', width: 100, dataIndex: 'instrument.label', filter: {type: 'combo', searchFieldName: 'instrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Note', width: 150, hidden: true, dataIndex: 'note'},
						{header: 'Latest Price Field', width: 100, dataIndex: 'latestPriceMarketDataField.name', filter: {type: 'combo', searchFieldName: 'latestPriceMarketDataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Latest Data Source', width: 80, dataIndex: 'latestPriceMarketDataSource.name', hidden: true, filter: {type: 'combo', searchFieldName: 'latestPriceMarketDataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
						{header: 'Closing Price Field', width: 100, dataIndex: 'closingPriceMarketDataField.name', filter: {type: 'combo', searchFieldName: 'closingPriceMarketDataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Closing Data Source', width: 80, dataIndex: 'closingPriceMarketDataSource.name', hidden: true, filter: {type: 'combo', searchFieldName: 'closingPriceMarketDataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}},
						{header: 'Official Price Field', width: 100, dataIndex: 'officialPriceMarketDataField.name', filter: {type: 'combo', searchFieldName: 'officialPriceMarketDataFieldId', url: 'marketDataFieldListFind.json'}},
						{header: 'Official Data Source', width: 80, dataIndex: 'officialPriceMarketDataSource.name', hidden: true, filter: {type: 'combo', searchFieldName: 'officialPriceMarketDataSourceId', url: 'marketDataSourceListFind.json?marketDataValueSupported=true'}}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.field.DataPriceFieldMappingWindow'
					}
				}]
			},


			{
				title: 'Interest Rate Mappings',
				items: [{
					name: 'marketDataInterestRateIndexMappingListFind',
					xtype: 'gridpanel',
					instructions: 'Interest rate mappings map interest rate indices to corresponding data source market sectors. If the data source uses a different symbol for the interest rate, the symbol can be overridden here.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Interest Rate Index', width: 80, dataIndex: 'referenceOne.label', filter: {searchFieldName: 'interestRateIndexLabel'}, defaultSortColumn: true},
						{header: 'Data Source', width: 80, dataIndex: 'referenceTwo.dataSource.label', filter: {searchFieldName: 'dataSourceName'}},
						{header: 'Market Sector', width: 80, dataIndex: 'referenceTwo.label', filter: {searchFieldName: 'dataSourceSectorName'}},
						{header: 'Symbol Override', width: 60, dataIndex: 'dataSourceSymbol', tooltip: 'If not defined, uses the index symbol'},
						{header: 'Value Transformation Bean', width: 60, dataIndex: 'valueTransformationBean.name', filter: {type: 'combo', searchFieldName: 'valueTransformationBeanId', displayField: 'name', url: 'systemBeanListFind.json?groupName=Market Data Value Transformer'}},
						{header: 'Active', width: 50, dataIndex: 'referenceOne.active', type: 'boolean', filter: {searchFieldName: 'interestRateIndexActive'}, tooltip: 'Interest Rate Index Active Today'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.rates.InterestRateIndexMappingWindow',
						deleteURL: 'marketDataInterestRateIndexMappingDelete.json'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to active
							this.setFilterValue('referenceOne.active', true);
						}
						return {};
					}
				}]
			},


			{
				title: 'Price Multipliers',
				items: [{
					name: 'marketDataSourcePriceMultiplierListFind',
					xtype: 'gridpanel',
					instructions: 'Some market data sources may use non-industry-standard price multipliers. Our system will apply the following additional multipliers to their prices sent to us.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Data Source', width: 150, dataIndex: 'marketDataSource.label', filter: {type: 'combo', searchFieldName: 'marketDataSourceId', url: 'marketDataSourceListFind.json'}},
						{header: 'Data Source Company', width: 150, dataIndex: 'marketDataSource.company.name', filter: {searchFieldName: 'companyName'}},
						{header: 'Data Source Purpose', width: 150, dataIndex: 'marketDataSourcePurpose.name', filter: {type: 'combo', searchFieldName: 'marketDataSourcePurposeId', url: 'marketDataSourcePurposeListFind.json'}},
						{header: 'Instrument', width: 200, dataIndex: 'investmentInstrument.label', filter: {type: 'combo', searchFieldName: 'investmentInstrumentId', displayField: 'label', url: 'investmentInstrumentListFind.json'}},
						{header: 'Additional Multiplier', width: 80, dataIndex: 'additionalPriceMultiplier', type: 'float'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePriceMultiplierWindow'
					}
				}]
			},


			{
				title: 'Company Mappings',
				items: [{
					xtype: 'marketdata-datasource-company-mapping-grid',
					columnOverrides: [
						{dataIndex: 'marketDataSource.name', hidden: false}
					],
					getLoadParams: function() {
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.CompanyMappingWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Exchange Mappings',
				items: [{
					xtype: 'marketdata-datasource-exchange-mapping-grid',
					columnOverrides: [
						{dataIndex: 'marketDataSource.name', hidden: false}
					],
					getLoadParams: function() {
						return {};
					},
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.ExchangeMappingWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Pricing Sources',
				items: [{
					name: 'marketDataSourcePricingSourceListFind',
					xtype: 'gridpanel',
					instructions: 'Some market data sources may optionally provide access to different pricing sources. This section defines all of these options and different parts of our system can be configured to use them as opposed to default pricing source.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Pricing Source', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Data Source', width: 100, dataIndex: 'marketDataSource.label', filter: {type: 'combo', searchFieldName: 'marketDataSourceId', url: 'marketDataSourceListFind.json'}},
						{header: 'Data Source Company', width: 100, dataIndex: 'marketDataSource.company.name', filter: {searchFieldName: 'companyName'}},
						{header: 'Symbol', width: 100, dataIndex: 'symbol'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePricingSourceWindow'
					}
				}]
			},


			{
				title: 'Data Source Purposes',
				items: [{
					name: 'marketDataSourcePurposeListFind',
					xtype: 'gridpanel',
					forceLocalFiltering: true,
					instructions: 'Some market data sources may optionally provide access to different pricing sources. This section defines all of these options and different parts of our system can be configured to use them as opposed to default pricing source.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Purpose Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.marketdata.datasource.DataSourcePurposeWindow'
					}
				}]
			}
		]
	}]
});

