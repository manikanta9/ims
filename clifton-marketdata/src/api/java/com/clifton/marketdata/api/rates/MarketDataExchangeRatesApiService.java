package com.clifton.marketdata.api.rates;


import com.clifton.business.shared.Company;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;

import java.math.BigDecimal;


/**
 * @author vgomelsky
 */
public interface MarketDataExchangeRatesApiService {

	/**
	 * Returns data source name to be used for exchange rate look ups for the specified client account.
	 * COALESCE(Default FX Source, Global Default)
	 */
	public String getExchangeRateDataSourceForClientAccount(int clientAccountId);


	/**
	 * Returns data source name to be used for exchange rate look ups for the specified client account.
	 * COALESCE(Default FX Source, Global Default)
	 */
	public String getExchangeRateDataSourceForClientAccount(ClientAccount clientAccount);


	/**
	 * Returns data source name to be used for exchange rate look ups for the specified holding account.
	 * COALESCE(Default FX Source, Account Issuer FX Source)
	 * <p>
	 * Return default data source (Goldman Sachs) if no valid data source exists and exceptionIfNotFound == false.
	 */
	public String getExchangeRateDataSourceForHoldingAccount(int holdingAccountId, boolean exceptionIfNotFound);


	/**
	 * Returns data source name to be used for exchange rate look ups for the specified holding account.
	 * COALESCE(Default FX Source, Account Issuer FX Source)
	 * <p>
	 * Return default data source (Goldman Sachs) if no valid data source exists and exceptionIfNotFound == false.
	 */
	public String getExchangeRateDataSourceForHoldingAccount(HoldingAccount holdingAccount, boolean exceptionIfNotFound);


	/**
	 * Returns data source name for the Data Source that is mapped to the specified FX Source company.
	 * <p>
	 * Return default data source (Goldman Sachs) if no valid data source exists and exceptionIfNotFound == false.
	 */
	public String getExchangeRateDataSourceForCompany(Company fxSourceCompany, boolean exceptionIfNotFound);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns BigDecimal Exchange Rate value between the two currencies specified on the command.
	 * See configuration options documentation for additional details.
	 */
	public BigDecimal getExchangeRate(FxRateLookupCommand command);
}
