package com.clifton.marketdata.api.pricing;


/**
 * A service intended for lookup of stored or live market data values, and to support field data lookup for securities that have an overridden investment exchange specified.
 * The service will first attempt to look up data from locally stored market data, and if not found, will attempt to look up data from a live provider service such as Bloomberg.
 *
 * @author davidi
 */


public interface MarketDataSecurityDataValueApiService {

	////////////////////////////////////////////////////////////////////////////
	////                    Field Value Lookup Methods                      ////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves field data based for the investment security specified in {@link MarketDataSecurityDataValueCommand}.
	 */
	public MarketDataSecurityDataValue getMarketDataSecurityDataValue(MarketDataSecurityDataValueCommand command);
}
