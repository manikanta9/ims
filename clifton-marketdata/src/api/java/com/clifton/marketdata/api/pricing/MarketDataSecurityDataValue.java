package com.clifton.marketdata.api.pricing;

import com.clifton.core.shared.dataaccess.DataTypeNames;

import java.util.Date;


/**
 * A class containing the data results returned by lookup methods in the {@link MarketDataSecurityDataValueApiService}.
 *
 * @author davidi
 */
public class MarketDataSecurityDataValue {

	/**
	 * ID of the security being priced.
	 */
	private Integer securityId;

	/**
	 * The name of the exchange specified in the SecurityDataValueCommand for field lookup. Null indicates no override was specified, and the exchange on the security's InvestmentInstrument was used.
	 */
	private String exchangeName;

	/**
	 * The exchange code of the exchange specified in the SecurityDataValueCommand for field lookup. Null indicates no override was specified, and the exchange on the security's InvestmentInstrument was used.
	 */
	private String exchangeCode;

	/**
	 * The date for which the data was requested.  This may differ from the returned measure date, as the requested date may not have field data if it is for a calendar holiday.
	 */
	private Date requestedDate;

	/**
	 * The data source name for the data source used to acquire the requested data.
	 */
	private String dataSourceName;

	/**
	 * The field name used in the SecurityDataValueCommand to look up the data.
	 */
	private String fieldName;

	/**
	 * The data type of the field value.  If null, the data type will be considered to be numeric (e.g. BigDecimal).
	 */
	private DataTypeNames dataTypeName;

	/**
	 * The value returned for the field name.  If null, this indicates no value was found.
	 */
	private Object fieldValue;

	/**
	 * The date the data was measured.
	 */
	private Date measureDate;


	////////////////////////////////////////////////////////////////////////////

	public MarketDataSecurityDataValue(Integer securityId, DataTypeNames dataTypeName, String fieldName, Object fieldValue) {
		setSecurityId(securityId);
		setFieldName(fieldName);
		setDataTypeName(dataTypeName);
		setFieldValue(fieldValue);
	}


	public MarketDataSecurityDataValue withExchangeName(String exchangeName) {
		setExchangeName(exchangeName);
		return this;
	}


	public MarketDataSecurityDataValue withExchangeCode(String exchangeCode) {
		setExchangeCode(exchangeCode);
		return this;
	}


	public MarketDataSecurityDataValue withRequestedDate(Date requestedDate) {
		setRequestedDate(requestedDate);
		return this;
	}


	public MarketDataSecurityDataValue withDataSourceName(String dataSourceName) {
		setDataSourceName(dataSourceName);
		return this;
	}


	public MarketDataSecurityDataValue withMeasureDate(Date measureDate) {
		setMeasureDate(measureDate);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityId() {
		return this.securityId;
	}


	private void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getExchangeName() {
		return this.exchangeName;
	}


	private void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}


	public String getExchangeCode() {
		return this.exchangeCode;
	}


	private void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}


	public Date getRequestedDate() {
		return this.requestedDate;
	}


	private void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	private void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public String getFieldName() {
		return this.fieldName;
	}


	private void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}


	private void setDataTypeName(DataTypeNames dataTypeName) {
		this.dataTypeName = dataTypeName;
	}


	public Object getFieldValue() {
		return this.fieldValue;
	}


	private void setFieldValue(Object fieldValue) {
		this.fieldValue = fieldValue;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	private void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}
}
