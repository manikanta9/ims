package com.clifton.marketdata.api.pricing;


import java.util.Date;


/**
 * A command containing search parameters for local MarketData or Live / Historical lookups from a MarketDataProvider to specify market data field lookups.
 *
 * @author davidi
 */
public class MarketDataSecurityDataValueCommand {

	/**
	 * The Security ID of the security for which data is to be retrieved. This field is required.
	 */
	private Integer securityId;

	/**
	 * The Exchange Name to use when performing data lookups.  This identifies an InvestmentExchange used to override the security's Investment Exchange.
	 * May be null, in which case only the security's symbol is used in the lookup. Only required if overriding the security's exchange.
	 */
	private String exchangeName;

	/**
	 * If true, indicates that a live market data should be queried from the source, no date is requried for this.
	 */
	private boolean liveValue;

	/**
	 * If true, allows flexible lookups of local marketdata where the last known value is looked up.
	 */
	private boolean flexible;

	/**
	 * An optional property that specifies how many business days back flexible data can go before it is considered valid.
	 */
	private Integer flexibleMaxDaysBackAllowed;

	/**
	 * The market sector for the security.  This field is optional, but may be needed for more specificity.
	 */
	private String marketSectorName;

	/**
	 * The date the field value is requested for, this field is required.
	 */
	private Date requestedDate;

	/**
	 * Single name or that identifies the name of the MarketData field. This is a required field.
	 */
	private String marketDataFieldName;

	/**
	 * Name that identifies the MarketDataSource (for local lookups from locally stored market data). If null, a default data source will be used for lookups (e.g. Bloomberg). This is a required field.
	 */
	private String marketDataSourceName;

	/**
	 * An optional field for the name of a pricing source supported by the market data source.
	 */
	private String pricingSourceName;

	/**
	 * The optional provider override to use when making a Live request for data.
	 */
	private String providerOverrideName;

	/**
	 * If true, throws a validation exception if requested data is not found. This is an optional field.
	 */
	private boolean exceptionOnMissingData;

	/**
	 * An optional prefix to prepend to a missing data exception.
	 */
	private String exceptionPrefix;

	/**
	 * Timeout override for live data requests. Timeout values are in milliseconds. This is an optional field.
	 */
	private Integer timeoutOverride;

	/**
	 * If true, will return a normalized price when historical prices are requested from local market data.
	 */
	private boolean normalizedPrice;

	/**
	 * Optional max age in seconds of the value to look up. May be null in which case a default value of 1800 seconds is used.
	 */
	private Integer maxAgeInSeconds;

	/**
	 * Optional flag to ignore errors for continuing to process live values. If an error occurs when this value is true, the error will be logged instead of thrown.
	 */
	private boolean ignoreErrors;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MarketDataSecurityDataValueCommand forLivePrice(int securityId) {
		return new MarketDataSecurityDataValueCommand().withSecurityId(securityId).withLiveValue(true);
	}


	public static MarketDataSecurityDataValueCommand forLive(int securityId, String fieldName) {
		return new MarketDataSecurityDataValueCommand().withSecurityId(securityId).withLiveValue(true).withFieldName(fieldName);
	}


	public static MarketDataSecurityDataValueCommand forPrice(int securityId, Date requestDate) {
		return new MarketDataSecurityDataValueCommand().withSecurityId(securityId).withRequestedDate(requestDate);
	}


	public static MarketDataSecurityDataValueCommand forField(int securityId, String fieldName, Date requestDate) {
		return new MarketDataSecurityDataValueCommand().withSecurityId(securityId).withFieldName(fieldName).withRequestedDate(requestDate);
	}


	public MarketDataSecurityDataValueCommand withSecurityId(int securityId) {
		setSecurityId(securityId);
		return this;
	}


	public MarketDataSecurityDataValueCommand withExchangeName(String exchangeName) {
		setExchangeName(exchangeName);
		return this;
	}


	public MarketDataSecurityDataValueCommand withLiveValue(boolean isLiveValue) {
		setLiveValue(isLiveValue);
		return this;
	}


	public MarketDataSecurityDataValueCommand withFlexible(boolean isFlexible) {
		setFlexible(isFlexible);
		return this;
	}


	public MarketDataSecurityDataValueCommand withFlexibleMaxDaysBackAllowed(int flexibleMaxDaysBackAllowed) {
		setFlexibleMaxDaysBackAllowed(flexibleMaxDaysBackAllowed);
		return this;
	}


	public MarketDataSecurityDataValueCommand withMarketSector(String marketSector) {
		setMarketSectorName(marketSector);
		return this;
	}


	public MarketDataSecurityDataValueCommand withRequestedDate(Date requestedDate) {
		setRequestedDate(requestedDate);
		return this;
	}


	public MarketDataSecurityDataValueCommand withFieldName(String fieldName) {
		setMarketDataFieldName(fieldName);
		return this;
	}


	public MarketDataSecurityDataValueCommand withMarketDataSourceName(String marketDataSourceName) {
		setMarketDataSourceName(marketDataSourceName);
		return this;
	}


	public MarketDataSecurityDataValueCommand withPricingSourceName(String pricingSourceName) {
		setPricingSourceName(pricingSourceName);
		return this;
	}


	public MarketDataSecurityDataValueCommand withProviderOverrideName(String providerOverrideName) {
		setProviderOverrideName(providerOverrideName);
		return this;
	}


	public MarketDataSecurityDataValueCommand withExceptionOnMissingData(boolean isExceptionOnMissingData) {
		setExceptionOnMissingData(isExceptionOnMissingData);
		return this;
	}


	public MarketDataSecurityDataValueCommand withExceptionPrefix(String exceptionPrefix) {
		setExceptionPrefix(exceptionPrefix);
		return this;
	}


	public MarketDataSecurityDataValueCommand withTimeoutOverride(int timeoutOverride) {
		setTimeoutOverride(timeoutOverride);
		return this;
	}


	public MarketDataSecurityDataValueCommand withNormalizedPrice(boolean isNormalizedPrice) {
		setNormalizedPrice(isNormalizedPrice);
		return this;
	}


	public MarketDataSecurityDataValueCommand withMaxAgeInSeconds(int maxAgeInSeconds) {
		setMaxAgeInSeconds(maxAgeInSeconds);
		return this;
	}


	public MarketDataSecurityDataValueCommand withIgnoreErrors(boolean isIgnoreErrors) {
		setIgnoreErrors(isIgnoreErrors);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getExchangeName() {
		return this.exchangeName;
	}


	public void setExchangeName(String exchangeName) {
		this.exchangeName = exchangeName;
	}


	public boolean isLiveValue() {
		return this.liveValue;
	}


	public boolean isFlexible() {
		return this.flexible;
	}


	public Integer getFlexibleMaxDaysBackAllowed() {
		return this.flexibleMaxDaysBackAllowed;
	}


	public void setFlexibleMaxDaysBackAllowed(Integer flexibleMaxDaysBackAllowed) {
		this.flexibleMaxDaysBackAllowed = flexibleMaxDaysBackAllowed;
	}


	public void setFlexible(boolean flexible) {
		this.flexible = flexible;
	}


	public void setLiveValue(boolean liveValue) {
		this.liveValue = liveValue;
	}


	public String getMarketSectorName() {
		return this.marketSectorName;
	}


	public void setMarketSectorName(String marketSectorName) {
		this.marketSectorName = marketSectorName;
	}


	public Date getRequestedDate() {
		return this.requestedDate;
	}


	public void setRequestedDate(Date requestedDate) {
		this.requestedDate = requestedDate;
	}


	public String getMarketDataFieldName() {
		return this.marketDataFieldName;
	}


	public void setMarketDataFieldName(String marketDataFieldName) {
		this.marketDataFieldName = marketDataFieldName;
	}


	public String getMarketDataSourceName() {
		return this.marketDataSourceName;
	}


	public void setMarketDataSourceName(String marketDataSourceName) {
		this.marketDataSourceName = marketDataSourceName;
	}


	public String getPricingSourceName() {
		return this.pricingSourceName;
	}


	public void setPricingSourceName(String pricingSourceName) {
		this.pricingSourceName = pricingSourceName;
	}


	public String getProviderOverrideName() {
		return this.providerOverrideName;
	}


	public void setProviderOverrideName(String providerOverrideName) {
		this.providerOverrideName = providerOverrideName;
	}


	public boolean isExceptionOnMissingData() {
		return this.exceptionOnMissingData;
	}


	public void setExceptionOnMissingData(boolean exceptionOnMissingData) {
		this.exceptionOnMissingData = exceptionOnMissingData;
	}


	public String getExceptionPrefix() {
		return this.exceptionPrefix;
	}


	public void setExceptionPrefix(String exceptionPrefix) {
		this.exceptionPrefix = exceptionPrefix;
	}


	public Integer getTimeoutOverride() {
		return this.timeoutOverride;
	}


	public void setTimeoutOverride(Integer timeoutOverride) {
		this.timeoutOverride = timeoutOverride;
	}


	public boolean isNormalizedPrice() {
		return this.normalizedPrice;
	}


	public void setNormalizedPrice(boolean normalizedPrice) {
		this.normalizedPrice = normalizedPrice;
	}


	public Integer getMaxAgeInSeconds() {
		return this.maxAgeInSeconds;
	}


	public void setMaxAgeInSeconds(Integer maxAgeInSeconds) {
		this.maxAgeInSeconds = maxAgeInSeconds;
	}


	public boolean isIgnoreErrors() {
		return this.ignoreErrors;
	}


	public void setIgnoreErrors(boolean ignoreErrors) {
		this.ignoreErrors = ignoreErrors;
	}
}
