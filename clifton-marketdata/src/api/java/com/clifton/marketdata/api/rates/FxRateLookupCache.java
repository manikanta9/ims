package com.clifton.marketdata.api.rates;

import com.clifton.business.shared.Company;
import com.clifton.investment.shared.HoldingAccount;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The FxRateLookupCache class should be used in cases where multiple Exchange Rate calls for the same data need to be made.
 * It caches retrieved Exchange Rates and avoids duplicate calls.
 *
 * @author vgomelsky
 */
public class FxRateLookupCache {

	private final MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private final Map<Integer, String> fxSourceCompanyToDataSourceMap;
	private final Map<Integer, String> holdingAccountToDataSourceMap;
	private final Map<String, BigDecimal> fxRateMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FxRateLookupCache(MarketDataExchangeRatesApiService apiService) {
		if (apiService == null) {
			throw new IllegalArgumentException("Required 'MarketDataExchangeRatesApiService apiService' parameter cannot be null.");
		}

		this.marketDataExchangeRatesApiService = apiService;
		this.fxSourceCompanyToDataSourceMap = new HashMap<>();
		this.holdingAccountToDataSourceMap = new HashMap<>();
		this.fxRateMap = new HashMap<>();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void clearFxRatesCache() {
		this.fxRateMap.clear();
	}


	/**
	 * Performs strict Exchange Rate lookup on the specified date. Throws an exception if the rate is not found.
	 */
	public BigDecimal getExchangeRate(HoldingAccount holdingAccount, String fromCurrency, String toCurrency, Date date) {
		return getExchangeRate(holdingAccount, fromCurrency, toCurrency, date, false);
	}


	public BigDecimal getExchangeRate(Company fxSourceCompany, boolean fxSourceCompanyOverridden, String fromCurrency, String toCurrency, Date date, boolean flexible) {
		if (fromCurrency.equals(toCurrency)) {
			return BigDecimal.ONE;
		}

		String dataSourceName = (fxSourceCompany == null) ? null : this.fxSourceCompanyToDataSourceMap.get(fxSourceCompany.getId());
		if (dataSourceName == null) {
			dataSourceName = this.marketDataExchangeRatesApiService.getExchangeRateDataSourceForCompany(fxSourceCompany, fxSourceCompanyOverridden);
			if (fxSourceCompany != null) {
				this.fxSourceCompanyToDataSourceMap.put(fxSourceCompany.getId(), dataSourceName);
			}
		}

		return getExchangeRate(dataSourceName, fromCurrency, toCurrency, date, flexible);
	}


	public BigDecimal getExchangeRate(HoldingAccount holdingAccount, String fromCurrency, String toCurrency, Date date, boolean flexible) {
		if (fromCurrency.equals(toCurrency)) {
			return BigDecimal.ONE;
		}

		String dataSourceName = this.holdingAccountToDataSourceMap.get(holdingAccount.getId());
		if (dataSourceName == null) {
			dataSourceName = this.marketDataExchangeRatesApiService.getExchangeRateDataSourceForHoldingAccount(holdingAccount, false);
			this.holdingAccountToDataSourceMap.put(holdingAccount.getId(), dataSourceName);
		}

		return getExchangeRate(dataSourceName, fromCurrency, toCurrency, date, flexible);
	}


	private BigDecimal getExchangeRate(String dataSourceName, String fromCurrency, String toCurrency, Date date, boolean flexible) {
		String cacheKey = dataSourceName + ':' + fromCurrency + '-' + toCurrency + (flexible ? ':' : '-') + date.getTime();
		BigDecimal fxRate = this.fxRateMap.get(cacheKey);
		if (fxRate == null) {
			fxRate = this.marketDataExchangeRatesApiService.getExchangeRate(FxRateLookupCommand.forDataSource(dataSourceName, fromCurrency, toCurrency, date).flexibleLookup(flexible));
			this.fxRateMap.put(cacheKey, fxRate);
		}
		return fxRate;
	}
}
