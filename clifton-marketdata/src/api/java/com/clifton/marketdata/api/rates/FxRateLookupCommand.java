package com.clifton.marketdata.api.rates;

import com.clifton.business.shared.Company;
import com.clifton.investment.shared.ClientAccount;
import com.clifton.investment.shared.HoldingAccount;

import java.io.Serializable;
import java.util.Date;


/**
 * The FxRateLookupCommand class defines required parameters and additional options for looking up Exchange Rates.
 * <p>
 * Defaults to safe parameter values: non-flexible lookup with exception if the rate is missing.
 *
 * @author vgomelsky
 */
public class FxRateLookupCommand implements Serializable {

	/**
	 * The name of the Data Source to be used for FX rate lookup.
	 */
	private String dataSourceName;

	/**
	 * Use this Company to determine its Exchange Rate data source.  Usually comes from Holding Account issuer but can also be overridden
	 * on Holding Account or Client Account.
	 */
	private Company fxSourceCompany;

	/**
	 * Use this Holding Account to determine its Exchange Rate data source.  It can either be specified on the account or derived from account's issuer.
	 */
	private HoldingAccount holdingAccount;
	/**
	 * Use this Holding Account to determine its Exchange Rate data source.  It can either be specified on the account or derived from account's issuer.
	 */
	private Integer holdingAccountId;
	/**
	 * Use this Client Account to determine its Exchange Rate data source.
	 */
	private ClientAccount clientAccount;
	/**
	 * Use this Client Account to determine its Exchange Rate data source.
	 */
	private Integer clientAccountId;


	private String fromCurrency;

	private String toCurrency;

	private Date lookupDate;

	/**
	 * If flexible lookup is requested, then if the rate is not found on the lookup date, then latest available FX will be returned.
	 */
	private boolean flexibleLookup;
	/**
	 * Checks which currency (fromCurrency or toCurrency) is the dominant currency using currency conventions.
	 * Returns FX using the dominant currency as the fromCurrency: if toCurrency is the dominant currency, then flips to/from currency arguments.
	 */
	private boolean dominantCurrency;
	/**
	 * Specifies whether an exception should be throw when no valid Data Source is defined for the Holding Account or globally default Data Source should be used.
	 * <p>
	 * If true, use globally default Data Source for lookups if the specified data source is not valid or FX rate is not available for the specified data source.
	 * If false, throw an exception if the specified Data Source is not valid or follow the {@link #exceptionIfRateIsMissing} when FX Rate is not found.
	 */
	private boolean fallbackToDefaultDataSource = true;
	/**
	 * Specifies whether exception should be thrown if FX Rate is not found.  Returns null otherwise.
	 */
	private boolean exceptionIfRateIsMissing = true;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FxRateLookupCommand forDataSource(String dataSource, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.dataSourceName = dataSource;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}


	public static FxRateLookupCommand forFxSource(Company fxSourceCompany, boolean fallbackToDefaultDataSource, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.fxSourceCompany = fxSourceCompany;
		command.fallbackToDefaultDataSource = fallbackToDefaultDataSource;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}


	public static FxRateLookupCommand forHoldingAccount(HoldingAccount holdingAccount, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.holdingAccount = holdingAccount;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}


	public static FxRateLookupCommand forHoldingAccount(int holdingAccountId, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.holdingAccountId = holdingAccountId;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}


	public static FxRateLookupCommand forClientAccount(ClientAccount clientAccount, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.clientAccount = clientAccount;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}


	public static FxRateLookupCommand forClientAccount(int clientAccountId, String fromCurrency, String toCurrency, Date lookupDate) {
		FxRateLookupCommand command = new FxRateLookupCommand();
		command.clientAccountId = clientAccountId;
		command.fromCurrency = fromCurrency;
		command.toCurrency = toCurrency;
		command.lookupDate = lookupDate;
		return command;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * When flexible lookup is requested, if FX rate is not found for the date requested, will return latest available rate.
	 */
	public FxRateLookupCommand flexibleLookup() {
		this.flexibleLookup = true;
		return this;
	}


	/**
	 * When flexible lookup is requested, if FX rate is not found for the date requested, will return latest available rate.
	 */
	public FxRateLookupCommand flexibleLookup(boolean flexible) {
		this.flexibleLookup = flexible;
		return this;
	}


	/**
	 * Checks which currency (fromCurrency or toCurrency) is the dominant currency using currency conventions.
	 * Returns FX using the dominant currency as the fromCurrency: if toCurrency is the dominant currency, then flips to/from currency arguments.
	 */
	public FxRateLookupCommand dominantCurrency() {
		this.dominantCurrency = true;
		return this;
	}


	/**
	 * Checks which currency (fromCurrency or toCurrency) is the dominant currency using currency conventions.
	 * Returns FX using the dominant currency as the fromCurrency: if toCurrency is the dominant currency, then flips to/from currency arguments.
	 */
	public FxRateLookupCommand dominantCurrency(boolean dominantCcy) {
		this.dominantCurrency = dominantCcy;
		return this;
	}


	/**
	 * If the specified data source is not valid or FX rate is not available for the specified data source, do globally default data source lookup
	 * instead of throwing an exception or returning null.
	 */
	public FxRateLookupCommand fallbackToDefaultDataSource() {
		this.fallbackToDefaultDataSource = true;
		return this;
	}


	/**
	 * If the specified data source is not valid, throw an exception.  If  FX rate for the specified Data Source is not available,
	 * return null or throw an exception instead of falling back to the  globally default data source lookup.
	 */
	public FxRateLookupCommand doNotFallbackToDefaultDataSource() {
		this.fallbackToDefaultDataSource = false;
		return this;
	}


	/**
	 * If the specified data source is not valid or FX rate is not available for the specified data source, do globally default data source lookup
	 * instead of throwing an exception or returning null.
	 */
	public FxRateLookupCommand fallbackToDefaultDataSource(boolean fallback) {
		this.fallbackToDefaultDataSource = fallback;
		return this;
	}


	/**
	 * Do not throw an exception if FX rate is not found.
	 */
	public FxRateLookupCommand noExceptionIfRateIsMissing() {
		this.exceptionIfRateIsMissing = false;
		return this;
	}


	/**
	 * If the command was created with a ClientAccount, this method allows a HoldingAccount to be specified as well.
	 */
	public FxRateLookupCommand withHoldingAccount(HoldingAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public Company getFxSourceCompany() {
		return this.fxSourceCompany;
	}


	public HoldingAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public ClientAccount getClientAccount() {
		return this.clientAccount;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public String getFromCurrency() {
		return this.fromCurrency;
	}


	public String getToCurrency() {
		return this.toCurrency;
	}


	public Date getLookupDate() {
		return this.lookupDate;
	}


	public boolean isDominantCurrency() {
		return this.dominantCurrency;
	}


	public boolean isFlexibleLookup() {
		return this.flexibleLookup;
	}


	public boolean isFallbackToDefaultDataSource() {
		return this.fallbackToDefaultDataSource;
	}


	public boolean isExceptionIfRateIsMissing() {
		return this.exceptionIfRateIsMissing;
	}
}
