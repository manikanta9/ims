package com.clifton.marketdata.datasource;

public class MarketDataSourceSectorBuilder {

	private MarketDataSourceSector marketDataSourceSector;


	private MarketDataSourceSectorBuilder(MarketDataSourceSector marketDataSourceSector) {
		this.marketDataSourceSector = marketDataSourceSector;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MarketDataSourceSectorBuilder createCommodity() {
		MarketDataSourceSector marketDataSourceSector = new MarketDataSourceSector();

		marketDataSourceSector.setId((short) 1);
		marketDataSourceSector.setDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
		marketDataSourceSector.setName("Comdty");
		marketDataSourceSector.setLabel("Commodity");
		marketDataSourceSector.setDescription("Commodity");

		return new MarketDataSourceSectorBuilder(marketDataSourceSector);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceSectorBuilder withId(short id) {
		getMarketDataSourceSector().setId(id);
		return this;
	}


	public MarketDataSourceSector toMarketDataSourceSector() {
		return this.marketDataSourceSector;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSourceSector getMarketDataSourceSector() {
		return this.marketDataSourceSector;
	}
}
