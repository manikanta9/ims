package com.clifton.marketdata.datasource.purpose;

public class MarketDataSourcePurposeBuilder {

	private MarketDataSourcePurpose marketDataSourcePurpose;


	private MarketDataSourcePurposeBuilder(MarketDataSourcePurpose marketDataSourcePurpose) {
		this.marketDataSourcePurpose = marketDataSourcePurpose;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MarketDataSourcePurposeBuilder createEmptyMarketDataSourcePurpose() {
		return new MarketDataSourcePurposeBuilder(new MarketDataSourcePurpose());
	}


	public static MarketDataSourcePurposeBuilder createFixMessaging() {
		MarketDataSourcePurpose marketDataSourcePurpose = new MarketDataSourcePurpose();

		marketDataSourcePurpose.setId((short) 2);
		marketDataSourcePurpose.setName("FIX Messaging");
		marketDataSourcePurpose.setDescription("Market data for FIX messaging.");

		return new MarketDataSourcePurposeBuilder(marketDataSourcePurpose);
	}


	public static MarketDataSourcePurposeBuilder createFixEMSXMessaging() {
		MarketDataSourcePurpose marketDataSourcePurpose = new MarketDataSourcePurpose();

		marketDataSourcePurpose.setId((short) 6);
		marketDataSourcePurpose.setName("FIX Messaging (EMSX)");
		marketDataSourcePurpose.setDescription("Bloomberg trade ticket on EMSX via FIX.");

		return new MarketDataSourcePurposeBuilder(marketDataSourcePurpose);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourcePurposeBuilder withId(short id) {
		getMarketDataSourcePurpose().setId(id);
		return this;
	}


	public MarketDataSourcePurposeBuilder withName(String name) {
		getMarketDataSourcePurpose().setName(name);
		return this;
	}


	public MarketDataSourcePurposeBuilder withDescription(String description) {
		getMarketDataSourcePurpose().setDescription(description);
		return this;
	}


	public MarketDataSourcePurpose toMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSourcePurpose getMarketDataSourcePurpose() {
		return this.marketDataSourcePurpose;
	}
}
