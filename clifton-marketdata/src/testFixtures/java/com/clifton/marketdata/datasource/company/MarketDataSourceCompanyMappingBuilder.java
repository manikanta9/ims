package com.clifton.marketdata.datasource.company;

import com.clifton.business.company.BusinessCompany;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.purpose.MarketDataSourcePurpose;


public class MarketDataSourceCompanyMappingBuilder {

	private MarketDataSourceCompanyMapping marketDataSourceCompanyMapping;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSourceCompanyMappingBuilder(MarketDataSourceCompanyMapping marketDataSourceCompanyMapping) {
		this.marketDataSourceCompanyMapping = marketDataSourceCompanyMapping;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MarketDataSourceCompanyMappingBuilder createEmptyMarketDataSourceCompanyMapping() {
		return new MarketDataSourceCompanyMappingBuilder(new MarketDataSourceCompanyMapping());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceCompanyMappingBuilder withId(Integer id) {
		getMarketDataSourceCompanyMapping().setId(id);
		return this;
	}


	public MarketDataSourceCompanyMappingBuilder withMarketDataSource(MarketDataSource marketDataSource) {
		getMarketDataSourceCompanyMapping().setMarketDataSource(marketDataSource);
		return this;
	}


	public MarketDataSourceCompanyMappingBuilder withMarketDataSourcePurpose(MarketDataSourcePurpose marketDataSourcePurpose) {
		getMarketDataSourceCompanyMapping().setMarketDataSourcePurpose(marketDataSourcePurpose);
		return this;
	}


	public MarketDataSourceCompanyMappingBuilder withBusinessCompany(BusinessCompany businessCompany) {
		getMarketDataSourceCompanyMapping().setBusinessCompany(businessCompany);
		return this;
	}


	public MarketDataSourceCompanyMappingBuilder withExternalCompanyName(String externalCompanyName) {
		getMarketDataSourceCompanyMapping().setExternalCompanyName(externalCompanyName);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSourceCompanyMapping getMarketDataSourceCompanyMapping() {
		return this.marketDataSourceCompanyMapping;
	}


	public MarketDataSourceCompanyMapping toMarketDataSourceCompanyMapping() {
		return this.marketDataSourceCompanyMapping;
	}
}
