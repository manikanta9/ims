package com.clifton.marketdata.datasource;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;


public class MarketDataSourceBuilder {

	private MarketDataSource marketDataSource;


	private MarketDataSourceBuilder(MarketDataSource marketDataSource) {
		this.marketDataSource = marketDataSource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MarketDataSourceBuilder createEmptyMarketDataSource() {
		return new MarketDataSourceBuilder(new MarketDataSource());
	}


	public static MarketDataSourceBuilder createRedi() {
		MarketDataSource marketDataSource = new MarketDataSource();

		marketDataSource.setId((short) 20);
		marketDataSource.setCompany(BusinessCompanyBuilder.createRedi().toBusinessCompany());
		marketDataSource.setName("REDI");
		marketDataSource.setDescription("REDI provides a multi-broker trading platform.");

		return new MarketDataSourceBuilder(marketDataSource);
	}


	public static MarketDataSourceBuilder createBloomberg() {
		MarketDataSource marketDataSource = new MarketDataSource();

		marketDataSource.setId((short) 1);
		marketDataSource.setCompany(BusinessCompanyBuilder.createBloomberg().toBusinessCompany());
		marketDataSource.setName("Bloomberg");
		marketDataSource.setDescription("Bloomberg is a company that provides a broad range of financial information including security prices and news.");
		marketDataSource.setMarketSectorSupported(true);
		marketDataSource.setDefaultDataSource(true);
		marketDataSource.setLabel("Bloomberg");

		return new MarketDataSourceBuilder(marketDataSource);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataSourceBuilder withId(short id) {
		getMarketDataSource().setId(id);
		return this;
	}


	public MarketDataSourceBuilder withCompany(BusinessCompany businessCompany) {
		getMarketDataSource().setCompany(businessCompany);
		return this;
	}


	public MarketDataSourceBuilder withName(String name) {
		getMarketDataSource().setName(name);
		return this;
	}


	public MarketDataSourceBuilder withDescription(String description) {
		getMarketDataSource().setDescription(description);
		return this;
	}


	public MarketDataSource toMarketDataSource() {
		return this.marketDataSource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MarketDataSource getMarketDataSource() {
		return this.marketDataSource;
	}
}
