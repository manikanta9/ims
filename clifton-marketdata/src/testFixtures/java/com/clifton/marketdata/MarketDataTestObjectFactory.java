package com.clifton.marketdata;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.core.util.MathUtils;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class MarketDataTestObjectFactory {

	private static final Map<String, BigDecimal> EXCHANGE_RATE_MAP = new HashMap<>();


	static {
		EXCHANGE_RATE_MAP.put("USD->CAD", new BigDecimal("1.0016"));
		EXCHANGE_RATE_MAP.put("CAD->USD", MathUtils.divide(BigDecimal.ONE, new BigDecimal("1.0016")));

		EXCHANGE_RATE_MAP.put("USD->YEN", new BigDecimal("82.4800"));
		EXCHANGE_RATE_MAP.put("YEN->USD", MathUtils.divide(BigDecimal.ONE, new BigDecimal("82.4800")));

		EXCHANGE_RATE_MAP.put("CAD->YEN", new BigDecimal("82.2484"));
		EXCHANGE_RATE_MAP.put("YEN->CAD", MathUtils.divide(BigDecimal.ONE, new BigDecimal("82.2484")));
	}


	public static MarketDataSourceService newMarketDataSourceService() {
		MarketDataSourceService result = Mockito.mock(MarketDataSourceService.class);
		Mockito.when(result.getMarketDataSourceName(ArgumentMatchers.nullable(BusinessCompany.class))).thenReturn("TestSource");
		return result;
	}


	public static MarketDataRetriever newMarketDataRetriever() {
		return Mockito.mock(MarketDataRetriever.class);
	}


	public static MarketDataExchangeRatesApiService newMarketDataExchangeRatesApiService(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, BigDecimal flexibleRate) {
		MarketDataExchangeRatesApiService ratesApi = Mockito.mock(MarketDataExchangeRatesApiService.class);
		Mockito.when(ratesApi.getExchangeRate(Mockito.argThat(command -> {
			if (command == null) {
				return false;
			}
			return StringUtils.isEqual(command.getFromCurrency(), command.getToCurrency());
		}))).thenReturn(BigDecimal.ONE);
		Mockito.when(ratesApi.getExchangeRate(Mockito.argThat(command -> {
					if (!command.getFromCurrency().equals(fromCurrency.getSymbol())) {
						return false;
					}
					if (!command.getToCurrency().equals(toCurrency.getSymbol())) {
						return false;
					}
					return true;
				}))
		).thenReturn(flexibleRate);
		return ratesApi;
	}


	public static MarketDataExchangeRatesApiService newMarketDataExchangeRatesApiService(Map<String, BigDecimal> fxRatesMap) {
		MarketDataExchangeRatesApiService ratesApi = Mockito.mock(MarketDataExchangeRatesApiService.class);
		Mockito.doAnswer(invocation -> {
					FxRateLookupCommand command = (FxRateLookupCommand) invocation.getArguments()[0];
					if (command.getFromCurrency().equals(command.getToCurrency())) {
						return BigDecimal.ONE;
					}
					String key = command.getFromCurrency() + "->" + command.getToCurrency();
					return fxRatesMap.getOrDefault(key, BigDecimal.ONE);
				}
		).when(ratesApi).getExchangeRate(Mockito.any());

		return ratesApi;
	}


	public static MarketDataExchangeRatesApiService newMarketDataExchangeRatesApiService() {
		return newMarketDataExchangeRatesApiService(EXCHANGE_RATE_MAP);
	}
}
