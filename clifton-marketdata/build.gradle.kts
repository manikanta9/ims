import com.clifton.gradle.plugin.build.registerVariant
import com.clifton.gradle.plugin.build.usingVariant


val apiVariant = registerVariant("api", upstreamForMain = true)


dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	apiVariant.api(project(":clifton-investment")) { usingVariant("shared") }

	api(project(":clifton-batch"))
	api(project(":clifton-investment"))
	api(project(":clifton-business"))
	api(project(":clifton-business:clifton-business-contact"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-investment")))
	testFixturesApi(testFixtures(project(":clifton-business")))
	testFixturesApi(testFixtures(project(":clifton-batch")))
}
